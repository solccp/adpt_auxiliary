#ifndef DATASTRUCTURE_VARIANT_H
#define DATASTRUCTURE_VARIANT_H

#include <Variant/Variant.h>
#include "variantqt.h"

#include "geometry.h"
#include "bandstructuredata.h"

#include "molecularatomizationenergydata.h"
#include "molecularfrequencydata.h"

#include "reactionenergydata.h"

namespace libvariant
{
    template<>
    struct VariantConvertor<ReactionItem>
    {
        static libvariant::Variant toVariant(const ReactionItem &rhs)
        {
            Variant var;
            //essential data
            var["uuid"] = rhs.uuid.toStdString();
            var["coeff"] = rhs.coeff;
            return var;
        }
        static void fromVariant(const libvariant::Variant &var, ReactionItem &rhs)
        {
            GET_VARIANT(rhs.uuid, var, "uuid", QString);
            GET_VARIANT(rhs.coeff, var, "coeff", double);
        }
    };

    template<>
    struct VariantConvertor<ReactionEnergyData>
    {
        using lpds = QList<ReactionItem> ;
        static libvariant::Variant toVariant(const ReactionEnergyData &rhs)
        {
            Variant var;
            //essential data
            var["uuid"] = rhs.m_UUID.toStdString();
            var["method"] = rhs.m_method.toStdString();
            var["basis_set"] = rhs.m_basis.toStdString();
            var["comment"] = rhs.m_comment.toStdString();
            var["citation"] = rhs.m_citation.toStdString();

            var["name"] = rhs.m_name.toStdString();
            var["energy"] = rhs.m_energy;
            var["unit"] = rhs.m_energy_unit.toStdString();


            var["products"] = VariantConvertor<lpds>::toVariant(rhs.m_products);
            var["reactants"] = VariantConvertor<lpds>::toVariant(rhs.m_reactants);

            var["optimize_components"] = rhs.m_optimize_components;

            return var;
        }
        static void fromVariant(const libvariant::Variant &var, ReactionEnergyData &rhs)
        {
            GET_VARIANT(rhs.m_UUID, var, "uuid", QString);
            GET_VARIANT(rhs.m_method, var, "method", QString);

            bool hasValue;
            GET_VARIANT_OPT(rhs.m_basis, var, "basis_set", QString, hasValue);
            GET_VARIANT_OPT(rhs.m_comment, var, "comment", QString, hasValue);
            GET_VARIANT_OPT(rhs.m_citation, var, "citation", QString, hasValue);

            GET_VARIANT(rhs.m_name, var, "name", QString);
            GET_VARIANT(rhs.m_energy, var, "energy", double);
            GET_VARIANT(rhs.m_energy_unit, var, "unit", QString);

            GET_VARIANT(rhs.m_products, var, "products", lpds);
            GET_VARIANT(rhs.m_reactants, var, "reactants", lpds);

            GET_VARIANT(rhs.m_optimize_components, var, "optimize_components", bool);

            Q_UNUSED(hasValue);
        }
    };

    template<>
    struct VariantConvertor<MolecularFrequencyData>
    {
        static libvariant::Variant toVariant(const MolecularFrequencyData &rhs)
        {
            Variant var;
            //essential data
            var["uuid"] = rhs.m_UUID.toStdString();
            var["method"] = rhs.m_method.toStdString();
            var["basis_set"] = rhs.m_basis.toStdString();
            var["comment"] = rhs.m_comment.toStdString();
            var["citation"] = rhs.m_citation.toStdString();
            var["geom_uuid"] = rhs.m_geom_uuid.toStdString();

            var["frequency"] = VariantConvertor<QList<double> >::toVariant(rhs.m_freq);
            var["weight"] = VariantConvertor<QList<double> >::toVariant(rhs.m_weight);

            return var;
        }
        static void fromVariant(const libvariant::Variant &var, MolecularFrequencyData &rhs)
        {
            GET_VARIANT(rhs.m_UUID, var, "uuid", QString);
            GET_VARIANT(rhs.m_method, var, "method", QString);

            bool hasValue;
            GET_VARIANT_OPT(rhs.m_basis, var, "basis_set", QString, hasValue);
            GET_VARIANT_OPT(rhs.m_comment, var, "comment", QString, hasValue);
            GET_VARIANT_OPT(rhs.m_citation, var, "citation", QString, hasValue);

            GET_VARIANT(rhs.m_freq, var, "frequency", QList<double>);
            GET_VARIANT_OPT(rhs.m_weight, var, "weight", QList<double>, hasValue);
            if (hasValue)
            {
                if (rhs.m_weight.size() != rhs.m_freq.size())
                {
                    rhs.m_weight.clear();
                }
            }

            GET_VARIANT(rhs.m_geom_uuid, var, "geom_uuid", QString);

            Q_UNUSED(hasValue);
        }
    };

    template<>
    struct VariantConvertor<MolecularAtomizationEnergyData>
    {
        static libvariant::Variant toVariant(const MolecularAtomizationEnergyData &rhs)
        {
            Variant var;
            //essential data
            var["uuid"] = rhs.m_UUID.toStdString();
            var["method"] = rhs.m_method.toStdString();
            var["basis_set"] = rhs.m_basis.toStdString();
            var["comment"] = rhs.m_comment.toStdString();
            var["citation"] = rhs.m_citation.toStdString();
            var["geom_uuid"] = rhs.m_geom_uuid.toStdString();
            var["energy"] = rhs.m_energy;
            var["unit"] = rhs.m_energy_unit.toStdString();

            return var;
        }
        static void fromVariant(const libvariant::Variant &var, MolecularAtomizationEnergyData &rhs)
        {
            GET_VARIANT(rhs.m_UUID, var, "uuid", QString);
            GET_VARIANT(rhs.m_method, var, "method", QString);

            bool hasValue;
            GET_VARIANT_OPT(rhs.m_basis, var, "basis_set", QString, hasValue);
            GET_VARIANT_OPT(rhs.m_comment, var, "comment", QString, hasValue);
            GET_VARIANT_OPT(rhs.m_citation, var, "citation", QString, hasValue);

            GET_VARIANT(rhs.m_energy, var, "energy", double);
            GET_VARIANT(rhs.m_geom_uuid, var, "geom_uuid", QString);

            GET_VARIANT(rhs.m_energy_unit, var, "unit", QString);

            Q_UNUSED(hasValue);
        }
    };


    template<>
    struct VariantConvertor<BSOptTargetInfo>
    {
        static libvariant::Variant toVariant(const BSOptTargetInfo &rhs)
        {
            Variant var;
            if (rhs.index_based)
            {
                var["band_end"] = rhs.band_endindex;
                var["band_start"] = rhs.band_startindex;
                var["point_end"] = rhs.point_endindex;
                var["point_start"] = rhs.point_startindex;
                var["type"] = "index_based";
            }
            else
            {
                var["lower_bound"] = rhs.energy_lowerbound;
                var["upper_bound"] = rhs.energy_upperbound;
                var["type"] = "energy_based";
            }
            var["weight"] = rhs.weight;
            return var;
        }
        static void fromVariant(const libvariant::Variant &var, BSOptTargetInfo &rhs)
        {
            QString type;
            GET_VARIANT(type, var, "type", QString);
            GET_VARIANT(rhs.weight, var, "weight", double);

            if (type.toLower() == "index_based")
            {
                rhs.index_based = true;
                GET_VARIANT(rhs.band_endindex, var, "band_end", int);
                GET_VARIANT(rhs.band_startindex, var, "band_start", int);
                GET_VARIANT(rhs.point_endindex, var, "point_end", int);
                GET_VARIANT(rhs.point_startindex, var, "point_start", int);
            }
            else
            {
                rhs.index_based = false;
                GET_VARIANT(rhs.energy_lowerbound, var, "lower_bound", double);
                GET_VARIANT(rhs.energy_upperbound, var, "upper_bound", double);
            }
        }
    };

//    template<>
//    struct VariantConvertor<vKLineEntry>
//    {
//        static libvariant::Variant toVariant(const vKLineEntry &rhs)
//        {
//            Variant var;
//            var.Append(rhs[0].toInt());
//            var.Append(rhs[1].toDouble());
//            var.Append(rhs[2].toDouble());
//            var.Append(rhs[3].toDouble());
//            return var;
//        }
//        static void fromVariant(const libvariant::Variant &var, vKLineEntry &rhs)
//        {
//             GET_VARIANT_LIST(rhs[0], var, 0, QVariant);
//             GET_VARIANT_LIST(rhs[1], var, 1, QVariant);
//             GET_VARIANT_LIST(rhs[2], var, 2, QVariant);
//             GET_VARIANT_LIST(rhs[3], var, 3, QVariant);
//        }
//    };




    template<>
    struct VariantConvertor<Coordinate>
    {
        static libvariant::Variant toVariant(const Coordinate &rhs)
        {
            Variant var;
            var.Append(rhs.symbol.toStdString());
            for(auto item : rhs.coord)
            {
                var.Append(item);
            }
            return var;
        }
        static void fromVariant(const libvariant::Variant &var, Coordinate &rhs)
        {
            GET_VARIANT_LIST(rhs.symbol, var, 0, QString);
            GET_VARIANT_LIST(rhs.coord[0], var, 1, double);
            GET_VARIANT_LIST(rhs.coord[1], var, 2, double);
            GET_VARIANT_LIST(rhs.coord[2], var, 3, double);
        }
    };


    template<>
    struct VariantConvertor<Geometry>
    {
        static libvariant::Variant toVariant(const Geometry &rhs)
        {
            Variant var;
            //essential data
            var["uuid"] = rhs.m_UUID.toStdString();
            var["name"] = rhs.m_name.toStdString();
            var["method"] = rhs.m_method.toStdString();
            var["basis_set"] = rhs.m_basis.toStdString();
            var["comment"] = rhs.m_comment.toStdString();
            var["citation"] = rhs.m_citation.toStdString();

            var["pbc"] = rhs.m_PBC;
            var["coordinates"] = VariantConvertor<QList<Coordinate>>::toVariant(rhs.m_coordinates);

            if (rhs.m_PBC)
            {
                var["kpoints"] = VariantConvertor<ADPT::KPointsSetting>::toVariant(rhs.m_kpoints_setting);
                var["lattice_vectors"] = VariantConvertor<dMatrix3x3>::toVariant(rhs.m_lattice_vectors);
                var["fractional"] = rhs.m_relativeCoordinates;
                var["scaling_factor"] = rhs.m_scaling_factor;
            }
            else
            {
                var["stationary_point"] = rhs.m_isStationary;
                if (!rhs.m_isStationary && rhs.m_force.size() == rhs.m_coordinates.size() )
                {
                    var["force"] = VariantConvertor<QList<dVector3> >::toVariant(rhs.m_force);
                }
                var["charge"] = rhs.m_charge;
                var["spin"] = rhs.m_spin;
            }
            return var;
        }
        static void fromVariant(const libvariant::Variant &var, Geometry &rhs)
        {
            GET_VARIANT(rhs.m_UUID, var, "uuid", QString);
            GET_VARIANT(rhs.m_method, var, "method", QString);
            GET_VARIANT(rhs.m_name, var, "name", QString);

            bool hasValue;
            GET_VARIANT_OPT(rhs.m_basis, var, "basis_set", QString, hasValue);
            GET_VARIANT_OPT(rhs.m_comment, var, "comment", QString, hasValue);
            GET_VARIANT_OPT(rhs.m_citation, var, "citation", QString, hasValue);

            GET_VARIANT(rhs.m_coordinates, var, "coordinates", QList<Coordinate>);

            GET_VARIANT(rhs.m_PBC, var, "pbc", bool);
            if (rhs.m_PBC)
            {
                GET_VARIANT(rhs.m_scaling_factor, var, "scaling_factor", double);
                GET_VARIANT(rhs.m_relativeCoordinates, var, "fractional", bool);

                GET_VARIANT(rhs.m_lattice_vectors, var, "lattice_vectors", dMatrix3x3);


                //get the kpoints
                GET_VARIANT(rhs.m_kpoints_setting, var, "kpoints", ADPT::KPointsSetting);
            }
            else
            {
                GET_VARIANT(rhs.m_isStationary, var, "stationary_point", bool);
                if (!rhs.m_isStationary)
                {
                    GET_VARIANT_OPT(rhs.m_force, var, "force", QList<dVector3>, hasValue);
                    if (rhs.m_force.size() != rhs.m_coordinates.size())
                    {
                        rhs.m_force.clear();
                    }
                }

                GET_VARIANT(rhs.m_charge, var, "charge", int);
                GET_VARIANT(rhs.m_spin, var, "spin", int);
            }

            rhs.computeFormula();
            Q_UNUSED(hasValue);
        }
    };

    template<>
    struct VariantConvertor<BandstructureData>
    {
        static libvariant::Variant toVariant(const BandstructureData &rhs)
        {
            Variant var;
            //essential data
            var["uuid"] = rhs.m_UUID.toStdString();
            var["name"] = rhs.m_name.toStdString();
            var["method"] = rhs.m_method.toStdString();
            var["basis_set"] = rhs.m_basis.toStdString();
            var["comment"] = rhs.m_comment.toStdString();
            var["citation"] = rhs.m_citation.toStdString();

            var["geom_uuid"] = rhs.m_geom_uuid.toStdString();
            if (rhs.m_fermiIndex.first == -1 && rhs.m_fermiIndex.second == -1)
            {
                var["fermi_index"] = VariantConvertor<QPair<int, int> >::toVariant(rhs.m_fermiIndex);
            }

            var["kpoints"] = VariantConvertor<ADPT::KPointsSetting>::toVariant(rhs.m_kpoints_setting);

            var["bands"] = VariantConvertor<QVector<QVector<double> > >::toVariant(rhs.m_bands);

            var["default_optimization_target"] = VariantConvertor<QList<BSOptTargetInfo> >::toVariant(rhs.m_default_optimization);

            return var;
        }
        static void fromVariant(const libvariant::Variant &var, BandstructureData &rhs)
        {
            GET_VARIANT(rhs.m_UUID, var, "uuid", QString);
            GET_VARIANT(rhs.m_method, var, "method", QString);
            GET_VARIANT(rhs.m_name, var, "name", QString);

            bool hasValue;
            GET_VARIANT_OPT(rhs.m_basis, var, "basis_set", QString, hasValue);
            GET_VARIANT_OPT(rhs.m_comment, var, "comment", QString, hasValue);
            GET_VARIANT_OPT(rhs.m_citation, var, "citation", QString, hasValue);

            GET_VARIANT(rhs.m_geom_uuid, var, "geom_uuid", QString);

            using pii = QPair<int, int>;
            GET_VARIANT_OPT(rhs.m_fermiIndex, var, "fermi_index", pii, hasValue);

            GET_VARIANT_OPT(rhs.m_default_optimization, var, "default_optimization_target", QList<BSOptTargetInfo>, hasValue );

            using vec = QVector<QVector<double> > ;
            GET_VARIANT(rhs.m_bands, var, "bands", vec);

            GET_VARIANT(rhs.m_kpoints_setting, var, "kpoints", ADPT::KPointsSetting);
            Q_UNUSED(hasValue);
        }
    };
}
#endif // DATASTRUCTURE_VARIANT_H

