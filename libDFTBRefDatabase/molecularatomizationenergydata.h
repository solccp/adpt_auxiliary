#ifndef MOLECULARATOMIZATIONENERGYDATA_H
#define MOLECULARATOMIZATIONENERGYDATA_H

#include <QObject>

namespace libvariant
{
    template<typename T>
    class VariantConvertor;
}

class MolecularAtomizationEnergyData : public QObject
{
    Q_OBJECT
    friend class DFTBRefDatabase;
    friend class libvariant::VariantConvertor<MolecularAtomizationEnergyData>;
public:
    explicit MolecularAtomizationEnergyData(QObject *parent = 0);

    QString UUID() const;
    void resetUUID();

    QString geom_uuid() const;
    void setGeom_uuid(const QString &geom_uuid);

    QString method() const;
    void setMethod(const QString &method);

    QString basis() const;
    void setBasis(const QString &basis);

    double energy() const;
    void setEnergy(double energy);

    QString citation() const;
    void setCitation(const QString &citation);

    QString comment() const;
    void setComment(const QString &comment);

    QString unit() const;

    QString createUUID();

signals:

public slots:
    void setUnit(const QString& unit);
private:
    QString m_UUID ;
    QString m_geom_uuid;

    QString m_method;
    QString m_basis;
    QString m_citation;
    QString m_comment;

    double m_energy = 0.0;
    QString m_energy_unit = "kcal/mol";
};

#endif // MOLECULARATOMIZATIONENERGYDATA_H
