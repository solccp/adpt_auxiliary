#include "dftbrefdatabase.h"
#include <QObject>

#include <QtSql>
#include <QDebug>
#include <QSqlQueryModel>
#include <QRegularExpression>
#include <QStringBuilder>

#include "geometry.h"
#include "bandstructuredata.h"
#include "molecularfrequencydata.h"
#include "molecularatomizationenergydata.h"
#include "reactionenergydata.h"

#include "geometrylistmodel.h"

#include "variantqt.h"
#include "datastructure_variant.h"

const int DB_VERSION = 7;

DFTBRefDatabase::DFTBRefDatabase(QObject *parent) : QObject(parent)
{

}

DFTBRefDatabase::~DFTBRefDatabase()
{
    closeDatabase();
}

void DFTBRefDatabase::disbleSignal()
{
    mutex.lock();
    m_signalEnabled = false;
    mutex.unlock();
}

void DFTBRefDatabase::enableSignal()
{
    mutex.lock();
    m_signalEnabled = true;
    mutex.unlock();
}

int DFTBRefDatabase::loadOnlineDatabase(const QString &hostName, const QString &userName,
                                         const QString &password, const QString &databaseName)
{
    int errorID = 0;
    bool opened = false;
    {
        QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL", "remote_1");
        db.setHostName(hostName);
        db.setDatabaseName(databaseName);
        db.setUserName(userName);
        db.setPassword(password);
        opened = db.open();
        if (!opened)
        {
            m_lastError = db.lastError().text();
            errorID = db.lastError().number();
            qDebug() << db.lastError();
        }
    }

    if (!opened)
    {
        QSqlDatabase::removeDatabase("remote_1");

        switch(errorID)
        {
        case 2003:
        case 2005:
            return DFTBRefDatabaseError::ServerError;
        case 1045:
            return DFTBRefDatabaseError::AccountError;
        case 1049:
            return DFTBRefDatabaseError::DatabaseNameError;
        }
        return DFTBRefDatabaseError::UnknownError;
    }

    int checkError = checkPermission();



    checkError = checkDatabase();
    if(checkError != DFTBRefDatabaseError::NoError)
    {
        if (!m_lastError.isEmpty())
        {
            qDebug() << m_lastError;
        }
        resetNeeded = true;
        database().close();
        QSqlDatabase::removeDatabase("remote_1");
        return checkError;
    }

    if (!canRead())
    {
        return DFTBRefDatabaseError::NoReadPermissionError;
    }

    emit databaseConnectionChanged();

    return DFTBRefDatabaseError::NoError;
}


bool DFTBRefDatabase::loadDatabasefile(const QString &filename)
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", "local_1");
    db.setDatabaseName(filename);
    if (!db.open())
    {
        m_lastError = db.lastError().text();
        return false;
    }

    resetNeeded = false;
    if(!checkDatabase())
    {
//        m_lastError = db.lastError().text();
        if (!m_lastError.isEmpty())
        {
            qDebug() << m_lastError;
        }
        resetNeeded = true;
//        db.close();
        return false;
    }

    return true;
}

void DFTBRefDatabase::closeDatabase()
{
    QString connection;
    connection = database().connectionName();
    database().close();
    QSqlDatabase::removeDatabase(connection);
    emit databaseConnectionChanged();
}

QString DFTBRefDatabase::getLastError() const
{
    return m_lastError;
}

bool DFTBRefDatabase::isResetNeeded()
{
    return resetNeeded;
}

bool DFTBRefDatabase::setupTables()
{
    return true;
}

void DFTBRefDatabase::clear()
{

}

void DFTBRefDatabase::clearError()
{
    m_lastError = "";
}

bool DFTBRefDatabase::isOpen()
{
    return database().isOpen();
}




QAbstractTableModel *DFTBRefDatabase::getGeometryListModel(DFTBRefDatabase::GeometryType type)
{
    QSqlDatabase db = database();
    QSqlQueryModel *model = new QSqlQueryModel(this);
    if (type == Both)
    {
        model->setQuery(" SELECT UUID, name, 0 as `charge`, 1 as `is_crystal`, method, basis,  elements FROM crystal_geometry "
                        " UNION "
                        " SELECT uuid, name, charge as `charge`, 0 as `is_crystal`, method, basis, elements from molecular_geometry "
                        " ORDER BY `name` ASC", db);
    }
    else if ( type == Crystal )
    {
        model->setQuery( "SELECT UUID, name, 0 as `charge`, 1 as `is_crystal`, method, basis,  elements FROM crystal_geometry", db);
    }
    else
    {
        model->setQuery(" SELECT uuid, name, charge as `charge`, 0 as `is_crystal`, method, basis, elements from molecular_geometry", db);
    }
    while(model->canFetchMore())
    {
        model->fetchMore();
    }
    GeometryListModel *gmodel = new GeometryListModel(model, this);
    return gmodel;
}

QAbstractTableModel *DFTBRefDatabase::getGeometryTableModel(GeometryType type)
{
    QSqlDatabase db = database();
    QSqlQueryModel *model = new QSqlQueryModel;
    if (type == Both)
    {
        model->setQuery(" SELECT UUID, name, 0 as `charge`, 1 as `is_crystal`, method, basis,  elements FROM crystal_geometry "
                        " UNION "
                        " SELECT uuid, name, charge as `charge`, 0 as `is_crystal`, method, basis, elements from molecular_geometry "
                        " ORDER BY `name` ASC", db);
    }
    else if ( type == Crystal )
    {
        model->setQuery( "SELECT UUID, name, 0 as `charge`, 1 as `is_crystal`, method, basis,  elements FROM crystal_geometry", db);
    }
    else
    {
        model->setQuery(" SELECT uuid, name, charge as `charge`, 0 as `is_crystal`, method, basis, elements from molecular_geometry", db);
    }
    model->setHeaderData(0, Qt::Horizontal, tr("UUID"));
    model->setHeaderData(1, Qt::Horizontal, tr("Name"));
    model->setHeaderData(2, Qt::Horizontal, tr("Charge"));
    model->setHeaderData(3, Qt::Horizontal, tr("Crystal?"));
    model->setHeaderData(4, Qt::Horizontal, tr("Method"));
    model->setHeaderData(5, Qt::Horizontal, tr("Basis"));
    model->setHeaderData(6, Qt::Horizontal, tr("Elements"));
    while(model->canFetchMore())
    {
        model->fetchMore();
    }

    return model;
}

bool DFTBRefDatabase::hasGeometryUUID(const QString &uuid, DFTBRefDatabase::GeometryType type)
{

    QSqlQuery query(database());
    if (type == Crystal)
    {
        query.prepare("SELECT UUID FROM geometry_uuid WHERE is_crystal = 1 and UUID=:uuid");
    }
    else if (type == Molecule)
    {
        query.prepare("SELECT UUID FROM geometry_uuid WHERE is_crystal = 0  and UUID=:uuid");
    }
    query.bindValue(":uuid", uuid);

    bool succ = query.exec();

    if (!succ)
    {
        return false;
    }

    if (query.next())
    {
        return true;
    }

    return false;
}


QStringList DFTBRefDatabase::getAvailableElements()
{
    QSqlQuery query(database());
    query.prepare("SELECT elements FROM geometry");
    bool succ = query.exec();

    QStringList elems;

    if (!succ)
    {
        m_lastError = query.lastError().text();
        m_lastErrorNumber = query.lastError().number();
        qDebug() << query.lastError().number() << query.lastError().text();
        return elems;
    }

    while (query.next())
    {
        QString str = query.value(0).toString();
        QStringList arr = str.split(",", QString::SkipEmptyParts);
        for(int i=0; i<arr.size();++i)
        {
            QString elem = arr.at(i);
            elem.replace(QRegularExpression("[\\[\\]]"),"");
            elems.append(elem);
        }
    }
    elems.removeDuplicates();
    return elems;
}

QList<QString> DFTBRefDatabase::getGeometryUUID()
{
    QSqlDatabase db = database();
    QSqlQueryModel *model = new QSqlQueryModel;
    model->setQuery(" SELECT UUID, is_crystal FROM geometry_uuid", db);
    while(model->canFetchMore())
    {
        model->fetchMore();
    }

    QList<QString> res;
    for(int i=0; i<model->rowCount(); ++i)
    {
        res.append(model->data(model->index(i,0)).toString());
    }

    return res;
}

QSqlDatabase DFTBRefDatabase::database() const
{
    return QSqlDatabase::database(dbname);
}



bool DFTBRefDatabase::getGeometry(const QString &uuid, Geometry *geom)
{
    if (!getMolecularGeometry(uuid, geom))
    {
        return getCrystalGeometry(uuid, geom);
    }
    return true;
}

bool DFTBRefDatabase::getBandStructure(const QString &uuid, BandstructureData *bandstructure)
{
    QSqlQuery query(database());
    query.prepare("SELECT * FROM bandstructure WHERE UUID = :UUID;");
    query.bindValue(":UUID", uuid);
    bool succ = query.exec();

    if (!succ)
    {
        m_lastError = query.lastError().text();
        m_lastErrorNumber = query.lastError().number();
        qDebug() << query.lastError().number() << query.lastError().text();
        return false;
    }

    if (query.next())
    {
        bandstructure->m_UUID = query.value("UUID").toString();
        bandstructure->m_name = query.value("name").toString().trimmed();
        bandstructure->m_method = query.value("method").toString().trimmed();
        bandstructure->m_basis = query.value("basis").toString().trimmed();
        bandstructure->m_comment = query.value("comment").toString();
        bandstructure->m_citation = query.value("citation").toString();

        bandstructure->parseJson_bands(query.value("bands").toString());
        bandstructure->parseJson_kpoints(query.value("kpoints").toString());
        bandstructure->parseJson_default_optimization(query.value("default_optimization_target").toString());

        bandstructure->m_geom_uuid = query.value("geom_id").toString();


        bandstructure->parseJson_fermiIndex(query.value("fermi_index").toString());
        return true;
    }
    return false;
}
bool DFTBRefDatabase::getReactionEnergy(const QString &uuid, ReactionEnergyData *data)
{
    QSqlQuery query(database());

    query.prepare("SELECT UUID, name, method, basis, energy, energy_unit, need_opt, citation, comment, is_reactant, coefficient, geom_id FROM `reactionenergy`, `reaction_components`"
                   " WHERE `reactionenergy`.UUID=:UUID AND `reaction_components`.reaction_id = `reactionenergy`.UUID ;");

    query.bindValue(":UUID", uuid);
    bool succ = query.exec();

    if (!succ)
    {
        m_lastError = query.lastError().text();
        m_lastErrorNumber = query.lastError().number();
        qDebug() << query.lastError().number() << query.lastError().text();
        return false;
    }
    bool first = true;
    while (query.next())
    {
        if (first)
        {
            first = false;
            data->setUUID(query.value("UUID").toString());
            data->setName(query.value("name").toString().trimmed());
            data->setMethod(query.value("method").toString().trimmed());
            data->setBasis(query.value("basis").toString().trimmed());
            data->setEnergy(query.value("energy").toDouble());
            data->setEnergyUnit(query.value("energy_unit").toString().trimmed());
            data->setCitation(query.value("citation").toString());
            data->setComment(query.value("comment").toString());
            data->setOptimizeComponents(query.value("need_opt").toBool());
        }

        double coeff = query.value("coefficient").toDouble();
        QString geom_id = query.value("geom_id").toString();

        ReactionItem item;
        item.coeff = coeff;
        item.uuid = geom_id;

        if (query.value("is_reactant").toBool())
        {
            data->reactants().append(item);
        }
        else
        {
            data->products().append(item);
        }

    }
    return true;
}

bool DFTBRefDatabase::addMolecularGeometry(Geometry *data)
{
    if (data->m_PBC)
    {
        m_lastError = "It's not a molecule";
        return false;
    }

    transaction();
    QSqlQuery query(database());
    query.prepare("INSERT INTO geometry_uuid (UUID, is_crystal)"
                  " VALUES(:UUID, :is_crystal)");
    query.bindValue(":UUID", data->m_UUID);
    query.bindValue(":is_crystal", data->m_PBC);
    bool succ = query.exec();
    if (!succ)
    {
        rollback();
        if (query.lastError().isValid())
        {
            m_lastError = query.lastError().text();
            m_lastErrorNumber = query.lastError().number();
            qDebug() << query.lastError().number() << query.lastError().text();
        }
        return false;
    }


    bindSQLMolecularGeometry(query, data, false);

    succ = query.exec();
    if (!succ)
    {
        qDebug() << query.executedQuery();
        rollback();
        if (query.lastError().isValid())
        {
            m_lastError = query.lastError().text();
            m_lastErrorNumber = query.lastError().number();
            qDebug() << query.lastError().number() << query.lastError().text();
        }
        return false;
    }
    commit();


    if (m_signalEnabled)
        emit geometryUpdated();
    return true;
}

bool DFTBRefDatabase::addCrystalGeometry(Geometry *data)
{
    if (!data->m_PBC)
    {
        m_lastError = "It's not a crystal";
        return false;
    }


    transaction();
    QSqlQuery query(database());
    query.prepare("INSERT INTO geometry_uuid (UUID, is_crystal)"
                  " VALUES(:UUID, :is_crystal)");

    query.bindValue(":UUID", data->m_UUID);
    query.bindValue(":is_crystal", data->m_PBC);

    bool succ = query.exec();
    if (!succ)
    {
        rollback();
        if (query.lastError().isValid())
        {
            m_lastError = query.lastError().text();
            m_lastErrorNumber = query.lastError().number();
            qDebug() << query.lastError().number() << query.lastError().text();
        }
        return false;
    }  

    bindSQLCrystalGeometry(query, data, false);

    succ = query.exec();
    if (!succ)
    {
        rollback();
        if (query.lastError().isValid())
        {
            m_lastError = query.lastError().text();
            m_lastErrorNumber = query.lastError().number();
            qDebug() << query.lastError().number() << query.lastError().text();
        }
        return false;
    }
    commit();

    if (m_signalEnabled)
        emit geometryUpdated();
    return true;
}

bool DFTBRefDatabase::getMolecularGeometry(const QString &uuid, Geometry *geom)
{
    QSqlQuery query(database());
    query.prepare("SELECT * FROM molecular_geometry WHERE UUID = :UUID;");
    query.bindValue(":UUID", uuid);
    bool succ = query.exec();

    if (!succ)
    {
        m_lastError = query.lastError().text();
        m_lastErrorNumber = query.lastError().number();
        qDebug() << query.lastError().number() << query.lastError().text();
        return false;
    }

    if (query.next())
    {
        geom->m_UUID = query.value("UUID").toString();
        geom->m_name = query.value("name").toString().trimmed();



        geom->m_charge = query.value("charge").toInt();
        geom->m_spin = query.value("spin").toInt();
        geom->m_isStationary = query.value("is_stationary").toBool();

        geom->m_method = query.value("method").toString().trimmed();
        geom->m_basis = query.value("basis").toString().trimmed();

        geom->m_comment = query.value("comment").toString();
        geom->m_citation = query.value("citation").toString();

        geom->parseDBCoordinates(query.value("coordinates"));
        geom->computeFormula();

        try
        {
            std::string force_str = query.value("force").toString().toStdString();
            libvariant::Variant var =  libvariant::DeserializeJSON(force_str);
            libvariant::VariantConvertor<QList<dVector3> >::fromVariant(var, geom->m_force);
        }
        catch(...)
        {
            geom->m_force.clear();
        }



        //geom->setFormula(query.value("formula").toString());

        return true;
    }
    return false;
}

bool DFTBRefDatabase::getCrystalGeometry(const QString &uuid, Geometry *geom)
{
    QSqlQuery query(database());
    query.prepare("SELECT * FROM crystal_geometry WHERE UUID = :UUID;");
    query.bindValue(":UUID", uuid);
    bool succ = query.exec();

    if (!succ)
    {
        m_lastError = query.lastError().text();
        m_lastErrorNumber = query.lastError().number();
        qDebug() << query.lastError().number() << query.lastError().text();
        return false;
    }

    if (query.next())
    {
        geom->m_UUID = query.value("UUID").toString();
        geom->m_name = query.value("name").toString().trimmed();


        geom->m_method = query.value("method").toString().trimmed();
        geom->m_basis = query.value("basis").toString().trimmed();
        geom->m_relativeCoordinates = query.value("fractional_coordinates").toBool();

        geom->m_comment = query.value("comment").toString();
        geom->m_citation = query.value("citation").toString();

        geom->parseDBKPoints(query.value("kpoints"));

        geom->setUPE(query.value("upe").toDouble());


//        geom->parseDBKPointshifts(query.value("kpoint_shifts"));
        geom->parseDBLattiveVectors(query.value("lattice_vectors"));
        geom->parseDBCoordinates(query.value("coordinates"));
//        geom->setFormula(query.value("formula").toString());
        geom->computeFormula();


        geom->m_scaling_factor = query.value("scaling_factor").toDouble();

        geom->setPBC(true);

        return true;
    }
    return false;
}

bool DFTBRefDatabase::addGeometry(Geometry *data)
{
    if (data->m_PBC)
    {
        return addCrystalGeometry(data);
    }
    else
    {
        return addMolecularGeometry(data);
    }

}

bool DFTBRefDatabase::addBandStructure(BandstructureData *data)
{
    QSqlQuery query(database());

    bindSQLBandStructure(query, data, false);

    query.exec();
    if (query.lastError().isValid())
    {
        m_lastError = query.lastError().text();
        m_lastErrorNumber = query.lastError().number();
        qDebug() << query.lastError().number() << query.lastError().text();
        return false;
    }
    if (m_signalEnabled)
        emit bandstructureUpdated();
    return true;
}
bool DFTBRefDatabase::addMolecularFrequency(MolecularFrequencyData *data)
{
    QSqlQuery query(database());
    bindSQLMolecularFrequency(query, data, false);

    query.exec();
    if (query.lastError().isValid())
    {
        m_lastError = query.lastError().text();
        m_lastErrorNumber = query.lastError().number();
        qDebug() << query.lastError().number() << query.lastError().text();
        return false;
    }
    if (m_signalEnabled)
        emit molecularFrequencyUpdated();
    return true;
}
bool DFTBRefDatabase::addMolecularAtomizationEnergy(MolecularAtomizationEnergyData *data)
{
    QSqlQuery query(database());

    bindSQLMolecularAtomizationEnergy(query, data, false);

    query.exec();
    if (query.lastError().isValid())
    {
        m_lastError = query.lastError().text();
        m_lastErrorNumber = query.lastError().number();
        qDebug() << query.lastError().number() << query.lastError().text();
        return false;
    }
    if (m_signalEnabled)
        emit molecularAtomizationEnergyUpdated();
    return true;
}
bool DFTBRefDatabase::addReactionEnergy(ReactionEnergyData *data)
{
    QSqlDatabase db = database();
    transaction();

    QSqlQuery query(db);
    query.prepare("INSERT INTO reactionenergy (UUID, name, method, basis, energy, energy_unit, elements, need_opt, citation, comment) "
                  "VALUES (:UUID, :name, :method, :basis, :energy, :energy_unit, :elements, :need_opt, :citation, :comment);");
    query.bindValue(":UUID", data->UUID());
    query.bindValue(":name", data->name().trimmed());
    query.bindValue(":method", data->method().trimmed());
    query.bindValue(":basis", data->basis().trimmed());
    query.bindValue(":energy", data->energy());
    query.bindValue(":energy_unit", data->unit());

    query.bindValue(":citation", data->citation());
    query.bindValue(":comment", data->comment());
    query.bindValue(":need_opt", data->getOptimizeComponents());

    QStringList elems;
    for(int i=0; i<data->reactants().size();++i)
    {
        Geometry* geom = new Geometry();
        auto pair = data->reactants().at(i);
        getGeometry(pair.uuid, geom);
        elems.append(geom->getDBElementString().split(",", QString::SkipEmptyParts));
        delete geom;
    }
    for(int i=0; i<data->products().size();++i)
    {
        Geometry* geom = new Geometry();
        auto pair = data->products().at(i);
        getGeometry(pair.uuid, geom);
        elems.append(geom->getDBElementString().split(",", QString::SkipEmptyParts));
        delete geom;
    }
    elems.removeDuplicates();
    elems.sort();

    query.bindValue(":elements", elems.join(","));

    //reaction components
    QSqlQuery query_comp(db);
    query_comp.prepare("INSERT INTO reaction_components VALUES (?, ?, ?, ?);");
    QVariantList rids;
    QVariantList isreacts;
    QVariantList coeffs;
    QVariantList geom_ids;

    for(int i=0; i<data->reactants().size();++i)
    {
        rids << data->UUID();
        isreacts << true;
        coeffs << data->reactants().at(i).coeff;
        geom_ids << data->reactants().at(i).uuid;
    }
    for(int i=0; i<data->products().size();++i)
    {
        rids << data->UUID();
        isreacts << false;
        coeffs << data->products().at(i).coeff;
        geom_ids << data->products().at(i).uuid;
    }

    query_comp.addBindValue(rids);
    query_comp.addBindValue(isreacts);
    query_comp.addBindValue(coeffs);
    query_comp.addBindValue(geom_ids);




    if (query.exec() && query_comp.execBatch())
    {
        if (!commit())
        {
            m_lastError = query.lastError().text();
            m_lastErrorNumber = query.lastError().number();
            qDebug() << query.lastError().number() << query.lastError().text();
            return false;
        }
    }
    else
    {
        rollback();
        m_lastError = query.lastError().text();
        m_lastErrorNumber = query.lastError().number();
        qDebug() << query.lastError().number() << query.lastError().text();
        return false;
    }
    if (m_signalEnabled)
        emit reactionEnergyUpdated();
    return true;
}




bool DFTBRefDatabase::updateGeometry(Geometry *data, const QString& oldUUID)
{
     transaction();
     QSqlQuery query(database());
     query.prepare("update geometry_uuid set UUID=:UUID where UUID=:old_UUID;");

     query.bindValue(":UUID", data->m_UUID);
     query.bindValue(":old_UUID", oldUUID);

     bool succ = query.exec();
     if (!succ)
     {
         rollback();
         if (query.lastError().isValid())
         {
             m_lastError = query.lastError().text();
             m_lastErrorNumber = query.lastError().number();
             qDebug() << query.lastError().number() << query.lastError().text();
             return false;
         }
     }

    if (data->PBC())
    {
        succ = updateCrystalGeometry(data);
    }
    else
    {
        succ = updateMolecularGeometry(data);
    }
    if (!succ)
    {
        rollback();
        if (query.lastError().isValid())
        {
            m_lastError = query.lastError().text();
            m_lastErrorNumber = query.lastError().number();
            qDebug() << query.lastError().number() << query.lastError().text();
            return false;
        }
    }
    commit();
    return true;
}

bool DFTBRefDatabase::updateMolecularGeometry(Geometry *data)
{
    QSqlQuery query(database());

    bindSQLMolecularGeometry(query, data, true);


    bool succ = query.exec();

    if (!succ)
    {
        m_lastError = query.lastError().text();
        m_lastErrorNumber = query.lastError().number();
        qDebug() << query.lastError().number() << query.lastError().text();
        return false;
    }
    if (m_signalEnabled)
        emit geometryUpdated();
    return true;
}

bool DFTBRefDatabase::updateCrystalGeometry(Geometry *data)
{
    QSqlQuery query(database());

    bindSQLCrystalGeometry(query, data, true);


    bool succ = query.exec();
    if (!succ)
    {
        qDebug() << query.executedQuery();
        m_lastError = query.lastError().text();
        m_lastErrorNumber = query.lastError().number();
        qDebug() << query.lastError().number() << query.lastError().text();
        return false;
    }
    if (m_signalEnabled)
        emit geometryUpdated();
    return true;
}
bool DFTBRefDatabase::updateBandStructure(BandstructureData *data, const QString& old_UUID)
{
    QSqlQuery query(database());

    bindSQLBandStructure(query, data, true, old_UUID);

    bool succ = query.exec();
    if (!succ)
    {
        m_lastError = query.lastError().text();
        m_lastErrorNumber = query.lastError().number();
        qDebug() << query.lastError().number() << query.lastError().text();
        return false;
    }
    if (m_signalEnabled)
        emit bandstructureUpdated();
    return true;
}
bool DFTBRefDatabase::updateMolecularFrequency(MolecularFrequencyData* data)
{
    QSqlQuery query(database());

    bindSQLMolecularFrequency(query, data, true);

    bool succ = query.exec();
    if (!succ)
    {
        m_lastError = query.lastError().text();
        m_lastErrorNumber = query.lastError().number();
        qDebug() << query.lastError().number() << query.lastError().text();
        return false;
    }
    if (m_signalEnabled)
        emit molecularFrequencyUpdated();
    return true;
}
bool DFTBRefDatabase::updateMolecularAtomizationEnergy(MolecularAtomizationEnergyData* data)
{
    QSqlQuery query(database());

    bindSQLMolecularAtomizationEnergy(query, data, true);

    bool succ = query.exec();
    if (!succ)
    {
        m_lastError = query.lastError().text();
        m_lastErrorNumber = query.lastError().number();
        qDebug() << query.lastError().number() << query.lastError().text();
        return false;
    }
    if (m_signalEnabled)
        emit molecularAtomizationEnergyUpdated();
    return true;
}



bool DFTBRefDatabase::updateReactionEnergy(ReactionEnergyData *data)
{
    QSqlDatabase db = database();
    transaction();
    QSqlQuery query(db);

    query.prepare("UPDATE reactionenergy SET name=:name, method=:method, basis=:basis, energy=:energy, energy_unit=:energy_unit, elements=:elements, need_opt=:need_opt, citation=:citation, comment=:comment WHERE UUID=:UUID");
    query.bindValue(":UUID", data->UUID());
    query.bindValue(":name", data->name().trimmed());
    query.bindValue(":method", data->method().trimmed());
    query.bindValue(":basis", data->basis().trimmed());
    query.bindValue(":energy", data->energy());
    query.bindValue(":energy_unit", data->unit());
    query.bindValue(":citation", data->citation());
    query.bindValue(":comment", data->comment());
    query.bindValue(":need_opt", data->getOptimizeComponents());



    QStringList elems;
    for(int i=0; i<data->reactants().size();++i)
    {
        Geometry* geom = new Geometry();

        auto pair = data->reactants().at(i);
        getGeometry(pair.uuid, geom);
        elems.append(geom->getDBElementString().split(",", QString::SkipEmptyParts));
        delete geom;
    }
    for(int i=0; i<data->products().size();++i)
    {
        Geometry* geom = new Geometry();

        auto pair = data->products().at(i);
        getGeometry(pair.uuid, geom);
        elems.append(geom->getDBElementString().split(",", QString::SkipEmptyParts));
        delete geom;
    }
    elems.removeDuplicates();
    elems.sort();

    query.bindValue(":elements", elems.join(","));


    //reaction components

    QSqlQuery query_del(db);
    query_del.prepare("delete from reaction_components where reaction_id=" + QString("'%1'").arg(data->UUID()) + ";");


    QSqlQuery query_comp(db);
    query_comp.prepare("INSERT INTO reaction_components VALUES (?, ?, ?, ?);");

    QVariantList rids;
    QVariantList isreacts;
    QVariantList coeffs;
    QVariantList geom_ids;

    for(int i=0; i<data->reactants().size();++i)
    {
        rids << data->UUID();
        isreacts << true;
        coeffs << data->reactants().at(i).coeff;
        geom_ids << data->reactants().at(i).uuid;
    }
    for(int i=0; i<data->products().size();++i)
    {
        rids << data->UUID();
        isreacts << false;
        coeffs << data->products().at(i).coeff;
        geom_ids << data->products().at(i).uuid;
    }

    query_comp.addBindValue(rids);
    query_comp.addBindValue(isreacts);
    query_comp.addBindValue(coeffs);
    query_comp.addBindValue(geom_ids);



    bool succ = query.exec();
    succ = succ && query_del.exec();
    succ = succ && query_comp.execBatch();
    if (succ)
    {
        commit();
    }
    else
    {
        rollback();
        m_lastError = query.lastError().text();
        m_lastErrorNumber = query.lastError().number();
        qDebug() << query.lastError().number() << query.lastError().text();
        return false;
    }
    if (m_signalEnabled)
        emit reactionEnergyUpdated();
    return true;
}


bool DFTBRefDatabase::removeGeometries(QStringList uuids)
{

    QStringList final_uuids;
    for(int i=0 ;i<uuids.size();++i)
    {
        final_uuids.append("'" + uuids[i] + "'");
    }

    QSqlDatabase db = database();
    transaction();
    QSqlQuery query(db);

    //! TODO: this assumes that the relation is set.
    //! Should be rely on that?
    query.prepare(QString("DELETE FROM geometry_uuid WHERE UUID IN (%1)").arg(final_uuids.join(", ")));

    bool succ = query.exec();
    if (!succ)
    {
        rollback();
        m_lastError = query.lastError().text();
        m_lastErrorNumber = query.lastError().number();
        qDebug() << query.lastError().number() << query.lastError().text();
        return false;
    }




    commit();
    emit geometryUpdated();
    return true;
}
bool DFTBRefDatabase::removeBandStructure(const QString &uuid)
{
    QSqlDatabase db = database();
    QSqlQuery query(db);
    query.prepare("DELETE FROM bandstructure WHERE UUID=:UUID");
    query.bindValue(":UUID", uuid);
    bool succ = query.exec();
    if (!succ)
    {
        m_lastError = query.lastError().text();
        m_lastErrorNumber = query.lastError().number();
        qDebug() << query.lastError().number() << query.lastError().text();
        return false;
    }
    if (m_signalEnabled)
        emit bandstructureUpdated();
    return true;
}

bool DFTBRefDatabase::removeBandStructure(const QStringList &uuids)
{
    QSqlDatabase db = database();
    QStringList final_uuids;
    for(int i=0 ;i<uuids.size();++i)
    {
        final_uuids.append("'" + uuids[i] + "'");
    }
    transaction();
    QSqlQuery query(db);
    query.prepare(QString("DELETE FROM bandstructure WHERE UUID IN (%1)").arg(final_uuids.join(", ")));
    bool succ = query.exec();
    if (!succ)
    {
        rollback();
        m_lastError = query.lastError().text();
        m_lastErrorNumber = query.lastError().number();
        qDebug() << query.lastError().number() << query.lastError().text();
        return false;
    }
    commit();
    if (m_signalEnabled)
        emit bandstructureUpdated();

    return true;
}

bool DFTBRefDatabase::getMolecularFrequency(const QString &uuid, MolecularFrequencyData *data)
{
    QSqlQuery query(database());
    query.prepare("SELECT * FROM molecularfrequency WHERE UUID = :UUID;");
    query.bindValue(":UUID", uuid);
    bool succ = query.exec();

    if (!succ)
    {
        m_lastError = query.lastError().text();
        m_lastErrorNumber = query.lastError().number();
        qDebug() << query.lastError().number() << query.lastError().text();
        return false;
    }

    if (query.next())
    {
        data->m_UUID = query.value("UUID").toString();
        data->m_method = query.value("method").toString().trimmed();
        data->m_basis = query.value("basis").toString().trimmed();
        data->m_comment = query.value("comment").toString();
        data->m_citation = query.value("citation").toString();

        data->m_geom_uuid = query.value("geom_id").toString();

        try
        {
            {
                libvariant::Variant var = libvariant::DeserializeJSON(query.value("frequencies").toString().toStdString());
                QList<double> list;
                libvariant::VariantConvertor<QList<double> >::fromVariant(var, list);
                data->setFreq(list);
            }

            {
                libvariant::Variant var = libvariant::DeserializeJSON(query.value("weight").toString().toStdString());
                QList<double> list;
                libvariant::VariantConvertor<QList<double> >::fromVariant(var, list);
                data->setWeight(list);
            }
            return true;
        }
        catch(...)
        {
            return false;
        }


        return true;
    }
    return false;
}
bool DFTBRefDatabase::removeMolecularFrequency(const QString &uuid)
{
    QSqlQuery query(database());
    query.prepare("DELETE FROM molecularfrequency WHERE UUID=:UUID");
    query.bindValue(":UUID", uuid);
    bool succ = query.exec();
    if (!succ)
    {
        m_lastError = query.lastError().text();
        m_lastErrorNumber = query.lastError().number();
        qDebug() << query.lastError().number() << query.lastError().text();
        return false;
    }
    if (m_signalEnabled)
        emit molecularFrequencyUpdated();
    return true;
}

bool DFTBRefDatabase::removeMolecularFrequency(const QStringList &uuids)
{
    QSqlDatabase db = database();
    QStringList final_uuids;
    for(int i=0 ;i<uuids.size();++i)
    {
        final_uuids.append("'" + uuids[i] + "'");
    }

    transaction();
    QSqlQuery query(db);
    query.prepare(QString("DELETE FROM molecularfrequency WHERE UUID IN (%1)").arg(final_uuids.join(", ")));
    bool succ = query.exec();
    if (!succ)
    {
        rollback();
        m_lastError = query.lastError().text();
        m_lastErrorNumber = query.lastError().number();
        qDebug() << query.lastError().number() << query.lastError().text();
        return false;
    }
    commit();
    if (m_signalEnabled)
        emit molecularFrequencyUpdated();

    return true;
}

bool DFTBRefDatabase::getMolecularAtomizationEnergy(const QString &uuid, MolecularAtomizationEnergyData *data)
{
    QSqlQuery query(database());
    query.prepare("SELECT * FROM molecularatomizationenergy WHERE UUID = :UUID;");
    query.bindValue(":UUID", uuid);
    bool succ = query.exec();

    if (!succ)
    {
        m_lastError = query.lastError().text();
        m_lastErrorNumber = query.lastError().number();
        qDebug() << query.lastError().number() << query.lastError().text();
        return false;
    }

    if (query.next())
    {
        data->m_UUID = query.value("UUID").toString();
        data->m_method = query.value("method").toString().trimmed();
        data->m_basis = query.value("basis").toString().trimmed();
        data->m_comment = query.value("comment").toString();
        data->m_citation = query.value("citation").toString();
        data->m_geom_uuid = query.value("geom_id").toString().trimmed();;
        data->m_energy_unit = query.value("energy_unit").toString().trimmed();
        data->m_energy = query.value("energy").toDouble();
        return true;
    }
    return false;
}

bool DFTBRefDatabase::removeMolecularAtomizationEnergy(const QString &uuid)
{
    QSqlQuery query(database());
    query.prepare("DELETE FROM molecularatomizationenergy WHERE UUID=:UUID");
    query.bindValue(":UUID", uuid);
    bool succ = query.exec();
    if (!succ)
    {
        m_lastError = query.lastError().text();
        m_lastErrorNumber = query.lastError().number();
        qDebug() << query.lastError().number() << query.lastError().text();
        return false;
    }
    if (m_signalEnabled)
        emit molecularAtomizationEnergyUpdated();
    return true;
}

bool DFTBRefDatabase::removeMolecularAtomizationEnergy(const QStringList &uuids)
{
    QSqlDatabase db = database();
    QStringList final_uuids;
    for(int i=0 ;i<uuids.size();++i)
    {
        final_uuids.append("'" + uuids[i] + "'");
    }
    transaction();
    QSqlQuery query(db);
    query.prepare(QString("DELETE FROM molecularatomizationenergy WHERE UUID IN (%1)").arg(final_uuids.join(", ")));
    bool succ = query.exec();
    if (!succ)
    {
        rollback();
        m_lastError = query.lastError().text();
        m_lastErrorNumber = query.lastError().number();
        qDebug() << query.lastError().number() << query.lastError().text();
        return false;
    }
    commit();
    if (m_signalEnabled)
        emit molecularAtomizationEnergyUpdated();

    return true;
}

bool DFTBRefDatabase::removeReactionEnergy(const QString &uuid)
{
    QSqlQuery query(database());
    query.prepare("DELETE FROM reactionenergy WHERE UUID=:UUID");
    query.bindValue(":UUID", uuid);
    bool succ = query.exec();
    if (!succ)
    {
        m_lastError = query.lastError().text();
        m_lastErrorNumber = query.lastError().number();
        qDebug() << query.lastError().number() << query.lastError().text();
        return false;
    }
    if (m_signalEnabled)
        emit reactionEnergyUpdated();
    return true;
}
bool DFTBRefDatabase::removeReactionEnergy(const QStringList &uuids)
{
    QSqlDatabase db = database();
    QStringList final_uuids;
    for(int i=0 ;i<uuids.size();++i)
    {
        final_uuids.append("'" + uuids[i] + "'");
    }

    transaction();
    QSqlQuery query(db);
    query.prepare(QString("DELETE FROM reactionenergy WHERE UUID IN (%1)").arg(final_uuids.join(", ")));
    bool succ = query.exec();
    if (!succ)
    {
        rollback();
        m_lastError = query.lastError().text();
        m_lastErrorNumber = query.lastError().number();
        qDebug() << query.lastError().number() << query.lastError().text();
        return false;
    }
    commit();
    if (m_signalEnabled)
        emit reactionEnergyUpdated();
    return true;
}


void DFTBRefDatabase::bindSQLCrystalGeometry(QSqlQuery &query, Geometry *data, bool update)
{

    QStringList fields;
    fields << "name" << "formula" << "method" << "basis"
           << "elements" <<  "comment" << "citation" << "coordinates" << "kpoints" << "upe"
           << "fractional_coordinates" << "lattice_vectors" << "scaling_factor";

    QString table_name = "crystal_geometry";
    QString sqlmain = field2sqlquery(fields, table_name, update);
    query.prepare(sqlmain);

    query.bindValue(":UUID", data->m_UUID);
    query.bindValue(":name", data->m_name.trimmed());
    query.bindValue(":formula", data->m_formula);
    query.bindValue(":method", data->m_method.trimmed());
    query.bindValue(":basis", data->m_basis.trimmed());
    query.bindValue(":upe", data->m_upe);

    query.bindValue(":elements", data->getDBElementString());

    query.bindValue(":comment", data->m_comment);
    query.bindValue(":citation", data->m_citation);

    query.bindValue(":kpoints", data->getDBKpointsString());
    query.bindValue(":fractional_coordinates", data->relativeCoordinates());
    query.bindValue(":scaling_factor", data->m_scaling_factor);

    query.bindValue(":lattice_vectors", data->getDBLatticeVectors());
    query.bindValue(":coordinates", data->getDBCoordinatesString());
}

QString DFTBRefDatabase::field2sqlquery(QStringList fields, QString table_name, bool update, const QString& old_UUID)
{
    QString sqlmain;
    if (update)
    {
        sqlmain = "update " + table_name + " set ";
        QStringList list;
        for (auto field : fields)
        {
            list.append("`" + field + "`=:" + field);
        }
        if (!old_UUID.isEmpty())
        {
            list.append("UUID=:UUID");
            sqlmain += list.join(", ");
            sqlmain += " where UUID=:old_UUID;";
        }
        else
        {
            sqlmain += list.join(", ");
            sqlmain += " where UUID=:UUID;";
        }

    }
    else
    {
        sqlmain = "INSERT INTO " + table_name + " (`UUID`, `" + fields.join("`, `") + "`) VALUES(:UUID, :" + fields.join(", :") + ");";
    }

    return sqlmain;
}

void DFTBRefDatabase::bindSQLMolecularGeometry(QSqlQuery &query, Geometry *data, bool update)
{
    QStringList fields;
    fields << "name" << "charge" << "spin" << "formula" << "method" << "basis"
           << "elements" <<  "comment" << "citation" << "coordinates" << "is_stationary"
           << "force";

    QString table_name = "molecular_geometry";

    QString sqlmain = field2sqlquery(fields, table_name, update);
    query.prepare(sqlmain);
    query.bindValue(":UUID", data->m_UUID);
    query.bindValue(":name", data->m_name.trimmed());
    query.bindValue(":charge", data->m_charge);
    query.bindValue(":spin", data->m_spin);
    query.bindValue(":is_stationary", data->m_isStationary);
    libvariant::Variant var;
    var = libvariant::VariantConvertor<QList<dVector3> >::toVariant(data->m_force);
    QString force = QString::fromStdString(libvariant::SerializeJSON(var));
    query.bindValue(":force", force);
    query.bindValue(":formula", data->m_formula);
    query.bindValue(":method", data->m_method.trimmed());
    query.bindValue(":basis", data->m_basis.trimmed());

    query.bindValue(":elements", data->getDBElementString());
    query.bindValue(":coordinates", data->getDBCoordinatesString());

    query.bindValue(":comment", data->m_comment);
    query.bindValue(":citation", data->m_citation);
}

void DFTBRefDatabase::bindSQLMolecularAtomizationEnergy(QSqlQuery &query, MolecularAtomizationEnergyData *data, bool update)
{
    QStringList fields;
    fields << "geom_id" << "method" << "basis" << "energy"
           << "energy_unit" <<  "comment" << "citation";

    QString table_name = "molecularatomizationenergy";
    QString sqlmain = field2sqlquery(fields, table_name, update);
    query.prepare(sqlmain);

    query.bindValue(":UUID", data->UUID());
    query.bindValue(":geom_id", data->geom_uuid());
    query.bindValue(":method", data->method());
    query.bindValue(":basis", data->basis());
    query.bindValue(":energy", data->energy());
    query.bindValue(":energy_unit", data->unit());

    query.bindValue(":comment", data->comment());
    query.bindValue(":citation", data->citation());
}

void DFTBRefDatabase::bindSQLMolecularFrequency(QSqlQuery &query, MolecularFrequencyData *data, bool update)
{

    QStringList fields;
    fields << "geom_id" << "method" << "basis" << "weight"
           << "frequencies" <<  "comment" << "citation";

    QString table_name = "molecularfrequency";
    QString sqlmain = field2sqlquery(fields, table_name, update);
    query.prepare(sqlmain);


    query.bindValue(":UUID", data->UUID());
    query.bindValue(":geom_id", data->geom_uuid());
    query.bindValue(":method", data->method().trimmed());
    query.bindValue(":basis", data->basis().trimmed());

    query.bindValue(":comment", data->comment());
    query.bindValue(":citation", data->citation());

    query.bindValue(":weight", QString::fromStdString(
                        libvariant::SerializeJSON(libvariant::VariantConvertor<QList<double> >::toVariant(data->weight())))
                    );
    query.bindValue(":frequencies", QString::fromStdString(
                        libvariant::SerializeJSON(libvariant::VariantConvertor<QList<double> >::toVariant(data->freq())))
                    );
}

void DFTBRefDatabase::bindSQLBandStructure(QSqlQuery &query, BandstructureData *data, bool update, const QString& old_UUID)
{

    QStringList fields;
    fields << "name" << "geom_id" << "method" << "basis" << "fermi_index"
           << "bands" <<  "kpoints" << "default_optimization_target" << "comment" << "citation" ;

    QString table_name = "bandstructure";
    QString sqlmain = field2sqlquery(fields, table_name, update, old_UUID);

    query.prepare(sqlmain);

    if (!old_UUID.isEmpty())
    {
        query.bindValue(":old_UUID", old_UUID);
    }
    query.bindValue(":UUID", data->m_UUID);
    query.bindValue(":name", data->m_name.trimmed());
    query.bindValue(":geom_id", data->m_geom_uuid);
    query.bindValue(":method", data->m_method.trimmed());
    query.bindValue(":basis", data->m_basis.trimmed());

    query.bindValue(":bands", data->getJson_bands());

    query.bindValue(":comment", data->comment());
    query.bindValue(":citation", data->citation());

    libvariant::Variant var_fermi_index = libvariant::VariantConvertor<QPair<int, int>>::toVariant(data->m_fermiIndex);
    QString fermi_index_str = QString::fromStdString(libvariant::SerializeJSON(var_fermi_index,false));
    query.bindValue(":fermi_index", fermi_index_str);

    libvariant::Variant var_kpoints = libvariant::VariantConvertor<ADPT::KPointsSetting>::toVariant(data->m_kpoints_setting);
    QString kpoints_str = QString::fromStdString(libvariant::SerializeJSON(var_kpoints,false));
    query.bindValue(":kpoints", kpoints_str);

    libvariant::Variant var_dop = libvariant::VariantConvertor<QList<BSOptTargetInfo>>::toVariant(data->m_default_optimization);
    QString dop_str = QString::fromStdString(libvariant::SerializeJSON(var_dop,false));
    query.bindValue(":default_optimization_target", dop_str);
}




int DFTBRefDatabase::checkDatabase()
{
    QSqlDatabase db = database();
    QStringList tables = db.tables();

    if (!tables.contains("info"))
    {
        m_lastError = "No info table found";
        return DFTBRefDatabaseError::DatabaseSchemaError;
    }
    QSqlRecord record = db.record("info");
    if(!record.contains("application") || !record.contains("version"))
    {
        m_lastError = "Wrong info table definitions";
        return DFTBRefDatabaseError::DatabaseSchemaError;
    }
    QSqlQuery info = db.exec("select * from info");
    if (!info.next())
    {
        m_lastError = "Wrong info table definitions";
        return DFTBRefDatabaseError::DatabaseSchemaError;
    }

    bool ok;
    int remote_version = info.value("version").toInt(&ok);
    if (!ok || (remote_version != DB_VERSION))
    {
        m_lastError = "Wrong database version, connected version: " + info.value("version").toString() + "\nRequired version: "
                + QString::number(DB_VERSION);
        return DFTBRefDatabaseError::DatabaseVersionError;
    }
    return DFTBRefDatabaseError::NoError ;
}

int DFTBRefDatabase::checkPermission()
{
    QSqlDatabase db = database();
    QSqlQuery query = db.exec("SHOW GRANTS for " + db.userName());

    if (query.lastError().isValid())
    {
        qDebug() << query.lastError().text();
        return DFTBRefDatabaseError::UnknownError;
    }

    bool hasSelect = false;
    bool hasInsert = false;
    bool hasUpdate = false;
    bool hasDelete = false;
    bool hasCreate = false;
    bool hasDrop = false;
    bool hasAlter = false;

    while (query.next())
    {
        QStringList arr = query.value(0).toString().split(QRegularExpression("[, ]"), QString::SkipEmptyParts);
        for(int i=0; i<arr.size();++i)
        {
            if (arr.at(i) == "SELECT")
            {
                hasSelect = true;
            }
            else if (arr.at(i) == "ALTER")
            {
                hasAlter = true;
            }
            else if (arr.at(i) == "INSERT")
            {
                hasInsert = true;
            }
            else if (arr.at(i) == "UPDATE")
            {
                hasUpdate = true;
            }
            else if (arr.at(i) == "DELETE")
            {
                hasDelete = true;
            }
            else if (arr.at(i) == "CREATE")
            {
                hasCreate = true;
            }
            else if (arr.at(i) == "DROP")
            {
                hasDrop = true;
                break;
            }
            else if (arr.at(i) == "ON")
            {
                break;
            }
            else if (arr.at(i) == "ALL" && arr.at(i+1) == "PRIVILEGES")
            {
                hasSelect = hasInsert = hasUpdate = hasDelete = hasCreate = hasDrop = true;
                break;
            }
        }
    }
    if (hasSelect)
    {
        setCanRead(true);
    }
    if (hasInsert && hasUpdate && hasDelete)
    {
        setCanWrite(true);
    }
    if (hasCreate && hasDrop && hasAlter)
    {
        setCanCreate(true);
    }
    return DFTBRefDatabaseError::NoError;
}
bool DFTBRefDatabase::canCreate() const
{
    return m_canCreate;
}
int DFTBRefDatabase::lastErrorNumber() const
{
    return m_lastErrorNumber;
}

void DFTBRefDatabase::transaction()
{
    mutex.lock();
    if (this->m_transactionEnabled && !this->m_inTransaction)
    {
        if (database().transaction())
        {
            this->m_inTransaction = true;
        }
    }
    mutex.unlock();
}

bool DFTBRefDatabase::commit()
{
    mutex.lock();
    bool res = true;
    if (this->m_transactionEnabled && this->m_inTransaction)
    {
        res = database().commit();
    }
    this->m_inTransaction = false;
    mutex.unlock();
    return res;
}

void DFTBRefDatabase::rollback()
{
    mutex.lock();
    if (this->m_transactionEnabled && this->m_inTransaction)
    {
        database().rollback();
    }
    this->m_inTransaction = false;
    mutex.unlock();
}

void DFTBRefDatabase::disableTransaction()
{
    mutex.lock();
    m_transactionEnabled = false;
    m_inTransaction = false;
    mutex.unlock();
}

void DFTBRefDatabase::enableTransaction()
{
    mutex.lock();
    m_transactionEnabled = true;
    m_inTransaction = false;
    mutex.unlock();
}


void DFTBRefDatabase::setCanCreate(bool canCreate)
{
    m_canCreate = canCreate;
}

bool DFTBRefDatabase::canWrite() const
{
    return m_canWrite;
}

void DFTBRefDatabase::setCanWrite(bool canWrite)
{
    m_canWrite = canWrite;
}

bool DFTBRefDatabase::canRead() const
{
    return m_canRead;
}

void DFTBRefDatabase::setCanRead(bool canRead)
{
    m_canRead = canRead;
}

