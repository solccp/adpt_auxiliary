#include "molecularfrequencydata.h"
#include <QUuid>
#include <QTextStream>

MolecularFrequencyData::MolecularFrequencyData(QObject *parent) :
    QObject(parent)
{
    this->m_UUID = QUuid::createUuid().toString();
}
QString MolecularFrequencyData::UUID() const
{
    return m_UUID;
}

void MolecularFrequencyData::resetUUID()
{
    this->m_UUID = createUUID();
}

QString MolecularFrequencyData::method() const
{
    return m_method;
}

void MolecularFrequencyData::setMethod(const QString &method)
{
    m_method = method;
}
QString MolecularFrequencyData::geom_uuid() const
{
    return m_geom_uuid;
}

void MolecularFrequencyData::setGeom_uuid(const QString &geom_uuid)
{
    m_geom_uuid = geom_uuid;
}
QString MolecularFrequencyData::basis() const
{
    return m_basis;
}

void MolecularFrequencyData::setBasis(const QString &basis)
{
    m_basis = basis;
}
QList<double> MolecularFrequencyData::freq() const
{
    return m_freq;
}

void MolecularFrequencyData::setFreq(const QList<double> &freq)
{
    m_freq = freq;
}

double &MolecularFrequencyData::freq(int index)
{
    return m_freq[index];
}

double &MolecularFrequencyData::weight(int index)
{
    return m_weight[index];
}

bool MolecularFrequencyData::checkData(QString &errorMsg)
{

    return true;
}

QString MolecularFrequencyData::createUUID()
{
    QByteArray array;
    QTextStream stream(&array);
    stream << this->m_geom_uuid << endl;
    stream << this->m_method << endl;
    stream << this->m_basis << endl;
    for(auto item: this->m_freq)
    {
        stream << QString::number(item, 'f', 6) << endl;
    }
    for(auto item: this->m_weight)
    {
        stream << QString::number(item, 'f', 6) << endl;
    }
    return QUuid::createUuidV5(QUuid(), array).toString();
}
QList<double> MolecularFrequencyData::weight() const
{
    return m_weight;
}

void MolecularFrequencyData::setWeight(const QList<double> &weight)
{
    m_weight = weight;
}

QString MolecularFrequencyData::comment() const
{
    return m_comment;
}

void MolecularFrequencyData::setComment(const QString &comment)
{
    m_comment = comment;
}

QString MolecularFrequencyData::citation() const
{
    return m_citation;
}

void MolecularFrequencyData::setCitation(const QString &citation)
{
    m_citation = citation;
}


