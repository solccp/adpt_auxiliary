#ifndef GEOMETRY_H
#define GEOMETRY_H


#include "basic_types.h"

#include <QString>
#include <QAbstractTableModel>
#include <QTextStream>
#include <QDataStream>
#include <QDebug>
#include <array>

using iVector3 = std::array<int, 3>;
using dVector3 = std::array<double, 3>;
using iMatrix3x3 = std::array<iVector3, 3>;
using dMatrix3x3 = std::array<dVector3, 3>;


namespace libvariant
{
    template<typename T>
    class VariantConvertor;
}

class Coordinate
{
public:
    QString symbol;
    dVector3 coord;
};



class GeometryCoordinationModel;
class Geometry
{
    friend class CrystalGeometryForm;
    friend class DFTBRefDatabase;
    friend class libvariant::VariantConvertor<Geometry>;
public:
    Geometry();
    ~Geometry();
    QString getMethod() const;
    void setMethod(const QString &value);

    QString getBasis() const;
    void setBasis(const QString &value);

    QString getName() const;
    QString getFormula() const;

    QString getDBElementString() const;
    QString getDBCoordinatesString() const;
    QString getDBKpointsString() const;
    QString getDBKpointshiftsString() const;
    QString getDBLatticeVectors() const;

    bool parseDBKPoints(const QVariant& var);
    bool parseDBKPointshifts(const QVariant& var);
    bool parseDBLattiveVectors(const QVariant& var);
    bool parseDBCoordinates(const QVariant& var);

    void loadDBCoordinatesString(const QString& str);

    bool PBC() const;
    void setPBC(bool PBC);

    QString citation() const;
    void setCitation(const QString &citation);

    QString comment() const;
    void setComment(const QString &comment);

    QString UUID() const;
    void setUUID(const QString& uuid);




    QList<Coordinate> &coordinates();
    const QList<Coordinate> &coordinates() const;

    QList<dVector3> &force();
    const QList<dVector3> &force() const;

    void setForce(const QList<dVector3>& force);

    bool relativeCoordinates() const;
    void setRelativeCoordinates(bool relativeCoordinates);

    bool isStationary() const;

    double scaling_factor() const;
    void setScaling_factor(double scaling_factor);

    dMatrix3x3 lattice_vectors() const;
    void setLattice_vectors(const dMatrix3x3 &lattice_vectors);

    ADPT::KPointsSetting& kpointsSettings();
    const ADPT::KPointsSetting& kpointsSettings() const;
    void setKpointsSettings(const ADPT::KPointsSetting& kpointsSettings);

//public slots:
    void setCharge(const QString& intStr);
    void setCharge(int charge);
    void setSpin(int spin);
    int getSpin() const;
    int getCharge() const;
    void setUPE(const QString& intStr);
    void setUPE(double upe);
    double getUPE() const;

    bool checkCrystalInputs(QString& errorMsg);
    void setName(const QString& name);
    void setIsStationary(bool isStationary);
    void resetUUID();
    QString getXYZ() const;
    QString getGen() const;

    bool writeOut(QTextStream& os, const QString& format);
    void computeFormula() ;

//signals:
//    void formulaChanged(const QString& newFormula);
public:
     QString computeUUID();
private:

private:
    //common
    bool m_PBC = false;
    bool m_relativeCoordinates = false;
    bool m_isStationary = true;

    double m_scaling_factor = 1.0;

    qint32 m_charge = 0;
    qint32 m_spin = 1;
    double m_upe = 0.0;

    QString m_name;
    QString m_method;
    QString m_basis;
    QString m_citation;
    QString m_comment;

    //auto-generated
    QString m_formula;
    QString m_UUID;

private:
    //cluster
    QList<Coordinate> m_coordinates;

private:
    //PBC
    dMatrix3x3 m_lattice_vectors;
public:
    ADPT::KPointsSetting m_kpoints_setting;
    QList<dVector3> m_force;

};


#endif // GEOMETRY_H
