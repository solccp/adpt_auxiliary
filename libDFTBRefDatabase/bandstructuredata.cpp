#include "bandstructuredata.h"
#include <QUuid>
#include "variantqt.h"
#include <Variant/Variant.h>

#include "datastructure_variant.h"

BandstructureData::BandstructureData(QObject *parent) :
    QObject(parent)
{
    this->m_UUID = QUuid::createUuid().toString();
    this->m_kpoints_setting.m_type = ADPT::KPointsSetting::Type::KLines;
}

bool BandstructureData::checkData(QString &errormsg)
{
//! TODO : implement this
    return true;
}

void BandstructureData::resetUUID()
{
    this->m_UUID = createUUID();
}
QString BandstructureData::getName() const
{
    return m_name;
}
QString BandstructureData::getGeomID() const
{
    return m_geom_uuid;
}
QString BandstructureData::method() const
{
    return m_method;
}

void BandstructureData::setMethod(const QString &method)
{
    m_method = method;
}
QString BandstructureData::basis() const
{
    return m_basis;
}

void BandstructureData::setBasis(const QString &basis)
{
    m_basis = basis;
}
QString BandstructureData::citation() const
{
    return m_citation;
}

void BandstructureData::setCitation(const QString &citation)
{
    m_citation = citation;
}
QString BandstructureData::comment() const
{
    return m_comment;
}

void BandstructureData::setComment(const QString &comment)
{
    m_comment = comment;
}

QList<BSOptTargetInfo> BandstructureData::default_optimization() const
{
    return m_default_optimization;
}

void BandstructureData::setDefault_optimization(const QList<BSOptTargetInfo> &default_optimization)
{
    m_default_optimization = default_optimization;
}

QString BandstructureData::UUID() const
{
    return m_UUID;
}

void BandstructureData::setUUID(const QString &uuid)
{
    this->m_UUID = uuid;
}

QVector<QVector<double> > BandstructureData::bands() const
{
    return m_bands;
}

void BandstructureData::setBands(const QVector<QVector<double> > &bands)
{
    m_bands = bands;
}

void BandstructureData::setFermiIndex(int bandIndex, int pointIndex)
{
    m_fermiIndex.first = bandIndex;
    m_fermiIndex.second = pointIndex;
}

QPair<int, int> BandstructureData::getFermiIndex() const
{
    return m_fermiIndex;
}

QString BandstructureData::getJson_bands() const
{
    libvariant::Variant var = libvariant::VariantConvertor<QVector<QVector<double> >>::toVariant(this->m_bands);
    QString var_str = QString::fromStdString(libvariant::SerializeJSON(var, false));
    return var_str;
}


bool BandstructureData::parseJson_bands(const QString &str)
{
//    m_bands.clear();
//    QJsonDocument doc = QJsonDocument::fromJson(str.toLatin1());
//    QJsonArray bands = doc.array();
//    for(int i=0; i<bands.size();++i)
//    {
//        QJsonArray band = bands[i].toArray();
//        QVector<double> v_band;
//        for(int j=0; j<band.size();++j)
//        {
//            v_band.append(band[j].toDouble());
//        }
//        m_bands.append(v_band);
//    }
//    return true;
    auto mvar = libvariant::DeserializeJSON(str.toStdString());
    try
    {
        libvariant::VariantConvertor<QVector<QVector<double>>>::fromVariant(mvar, this->m_bands);
        return true;
    }
    catch(...)
    {
        return false;
    }
}

bool BandstructureData::parseJson_default_optimization(const QString &str)
{
    auto mvar = libvariant::DeserializeJSON(str.toStdString());
    try
    {
        libvariant::VariantConvertor<QList<BSOptTargetInfo>>::fromVariant(mvar, this->m_default_optimization);
        return true;
    }
    catch(...)
    {
        return false;
    }
}

bool BandstructureData::parseJson_kpoints(const QString &str)
{
    auto mvar = libvariant::DeserializeJSON(str.toStdString());
    try
    {
        libvariant::VariantConvertor<ADPT::KPointsSetting>::fromVariant(mvar, this->m_kpoints_setting);
        return true;
    }
    catch(...)
    {
        return false;
    }
}

bool BandstructureData::parseJson_fermiIndex(const QString &str)
{
    auto mvar = libvariant::DeserializeJSON(str.toStdString());
    try
    {
        libvariant::VariantConvertor<QPair<int, int>>::fromVariant(mvar, this->m_fermiIndex);
        return true;
    }
    catch(...)
    {
        return false;
    }
}

QString BandstructureData::createUUID()
{
    QByteArray array;
    QTextStream stream(&array);
    stream << this->m_geom_uuid << endl;
    stream << this->m_name << endl;
    stream << this->m_method << endl;
    stream << this->m_basis << endl;

    stream << "kpoints" << " ";
    if (this->m_kpoints_setting.m_type == ADPT::KPointsSetting::Type::Plain)
    {
        stream << "plain" << endl;
        for(auto const& item : this->m_kpoints_setting.m_kpoints_plain)
        {
            stream << QString::number(item.weight, 'f', 6) << " "
                   << QString::number(item.kpoint[0], 'f', 6) << " "
                   << QString::number(item.kpoint[1], 'f', 6) << " "
                   << QString::number(item.kpoint[2], 'f', 6) << endl;
        }
    }
    else if (this->m_kpoints_setting.m_type == ADPT::KPointsSetting::Type::SupercellFolding)
    {
        stream << "supercell_folding" << endl;
        for(auto i : {0,1,2})
        {
            stream << QString::number(m_kpoints_setting.m_kpoints_supercell.num_of_cells[i][0]) << " "
                   << QString::number(m_kpoints_setting.m_kpoints_supercell.num_of_cells[i][1]) << " "
                   << QString::number(m_kpoints_setting.m_kpoints_supercell.num_of_cells[i][2]) << endl;
        }
        stream << QString::number(m_kpoints_setting.m_kpoints_supercell.shifts[0]) << " "
               << QString::number(m_kpoints_setting.m_kpoints_supercell.shifts[1]) << " "
               << QString::number(m_kpoints_setting.m_kpoints_supercell.shifts[2]) << endl;
    }
    else if (this->m_kpoints_setting.m_type == ADPT::KPointsSetting::Type::KLines)
    {
        stream << "klines" << endl;
        for(auto const & item: this->m_kpoints_setting.m_kpoints_klines)
        {
            stream << QString::number(item.num_of_points) << " "
                   << QString::number(item.kpoint[0], 'f', 6) << " "
                   << QString::number(item.kpoint[1], 'f', 6) << " "
                   << QString::number(item.kpoint[2], 'f', 6) << endl;
        }
    }
    else
    {
        throw std::runtime_error("unknown kpoints setting");
    }

    return QUuid::createUuidV5(QUuid(), array).toString();
}
const ADPT::KPointsSetting& BandstructureData::getKpoints_setting() const
{
    return m_kpoints_setting;
}


ADPT::KPointsSetting& BandstructureData::getKpoints_setting()
{
    return m_kpoints_setting;
}

void BandstructureData::setKpoints_setting(const ADPT::KPointsSetting &kpoints_setting)
{
    m_kpoints_setting = kpoints_setting;
}


void BandstructureData::setGeomID(const QString &geom_uuid)
{
    m_geom_uuid = geom_uuid;
}


void BandstructureData::setName(const QString &name)
{
    m_name = name;
}

