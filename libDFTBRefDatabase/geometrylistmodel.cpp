#include "geometrylistmodel.h"

#include <QSqlQueryModel>

GeometryListModel::GeometryListModel(QSqlQueryModel *model, QObject *parent) : QAbstractTableModel(parent)
{
    m_model = model;
}

int GeometryListModel::rowCount(const QModelIndex &parent) const
{
    return m_model->rowCount(parent);
}

QVariant GeometryListModel::data(const QModelIndex &index, int role) const
{
    //Column mapping:
    //0: UUID
    //1: Name
    //2: Charge
    //3: is_crystal
    //4: Method
    //5: Basis
    //6: Element
    int row = index.row();
    int col = index.column();
    if (col == 0)
    {
        return m_model->data(m_model->index(row,0)).toString();
    }
    else if (col == 1)
    {
        QString finaldata;
        if (!m_model->data(m_model->index(row,5)).toString().isEmpty()) // basis
        {
            if (m_model->data(m_model->index(row,3)).toInt() == 1) //PBC
            {
                QString pat =  QString("%1(%2/%3)");
                finaldata = pat.arg(m_model->data(m_model->index(row,1)).toString())
                        .arg(m_model->data(m_model->index(row,4)).toString())
                        .arg(m_model->data(m_model->index(row,5)).toString());
            }
            else
            {
                QString pat =  QString("%1(%2, %3/%4)");
                finaldata = pat.arg(m_model->data(m_model->index(row,1)).toString())
                        .arg(m_model->data(m_model->index(row,2)).toString())
                        .arg(m_model->data(m_model->index(row,4)).toString())
                        .arg(m_model->data(m_model->index(row,5)).toString());
            }
        }
        else
        {
            if (m_model->data(m_model->index(row,3)).toInt() == 1)
            {
                QString pat =  QString("%1(%2)");
                finaldata = pat.arg(m_model->data(m_model->index(row,1)).toString())
                        .arg(m_model->data(m_model->index(row,4)).toString());
            }
            else
            {
                QString pat =  QString("%1(%2, %3)");
                finaldata = pat.arg(m_model->data(m_model->index(row,1)).toString())
                        .arg(m_model->data(m_model->index(row,2)).toString())
                        .arg(m_model->data(m_model->index(row,4)).toString());
            }
        }

        return finaldata;
    }
    return QVariant();
}

int GeometryListModel::columnCount(const QModelIndex &parent) const
{
    return 2;
}
