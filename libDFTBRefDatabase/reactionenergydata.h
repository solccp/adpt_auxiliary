#ifndef REACTIONENERGYDATA_H
#define REACTIONENERGYDATA_H

#include <QObject>
#include <QPair>
#include <QList>


namespace libvariant
{
    template<typename T>
    class VariantConvertor;
}


class ReactionItem
{
public:
    double coeff = 1.0;
    QString uuid;
};

class ReactionEnergyData : public QObject
{
    Q_OBJECT
    friend class libvariant::VariantConvertor<ReactionEnergyData>;
public:
    explicit ReactionEnergyData(QObject *parent = 0);

    QString UUID() const;
    void setUUID(const QString& uuid);
    void resetUUID();

    QString method() const;
    void setMethod(const QString &method);

    QString basis() const;
    void setBasis(const QString &basis);

    double energy() const;
    void setEnergy(double energy);

    void setEnergyUnit(const QString& unit);

    QString name() const;
    void setName(const QString &name);

    bool checkData(QString& errorMsg);

    QString comment() const;
    void setComment(const QString &comment);

    QString citation() const;
    void setCitation(const QString &citation);

    QList<ReactionItem> &reactants();
    const QList<ReactionItem> &reactants() const;
    void setReactants(const QList<ReactionItem> &reactants);

    QList<ReactionItem> &products();
    const QList<ReactionItem> &products() const;
    void setProducts(const QList<ReactionItem> &products);

    QString unit() const;

    bool getOptimizeComponents() const;
    QString createUUID();
signals:

public slots:

    void setOptimizeComponents(bool value);
    void setUnit(const QString& unit);
private:
    QString m_UUID ;
    QString m_name;

    QString m_method;
    QString m_basis;
    QString m_comment;
    QString m_citation;

    double m_energy = 0.0;
    QString m_energy_unit = "kcal/mol";

    bool m_optimize_components = true;

    QList<ReactionItem> m_reactants;
    QList<ReactionItem> m_products;

};

#endif // REACTIONENERGYDATA_H
