#ifndef MOLECULARFREQUENCYDATA_H
#define MOLECULARFREQUENCYDATA_H

#include <QObject>


namespace libvariant
{
    template<typename T>
    class VariantConvertor;
}


class MolecularFrequencyData : public QObject
{
    Q_OBJECT
    friend class DFTBRefDatabase;
    friend class libvariant::VariantConvertor<MolecularFrequencyData>;
public:
    explicit MolecularFrequencyData(QObject *parent = 0);


    QList<double> weight() const;
    void setWeight(const QList<double> &weight);

public slots:
    QString citation() const;
    void setCitation(const QString &citation);

    QString comment() const;
    void setComment(const QString &comment);
    QString UUID() const;
    void resetUUID();

    QString method() const;
    void setMethod(const QString &method);

    QString geom_uuid() const;
    void setGeom_uuid(const QString &geom_uuid);

    QString basis() const;
    void setBasis(const QString &basis);

    QList<double> freq() const;
    void setFreq(const QList<double> &freq);

    double& freq(int index);
    double& weight(int index);

    bool checkData(QString& errorMsg);

    QString createUUID();

signals:


private:
    QString m_UUID ;
    QString m_geom_uuid;

    QString m_method;
    QString m_basis;

    QString m_comment;
    QString m_citation;

    bool m_isDefault = false;

    QList<double> m_freq;
    QList<double> m_weight;

};

#endif // MOLECULARFREQUENCYDATA_H
