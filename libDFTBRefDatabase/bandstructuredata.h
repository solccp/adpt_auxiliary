#ifndef BANDSTRUCTUREDATA_H
#define BANDSTRUCTUREDATA_H

#include <QObject>

#include <QList>
#include <array>
#include <QVariant>
#include <QVector>

#include "geometry.h"

namespace libvariant
{
    template<typename T>
    class VariantConvertor;
}


class BSOptTargetInfo
{
public:
    int band_startindex;
    int band_endindex;
    int point_startindex;
    int point_endindex;
    double energy_lowerbound;
    double energy_upperbound;
    bool index_based = true;
    double weight = 1.0;
    BSOptTargetInfo() = default;
    BSOptTargetInfo(const BSOptTargetInfo& rhs) = default;

    bool operator ==(const BSOptTargetInfo& rhs)
    {
        if (index_based != rhs.index_based)
        {
            return false;
        }
        if (index_based)
        {
            return (band_startindex == rhs.band_startindex
                && band_endindex == rhs.band_endindex
                    && point_endindex == rhs.point_endindex &&
                    point_startindex == rhs.point_startindex && weight == rhs.weight);
        }
        else
        {
            return (energy_lowerbound == rhs.energy_lowerbound
                && energy_upperbound == rhs.energy_upperbound
                    && weight == rhs.weight);
        }
    }

};

class BandstructureData : public QObject
{
    friend class BandstructureDataEditor;
    friend class BandStructureBandsModel;
    friend class BSOptTargetIndexBasedModel;
    friend class BSOptTargetEnergyBasedModel;
    friend class DFTBRefDatabase;
    friend class libvariant::VariantConvertor<BandstructureData>;
    Q_OBJECT
public:
    BandstructureData(QObject *parent = 0);
    bool checkData(QString& errormsg);
    void resetUUID();
signals:
    
public slots:
    void setName(const QString &getName);
    void setGeomID(const QString &getGeomID);

    QString getName() const;
    QString getGeomID() const;

    QString method() const;
    void setMethod(const QString &method);

    QString basis() const;
    void setBasis(const QString &basis);

    QString citation() const;
    void setCitation(const QString &citation);

    QString comment() const;
    void setComment(const QString &comment);

public:

    QList<BSOptTargetInfo> default_optimization() const;
    void setDefault_optimization(const QList<BSOptTargetInfo> &default_optimization);

    QString UUID() const;
    void setUUID(const QString& uuid);

    QVector<QVector<double> > bands() const;
    void setBands(const QVector<QVector<double> > &bands);

    void setFermiIndex(int bandIndex, int pointIndex);
    QPair<int, int> getFermiIndex() const;

    QString getJson_bands() const;

    bool parseJson_bands(const QString& str);
    bool parseJson_default_optimization(const QString& str);
    bool parseJson_kpoints(const QString& str);
    bool parseJson_fermiIndex(const QString& str);

public:
     QString createUUID();
private:
    QString m_UUID;
    QString m_name;
    QString m_geom_uuid;

    QString m_method;
    QString m_basis;
    QString m_citation;
    QString m_comment;


    QVector<QVector<double> > m_bands;
    QList<BSOptTargetInfo> m_default_optimization;

    QPair<int, int> m_fermiIndex = {-1, -1};

    ADPT::KPointsSetting m_kpoints_setting;
public:
    ADPT::KPointsSetting &getKpoints_setting();
    const ADPT::KPointsSetting &getKpoints_setting() const;
    void setKpoints_setting(const ADPT::KPointsSetting &kpoints_setting);
};

#endif // BANDSTRUCTUREDATA_H

