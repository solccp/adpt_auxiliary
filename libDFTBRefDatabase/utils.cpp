#include "utils.h"



QString orderNumber(int number)
{
    int rem = number % 10;
    switch(rem)
    {
    case 1:
        return QString("%1st").arg(number);
        break;
    case 2:
        return QString("%1nd").arg(number);
        break;
    case 3:
        return QString("%1rd").arg(number);
        break;
    default:
        return QString("%1th").arg(number);
        break;
    }
}
