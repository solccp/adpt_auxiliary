START TRANSACTION;
ALTER TABLE `crystal_geometry` ADD `upe` DOUBLE NULL DEFAULT '0.0' AFTER `kpoints` ;
ALTER VIEW `CrystalGeometryListView` AS select `GEOM`.`UUID` AS `UUID`,`GEOM`.`name` AS `Name`,`GEOM`.`formula` AS `Formula`,`GEOM`.`method` AS `Method`,`GEOM`.`basis` AS `Basis`, `GEOM`.`upe` AS `UPE`,`GEOM`.`elements` AS `elements` from `crystal_geometry` `GEOM` order by `GEOM`.`name`;
UPDATE `info` SET `version` = 6 WHERE `version` = 5;
COMMIT;
