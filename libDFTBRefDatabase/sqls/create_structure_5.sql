START TRANSACTION;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";
SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `molecularatomizationenergy`;
DROP TABLE IF EXISTS `molecularfrequency`;
DROP TABLE IF EXISTS `bandstructure`;
DROP TABLE IF EXISTS `reactionenergy`;
DROP TABLE IF EXISTS `reaction_components`;

DROP TABLE IF EXISTS `geometry_uuid`;
CREATE TABLE IF NOT EXISTS `geometry_uuid` (
  `UUID` varchar(50) NOT NULL,
  `is_crystal` tinyint(1) NOT NULL,
  PRIMARY KEY (`UUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `crystal_geometry`;
CREATE TABLE IF NOT EXISTS `crystal_geometry` (
    `UUID` varchar(50) NOT NULL,
    `name` text NOT NULL,
    `formula` text NOT NULL,
    `method` text NOT NULL,
    `basis` text ,
    `kpoints` longtext NOT NULL,
    `fractional_coordinates` tinyint(1) NOT NULL DEFAULT '1',
    `elements` text NOT NULL,
    `scaling_factor` double NOT NULL,
    `lattice_vectors` text NOT NULL,
    `coordinates` LONGTEXT NOT NULL,
    `comment` LONGTEXT,
    `citation` LONGTEXT,
    PRIMARY KEY (`UUID`),
    CONSTRAINT `crystal_geometry_cs1` FOREIGN KEY (`UUID`) REFERENCES `geometry_uuid`(`UUID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `molecular_geometry`;    
CREATE TABLE IF NOT EXISTS `molecular_geometry` (
  `UUID` varchar(50) NOT NULL,
  `name` text NOT NULL,
  `charge` int(11) NOT NULL,
  `spin` int(11) NOT NULL DEFAULT '1',
  `is_stationary` tinyint(1) NOT NULL,
  `formula` text NOT NULL,
  `method` text NOT NULL,
  `basis` text ,
  `elements` LONGTEXT NOT NULL,
  `coordinates` LONGTEXT NOT NULL,
  `comment` LONGTEXT,
  `citation` LONGTEXT,
  PRIMARY KEY (`UUID`),
  CONSTRAINT `molecular_geometry_cs1` FOREIGN KEY (`UUID`) REFERENCES `geometry_uuid`(`UUID`)
  ON DELETE CASCADE
  ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `molecularatomizationenergy` (
  `UUID` varchar(50) NOT NULL,
  `geom_id` varchar(50) NOT NULL,
  `method` text NOT NULL,
  `basis` text,
  `energy` double NOT NULL,
  `energy_unit` varchar(32) NOT NULL DEFAULT 'kcal/mol',
  `comment` LONGTEXT,
  `citation` LONGTEXT,
  PRIMARY KEY (`UUID`),
  KEY (`geom_id`),
  CONSTRAINT `molecularatomizationenergy_ibfk_1` FOREIGN KEY (`geom_id`) REFERENCES `molecular_geometry`(`UUID`)
  ON DELETE CASCADE
  ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `molecularfrequency` (
  `UUID` varchar(50) NOT NULL,
  `geom_id` varchar(50) NOT NULL,
  `method` text NOT NULL,
  `basis` text,
  `weight` LONGTEXT,
  `frequencies` LONGTEXT NOT NULL,
  `comment` LONGTEXT,
  `citation` LONGTEXT,  
  PRIMARY KEY (`UUID`),
  KEY (`geom_id`),
  CONSTRAINT `molecularfrequency_ibfk_1`
  FOREIGN KEY (`geom_id`) REFERENCES `molecular_geometry`(`UUID`)
  ON DELETE CASCADE
  ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `reactionenergy` (
  `UUID` varchar(50) NOT NULL,
  `name` text NOT NULL,
  `method` text NOT NULL,
  `basis` text,
  `energy` double NOT NULL,
  `energy_unit` varchar(32) NOT NULL DEFAULT 'kcal/mol',
  `elements` LONGTEXT,
  `citation` LONGTEXT,
  `comment` LONGTEXT,
  `need_opt` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`UUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `reaction_components` (
  `reaction_id` varchar(50) NOT NULL,
  `is_reactant` int(1) NOT NULL,
  `coefficient` double NOT NULL,
  `geom_id` varchar(50) NOT NULL,
  UNIQUE KEY `is_reactant` (`is_reactant`,`geom_id`,`reaction_id`), 
  KEY `reaction_id` (`reaction_id`),
  KEY `geom_id` (`geom_id`),
  CONSTRAINT `reaction_components_ibfk_1`
  FOREIGN KEY (`reaction_id`) REFERENCES `reactionenergy` (`UUID`)
  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `reaction_components_ibfk_2`
  FOREIGN KEY (`geom_id`) REFERENCES `geometry_uuid` (`UUID`)
  ON DELETE RESTRICT 
  ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `bandstructure` (
  `UUID` varchar(50) NOT NULL,
  `name` text NOT NULL,
  `geom_id` varchar(50) NOT NULL,
  `method` text NOT NULL,
  `basis` text,
  `bands` LONGTEXT NOT NULL,
  `kpoints` LONGTEXT NOT NULL,
  `default_optimization_target` LONGTEXT,
  `comment` LONGTEXT,
  `citation` LONGTEXT,
  `fermi_index` varchar(32) NOT NULL,
  PRIMARY KEY (`UUID`), 
  KEY (`geom_id`),
  CONSTRAINT `bandstructure_ibfk_1` FOREIGN KEY (`geom_id`) REFERENCES `crystal_geometry` (`UUID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP VIEW IF EXISTS `AE_IDSTR`;
CREATE TABLE IF NOT EXISTS `AE_IDSTR` (
`ae_uuid` varchar(50)
,`energy` double
,`energy_unit` varchar(32) 
,`name` text
,`charge` int(11)
,`method` text
,`basis` text
);

DROP VIEW IF EXISTS `AllGeometryUUID`;
CREATE TABLE IF NOT EXISTS `AllGeometryUUID` (
`UUID` varchar(50)
);

-- --------------------------------------------------------

DROP VIEW IF EXISTS `All_UUID_View`;
CREATE TABLE IF NOT EXISTS `All_UUID_View` (
`UUID` varchar(50)
,`name` text
,`elements` longtext
,`Method` text
,`Basis` text
);

-- --------------------------------------------------------

DROP VIEW IF EXISTS `BandstructureListView`;
CREATE TABLE IF NOT EXISTS `BandstructureListView` (
`UUID` varchar(50)
,`Name` text
,`Reference Geometry` text
,`Method` text
,`Basis` text
,`elements` text
);

-- --------------------------------------------------------

DROP VIEW IF EXISTS `CrystalGeometryListView`;
CREATE TABLE IF NOT EXISTS `CrystalGeometryListView` (
`UUID` varchar(50)
,`Name` text
,`Formula` text
,`Method` text
,`Basis` text
,`elements` text
);

-- --------------------------------------------------------

DROP VIEW IF EXISTS `CrystalGeometry_IDSTR`;
CREATE TABLE IF NOT EXISTS `CrystalGeometry_IDSTR` (
`geom_uuid` varchar(50)
,`name` text
,`method` text
,`basis` text
);

-- --------------------------------------------------------

DROP VIEW IF EXISTS `EnergyEquationList`;
CREATE TABLE IF NOT EXISTS `EnergyEquationList` (
`UUID` varchar(50)
,`Reference Geometry` text
,`Geometry Method` text
,`Geometry Basis` text
,`Method` text
,`Basis` text
,`Energy` double
,`elements` longtext
);

DROP VIEW IF EXISTS `MolecularAtomizationEnergyListView`;
CREATE TABLE IF NOT EXISTS `MolecularAtomizationEnergyListView` (
`UUID` varchar(50)
,`Reference Geometry` text
,`Method` text
,`Basis` text
,`Energy` double
,`elements` longtext
);

-- --------------------------------------------------------
DROP VIEW IF EXISTS `MolecularFrequencyListView`;
CREATE TABLE IF NOT EXISTS `MolecularFrequencyListView` (
`UUID` varchar(50)
,`Reference Geometry` text
,`Method` text
,`Basis` text
,`elements` longtext
);

-- --------------------------------------------------------

DROP VIEW IF EXISTS `MolecularGeometry_IDSTR`;
CREATE TABLE IF NOT EXISTS `MolecularGeometry_IDSTR` (
`geom_uuid` varchar(50)
,`name` text
,`charge` int(11)
,`spin` int(11)
,`method` text
,`basis` text
);

-- --------------------------------------------------------

DROP VIEW IF EXISTS `MoleculeGeometryListView`;
CREATE TABLE IF NOT EXISTS `MoleculeGeometryListView` (
`UUID` varchar(50)
,`Name` text
,`Formula` text
,`Charge` int(11)
,`Spin` int(11)
,`Stationary` tinyint(1)
,`Method` text
,`Basis` text
,`InChIKey` text
,`elements` longtext
);

-- --------------------------------------------------------

DROP VIEW IF EXISTS `ReactionEnergyListView`;
CREATE TABLE IF NOT EXISTS `ReactionEnergyListView` (
`UUID` varchar(50)
,`Name` text
,`Method` text
,`Basis` text
,`Energy` double
,`GeomOpt` tinyint(1)
,`elements` longtext
);

-- --------------------------------------------------------

DROP TABLE IF EXISTS `AE_IDSTR`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `AE_IDSTR` AS select `AE`.`UUID` AS `ae_uuid`,`AE`.`energy` AS `energy`, `AE`.`energy_unit` as `unit`, `GEOM`.`name` AS `name`,`GEOM`.`charge` AS `charge`,`AE`.`method` AS `method`,`AE`.`basis` AS `basis` from (`molecular_geometry` `GEOM` join `molecularatomizationenergy` `AE`) where (`AE`.`geom_id` = `GEOM`.`UUID`);

-- --------------------------------------------------------

DROP TABLE IF EXISTS `AllGeometryUUID`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `AllGeometryUUID` AS select `molecular_geometry`.`UUID` AS `UUID` from `molecular_geometry` union select `crystal_geometry`.`UUID` AS `UUID` from `crystal_geometry`;

-- --------------------------------------------------------

DROP TABLE IF EXISTS `All_UUID_View`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `All_UUID_View` AS select `ReactionEnergyListView`.`UUID` AS `UUID`,`ReactionEnergyListView`.`Name` AS `name`,`ReactionEnergyListView`.`elements` AS `elements`,`ReactionEnergyListView`.`Method` AS `Method`,`ReactionEnergyListView`.`Basis` AS `Basis` from `ReactionEnergyListView` union select `BandstructureListView`.`UUID` AS `UUID`,`BandstructureListView`.`Name` AS `name`,`BandstructureListView`.`elements` AS `elements`,`BandstructureListView`.`Method` AS `Method`,`BandstructureListView`.`Basis` AS `Basis` from `BandstructureListView` union select `MoleculeGeometryListView`.`UUID` AS `UUID`,`MoleculeGeometryListView`.`Name` AS `name`,`MoleculeGeometryListView`.`elements` AS `elements`,`MoleculeGeometryListView`.`Method` AS `Method`,`MoleculeGeometryListView`.`Basis` AS `Basis` from `MoleculeGeometryListView` union select `MolecularFrequencyListView`.`UUID` AS `UUID`,`MolecularFrequencyListView`.`Reference Geometry` AS `Reference Geometry`,`MolecularFrequencyListView`.`elements` AS `elements`,`MolecularFrequencyListView`.`Method` AS `Method`,`MolecularFrequencyListView`.`Basis` AS `Basis` from `MolecularFrequencyListView` union select `MolecularAtomizationEnergyListView`.`UUID` AS `UUID`,`MolecularAtomizationEnergyListView`.`Reference Geometry` AS `Reference Geometry`,`MolecularAtomizationEnergyListView`.`elements` AS `elements`,`MolecularAtomizationEnergyListView`.`Method` AS `Method`,`MolecularAtomizationEnergyListView`.`Basis` AS `Basis` from `MolecularAtomizationEnergyListView` order by `Name`;

-- --------------------------------------------------------

DROP TABLE IF EXISTS `BandstructureListView`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `BandstructureListView` AS select `BS`.`UUID` AS `UUID`,`BS`.`name` AS `Name`,`GEOM`.`name` AS `Reference Geometry`,`BS`.`method` AS `Method`,`BS`.`basis` AS `Basis`,`GEOM`.`elements` AS `elements` from (`bandstructure` `BS` join `crystal_geometry` `GEOM`) where (`GEOM`.`UUID` = `BS`.`geom_id`) order by `BS`.`name`;

-- --------------------------------------------------------

DROP TABLE IF EXISTS `CrystalGeometryListView`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `CrystalGeometryListView` AS select `GEOM`.`UUID` AS `UUID`,`GEOM`.`name` AS `Name`,`GEOM`.`formula` AS `Formula`,`GEOM`.`method` AS `Method`,`GEOM`.`basis` AS `Basis`,`GEOM`.`elements` AS `elements` from `crystal_geometry` `GEOM` order by `GEOM`.`name`;

-- --------------------------------------------------------

DROP TABLE IF EXISTS `CrystalGeometry_IDSTR`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `CrystalGeometry_IDSTR` AS select `GEOM`.`UUID` AS `geom_uuid`,`GEOM`.`name` AS `name`,`GEOM`.`method` AS `method`,`GEOM`.`basis` AS `basis` from `crystal_geometry` `GEOM` order by `GEOM`.`name`;

-- --------------------------------------------------------


DROP TABLE IF EXISTS `EnergyEquationList`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `EnergyEquationList` AS select `BS`.`UUID` AS `UUID`,`GEOM`.`name` AS `Reference Geometry`,`GEOM`.`method` AS `Geometry Method`,`GEOM`.`basis` AS `Geometry Basis`,`BS`.`method` AS `Method`,`BS`.`basis` AS `Basis`,`BS`.`energy` AS `Energy`, `BS`.`energy_unit` AS `Unit`, `GEOM`.`elements` AS `elements` from (`molecularatomizationenergy` `BS` join `molecular_geometry` `GEOM`) where (`GEOM`.`UUID` = `BS`.`geom_id`) order by `GEOM`.`name`;

-- --------------------------------------------------------

DROP TABLE IF EXISTS `MolecularAtomizationEnergyListView`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `MolecularAtomizationEnergyListView` AS select `BS`.`UUID` AS `UUID`,`GEOM`.`name` AS `Reference Geometry`,`BS`.`method` AS `Method`,`BS`.`basis` AS `Basis`,`BS`.`energy` AS `Energy`, `BS`.`energy_unit` AS `Unit`, `GEOM`.`elements` AS `elements` from (`molecularatomizationenergy` `BS` join `molecular_geometry` `GEOM`) where (`GEOM`.`UUID` = `BS`.`geom_id`) order by `GEOM`.`name`;

-- --------------------------------------------------------

DROP TABLE IF EXISTS `MolecularFrequencyListView`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `MolecularFrequencyListView` AS select `BS`.`UUID` AS `UUID`,`GEOM`.`name` AS `Reference Geometry`,`BS`.`method` AS `Method`,`BS`.`basis` AS `Basis`,`GEOM`.`elements` AS `elements` from (`molecularfrequency` `BS` join `molecular_geometry` `GEOM`) where (`GEOM`.`UUID` = `BS`.`geom_id`) order by `GEOM`.`name`;

-- --------------------------------------------------------

DROP TABLE IF EXISTS `MolecularGeometry_IDSTR`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `MolecularGeometry_IDSTR` AS select `GEOM`.`UUID` AS `geom_uuid`,`GEOM`.`name` AS `name`,`GEOM`.`charge` AS `charge`,`GEOM`.`spin` AS `spin`,`GEOM`.`method` AS `method`,`GEOM`.`basis` AS `basis` from `molecular_geometry` `GEOM` order by `GEOM`.`name`;

-- --------------------------------------------------------
DROP TABLE IF EXISTS `MoleculeGeometryListView`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `MoleculeGeometryListView` AS select `GEOM`.`UUID` AS `UUID`,`GEOM`.`name` AS `Name`,`GEOM`.`formula` AS `Formula`,`GEOM`.`charge` AS `Charge`,`GEOM`.`spin` AS `Spin`,`GEOM`.`is_stationary` AS `Stationary`,`GEOM`.`method` AS `Method`,`GEOM`.`basis` AS `Basis`,`GEOM`.`elements` AS `elements` from `molecular_geometry` `GEOM` order by `GEOM`.`name`;

-- --------------------------------------------------------

DROP TABLE IF EXISTS `ReactionEnergyListView`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `ReactionEnergyListView` AS select `BS`.`UUID` AS `UUID`,`BS`.`name` AS `Name`,`BS`.`method` AS `Method`,`BS`.`basis` AS `Basis`,`BS`.`energy` AS `Energy`, `BS`.`energy_unit` as `Unit`,`BS`.`need_opt` AS `GeomOpt`,`BS`.`elements` AS `elements` from `reactionenergy` `BS` order by `BS`.`name`;

-- --------------------------------------------------------

DROP TABLE IF EXISTS `info`;
CREATE TABLE IF NOT EXISTS `info` (
      `application` text ,
      `version` int(11) NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `info` (`application`, `version`) VALUES
('DFTBDB', 5);

ALTER TABLE `info`
  ADD PRIMARY KEY (`version`);

SET FOREIGN_KEY_CHECKS=1;
COMMIT;




