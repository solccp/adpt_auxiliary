#ifndef DFTBREFDATABASE_H
#define DFTBREFDATABASE_H

#include <QObject>
#include <QStringList>
#include <QSqlDatabase>
#include <QMutex>
#include <QHash>

QT_BEGIN_NAMESPACE
class QAbstractTableModel;
class QAbstractListModel;
QT_END_NAMESPACE
class Geometry;
class BandstructureData;
class MolecularFrequencyData;
class MolecularAtomizationEnergyData;
class ReactionEnergyData;
//class MolecularTotalEnergy;

class DFTBRefDatabase : public QObject
{
    Q_OBJECT
public:
    QMutex mutex;
    DFTBRefDatabase(QObject* parent = 0);
    ~DFTBRefDatabase();
    void disbleSignal();
    void enableSignal();
    bool m_signalEnabled = true;

    int loadOnlineDatabase(const QString& hostName, const QString& userName, const QString& password, const QString& databaseName);
    bool loadDatabasefile(const QString& filename);
    void closeDatabase();
    QString getLastError() const;
    bool setupTables();
    void clear();
    void clearError();
    bool isOpen();

    enum DFTBRefDatabaseError
    {
        NoError = 0,
        DatabaseVersionError = 1,
        DatabaseSchemaError = 2,
        ServerError = 3,
        AccountError = 4,
        DatabaseNameError = 5,
        NoReadPermissionError = 6,
        UnknownError = 99
    };


    enum GeometryType
    {
        Crystal,
        Molecule,
        Both
    };

    QAbstractTableModel *getGeometryListModel(GeometryType type = Both);
    QAbstractTableModel* getGeometryTableModel(GeometryType type = Both);
//    QAbstractTableModel* getBandStructureTableModel();
//    QAbstractTableModel* getMolecularFrequencyTableModel();
//    QAbstractTableModel* getMolecularAtomizationEnergyTableModel();
//    QAbstractTableModel* getReactionEnergyTableModel();
//    QAbstractTableModel* getTableModelFromSQL(const QString& sql);

    bool hasGeometryUUID(const QString& uuid, GeometryType type);

public:
    QStringList getAvailableElements();
    QList<Geometry*> getAllGeometry();

    QList<QString> getGeometryUUID();

    QString getGeometryName(const QString& uuid);

    QSqlDatabase database() const;

public:



    //geometry
    bool getGeometry(const QString& uuid, Geometry* geom);

    bool addGeometry(Geometry* data);
    bool updateGeometry(Geometry* data, const QString &oldUUID);
    bool removeGeometries(QStringList uuids);

private:
    bool addMolecularGeometry(Geometry* data);
    bool addCrystalGeometry(Geometry* data);
    bool getMolecularGeometry(const QString& uuid, Geometry* geom);
    bool getCrystalGeometry(const QString& uuid, Geometry* geom);
    bool updateMolecularGeometry(Geometry* data);
    bool updateCrystalGeometry(Geometry* data);




public:
    bool getBandStructure(const QString &uuid, BandstructureData *bandstructure);

    bool addBandStructure(BandstructureData *data);
    bool updateBandStructure(BandstructureData *data, const QString &old_UUID="");
    bool removeBandStructure(const QString& uuid);
    bool removeBandStructure(const QStringList& uuids);
public:
    bool getMolecularFrequency(const QString &uuid, MolecularFrequencyData *data);


    bool addMolecularFrequency(MolecularFrequencyData *data);
    bool updateMolecularFrequency(MolecularFrequencyData *data);
    bool removeMolecularFrequency(const QString& uuid);
    bool removeMolecularFrequency(const QStringList& uuids);
public:
    bool getMolecularAtomizationEnergy(const QString &uuid, MolecularAtomizationEnergyData *data);
    bool getMolecularAtomizationEnergy_old(const QString &uuid, MolecularAtomizationEnergyData *data);

    bool addMolecularAtomizationEnergy(MolecularAtomizationEnergyData *data);
    bool updateMolecularAtomizationEnergy(MolecularAtomizationEnergyData *data);
    bool removeMolecularAtomizationEnergy(const QString& uuid);
    bool removeMolecularAtomizationEnergy(const QStringList& uuids);
public:
    bool getReactionEnergy(const QString &uuid, ReactionEnergyData *data);
    bool addReactionEnergy(ReactionEnergyData *data);
    bool updateReactionEnergy(ReactionEnergyData *data);
    bool removeReactionEnergy(const QString& uuid);
    bool removeReactionEnergy(const QStringList& uuids);


private:  //data_binding
    void bindSQLCrystalGeometry(QSqlQuery& query, Geometry* data, bool update);

    void bindSQLMolecularGeometry(QSqlQuery& query, Geometry* data, bool update);

    void bindSQLMolecularAtomizationEnergy(QSqlQuery& query, MolecularAtomizationEnergyData *data, bool update);
    void bindSQLMolecularFrequency(QSqlQuery& query, MolecularFrequencyData *data, bool update) ;

    void bindSQLBandStructure(QSqlQuery& query, BandstructureData *data, bool update, const QString &old_UUID="");


public:
    bool isResetNeeded();
    bool canRead() const;
    bool canWrite() const;
    bool canCreate() const;

    int lastErrorNumber() const;

    void transaction();
    bool commit();
    void rollback();

    void disableTransaction();
    void enableTransaction();

signals:
    void geometryUpdated();
    void bandstructureUpdated();
    void molecularFrequencyUpdated();
    void molecularAtomizationEnergyUpdated();
    void reactionEnergyUpdated();
    void databaseConnectionChanged();

protected:
    QString field2sqlquery(QStringList fields, QString table_name, bool update, const QString &old_UUID="");
    
private:
//    QSqlDatabase db;

    QString dbname = "remote_1";

    QString m_lastError;
    int m_lastErrorNumber;
    bool resetNeeded = false;
private:
    void setCanRead(bool canRead);
    void setCanWrite(bool canWrite);
    void setCanCreate(bool canCreate);
    int checkDatabase();
    int checkPermission();
    bool m_canRead = false;
    bool m_canWrite = false;
    bool m_canCreate = false;

    bool m_inTransaction = false;
    bool m_transactionEnabled = true;

};

#endif // LIBDFTBREFDATABASE_H
