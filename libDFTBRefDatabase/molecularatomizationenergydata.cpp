#include "molecularatomizationenergydata.h"
#include <QTextStream>
#include <QUuid>

MolecularAtomizationEnergyData::MolecularAtomizationEnergyData(QObject *parent) :
    QObject(parent)
{
    this->m_UUID = QUuid::createUuid().toString();
}
QString MolecularAtomizationEnergyData::UUID() const
{
    return m_UUID;
}

void MolecularAtomizationEnergyData::resetUUID()
{
    this->m_UUID = createUUID();
}
QString MolecularAtomizationEnergyData::geom_uuid() const
{
    return m_geom_uuid;
}

void MolecularAtomizationEnergyData::setGeom_uuid(const QString &geom_uuid)
{
    m_geom_uuid = geom_uuid;
}
QString MolecularAtomizationEnergyData::method() const
{
    return m_method;
}

void MolecularAtomizationEnergyData::setMethod(const QString &method)
{
    m_method = method;
}
QString MolecularAtomizationEnergyData::basis() const
{
    return m_basis;
}

void MolecularAtomizationEnergyData::setBasis(const QString &basis)
{
    m_basis = basis;
}
double MolecularAtomizationEnergyData::energy() const
{
    return m_energy;
}

void MolecularAtomizationEnergyData::setEnergy(double energy)
{
    m_energy = energy;
}
QString MolecularAtomizationEnergyData::citation() const
{
    return m_citation;
}

void MolecularAtomizationEnergyData::setCitation(const QString &citation)
{
    m_citation = citation;
}
QString MolecularAtomizationEnergyData::comment() const
{
    return m_comment;
}

void MolecularAtomizationEnergyData::setComment(const QString &comment)
{
    m_comment = comment;
}

QString MolecularAtomizationEnergyData::unit() const
{
    return m_energy_unit;
}

QString MolecularAtomizationEnergyData::createUUID()
{
    QByteArray array;
    QTextStream stream(&array);
    stream << this->m_geom_uuid << endl;
    stream << this->m_method << endl;
    stream << this->m_basis << endl;
    stream << QString::number(this->m_energy, 'f', 6) << endl;
    stream << this->m_energy_unit << endl;
    return QUuid::createUuidV5(QUuid(), array).toString();
}

void MolecularAtomizationEnergyData::setUnit(const QString &unit)
{
    m_energy_unit  =unit;
}

