#-------------------------------------------------
#
# Project created by QtCreator 2013-09-13T15:27:50
#
#-------------------------------------------------

QT       += sql core


TARGET = DFTBRefDatabase
TEMPLATE = lib
CONFIG += staticlib c++11

SOURCES += \
    dftbrefdatabase.cpp \
    geometry.cpp \
    utils.cpp \
    bandstructuredata.cpp \
    molecularfrequencydata.cpp \
    molecularatomizationenergydata.cpp \
    reactionenergydata.cpp \
    geometrylistmodel.cpp

HEADERS += \
    dftbrefdatabase.h \
    geometry.h \
    utils.h \
    bandstructuredata.h \
    molecularfrequencydata.h \
    molecularatomizationenergydata.h \
    reactionenergydata.h \
    geometrylistmodel.h \
    datastructure_variant.h

RESOURCES += sqls/database_structure_sql.qrc

OTHER_FILES +=

INCLUDEPATH += $$PWD/../external_libs/include


win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libadpt_common/release/ -ladpt_common
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libadpt_common/debug/ -ladpt_common
else:unix: LIBS += -L$$OUT_PWD/../libadpt_common/ -ladpt_common

INCLUDEPATH += $$PWD/../libadpt_common
DEPENDPATH += $$PWD/../libadpt_common

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/release/libadpt_common.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/debug/libadpt_common.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/release/libadpt_common.a
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/debug/libadpt_common.a
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/libadpt_common.a
