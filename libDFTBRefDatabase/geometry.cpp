#include "geometry.h"
#include "atomicproperties.h"

#include "utils.h"

#include <QStringList>

#include <sstream>
#include <QDebug>

#include <QUuid>

#include <array>
#include <Variant/Variant.h>
#include "variantqt.h"

#include "datastructure_variant.h"



Geometry::Geometry()//(QObject *parent) :QObject(parent)
{
    m_UUID = QUuid::createUuid().toString();
    m_lattice_vectors[0] = {{1,0,0}};
    m_lattice_vectors[1] = {{0,1,0}};
    m_lattice_vectors[2] = {{0,0,1}};
    m_kpoints_setting.m_type = ADPT::KPointsSetting::Type::SupercellFolding;
    m_kpoints_setting.m_kpoints_supercell.num_of_cells[0] = {{16, 0, 0}};
    m_kpoints_setting.m_kpoints_supercell.num_of_cells[1] = {{0, 16, 0}};
    m_kpoints_setting.m_kpoints_supercell.num_of_cells[2] = {{0, 0, 16}};
    m_kpoints_setting.m_kpoints_supercell.shifts = {{0.5, 0.5, 0.5}};
}

Geometry::~Geometry()
{

}
QString Geometry::getMethod() const
{
    return m_method;
}

void Geometry::setMethod(const QString &value)
{
    m_method = value;
}
QString Geometry::getBasis() const
{
    return m_basis;
}

void Geometry::setBasis(const QString &value)
{
    m_basis = value;
}

QString Geometry::getName() const
{
    return m_name;
}

QString Geometry::getFormula() const
{
    return m_formula;
}

QString Geometry::getDBElementString() const
{
    QStringList elements;
    for(int i=0; i<m_coordinates.size();++i)
    {
        const ADPT::AtomicProperties* elem = ADPT::AtomicProperties::fromSymbol(m_coordinates[i].symbol);
        if (elem != &ADPT::AtomicProperties::ERROR_ATOM)
        {
            elements.append("[" + elem->getSymbol() + "]");
        }
    }

    elements.removeDuplicates();
    elements.sort();
    return elements.join(",");
}

QString Geometry::getDBCoordinatesString() const
{
    libvariant::Variant var = libvariant::VariantConvertor<QList<Coordinate>>::toVariant(this->m_coordinates);
    QString var_str = QString::fromStdString(libvariant::SerializeJSON(var, false));
    return var_str;
}

QString Geometry::getDBKpointsString() const
{
    libvariant::Variant var = libvariant::VariantConvertor<ADPT::KPointsSetting>::toVariant(this->m_kpoints_setting);
    QString var_str = QString::fromStdString(libvariant::SerializeJSON(var, false));
    return var_str;
}

QString Geometry::getDBLatticeVectors() const
{
    libvariant::Variant var = libvariant::VariantConvertor<dMatrix3x3>::toVariant(this->m_lattice_vectors);
    QString var_str = QString::fromStdString(libvariant::SerializeJSON(var, false));
    return var_str;
}

bool Geometry::parseDBKPoints(const QVariant &var)
{
    libvariant::Variant mvar = libvariant::DeserializeJSON(var.toString().toLatin1());
    try
    {
        libvariant::VariantConvertor<ADPT::KPointsSetting>::fromVariant(mvar, this->m_kpoints_setting);
        return true;
    }
    catch(...)
    {
        return false;
    }
}

bool Geometry::parseDBLattiveVectors(const QVariant &var)
{
    auto mvar = libvariant::DeserializeJSON(var.toString().toLatin1());
    try
    {
        libvariant::VariantConvertor<dMatrix3x3>::fromVariant(mvar, this->m_lattice_vectors);
        return true;
    }
    catch(...)
    {
        return false;
    }
}

bool Geometry::parseDBCoordinates(const QVariant &var)
{
    auto mvar = libvariant::DeserializeJSON(var.toString().toLatin1());
    try
    {
        libvariant::VariantConvertor<QList<Coordinate>>::fromVariant(mvar, this->m_coordinates);
        return true;
    }
    catch(...)
    {
        return false;
    }
}


void Geometry::setCharge(const QString &intStr)
{
    bool ok;
    int charge = intStr.toInt(&ok);
    if (ok)
    {
        setCharge(charge);
    }
}

void Geometry::setCharge(int charge)
{
    m_charge = charge;
}

void Geometry::setSpin(int spin)
{
    m_spin = spin;
}

int Geometry::getSpin() const
{
    return m_spin;
}

int Geometry::getCharge() const
{
    return m_charge;
}

void Geometry::setUPE(const QString &intStr)
{
    bool ok;
    double value = intStr.toDouble(&ok);
    if (ok)
    {
        m_upe = value;
    }
}

void Geometry::setUPE(double upe)
{
    m_upe = upe;
}

double Geometry::getUPE() const
{
    return m_upe;
}


bool Geometry::checkCrystalInputs(QString &errorMsg)
{
    if (m_PBC)
    {
        for (int row=0; row<3; ++row)
        {
            double norm = 0.0;
            for(int col=0; col<3; ++col)
            {
                norm += (m_lattice_vectors[row][col])*(m_lattice_vectors[row][col]);
            }
            norm = sqrt(norm);
            if (! (norm > 0.0))
            {
                errorMsg = QString("The norm of the %1 lattice vector is 0").arg(orderNumber(row+1));
                return false;
            }
        }



        return true;
    }
    else
    {
        return true;
    }
    errorMsg = "Unknown Error";
    return false;
}

void Geometry::setName(const QString &name)
{
    this->m_name = name;
}

QString Geometry::getXYZ() const
{
    QStringList res;
    res.append(QString::number(m_coordinates.size()));
    res.append("");

    std::array<std::array<double, 3> ,3> vec;
    if (this->PBC())
    {
        for(int row=0; row<3; ++row)
        {
          for(int col=0; col<3; ++col)
          {
              vec[row][col] = m_scaling_factor*m_lattice_vectors[row][col];
          }
        }

        if (this->relativeCoordinates())
        {
            for(int i=0; i<m_coordinates.size();++i)
            {
                std::array<double, 3> final_coordinate = {{0.0, 0.0, 0.0}};
                for(int j=0; j<3; ++j)
                {
                    for(int k=0; k<3; ++k)
                    {
                        final_coordinate[j] += m_coordinates[i].coord[j]*vec[k][j];
                    }
                }

                res.append(QString("%1 %2 %3 %4").arg(m_coordinates.at(i).symbol)
                           .arg(final_coordinate[0],14,'f',8)
                           .arg(final_coordinate[1],14,'f',8)
                           .arg(final_coordinate[2],14,'f',8)
                           );
            }
        }
        else
        {
            for(int i=0; i<m_coordinates.size();++i)
            {
                res.append(QString("%1 %2 %3 %4").arg(m_coordinates.at(i).symbol)
                           .arg(m_coordinates.at(i).coord[0],14,'f',8)
                           .arg(m_coordinates.at(i).coord[1],14,'f',8)
                           .arg(m_coordinates.at(i).coord[2],14,'f',8)
                           );
            }
        }
    }
    else
    {
        for(int i=0; i<m_coordinates.size();++i)
        {
            res.append(QString("%1 %2 %3 %4").arg(m_coordinates.at(i).symbol)
                       .arg(m_coordinates.at(i).coord[0],14,'f',8)
                       .arg(m_coordinates.at(i).coord[1],14,'f',8)
                       .arg(m_coordinates.at(i).coord[2],14,'f',8)
                       );
        }
    }

    if (this->PBC())
    {
        for(int row=0; row<3; ++row)
        {
          res.append(QString("TV %1 %2 %3")
                .arg(vec[row][0],20,'f',12)
                .arg(vec[row][1],20,'f',12)
                .arg(vec[row][2],20,'f',12));
        }
    }

    return res.join("\n");
}

QString Geometry::getGen() const
{
    QStringList res;
    if (this->PBC())
    {
        if (this->relativeCoordinates())
        {
            res << QString("%1 F").arg(m_coordinates.size());
        }
        else
        {
            res << QString("%1 S").arg(m_coordinates.size());
        }
    }
    else
    {
        res << QString("%1 C").arg(m_coordinates.size());
    }

    QStringList elements;
    for(int i=0; i<m_coordinates.size();++i)
    {
        const ADPT::AtomicProperties* elem = ADPT::AtomicProperties::fromSymbol(m_coordinates[i].symbol);
                elements.append(elem->getSymbol());
    }

    elements.removeDuplicates();
    res << elements.join(" ");
    for(int i=0; i<m_coordinates.size();++i)
    {
        const ADPT::AtomicProperties* elem = ADPT::AtomicProperties::fromSymbol(m_coordinates[i].symbol);
        res << QString("%1 %2 %3 %4 %5").arg(i+1)
               .arg(elements.indexOf(elem->getSymbol())+1)
               .arg(m_coordinates[i].coord[0], 20, 'f', 12)
               .arg(m_coordinates[i].coord[1], 20, 'f', 12)
               .arg(m_coordinates[i].coord[2], 20, 'f', 12);
    }
    if (this->PBC())
    {
       res.append("0.0 0.0 0.0");
          std::array<std::array<double, 3> ,3> vec;
          for(int row=0; row<3; ++row)
          {
            for(int col=0; col<3; ++col)
            {
                vec[row][col] = m_scaling_factor*m_lattice_vectors[row][col];
            }
            res.append(QString("%1 %2 %3")
                  .arg(vec[row][0],20,'f',12)
                  .arg(vec[row][1],20,'f',12)
                  .arg(vec[row][2],20,'f',12));
          }
    }
    return res.join("\n");
}

bool Geometry::writeOut(QTextStream &os, const QString &format)
{
    if (format.toLower() == "gen")
    {
        os << getGen() << endl;
        return true;
    }
    else if (format.toLower() == "xyz")
    {
        os << getXYZ() << endl;
        return true;

    }
    else if (format.toLower() == "hsd")
    {
        std::array<std::array<double, 3> ,3> vec;
        if (this->PBC())
        {
            os << "Periodic = Yes" << endl;
            os << "LatticeVectors[Angstrom] = {" << endl ;

            for(int row=0; row<3; ++row)
            {
              for(int col=0; col<3; ++col)
              {
                  vec[row][col] = m_scaling_factor*m_lattice_vectors[row][col];
              }
              os << QString("%1 %2 %3")
                    .arg(vec[row][0],20,'f',12)
                    .arg(vec[row][1],20,'f',12)
                    .arg(vec[row][2],20,'f',12) << endl;
            }

            os << "}" << endl;
        }

        QStringList elements;
        for(int i=0; i<m_coordinates.size();++i)
        {
            const ADPT::AtomicProperties* elem = ADPT::AtomicProperties::fromSymbol(m_coordinates[i].symbol);
            elements.append(elem->getSymbol());
        }
        elements.removeDuplicates();

        os << "TypeNames = {" << elements.join(" ") << "}" << endl;

        if (!this->PBC())
        {
            os << "TypesAndCoordinates = {" << endl;
            for(int i=0; i<m_coordinates.size();++i)
            {
                const ADPT::AtomicProperties* elem = ADPT::AtomicProperties::fromSymbol(m_coordinates[i].symbol);
                os << elements.indexOf(elem->getSymbol())+1 << " "
                   << QString("%1").arg(m_coordinates[i].coord[0], 20, 'f', 12) << " "
                   << QString("%1").arg(m_coordinates[i].coord[1], 20, 'f', 12) << " "
                   << QString("%1").arg(m_coordinates[i].coord[2], 20, 'f', 12) << endl;
            }
            os << "}" << endl;
            os << endl;
        }
        else
        {
            os << "TypesAndCoordinates" ;
            if (this->relativeCoordinates())
            {
                os << "[Relative]";
                os << " = {" << endl;
                for(int i=0; i<m_coordinates.size();++i)
                {
                    const ADPT::AtomicProperties* elem = ADPT::AtomicProperties::fromSymbol(m_coordinates[i].symbol);
                    os << elements.indexOf(elem->getSymbol())+1 << " "
                       << QString("%1").arg(m_coordinates[i].coord[0], 20, 'f', 12) << " "
                       << QString("%1").arg(m_coordinates[i].coord[1], 20, 'f', 12) << " "
                       << QString("%1").arg(m_coordinates[i].coord[2], 20, 'f', 12) << endl;
                }
            }
            else
            {
                os << "[Angstrom]";
                os << " = {" << endl;
                for(int i=0; i<m_coordinates.size();++i)
                {
                    const ADPT::AtomicProperties* elem = ADPT::AtomicProperties::fromSymbol(m_coordinates[i].symbol);
                    std::array<double, 3> final_coordinate = {{0.0, 0.0, 0.0}};
                    for(int j=0; j<3; ++j)
                    {
                        for(int k=0; k<3; ++k)
                        {
                            final_coordinate[j] += m_coordinates[i].coord[j]*vec[k][j];
                        }
                    }
                  os << elements.indexOf(elem->getSymbol())+1 << " "
                     << QString("%1").arg(final_coordinate[0], 20, 'f', 12) << " "
                     << QString("%1").arg(final_coordinate[1], 20, 'f', 12) << " "
                     << QString("%1").arg(final_coordinate[2], 20, 'f', 12) << endl;
                }
            }



            os << "}" << endl;
            os << endl;
        }
        return true;
    }
    return false;
}

QString Geometry::computeUUID()
{
    QByteArray array;
    QTextStream stream(&array);
    stream << this->m_name << endl;
    stream << this->m_method << endl;
    stream << this->m_basis << endl;
    for(auto const& item : this->m_coordinates)
    {
        stream << item.symbol << " " << item.coord[0] << " " << item.coord[1] << " " << item.coord[2] << endl;
    }
    if (this->m_PBC)
    {
        if (this->m_relativeCoordinates)
        {
            stream << "true" << endl;
        }
        else
        {
            stream << "false" << endl;
        }
        stream << QString::number(m_scaling_factor, 'f', 6) << endl;
        for(auto i: {0,1,2})
        {
            stream << QString::number(m_lattice_vectors[i][0], 'f', 6) << " "
                   << QString::number(m_lattice_vectors[i][1], 'f', 6) << " "
                   << QString::number(m_lattice_vectors[i][2], 'f', 6) << endl;
        }
        stream << "kpoints" << " ";
        if (this->m_kpoints_setting.m_type == ADPT::KPointsSetting::Type::Plain)
        {
            stream << "plain" << endl;
            for(auto const& item : this->m_kpoints_setting.m_kpoints_plain)
            {
                stream << QString::number(item.weight, 'f', 6) << " "
                       << QString::number(item.kpoint[0], 'f', 6) << " "
                       << QString::number(item.kpoint[1], 'f', 6) << " "
                       << QString::number(item.kpoint[2], 'f', 6) << endl;
            }
        }
        else if (this->m_kpoints_setting.m_type == ADPT::KPointsSetting::Type::SupercellFolding)
        {
            stream << "supercell_folding" << endl;
            for(auto i : {0,1,2})
            {
                stream << QString::number(m_kpoints_setting.m_kpoints_supercell.num_of_cells[i][0]) << " "
                       << QString::number(m_kpoints_setting.m_kpoints_supercell.num_of_cells[i][1]) << " "
                       << QString::number(m_kpoints_setting.m_kpoints_supercell.num_of_cells[i][2]) << endl;
            }
            stream << QString::number(m_kpoints_setting.m_kpoints_supercell.shifts[0]) << " "
                   << QString::number(m_kpoints_setting.m_kpoints_supercell.shifts[1]) << " "
                   << QString::number(m_kpoints_setting.m_kpoints_supercell.shifts[2]) << endl;
        }
        else
        {
            throw std::runtime_error("unknown kpoints setting");
        }

    }
    else
    {
        stream << QString::number(m_charge) << " " << QString::number(m_spin) << endl;
        if (this->m_isStationary)
        {
            stream << "true" << endl;
        }
        else
        {
            stream << "false" << endl;
        }
    }

    return QUuid::createUuidV5(QUuid(), array).toString();
}

void Geometry::computeFormula()
{
    QMap<QString, int> map;
    for(auto const & line : this->m_coordinates)
    {
        map[line.symbol] += 1;
    }
    QStringList formula_list;
    QMapIterator<QString, int> it(map);
    while(it.hasNext())
    {
        it.next();
        formula_list.append(it.key());
        formula_list.append(QString::number(it.value()));
    }
    this->m_formula = formula_list.join("");
//    emit formulaChanged(m_formula);
}

ADPT::KPointsSetting &Geometry::kpointsSettings()
{
    return m_kpoints_setting;
}

const ADPT::KPointsSetting &Geometry::kpointsSettings() const
{
    return m_kpoints_setting;
}

void Geometry::setKpointsSettings(const ADPT::KPointsSetting &kpointsSettings)
{
    m_kpoints_setting = kpointsSettings;
}


dMatrix3x3 Geometry::lattice_vectors() const
{
    return m_lattice_vectors;
}

void Geometry::setLattice_vectors(const dMatrix3x3 &lattice_vectors)
{
    m_lattice_vectors = lattice_vectors;
}

double Geometry::scaling_factor() const
{
    return m_scaling_factor;
}

void Geometry::setScaling_factor(double scaling_factor)
{
    m_scaling_factor = scaling_factor;
}

bool Geometry::isStationary() const
{
    return m_isStationary;
}

void Geometry::setIsStationary(bool isStationary)
{
    m_isStationary = isStationary;
}

void Geometry::resetUUID()
{
    m_UUID = computeUUID();
}

bool Geometry::relativeCoordinates() const
{
    return m_relativeCoordinates;
}

void Geometry::setRelativeCoordinates(bool relativeCoordinates)
{
    m_relativeCoordinates = relativeCoordinates;
}

const QList<Coordinate>& Geometry::coordinates() const
{
    return m_coordinates;
}

QList<dVector3> &Geometry::force()
{
    return m_force;
}

const QList<dVector3> &Geometry::force() const
{
    return m_force;
}

void Geometry::setForce(const QList<dVector3> &force)
{
    m_force = force;
}


QString Geometry::UUID() const
{
    return m_UUID;
}

void Geometry::setUUID(const QString &uuid)
{
    m_UUID = uuid;
}

QList<Coordinate> &Geometry::coordinates()
{
    return m_coordinates;
}


QString Geometry::comment() const
{
    return m_comment;
}

void Geometry::setComment(const QString &comment)
{
    m_comment = comment;
}

QString Geometry::citation() const
{
    return m_citation;
}

void Geometry::setCitation(const QString &citation)
{
    m_citation = citation;
}

bool Geometry::PBC() const
{
    return m_PBC;
}

void Geometry::setPBC(bool PBC)
{
    m_PBC = PBC;
}


