#include "reactionenergydata.h"
#include <QUuid>
#include <QTextStream>

ReactionEnergyData::ReactionEnergyData(QObject *parent) :
    QObject(parent)
{
    this->m_UUID = QUuid::createUuid().toString();
}
QString ReactionEnergyData::UUID() const
{
    return m_UUID;
}

void ReactionEnergyData::setUUID(const QString &uuid)
{
    m_UUID = uuid;
}

void ReactionEnergyData::resetUUID()
{
    this->m_UUID = createUUID();
}
QString ReactionEnergyData::method() const
{
    return m_method;
}

void ReactionEnergyData::setMethod(const QString &method)
{
    m_method = method;
}
QString ReactionEnergyData::basis() const
{
    return m_basis;
}

void ReactionEnergyData::setBasis(const QString &basis)
{
    m_basis = basis;
}
double ReactionEnergyData::energy() const
{
    return m_energy;
}

void ReactionEnergyData::setEnergy(double energy)
{
    m_energy = energy;
}

void ReactionEnergyData::setEnergyUnit(const QString &unit)
{
    m_energy_unit = unit;
}
QString ReactionEnergyData::name() const
{
    return m_name;
}

void ReactionEnergyData::setName(const QString &name)
{
    m_name = name;
}

bool ReactionEnergyData::checkData(QString &errorMsg)
{
    if (m_name.isEmpty() || m_method.isEmpty())
    {
        if (m_name.isEmpty())
        {
            errorMsg = "Name cannot be empty.";
        }
        else
        {
            errorMsg = "Method cannot be empty.";
        }
        return false;
    }
    return true;
}
QString ReactionEnergyData::comment() const
{
    return m_comment;
}

void ReactionEnergyData::setComment(const QString &comment)
{
    m_comment = comment;
}
QString ReactionEnergyData::citation() const
{
    return m_citation;
}

void ReactionEnergyData::setCitation(const QString &citation)
{
    m_citation = citation;
}
const QList<ReactionItem >& ReactionEnergyData::reactants() const
{
    return m_reactants;
}
QList<ReactionItem> &ReactionEnergyData::reactants()
{
    return m_reactants;
}

void ReactionEnergyData::setReactants(const QList<ReactionItem> &reactants)
{
    m_reactants = reactants;
}

QList<ReactionItem> &ReactionEnergyData::products()
{
    return m_products;
}
const QList<ReactionItem> &ReactionEnergyData::products() const
{
    return m_products;
}

void ReactionEnergyData::setProducts(const QList<ReactionItem> &products)
{
    m_products = products;
}

QString ReactionEnergyData::unit() const
{
    return m_energy_unit;
}
bool ReactionEnergyData::getOptimizeComponents() const
{
    return m_optimize_components;
}

QString ReactionEnergyData::createUUID()
{
    QByteArray array;
    QTextStream stream(&array);
    stream << this->m_name << endl;
    stream << this->m_method << endl;
    stream << this->m_basis << endl;
    if (m_optimize_components)
    {
        stream << "true" << endl;
    }
    else
    {
        stream << "false" << endl;
    }
    stream << QString::number(this->m_energy, 'f', 6) << endl;
    stream << m_energy_unit << endl;

    for(auto item: this->m_products)
    {
        stream << QString::number(item.coeff, 'f', 6) << " "
               << item.uuid << endl;
    }
    for(auto item: this->m_reactants)
    {
        stream << QString::number(item.coeff, 'f', 6) << " "
               << item.uuid << endl;
    }
    return QUuid::createUuidV5(QUuid(), array).toString();
}

void ReactionEnergyData::setOptimizeComponents(bool value)
{
    m_optimize_components = value;
}

void ReactionEnergyData::setUnit(const QString &unit)
{
    m_energy_unit = unit;
}








