#ifndef GEOMETRYLISTMODEL_H
#define GEOMETRYLISTMODEL_H

#include <QAbstractTableModel>

QT_BEGIN_NAMESPACE
class QSqlQueryModel;
QT_END_NAMESPACE

class GeometryListModel: public QAbstractTableModel
{
    Q_OBJECT
private:
    QSqlQueryModel *m_model;
public:
    GeometryListModel(QSqlQueryModel* model, QObject* parent = 0);

    // QAbstractItemModel interface
public:
    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;

    // QAbstractItemModel interface
public:
    int columnCount(const QModelIndex &parent) const;
};

#endif // GEOMETRYLISTMODEL_H
