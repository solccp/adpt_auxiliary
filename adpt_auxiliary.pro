TEMPLATE = subdirs

SUBDIRS = libadpt_common libDFTBTestSuiteCommon libDFTBRefDatabase libDFTBRefModels \
    libDFTBOPTInputAdaptor DFTBRefDatabaseManager external_libs/archive/Archive \
    libHSDParser libDFTBPlusAdaptor libSKBuilderCommon libErepfitCommon libSKOptimizerCommon DFTBOPTInputCreator 

!win32 {
  SUBDIRS +=
}


CONFIG += ordered
