#!/usr/bin/env ruby

require 'yaml'

doc = YAML::load_file("orbs.yaml")

orbs_map = {}

doc.each do |elem, orbs|
    orbs_map[elem] = {}
    orbs.flatten.each do |orb|
        qn = orb[0..0].to_i
        ql = orb[1..1]
        orbs_map[elem][ql] = orb
    end
end

File.open("data.csv") do |file|
    header = file.gets.chomp.split(",")

    map = {}
    while line = file.gets
        arr = line.chomp.split(",")

        elem = arr[0]

        ns = arr[1].to_i 
        np = arr[2].to_i 
        nd = arr[3].to_i 
        nf = arr[4].to_i 
        
        es = arr[5].sub(/a/,'').to_f
        ep = arr[6].sub(/a/,'').to_f
        ed = arr[7].sub(/a/,'').to_f
        ef = arr[8].sub(/a/,'').to_f
       
#        us = arr[9].to_f 
#        up = arr[10].to_f 
#        ud = arr[11].to_f 
#        uf = arr[12].to_f 

        uds = arr[13].sub(/a/,'').to_f if arr[13]
        udp = arr[14].sub(/a/,'').to_f if arr[14]
        udd = arr[15].sub(/a/,'').to_f if arr[15]
        udf = arr[16].sub(/a/,'').to_f if arr[16]
        
        map[elem] = {}
    
        os = orbs_map[elem]["s"]
        op = orbs_map[elem]["p"]
        od = orbs_map[elem]["d"]
        of = orbs_map[elem]["f"]

        map[elem][os] = uds if os
        map[elem][op] = udp if op
        map[elem][od] = udd if od
        map[elem][of] = udf if of

#        p elem, os, op, od, of

    end 
    puts YAML.dump( map)

end

