#!/usr/bin/env ruby

require 'mysql'
require 'yaml'


begin
    con = Mysql.new 'qcl.ac.nctu.edu.tw', 'dftbrefdata_rw', 'quantum112', 'dftb_ref_data'

    files = `ls confining/*.yaml`.split
    
    files.each do |filename|
        doc = YAML::load_file(filename)
        doc["confining"].each do |elem, content|
            content.each do |type, confining|
                w = confining["w"]
                a = confining["a"]
                r = confining["r"]
                con.query("INSERT INTO `electronic_fitting_default_confining` (element, type, w, a, r) VALUES ('#{elem}', '#{type}', '#{w}', '#{a}', '#{r}');")
            end
        end
    end
rescue Mysql::Error => e
    puts e.errno
    puts e.error
    
ensure
    con.close if con
end
