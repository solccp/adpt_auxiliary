#include "bandstructureviewer.h"

#include <QGridLayout>
#include <QMessageBox>
#include <QFileDialog>
#include <QtConcurrent/QtConcurrent>

#include "bandstructuredata.h"
#include "dopwidget.h"
#include "BSOptTargetIndexBasedModel.h"

BandStructureWidget::BandStructureWidget(QWidget *parent) :
    QWidget(parent)
{
    this->resize(600,400);

    m_model = NULL;

    m_tableView = new QTableView(this);

    butImportPlainFile = new QPushButton(tr("Load BandStructure(nxy) data file"));
    butClear = new QPushButton(tr("Clear"));

    butExportPlainText = new QPushButton(tr("Export"));
    QObject::connect(butImportPlainFile, SIGNAL(clicked()), this, SLOT(importPlainInput()));
    QObject::connect(butExportPlainText, SIGNAL(clicked()), this, SLOT(exportPlainText()));
    QObject::connect(butClear, SIGNAL(clicked()), this, SLOT(clear()));

//    butPreview = new QPushButton(tr("Preview"));
//    QObject::connect(butPreview, SIGNAL(clicked()), this, SLOT(preview()));

    butImportPlainFile->setAutoDefault(false);
    butClear->setAutoDefault(false);
//    butPreview->setAutoDefault(false);

    butShowFittingArea = new QPushButton(tr("Fitting area..."));
    butShowFittingArea->setAutoDefault(false);
    connect(butShowFittingArea, SIGNAL(clicked()), this, SLOT(showFittingArea()));


    QGridLayout *grid = new QGridLayout(this);
    grid->addWidget(butImportPlainFile,0,0,1,1);
    grid->addWidget(butExportPlainText,0,1,1,1);
    grid->addWidget(butClear,0,2,1,1);
//    grid->addWidget(butPreview,0,3,1,1);
    grid->addWidget(butShowFittingArea,0,4,1,1);
    grid->addWidget(m_tableView,1,0,1,5);


    if (m_customplot)
    {
        delete m_customplot;
    }
    m_customplot = new QCustomPlot();

    m_customplot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectAxes);
    m_customplot->axisRect()->setupFullAxesBox();
    connect(m_customplot->xAxis, SIGNAL(rangeChanged(QCPRange)), m_customplot->xAxis2, SLOT(setRange(QCPRange)));
    connect(m_customplot->yAxis, SIGNAL(rangeChanged(QCPRange)), m_customplot->yAxis2, SLOT(setRange(QCPRange)));
    m_customplot->axisRect()->setRangeDrag(Qt::Vertical);
    m_customplot->axisRect()->setRangeZoom(Qt::Vertical);

    m_customplot->xAxis->setLabel("k-vector");
    m_customplot->yAxis->setLabel("Energy[eV]");
    m_customplot->legend->setVisible(true);
    QFont legendFont = font();
    legendFont.setPointSize(10);
    m_customplot->legend->setFont(legendFont);
    m_customplot->legend->setSelectedFont(legendFont);

}

void BandStructureWidget::importPlainInput()
{
    QString filename = QFileDialog::getOpenFileName(
           this,
           tr("Import BandStructure data file"),
           QDir::homePath(),
           tr("Data files (*.*)") );
    if (!filename.isNull())
    {
        if (!m_model->importPlainFile(filename))
        {
            QMessageBox qbox(this);
            qbox.setWindowTitle(tr("DFTBRefDatabaseManager"));
            qbox.setText(tr("Plain data file parsing error. Cancelled."));
            qbox.setStandardButtons(QMessageBox::Ok);
            qbox.setIcon(QMessageBox::Critical);
            qbox.exec();
        }
//        prepareGraph();
//        replotGraph();
    }
}

void BandStructureWidget::clear()
{
    if (m_model != NULL)
    {
        m_model->clear();
        cur_EF = 0.0;
    }
}

void BandStructureWidget::showFittingArea()
{
    QDialog dia;
    dia.setModal(true);
    dia.resize(800,400);
    QGridLayout *vb =new QGridLayout();


    BandstructureData* newdata = new BandstructureData();
    newdata->setDefault_optimization(m_model->getBandStructureData()->default_optimization());

    DOPWidget* e = new DOPWidget(newdata);
    e->setEditable(m_editable);

    vb->addWidget(e,0,0,1,6);

    QPushButton *butOK = new QPushButton(tr("OK"));


    if (m_editable)
    {
        QPushButton *butCancel = new QPushButton(tr("Cancel"));
        connect(butOK, SIGNAL(clicked()), &dia, SLOT(accept()));
        connect(butCancel, SIGNAL(clicked()), &dia, SLOT(reject()));
        vb->addWidget(butOK, 1,4,1,1);
        vb->addWidget(butCancel, 1,5,1,1);
    }
    else
    {
        connect(butOK, SIGNAL(clicked()), &dia, SLOT(reject()));
        vb->addWidget(butOK, 1,5,1,1);
    }

    dia.setLayout(vb);

    int ret = dia.exec();
    if (ret == QDialog::Accepted)
    {
        m_model->getBandStructureData()->setDefault_optimization(newdata->default_optimization());
        this->replotGraph();
    }
    return ;
}

void BandStructureWidget::exportPlainText()
{
    QString filename = QFileDialog::getSaveFileName(
           this,
           tr("Export BandStructure data file"),
           QDir::homePath(),
           tr("Data files (*.*)") );
    if (!filename.isNull())
    {
        if (m_model->exportPlainText(filename))
        {
            QMessageBox qbox(this);
            qbox.setWindowTitle(tr("DFTBRefDatabaseManager"));
            qbox.setText(tr("Data exported to ") + filename);
            qbox.setStandardButtons(QMessageBox::Ok);
            qbox.setIcon(QMessageBox::Information);
            qbox.exec();
        }
    }
}


void BandStructureWidget::replotGraph()
{

    int npoints = m_model->rowCount(QModelIndex());
    int nbands  = m_model->columnCount(QModelIndex());

    if (nbands == 0 || npoints == 0)
    {
        m_customplot->replot();
        return;
    }

    const BandstructureData *bsData = m_model->getBandStructureData();
    QPair<int, int> fermiIndex = bsData->getFermiIndex();

    double EF = 0.0;
    if (fermiIndex.first > -1 && fermiIndex.first < nbands
            && fermiIndex.second > -1 && fermiIndex.second < npoints)
    {
        EF = m_model->getData(fermiIndex.first, fermiIndex.second);
    }

    shiftData(EF);
    m_customplot->yAxis->setRange(y_min-cur_EF, -y_min);


    for(int i=m_customplot->graphCount()-1;i>= nbands; --i)
    {
        m_customplot->removeGraph(m_customplot->graph(i));
    }

    for(int k=0; k<m_model->getBandStructureData()->default_optimization().size();++k)
    {
        BSOptTargetInfo info = m_model->getBandStructureData()->default_optimization().at(k);

        QPen graphPen;
        int colorIndex = (k % colors.size());
        graphPen.setColor(QColor(colors[colorIndex]));

        if (info.index_based)
        {
            while(info.band_startindex < 0)
            {
                info.band_startindex += nbands;
            }
            while(info.band_endindex < 0)
            {
                info.band_endindex += nbands;
            }
            while(info.point_startindex <0)
            {
                info.point_startindex += npoints;
            }
            while(info.point_endindex <0)
            {
                info.point_endindex += npoints;
            }

            for (int i=info.band_startindex; i<=info.band_endindex; ++i)
            {
                m_customplot->addGraph();
                m_customplot->graph()->setPen(graphPen);
                QString name = QString("Fitting target %1").arg(k+1);
                m_customplot->graph()->setName(name);
                if (i!= info.band_startindex)
                {
                    m_customplot->graph()->removeFromLegend();
                }

                QVector<double> xdata, ydata;
                for(int j=info.point_startindex; j<= info.point_endindex; ++j)
                {
                    xdata << j;
                    ydata << m_model->getData(i,j)-EF;
                }

                m_customplot->graph()->addData(xdata, ydata);
            }
        }
        else
        {
            int pointcount = m_model->rowCount(QModelIndex());
            int bandcount = m_model->columnCount(QModelIndex());
            for(int band = 0; band < bandcount; ++band)
            {
                QVector<double> xdata, ydata;

                m_customplot->addGraph();
                m_customplot->graph()->setPen(graphPen);
                QString name = QString("Fitting target %1").arg(k+1);
                m_customplot->graph()->setName(name);

                if (band!= 0)
                {
                    m_customplot->graph()->removeFromLegend();
                    m_customplot->graph()->setLineStyle(QCPGraph::lsNone);
                    m_customplot->graph()->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, 1));
                }

                for(int point = 0; point < pointcount ; ++point)
                {
                    double val = m_model->getData(band, point)-EF;
                    if (info.energy_lowerbound <= val && info.energy_upperbound >= val)
                    {
                        xdata << point;
                        ydata << val;
                    }
                }

                m_customplot->graph()->addData(xdata, ydata);
            }

        }
    }

    m_customplot->replot();
}

void BandStructureWidget::prepareGraph()
{
    int npoints = m_model->rowCount(QModelIndex());
    int nbands  = m_model->columnCount(QModelIndex());

    m_customplot->clearPlottables();

    m_banddata.clear();

    y_min = 0.0;
    for (int i=0; i<nbands; ++i)
    {
        QCPDataMap* dataMap = new QCPDataMap();

        for(int j=0; j<npoints; ++j)
        {
            double value = m_model->getData(i,j);
            if (value < y_min)
                y_min = value;
            QCPData newData;
            newData.key = j;
            newData.value = value;
            dataMap->insertMulti(newData.key, newData);
        }

        m_banddata.append(dataMap);
    }


    QPen graphPen;
    graphPen.setColor(Qt::black);

    for(int i=0; i< m_banddata.size();++i)
    {
        m_customplot->addGraph();
        m_customplot->graph()->setName("Reference");
        m_customplot->graph()->setPen(graphPen);
        if (i!=0)
        {
            m_customplot->graph()->removeFromLegend();
        }
        m_customplot->graph()->setData(m_banddata[i]);
    }

    y_min *=1.05;
    if (y_min > 0)
        y_min = -y_min;


    m_customplot->xAxis->setRange(0, npoints-1);
    m_customplot->xAxis->setTickLabels(false);

    replotGraph();
}

void BandStructureWidget::shiftData(double EF)
{
    double delta_EF = EF - cur_EF;
    if (std::abs(delta_EF) < 1.0e-3)
        return;
    for(int i=0; i<m_banddata.size();++i)
    {
        QCPDataMutableMapIterator it(*m_banddata[i]);
        while(it.hasNext())
        {
            it.next();
            QCPData& data = it.value();
            data.value -= delta_EF;
//            it.setValue(data);
        }
    }
    cur_EF = EF;
}


bool BandStructureWidget::editable() const
{
    return m_editable;
}

void BandStructureWidget::setEditable(bool editable)
{
    m_editable = editable;
    butImportPlainFile->setEnabled(editable);
    butClear->setEnabled(editable);
}

BandStructureBandsModel *BandStructureWidget::bandStructureModel() const
{
    return m_model;
}

void BandStructureWidget::setBandStructureModel(BandStructureBandsModel *data)
{
    m_model = data;
    m_tableView->setModel(m_model);
    prepareGraph();
    connect(m_model, SIGNAL(dataChanged()), this, SLOT(prepareGraph()));
}

QCustomPlot *BandStructureWidget::getPreviewPlot() const
{
    return m_customplot;
}

