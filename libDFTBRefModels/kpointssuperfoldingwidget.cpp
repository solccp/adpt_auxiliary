#include "kpointssuperfoldingwidget.h"

#include "kpointssupercellfoldingmodel.h"

KPointsSuperFoldingWidget::KPointsSuperFoldingWidget(QWidget *parent) : QWidget(parent)
{
    this->resize(480,400);

    m_tableView = new QTableView(this);
    m_tableView->setSelectionBehavior(QAbstractItemView::SelectRows);


    butImportPlainFile = new QPushButton(tr("Import plaintext input"));
    butImportPlainFile->setAutoDefault(false);
    butImportHSDFile = new QPushButton(tr("Import HSD input"));
    butImportHSDFile->setAutoDefault(false);
    QObject::connect(butImportPlainFile, SIGNAL(clicked()), this, SLOT(importPlainInput()));
    QObject::connect(butImportHSDFile, SIGNAL(clicked()), this, SLOT(importHSDInput()));


    QVBoxLayout *vbox = new QVBoxLayout();
    vbox->setMargin(0);
    QGridLayout *buttonGrid = new QGridLayout();
    buttonGrid->addWidget(butImportPlainFile,0,0,1,1);
    buttonGrid->addWidget(butImportHSDFile,0,1,1,1);
    buttonBox = new QGroupBox;
    buttonBox->setLayout(buttonGrid);

    vbox->addWidget(buttonBox);
    vbox->addWidget(m_tableView);

    this->setLayout(vbox);
}

KPointsSupercellFoldingModel *KPointsSuperFoldingWidget::getModel() const
{
    return m_model;
}

void KPointsSuperFoldingWidget::setModel(KPointsSupercellFoldingModel *model)
{
    m_model = model;
    m_tableView->setModel(model);
}

void KPointsSuperFoldingWidget::setEditable(bool enable)
{
    if (enable)
    {
        buttonBox->show();
    }
    else
    {
        buttonBox->hide();
    }
}

void KPointsSuperFoldingWidget::importPlainInput()
{
    QString filename = QFileDialog::getOpenFileName(
           this,
           tr("Import KLines data file"),
           QDir::homePath(),
           tr("Data files (*)") );
    if (!filename.isNull())
    {
        if (!m_model->importPlainFile(filename))
        {
            QMessageBox qbox(this);
            qbox.setWindowTitle(tr("DFTBRefDatabaseManager"));
            qbox.setText(tr("Plain data file parsing error. Cancelled."));
            qbox.setStandardButtons(QMessageBox::Ok);
            qbox.setIcon(QMessageBox::Critical);
            qbox.exec();
        }
    }
}

void KPointsSuperFoldingWidget::importHSDInput()
{
    QString filename = QFileDialog::getOpenFileName(
           this,
           tr("Import KLines data file"),
           QDir::homePath(),
           tr("HSD files (*)") );
    if (!filename.isNull())
    {
        if (!m_model->importHSDFile(filename))
        {
            QMessageBox qbox(this);
            qbox.setWindowTitle(tr("DFTBRefDatabaseManager"));
            qbox.setText(tr("HSD data file parsing error. Cancelled."));
            qbox.setStandardButtons(QMessageBox::Ok);
            qbox.setIcon(QMessageBox::Critical);
            qbox.exec();
        }
    }
}


