#include "reactioncomponentdelegate.h"
#include "dftbrefdatabase.h"
#include <QComboBox>
#include <QDebug>

ReactionComponentDelegate::ReactionComponentDelegate(DFTBRefDatabase *database, QObject *parent) :
    QStyledItemDelegate(parent)
{
    m_database = database;
}

void ReactionComponentDelegate::changed()
{
    QComboBox* editor = qobject_cast<QComboBox*>(sender());
    emit commitData(editor);
    emit closeEditor(editor);
}

QWidget *ReactionComponentDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(index);
    Q_UNUSED(option);
    QComboBox *cb = new QComboBox(parent);
    cb->setEditable(false);
    connect(cb, SIGNAL(currentIndexChanged(int)), this, SLOT(changed()));
    return cb;
}

void ReactionComponentDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    QAbstractTableModel *model = m_database->getGeometryListModel(DFTBRefDatabase::Both);
    if(QComboBox *cb = qobject_cast<QComboBox *>(editor))
    {
        for (int i=0; i<model->rowCount(); ++i)
        {
            cb->addItem(model->data(model->index(i,1)).toString(), model->data(model->index(i,0)));
        }

        cb->setCurrentText(index.data().toString());
        cb->showPopup();
    }
}

void ReactionComponentDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    if(QComboBox *cb = qobject_cast<QComboBox *>(editor))
    {
        model->setData(index,cb->currentData());
    }
}

void ReactionComponentDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(index);
    QRect rect = option.rect;
    QSize sizeHint = editor->sizeHint();

    if (rect.width()<sizeHint.width()) rect.setWidth(sizeHint.width());
    if (rect.height()<sizeHint.height()) rect.setHeight(sizeHint.height());

    editor->setGeometry(rect);
}
