#ifndef MOLECULARFREQUENCYEDITOR_H
#define MOLECULARFREQUENCYEDITOR_H

#include <QWidget>
#include <QAbstractListModel>
#include <QComboBox>
#include <QTableView>
#include <QStandardItemModel>


#include "SingleSelectTableModelComboBox.h"

class MolecularFrequencyData;
class MolecularFrequencyDataEditor : public QWidget
{
    Q_OBJECT
public:
    MolecularFrequencyDataEditor(MolecularFrequencyData* bandstructure, QAbstractTableModel* geom_ids,
                                 bool ReadOnly = false, QWidget *parent = 0);

signals:

public slots:
private slots:
    void setupUI();
    void setGeomID(const QString& id);
    void capitalize();
    void setMethod();
    void setBasis();
    void editComments();
    void editCitations();
    void itemChanged(QStandardItem* item);
    void moveUp();
    void moveDown();
    void addRow();
    void removeRow();
    void freqRowMoved();
private:
    MolecularFrequencyData* m_data;
    QAbstractTableModel* m_geom_ids;
    SingleSelectTableModelComboBox *comboRef;
    QLineEdit *m_editBasis;
    QLineEdit *m_editMethod;
    QTableView *m_view;
    QStandardItemModel *m_model;

    bool m_ReadOnly = false;
private:
    bool showTextEditor(QWidget *parent, const QString &title, QString &variable);
};

#endif // MOLECULARFREQUENCYEDITOR_H
