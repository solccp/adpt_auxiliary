#include "UpperCaseLineEdit.h"

#include <QKeyEvent>

#include <QDebug>

UpperCaseLineEdit::UpperCaseLineEdit(QWidget *parent) :
    QLineEdit(parent)
{
}

void UpperCaseLineEdit::keyPressEvent(QKeyEvent *event)
{
    if(event->key() >= Qt::Key_A && event->key() <= Qt::Key_Z &&
                ((event->modifiers() & Qt::ShiftModifier) == false))
    {

        QLineEdit::keyPressEvent(new QKeyEvent(QEvent::KeyPress,
            event->key(), event->modifiers() | Qt::ShiftModifier,
            event->text().toUpper()));
    }
    else
    {
        QLineEdit::keyPressEvent(event);
    }
}
