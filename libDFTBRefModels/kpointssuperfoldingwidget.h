#ifndef KPOINTSSUPERFOLDINGWIDGET_H
#define KPOINTSSUPERFOLDINGWIDGET_H

#include <QWidget>
#include <QtWidgets>

class KPointsSupercellFoldingModel;
class KPointsSuperFoldingWidget : public QWidget
{
    Q_OBJECT
public:
    explicit KPointsSuperFoldingWidget(QWidget *parent = 0);
public slots:
    KPointsSupercellFoldingModel* getModel() const;
public:
    void setModel(KPointsSupercellFoldingModel* model);
    void setEditable(bool enable);
private slots:
    void importPlainInput();
    void importHSDInput();
private:
    QTableView *m_tableView;
    KPointsSupercellFoldingModel* m_model;
    QPushButton *butImportPlainFile;
    QPushButton *butImportHSDFile;
    QGroupBox *buttonBox;
};

#endif // KPOINTSSUPERFOLDINGWIDGET_H
