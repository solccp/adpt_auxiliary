#include "kpointsplainwidget.h"

#include "kpointsplainmodel.h"

KPointsPlainWidget::KPointsPlainWidget(QWidget *parent) : QWidget(parent)
{
    this->resize(480,400);

    m_tableView = new QTableView(this);
    m_tableView->setSelectionBehavior(QAbstractItemView::SelectRows);

    butAddRow = new QPushButton(tr("Add row"));
    butAddRow->setAutoDefault(false);
    butRemoveRow = new QPushButton(tr("Delete row"));
    butRemoveRow->setAutoDefault(false);
    QObject::connect(butAddRow, SIGNAL(clicked()), this, SLOT(addRow()));
    QObject::connect(butRemoveRow, SIGNAL(clicked()), this, SLOT(removeRow()));
    butImportPlainFile = new QPushButton(tr("Import plaintext input"));
    butImportPlainFile->setAutoDefault(false);
    butImportHSDFile = new QPushButton(tr("Import HSD input"));
    butImportHSDFile->setAutoDefault(false);
    QObject::connect(butImportPlainFile, SIGNAL(clicked()), this, SLOT(importPlainInput()));
    QObject::connect(butImportHSDFile, SIGNAL(clicked()), this, SLOT(importHSDInput()));


    QVBoxLayout *vbox = new QVBoxLayout();
    vbox->setMargin(0);
    QGridLayout *buttonGrid = new QGridLayout();
    buttonGrid->addWidget(butImportPlainFile,0,0,1,1);
    buttonGrid->addWidget(butImportHSDFile,0,1,1,1);
    buttonGrid->addWidget(butAddRow,1,0,1,1);
    buttonGrid->addWidget(butRemoveRow,1,1,1,1);
    buttonBox = new QGroupBox;
    buttonBox->setLayout(buttonGrid);

    vbox->addWidget(buttonBox);
    vbox->addWidget(m_tableView);

    this->setLayout(vbox);
}

KPointsPlainModel *KPointsPlainWidget::getModel() const
{
    return m_model;
}

void KPointsPlainWidget::setModel(KPointsPlainModel *model)
{
    m_model = model;
    m_tableView->setModel(model);
}

void KPointsPlainWidget::setEditable(bool enable)
{
    if (enable)
    {
        buttonBox->show();
    }
    else
    {
        buttonBox->hide();
    }
}



void KPointsPlainWidget::addRow()
{
    m_tableView->model()->insertRows(m_tableView->model()->rowCount(), 1, QModelIndex());
    m_tableView->setFocus();
    m_tableView->selectRow(m_tableView->model()->rowCount());
}

void KPointsPlainWidget::removeRow()
{
    QModelIndexList list = m_tableView->selectionModel()->selectedRows();
    if (!list.empty())
    {
        for (int i=list.size()-1; i>=0; --i)
        {
            int row = list.at(i).row();
            m_tableView->model()->removeRows(row,1);
        }
    }
}

void KPointsPlainWidget::importPlainInput()
{
    QString filename = QFileDialog::getOpenFileName(
           this,
           tr("Import KLines data file"),
           QDir::homePath(),
           tr("Data files (*)") );
    if (!filename.isNull())
    {
        if (!m_model->importPlainFile(filename))
        {
            QMessageBox qbox(this);
            qbox.setWindowTitle(tr("DFTBRefDatabaseManager"));
            qbox.setText(tr("Plain data file parsing error. Cancelled."));
            qbox.setStandardButtons(QMessageBox::Ok);
            qbox.setIcon(QMessageBox::Critical);
            qbox.exec();
        }
    }
}

void KPointsPlainWidget::importHSDInput()
{
    QString filename = QFileDialog::getOpenFileName(
           this,
           tr("Import KLines data file"),
           QDir::homePath(),
           tr("HSD files (*)") );
    if (!filename.isNull())
    {
        if (!m_model->importHSDFile(filename))
        {
            QMessageBox qbox(this);
            qbox.setWindowTitle(tr("DFTBRefDatabaseManager"));
            qbox.setText(tr("HSD data file parsing error. Cancelled."));
            qbox.setStandardButtons(QMessageBox::Ok);
            qbox.setIcon(QMessageBox::Critical);
            qbox.exec();
        }
    }
}
