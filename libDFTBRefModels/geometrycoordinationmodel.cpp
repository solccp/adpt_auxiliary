#include "geometrycoordinationmodel.h"
#include "geometry.h"
#include "atomicproperties.h"

#include <array>

#include <openbabel/obconversion.h>
#include <openbabel/mol.h>

#include <QStringList>
#include <QTextStream>
#include <QRegExp>




GeometryCoordinationModel::GeometryCoordinationModel(Geometry *geom, QObject *parent) :
    QAbstractTableModel(parent), m_geom(geom)
{
//    connect(this, SIGNAL(modelChanged()), m_geom, SLOT(computeFormula()));
}

void GeometryCoordinationModel::addAtom(const QString &Symbol, double x, double y, double z)
{
    beginInsertRows(QModelIndex(),m_geom->coordinates().size(), m_geom->coordinates().size());
    Coordinate newatom;
    newatom.symbol = Symbol;
    newatom.coord[0] = x;
    newatom.coord[1] = y;
    newatom.coord[2] = z;
    m_geom->coordinates().append(newatom);
    endInsertRows();
    if (!beingModified)
    {
        emit modelChanged();
        m_geom->computeFormula();
        emit formulaChanged(m_geom->getFormula());
    }
}

bool GeometryCoordinationModel::loadXYZGeometry(QTextStream &stream, QString& metadata)
{
    bool ok;
    int nat;

    nat = stream.readLine().toInt(&ok);
    if (!ok)
        return false;
    metadata = stream.readLine();
    QList<QString> symbols;
    QList<double> xs, ys, zs;

    for(int i=0; i<nat; ++i)
    {
        if (stream.atEnd())
            return false;
        QString line = stream.readLine();

        QRegExp rx("(\\ |\\,|\\t)");
        QStringList array = line.split(rx, QString::SkipEmptyParts);
        const ADPT::AtomicProperties* symbol = ADPT::AtomicProperties::fromSymbol(array.at(0));
        if (symbol == &ADPT::AtomicProperties::ERROR_ATOM)
        {
            return false;
        }
        double x = array.at(1).toDouble(&ok);
        if (!ok)
            return false;
        double y = array.at(2).toDouble(&ok);
        if (!ok)
            return false;
        double z = array.at(3).toDouble(&ok);
        if (!ok)
            return false;
        xs.append(x);
        ys.append(y);
        zs.append(z);
        symbols.append(symbol->getSymbol());
    }
    if (!symbols.empty())
    {
        this->clear();
        BeginModify();
        for(int i=0; i<symbols.size(); ++i)
        {
            addAtom(symbols.at(i),xs.at(i),ys.at(i),zs.at(i));
        }
        EndModify();
        m_geom->computeFormula();
        emit formulaChanged(m_geom->getFormula());
        return true;
    }
    return false;
}

bool GeometryCoordinationModel::loadGaussianOutputGeometry(QTextStream &stream)
{
    using namespace OpenBabel;
    OBConversion conv;
    conv.SetInFormat("g09");
    std::stringstream str;
    while(!stream.atEnd())
    {
        QString line = stream.readLine();
        if (!line.trimmed().startsWith("Entering Gaussian System"))
        {
            continue;
        }
        str << line.toStdString() << std::endl;
        break;
    }
    str << stream.readAll().toStdString();
    conv.SetInStream(&str);
    OBMol mol;
    if(conv.Read(&mol))
    {
        m_geom->setCharge(mol.GetTotalCharge());
        m_geom->setSpin(mol.GetTotalSpinMultiplicity());
        this->clear();
        BeginModify();
        FOR_ATOMS_OF_MOL(a, mol)
        {
            addAtom(ADPT::AtomicProperties::fromAtomicNumber(a->GetAtomicNum())->getSymbol(),
                                     a->x(),a->y(), a->z());
        }
        EndModify();
        m_geom->computeFormula();
        emit formulaChanged(m_geom->getFormula());
        return true;
    }
    return false;
}

bool GeometryCoordinationModel::loadORCAOutputGeometry(QTextStream &stream)
{
    return false;
}

bool GeometryCoordinationModel::loadGenGeometry(QTextStream &stream)
{
    if (stream.atEnd())
        return false;
    QString Line = stream.readLine();
    QStringList array = Line.split(" ", QString::SkipEmptyParts);
    bool isPBC = false;
    bool fractional = false;

    if (array.empty())
    {
        return false;
    }
    if (array.size() > 1 && (array.at(1).toUpper() == "S" || array.at(1).toUpper() == "F") )
    {
        if (array.at(1).toUpper() == "F")
            fractional = true;
        isPBC = true;
    }
    bool ok;
    int nat = array.at(0).toInt(&ok);
    if (!ok)
    {
        return false;
    }
    if (stream.atEnd())
        return false;
    QStringList elementList = stream.readLine().split(" ", QString::SkipEmptyParts);
    QList<QString> elementListReal;
    for(int i=0; i<elementList.size();++i)
    {
         const ADPT::AtomicProperties* atom = ADPT::AtomicProperties::fromSymbol(elementList.at(i).toLower());
         if (atom != &ADPT::AtomicProperties::ERROR_ATOM)
         {
             elementListReal.append(atom->getSymbol());
         }
         else
         {
             return false;
         }
    }
    QList<QString> symbols;
    QList<std::array<double,3> > coords;
    dMatrix3x3 mat;

    for(int i=0; i<nat; ++i)
    {
        if (stream.atEnd())
            return false;
        QStringList arr = stream.readLine().split(" ", QString::SkipEmptyParts);
        if (arr.size() < 5)
            return false;
        int elementIndex = arr.at(1).toInt(&ok);
        if (!ok)
            return false;
        double x = arr.at(2).toDouble(&ok);
        if (!ok)
            return false;
        double y = arr.at(3).toDouble(&ok);
        if (!ok)
            return false;
        double z = arr.at(4).toDouble(&ok);
        if (!ok)
            return false;
        symbols.append(elementListReal.at(elementIndex-1));
        coords.append(std::array<double, 3>{{x,y,z}});
    }
    if (isPBC)
    {
        if (stream.atEnd())
            return false;
        Line = stream.readLine();
        QStringList array = Line.split(" ", QString::SkipEmptyParts);
        if (array.size() < 3)
            return false;


        for(int i=0; i<3; ++i)
        {
            if (stream.atEnd())
                return false;
            Line = stream.readLine();
            array = Line.split(" ", QString::SkipEmptyParts);
            if (array.size() < 3)
                return false;

            double x = array.at(0).toDouble(&ok);
            if (!ok)
                return false;
            double y = array.at(1).toDouble(&ok);
            if (!ok)
                return false;
            double z = array.at(2).toDouble(&ok);
            if (!ok)
                return false;


            mat[i][0] = x;
            mat[i][1] = y;
            mat[i][2] = z;
        }

    }
    if (!coords.empty())
    {
        if (isPBC)
        {
            m_geom->setLattice_vectors(mat);
            m_geom->setPBC(true);
            m_geom->setRelativeCoordinates(fractional);
        }
        else
        {
            m_geom->setPBC(false);
        }
        this->clear();
        BeginModify();
        for(int i=0; i<symbols.size(); ++i)
        {
            this->addAtom(symbols.at(i),coords.at(i)[0],coords.at(i)[1],coords.at(i)[2]);
        }
        EndModify();
        m_geom->computeFormula();
        emit formulaChanged(m_geom->getFormula());
        return true;
    }
    return false;
}


///todo: fix loading lv
bool GeometryCoordinationModel::loadHSDGeometry(QTextStream &stream)
{


    QString str = stream.readAll();
    QRegExp rx_gen(".*Geometry\\s*=\\s*GenFormat\\s*\\{([^\\}]*)\\}.*", Qt::CaseInsensitive);
    QRegExp rx_geom(".*Geometry\\s*=\\s*\\{(.*)\\}.*", Qt::CaseInsensitive);

    bool fraction = false;
    bool isPBC = false;


    if (rx_gen.exactMatch(str) && rx_gen.capturedTexts().size() > 1)
    {
        QString genstr = rx_gen.capturedTexts().at(1).trimmed();
        QTextStream stream_gen(&genstr);
        return loadGenGeometry(stream_gen);
    }
    else if (rx_geom.exactMatch(str))
    {
        QRegExp rx_periodic(".*Geometry\\s*=\\s*\\{.*Periodic\\s*=\\s*(Yes|No).*", Qt::CaseInsensitive);
        int index = rx_periodic.indexIn(str);
        if (index > -1 && rx_periodic.capturedTexts().size() > 1)
        {
            if (rx_periodic.capturedTexts().at(1).toLower() == "yes" )
            {
                isPBC = true;
            }
            else
            {
                isPBC = false;
            }
        }
        else
        {
            isPBC = false;
        }
///fixme
//        QRegExp rx_lv(".*Geometry\\s*=\\s*\\{.*LatticeVectors\\s*(\\[(.*)\\])?\\s*=\\s*\\{([^\\}]*)\\}", Qt::CaseInsensitive);
        QRegExp rx_names(".*Geometry\\s*=\\s*\\{.*TypeNames\\s*=\\s*\\{([^\\}]*)\\}", Qt::CaseInsensitive);

        index = rx_names.indexIn(str);
        QString names = rx_names.capturedTexts().at(1);
        names.replace('"',"");
        QStringList typenames = names.split(" ", QString::SkipEmptyParts);

        QList<QString> elementListReal;
        for(int i=0; i<typenames.size();++i)
        {
             const ADPT::AtomicProperties* atom = ADPT::AtomicProperties::fromSymbol(typenames.at(i).toLower());
             if (atom != &ADPT::AtomicProperties::ERROR_ATOM)
             {
                 elementListReal.append(atom->getSymbol());
             }
             else
             {
                 return false;
             }
        }

        QRegExp rx_coords(".*Geometry\\s*=\\s*\\{.*TypesAndCoordinates\\s*(\\[\\s*(\\w*)\\s*\\])?\\s*=\\s*\\{([^\\}]*)\\}", Qt::CaseInsensitive);
        index = rx_coords.indexIn(str);


        QList<QString> symbols;
        QList<std::array<double,3> > coords;
        bool ok;
        if (index > -1 && rx_coords.capturedTexts().size() == 4)
        {
            QString modifier = rx_coords.capturedTexts().at(2).toLower();
            if (modifier == "relative")
            {
                fraction = true;
            }
            QTextStream genin(rx_coords.capturedTexts().at(3).trimmed().toLatin1());
            while(!genin.atEnd())
            {
                QString line = genin.readLine();
                QStringList arr = line.split(" ",QString::SkipEmptyParts);
                if (arr.size() < 4)
                    return false;
                int elementIndex = arr.at(0).toInt(&ok);
                if (!ok)
                    return false;
                double x = arr.at(1).toDouble(&ok);
                if (!ok)
                    return false;
                double y = arr.at(2).toDouble(&ok);
                if (!ok)
                    return false;
                double z = arr.at(3).toDouble(&ok);
                if (!ok)
                    return false;
                symbols.append(elementListReal.at(elementIndex-1));
                coords.append(std::array<double, 3>{{x,y,z}});

            }
        }

        if (!coords.empty())
        {
            this->clear();
            m_geom->setPBC(isPBC);
            m_geom->setRelativeCoordinates(fraction);
            BeginModify();
            for(int i=0; i<symbols.size();++i)
            {
                addAtom(symbols.at(i),coords.at(i)[0],coords.at(i)[1],coords.at(i)[2]);
            }
            EndModify();
            m_geom->computeFormula();
            emit formulaChanged(m_geom->getFormula());
            return true;
        }
        return false;

    }

    return false;
}

bool GeometryCoordinationModel::loadPOSCAR(QTextStream &stream, QString* name, bool* assumed_elements)
{
    *assumed_elements = false;
    QString title = stream.readLine();
    *name = title;
    double sf = 0.0;
    QString line = stream.readLine();
    QStringList arr = line.split(" ", QString::SkipEmptyParts);
    sf = arr[0].toDouble();
    std::array<std::array<double, 3>, 3> lv;
    for(int i=0; i<3;++i)
    {
        line = stream.readLine();
        arr = line.split(" ", QString::SkipEmptyParts);
        if (arr.size()==3)
        {
            for(int j=0; j<3;++j)
            {
                bool ok;
                lv[i][j] = arr[j].toDouble(&ok);
                if (!ok)
                    return false;
            }
        }
        else
        {
            return false;
        }
    }
    line = stream.readLine();
    arr = line.split(" ", QString::SkipEmptyParts);
    int dummy;
    bool ok;
    dummy = arr[0].toInt(&ok);
    bool HasSymbolAbove = false;
    QStringList symbols;
    QList<int> num_elements;
    if (!ok)
    {
        HasSymbolAbove = true;
        symbols = arr;
        line = stream.readLine();
        arr = line.split(" ", QString::SkipEmptyParts);
    }
    int nat = 0;
    for(int i=0; i<arr.size();++i)
    {
        int num = arr[i].toInt(&ok);
        if (!ok)
        {
            return false;
        }
        num_elements.append(num);
        nat += num;
    }


    QString mode = stream.readLine();
    bool fractional = false;
    if (mode.startsWith("D") || mode.startsWith("d"))
    {
        fractional= true;
    }
    else if (mode.startsWith("C") || mode.startsWith("c"))
    {
        fractional = false;
    }
    else
    {
        return false;
    }

    QList<QString> final_symbols;
    QList<std::array<double,3> > coords;
    for(int iEl=0; iEl<num_elements.size();++iEl)
    {
        for(int i=0; i<num_elements[iEl];++i)
        {
            line = stream.readLine();
            arr = line.split(" ", QString::SkipEmptyParts);
            if (HasSymbolAbove)
            {
                if ( arr.size() < 3)
                {
                    return false;
                }
                QString elem = symbols[iEl];
                final_symbols.append(elem);
                std::array<double,3> coord ;
                for(int j=0; j<3;++j)
                {
                    bool pok;

                    double v = arr[j].toDouble(&pok);
                    if (!pok)
                    {
                        return false;
                    }
                    coord[j] = v;
                }
                coords.append(coord);
            }
            else
            {
                QString elem;
                if (arr.size() < 3)
                {
                    return false;
                }
                if ( arr.size() >= 4)
                {
                    elem = arr[3];
                }
                else
                {
                    elem = "C";
                    *assumed_elements = true;
                }

                final_symbols.append(elem);
                std::array<double,3> coord ;
                for(int j=0; j<3;++j)
                {
                    bool pok;

                    double v = arr[j].toDouble(&pok);
                    if (!pok)
                    {
                        return false;
                    }
                    coord[j] = v;
                }
                coords.append(coord);
            }
        }
    }

    if (!coords.empty())
    {
        this->clear();
        m_geom->setPBC(true);
        m_geom->setScaling_factor(sf);
        m_geom->setLattice_vectors(lv);
        m_geom->setRelativeCoordinates(fractional);
        BeginModify();
        for(int i=0; i<final_symbols.size();++i)
        {
            addAtom(final_symbols.at(i),coords.at(i)[0],coords.at(i)[1],coords.at(i)[2]);
        }
        EndModify();
        m_geom->computeFormula();
        emit formulaChanged(m_geom->getFormula());
        return true;
    }
    return false;


}

void GeometryCoordinationModel::clear()
{
    beginResetModel();
    m_geom->coordinates().clear();
    m_geom->computeFormula();
    emit formulaChanged(m_geom->getFormula());
    endResetModel();
    if (!beingModified)
        emit modelChanged();
}

void GeometryCoordinationModel::BeginModify()
{
    beingModified = true;
}

void GeometryCoordinationModel::EndModify()
{
    beingModified = false;
    emit modelChanged();
}


int GeometryCoordinationModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
    {
        return 0;
    }
    return m_geom->coordinates().size();
}

int GeometryCoordinationModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
    {
        return 0;
    }
    return 5;
}

QVariant GeometryCoordinationModel::data(const QModelIndex &index, int role) const
{
    int row = index.row();
    int col = index.column()-1;

    if (role == Qt::DisplayRole || role == Qt::EditRole)
    {
        if (col == -1)
        {
            return QString("%1").arg(row+1);
        }
        else if (col == 0)
        {
            return QString("%1").arg(m_geom->coordinates().at(row).symbol);
        }
        else
        {
            return QString("%1").arg(m_geom->coordinates().at(row).coord[col-1],0,'f',6);
        }
    }
    else if (role == Qt::TextAlignmentRole)
    {
        if (index.column() == 1 || index.column() == 0)
        {
            return Qt::AlignCenter;
        }
        else
        {

            return (Qt::AlignVCenter + Qt::AlignRight);
        }
    }

    return QVariant();
}

QVariant GeometryCoordinationModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal)
    {
        if (role == Qt::DisplayRole)
        {
            switch (section-1)
            {
            case -1:
                return tr("Index");
                break;
            case 0:
                return tr("Element");
                break;
            case 1:
                return tr("Coord x");
                break;
            case 2:
                return tr("Coord y");
                break;
            case 3:
                return tr("Coord z");
                break;
            default:
                break;
            }
            return QAbstractTableModel::headerData(section, orientation, role);
        }
    }
    return QAbstractTableModel::headerData(section, orientation, role);
}



bool GeometryCoordinationModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if ( role == Qt::EditRole )
    {
        int row = index.row();
        int col = index.column()-1;
        if (col < 0)
            return false;
        if (col==0)
        {
            m_geom->coordinates()[row].symbol = value.toString();
            m_geom->computeFormula();
            emit formulaChanged(m_geom->getFormula());
        }
        else
        {
            m_geom->coordinates()[row].coord[col-1] = value.toDouble();
        }
        emit dataChanged(index, index);
        if (!beingModified)
            emit modelChanged();
        return true;
    }
    return false;
}

Qt::ItemFlags GeometryCoordinationModel::flags(const QModelIndex &index) const
{
    if (index.column()==0)
        return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
    else
        return Qt::ItemIsEnabled | Qt::ItemIsEditable | Qt::ItemIsSelectable;
}


bool GeometryCoordinationModel::removeRows(int row, int count, const QModelIndex &parent)
{
    beginRemoveRows(QModelIndex(),row, row+count-1);
    for(int i=row+count-1; i>=row; --i)
    {
        m_geom->coordinates().removeAt(i);
    }
    endRemoveRows();
    if (!beingModified)
    {
        emit modelChanged();
        m_geom->computeFormula();
        emit formulaChanged(m_geom->getFormula());
    }
    return true;
}
