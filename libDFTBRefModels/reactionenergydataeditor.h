#ifndef REACTIONENERGYDATAEDITOR_H
#define REACTIONENERGYDATAEDITOR_H

#include <QWidget>

QT_BEGIN_NAMESPACE
class QLineEdit;
class QCheckBox;
class QComboBox;
QT_END_NAMESPACE

class ReactionEnergyData;
class DFTBRefDatabase;
class ReactionComponentWidget;
class ReactionComponentModel;
class ReactionEnergyDataEditor : public QWidget
{
    Q_OBJECT
public:
    ReactionEnergyDataEditor(ReactionEnergyData* data, DFTBRefDatabase* database, bool ReadOnly = false, QWidget *parent = 0);

signals:

private slots:
    void setName();
    void setMethod();
    void setBasis();
    void setEnergy();
    void editComments();
    void editCitations();
    void updateReactantComponents();
    void updateProductComponents();
public slots:

private:
    void setupUI();
    ReactionEnergyData* m_data;
    DFTBRefDatabase* m_database;
    QCheckBox* m_opt_comp;
    QLineEdit* m_editName;
    QLineEdit* m_editMethod;
    QLineEdit* m_editBasis;
    QLineEdit* m_editEnergy;
    QComboBox* m_combo_energy_unit;

    ReactionComponentWidget *m_reactantsWidget;
    ReactionComponentWidget *m_productsWidget;

    ReactionComponentModel *reactantModel;
    ReactionComponentModel *productModel;


    bool showTextEditor(QWidget *parent, const QString &title, QString &variable);
    bool m_ReadOnly = false;

};

#endif // REACTIONENERGYDATAEDITOR_H
