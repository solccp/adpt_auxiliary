#ifndef REACTIONCOMPONENTWIDGET_H
#define REACTIONCOMPONENTWIDGET_H

#include <QWidget>
#include <QGroupBox>

class ReactionComponentModel;
class DFTBRefDatabase;
class QTableView;
class ReactionComponentWidget : public QWidget
{
    Q_OBJECT
public:
    ReactionComponentWidget(ReactionComponentModel *model, DFTBRefDatabase* database, QWidget *parent = 0);
    void setEditable(bool editable);
signals:

public slots:
    const ReactionComponentModel* getModel();
private slots:
    void setupUI();
    void addEntry();
    void removeEntry();

private:
    ReactionComponentModel* m_model;
    DFTBRefDatabase* m_database;
    QTableView* m_tableview;
    QGroupBox *buttonBox;
};

#endif // REACTIONCOMPONENTEDITOR_H
