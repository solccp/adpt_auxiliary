#include "doublenumbercelldelegate.h"

#include <QLineEdit>


DoubleNumberCellDelegate::DoubleNumberCellDelegate(QObject *parent) :
    QStyledItemDelegate(parent)
{
}


QWidget *DoubleNumberCellDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    if (!index.isValid())
        return 0;
//    if (index.column() == 0)
//    {
        QLineEdit* editor = new QLineEdit(parent);
        QDoubleValidator* val = new QDoubleValidator();
        val->setDecimals(6);
        editor->setValidator(val);
        return editor;
//    }
//    return QStyledItemDelegate::createEditor(parent, option, index);
}


//void DoubleNumberCellDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
//{
//    QLineEdit *lineedit = qobject_cast<QLineEdit*>(editor);
//    lineedit->setText(QString::number(index.data().toDouble(), 'f', 6));

//}


QString DoubleNumberCellDelegate::displayText(const QVariant &value, const QLocale &locale) const
{
    Q_UNUSED(locale);
    bool ok;
    double dv = value.toDouble(&ok);
    if (ok)
    {
        return QString("%1").arg(dv,0,'f',4);
    }
    return value.toString();
}

void DoubleNumberCellDelegate::initStyleOption(QStyleOptionViewItem *option, const QModelIndex &index) const
{
    QStyledItemDelegate::initStyleOption(option, index);
    option->displayAlignment |= (Qt::AlignRight | Qt::AlignVCenter) ;
}
