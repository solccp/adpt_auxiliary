#ifndef GEOMETRYMODEL_H
#define GEOMETRYMODEL_H

#include <QAbstractTableModel>
#include <memory>
#include <QSet>

#include "geometry.h"
#include "bandstructuredata.h"
#include "molecularatomizationenergydata.h"
#include "molecularfrequencydata.h"
#include "reactionenergydata.h"

//class Geometry;
//class BandstructureData;
//class MolecularAtomizationEnergyData;
//class MolecularFrequencyData;
//class ReactionEnergyData;

class ReferenceDataModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit ReferenceDataModel(QObject *parent = 0);
public:
    virtual QVariant data(const QModelIndex &index, int role) const;
    void clear();
protected:
    QSet<QString> m_checkedItems;
    QMap<int, QMap<QString, QVariant> > m_data_mapping;
public:
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role);
    virtual Qt::ItemFlags flags(const QModelIndex &index) const;
    void setDataMapping(int column, const QMap<QString, QVariant>& mapping);
    void removeDataMapping(int column);
    bool isChecked(const QString& uuid) const;
};



class GeometryModel : public ReferenceDataModel
{
    Q_OBJECT
    friend class ReferenceDataImportor;
public:
    explicit GeometryModel(QObject *parent = 0);
private:
    QList<std::shared_ptr<Geometry>> m_data;
public:
    virtual int rowCount(const QModelIndex &parent) const;
    virtual int columnCount(const QModelIndex &parent) const;
    virtual QVariant data(const QModelIndex &index, int role) const;
    void addGeometry(const std::shared_ptr<Geometry>& geom);
    void clear();
private:
    QMap<QString, QString> m_UUID_mapping;

    QSet<QString> m_allUUIDs;
public:
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    QMap<QString, QString> UUID_mapping() const;
    QMap<QString, QVariant> getMapping(int column);
};

#define DECLEAR_MODEL_HEADER(modelclassname, modeldataclass) \
class modelclassname : public ReferenceDataModel \
{ \
    friend class ReferenceDataImportor; \
    Q_OBJECT \
public: \
    explicit modelclassname(QObject *parent = 0) : ReferenceDataModel(parent) \
    {} \
private: \
    QList<std::shared_ptr<modeldataclass>> m_data; \
public: \
    virtual int rowCount(const QModelIndex &parent) const \
    { \
        return m_data.size(); \
    } \
    virtual int columnCount(const QModelIndex &parent) const; \
    virtual QVariant data(const QModelIndex &index, int role) const; \
    void addData(const std::shared_ptr<modeldataclass>& data) \
    { \
        int old_size = m_data.size(); \
        beginInsertRows(QModelIndex(), old_size, old_size); \
        m_data.append(data); \
        m_checkedItems.insert(data->UUID()); \
        endInsertRows(); \
    } \
    void clear() \
    { \
        ReferenceDataModel::clear();\
        m_data.clear(); \
    }\
private: \
public: \
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const; \
};


DECLEAR_MODEL_HEADER(AtomizationEnergyModel, MolecularAtomizationEnergyData)
DECLEAR_MODEL_HEADER(BandstructureDataModel, BandstructureData)
DECLEAR_MODEL_HEADER(FrequencyModel, MolecularFrequencyData)
DECLEAR_MODEL_HEADER(ReactionEnergyModel, ReactionEnergyData)




#endif // GEOMETRYMODEL_H
