#include "reactioncomponentwidget.h"

#include "reactioncomponentmodel.h"
#include "reactioncomponentdelegate.h"

#include "doublenumbercelldelegate.h"

#include <QTableView>
#include <QHeaderView>
#include <QGridLayout>
#include <QPushButton>
#include <QMessageBox>



ReactionComponentWidget::ReactionComponentWidget(ReactionComponentModel *model, DFTBRefDatabase *database, QWidget *parent) :
    QWidget(parent)
{
    m_model = model;
    m_database = database;
    setupUI();
}

void ReactionComponentWidget::setEditable(bool editable)
{
    buttonBox->setVisible(editable);
    if (!editable)
    {
        m_tableview->setEditTriggers(QAbstractItemView::NoEditTriggers);
    }
    else
    {
        m_tableview->setEditTriggers(QAbstractItemView::DoubleClicked | QAbstractItemView::EditKeyPressed);
    }
}

const ReactionComponentModel *ReactionComponentWidget::getModel()
{
    return m_model;
}

void ReactionComponentWidget::setupUI()
{
//    QGridLayout *grid = new QGridLayout(this);
//    grid->setMargin(0);

    buttonBox = new QGroupBox;
    QHBoxLayout *hbox = new QHBoxLayout;

    QPushButton *butAddEntry = new QPushButton(tr("Add..."));
    butAddEntry->setAutoDefault(false);
    QObject::connect(butAddEntry, SIGNAL(clicked()), this, SLOT(addEntry()));
//    grid->addWidget(butAddEntry, 0,0,1,1);

    QPushButton *butRemoveEntry = new QPushButton(tr("Remove..."));
    QObject::connect(butRemoveEntry, SIGNAL(clicked()), this, SLOT(removeEntry()));
    butRemoveEntry->setAutoDefault(false);
//    grid->addWidget(butRemoveEntry, 0,1,1,1);
    hbox->addWidget(butAddEntry);
    hbox->addWidget(butRemoveEntry);
    hbox->setMargin(0);
    buttonBox->setLayout(hbox);

    QVBoxLayout *vbox = new QVBoxLayout;

    m_tableview = new QTableView();
    m_tableview->setModel(m_model);

    m_tableview->setItemDelegateForColumn(1,new ReactionComponentDelegate(m_database));
    m_tableview->setItemDelegateForColumn(0,new DoubleNumberCellDelegate(m_tableview));

    m_tableview->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_tableview->horizontalHeader()->setStretchLastSection(true);
    m_tableview->verticalHeader()->hide();
//    m_tableview->setEditTriggers(QAbstractItemView::NoEditTriggers);
//    tableView->setSelectionMode(QAbstractItemView::SingleSelection);


//    grid->addWidget(m_tableview,1,0,1,2);
    vbox->addWidget(buttonBox);
    vbox->addWidget(m_tableview);
    vbox->setMargin(0);
    this->setLayout(vbox);
}

void ReactionComponentWidget::addEntry()
{
    QAbstractTableModel* model = m_database->getGeometryTableModel(DFTBRefDatabase::Both);
    if (model->rowCount() == 0)
    {
        QMessageBox qbox(this);
        qbox.setText("Molecular geometry needs to be defined first");
        qbox.setStandardButtons(QMessageBox::Ok);
        qbox.setIcon(QMessageBox::Critical);
        qbox.exec();
        return;
    }
    m_model->insertRow(0);
    QModelIndex index = m_tableview->model()->index(0, 1, QModelIndex());
    m_tableview->setFocus();
    m_tableview->edit(index);
}

void ReactionComponentWidget::removeEntry()
{
    QModelIndexList list = m_tableview->selectionModel()->selectedRows();
    if (!list.empty())
    {
        for (int i=list.size()-1; i>=0; --i)
        {
            int row = list.at(i).row();
            m_tableview->model()->removeRows(row,1);
        }
    }
}
