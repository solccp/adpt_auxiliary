#include "klinesmodel.h"
#include <QDebug>
#include <QFile>
#include <QTextStream>
#include <QStringList>
#include <QRegExp>

#include "geometry.h"

KLinesModel::KLinesModel(ADPT::KPointsSetting *data, QObject *parent) : QAbstractTableModel(parent)
{
    m_data = data;
}


int KLinesModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return 4;
}


QVariant KLinesModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal)
    {
        switch (section) {
        case 0:
            return tr("# intervals");
            break;
        case 1:
            return tr("coord x");
            break;
        case 2:
            return tr("coord y");
            break;
        case 3:
            return tr("coord z");
            break;
        default:
            break;
        }
        return QAbstractTableModel::headerData(section, orientation, role);
    }
    else
    {
        return QAbstractTableModel::headerData(section, orientation, role);
    }
}


int KLinesModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return m_data->m_kpoints_klines.size();
}

QVariant KLinesModel::data(const QModelIndex &index, int role) const
{
    int row = index.row();
    int col = index.column();

    if (role == Qt::DisplayRole || role == Qt::EditRole)
    {
        if (col == 0)
        {
            return QString("%1").arg(m_data->m_kpoints_klines.at(row).num_of_points);
        }
        else
        {
            return QString("%1").arg(m_data->m_kpoints_klines.at(row).kpoint[col-1],0,'f',6);
        }
    }
    return QVariant();
}


QModelIndex KLinesModel::index(int row, int column, const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    if (column == 0)
    {
        QModelIndex ind = createIndex(row, column, (void *)&m_data->m_kpoints_klines.at(row).num_of_points);
        return ind;
    }
    else
    {
        QModelIndex ind = createIndex(row, column, (void *)&m_data->m_kpoints_klines.at(row).kpoint[column-1]);
        return ind;
    }
}

QModelIndex KLinesModel::parent(const QModelIndex &child) const
{
    Q_UNUSED(child);
    return QModelIndex();
}



bool KLinesModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (role == Qt::EditRole)
    {
        int row = index.row();
        int col = index.column();
        bool ok;
        if (col==0)
        {
            int val = value.toInt(&ok);
            if (val < 1)
            {
                ok = false;
            }
            if (!ok)
            {
                return false;
            }
            m_data->m_kpoints_klines[row].num_of_points = val;
        }
        else
        {
            double val = value.toDouble(&ok);
            if (!ok)
            {
                return false;
            }
            m_data->m_kpoints_klines[row].kpoint[col-1] = val;
        }
        emit dataChanged(index, index);
        return true;
    }
    return false;
}

Qt::ItemFlags KLinesModel::flags(const QModelIndex &index) const
{
    return Qt::ItemIsEnabled | Qt::ItemIsEditable | Qt::ItemIsSelectable;
}


bool KLinesModel::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(QModelIndex(),row, row+count-1);
    for (int i=0; i< count; ++i)
    {
        ADPT::KLinesEntry entry;
        entry.num_of_points = 1;
        entry.kpoint = {{0.0,0.0,0.0}};

        m_data->m_kpoints_klines.append(entry);
    }
    endInsertRows();
    return true;
}


bool KLinesModel::removeRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent);
    beginRemoveRows(QModelIndex(),row, row+count-1);
    for(int i=row+count-1; i>=row; --i)
    {
        m_data->m_kpoints_klines.removeAt(i);
    }
    endRemoveRows();
    return true;
}




bool KLinesModel::importPlainFile(const QString &filename)
{
    QFile file( filename );
    if( !file.open( QIODevice::ReadOnly | QIODevice::Text ) )
    {
        qFatal( "Could not open the file" );
    }

    QTextStream stream( &file );

    bool succ = readKLinesString(stream);

    file.close();

    return succ;
}

bool KLinesModel::importVaspKPointsFile(const QString &filename)
{
    QFile file( filename );
    if( !file.open( QIODevice::ReadOnly | QIODevice::Text ) )
    {
        qFatal( "Could not open the file" );
    }

    QTextStream stream( &file );

    bool succ = readKLinesVasp(stream);

    file.close();

    return succ;
}

bool KLinesModel::importHSDFile(const QString &filename)
{
    QFile file( filename );
    if( !file.open( QIODevice::ReadOnly | QIODevice::Text ) )
    {
        qFatal( "Could not open the file" );
    }

    QTextStream stream( &file );

    QList<std::array<QVariant,4> > res;

    QString str = stream.readAll();
    QRegExp rx(".*KPointsAndWeights\\s*=\\s*KLines\\s*\\{([^\\}]*)\\}.*");

    if (rx.exactMatch(str) && rx.capturedTexts().size() > 1 )
    {
        QString instr = rx.capturedTexts().at(1);
        QTextStream strstream( instr.toLatin1()  );
        bool succ = readKLinesString(strstream);
        return succ;
    }
    return false;

}

bool KLinesModel::readKLinesVasp(QTextStream &stream)
{
    QList<ADPT::KLinesEntry> res;

    int npt = 0;
    try
    {
        QString line;
        line= stream.readLine();
        bool ok;
        npt = stream.readLine().split(" ",QString::SkipEmptyParts).first().toInt(&ok);
        if (!ok)
        {
            return false;
        }
        line = stream.readLine();
        if (!line.toLower().startsWith('l'))
        {
            return false;
        }

        line = stream.readLine();
        if (!line.toLower().startsWith('r'))
        {
            return false;
        }
    }
    catch(...)
    {
        return false;
    }

    int stage = 1;
    while( !stream.atEnd() )
    {
        QString line = stream.readLine();
        if (line.contains('!'))
        {
            auto index = line.indexOf('!');
            line = line.mid(0, index);
        }
        QStringList list = line.split(" ",QString::SkipEmptyParts);
        if (list.empty())
        {
            continue;
        }
        else if (list.size() < 3)
        {
            return false;
        }
        else
        {
            bool ok;

            double x = list.at(0).toDouble(&ok);
            if (!ok)
            {
                return false;
            }
            double y = list.at(1).toDouble(&ok);
            if (!ok)
            {
                return false;
            }
            double z = list.at(2).toDouble(&ok);
            if (!ok)
            {
                return false;
            }

            ADPT::KLinesEntry newline;
            newline.kpoint[0] = x;
            newline.kpoint[1] = y;
            newline.kpoint[2] = z;
            if (stage == 1)
            {
                newline.num_of_points = 1;
                stage = 2;
            }
            else
            {
                newline.num_of_points = npt - 1;
                stage = 1;
            }


            res.append(newline);
        }
    }
    if (!res.empty())
    {
        int startindex = m_data->m_kpoints_klines.size();
        int endindex = m_data->m_kpoints_klines.size()+res.size()-1;
        this->beginInsertRows(QModelIndex(), startindex, endindex);
        m_data->m_kpoints_klines.append(res);
        this->endInsertRows();
        return true;
    }
    return false;
}

//int KLinesModel::numOfPoints()
//{
//    int res = 0;
//    for(auto const & item : m_data->m_kpoints_klines)
//    {
//        res += item.num_of_points;
//    }
//    return res;
//}

bool KLinesModel::readKLinesString(QTextStream &stream)
{
    QList<ADPT::KLinesEntry> res;
    while( !stream.atEnd() )
    {
        QStringList list = stream.readLine().split(" ",QString::SkipEmptyParts);
        if (list.empty())
        {
            continue;
        }
        else if (list.first().startsWith("#"))
        {
            continue;
        }
        else if (list.size() < 4)
        {
            return false;
        }
        else
        {
            bool ok;
            int nint = list.at(0).toInt(&ok);
            if (!ok)
            {
                return false;
            }
            double x = list.at(1).toDouble(&ok);
            if (!ok)
            {
                return false;
            }
            double y = list.at(2).toDouble(&ok);
            if (!ok)
            {
                return false;
            }
            double z = list.at(3).toDouble(&ok);
            if (!ok)
            {
                return false;
            }
            ADPT::KLinesEntry newline;
            newline.num_of_points = nint;
            newline.kpoint[0] = x;
            newline.kpoint[1] = y;
            newline.kpoint[2] = z;

            res.append(newline);
        }
    }
    if (!res.empty())
    {
        int startindex = m_data->m_kpoints_klines.size();
        int endindex = m_data->m_kpoints_klines.size()+res.size()-1;
        this->beginInsertRows(QModelIndex(), startindex, endindex);
        m_data->m_kpoints_klines.append(res);
        this->endInsertRows();
        return true;
    }
    return false;
}
