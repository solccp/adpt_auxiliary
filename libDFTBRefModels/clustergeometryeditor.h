#ifndef CLUSTERGEOMETRYEDITOR_H
#define CLUSTERGEOMETRYEDITOR_H

#include <QWidget>
#include <QDebug>

QT_BEGIN_NAMESPACE
class QCheckBox;
class QPushButton;
class QTableView;
class QDialog;
class QLineEdit;
QT_END_NAMESPACE

class Geometry;

class GeometryCoordinationModel;
class ClusterGeometryEditor : public QWidget
{
    Q_OBJECT
public:
    ClusterGeometryEditor(Geometry *geometry, bool isReadOnly = false, QWidget *parent = 0);
    Geometry *geometry() const;
public slots:
private slots:
    void editForce();
    void loadGeometry();
    void saveGeometry();
    void clear();
    void addAtom();
    void deleteAtom();
    void editComments();
    void editCitations();
    void setBasis();
    void setMethod();
    void updateUUID();

private:
    void setupControllers();
    void setupDialog();
    Geometry* m_geometry = nullptr;
    GeometryCoordinationModel* m_geometrymodel = nullptr;

private:


private:
    QLineEdit *editUUID;
    QTableView* m_geometryTableView;
    QPushButton* butExternalForce;
    QPushButton* butLoadGeometry;
    QPushButton* butSaveGeometry;
    QPushButton* butClear;
    QPushButton* butAddAtom;
    QPushButton* butDeleteAtom;
    QLineEdit* editCharge;
    QLineEdit* editSpin;
    QLineEdit* comboName;
    QLineEdit* editMethod;
    QLineEdit* editBasis;
    QLineEdit* editFormula;

    QPushButton* butEditComments;
    QPushButton* butEditCitation;
    QCheckBox* checkStationaryPoint;
    bool m_isReadOnly = false;

private:

};

#endif // CLUSTERGEOMETRYEDITOR_H
