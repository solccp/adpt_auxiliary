#include "dopwidget.h"
#include "BSOptTargetIndexBasedModel.h"
#include <QGridLayout>
#include <QHeaderView>
#include <QFileDialog>
#include <QMessageBox>

DOPWidget::DOPWidget(BandstructureData *bsData, QWidget *parent) :
    QWidget(parent), m_bsData(bsData)
{

    setupUI();
}

void DOPWidget::setEditable(bool editable)
{
    butAddEntry->setEnabled(editable);
    butClear->setEnabled(editable);
    butImportPlainFile->setEnabled(editable);
    butRemoveEntry->setEnabled(editable);
}

void DOPWidget::importPlainInput()
{
    QString filename = QFileDialog::getOpenFileName(
           this,
           tr("Import data file for default fitting area"),
           QDir::homePath(),
           tr("Data files (*.*)") );
    if (!filename.isNull())
    {
        bool loaded = false;
        if (this->m_tabWidget->currentIndex() == 0)
        {
            loaded = m_index_model->importPlaintextFile(filename);
        }
        else if (this->m_tabWidget->currentIndex() == 1)
        {
            loaded = m_energy_model->importPlaintextFile(filename);
        }

        if (!loaded)
        {
            QMessageBox qbox(this);
//            qbox.setWindowTitle(tr("DFTBRefDatabaseManager"));
            qbox.setText(tr("Plain data file parsing error. Cancelled."));
            qbox.setStandardButtons(QMessageBox::Ok);
            qbox.setIcon(QMessageBox::Critical);
            qbox.exec();
        }
    }
}

void DOPWidget::addEntry()
{
    if (this->m_tabWidget->currentIndex() == 0)
    {
        m_tableView_index->model()->insertRows(m_tableView_index->model()->rowCount(), 1, QModelIndex());
        m_tableView_index->setFocus();
        m_tableView_index->selectRow(m_tableView_index->model()->rowCount());
    }
    else if (this->m_tabWidget->currentIndex() == 1)
    {
        m_tableView_energy->model()->insertRows(m_tableView_energy->model()->rowCount(), 1, QModelIndex());
        m_tableView_energy->setFocus();
        m_tableView_energy->selectRow(m_tableView_energy->model()->rowCount());
    }
}

void DOPWidget::clear()
{
    if (this->m_tabWidget->currentIndex() == 0)
    {
        if (m_index_model != NULL)
        {
            m_index_model->clear();
        }
    }
    else if (this->m_tabWidget->currentIndex() == 1)
    {
        if (m_energy_model != NULL)
        {
            m_energy_model->clear();
        }
    }
}

void DOPWidget::removeEntry()
{
    if (this->m_tabWidget->currentIndex() == 0)
    {
        QModelIndexList list = m_tableView_index->selectionModel()->selectedRows();
        if (!list.empty())
        {
            for (int i=list.size()-1; i>=0; --i)
            {
                int row = list.at(i).row();
                m_tableView_index->model()->removeRows(row,1);
            }
        }
    }
    else if (this->m_tabWidget->currentIndex() == 1)
    {
        QModelIndexList list = m_tableView_energy->selectionModel()->selectedRows();
        if (!list.empty())
        {
            for (int i=list.size()-1; i>=0; --i)
            {
                int row = list.at(i).row();
                m_tableView_energy->model()->removeRows(row,1);
            }
        }
    }
}

void DOPWidget::setupUI()
{



    m_tabWidget = new QTabWidget(this);


    QGridLayout* grid = new QGridLayout();


    butAddEntry = new QPushButton(tr("Add entry"));
    butRemoveEntry = new QPushButton(tr("Remove entry"));
    butImportPlainFile = new QPushButton(tr("Import..."));
    butClear = new QPushButton(tr("Clear"));

    butAddEntry->setAutoDefault(false);
    butRemoveEntry->setAutoDefault(false);
    butImportPlainFile->setAutoDefault(false);
    butClear->setAutoDefault(false);

    grid->addWidget(butAddEntry,0,0,1,1);
    grid->addWidget(butRemoveEntry,0,1,1,1);
    grid->addWidget(butImportPlainFile,0,2,1,1);
    grid->addWidget(butClear,0,3,1,1);

    grid->addWidget(m_tabWidget, 1,0,1,4);

    m_tableView_index = new QTableView();
    m_tableView_index->setSelectionBehavior(QAbstractItemView::SelectRows);

    m_tableView_energy = new QTableView();
    m_tableView_energy->setSelectionBehavior(QAbstractItemView::SelectRows);

    m_tabWidget->addTab(m_tableView_index,"Index Based");
    m_tabWidget->addTab(m_tableView_energy,"Energy Based");

    m_index_model = new BSOptTargetIndexBasedModel(m_bsData);
    m_energy_model = new BSOptTargetEnergyBasedModel(m_bsData);

    m_tableView_index->setModel(m_index_model);
    m_tableView_index->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    m_tableView_energy->setModel(m_energy_model);
    m_tableView_energy->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    connect(butImportPlainFile, SIGNAL(clicked()), this, SLOT(importPlainInput()));
    connect(butAddEntry, SIGNAL(clicked()), this, SLOT(addEntry()));
    connect(butRemoveEntry, SIGNAL(clicked()), this, SLOT(removeEntry()));
    connect(butClear, SIGNAL(clicked()), this, SLOT(clear()));


    this->setLayout(grid);
}

