#include "kpointssupercellfoldingmodel.h"

#include <QFile>

#include "geometry.h"

KPointsSupercellFoldingModel::KPointsSupercellFoldingModel(ADPT::KPointsSetting *data, QObject *parent) : QAbstractTableModel(parent)
{
    m_data = data;
}


int KPointsSupercellFoldingModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return 3;
}


QVariant KPointsSupercellFoldingModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal)
    {
        switch (section) {
        case 0:
            return tr("x");
            break;
        case 1:
            return tr("y");
            break;
        case 2:
            return tr("z");
            break;
        default:
            break;
        }
        return QAbstractTableModel::headerData(section, orientation, role);
    }
    else if (role == Qt::DisplayRole && orientation == Qt::Vertical)
    {
        switch (section) {
        case 0:
            return tr("x");
            break;
        case 1:
            return tr("y");
            break;
        case 2:
            return tr("z");
            break;
        case 3:
            return tr("shift");
            break;
        default:
            break;
        }
        return QAbstractTableModel::headerData(section, orientation, role);
    }
    else
    {
        return QAbstractTableModel::headerData(section, orientation, role);
    }
}


int KPointsSupercellFoldingModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return 4;
}

QVariant KPointsSupercellFoldingModel::data(const QModelIndex &index, int role) const
{
    int row = index.row();
    int col = index.column();

    if (role == Qt::DisplayRole || role == Qt::EditRole)
    {
        if (row == 3)
        {
            return QString("%1").arg(m_data->m_kpoints_supercell.shifts[col],0,'f',6);
        }
        else
        {
            return QString("%1").arg(m_data->m_kpoints_supercell.num_of_cells[row][col]);
        }
    }
    return QVariant();
}


QModelIndex KPointsSupercellFoldingModel::index(int row, int column, const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    if (row == 3)
    {
        QModelIndex ind = createIndex(row, column, (void *)&m_data->m_kpoints_supercell.shifts[column]);
        return ind;
    }
    else
    {
        QModelIndex ind = createIndex(row, column, (void *)&m_data->m_kpoints_supercell.num_of_cells[row][column]);
        return ind;
    }
}

QModelIndex KPointsSupercellFoldingModel::parent(const QModelIndex &child) const
{
    Q_UNUSED(child);
    return QModelIndex();
}



bool KPointsSupercellFoldingModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (role == Qt::EditRole)
    {
        int row = index.row();
        int col = index.column();
        bool ok;
        if (row == 3)
        {
            auto val = value.toDouble(&ok);
            if (!ok)
            {
                return false;
            }
            m_data->m_kpoints_supercell.shifts[col] = val;
        }
        else
        {
            auto val = value.toDouble(&ok);
            if (!ok)
            {
                return false;
            }
            m_data->m_kpoints_supercell.num_of_cells[row][col] = val;
        }
        emit dataChanged(index, index);
        return true;
    }
    return false;
}

Qt::ItemFlags KPointsSupercellFoldingModel::flags(const QModelIndex &index) const
{
    return Qt::ItemIsEnabled | Qt::ItemIsEditable | Qt::ItemIsSelectable;
}


bool KPointsSupercellFoldingModel::importPlainFile(const QString &filename)
{
    QFile file( filename );
    if( !file.open( QIODevice::ReadOnly | QIODevice::Text ) )
    {
        qFatal( "Could not open the file" );
    }

    QTextStream stream( &file );

    bool succ = readKLinesString(stream);

    file.close();

    return succ;
}

bool KPointsSupercellFoldingModel::importHSDFile(const QString &filename)
{
    QFile file( filename );
    if( !file.open( QIODevice::ReadOnly | QIODevice::Text ) )
    {
        qFatal( "Could not open the file" );
    }

    QTextStream stream( &file );

    QList<std::array<QVariant,4> > res;

    QString str = stream.readAll();
    QRegExp rx(".*KPointsAndWeights\\s*=\\s*KLines\\s*\\{([^\\}]*)\\}.*");

    if (rx.exactMatch(str) && rx.capturedTexts().size() > 1 )
    {
        QString instr = rx.capturedTexts().at(1);
        QTextStream strstream( instr.toLatin1()  );
        bool succ = readKLinesString(strstream);
        return succ;
    }
    return false;

}

bool KPointsSupercellFoldingModel::readKLinesString(QTextStream &stream)
{
    ADPT::KPointShpercellFolding res;
    int linesRead = 0;
    while( !stream.atEnd() )
    {
        QStringList list = stream.readLine().split(" ",QString::SkipEmptyParts);
        if (list.empty())
        {
            continue;
        }
        else if (list.first().startsWith("#"))
        {
            continue;
        }
        else if (list.size() < 4)
        {
            return false;
        }
        else
        {
            bool ok;
            if (linesRead < 3)
            {
                int x = list.at(0).toInt(&ok);
                if (!ok)
                {
                    return false;
                }
                int y = list.at(1).toInt(&ok);
                if (!ok)
                {
                    return false;
                }
                int z = list.at(2).toInt(&ok);
                if (!ok)
                {
                    return false;
                }
                res.num_of_cells[linesRead][0] = x;
                res.num_of_cells[linesRead][1] = y;
                res.num_of_cells[linesRead][2] = z;
            }
            else if (linesRead == 3)
            {
                double x = list.at(0).toDouble(&ok);
                if (!ok)
                {
                    return false;
                }
                double y = list.at(1).toDouble(&ok);
                if (!ok)
                {
                    return false;
                }
                double z = list.at(2).toDouble(&ok);
                if (!ok)
                {
                    return false;
                }
                res.num_of_cells[linesRead][0] = x;
                res.num_of_cells[linesRead][1] = y;
                res.num_of_cells[linesRead][2] = z;
                break;
            }
            ++linesRead;
        }
    }

    this->m_data->m_kpoints_supercell = res;
    return true;
}


