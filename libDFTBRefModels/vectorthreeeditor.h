#ifndef VECTORTHREEEDITOR_H
#define VECTORTHREEEDITOR_H

#include <QWidget>
#include <QTableView>
#include <QAbstractTableModel>
#include <QList>
#include "basic_types.h"
#include "geometry.h"


class VectorThreeEditor;
class VectorThreeModel: public QAbstractTableModel
{
    friend class VectorThreeEditor;
    Q_OBJECT

public:
    explicit VectorThreeModel(QVector<dVector3> *array, QObject *parent = 0);
private:
    QVector<dVector3> *m_array = nullptr;
    // QAbstractItemModel interface

    QStringList m_headers;
public:
    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);


    // QAbstractItemModel interface
public:
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;

    // QAbstractItemModel interface
public:
    Qt::ItemFlags flags(const QModelIndex &index) const;
};

class VectorThreeEditor : public QWidget
{
    Q_OBJECT
public:
    explicit VectorThreeEditor(Geometry* geom, QWidget *parent = 0);
signals:

public slots:
    void setHeaders(const QString& h1, const QString& h2, const QString& h3);
public:
    bool isValid() const;
private:
    Geometry *m_geom;

    QTableView *m_tableview = nullptr;
    VectorThreeModel *m_model = nullptr;
public:
    QVector<dVector3> m_data;

    // QWidget interface
protected:
    void resizeEvent(QResizeEvent *event);
};









#endif // VECTORTHREEEDITOR_H
