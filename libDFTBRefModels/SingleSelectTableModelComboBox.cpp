#include "SingleSelectTableModelComboBox.h"

#include <QVariant>
#include <QLineEdit>
#include <QHBoxLayout>
#include <QPushButton>

#include <QDebug>

SingleSelectTableModelComboBox::SingleSelectTableModelComboBox(QWidget *parent) :
    QWidget(parent)
{
    QHBoxLayout *vbox = new QHBoxLayout();
    vbox->setMargin(0);
    this->setLayout(vbox);

    m_lineEdit = new QLineEdit();
    m_lineEdit->setReadOnly(true);
    vbox->addWidget(m_lineEdit);
    m_button = new QPushButton(tr("Choose..."));
    connect(m_button, SIGNAL(clicked()), this, SLOT(showPopup()));
    vbox->addWidget(m_button);

    m_listWidget = new SingleSelectListWidget();
    m_listWidget->setWindowFlags(Qt::Popup);
    connect(m_listWidget, SIGNAL(valueSelected(int,QVariant,QString)), this, SLOT(valueSelected(int,QVariant,QString)));

    this->setMaximumHeight(30);
}

void SingleSelectTableModelComboBox::setModel(QAbstractItemModel *model)
{
    m_listWidget->setModel(model);
}

void SingleSelectTableModelComboBox::setModelColumn(int visibleColumn)
{
    m_listWidget->setIndexColumn(visibleColumn);
}

void SingleSelectTableModelComboBox::setEditable(bool value)
{
    m_lineEdit->setReadOnly(!value);
    if (value)
    {
        m_button->show();
    }
    else
    {
        m_button->hide();
    }
}

void SingleSelectTableModelComboBox::valueSelected(int index, const QVariant &value, const QString& display)
{
    m_lineEdit->setText(display);
    m_listWidget->hide();
    emit currentTextChanged(value.toString());
}


void SingleSelectTableModelComboBox::showPopup()
{
    QRect rect = this->rect();
    rect.moveTo(this->mapToGlobal(this->rect().bottomLeft()));
    rect.setHeight(400);
    m_listWidget->setGeometry(rect);
    m_listWidget->show();
}

void SingleSelectTableModelComboBox::setCurrentIndex(const QString &text)
{
    m_listWidget->setCurrentIndex(text);
    m_lineEdit->setText(m_listWidget->currentDisplayText());
}

QString SingleSelectTableModelComboBox::currentIndex()
{
    return m_listWidget->currentIndex().toString();
}

void SingleSelectTableModelComboBox::setDisplayColumn(int index)
{
    m_displayColumn = index;
    m_listWidget->setDisplayColumn(index);
    m_lineEdit->setText(m_listWidget->currentDisplayText());
}

int SingleSelectTableModelComboBox::displayColumn()
{
    return m_displayColumn;
}


QSize SingleSelectTableModelComboBox::sizeHint() const
{
    return QWidget::sizeHint();
}
