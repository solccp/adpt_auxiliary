#include "bandstructuredata.h"
#include "bandstructurebandsmodel.h"
#include <QTextStream>
#include <QStringList>
#include <QFile>
#include <QVector>

BandStructureBandsModel::BandStructureBandsModel(BandstructureData* bsdata, QObject *parent) : QAbstractTableModel(parent)
{
    m_data = bsdata;
}

void BandStructureBandsModel::clear()
{
    beginResetModel();
    m_data->m_bands.clear();
    endResetModel();
}

bool BandStructureBandsModel::importPlainFile(const QString &filename)
{
    if (filename.isEmpty())
        return false;
    QFile file( filename );
    if( !file.open( QIODevice::ReadOnly | QIODevice::Text ) )
    {
        qFatal( "Could not open the file" );
    }

    QTextStream stream( &file );


    QVector<QVector<double> > res;

    QVector<double> temp;
    int ncol = -1;
    bool ncolset = false;
    while(!stream.atEnd())
    {
        QString line = stream.readLine();
        QStringList array = line.split(" ", QString::SkipEmptyParts);
        if (array.size() == 0)
        {
            continue;
        }
        if (array.first().startsWith('#'))
        {
            continue;
        }
        int lncol = -1;
        for (lncol = 0; lncol< array.size(); ++lncol)
        {
            if (array.at(lncol).startsWith("#"))
            {
                break;
            }
        }
        if (ncolset == false)
        {
            ncolset = true;
            ncol = lncol;
            res.clear();
            res.resize(ncol-1);
        }
        else
        {
            if (lncol != ncol)
            {
                return false;
            }
        }
        temp.clear();
        for(int i=1; i<ncol; i++)
        {
            bool ok;
            double val = array.at(i).toDouble(&ok);
            if (!ok)
            {
                return false;
            }
            temp.append(val);
        }
        for(int i=0; i<temp.size();++i)
        {
            res[i].append(temp[i]);
        }
    }
    file.close();

    if (!res.empty())
    {
        clear();
        beginInsertColumns(QModelIndex(), 0, res.size()-1);
        beginInsertRows(QModelIndex(),0,res.first().size()-1);
        for(int i=0; i<res.size();++i)
        {
            m_data->m_bands.append(res.value(i));
        }
        endInsertRows();
        endInsertColumns();

        emit dataChanged();
        return true;
    }
    return false;
}

bool BandStructureBandsModel::exportPlainText(const QString &filename)
{
    QFile file(filename);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        return false;
    }

    int nbands = m_data->m_bands.size();
    int npoints = m_data->m_bands.first().size();
    QTextStream os(&file);

    for(int i=0; i<npoints;++i)
    {
        os << i+1 << " ";
        for(int j=0; j<nbands;++j)
        {
            os << " " << QString("%1").arg(m_data->m_bands[j][i], 20, 'E', 12);
        }
        os << endl;
    }
    os.flush();
    file.close();
    return true;
}

double BandStructureBandsModel::getData(int bandIndex, int pointIndex)
{
    return m_data->m_bands.at(bandIndex).at(pointIndex);
}

BandstructureData *BandStructureBandsModel::getBandStructureData() const
{
    return m_data;
}


int BandStructureBandsModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    if (m_data->m_bands.empty())
    {
        return 0;
    }
    else
    {
        return m_data->m_bands[0].size();
    }
}

int BandStructureBandsModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return m_data->m_bands.size();
}

QVariant BandStructureBandsModel::data(const QModelIndex &index, int role) const
{
    int row = index.row();
    int col = index.column();

    if (role == Qt::DisplayRole || role == Qt::EditRole)
    {
        double value = m_data->m_bands.at(col).at(row);
        return QString("%1").arg(value,0,'f',6);
    }
    return QVariant();
}
