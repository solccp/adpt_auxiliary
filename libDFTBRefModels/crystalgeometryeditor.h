#ifndef CRYSTALGEOMETRYEDITOR_H
#define CRYSTALGEOMETRYEDITOR_H

#include <QWidget>
#include <QDebug>

QT_BEGIN_NAMESPACE
class QCheckBox;
class QPushButton;
class QTableView;
class QDialog;
class QLineEdit;
class QComboBox;
class QSpinBox;
QT_END_NAMESPACE

class Geometry;
class CrystalGeometryForm;
class KPointsSettingWidget;

class GeometryCoordinationModel;
class CrystalGeometryEditor : public QWidget
{
    Q_OBJECT
public:
    CrystalGeometryEditor(Geometry *geometry, bool isReadOnly = false, QWidget *parent = 0);
    Geometry *geometry() const;
signals:
    
public slots:
private slots:
    void loadGeometry();
    void saveGeometry();
    void clear();
    void addAtom();
    void deleteAtom();
    void editComments();
    void editCitations();
    void setUPE(const QString& str);
    void setBasis();
    void setMethod();
    void geometryChanged();

    void showKpointDetails();
    void updateUUID();
private:

    void setupControllers();
    Geometry* m_geometry = nullptr;
    GeometryCoordinationModel* m_geometrymodel = nullptr;
    bool showTextEditor(QWidget *parent, const QString &title, QString &variable);
private:

private:
    QTableView* m_geometryTableView;
    QPushButton* butLoadGeometry;
    QPushButton* butSaveGeometry;
    QPushButton* butClear;
    QPushButton* butAddAtom;
    QPushButton* butDeleteAtom;

    QLineEdit *editUUID;
    QLineEdit* comboName;
    QLineEdit* editMethod;
    QLineEdit* editBasis;
    QLineEdit* editUPE;
    QLineEdit* editFormula;
    QPushButton* butCrystalEditor;
    QPushButton* butEditComments;
    QPushButton* butEditCitation;
    bool m_isReadOnly = false;

private:
    CrystalGeometryForm* m_crystalGeometryForm;
    KPointsSettingWidget* kpointsSettingwidget;
};

#endif // CRYSTALGEOMETRYEDITOR_H
