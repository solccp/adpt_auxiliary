#include "klineswidget.h"

#include <QtGui>
#include <QBoxLayout>
#include <QFileDialog>
#include <QMessageBox>


#include "klinesmodel.h"

KLinesWidget::KLinesWidget(QWidget *parent) :
    QWidget(parent)
{
    this->resize(480,400);




    m_tableView = new QTableView(this);
    m_tableView->setSelectionBehavior(QAbstractItemView::SelectRows);

    butAddRow = new QPushButton(tr("Add row"));
    butAddRow->setAutoDefault(false);
    butRemoveRow = new QPushButton(tr("Delete row"));
    butRemoveRow->setAutoDefault(false);
    QObject::connect(butAddRow, SIGNAL(clicked()), this, SLOT(addRow()));
    QObject::connect(butRemoveRow, SIGNAL(clicked()), this, SLOT(removeRow()));
    butImportVaspFile = new QPushButton(tr("Import VASP"));
    butImportVaspFile->setAutoDefault(false);
    butImportPlainFile = new QPushButton(tr("Import plaintext"));
    butImportPlainFile->setAutoDefault(false);
    butImportHSDFile = new QPushButton(tr("Import HSD"));
    butImportHSDFile->setAutoDefault(false);
    QObject::connect(butImportPlainFile, SIGNAL(clicked()), this, SLOT(importPlainInput()));
    QObject::connect(butImportVaspFile, SIGNAL(clicked()), this, SLOT(importVaspInput()));
    QObject::connect(butImportHSDFile, SIGNAL(clicked()), this, SLOT(importHSDInput()));


    QVBoxLayout *vbox = new QVBoxLayout();
    vbox->setMargin(0);
    QGridLayout *buttonGrid = new QGridLayout();
    auto hbox = new QHBoxLayout();
    hbox->addWidget(butImportPlainFile);
    hbox->addWidget(butImportVaspFile);
    hbox->addWidget(butImportHSDFile);
    buttonGrid->addLayout(hbox, 0,0,1,2);
//    buttonGrid->addWidget(butImportPlainFile,0,0,1,1);
//    buttonGrid->addWidget(butImportHSDFile,0,1,1,1);
    buttonGrid->addWidget(butAddRow,1,0,1,1);
    buttonGrid->addWidget(butRemoveRow,1,1,1,1);
    buttonBox = new QGroupBox;
    buttonBox->setLayout(buttonGrid);

    vbox->addWidget(buttonBox);
    vbox->addWidget(m_tableView);

    this->setLayout(vbox);

//    grid->addWidget(m_tableView,2,0,1,2);



//    QHBoxLayout *hbox = new QHBoxLayout(this);
//    QVBoxLayout *vbox = new QVBoxLayout();
//    hbox->addWidget(m_tableView);
//    hbox->addLayout(vbox);



//    vbox->addWidget(butAddRow);
//    vbox->addWidget(butRemoveRow);



//    vbox->addSpacing(40);
//    vbox->addWidget(butImportPlainFile);
//    vbox->addWidget(butImportHSDFile);
//    vbox->addStretch();




}

KLinesWidget::~KLinesWidget()
{

}

KLinesModel *KLinesWidget::getModel() const
{
    return m_model;
}

void KLinesWidget::setModel(KLinesModel *model)
{
    m_model = model;
    m_tableView->setModel(model);
}

void KLinesWidget::setEditable(bool enable)
{
    if (enable)
    {
        buttonBox->show();
    }
    else
    {
        buttonBox->hide();
    }
}



void KLinesWidget::addRow()
{
    m_tableView->model()->insertRows(m_tableView->model()->rowCount(), 1, QModelIndex());
    m_tableView->setFocus();
    m_tableView->selectRow(m_tableView->model()->rowCount());
}

void KLinesWidget::removeRow()
{
    QModelIndexList list = m_tableView->selectionModel()->selectedRows();
    if (!list.empty())
    {
        for (int i=list.size()-1; i>=0; --i)
        {
            int row = list.at(i).row();
            m_tableView->model()->removeRows(row,1);
        }
    }
}

void KLinesWidget::importVaspInput()
{
    QString filename = QFileDialog::getOpenFileName(
           this,
           tr("Import Vasp KLines data file"),
           QDir::homePath(),
           tr("Data files (* KPOINTS*)") );
    if (!filename.isNull())
    {
        if (!m_model->importVaspKPointsFile(filename))
        {
            QMessageBox qbox(this);
            qbox.setWindowTitle(tr("DFTBRefDatabaseManager"));
            qbox.setText(tr("Plain data file parsing error. Cancelled."));
            qbox.setStandardButtons(QMessageBox::Ok);
            qbox.setIcon(QMessageBox::Critical);
            qbox.exec();
        }
    }
}

void KLinesWidget::importPlainInput()
{
    QString filename = QFileDialog::getOpenFileName(
           this,
           tr("Import KLines data file"),
           QDir::homePath(),
           tr("Data files (*)") );
    if (!filename.isNull())
    {
        if (!m_model->importPlainFile(filename))
        {
            QMessageBox qbox(this);
            qbox.setWindowTitle(tr("DFTBRefDatabaseManager"));
            qbox.setText(tr("Plain data file parsing error. Cancelled."));
            qbox.setStandardButtons(QMessageBox::Ok);
            qbox.setIcon(QMessageBox::Critical);
            qbox.exec();
        }
    }
}

void KLinesWidget::importHSDInput()
{
    QString filename = QFileDialog::getOpenFileName(
           this,
           tr("Import KLines data file"),
           QDir::homePath(),
           tr("HSD files (*)") );
    if (!filename.isNull())
    {
        if (!m_model->importHSDFile(filename))
        {
            QMessageBox qbox(this);
            qbox.setWindowTitle(tr("DFTBRefDatabaseManager"));
            qbox.setText(tr("HSD data file parsing error. Cancelled."));
            qbox.setStandardButtons(QMessageBox::Ok);
            qbox.setIcon(QMessageBox::Critical);
            qbox.exec();
        }
    }
}
