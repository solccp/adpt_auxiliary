#ifndef SINGLESELECTLISTWIDGET_H
#define SINGLESELECTLISTWIDGET_H

#include <QWidget>
#include <QVariant>
#include <QModelIndex>

QT_BEGIN_NAMESPACE
class QAbstractItemModel;
class QSortFilterProxyModel;
class QTableView;
class QComboBox;
class QLineEdit;
QT_END_NAMESPACE

class SingleSelectListWidget : public QWidget
{
    Q_OBJECT
public:
    explicit SingleSelectListWidget(QWidget *parent = 0);
    void setIndexColumn(int index);
    int indexColumn() const;
    int displayColumn() const;
    void setDisplayColumn(int displayColumn);
    void setModel(QAbstractItemModel* model);
signals:
    void valueSelected(int row, QVariant value, QString displayText);
public slots:
    QVariant currentIndex();
    void setCurrentIndex(const QVariant& data);
    QString currentDisplayText();
private slots:
    void filterChanged(const QString& filter_text);
    void filterChanged(int index);
    void itemSelected(QModelIndex index);
    void itemDoubleClicked(const QModelIndex index);
private:
    QModelIndex m_selectedIndex = QModelIndex();
    int m_indexColumn = 0;
    int m_displayColumn = 0;
    void setupView();
    void setupUI();
    QTableView* m_tableView;
    QSortFilterProxyModel * m_model = nullptr;
    QLineEdit* m_filter;
    QComboBox* m_filterColumn;

    // QWidget interface
public:
    virtual QSize sizeHint() const;

};

#endif // SINGLESELECTLISTWIDGET_H
