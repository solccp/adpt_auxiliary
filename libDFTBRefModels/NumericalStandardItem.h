#ifndef NUMERICALSTANDARDITEM_H
#define NUMERICALSTANDARDITEM_H

#include <QStandardItem>

class NumericalStandardItem : public QStandardItem
{
public:
    // QStandardItem interface
public:
    void setData(const QVariant &value, int role);
    QStandardItem *clone() const;
private:
    double m_value;

    // QStandardItem interface
public:
    QVariant data(int role) const;
};

#endif // NUMERICALSTANDARDITEM_H
