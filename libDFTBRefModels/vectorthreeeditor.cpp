#include "vectorthreeeditor.h"
#include <QResizeEvent>
#include <QAbstractTableModel>
#include <QHeaderView>

VectorThreeModel::VectorThreeModel(QVector<dVector3> *array,
                                    QObject *parent) : QAbstractTableModel(parent)
{
    m_array = array;
    m_headers.append("1");
    m_headers.append("2");
    m_headers.append("3");
}

int VectorThreeModel::rowCount(const QModelIndex &parent) const
{
    return m_array->size();
}

int VectorThreeModel::columnCount(const QModelIndex &parent) const
{
    return 3;
}

QVariant VectorThreeModel::data(const QModelIndex &index, int role) const
{
    int row = index.row();
    int col = index.column();
    if (role == Qt::EditRole || role == Qt::DisplayRole )
    {
        return QVariant(QString::number((*m_array)[row][col],'f', 6));
    }
    else
    {
        return QVariant();
    }
}

bool VectorThreeModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    int row = index.row();
    int col = index.column();
    if (role == Qt::EditRole)
    {
        (*m_array)[row][col] = value.toDouble();
        return true;
    }
    else
    {
        return false;
    }

}


VectorThreeEditor::VectorThreeEditor(Geometry *geom, QWidget *parent)
{
    m_geom = geom;

    if (m_geom->force().size() == m_geom->coordinates().size())
    {
        m_data = m_geom->force().toVector();
    }
    else
    {
        m_data.resize(m_geom->coordinates().size());
    }

    m_model = new VectorThreeModel(&m_data, this);
    m_tableview = new QTableView(this);
    m_tableview->setModel(m_model);

    m_tableview->setEditTriggers(QAbstractItemView::DoubleClicked);
}

void VectorThreeEditor::setHeaders(const QString &h1, const QString &h2, const QString &h3)
{
    m_model->m_headers[0] = h1;
    m_model->m_headers[1] = h2;
    m_model->m_headers[2] = h3;
    m_tableview->update();
}

bool VectorThreeEditor::isValid() const
{
    double max_value = 0.0;
    for(int i=0; i<m_data.size(); ++i)
    {
        for(int j=0; j<3; ++j)
        {
            double value = std::abs(m_data[i][j]);
            if (value > max_value)
            {
                max_value = value;
            }
        }
    }
    if (max_value >= 0.0001)
    {
        return true;
    }
    else
    {
        return false;
    }
}


void VectorThreeEditor::resizeEvent(QResizeEvent *event)
{
    m_tableview->resize(event->size());
}

QVariant VectorThreeModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    QVariant var ;
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal)
    {
        var = m_headers[section];
    }
    else
    {
        var = QAbstractTableModel::headerData(section, orientation, role);
    }
    return var;
}


Qt::ItemFlags VectorThreeModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags flags = QAbstractTableModel::flags(index);
    flags |= Qt::ItemIsEditable | Qt::ItemIsEnabled;
    return flags;
}
