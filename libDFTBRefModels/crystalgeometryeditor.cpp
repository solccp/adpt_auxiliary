#include "crystalgeometryeditor.h"

#include <QTableView>
#include <QGridLayout>
#include <QPushButton>
#include <QFileDialog>
#include <QMessageBox>
#include <QHeaderView>
#include <QLineEdit>
#include <QLabel>
#include <QTextEdit>
#include <QComboBox>
#include <QCheckBox>
#include <QSet>
#include <QSpinBox>

#include "utils.h"

#include "geometry.h"
#include "atomsymbolcombodelegate.h"
#include "crystalgeometryform.h"
#include "geometrycoordinationmodel.h"

#include "UpperCaseLineEdit.h"
#include "kpointssettingwidget.h"

#include <QDebug>

CrystalGeometryEditor::CrystalGeometryEditor(Geometry *geometry, bool isReadOnly, QWidget *parent) :
    QWidget(parent), m_isReadOnly(isReadOnly)
{
    m_geometry = geometry;
    m_geometrymodel = new GeometryCoordinationModel(m_geometry);
    m_crystalGeometryForm = new CrystalGeometryForm(m_geometry);
    connect(m_crystalGeometryForm, SIGNAL(modelChanged()), this, SLOT(updateUUID()));
    setupControllers();
//    setupDialog();
}
Geometry *CrystalGeometryEditor::geometry() const
{
    return m_geometry;
}



void CrystalGeometryEditor::loadGeometry()
{
    QString filter = nullptr;
    QString filename = QFileDialog::getOpenFileName(
           this,
           tr("Load Geometry"),
           QDir::homePath(),
           tr("XYZ file (*.xyz);;Gaussion log file (*.log *.out);;Gen file (*.gen);;HSD file (*.hsd);;Vasp files (POSCAR CONTCAR)"), &filter );
    if (!filename.isNull() && !filename.isEmpty())
    {
        QFile file(filename);
        if( !file.open( QIODevice::ReadOnly | QIODevice::Text ) )
        {
            qFatal( ("Could not open the file:" + filename).toLatin1() );
            return;
        }
        QTextStream stream(&file);
        bool succ = false;
        QString metadata;
        if (filter == "XYZ file (*.xyz)")
        {
            succ = m_geometrymodel->loadXYZGeometry(stream, metadata);
            try
            {
                libvariant::Variant var = libvariant::DeserializeJSON(metadata.toStdString());
//                libvariant::Variant ala = var["name"];

                if (var.Contains("name"))
                {
                    comboName->setText(QString::fromStdString(var["name"].AsString()));
                }
            }
            catch(...){}
        }
        else if (filter == "Gaussion log file (*.log *.out)")
        {
            succ = m_geometrymodel->loadGaussianOutputGeometry(stream);
        }
        else if (filter == "Gen file (*.gen)")
        {
            succ = m_geometrymodel->loadGenGeometry(stream);
        }
        else if (filter == "HSD file (*.hsd)")
        {
            succ = m_geometrymodel->loadHSDGeometry(stream);
        }
        else if (filter == "Vasp files (POSCAR CONTCAR)")
        {
            QString title;
            bool assumed_elements;
            succ = m_geometrymodel->loadPOSCAR(stream, &title, &assumed_elements);
            comboName->setText(title);

            if (assumed_elements)
            {
                QMessageBox qbox(this);
                qbox.setWindowTitle(tr("DFTBRefDatabaseManager"));
                qbox.setText("No element information found in POSCAR file. Assuming C.");
                qbox.setStandardButtons(QMessageBox::Ok);
                qbox.setIcon(QMessageBox::Warning);
                qbox.exec();
            }

        }

        file.close();
        if (!succ)
        {
            QMessageBox qbox(this);
            qbox.setWindowTitle(tr("DFTBRefDatabaseManager"));
            qbox.setText("Parsing " + filter + " failed. Cancelled.");
            qbox.setStandardButtons(QMessageBox::Ok);
            qbox.setIcon(QMessageBox::Critical);
            qbox.exec();
            return;
        }
        else
        {
//            if (this->m_geometry->m_PBC)
//            {
//                QMessageBox qbox(this);
//                qbox.setText("The lattice information in the input file was not imported due to different format. Please fill the information.");
//                qbox.setStandardButtons(QMessageBox::Ok);
//                qbox.setIcon(QMessageBox::Critical);
//                qbox.exec();
//            }
            if (comboName->text().isEmpty())
            {
                comboName->setText(QFileInfo(file).baseName());
            }
        }
    }
}

void CrystalGeometryEditor::saveGeometry()
{
    QString filter = nullptr;
    QString filename = QFileDialog::getSaveFileName(
           this,
           tr("Save Geometry"),
           QDir::homePath(),
           tr("XYZ file (*.xyz);;Gen file (*.gen);;HSD file (*.hsd)"), &filter);
    if (!filename.isEmpty())
    {
        QFile file(filename);
        if( !file.open( QIODevice::WriteOnly | QIODevice::Text ) )
        {
            qFatal( ("Could not open the file:" + filename).toLatin1() );
            return;
        }
        QTextStream stream(&file);
        if (filter == "XYZ file (*.xyz)")
        {
            stream << m_geometry->getXYZ();
        }
        else if (filter == "Gen file (*.gen)")
        {
            stream << m_geometry->getGen();
        }
        else if (filter == "HSD file (*.hsd)")
        {
            m_geometry->writeOut(stream, "hsd");
        }

        file.close();

    }
}

void CrystalGeometryEditor::clear()
{
    m_geometrymodel->clear();
}

void CrystalGeometryEditor::addAtom()
{
    m_geometrymodel->addAtom("C", 0.0, 0.0, 0.0);
    m_geometryTableView->selectRow(m_geometryTableView->model()->rowCount()-1);
}

void CrystalGeometryEditor::deleteAtom()
{
    QModelIndexList list = m_geometryTableView->selectionModel()->selectedRows();
    if (!list.empty())
    {
        for (int i=list.size()-1; i>=0; --i)
        {
            int row = list.at(i).row();
            m_geometryTableView->model()->removeRows(row,1);
        }
    }
}

bool CrystalGeometryEditor::showTextEditor(QWidget *parent, const QString &title, QString &variable)
{
    QDialog *d = new QDialog(parent);
    d->resize(640,480);
    QLabel *label = new QLabel(title);
    QTextEdit* edit = new QTextEdit();
    edit->setText(variable);
    QGridLayout* layout = new QGridLayout(d);
    layout->addWidget(label,0,0,1,1);
    layout->addWidget(edit,1,0,1,3);
    QObject::connect(edit, &QTextEdit::textChanged, [&edit, &variable](){ variable = edit->document()->toPlainText(); });

    QPushButton* butAccept = new QPushButton("OK");
    layout->addWidget(butAccept,2,2,1,1);
//    QPushButton* butClose = new QPushButton("Cancel");
//    layout->addWidget(butClose,2,3,1,1);

    QObject::connect(butAccept, SIGNAL(clicked()), d, SLOT(accept()));
//    QObject::connect(butClose, SIGNAL(clicked()), d, SLOT(reject()));

    d->setModal(true);
    int ret = d->exec();
    delete d;
    if (ret == QDialog::Accepted)
    {
        return true;
    }
    else
    {
        return false;
    }
}


void CrystalGeometryEditor::editComments()
{
    QString str = m_geometry->comment();
    if (showTextEditor(this, "Comments:", str))
    {
        if (!m_isReadOnly)
        {
            m_geometry->setComment(str);
        }
    }
}

void CrystalGeometryEditor::editCitations()
{
    QString str = m_geometry->citation();
    if (showTextEditor(this, "Citation:", str))
    {
        if (!m_isReadOnly)
        {
            m_geometry->setCitation(str);
        }
    }
}

void CrystalGeometryEditor::setUPE(const QString &str)
{
    m_geometry->setUPE(str);
}

void CrystalGeometryEditor::setBasis()
{
    if (sender() == editBasis)
    {
        m_geometry->setBasis(editBasis->text());
        updateUUID();
    }
}

void CrystalGeometryEditor::setMethod()
{
    if (sender() == editMethod)
    {
        m_geometry->setMethod(editMethod->text());
        updateUUID();
    }
}

void CrystalGeometryEditor::geometryChanged()
{
    updateUUID();
    m_crystalGeometryForm->setupControls();
}


void CrystalGeometryEditor::showKpointDetails()
{
    QDialog d;

    d.resize(640,480);
    d.setWindowTitle("KPoints");

    QVBoxLayout *vbox = new QVBoxLayout;
    vbox->addWidget(kpointsSettingwidget);
    QHBoxLayout *hbox = new QHBoxLayout();
    QPushButton* butAccept = new QPushButton("OK");
    hbox->addStretch(5);
    hbox->addWidget(butAccept);
    QObject::connect(butAccept, SIGNAL(clicked()), &d, SLOT(accept()));
    vbox->addLayout(hbox);
    d.setModal(true);
    d.setLayout(vbox);
    d.exec();
    kpointsSettingwidget->setParent(0);
}

void CrystalGeometryEditor::updateUUID()
{
//    m_geometry->setUUID(m_geometry->createUUID());
//    editUUID->setText(m_geometry->UUID());
}

void CrystalGeometryEditor::setupControllers()
{
    this->resize(700,480);
    QGridLayout *grid = new QGridLayout(this);
    m_geometryTableView = new QTableView();
    m_geometryTableView->verticalHeader()->setVisible(false);
    m_geometryTableView->setSelectionBehavior(QAbstractItemView::SelectRows);

    m_geometryTableView->setModel((QAbstractItemModel*)m_geometrymodel);
    m_geometryTableView->setItemDelegateForColumn(1,new AtomSymbolComboDelegate() );

    connect(m_geometrymodel, SIGNAL(modelChanged()), this, SLOT(geometryChanged()));


    butLoadGeometry = new QPushButton(tr("Load Geometry"));
    connect(butLoadGeometry, SIGNAL(clicked()), this, SLOT(loadGeometry()));

    butSaveGeometry = new QPushButton(tr("Save Geometry"));
    connect(butSaveGeometry, SIGNAL(clicked()), this, SLOT(saveGeometry()));


    butClear = new QPushButton(tr("Clear"));
    connect(butClear, SIGNAL(clicked()), this, SLOT(clear()));

    butAddAtom = new QPushButton(tr("Add atom"));
    connect(butAddAtom, SIGNAL(clicked()), this, SLOT(addAtom()));

    butDeleteAtom = new QPushButton(tr("Delete atom"));
    connect(butDeleteAtom, SIGNAL(clicked()), this, SLOT(deleteAtom()));




    comboName = new QLineEdit();
    comboName->setText(m_geometry->getName());

    connect(comboName, &QLineEdit::textChanged, [=](const QString &newValue) {
     m_geometry->setName(newValue);
     updateUUID();
     } );

    editFormula = new QLineEdit();
    editFormula->setReadOnly(true);
    editFormula->setText(m_geometry->getFormula());

    connect(m_geometrymodel, SIGNAL(formulaChanged(QString)), editFormula,SLOT(setText(QString)));

    editMethod = new UpperCaseLineEdit();
    editMethod->setText(m_geometry->getMethod());
    connect(editMethod, SIGNAL(textChanged(QString)), this, SLOT(setMethod()));

    editBasis = new UpperCaseLineEdit();
    editBasis->setText(m_geometry->getBasis());
    connect(editBasis, SIGNAL(textChanged(QString)), this, SLOT(setBasis()));

    butEditComments = new QPushButton(tr("Comments..."));
    connect(butEditComments, SIGNAL(clicked()), this, SLOT(editComments()));

    butEditCitation = new QPushButton(tr("Citation..."));
    connect(butEditCitation, SIGNAL(clicked()), this, SLOT(editCitations()));

    editUUID = new QLineEdit();
    editUUID->setText(m_geometry->UUID());
    editUUID->setReadOnly(true);

    grid->addWidget(new QLabel(tr("UUID:")),0,0,1,1);
    grid->addWidget(editUUID,0,1,1,5);

    grid->addWidget(new QLabel(tr("Name:")),1,0,1,1);
    grid->addWidget(comboName,1,1,1,3);

    grid->addWidget(new QLabel(tr("Formula:")),1,4,1,1);
    grid->addWidget(editFormula,1,5,1,1);


    grid->addWidget(new QLabel(tr("Method:")),2,0,1,1);
    grid->addWidget(editMethod,2,1,1,3);

    grid->addWidget(new QLabel(tr("Basis:")),2,4,1,1);
    grid->addWidget(editBasis,2,5,1,1);


    kpointsSettingwidget = new KPointsSettingWidget(&m_geometry->m_kpoints_setting);
    kpointsSettingwidget->setHide_klines_setting(true);
    kpointsSettingwidget->updateUI();


    grid->addWidget(new QLabel(tr("K-Points:")),3,0,1,1);
    QHBoxLayout *hbox = new QHBoxLayout;
    auto button = new QPushButton("Details");
    connect(button, SIGNAL(clicked()), this, SLOT(showKpointDetails()));
    hbox->addWidget(button);
    hbox->addStretch(2);

    grid->addLayout(hbox, 3, 1, 1, 3);


    editUPE = new QLineEdit();
    editUPE->setText(QString::number(m_geometry->getUPE()));
    grid->addWidget(new QLabel(tr("Unpaired Elec.:")),3,4,1,1);
    grid->addWidget(editUPE,3,5,1,1);

    editUPE->setValidator(new QDoubleValidator(0.0, 100, 5));
    connect(editUPE, SIGNAL(textChanged(QString)), this, SLOT(setUPE(QString)));


    grid->addWidget(m_crystalGeometryForm,4,0,1,5);
    grid->addWidget(m_geometryTableView, 5,0,7,5);

    QVBoxLayout *vbox = new QVBoxLayout();
    vbox->addSpacing(20);
    vbox->addWidget(butLoadGeometry);
    vbox->addWidget(butSaveGeometry);
    vbox->addWidget(butClear);
    vbox->addSpacing(20);
    vbox->addWidget(butAddAtom);
    vbox->addWidget(butDeleteAtom);
    vbox->addSpacing(20);
    vbox->addWidget(butEditCitation);
    vbox->addWidget(butEditComments);

    grid->addLayout(vbox,4,5,2,1);


    if (m_isReadOnly)
    {
        butLoadGeometry->setEnabled(false);
        butClear->setEnabled(false);
        butAddAtom->setEnabled(false);
        butDeleteAtom->setEnabled(false);

        editMethod->setReadOnly(true);
        editBasis->setReadOnly(true);
        comboName->setReadOnly(true);
        editUPE->setReadOnly(true);

        m_geometryTableView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    }



}
