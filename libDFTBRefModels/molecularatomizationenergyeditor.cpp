#include "molecularatomizationenergyeditor.h"

#include "molecularatomizationenergydata.h"

#include "utils.h"

#include <QLineEdit>
#include <QGridLayout>
#include <QLabel>
#include <QPushButton>
#include <QtWidgets>
#include <QDebug>

#include "UpperCaseLineEdit.h"

#include "SingleSelectTableModelComboBox.h"

MolecularAtomizationEnergyEditor::MolecularAtomizationEnergyEditor(
        MolecularAtomizationEnergyData *data, QAbstractTableModel *geom_ids, QWidget *parent) :
    QWidget(parent)
{
    m_data = data;
    m_geom_ids = geom_ids;
    setupUI();
}

void MolecularAtomizationEnergyEditor::setupUI()
{
    QGridLayout *layout = new QGridLayout();
    this->setLayout(layout);

    layout->addWidget(new QLabel(tr("Reference Geometry:")),0,0,1,1);
    comboRef = new SingleSelectTableModelComboBox();
    comboRef->setModel(m_geom_ids);
    comboRef->setModelColumn(0);
    comboRef->setDisplayColumn(1);
    comboRef->setCurrentIndex(m_data->geom_uuid());

    connect(comboRef, SIGNAL(currentTextChanged(QString)), this, SLOT(setGeomID(QString)));

    layout->addWidget(comboRef,0,1,1,2);

    layout->addWidget(new QLabel(tr("Method:")),1,0,1,1,Qt::AlignRight);
    m_editMethod = new UpperCaseLineEdit();
    m_editMethod->setText(m_data->method());
    connect(m_editMethod, SIGNAL(editingFinished()), this, SLOT(setMethod()));
    layout->addWidget(m_editMethod,1,1,1,2);

    layout->addWidget(new QLabel(tr("Basis:")),2,0,1,1,Qt::AlignRight);
    m_editBasis = new UpperCaseLineEdit();
    m_editBasis->setText(m_data->basis());
    connect(m_editBasis, SIGNAL(editingFinished()), this, SLOT(setBasis()));
    layout->addWidget(m_editBasis,2,1,1,2);

    layout->addWidget(new QLabel(tr("Energy:")),3,0,4,1, Qt::AlignRight | Qt::AlignTop);
    m_editEnergy = new QLineEdit();
    m_editEnergy->setValidator(new QDoubleValidator());
    layout->addWidget(m_editEnergy,3,1,1,1);

    m_combo_energy_unit = new QComboBox();
    m_combo_energy_unit->addItems(QStringList() << "kcal/mol" << "eV" << "H");

    m_combo_energy_unit->setCurrentText(m_data->unit());

    connect(m_combo_energy_unit, SIGNAL(currentTextChanged(QString)), m_data, SLOT(setUnit(QString)));

    layout->addWidget(m_combo_energy_unit,3,2,1,1);

    double energy = m_data->energy();

    m_editEnergy->setText(QString::number(energy));
    connect(m_editEnergy, SIGNAL(editingFinished()), this, SLOT(setEnergy()));



    QPushButton *butEditComments = new QPushButton(tr("Comments..."));
    connect(butEditComments, SIGNAL(clicked()), this, SLOT(editComments()));
    butEditComments->setMaximumWidth(100);
    butEditComments->setAutoDefault(false);

    QPushButton *butEditCitation = new QPushButton(tr("Citation..."));
    connect(butEditCitation, SIGNAL(clicked()), this, SLOT(editCitations()));
    butEditCitation->setMaximumWidth(100);
    butEditCitation->setAutoDefault(false);

    layout->addWidget(butEditComments, 4,0,1,1);
    layout->addWidget(butEditCitation, 5,0,1,1);

}

void MolecularAtomizationEnergyEditor::editComments()
{
    QString str = m_data->comment();
    if (showTextEditor(this, "Comments:", str))
    {
        m_data->setComment(str);
    }
}

void MolecularAtomizationEnergyEditor::editCitations()
{
    QString str = m_data->citation();
    if (showTextEditor(this, "Citation:", str))
    {
        m_data->setCitation(str);
    }
}

bool MolecularAtomizationEnergyEditor::showTextEditor(QWidget *parent, const QString &title, QString &variable)
{
    QDialog *d = new QDialog(parent);
    d->resize(640,480);
    QLabel *label = new QLabel(title);
    QTextEdit* edit = new QTextEdit();
    edit->setText(variable);
    QGridLayout* layout = new QGridLayout(d);
    layout->addWidget(label,0,0,1,1);
    layout->addWidget(edit,1,0,1,3);
    QObject::connect(edit, &QTextEdit::textChanged, [&edit, &variable](){ variable = edit->document()->toPlainText(); });

    QPushButton* butAccept = new QPushButton("OK");
    layout->addWidget(butAccept,2,2,1,1);
//    QPushButton* butClose = new QPushButton("Cancel");
//    layout->addWidget(butClose,2,3,1,1);

    QObject::connect(butAccept, SIGNAL(clicked()), d, SLOT(accept()));
//    QObject::connect(butClose, SIGNAL(clicked()), d, SLOT(reject()));

    d->setModal(true);
    int ret = d->exec();
    delete d;
    if (ret == QDialog::Accepted)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void MolecularAtomizationEnergyEditor::setEnergy()
{
    if (sender() == m_editEnergy)
    {
        m_data->setEnergy(m_editEnergy->text().toDouble());
    }
}

void MolecularAtomizationEnergyEditor::setGeomID(const QString &index)
{
    m_data->setGeom_uuid(index);
}

void MolecularAtomizationEnergyEditor::capitalize()
{
    QObject* obj = sender();
    QLineEdit* edit = qobject_cast<QLineEdit*>(obj);
    if (edit)
    {
        edit->setText(edit->text().toUpper());
    }
}

void MolecularAtomizationEnergyEditor::setMethod()
{
    if (sender() == m_editMethod)
    {
        m_data->setMethod(m_editMethod->text());
    }
}

void MolecularAtomizationEnergyEditor::setBasis()
{
    if (sender() == m_editBasis)
    {
        m_data->setBasis(m_editBasis->text());
    }
}
