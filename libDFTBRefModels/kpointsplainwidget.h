#ifndef KPOINTSPLAINWIDGET_H
#define KPOINTSPLAINWIDGET_H

#include <QWidget>
#include <QtWidgets>

class KPointsPlainModel;
class KPointsPlainWidget : public QWidget
{
    Q_OBJECT
public:
    explicit KPointsPlainWidget(QWidget *parent = 0);

public slots:
    KPointsPlainModel* getModel() const;
public:
    void setModel(KPointsPlainModel* model);
    void setEditable(bool enable);
private slots:
    void addRow();
    void removeRow();
    void importPlainInput();
    void importHSDInput();
private:
    QTableView *m_tableView;
    KPointsPlainModel* m_model;
    QPushButton *butImportPlainFile;
    QPushButton *butImportHSDFile;
    QPushButton *butAddRow;
    QPushButton *butRemoveRow;
    QGroupBox *buttonBox;

};

#endif // KPOINTSPLAINWIDGET_H
