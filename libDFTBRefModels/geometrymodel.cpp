#include "geometrymodel.h"

#include "geometry.h"
#include "bandstructuredata.h"
#include "molecularatomizationenergydata.h"
#include "molecularfrequencydata.h"
#include "reactionenergydata.h"


GeometryModel::GeometryModel(QObject *parent)
    : ReferenceDataModel(parent)
{
}

int GeometryModel::rowCount(const QModelIndex &parent) const
{
    return m_data.size();
}

int GeometryModel::columnCount(const QModelIndex &parent) const
{
    return 10;
}

QVariant GeometryModel::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole || role == Qt::EditRole)
    {
        int row = index.row();
        int col = index.column();
        auto geom = m_data.at(row);
        switch(col)
        {
            case 0: //UUID
                return QVariant(geom->UUID());
            case 1: //Name
                return QVariant(geom->getName());
            case 2: //Formula
                geom->computeFormula();
                return QVariant(geom->getFormula());
            case 3: //Method
                return QVariant(geom->getMethod());
            case 4: //Basis
                return QVariant(geom->getBasis());
            case 5: //PBC
                return QVariant(geom->PBC());
            case 6: //Spin
                if (geom->PBC())
                {
                    return "-";
                }
                return QVariant(geom->getSpin());
            case 7: //Charge
                if (geom->PBC())
                {
                    return "-";
                }
                return QVariant(geom->getCharge());
            case 8: //isStaionary
                if (geom->PBC())
                {
                    return "-";
                }
                return QVariant(geom->isStationary());
            case 9: //Elements
                return QVariant(geom->getDBElementString());
        }
    }
    return ReferenceDataModel::data(index, role);
}

void GeometryModel::addGeometry(const std::shared_ptr<Geometry> &geom)
{
    QString old_uuid = geom->UUID();
    QString new_uuid = geom->computeUUID();

    m_UUID_mapping.insert(old_uuid, new_uuid);
    geom->setUUID(new_uuid);

    if (m_allUUIDs.contains(new_uuid))
    {
        return ;
    }
    int old_size = m_data.size();
    beginInsertRows(QModelIndex(), old_size, old_size);
    m_data.append(geom);
    m_checkedItems.insert(new_uuid);
    m_allUUIDs.insert(new_uuid);
    endInsertRows();
}

void GeometryModel::clear()
{
    ReferenceDataModel::clear();
    m_data.clear();
    m_allUUIDs.clear();
}
QMap<QString, QString> GeometryModel::UUID_mapping() const
{
    return m_UUID_mapping;
}

QMap<QString, QVariant> GeometryModel::getMapping(int column)
{
    QMap<QString, QVariant> res;
    for(int i=0; i<rowCount(QModelIndex());++i)
    {
        auto index_ = this->index(i, column);
        auto uuid_index = this->index(i, 0);
        res.insert(data(uuid_index,Qt::EditRole).toString(),
                   data(index_,Qt::DisplayRole));
    }
    return res;
}



QVariant GeometryModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
    {
        int col = section;
        switch(col)
        {
            case 1: //Name
                return QVariant("Name");
            case 2: //Formula
                return QVariant("Formula");
            case 3: //Method
                return QVariant("Method");
            case 4: //Basis
                return QVariant("Basis");
            case 5: //PBC
                return QVariant("PBC");
            case 6: //Spin
                return QVariant("Spin");
            case 7: //Charge
                return QVariant("Charge");
            case 8: //isStaionary
                return QVariant("isStaionary");
            case 9: //Elements
                return QVariant("Elements");
        }
        return ReferenceDataModel::headerData(section, orientation, role);
    }
    return ReferenceDataModel::headerData(section, orientation, role);
}

bool ReferenceDataModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (role == Qt::CheckStateRole && index.column() == 0)
    {
        QString key = data(index, Qt::EditRole).toString();
        if (value == Qt::Checked)
        {
            m_checkedItems.insert(key);
        }
        else
        {
            m_checkedItems.remove(key);
        }
        return true;
    }
    return QAbstractTableModel::setData(index, value);
}


ReferenceDataModel::ReferenceDataModel(QObject *parent) : QAbstractTableModel(parent)
{

}

QVariant ReferenceDataModel::data(const QModelIndex &index, int role) const
{
    int col = index.column();
    if (role == Qt::CheckStateRole && index.column() == 0)
    {
        QString key = data(index, Qt::EditRole).toString();
        if (m_checkedItems.contains(key))
        {
             return Qt::Checked;
        }
        else
        {
            return Qt::Unchecked;
        }
    }
    else if (role == Qt::DisplayRole && index.column() == 0)
    {
        return QVariant();
    }

    if (role == Qt::DisplayRole)
    {
        if (m_data_mapping.contains(col))
        {
            QString key = data(index, Qt::EditRole).toString();
            if (m_data_mapping[col].contains(key))
            {
                return m_data_mapping[col][key];
            }
        }
    }
    return QVariant();
}

void ReferenceDataModel::clear()
{
    m_checkedItems.clear();
}

QVariant ReferenceDataModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
    {
        if (section == 0)
        {
            return QVariant();
        }
    }
    return QAbstractTableModel::headerData(section, orientation, role);
}

Qt::ItemFlags ReferenceDataModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags flags = QAbstractTableModel::flags(index);
    if (index.column() == 0)
    {
        flags |= Qt::ItemIsUserCheckable | Qt::ItemIsEditable | Qt::ItemIsEnabled;
    }
    return flags;
}

void ReferenceDataModel::setDataMapping(int column, const QMap<QString, QVariant> &mapping)
{
    m_data_mapping[column] = mapping;
}

void ReferenceDataModel::removeDataMapping(int column)
{
    m_data_mapping.remove(column);
}

bool ReferenceDataModel::isChecked(const QString &uuid) const
{
    return m_checkedItems.contains(uuid);
}

//*****************************************************************************************
int BandstructureDataModel::columnCount(const QModelIndex &parent) const
{
    return 6;
}

QVariant BandstructureDataModel::data(const QModelIndex &index, int role) const
{
    int col = index.column();
    if (role == Qt::DisplayRole && m_data_mapping.contains(col))
    {
        return ReferenceDataModel::data(index, role);
    }
    if (role == Qt::DisplayRole || role == Qt::EditRole)
    {
        int row = index.row();
        auto data = m_data.at(row);
        switch(col)
        {
            case 0: //UUID
                return QVariant(data->UUID());
            case 1: //Name
                return QVariant(data->getName());
            case 2: //Geom UUID
                return QVariant(data->getGeomID());
            case 3: //Method
                return QVariant(data->method());
            case 4: //Basis
                return QVariant(data->basis());
            case 5: //Elements
                return QVariant(data->getGeomID());
        }
    }
    return ReferenceDataModel::data(index, role);
}

QVariant BandstructureDataModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
    {
        int col = section;
        switch(col)
        {
            case 1: //Name
                return QVariant("Name");
            case 2: //Ref. Geom
                return QVariant("Ref. Geometry");
            case 3: //Method
                return QVariant("Method");
            case 4: //Basis
                return QVariant("Basis");
            case 5: //Elements
                return QVariant("Elements");
        }
        return ReferenceDataModel::headerData(section, orientation, role);
    }
    return ReferenceDataModel::headerData(section, orientation, role);
}


//*****************************************************************************************
int AtomizationEnergyModel::columnCount(const QModelIndex &parent) const
{
    return 7;
}

QVariant AtomizationEnergyModel::data(const QModelIndex &index, int role) const
{
    int col = index.column();
    if (role == Qt::DisplayRole && m_data_mapping.contains(col))
    {
        return ReferenceDataModel::data(index, role);
    }
    if (role == Qt::DisplayRole || role == Qt::EditRole)
    {
        int row = index.row();
        auto data = m_data.at(row);
        switch(col)
        {
            case 0: //UUID
                return QVariant(data->UUID());
            case 1: //Geom UUID
                return QVariant(data->geom_uuid());
            case 2: //Method
                return QVariant(data->method());
            case 3: //Basis
                return QVariant(data->basis());
            case 4: //Energy
                return QVariant(data->energy());
            case 5: //EnergyUnit
                return QVariant(data->unit());
            case 6: //Elements
                return QVariant(data->geom_uuid());
        }
    }
    return ReferenceDataModel::data(index, role);
}


QVariant AtomizationEnergyModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
    {
        int col = section;
        switch(col)
        {
            case 1: //Ref. Geom
                return QVariant("Ref. Geometry");
            case 2: //Method
                return QVariant("Method");
            case 3: //Basis
                return QVariant("Basis");
            case 4: //Basis
                return QVariant("Energy");
            case 5: //Basis
                return QVariant("Unit");
            case 6: //Elements
                return QVariant("Elements");
        }
        return ReferenceDataModel::headerData(section, orientation, role);
    }
    return ReferenceDataModel::headerData(section, orientation, role);
}

//*****************************************************************************************
int FrequencyModel::columnCount(const QModelIndex &parent) const
{
    return 5;
}

QVariant FrequencyModel::data(const QModelIndex &index, int role) const
{
    int col = index.column();
    if (role == Qt::DisplayRole && m_data_mapping.contains(col))
    {
        return ReferenceDataModel::data(index, role);
    }
    if (role == Qt::DisplayRole || role == Qt::EditRole)
    {
        int row = index.row();
        auto data = m_data.at(row);
        switch(col)
        {
            case 0: //UUID
                return QVariant(data->UUID());
            case 1: //Geom UUID
                return QVariant(data->geom_uuid());
            case 2: //Method
                return QVariant(data->method());
            case 3: //Basis
                return QVariant(data->basis());
            case 4: //Elements
                return QVariant(data->geom_uuid());
        }
    }
    return ReferenceDataModel::data(index, role);
}


QVariant FrequencyModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
    {
        int col = section;
        switch(col)
        {
            case 1: //Ref. Geom
                return QVariant("Ref. Geometry");
            case 2: //Method
                return QVariant("Method");
            case 3: //Basis
                return QVariant("Basis");
            case 4: //Elements
                return QVariant("Elements");
        }
        return ReferenceDataModel::headerData(section, orientation, role);
    }
    return ReferenceDataModel::headerData(section, orientation, role);
}

//*****************************************************************************************
int ReactionEnergyModel::columnCount(const QModelIndex &parent) const
{
    return 7;
}

QVariant ReactionEnergyModel::data(const QModelIndex &index, int role) const
{
    int col = index.column();
    if (role == Qt::DisplayRole && m_data_mapping.contains(col))
    {
        return ReferenceDataModel::data(index, role);
    }
    if (role == Qt::DisplayRole || role == Qt::EditRole)
    {
        int row = index.row();
        auto data = m_data.at(row);
        switch(col)
        {
            case 0: //UUID
                return QVariant(data->UUID());
            case 1: //Name
                return QVariant(data->name());
            case 2: //Method
                return QVariant(data->method());
            case 3: //Basis
                return QVariant(data->basis());
            case 4: //Energy
                return QVariant(data->energy());
            case 5: //EnergyUnit
                return QVariant(data->unit());
            case 6: //Elements
                return QVariant(data->getOptimizeComponents());
            break;
        }
    }
    return ReferenceDataModel::data(index, role);
}


QVariant ReactionEnergyModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
    {
        int col = section;
        switch(col)
        {
            case 1: //Name
                return QVariant("Name");
            case 2: //Method
                return QVariant("Method");
            case 3: //Basis
                return QVariant("Basis");
            case 4: //Basis
                return QVariant("Energy");
            case 5: //Basis
                return QVariant("Unit");
            case 6: //OptimizeComponents
                return QVariant("OptimizeComponents");
        }
        return ReferenceDataModel::headerData(section, orientation, role);
    }
    return ReferenceDataModel::headerData(section, orientation, role);
}
