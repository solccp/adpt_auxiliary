#ifndef BANDSTRUCTUREVIEWER_H
#define BANDSTRUCTUREVIEWER_H

#include <QWidget>
#include <QPushButton>
#include <QTableView>
#include "bandstructurebandsmodel.h"
#include <qcustomplot.h>


class BandStructureWidget : public QWidget
{
    Q_OBJECT
public:
    explicit BandStructureWidget(QWidget *parent = 0);
    
    BandStructureBandsModel *bandStructureModel() const;
    void setBandStructureModel(BandStructureBandsModel *data);

    QCustomPlot* getPreviewPlot() const;


    bool editable() const;
    void setEditable(bool editable);

signals:
    
private slots:
    void importPlainInput();
    void clear();
    void showFittingArea();
    void exportPlainText();
public slots:
    void replotGraph();
    void prepareGraph();


private:
    QTableView *m_tableView;
    BandStructureBandsModel *m_model;

    QPushButton *butImportPlainFile;
    QPushButton *butExportPlainText;
    QPushButton *butClear;
//    QPushButton *butPreview;
    QPushButton *butShowFittingArea;

    bool m_editable = true;
    
    QCustomPlot* m_customplot = nullptr;

    const QVector<Qt::GlobalColor> colors = {Qt::red, Qt::blue, Qt::green, Qt::cyan, Qt::magenta, Qt::gray, Qt::darkRed, Qt::darkBlue, Qt::darkGreen, Qt::darkCyan, Qt::darkMagenta, Qt::darkGray, Qt::darkYellow};

    QList<QCPDataMap*> m_banddata;


    void shiftData(double EF);
    double y_min = 0.0;
    double cur_EF = 0.0;

};

#endif // BANDSTRUCTUREVIEWER_H
