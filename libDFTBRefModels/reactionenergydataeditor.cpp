#include "reactionenergydataeditor.h"
#include "reactioncomponentwidget.h"

#include "reactionenergydata.h"
#include "reactioncomponentmodel.h"

#include "utils.h"

#include <QtWidgets>

#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QDoubleValidator>
#include <QPushButton>
#include <QCheckBox>
#include "UpperCaseLineEdit.h"


ReactionEnergyDataEditor::ReactionEnergyDataEditor(ReactionEnergyData *data, DFTBRefDatabase *database, bool ReadOnly, QWidget *parent) :
    QWidget(parent), m_ReadOnly(ReadOnly)
{
    m_data = data;
    m_database = database;
    setupUI();
}

void ReactionEnergyDataEditor::setName()
{
    if (sender() == m_editName)
    {
        m_data->setName(m_editName->text());
    }
}

void ReactionEnergyDataEditor::setMethod()
{
    if (sender() == m_editMethod)
    {
        m_data->setMethod(m_editMethod->text());
    }
}

void ReactionEnergyDataEditor::setBasis()
{
    if (sender() == m_editBasis)
    {
        m_data->setBasis(m_editBasis->text());
    }
}

void ReactionEnergyDataEditor::setEnergy()
{
    if (sender() == m_editEnergy)
    {
        m_data->setEnergy(m_editEnergy->text().toDouble());
    }
}

void ReactionEnergyDataEditor::setupUI()
{
    QGridLayout *layout = new QGridLayout();
    this->setLayout(layout);

    layout->addWidget(new QLabel(tr("Name:")),0,0,1,1,Qt::AlignRight);
    m_editName = new QLineEdit();
    m_editName->setText(m_data->name());
    connect(m_editName, SIGNAL(editingFinished()), this, SLOT(setName()));
    layout->addWidget(m_editName,0,1,1,2);

    m_opt_comp = new QCheckBox(tr("Optimize components"));
    m_opt_comp->setChecked(m_data->getOptimizeComponents());
    connect(m_opt_comp, SIGNAL(toggled(bool)), m_data, SLOT(setOptimizeComponents(bool)));
    layout->addWidget(m_opt_comp,0,3,1,1);


    layout->addWidget(new QLabel(tr("Method:")),1,0,1,1,Qt::AlignRight);
    m_editMethod = new UpperCaseLineEdit();
    m_editMethod->setText(m_data->method());
    connect(m_editMethod, SIGNAL(editingFinished()), this, SLOT(setMethod()));
    layout->addWidget(m_editMethod,1,1,1,1);

    layout->addWidget(new QLabel(tr("Basis:")),1,2,1,1,Qt::AlignRight);
    m_editBasis = new UpperCaseLineEdit();
    m_editBasis->setText(m_data->basis());
    connect(m_editBasis, SIGNAL(editingFinished()), this, SLOT(setBasis()));
    layout->addWidget(m_editBasis,1,3,1,1);

    layout->addWidget(new QLabel(tr("Energy:")),2,0,1,1, Qt::AlignRight);
    m_editEnergy = new QLineEdit();
    m_editEnergy->setValidator(new QDoubleValidator());

    layout->addWidget(m_editEnergy,2,1,1,1);


    m_combo_energy_unit = new QComboBox();
    m_combo_energy_unit->addItems(QStringList() << "kcal/mol" << "eV" << "H");

    m_combo_energy_unit->setCurrentText(m_data->unit());

    connect(m_combo_energy_unit, SIGNAL(currentTextChanged(QString)), m_data, SLOT(setUnit(QString)));


    layout->addWidget(m_combo_energy_unit,2,2,1,1);

    double energy = m_data->energy();
    m_editEnergy->setText(QString::number(energy));
    connect(m_editEnergy, SIGNAL(editingFinished()), this, SLOT(setEnergy()));



    reactantModel = new ReactionComponentModel(m_database);
    productModel = new ReactionComponentModel(m_database);

    reactantModel->setComponents(m_data->reactants());
    productModel->setComponents(m_data->products());

    connect(reactantModel, SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(updateReactantComponents()));
    connect(productModel, SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(updateProductComponents()));




    m_reactantsWidget = new ReactionComponentWidget(reactantModel,m_database);
    m_productsWidget = new ReactionComponentWidget(productModel,m_database);

    layout->addWidget(new QLabel(tr("Reactants:")),3,0,3,1, Qt::AlignTop);
    layout->addWidget(m_reactantsWidget,3,1,3,3);


    layout->addWidget(new QLabel(tr("Products:")),6,0,3,1, Qt::AlignTop);
    layout->addWidget(m_productsWidget,6,1,3,3);

    QPushButton *butEditComments = new QPushButton(tr("Comments..."));
    connect(butEditComments, SIGNAL(clicked()), this, SLOT(editComments()));
    butEditComments->setMaximumWidth(100);
    butEditComments->setAutoDefault(false);

    QPushButton *butEditCitation = new QPushButton(tr("Citation..."));
    connect(butEditCitation, SIGNAL(clicked()), this, SLOT(editCitations()));
    butEditCitation->setMaximumWidth(100);
    butEditCitation->setAutoDefault(false);

    layout->addWidget(butEditComments, 9,0,1,1);
    layout->addWidget(butEditCitation, 10,0,1,1);

    if (m_ReadOnly)
    {
        m_editEnergy->setReadOnly(true);
        m_editBasis->setReadOnly(true);
        m_editMethod->setReadOnly(true);
        m_editName->setReadOnly(true);
        m_opt_comp->setEnabled(false);
        m_reactantsWidget->setEditable(false);
        m_productsWidget->setEditable(false);
    }



}

bool ReactionEnergyDataEditor::showTextEditor(QWidget *parent, const QString &title, QString &variable)
{
    QDialog *d = new QDialog(parent);
    d->resize(640,480);
    QLabel *label = new QLabel(title);
    QTextEdit* edit = new QTextEdit();
    edit->setText(variable);
    QGridLayout* layout = new QGridLayout(d);
    layout->addWidget(label,0,0,1,1);
    layout->addWidget(edit,1,0,1,3);
    QObject::connect(edit, &QTextEdit::textChanged, [&edit, &variable](){ variable = edit->document()->toPlainText(); });

    QPushButton* butAccept = new QPushButton("OK");
    layout->addWidget(butAccept,2,2,1,1);
//    QPushButton* butClose = new QPushButton("Cancel");
//    layout->addWidget(butClose,2,3,1,1);

    QObject::connect(butAccept, SIGNAL(clicked()), d, SLOT(accept()));
//    QObject::connect(butClose, SIGNAL(clicked()), d, SLOT(reject()));

    d->setModal(true);
    int ret = d->exec();
    delete d;
    if (ret == QDialog::Accepted)
    {
        return true;
    }
    else
    {
        return false;
    }
}



void ReactionEnergyDataEditor::editComments()
{
    QString str;
    if (showTextEditor(this, "Comments:", str))
    {
        if (!m_ReadOnly)
        {
            m_data->setComment(str);
        }
    }
}

void ReactionEnergyDataEditor::editCitations()
{
    QString str = m_data->citation();
    if (showTextEditor(this, "Citation:", str))
    {
        if (!m_ReadOnly)
        {
            m_data->setCitation(str);
        }
    }
}

void ReactionEnergyDataEditor::updateReactantComponents()
{
    m_data->setReactants(reactantModel->components());
}

void ReactionEnergyDataEditor::updateProductComponents()
{
    m_data->setProducts(productModel->components());
}
