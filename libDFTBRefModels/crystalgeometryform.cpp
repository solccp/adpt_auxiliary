#include "crystalgeometryform.h"
#include "ui_crystalgeometryform.h"
#include <QSignalMapper>
#include <QDebug>

#include "geometry.h"
#include <array>

CrystalGeometryForm::CrystalGeometryForm(Geometry *geom, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CrystalGeometryForm)
{
    ui->setupUi(this);
    signalMapper = new QSignalMapper(this);
    m_geometry = geom;
    createControls();
    setupControls();
}

CrystalGeometryForm::~CrystalGeometryForm()
{
    delete ui;
}

void CrystalGeometryForm::textChanged(int id)
{
    switch(id)
    {
    case 1:
        m_geometry->m_scaling_factor = ui->edit_lattice_a->text().toDouble();
        break;
    case 111:
        m_geometry->m_lattice_vectors[0][0]  = ui->edit_latticevec_1_1->text().toDouble();
        break;
    case 112:
        m_geometry->m_lattice_vectors[0][1] = ui->edit_latticevec_1_2->text().toDouble();
        break;
    case 113:
        m_geometry->m_lattice_vectors[0][2] = ui->edit_latticevec_1_3->text().toDouble();
        break;
    case 121:
        m_geometry->m_lattice_vectors[1][0] = ui->edit_latticevec_2_1->text().toDouble();
        break;
    case 122:
        m_geometry->m_lattice_vectors[1][1] = ui->edit_latticevec_2_2->text().toDouble();
        break;
    case 123:
        m_geometry->m_lattice_vectors[1][2] = ui->edit_latticevec_2_3->text().toDouble();
        break;
    case 131:
        m_geometry->m_lattice_vectors[2][0] = ui->edit_latticevec_3_1->text().toDouble();
        break;
    case 132:
        m_geometry->m_lattice_vectors[2][1] = ui->edit_latticevec_3_2->text().toDouble();
        break;
    case 133:
        m_geometry->m_lattice_vectors[2][2] = ui->edit_latticevec_3_3->text().toDouble();
        break;
    case 900:
        m_geometry->m_relativeCoordinates = ui->checkRelativeCoords ->isChecked();
    }

    emit modelChanged();

}


void CrystalGeometryForm::setupControls()
{
    ui->checkRelativeCoords->setChecked(m_geometry->m_relativeCoordinates);

    ui->edit_lattice_a->setText(QString("%1").arg(m_geometry->m_scaling_factor));

    ui->edit_latticevec_1_1->setText(QString("%1").arg(m_geometry->m_lattice_vectors[0][0]));
    ui->edit_latticevec_1_2->setText(QString("%1").arg(m_geometry->m_lattice_vectors[0][1]));
    ui->edit_latticevec_1_3->setText(QString("%1").arg(m_geometry->m_lattice_vectors[0][2]));
    ui->edit_latticevec_2_1->setText(QString("%1").arg(m_geometry->m_lattice_vectors[1][0]));
    ui->edit_latticevec_2_2->setText(QString("%1").arg(m_geometry->m_lattice_vectors[1][1]));
    ui->edit_latticevec_2_3->setText(QString("%1").arg(m_geometry->m_lattice_vectors[1][2]));
    ui->edit_latticevec_3_1->setText(QString("%1").arg(m_geometry->m_lattice_vectors[2][0]));
    ui->edit_latticevec_3_2->setText(QString("%1").arg(m_geometry->m_lattice_vectors[2][1]));
    ui->edit_latticevec_3_3->setText(QString("%1").arg(m_geometry->m_lattice_vectors[2][2]));
}

void CrystalGeometryForm::createControls()
{
    QDoubleValidator *dv = new QDoubleValidator(this);
    dv->setBottom(0.0);
    ui->edit_lattice_a->setValidator(dv);

    connect(ui->edit_lattice_a, SIGNAL(textChanged(QString)), signalMapper, SLOT(map()));

    signalMapper->setMapping(ui->edit_lattice_a, 1);

    dv = new QDoubleValidator(this);
    ui->edit_latticevec_1_1->setValidator(dv);
    ui->edit_latticevec_1_2->setValidator(dv);
    ui->edit_latticevec_1_3->setValidator(dv);
    ui->edit_latticevec_2_1->setValidator(dv);
    ui->edit_latticevec_2_2->setValidator(dv);
    ui->edit_latticevec_2_3->setValidator(dv);
    ui->edit_latticevec_3_1->setValidator(dv);
    ui->edit_latticevec_3_2->setValidator(dv);
    ui->edit_latticevec_3_3->setValidator(dv);



    connect(ui->edit_latticevec_1_1, SIGNAL(textChanged(QString)), signalMapper, SLOT(map()));
    connect(ui->edit_latticevec_1_2, SIGNAL(textChanged(QString)), signalMapper, SLOT(map()));
    connect(ui->edit_latticevec_1_3, SIGNAL(textChanged(QString)), signalMapper, SLOT(map()));
    connect(ui->edit_latticevec_2_1, SIGNAL(textChanged(QString)), signalMapper, SLOT(map()));
    connect(ui->edit_latticevec_2_2, SIGNAL(textChanged(QString)), signalMapper, SLOT(map()));
    connect(ui->edit_latticevec_2_3, SIGNAL(textChanged(QString)), signalMapper, SLOT(map()));
    connect(ui->edit_latticevec_3_1, SIGNAL(textChanged(QString)), signalMapper, SLOT(map()));
    connect(ui->edit_latticevec_3_2, SIGNAL(textChanged(QString)), signalMapper, SLOT(map()));
    connect(ui->edit_latticevec_3_3, SIGNAL(textChanged(QString)), signalMapper, SLOT(map()));

    signalMapper->setMapping(ui->edit_latticevec_1_1, 111);
    signalMapper->setMapping(ui->edit_latticevec_1_2, 112);
    signalMapper->setMapping(ui->edit_latticevec_1_3, 113);
    signalMapper->setMapping(ui->edit_latticevec_2_1, 121);
    signalMapper->setMapping(ui->edit_latticevec_2_2, 122);
    signalMapper->setMapping(ui->edit_latticevec_2_3, 123);
    signalMapper->setMapping(ui->edit_latticevec_3_1, 131);
    signalMapper->setMapping(ui->edit_latticevec_3_2, 132);
    signalMapper->setMapping(ui->edit_latticevec_3_3, 133);

    connect(ui->checkRelativeCoords, SIGNAL(toggled(bool)), signalMapper, SLOT(map()));
//    connect(ui->checkRelativeCoords, SIGNAL(clicked(bool)), signalMapper, SLOT(map()));
    signalMapper->setMapping(ui->checkRelativeCoords, 900);

    connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(textChanged(int)));

}

void CrystalGeometryForm::on_comboBox_currentIndexChanged(int index)
{

    switch(index)
    {
    case 1: //SC
        ui->edit_latticevec_1_1->setText("1");
        ui->edit_latticevec_1_2->setText("0");
        ui->edit_latticevec_1_3->setText("0");
        ui->edit_latticevec_2_1->setText("0");
        ui->edit_latticevec_2_2->setText("1");
        ui->edit_latticevec_2_3->setText("0");
        ui->edit_latticevec_3_1->setText("0");
        ui->edit_latticevec_3_2->setText("0");
        ui->edit_latticevec_3_3->setText("1");
//        ui->checkRelativeCoords->setChecked(true);
        break;
    case 2: //BCC
        ui->edit_latticevec_1_1->setText("-0.5");
        ui->edit_latticevec_1_2->setText("0.5");
        ui->edit_latticevec_1_3->setText("0.5");
        ui->edit_latticevec_2_1->setText("0.5");
        ui->edit_latticevec_2_2->setText("-0.5");
        ui->edit_latticevec_2_3->setText("0.5");
        ui->edit_latticevec_3_1->setText("0.5");
        ui->edit_latticevec_3_2->setText("0.5");
        ui->edit_latticevec_3_3->setText("-0.5");
//        ui->checkRelativeCoords->setChecked(true);
        break;
    case 3: //FCC
        ui->edit_latticevec_1_1->setText("0");
        ui->edit_latticevec_1_2->setText("0.5");
        ui->edit_latticevec_1_3->setText("0.5");
        ui->edit_latticevec_2_1->setText("0.5");
        ui->edit_latticevec_2_2->setText("0");
        ui->edit_latticevec_2_3->setText("0.5");
        ui->edit_latticevec_3_1->setText("0.5");
        ui->edit_latticevec_3_2->setText("0.5");
        ui->edit_latticevec_3_3->setText("0");
//        ui->checkRelativeCoords->setChecked(true);
        break;
    case 4: //HCP
        ui->edit_latticevec_1_1->setText("0.5");
        ui->edit_latticevec_1_2->setText("0.866025");
        ui->edit_latticevec_1_3->setText("0.0");
        ui->edit_latticevec_2_1->setText("0.5");
        ui->edit_latticevec_2_2->setText("-0.866025");
        ui->edit_latticevec_2_3->setText("0.0");
        ui->edit_latticevec_3_1->setText("0.0");
        ui->edit_latticevec_3_2->setText("0.0");
        ui->edit_latticevec_3_3->setText("1.633");
//        ui->checkRelativeCoords->setChecked(true);
        break;
    }
    textChanged(900);
}
