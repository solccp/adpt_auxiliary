#ifndef KLINESDATA_H
#define KLINESDATA_H

#include <QAbstractTableModel>
#include <array>
#include <QVariant>
#include <QList>
#include <QTextStream>


namespace ADPT
{
    class KPointsSetting;
}
class KLinesModel : public QAbstractTableModel
{
public:
    KLinesModel(ADPT::KPointsSetting* data, QObject* parent = 0);

public:
    virtual int columnCount(const QModelIndex &parent) const;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    virtual int rowCount(const QModelIndex &) const;
    virtual QVariant data(const QModelIndex &index, int role) const;
    virtual QModelIndex index(int row, int column, const QModelIndex &parent) const;
    virtual QModelIndex parent(const QModelIndex &child) const;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role);
    virtual Qt::ItemFlags flags(const QModelIndex &index) const;
    virtual bool insertRows(int row, int count, const QModelIndex &parent);
    virtual bool removeRows(int row, int count, const QModelIndex &parent);

public slots:
    bool importPlainFile(const QString& filename);
    bool importVaspKPointsFile(const QString& filename);
    bool importHSDFile(const QString& filename);
//    int numOfPoints();
private:
    bool readKLinesVasp(QTextStream &stream);
    bool readKLinesString(QTextStream &stream);
private:
    ADPT::KPointsSetting* m_data;

};

#endif // KLINESDATA_H
