#ifndef ATOMSYMBOLCOMBODELEGATE_H
#define ATOMSYMBOLCOMBODELEGATE_H

#include <QStyledItemDelegate>

class QComboBox;
class AtomSymbolComboDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    explicit AtomSymbolComboDelegate(QObject *parent = 0);
    
signals:
private slots:
    void changed();
public slots:
    // QAbstractItemDelegate interface
public:
    virtual QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const;
    virtual void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
    virtual void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;
};

#endif // ATOMSYMBOLCOMBODELEGATE_H
