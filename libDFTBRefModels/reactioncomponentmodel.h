#ifndef REACTIONCOMPONENTMODEL_H
#define REACTIONCOMPONENTMODEL_H

#include "dftbrefdatabase.h"
#include "reactionenergydata.h"

#include <QAbstractTableModel>

class ReactionComponentModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    ReactionComponentModel(DFTBRefDatabase *database, QObject *parent = 0);

signals:

public slots:
    const QList<ReactionItem>& components() const;
    QList<ReactionItem>& components();
    void setComponents(const QList<ReactionItem> &components);
private:
    DFTBRefDatabase *m_database;
    QList<ReactionItem> m_components;

    // QAbstractItemModel interface
public:
    int rowCount(const QModelIndex &) const;
    int columnCount(const QModelIndex &) const;
    QVariant data(const QModelIndex &index, int role) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    bool insertRows(int row, int count, const QModelIndex &parent);
    bool removeRows(int row, int count, const QModelIndex &parent);
    Qt::ItemFlags flags(const QModelIndex &index) const;

};

#endif // REACTIONCOMPONENTMODEL_H
