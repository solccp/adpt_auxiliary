#include "singleselectlistwidget.h"
#include <QtWidgets>

#include <QSortFilterProxyModel>
#include <QVariant>

SingleSelectListWidget::SingleSelectListWidget(QWidget *parent) :
    QWidget(parent)
{
    setupUI();
}

void SingleSelectListWidget::setIndexColumn(int index)
{
    m_indexColumn = index;
}

int SingleSelectListWidget::indexColumn() const
{
    return m_indexColumn;
}

void SingleSelectListWidget::setModel(QAbstractItemModel *model)
{
    if (m_model)
    {
        m_model->deleteLater();
    }
    m_model = new QSortFilterProxyModel(this);
    m_model->setSourceModel(model);

    if (!m_filter->text().isEmpty())
    {
        m_model->setFilterRegExp(m_filter->text());
        m_model->setFilterCaseSensitivity(Qt::CaseInsensitive);
    }


    for(int i=0; i<m_model->columnCount();++i)
    {
        if (i == m_indexColumn)
        {
            continue;
        }
        m_filterColumn->addItem(m_model->headerData(i, Qt::Horizontal).toString(), i);
    }
    m_model->setFilterKeyColumn(m_filterColumn->itemData(0).toInt());


    m_tableView->setModel(m_model);
    m_tableView->resizeColumnsToContents();
    m_tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Interactive);
    m_tableView->hideColumn(m_indexColumn);
    setupView();
}

void SingleSelectListWidget::filterChanged(const QString &filter_text)
{
    m_model->setFilterRegExp(filter_text);
    m_model->setFilterCaseSensitivity(Qt::CaseInsensitive);
    m_model->setFilterKeyColumn(m_filterColumn->currentData().toInt());
    setupView();
}

void SingleSelectListWidget::filterChanged(int index)
{
    m_model->setFilterRegExp(m_filter->text());
    m_model->setFilterCaseSensitivity(Qt::CaseInsensitive);
    m_model->setFilterKeyColumn(m_filterColumn->itemData(index).toInt());
    setupView();
}

void SingleSelectListWidget::itemSelected(QModelIndex index)
{
    if (!index.isValid())
        return;

    m_selectedIndex = m_model->mapToSource(index);

}

void SingleSelectListWidget::itemDoubleClicked(const QModelIndex index)
{
    if (!index.isValid())
        return;

    m_selectedIndex = m_model->mapToSource(index);
    emit valueSelected(m_selectedIndex.row(), currentIndex(), currentDisplayText());
}
int SingleSelectListWidget::displayColumn() const
{
    return m_displayColumn;
}

void SingleSelectListWidget::setDisplayColumn(int displayColumn)
{
    m_displayColumn = displayColumn;
}


void SingleSelectListWidget::setupView()
{
    QModelIndex newIndex = m_model->mapFromSource(m_selectedIndex);
    if (newIndex.isValid())
    {
        m_tableView->selectRow(newIndex.row());
    }
}

void SingleSelectListWidget::setupUI()
{
    QVBoxLayout *layout = new QVBoxLayout();
    QHBoxLayout *hbox = new QHBoxLayout();
    hbox->addWidget(new QLabel(tr("Filter:")));
    m_filter = new QLineEdit();
    hbox->addWidget(m_filter, 2);
    m_filterColumn = new QComboBox(this);
    hbox->addWidget(m_filterColumn);

    connect(m_filter, SIGNAL(textChanged(QString)), this, SLOT(filterChanged(QString)));
    connect(m_filterColumn, SIGNAL(currentIndexChanged(int)), this, SLOT(filterChanged(int)));

    layout->addLayout(hbox);

    m_tableView = new QTableView();
    m_tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    m_tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_tableView->setSelectionMode(QAbstractItemView::SingleSelection);
    m_tableView->setSortingEnabled(true);
    m_tableView->setShowGrid(false);

    connect(m_tableView, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(itemDoubleClicked(QModelIndex)));
    layout->addWidget(m_tableView);
    this->setLayout(layout);
}

QVariant SingleSelectListWidget::currentIndex()
{
    if (!m_selectedIndex.isValid())
    {
        return QString();
    }
    QModelIndex newIndex = m_model->index(m_model->mapFromSource(m_selectedIndex).row(), m_indexColumn);
    if (newIndex.isValid())
    {
        QVariant res  = m_model->data(newIndex);
        return res.toString();
    }
    return QString();
}

void SingleSelectListWidget::setCurrentIndex(const QVariant &data)
{
    if (!m_model)
    {
        return ;
    }
    QModelIndexList list = m_model->match(m_model->index(0,m_indexColumn), Qt::DisplayRole, data);
    if (list.size() == 1)
    {
        m_selectedIndex = m_model->mapToSource(list.first());
        m_tableView->selectRow(list.first().row());
        emit valueSelected(m_selectedIndex.row(), currentIndex(), currentDisplayText());
    }
}

QString SingleSelectListWidget::currentDisplayText()
{
    if (!m_selectedIndex.isValid())
    {
        return QString();
    }
    QModelIndex newIndex = m_model->index(m_model->mapFromSource(m_selectedIndex).row(), m_displayColumn);
    if (newIndex.isValid())
    {
        QVariant res  = m_model->data(newIndex);
        return res.toString();
    }
    return QString();
}


QSize SingleSelectListWidget::sizeHint() const
{
    return QSize(600,400);
}
