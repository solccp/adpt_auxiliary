#ifndef DOPWIDGET_H
#define DOPWIDGET_H

#include <QWidget>
#include <QTableView>
#include <QPushButton>
//#include "dopbandstructuremodel.h"

class BSOptTargetIndexBasedModel;
class BSOptTargetEnergyBasedModel;
class BandstructureData;
class DOPWidget : public QWidget
{
    Q_OBJECT
public:
    DOPWidget(BandstructureData *bsData, QWidget *parent = 0);

    void setEditable(bool editable);
signals:

public slots:
    void importPlainInput();
    void addEntry();
    void clear();
    void removeEntry();
private:
    void setupUI();
    BandstructureData* m_bsData;

    BSOptTargetIndexBasedModel* m_index_model;
    BSOptTargetEnergyBasedModel* m_energy_model;

    QTableView *m_tableView_index;
    QTableView *m_tableView_energy;

    QPushButton *butImportPlainFile;
    QPushButton *butClear;
    QPushButton *butAddEntry;
    QPushButton *butRemoveEntry;

    QTabWidget *m_tabWidget;
};

#endif // DOPWIDGET_H
