#include "clustergeometryeditor.h"

#include <QTableView>
#include <QGridLayout>
#include <QPushButton>
#include <QFileDialog>
#include <QMessageBox>
#include <QHeaderView>
#include <QLineEdit>
#include <QLabel>
#include <QTextEdit>
#include <QComboBox>
#include <QCheckBox>
#include <QSet>

#include "utils.h"

#include "geometry.h"
#include "atomsymbolcombodelegate.h"
#include "geometrycoordinationmodel.h"

#include "UpperCaseLineEdit.h"
#include "vectorthreeeditor.h"

#include <QDebug>

ClusterGeometryEditor::ClusterGeometryEditor(Geometry *geometry, bool isReadOnly, QWidget *parent) :
    QWidget(parent), m_isReadOnly(isReadOnly)
{
    m_geometry = geometry;
    m_geometrymodel = new GeometryCoordinationModel(m_geometry);
    connect(m_geometrymodel, SIGNAL(modelChanged()), this, SLOT(updateUUID()));
    connect(m_geometrymodel, SIGNAL(formulaChanged(QString)), this, SLOT(updateUUID()));
    setupControllers();
}
Geometry *ClusterGeometryEditor::geometry() const
{
    return m_geometry;
}

void ClusterGeometryEditor::editForce()
{
    QDialog *d = new QDialog(this);
    d->resize(640,480);

    QVBoxLayout *layout = new QVBoxLayout(d);

    VectorThreeEditor *editor = new VectorThreeEditor(m_geometry);
    editor->setHeaders("X", "Y", "Z");
    layout->addWidget(editor);

    QPushButton* butAccept = new QPushButton("OK");
    layout->addWidget(butAccept);

    QObject::connect(butAccept, SIGNAL(clicked()), d, SLOT(accept()));

    d->setModal(true);
    int ret = d->exec();


    if (editor->isValid())
    {
        m_geometry->setForce(editor->m_data.toList());
        m_geometry->setIsStationary(false);
    }
    else
    {
        m_geometry->setForce(QList<dVector3>());
    }

    delete d;

}

void ClusterGeometryEditor::loadGeometry()
{
    QString filter;
    QString filename = QFileDialog::getOpenFileName(
           this,
           tr("Load Geometry"),
           "",
           tr("XYZ file (*.xyz);;Gaussion log file (*.log *.out);;Gen file (*.gen);;HSD file (*.hsd)"), &filter );
    if (!filename.isNull())
    {
        QFile file(filename);
        if( !file.open( QIODevice::ReadOnly | QIODevice::Text ) )
        {
            qFatal( ("Could not open the file:" + filename).toLatin1() );
            return;
        }
        QTextStream stream(&file);
        bool succ = false;
        QString metadata;
        if (filter == "XYZ file (*.xyz)")
        {
            succ = m_geometrymodel->loadXYZGeometry(stream, metadata);
            try
            {
                libvariant::Variant var = libvariant::DeserializeJSON(metadata.toStdString());
//                libvariant::Variant ala = var["name"];

                if (var.Contains("name"))
                {
                    comboName->setText(QString::fromStdString(var["name"].AsString()));
                }
                if (var.Contains("method"))
                {
                    editMethod->setText(QString::fromStdString(var["method"].AsString()));
                }
                if (var.Contains("basis_set"))
                {
                    editBasis->setText(QString::fromStdString(var["basis_set"].AsString()));
                }
                if (var.Contains("charge"))
                {
                    editCharge->setText(QString::number(var["charge"].AsInt()));
                }
                if (var.Contains("spin"))
                {
                    editSpin->setText(QString::number(var["spin"].AsInt()));
                }
            }
            catch(...){}
        }
        else if (filter == "Gaussion log file (*.log *.out)")
        {
            succ = m_geometrymodel->loadGaussianOutputGeometry(stream);
        }
        else if (filter == "ORCA log file (*.log *.out)")
        {
            succ = m_geometrymodel->loadORCAOutputGeometry(stream);
        }
        else if (filter == "Gen file (*.gen)")
        {
            succ = m_geometrymodel->loadGenGeometry(stream);
        }
        else if (filter == "HSD file (*.hsd)")
        {
            succ = m_geometrymodel->loadHSDGeometry(stream);
        }


        file.close();
        if (!succ)
        {
            QMessageBox qbox(this);
            qbox.setWindowTitle(tr("DFTBRefDatabaseManager"));
            qbox.setText("Parsing " + filter + " failed. Cancelled.");
            qbox.setStandardButtons(QMessageBox::Ok);
            qbox.setIcon(QMessageBox::Critical);
            qbox.exec();
            return;
        }
        else
        {
            if (this->m_geometry->PBC())
            {
                QMessageBox qbox(this);
                qbox.setText("The lattice information in the input file was not imported due to different format. Please fill the information.");
                qbox.setStandardButtons(QMessageBox::Ok);
                qbox.setIcon(QMessageBox::Critical);
                qbox.exec();
            }
            if (comboName->text().isEmpty())
            {
                comboName->setText(QFileInfo(file).baseName());
            }
        }
    }
}

void ClusterGeometryEditor::saveGeometry()
{
    QString filter = nullptr;
    QString filename = QFileDialog::getSaveFileName(
           this,
           tr("Save Geometry"),
           QDir::homePath(),
           tr("XYZ file (*.xyz);;Gen file (*.gen);;HSD file (*.hsd)"), &filter);
    if (!filename.isEmpty())
    {
        QFile file(filename);
        if( !file.open( QIODevice::WriteOnly | QIODevice::Text ) )
        {
            qFatal( ("Could not open the file:" + filename).toLatin1() );
            return;
        }
        QTextStream stream(&file);
        if (filter == "XYZ file (*.xyz)")
        {
            stream << m_geometry->getXYZ();
        }
        else if (filter == "Gen file (*.gen)")
        {
            stream << m_geometry->getGen();
        }
        else if (filter == "HSD file (*.hsd)")
        {
            m_geometry->writeOut(stream, "hsd");
        }

        file.close();

    }
}

void ClusterGeometryEditor::clear()
{
    m_geometrymodel->clear();
}

void ClusterGeometryEditor::addAtom()
{
    m_geometrymodel->addAtom("C", 0.0, 0.0, 0.0);
    m_geometryTableView->selectRow(m_geometryTableView->model()->rowCount()-1);
}

void ClusterGeometryEditor::deleteAtom()
{
    QModelIndexList list = m_geometryTableView->selectionModel()->selectedRows();
    if (!list.empty())
    {
        for (int i=list.size()-1; i>=0; --i)
        {
            int row = list.at(i).row();
            m_geometryTableView->model()->removeRows(row,1);
        }
    }
}

bool showTextEditor(QWidget *parent, const QString &title, QString &variable)
{
    QDialog *d = new QDialog(parent);
    d->resize(640,480);
    QLabel *label = new QLabel(title);
    QTextEdit* edit = new QTextEdit();
    edit->setText(variable);
    QGridLayout* layout = new QGridLayout(d);
    layout->addWidget(label,0,0,1,1);
    layout->addWidget(edit,1,0,1,3);
    QObject::connect(edit, &QTextEdit::textChanged, [&edit, &variable](){ variable = edit->document()->toPlainText(); });

    QPushButton* butAccept = new QPushButton("OK");
    layout->addWidget(butAccept,2,2,1,1);
//    QPushButton* butClose = new QPushButton("Cancel");
//    layout->addWidget(butClose,2,3,1,1);

    QObject::connect(butAccept, SIGNAL(clicked()), d, SLOT(accept()));
//    QObject::connect(butClose, SIGNAL(clicked()), d, SLOT(reject()));

    d->setModal(true);
    int ret = d->exec();
    delete d;
    if (ret == QDialog::Accepted)
    {
        return true;
    }
    else
    {
        return false;
    }
}


void ClusterGeometryEditor::editComments()
{
    QString str = m_geometry->comment();
    if (showTextEditor(this, "Comments:", str))
    {
        if (!m_isReadOnly)
        {
            m_geometry->setComment(str);
        }
    }
}

void ClusterGeometryEditor::editCitations()
{
    QString str = m_geometry->citation();
    if (showTextEditor(this, "Citation:", str))
    {
        if (!m_isReadOnly)
        {
            m_geometry->setCitation(str);
        }
    }
}

void ClusterGeometryEditor::setBasis()
{
    if (sender() == editBasis)
    {
        m_geometry->setBasis(editBasis->text());
        updateUUID();
    }
}

void ClusterGeometryEditor::setMethod()
{
    if (sender() == editMethod)
    {
        m_geometry->setMethod(editMethod->text());
        updateUUID();
    }
}

void ClusterGeometryEditor::updateUUID()
{
    m_geometry->setUUID(m_geometry->computeUUID());
    editUUID->setText(m_geometry->UUID());
}

void ClusterGeometryEditor::setupControllers()
{
    this->resize(700,480);
    QGridLayout *grid = new QGridLayout(this);
    m_geometryTableView = new QTableView();
    m_geometryTableView->verticalHeader()->setVisible(false);
    m_geometryTableView->setSelectionBehavior(QAbstractItemView::SelectRows);

    m_geometryTableView->setModel((QAbstractItemModel*)m_geometrymodel);
    m_geometryTableView->setItemDelegateForColumn(1,new AtomSymbolComboDelegate() );



    butExternalForce = new QPushButton(tr("External Force"));
    connect(butExternalForce, SIGNAL(clicked()), this, SLOT(editForce()));


    butLoadGeometry = new QPushButton(tr("Load Geometry"));
    connect(butLoadGeometry, SIGNAL(clicked()), this, SLOT(loadGeometry()));

    butSaveGeometry = new QPushButton(tr("Save Geometry"));
    connect(butSaveGeometry, SIGNAL(clicked()), this, SLOT(saveGeometry()));


    butClear = new QPushButton(tr("Clear"));
    connect(butClear, SIGNAL(clicked()), this, SLOT(clear()));

    butAddAtom = new QPushButton(tr("Add atom"));
    connect(butAddAtom, SIGNAL(clicked()), this, SLOT(addAtom()));

    butDeleteAtom = new QPushButton(tr("Delete atom"));
    connect(butDeleteAtom, SIGNAL(clicked()), this, SLOT(deleteAtom()));



    editCharge = new QLineEdit();
    editCharge->setText(QString::number(m_geometry->getCharge()));
    QIntValidator *iv = new QIntValidator();
    editCharge->setValidator(iv);

    connect(editCharge, &QLineEdit::textChanged, [=](const QString &newValue) {
     m_geometry->setCharge(newValue);
     updateUUID();
     } );

//    connect(editCharge, SIGNAL(textChanged(QString)), m_geometry, SLOT(setCharge(QString)));



    editSpin = new QLineEdit();
    editSpin->setText(QString::number(m_geometry->getSpin()));
    {
        QIntValidator *iv = new QIntValidator(1, 100);
        editSpin->setValidator(iv);
    }

    connect(editSpin, &QLineEdit::textChanged, [=](const QString &newValue) {
         m_geometry->setSpin(newValue.toInt());
         } );















    comboName = new QLineEdit();
    comboName->setText(m_geometry->getName());
    connect(comboName, &QLineEdit::textChanged, [=](const QString &newValue) {
     m_geometry->setName(newValue);
     updateUUID();
     } );
//    connect(comboName, SIGNAL(textChanged(QString)), m_geometry, SLOT(setName(QString)));

    editFormula = new QLineEdit();
    editFormula->setReadOnly(true);
    editFormula->setText(m_geometry->getFormula());

    connect(m_geometrymodel, SIGNAL(formulaChanged(QString)), editFormula, SLOT(setText(QString)));

    editMethod = new UpperCaseLineEdit();
    editMethod->setText(m_geometry->getMethod());
    connect(editMethod, SIGNAL(textChanged(QString)), this, SLOT(setMethod()));

    editBasis = new UpperCaseLineEdit();
    editBasis->setText(m_geometry->getBasis());
    connect(editBasis, SIGNAL(textChanged(QString)), this, SLOT(setBasis()));

    butEditComments = new QPushButton(tr("Comments..."));
    connect(butEditComments, SIGNAL(clicked()), this, SLOT(editComments()));

    butEditCitation = new QPushButton(tr("Citation..."));
    connect(butEditCitation, SIGNAL(clicked()), this, SLOT(editCitations()));


    checkStationaryPoint = new QCheckBox(tr("Stationary Point"));
    checkStationaryPoint->setChecked(m_geometry->isStationary());

    connect(checkStationaryPoint, &QCheckBox::clicked, [=](bool newValue) {
     m_geometry->setIsStationary(newValue);
     updateUUID();
     });

//    connect(checkStationaryPoint, SIGNAL(clicked(bool)), m_geometry, SLOT(setIsStationary(bool)));

    editUUID = new QLineEdit();
    editUUID->setText(m_geometry->UUID());
    editUUID->setReadOnly(true);

//    grid->addWidget(new QLabel(tr("UUID:")),0,0,1,1);
//    grid->addWidget(editUUID,0,1,1,3);

    grid->addWidget(new QLabel(tr("Name:")),0,0,1,1);
    grid->addWidget(comboName,0,1,1,7);

    grid->addWidget(checkStationaryPoint,0,8,1,2);


    grid->addWidget(new QLabel(tr("Method:")),1,0,1,1);
    grid->addWidget(editMethod,1,1,1,4);

    grid->addWidget(new QLabel(tr("Basis:")),1,5,1,1);
    grid->addWidget(editBasis,1,6,1,4);

    grid->addWidget(new QLabel(tr("Charge:")),2,0,1,1);
    grid->addWidget(editCharge,2,1,1,2);

    grid->addWidget(new QLabel(tr("Spin:")),2,3,1,1);
    grid->addWidget(editSpin,2,4,1,2);

    grid->addWidget(new QLabel(tr("Formula:")),2,6,1,1);
    grid->addWidget(editFormula,2,7,1,3);

//    auto hb = new QHBoxLayout();
//    hb->addWidget(new QLabel(tr("Charge:")));
//    hb->addWidget(editCharge);
//    grid->addLayout(hb,3,0,1,1);
//    hb = new QHBoxLayout();
//    hb->addWidget(new QLabel(tr("Spin:")));
//    hb->addWidget(editSpin);
//    grid->addLayout(hb,3,1,1,1);


    grid->addWidget(m_geometryTableView,3,0,4,8);

    QVBoxLayout *vbox = new QVBoxLayout();
    vbox->addWidget(butExternalForce);
    vbox->addSpacing(20);
    vbox->addWidget(butLoadGeometry);
    vbox->addWidget(butSaveGeometry);
    vbox->addWidget(butClear);
    vbox->addSpacing(20);
    vbox->addWidget(butAddAtom);
    vbox->addWidget(butDeleteAtom);
    vbox->addSpacing(20);
    vbox->addWidget(butEditCitation);
    vbox->addWidget(butEditComments);

    grid->addLayout(vbox,3,8,4,2);


    if (m_isReadOnly)
    {
        butLoadGeometry->setEnabled(false);
        butClear->setEnabled(false);
        butAddAtom->setEnabled(false);
        butDeleteAtom->setEnabled(false);

        editMethod->setReadOnly(true);
        editBasis->setReadOnly(true);
        editCharge->setReadOnly(true);
        comboName->setReadOnly(true);
        checkStationaryPoint->setEnabled(false);

        m_geometryTableView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    }



}




