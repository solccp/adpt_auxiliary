#include "kpointssettingwidget.h"

#include "basic_types.h"

#include <QtWidgets>

#include "klineswidget.h"
#include "kpointsplainwidget.h"
#include "kpointssuperfoldingwidget.h"

#include "klinesmodel.h"
#include "kpointsplainmodel.h"
#include "kpointssupercellfoldingmodel.h"



KPointsSettingWidget::KPointsSettingWidget(ADPT::KPointsSetting *kpoints, QWidget *parent) :
    QWidget(parent), m_kpoints_setting(kpoints)
{
    setupUI();
    updateUI();
}
bool KPointsSettingWidget::hide_klines_setting() const
{
    return m_hide_klines_setting;
}

void KPointsSettingWidget::setHide_klines_setting(bool hide_klines_setting)
{
    m_hide_klines_setting = hide_klines_setting;
}
bool KPointsSettingWidget::hide_superfolding_setting() const
{
    return m_hide_superfolding_setting;
}

void KPointsSettingWidget::setHide_superfolding_setting(bool hide_superfolding_setting)
{
    m_hide_superfolding_setting = hide_superfolding_setting;
}

void KPointsSettingWidget::updateUI()
{
    disconnect(this->m_comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(comboIndexChanged()));
    m_comboBox->clear();

    m_comboBox->addItem("Plain", 0);

    if (!this->m_hide_superfolding_setting)
    {
        m_comboBox->addItem("SupercellFolding", 1);
    }

    if (!this->m_hide_klines_setting)
    {
        m_comboBox->addItem("KLines", 2);
    }
    connect(this->m_comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(comboIndexChanged()));
    if (m_kpoints_setting->m_type == ADPT::KPointsSetting::Type::SupercellFolding)
    {
        this->m_comboBox->setCurrentText("SupercellFolding");
    }
    else if (m_kpoints_setting->m_type == ADPT::KPointsSetting::Type::KLines)
    {
        this->m_comboBox->setCurrentText("KLines");
    }
    else
    {
        this->m_comboBox->setCurrentIndex(0);
    }

}

void KPointsSettingWidget::setEditable(bool value)
{
    m_comboBox->setEditable(value);
    m_klinesWidget->setEditable(value);
}

void KPointsSettingWidget::comboIndexChanged()
{
    auto var = m_comboBox->currentData();
    auto index = var.toInt();
    m_layout->setCurrentIndex(index);

    if (index == 0)
    {
        m_kpoints_setting->m_type = ADPT::KPointsSetting::Type::Plain;
    }
    else if (index == 1)
    {
        m_kpoints_setting->m_type = ADPT::KPointsSetting::Type::SupercellFolding;
    }
    else if (index == 2)
    {
        m_kpoints_setting->m_type = ADPT::KPointsSetting::Type::KLines;
    }

}

void KPointsSettingWidget::setupUI()
{
    m_comboBox = new QComboBox(this);

    m_mainlayout = new QVBoxLayout();

    auto combolayout = new QHBoxLayout();
    combolayout->addWidget(new QLabel("Type:"));
    combolayout->addWidget(m_comboBox);
    m_mainlayout->addLayout(combolayout);

    m_layout = new QStackedLayout();
    //Plain, index=0
    m_kpointsPlainWidget = new KPointsPlainWidget(this);
    m_kpointsPlainModel = new KPointsPlainModel(m_kpoints_setting, this);
    m_kpointsPlainWidget->setModel(m_kpointsPlainModel);
    m_layout->addWidget(m_kpointsPlainWidget);

    //SupercellFolding, index=1
    m_kpointsSupercellWidget = new KPointsSuperFoldingWidget(this);
    m_kpointsSupercellModel = new KPointsSupercellFoldingModel(m_kpoints_setting, this);
    m_kpointsSupercellWidget->setModel(m_kpointsSupercellModel);
    m_layout->addWidget(m_kpointsSupercellWidget);

    //Klines, index=2
    m_klinesWidget = new KLinesWidget(this);
    m_klinesModel = new KLinesModel(m_kpoints_setting, this);
    m_klinesWidget->setModel(m_klinesModel);
    m_layout->addWidget(m_klinesWidget);

    m_mainlayout->addLayout(m_layout);
    this->setLayout(m_mainlayout);


}



