#ifndef BANDSTRUCTUREBANDSMODEL_H
#define BANDSTRUCTUREBANDSMODEL_H

#include <QAbstractTableModel>
#include <QVector>


class BandstructureData;
class BandStructureBandsModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    BandStructureBandsModel(BandstructureData* bsdata, QObject *parent = 0);
    
signals:
    void dataChanged();
public slots:
    void clear();
    bool importPlainFile(const QString& filename);
    bool exportPlainText(const QString& filename);
    double getData(int bandIndex, int pointIndex);
    BandstructureData* getBandStructureData() const;

    // QAbstractItemModel interface
public:
    virtual int rowCount(const QModelIndex &parent) const;
    virtual int columnCount(const QModelIndex &parent) const;
    virtual QVariant data(const QModelIndex &index, int role) const;
private:
    BandstructureData* m_data;

};

#endif // BANDSTRUCTUREMODEL_H
