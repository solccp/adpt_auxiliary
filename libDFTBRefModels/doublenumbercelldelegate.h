#ifndef DOUBLENUMBERCELLDELEGATE_H
#define DOUBLENUMBERCELLDELEGATE_H

#include <QStyledItemDelegate>

class DoubleNumberCellDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    explicit DoubleNumberCellDelegate(QObject *parent = 0);

signals:

public slots:

    // QAbstractItemDelegate interface
public:
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;

    // QAbstractItemDelegate interface
//public:
//    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const;

    // QStyledItemDelegate interface
public:
    QString displayText(const QVariant &value, const QLocale &locale) const;

protected:
    void initStyleOption(QStyleOptionViewItem *option, const QModelIndex &index) const;

    // QAbstractItemDelegate interface
};

#endif // DOUBLENUMBERCELLDELEGATE_H
