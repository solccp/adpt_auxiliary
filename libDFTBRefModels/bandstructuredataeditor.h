#ifndef BANDSTRUCTUREDATAEDITOR_H
#define BANDSTRUCTUREDATAEDITOR_H

#include <QWidget>

QT_BEGIN_NAMESPACE
class QComboBox;
class QAbstractTableModel;
class QLineEdit;
class QSpinBox;
QT_END_NAMESPACE

class SingleSelectTableModelComboBox;
class BandstructureData;
class KLinesModel;
class KPointsSettingWidget;
class BandStructureWidget;
class BandStructureBandsModel;
class BandstructureDataEditor : public QWidget
{

    Q_OBJECT
public:
    BandstructureDataEditor(BandstructureData* bandstructure, QAbstractTableModel* geom_ids, bool isReadOnly = false, QWidget *parent = 0);
    
signals:
    
public slots:
private slots:   
    void setGeomID(const QString& value);
    void capitalize();
    void editComments();
    void editCitations();
    void showKlineDetails();
    void showBandDetails();
private:
    void setupUI();
    bool showTextEditor(QWidget *parent, const QString &title, QString &variable);
    BandstructureData* m_bandstructure;
    QAbstractTableModel* m_geom_ids;
    SingleSelectTableModelComboBox *comboRef;
    QLineEdit *refUUID;

private:
    KPointsSettingWidget* kpointsSettingwidget;
    BandStructureWidget *bsWidget;
    BandStructureBandsModel *bsModel;

    bool m_isReadOnly = false;
};

#endif // BANDSTRUCTUREDATAEDITOR_H
