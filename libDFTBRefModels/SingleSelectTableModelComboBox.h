#ifndef SINGLESELECTTABLEMODELCOMBOBOX_H
#define SINGLESELECTTABLEMODELCOMBOBOX_H

#include <QComboBox>
#include "singleselectlistwidget.h"

QT_BEGIN_NAMESPACE
class QPushButton;
QT_END_NAMESPACE

class SingleSelectTableModelComboBox : public QWidget
{
    Q_OBJECT
public:
    explicit SingleSelectTableModelComboBox(QWidget *parent = 0);

signals:
    void currentTextChanged(const QString& value);
public slots:
    void setModel(QAbstractItemModel *model);
    void setModelColumn(int visibleColumn);
    void setEditable(bool value);
private slots:
    void valueSelected(int index, const QVariant& value, const QString &display);
    void showPopup();

    // QComboBox interface
public:
    void setCurrentIndex(const QString &text);
    QString currentIndex();

    void setDisplayColumn(int index);
    int displayColumn();
private:
    SingleSelectListWidget* m_listWidget;
    int m_displayColumn = -1;
    QLineEdit* m_lineEdit;
    QPushButton* m_button;

    // QWidget interface
public:
    QSize sizeHint() const;
};

#endif // SINGLESELECTTABLEMODELCOMBOBOX_H
