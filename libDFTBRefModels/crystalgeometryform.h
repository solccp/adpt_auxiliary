#ifndef CRYSTALGEOMETRYFORM_H
#define CRYSTALGEOMETRYFORM_H

#include <QWidget>


namespace Ui {
class CrystalGeometryForm;
}

class QSignalMapper;
class Geometry;

class CrystalGeometryForm : public QWidget
{
    Q_OBJECT
    friend class CrystalGeometryEditor;
public:
    CrystalGeometryForm(Geometry* geom, QWidget *parent = 0);
    ~CrystalGeometryForm();
    void setupControls();
signals:
    void modelChanged();
private slots:
    void textChanged(int id);
    void on_comboBox_currentIndexChanged(int index);

private:
    void createControls();
private:
    Ui::CrystalGeometryForm *ui;
    Geometry* m_geometry;
    QSignalMapper* signalMapper;
};

#endif // CRYSTALGEOMETRYFORM_H
