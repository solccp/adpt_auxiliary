#include "molecularfrequencyeditor.h"
#include "molecularfrequencydata.h"
#include "utils.h"

#include "UpperCaseLineEdit.h"
#include "doublenumbercelldelegate.h"

#include <QtWidgets>
#include <QStandardItem>
#include "NumericalStandardItem.h"

MolecularFrequencyDataEditor::MolecularFrequencyDataEditor(MolecularFrequencyData *data, QAbstractTableModel *geom_ids, bool ReadOnly, QWidget *parent):
    QWidget(parent), m_ReadOnly(ReadOnly)
{
    m_data = data;
    m_geom_ids = geom_ids;
    setupUI();
}

void MolecularFrequencyDataEditor::setupUI()
{
    QGridLayout *layout = new QGridLayout();
    this->setLayout(layout);

    layout->addWidget(new QLabel(tr("Reference Geometry:")),0,0,1,1);

    comboRef = new SingleSelectTableModelComboBox();

    comboRef->setModel(m_geom_ids);
    comboRef->setModelColumn(0);
    comboRef->setDisplayColumn(1);
    comboRef->setCurrentIndex(m_data->geom_uuid());


    connect(comboRef, SIGNAL(currentTextChanged(QString)), this, SLOT(setGeomID(QString)));

    layout->addWidget(comboRef,0,1,1,3);

    layout->addWidget(new QLabel(tr("Method:")),1,0,1,1,Qt::AlignRight);
    m_editMethod = new UpperCaseLineEdit();
    m_editMethod->setText(m_data->method());
    connect(m_editMethod, SIGNAL(editingFinished()), this, SLOT(setMethod()));
    layout->addWidget(m_editMethod,1,1,1,1);

    layout->addWidget(new QLabel(tr("Basis:")),1,2,1,1);
    m_editBasis = new UpperCaseLineEdit();
    m_editBasis->setText(m_data->basis());
    connect(m_editBasis, SIGNAL(editingFinished()), this, SLOT(setBasis()));
    layout->addWidget(m_editBasis,1,3,1,1);

    layout->addWidget(new QLabel(tr("Frequency:")),2,0,4,1, Qt::AlignTop|Qt::AlignRight);

    m_view = new QTableView;
    const QList<double> &freqs = m_data->freq();
    const QList<double> &weights = m_data->weight();

    m_model = new QStandardItemModel(freqs.size(), 2);
    for(int i=0; i<freqs.size();++i)
    {
        QStandardItem* item = new QStandardItem();
        item->setTextAlignment(Qt::AlignRight);
        if (i<weights.size())
        {
            item->setData(weights[i], Qt::EditRole);
        }
        else
        {
            item->setData(1.0, Qt::EditRole);
        }
        m_model->setItem(i,0,item);
        item = new NumericalStandardItem();
        item->setTextAlignment(Qt::AlignRight);
        item->setData(freqs[i], Qt::EditRole);
        m_model->setItem(i,1,item);
    }
    m_model->setSortRole(Qt::UserRole);
    m_view->verticalHeader()->hide();
    m_model->setHeaderData(0, Qt::Horizontal, "Weight");
    m_model->setHeaderData(1, Qt::Horizontal, "Frequency");
    m_view->setModel(m_model);
    m_view->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    m_view->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_view->setSelectionMode(QAbstractItemView::SingleSelection);

    layout->addWidget(m_view,2,1,4,3);

    m_view->setItemDelegateForColumn(0, new DoubleNumberCellDelegate(this));
    m_view->setItemDelegateForColumn(1, new DoubleNumberCellDelegate(this));


    connect(m_model, SIGNAL(itemChanged(QStandardItem*)), this, SLOT(itemChanged(QStandardItem*)));


    QVBoxLayout *editButtons = new QVBoxLayout;
    QPushButton *butAddRow = new QPushButton(tr("Add row"));
    QPushButton *butRemoveRow = new QPushButton(tr("Remove row"));

    connect(butAddRow, SIGNAL(clicked()), this, SLOT(addRow()));
    connect(butRemoveRow, SIGNAL(clicked()), this, SLOT(removeRow()));

    editButtons->addWidget(butAddRow);
    editButtons->addWidget(butRemoveRow);
    editButtons->addStretch(3);

    QPushButton *butEditComments = new QPushButton(tr("Comments..."));
    connect(butEditComments, SIGNAL(clicked()), this, SLOT(editComments()));
    butEditComments->setMaximumWidth(100);
    butEditComments->setAutoDefault(false);

    QPushButton *butEditCitation = new QPushButton(tr("Citation..."));
    connect(butEditCitation, SIGNAL(clicked()), this, SLOT(editCitations()));
    butEditCitation->setMaximumWidth(100);
    butEditCitation->setAutoDefault(false);

    layout->addWidget(butEditComments, 6,0,1,1);
    layout->addWidget(butEditCitation, 7,0,1,1);

    if (m_ReadOnly)
    {
        comboRef->setEditable(false);
        m_editBasis->setReadOnly(true);
        m_editMethod->setReadOnly(true);
        m_view->setEditTriggers(QAbstractItemView::NoEditTriggers);

    }
    else
    {
        layout->addLayout(editButtons, 3,0,2,1);
    }
}

bool MolecularFrequencyDataEditor::showTextEditor(QWidget *parent, const QString &title, QString &variable)
{
    QDialog *d = new QDialog(parent);
    d->resize(640,480);
    QLabel *label = new QLabel(title);
    QTextEdit* edit = new QTextEdit();
    edit->setText(variable);
    QGridLayout* layout = new QGridLayout(d);
    layout->addWidget(label,0,0,1,1);
    layout->addWidget(edit,1,0,1,3);
    QObject::connect(edit, &QTextEdit::textChanged, [&edit, &variable](){ variable = edit->document()->toPlainText(); });

    QPushButton* butAccept = new QPushButton("OK");
    layout->addWidget(butAccept,2,2,1,1);

    QObject::connect(butAccept, SIGNAL(clicked()), d, SLOT(accept()));

    d->setModal(true);
    int ret = d->exec();
    delete d;
    if (ret == QDialog::Accepted)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void MolecularFrequencyDataEditor::editComments()
{
    QString str = m_data->comment();
    if (showTextEditor(this, "Comments:", str))
    {
        if (!m_ReadOnly)
        {
            m_data->setComment(str);
        }
    }
}

void MolecularFrequencyDataEditor::editCitations()
{
    QString str = m_data->citation();
    if (showTextEditor(this, "Citation:", str))
    {
        if (!m_ReadOnly)
        {
            m_data->setCitation(str);
        }
    }
}

void MolecularFrequencyDataEditor::itemChanged(QStandardItem *item)
{
    if (item->column() == 0)
    {
        m_data->weight(item->row()) = item->text().toDouble();
    }
    else
    {
        m_view->sortByColumn(1, Qt::AscendingOrder);
        m_view->scrollTo(m_model->indexFromItem(item));
        freqRowMoved();
    }
}

void MolecularFrequencyDataEditor::moveUp()
{
    QModelIndexList list = m_view->selectionModel()->selectedRows();
    int oldrow = list.first().row();
    if (oldrow == 0)
    {
        return;
    }
    m_model->insertRow(oldrow-1,m_model->takeRow(oldrow));
    m_view->selectRow(oldrow-1);
    m_view->setFocus();
}

void MolecularFrequencyDataEditor::moveDown()
{
    QModelIndexList list = m_view->selectionModel()->selectedRows();
    int oldrow = list.first().row();
    if (oldrow >= m_model->rowCount()-1)
    {
        return;
    }
    m_model->insertRow(oldrow+1,m_model->takeRow(oldrow));
    m_view->selectRow(oldrow+1);
    m_view->setFocus();
}

void MolecularFrequencyDataEditor::addRow()
{
    QList<QStandardItem*> items;
    QStandardItem* item = new QStandardItem();
    item->setData(1.0, Qt::EditRole);
    items.append(item);
    item = new NumericalStandardItem();
    item->setData(0.0, Qt::EditRole);
    items.append(item);
    m_model->appendRow(items);
    m_view->scrollToBottom();
    m_view->edit(m_model->indexFromItem(item));
}

void MolecularFrequencyDataEditor::removeRow()
{
    QModelIndexList list = m_view->selectionModel()->selectedRows();
    QList<QStandardItem*> items = m_model->takeRow(list.first().row());
    for(int i=0; i<items.size();++i)
    {
        delete items[i];
    }
    freqRowMoved();
    m_view->setFocus();
}

void MolecularFrequencyDataEditor::freqRowMoved()
{
    QList<double> newFreq;
    QList<double> newWeight;
    for(int i=0; i<m_model->rowCount() ;++i)
    {
        newWeight.append(m_model->item(i,0)->data(Qt::EditRole).toDouble());
        newFreq.append(m_model->item(i,1)->data(Qt::UserRole).toDouble());
    }
    m_data->setFreq(newFreq);
    m_data->setWeight(newWeight);
}

void MolecularFrequencyDataEditor::setGeomID(const QString &id)
{
    m_data->setGeom_uuid(id);
}

void MolecularFrequencyDataEditor::capitalize()
{
    QObject* obj = sender();
    QLineEdit* edit = qobject_cast<QLineEdit*>(obj);
    if (edit)
    {
        edit->setText(edit->text().toUpper());
    }
}

void MolecularFrequencyDataEditor::setMethod()
{
    if (sender() == m_editMethod)
    {
        m_data->setMethod(m_editMethod->text());
    }
}

void MolecularFrequencyDataEditor::setBasis()
{
    if (sender() == m_editBasis)
    {
        m_data->setBasis(m_editBasis->text());
    }
}
