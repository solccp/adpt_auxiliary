#include "BSOptTargetIndexBasedModel.h"
#include "bandstructuredata.h"

#include <QFile>
#include <QTextStream>
#include <QStringList>

BSOptTargetIndexBasedModel::BSOptTargetIndexBasedModel(BandstructureData *bsdata, QObject *parent) :
    QAbstractTableModel(parent)
{
    m_data = bsdata;
}

bool BSOptTargetIndexBasedModel::importPlaintextFile(const QString &filename)
{
    QFile file( filename );
    if( !file.open( QIODevice::ReadOnly | QIODevice::Text ) )
    {
        qFatal( "Could not open the file" );
    }

    QTextStream stream( &file );

    QList<BSOptTargetInfo> res;
    while( !stream.atEnd() )
    {
        QStringList list = stream.readLine().split(" ",QString::SkipEmptyParts);
        if (list.empty())
        {
            continue;
        }
        else if (list.first().startsWith("#"))
        {
            continue;
        }
        else if (list.size() < 4)
        {
            return false;
        }
        else
        {
            bool ok;
            int bs = list.at(0).toInt(&ok);
            if (!ok)
            {
                return false;
            }
            int be = list.at(1).toInt(&ok);
            if (!ok)
            {
                return false;
            }
            int ps = list.at(2).toInt(&ok);
            if (!ok)
            {
                return false;
            }
            int pe = list.at(3).toInt(&ok);
            if (!ok)
            {
                return false;
            }

            BSOptTargetInfo newline;
            newline.band_startindex = bs;
            newline.band_endindex = be;
            newline.point_startindex = ps;
            newline.point_endindex = pe;
            newline.index_based = true;

            if (list.size()==5)
            {
                double weight = list.at(4).toInt(&ok);
                if(ok)
                {
                    newline.weight = weight;
                }
            }


            res.append(newline);
        }
    }
    file.close();
    if (!res.empty())
    {
        int startindex = this->rowCount(QModelIndex());
        int endindex = startindex + res.size() -1;
        this->beginInsertRows(QModelIndex(), startindex, endindex);
        m_data->m_default_optimization.append(res);
        this->endInsertRows();
        return true;
    }
    else
    {
        return false;
    }
}

void BSOptTargetIndexBasedModel::clear()
{
    this->beginResetModel();
    m_data->m_default_optimization.clear();
    for(auto & item : m_data->m_default_optimization)
    {
        if(item.index_based)
        {
            m_data->m_default_optimization.removeOne(item);
        }
    }
    this->endResetModel();
}

int BSOptTargetIndexBasedModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    int count = 0;
    for(auto & item : m_data->m_default_optimization)
    {
        if(item.index_based)
        {
            ++count;
        }
    }
    return count;
}

int BSOptTargetIndexBasedModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return 5;
}

QVariant BSOptTargetIndexBasedModel::data(const QModelIndex &index, int role) const
{
    int curIndex = 0;
    int targetIndex = 0;
    for(auto & item : m_data->m_default_optimization)
    {
        if(item.index_based)
        {
            if (targetIndex == index.row())
            {
                break;
            }
            ++targetIndex;
        }
        ++curIndex;
    }
    if (role == Qt::DisplayRole || role == Qt::EditRole)
    {
        int row = curIndex;
        int col = index.column();
        BSOptTargetInfo info = m_data->m_default_optimization.at(row);
        switch(col)
        {
        case 0:
            return info.band_startindex;
        case 1:
            return info.band_endindex;
        case 2:
            return info.point_startindex;
        case 3:
            return info.point_endindex;
        case 4:
            return info.weight;
        }
    }
    return QVariant();
}

QVariant BSOptTargetIndexBasedModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
    {
        switch (section)
        {
        case 0:
            return "Band StartIndex";
        case 1:
            return "Band EndIndex";
        case 2:
            return "Point StartIndex";
        case 3:
            return "Point EndIndex";
        case 4:
            return "Weight";
        }
    }
    return QAbstractTableModel::headerData(section, orientation, role);
}


bool BSOptTargetIndexBasedModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    int curIndex = 0;
    int targetIndex = 0;
    for(auto & item : m_data->m_default_optimization)
    {
        if(item.index_based)
        {
            if (targetIndex == index.row())
            {
                break;
            }
            ++targetIndex;
        }
        ++curIndex;
    }
    if (role == Qt::EditRole)
    {
        int row = curIndex;
        int col = index.column();
        bool ok;
        if (col >=0 && col <=3)
        {
            value.toInt(&ok);
            if (!ok)
            {
                return false;
            }
        }
        else
        {
            value.toDouble(&ok);
            if (!ok)
            {
                return false;
            }
        }
        switch(col)
        {
        case 0:
            m_data->m_default_optimization[row].band_startindex = value.toInt();
            break;
        case 1:
            m_data->m_default_optimization[row].band_endindex = value.toInt();
            break;
        case 2:
            m_data->m_default_optimization[row].point_startindex = value.toInt();
            break;
        case 3:
            m_data->m_default_optimization[row].point_endindex = value.toInt();
            break;
        case 4:
            m_data->m_default_optimization[row].weight = value.toDouble();
            break;
        default:
            return false;
        }
        emit dataChanged(index, index);
        return true;
    }
    return false;
}

Qt::ItemFlags BSOptTargetIndexBasedModel::flags(const QModelIndex &index) const
{
    Q_UNUSED(index);
    return Qt::ItemIsEnabled | Qt::ItemIsEditable | Qt::ItemIsSelectable;
}


bool BSOptTargetIndexBasedModel::insertRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent);
    beginInsertRows(QModelIndex(),row, row+count-1);
    for (int i=0; i< count; ++i)
    {
        BSOptTargetInfo newline;
        newline.band_startindex = 0;
        newline.band_endindex = -1;
        newline.point_startindex = 0;
        newline.point_endindex = -1;
        newline.index_based = true;
        m_data->m_default_optimization.append(newline);
    }
    endInsertRows();
    return true;
}

bool BSOptTargetIndexBasedModel::removeRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent);
    QList<int> removingIndexes;
    int curIndex = 0;
    int targetIndex = 0;
    bool firstRow = true;
    for(auto & item : m_data->m_default_optimization)
    {
        if(item.index_based)
        {
            if (firstRow && targetIndex == row)
            {
                removingIndexes.append(curIndex);
                if (removingIndexes.size() == count)
                {
                    break;
                }
            }
            else if (firstRow == false)
            {
                removingIndexes.append(curIndex);
                if (removingIndexes.size() == count)
                {
                    break;
                }
            }
            ++targetIndex;
        }
        ++curIndex;
    }

    beginRemoveRows(QModelIndex(),row, row+count-1);
    for(int i=removingIndexes.size()-1;i>=0;--i)
    {
        m_data->m_default_optimization.removeAt(removingIndexes[i]);
    }
    endRemoveRows();
    return true;
}

BSOptTargetEnergyBasedModel::BSOptTargetEnergyBasedModel(BandstructureData *bsdata, QObject *parent) :
    QAbstractTableModel(parent)
{
    m_data = bsdata;
}

bool BSOptTargetEnergyBasedModel::importPlaintextFile(const QString &filename)
{
    QFile file( filename );
    if( !file.open( QIODevice::ReadOnly | QIODevice::Text ) )
    {
        qFatal( "Could not open the file" );
    }

    QTextStream stream( &file );

    QList<BSOptTargetInfo> res;
    while( !stream.atEnd() )
    {
        QStringList list = stream.readLine().split(" ",QString::SkipEmptyParts);
        if (list.empty())
        {
            continue;
        }
        else if (list.first().startsWith("#"))
        {
            continue;
        }
        else if (list.size() < 2)
        {
            return false;
        }
        else
        {
            bool ok;
            double lower = list.at(0).toDouble(&ok);
            if (!ok)
            {
                return false;
            }
            double upper = list.at(1).toDouble(&ok);
            if (!ok)
            {
                return false;
            }

            BSOptTargetInfo newline;
            newline.energy_lowerbound = lower;
            newline.energy_upperbound = upper;
            newline.index_based = false;

            if (list.size()==3)
            {
                double weight = list.at(2).toInt(&ok);
                if(ok)
                {
                    newline.weight = weight;
                }
            }
            res.append(newline);
        }
    }
    file.close();
    if (!res.empty())
    {
        int startindex = this->rowCount(QModelIndex());
        int endindex = startindex + res.size() -1;
        this->beginInsertRows(QModelIndex(), startindex, endindex);
        m_data->m_default_optimization.append(res);
        this->endInsertRows();
        return true;
    }
    else
    {
        return false;
    }
}

void BSOptTargetEnergyBasedModel::clear()
{
    this->beginResetModel();
    m_data->m_default_optimization.clear();
    for(auto & item : m_data->m_default_optimization)
    {
        if(!item.index_based)
        {
            m_data->m_default_optimization.removeOne(item);
        }
    }
    this->endResetModel();
}

int BSOptTargetEnergyBasedModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    int count = 0;
    for(auto & item : m_data->m_default_optimization)
    {
        if(!item.index_based)
        {
            ++count;
        }
    }
    return count;
}

int BSOptTargetEnergyBasedModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return 3;
}

QVariant BSOptTargetEnergyBasedModel::data(const QModelIndex &index, int role) const
{
    int curIndex = 0;
    int targetIndex = 0;
    for(auto & item : m_data->m_default_optimization)
    {
        if(!item.index_based)
        {
            if (targetIndex == index.row())
            {
                break;
            }
            ++targetIndex;
        }
        ++curIndex;
    }
    if (role == Qt::DisplayRole || role == Qt::EditRole)
    {
        int row = curIndex;
        int col = index.column();
        BSOptTargetInfo info = m_data->m_default_optimization.at(row);
        switch(col)
        {
        case 0:
            return info.energy_lowerbound;
        case 1:
            return info.energy_upperbound;
        case 2:
            return info.weight;
        }
    }
    return QVariant();
}

QVariant BSOptTargetEnergyBasedModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
    {
        switch (section)
        {
        case 0:
            return "Energy Lowerbound";
        case 1:
            return "Energy Upperbound";
        case 2:
            return "Weight";
        }
    }
    return QAbstractTableModel::headerData(section, orientation, role);
}


bool BSOptTargetEnergyBasedModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    int curIndex = 0;
    int targetIndex = 0;
    for(auto & item : m_data->m_default_optimization)
    {
        if(!item.index_based)
        {
            if (targetIndex == index.row())
            {
                break;
            }
            ++targetIndex;
        }
        ++curIndex;
    }
    if (role == Qt::EditRole)
    {
        int row = curIndex;
        int col = index.column();
        bool ok;
        double val;
        if (col >=0 && col <=2)
        {
            val = value.toDouble(&ok);
            if (!ok)
            {
                return false;
            }
        }
        else
        {
            return false;
        }
        switch(col)
        {
        case 0:
            m_data->m_default_optimization[row].energy_lowerbound = val;
            break;
        case 1:
            m_data->m_default_optimization[row].energy_upperbound = val;
            break;
        case 2:
            m_data->m_default_optimization[row].weight = val;
            break;
        default:
            return false;
        }
        emit dataChanged(index, index);
        return true;
    }
    return false;
}

Qt::ItemFlags BSOptTargetEnergyBasedModel::flags(const QModelIndex &index) const
{
    Q_UNUSED(index);
    return Qt::ItemIsEnabled | Qt::ItemIsEditable | Qt::ItemIsSelectable;
}


bool BSOptTargetEnergyBasedModel::insertRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent);
    beginInsertRows(QModelIndex(),row, row+count-1);
    for (int i=0; i< count; ++i)
    {
        BSOptTargetInfo newline;
        newline.energy_lowerbound = -100.0;
        newline.energy_upperbound = 0.0;
        newline.index_based = false;
        m_data->m_default_optimization.append(newline);
    }
    endInsertRows();
    return true;
}

bool BSOptTargetEnergyBasedModel::removeRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent);
    QList<int> removingIndexes;
    int curIndex = 0;
    int targetIndex = 0;
    bool firstRow = true;
    for(auto & item : m_data->m_default_optimization)
    {
        if(!item.index_based)
        {
            if (firstRow && targetIndex == row)
            {
                removingIndexes.append(curIndex);
                if (removingIndexes.size() == count)
                {
                    break;
                }
            }
            else if (firstRow == false)
            {
                removingIndexes.append(curIndex);
                if (removingIndexes.size() == count)
                {
                    break;
                }
            }
            ++targetIndex;
        }
        ++curIndex;
    }

    beginRemoveRows(QModelIndex(),row, row+count-1);
    for(int i=removingIndexes.size()-1;i>=0;--i)
    {
        m_data->m_default_optimization.removeAt(removingIndexes[i]);
    }
    endRemoveRows();
    return true;
}
