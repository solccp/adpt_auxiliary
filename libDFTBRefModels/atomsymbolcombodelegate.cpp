#include "atomsymbolcombodelegate.h"
#include "atomicproperties.h"

#include <QComboBox>
#include <QLineEdit>
#include <QAbstractItemModel>
#include <QAbstractItemView>


AtomSymbolComboDelegate::AtomSymbolComboDelegate(QObject *parent) :
    QStyledItemDelegate(parent)
{

}

void AtomSymbolComboDelegate::changed()
{
     QComboBox* editor = qobject_cast<QComboBox*>(sender());
     emit commitData(editor);
     emit closeEditor(editor);
}


QWidget *AtomSymbolComboDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(option);
    Q_UNUSED(index);
    QComboBox *cb = new QComboBox(parent);
    cb->setEditable(false);
    connect(cb, SIGNAL(currentIndexChanged(int)), this, SLOT(changed()));
    return cb;
}

void AtomSymbolComboDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    QVector<QString> allSymbols = ADPT::AtomicProperties::allSymbols();
    if(QComboBox *cb = qobject_cast<QComboBox *>(editor))
    {
        for (int i=0; i<allSymbols.size(); ++i)
        {
            cb->addItem(allSymbols[i]);
        }
        cb->setCurrentText(index.data().toString());
    }
}

void AtomSymbolComboDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    if(QComboBox *cb = qobject_cast<QComboBox *>(editor))
    {
        model->setData(index,cb->currentText());
    }
}

void AtomSymbolComboDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(index);
    QRect rect = option.rect;
    QSize sizeHint = editor->sizeHint();

    if (rect.width()<sizeHint.width()) rect.setWidth(sizeHint.width());
    if (rect.height()<sizeHint.height()) rect.setHeight(sizeHint.height());

    editor->setGeometry(rect);
}
