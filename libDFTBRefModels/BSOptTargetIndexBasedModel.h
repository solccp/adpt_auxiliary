#ifndef BSOptTargetIndexBasedModel_H
#define BSOptTargetIndexBasedModel_H

#include <QAbstractTableModel>
#include <QList>




class BandstructureData;
class BSOptTargetIndexBasedModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    BSOptTargetIndexBasedModel(BandstructureData* bsdata, QObject *parent = 0);
    
signals:
    
public slots:
    bool importPlaintextFile(const QString& filename);
    void clear();
private:
    BandstructureData* m_data;

    // QAbstractItemModel interface
public:
    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;

    // QAbstractItemModel interface
public:
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;

    // QAbstractItemModel interface
public:
    bool setData(const QModelIndex &index, const QVariant &value, int role);

    // QAbstractItemModel interface
public:
    Qt::ItemFlags flags(const QModelIndex &index) const;

    // QAbstractItemModel interface
public:
    bool insertRows(int row, int count, const QModelIndex &parent);
    bool removeRows(int row, int count, const QModelIndex &parent);
};


class BSOptTargetEnergyBasedModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    BSOptTargetEnergyBasedModel(BandstructureData* bsdata, QObject *parent = 0);

signals:

public slots:
    bool importPlaintextFile(const QString& filename);
    void clear();
private:
    BandstructureData* m_data;

    // QAbstractItemModel interface
public:
    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;

    // QAbstractItemModel interface
public:
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;

    // QAbstractItemModel interface
public:
    bool setData(const QModelIndex &index, const QVariant &value, int role);

    // QAbstractItemModel interface
public:
    Qt::ItemFlags flags(const QModelIndex &index) const;

    // QAbstractItemModel interface
public:
    bool insertRows(int row, int count, const QModelIndex &parent);
    bool removeRows(int row, int count, const QModelIndex &parent);
};

#endif // DOPBANDSTRUCTURE_H
