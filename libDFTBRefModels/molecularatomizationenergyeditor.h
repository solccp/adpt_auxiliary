#ifndef MOLECULARATOMIZATIONENERGYEDITOR_H
#define MOLECULARATOMIZATIONENERGYEDITOR_H

#include <QWidget>
#include <QComboBox>

class SingleSelectTableModelComboBox;
class MolecularAtomizationEnergyData;
class MolecularAtomizationEnergyEditor : public QWidget
{
    Q_OBJECT
public:
    MolecularAtomizationEnergyEditor(MolecularAtomizationEnergyData* data, QAbstractTableModel* geom_ids, QWidget *parent = 0);

signals:

public slots:
private slots:
    void setupUI();
    void setEnergy();
    void setGeomID(const QString& index);
    void capitalize();
    void setMethod();
    void setBasis();
    void editComments();
    void editCitations();
private:
    MolecularAtomizationEnergyData* m_data;
    QAbstractTableModel* m_geom_ids;
    SingleSelectTableModelComboBox *comboRef;
    QLineEdit *m_editBasis;
    QLineEdit *m_editMethod;
    QLineEdit *m_editEnergy;
    QComboBox *m_combo_energy_unit;
private:
    bool showTextEditor(QWidget *parent, const QString &title, QString &variable);
};

#endif // MOLECULARATOMIZATIONENERGYEDITOR_H
