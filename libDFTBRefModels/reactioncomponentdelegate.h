#ifndef REACTIONCOMPONENTDELEGATE_H
#define REACTIONCOMPONENTDELEGATE_H

#include <QStyledItemDelegate>


class DFTBRefDatabase;
class ReactionComponentDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    ReactionComponentDelegate(DFTBRefDatabase* database, QObject *parent = 0);

signals:

private slots:
    void changed();
public slots:


    // QAbstractItemDelegate interface
public:
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;

private:
    DFTBRefDatabase *m_database;

};

#endif // REACTIONCOMPONENTDELEGATE_H
