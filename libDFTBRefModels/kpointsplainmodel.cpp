#include "kpointsplainmodel.h"

#include <QDebug>
#include <QFile>
#include <QTextStream>
#include <QStringList>
#include <QRegExp>

#include "basic_types.h"

KPointsPlainModel::KPointsPlainModel(ADPT::KPointsSetting *data, QObject *parent) : QAbstractTableModel(parent)
{
    m_data = data;
}


int KPointsPlainModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return 4;
}


QVariant KPointsPlainModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal)
    {
        switch (section) {
        case 0:
            return tr("coord x");
            break;
        case 1:
            return tr("coord y");
            break;
        case 2:
            return tr("coord z");
            break;
        case 3:
            return tr("weight");
            break;
        default:
            break;
        }
        return QAbstractTableModel::headerData(section, orientation, role);
    }
    else
    {
        return QAbstractTableModel::headerData(section, orientation, role);
    }
}


int KPointsPlainModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return m_data->m_kpoints_plain.size();
}

QVariant KPointsPlainModel::data(const QModelIndex &index, int role) const
{
    int row = index.row();
    int col = index.column();

    if (role == Qt::DisplayRole || role == Qt::EditRole)
    {
        if (col == 3)
        {
            return QString("%1").arg(m_data->m_kpoints_plain.at(row).weight);
        }
        else
        {
            return QString("%1").arg(m_data->m_kpoints_plain.at(row).kpoint[col],0,'f',6);
        }
    }
    return QVariant();
}


QModelIndex KPointsPlainModel::index(int row, int column, const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    if (column == 3)
    {
        QModelIndex ind = createIndex(row, column, (void *)&m_data->m_kpoints_plain.at(row).weight);
        return ind;
    }
    else
    {
        QModelIndex ind = createIndex(row, column, (void *)&m_data->m_kpoints_plain.at(row).kpoint[column]);
        return ind;
    }
}

QModelIndex KPointsPlainModel::parent(const QModelIndex &child) const
{
    Q_UNUSED(child);
    return QModelIndex();
}



bool KPointsPlainModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (role == Qt::EditRole)
    {
        int row = index.row();
        int col = index.column();
        bool ok;
        if (col==3)
        {
            double val = value.toDouble(&ok);
            if (!ok)
            {
                return false;
            }
            m_data->m_kpoints_plain[row].weight = val;
        }
        else
        {
            int val = value.toInt(&ok);
            if (!ok)
            {
                return false;
            }
            m_data->m_kpoints_plain[row].kpoint[col] = val;
        }
        emit dataChanged(index, index);
        return true;
    }
    return false;
}

Qt::ItemFlags KPointsPlainModel::flags(const QModelIndex &index) const
{
    return Qt::ItemIsEnabled | Qt::ItemIsEditable | Qt::ItemIsSelectable;
}


bool KPointsPlainModel::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(QModelIndex(),row, row+count-1);
    for (int i=0; i< count; ++i)
    {
        ADPT::KPointsAndWeightEntry entry;
        entry.weight = 1;
        entry.kpoint = {{0.0,0.0,0.0}};

        m_data->m_kpoints_plain.append(entry);
    }
    endInsertRows();
    return true;
}


bool KPointsPlainModel::removeRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent);
    beginRemoveRows(QModelIndex(),row, row+count-1);
    for(int i=row+count-1; i>=row; --i)
    {
        m_data->m_kpoints_plain.removeAt(i);
    }
    endRemoveRows();
    return true;
}




bool KPointsPlainModel::importPlainFile(const QString &filename)
{
    QFile file( filename );
    if( !file.open( QIODevice::ReadOnly | QIODevice::Text ) )
    {
        qFatal( "Could not open the file" );
    }

    QTextStream stream( &file );

    bool succ = readKLinesString(stream);

    file.close();

    return succ;
}

bool KPointsPlainModel::importHSDFile(const QString &filename)
{
    QFile file( filename );
    if( !file.open( QIODevice::ReadOnly | QIODevice::Text ) )
    {
        qFatal( "Could not open the file" );
    }

    QTextStream stream( &file );

    QList<std::array<QVariant,4> > res;

    QString str = stream.readAll();
    QRegExp rx(".*KPointsAndWeights\\s*=\\s*KLines\\s*\\{([^\\}]*)\\}.*");

    if (rx.exactMatch(str) && rx.capturedTexts().size() > 1 )
    {
        QString instr = rx.capturedTexts().at(1);
        QTextStream strstream( instr.toLatin1()  );
        bool succ = readKLinesString(strstream);
        return succ;
    }
    return false;

}

//int KPointsPlainModel::numOfPoints()
//{
//    int res = 0;
//    for(auto const & item : m_data->m_kpoints_plain)
//    {
//        res += item.num_of_points;
//    }
//    return res;
//}

bool KPointsPlainModel::readKLinesString(QTextStream &stream)
{
    QList<ADPT::KPointsAndWeightEntry> res;
    while( !stream.atEnd() )
    {
        QStringList list = stream.readLine().split(" ",QString::SkipEmptyParts);
        if (list.empty())
        {
            continue;
        }
        else if (list.first().startsWith("#"))
        {
            continue;
        }
        else if (list.size() < 4)
        {
            return false;
        }
        else
        {
            bool ok;
            double nint = list.at(3).toDouble(&ok);
            if (!ok)
            {
                return false;
            }
            double x = list.at(0).toDouble(&ok);
            if (!ok)
            {
                return false;
            }
            double y = list.at(1).toDouble(&ok);
            if (!ok)
            {
                return false;
            }
            double z = list.at(2).toDouble(&ok);
            if (!ok)
            {
                return false;
            }
            ADPT::KPointsAndWeightEntry newline;
            newline.weight = nint;
            newline.kpoint[0] = x;
            newline.kpoint[1] = y;
            newline.kpoint[2] = z;

            res.append(newline);
        }
    }
    if (!res.empty())
    {
        int startindex = m_data->m_kpoints_plain.size();
        int endindex = m_data->m_kpoints_plain.size()+res.size()-1;
        this->beginInsertRows(QModelIndex(), startindex, endindex);
        m_data->m_kpoints_plain.append(res);
        this->endInsertRows();
        return true;
    }
    return false;
}


