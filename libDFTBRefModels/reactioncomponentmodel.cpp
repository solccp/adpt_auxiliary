#include "reactioncomponentmodel.h"

#include "geometry.h"

ReactionComponentModel::ReactionComponentModel(DFTBRefDatabase *database, QObject *parent):
    QAbstractTableModel(parent)
{
    m_database = database;
}


const QList<ReactionItem>& ReactionComponentModel::components() const
{
    return m_components;
}
QList<ReactionItem>& ReactionComponentModel::components()
{
    return m_components;
}

void ReactionComponentModel::setComponents(const QList<ReactionItem> &components)
{
    m_components = components;
}


int ReactionComponentModel::rowCount(const QModelIndex &) const
{
    return m_components.size();
}

int ReactionComponentModel::columnCount(const QModelIndex &) const
{
    return 2;
}

QVariant ReactionComponentModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    int row = index.row();
    int col = index.column();

    if (row >= m_components.size() || index.row() < 0)
        return QVariant();



    if (role == Qt::DisplayRole || role == Qt::EditRole)
    {
        if (col == 0)
        {
            return QString("%1").arg(m_components.at(row).coeff);
        }
        else if (col == 1)
        {
            QString uuid = m_components.at(row).uuid;
            Geometry geom;
            m_database->getGeometry(uuid, &geom);
            QString compactName;
            if (geom.PBC())
            {
                if (!geom.getBasis().isEmpty())
                {
                    compactName = QString("%1(%2/%3)").arg(geom.getName())
                            .arg(geom.getMethod())
                            .arg(geom.getBasis());
                }
                else
                {
                    compactName = QString("%1(%2)").arg(geom.getName())
                            .arg(geom.getMethod());
                }
            }
            else
            {
                if (!geom.getBasis().isEmpty())
                {
                    compactName = QString("%1(%2, %3/%4)").arg(geom.getName())
                            .arg(QString::number(geom.getCharge()))
                            .arg(geom.getMethod())
                            .arg(geom.getBasis());
                }
                else
                {
                    compactName = QString("%1(%2, %3)").arg(geom.getName())
                            .arg(QString::number(geom.getCharge()))
                            .arg(geom.getMethod());
                }
            }
            return compactName;
        }
    }
    else if (role == Qt::TextAlignmentRole)
    {
        if (index.column() == 0)
        {
            return Qt::AlignCenter;
        }
    }
    return QVariant();
}

bool ReactionComponentModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if ( index.isValid() && role == Qt::EditRole)
    {
        int row = index.row();
        int col = index.column();

        ReactionItem p = m_components.value(row);

        if (col == 1 )
        {
            p.uuid = value.toString();
        }
        else if (col == 0)
        {
            bool ok;
            p.coeff = value.toDouble(&ok);
            if (!ok)
                return false;
        }
        else
        {
            return false;
        }

        m_components.replace(row, p);
        emit dataChanged(index, index);
        return true;
    }
    return false;
}

QVariant ReactionComponentModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal)
    {
        switch (section)
        {
        case 0:
            return tr("Coefficient");
            break;
        case 1:
            return tr("Molecule");
            break;
        default:
            return QVariant();
        }
    }
    return QVariant();
}

bool ReactionComponentModel::insertRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent);
    beginInsertRows(QModelIndex(), row, row+count-1);
    for(int i = 0; i<count; i++)
    {
        ReactionItem pair;
        pair.uuid = "";
        pair.coeff = 1.0;
        m_components.insert(row, pair);
    }
    endInsertRows();
    return true;
}

bool ReactionComponentModel::removeRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent);
    beginRemoveRows(QModelIndex(), row, row+count-1);
    for (int i=0; i < count; ++i)
    {
        m_components.removeAt(row);
    }
    endRemoveRows();
    return true;
}

Qt::ItemFlags ReactionComponentModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;
    return QAbstractTableModel::flags(index) | Qt::ItemIsEditable;
}
