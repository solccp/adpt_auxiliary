#include "bandstructuredataeditor.h"

#include "kpointssettingwidget.h"

#include "bandstructureviewer.h"
//#include "dopbandstructure.h"

#include "utils.h"

#include <bandstructuredata.h>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QGroupBox>
#include <QDialog>
#include <QTextEdit>
#include <QListView>
#include <QSpinBox>

#include <QRadioButton>

#include "UpperCaseLineEdit.h"

#include "SingleSelectTableModelComboBox.h"

BandstructureDataEditor::BandstructureDataEditor(BandstructureData *bandstructure, QAbstractTableModel *geom_ids, bool isReadOnly, QWidget *parent) :
    QWidget(parent), m_isReadOnly(isReadOnly)
{
    m_bandstructure = bandstructure;
    m_geom_ids = geom_ids;
    setupUI();
}

bool BandstructureDataEditor::showTextEditor(QWidget *parent, const QString &title, QString &variable)
{
    QDialog *d = new QDialog(parent);
    d->resize(640,480);
    QLabel *label = new QLabel(title);
    QTextEdit* edit = new QTextEdit();
    edit->setText(variable);
    QGridLayout* layout = new QGridLayout(d);
    layout->addWidget(label,0,0,1,1);
    layout->addWidget(edit,1,0,1,3);
    QObject::connect(edit, &QTextEdit::textChanged, [&edit, &variable](){ variable = edit->document()->toPlainText(); });

    QPushButton* butAccept = new QPushButton("OK");
    layout->addWidget(butAccept,2,2,1,1);
//    QPushButton* butClose = new QPushButton("Cancel");
//    layout->addWidget(butClose,2,3,1,1);

    QObject::connect(butAccept, SIGNAL(clicked()), d, SLOT(accept()));
//    QObject::connect(butClose, SIGNAL(clicked()), d, SLOT(reject()));

    d->setModal(true);
    int ret = d->exec();
    delete d;
    if (ret == QDialog::Accepted)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void BandstructureDataEditor::editComments()
{
    QString str = m_bandstructure->comment();
    if (showTextEditor(this, "Comments:", str))
    {
        m_bandstructure->setComment(str);
    }
}

void BandstructureDataEditor::editCitations()
{
    QString str= m_bandstructure->citation();
    if (showTextEditor(this, "Citation:", str))
    {
        m_bandstructure->setCitation(str);
    }
}


void BandstructureDataEditor::showKlineDetails()
{
    QDialog d;

    d.resize(640,480);
    d.setWindowTitle("KPoints");

    QVBoxLayout *vbox = new QVBoxLayout;
    vbox->addWidget(kpointsSettingwidget);
    QHBoxLayout *hbox = new QHBoxLayout();
    QPushButton* butAccept = new QPushButton("OK");
    hbox->addStretch(5);
    hbox->addWidget(butAccept);
    QObject::connect(butAccept, SIGNAL(clicked()), &d, SLOT(accept()));
    vbox->addLayout(hbox);
    d.setModal(true);
    d.setLayout(vbox);
    d.exec();
    kpointsSettingwidget->setParent(0);
}

void BandstructureDataEditor::showBandDetails()
{
    QDialog d;

    d.resize(800,600);
    d.setWindowTitle("Bandstructure data");

    QVBoxLayout *vbox = new QVBoxLayout;
    vbox->addWidget(bsWidget);
    QHBoxLayout *hbox = new QHBoxLayout();
    QPushButton* butAccept = new QPushButton("OK");
    hbox->addStretch(5);
    hbox->addWidget(butAccept);
    QObject::connect(butAccept, SIGNAL(clicked()), &d, SLOT(accept()));
    vbox->addLayout(hbox);
    d.setModal(true);
    d.setLayout(vbox);
    d.exec();
    bsWidget->setParent(0);
}

void BandstructureDataEditor::setGeomID(const QString &value)
{
    m_bandstructure->setGeomID(value);
    refUUID->setText(m_bandstructure->getGeomID());
}

void BandstructureDataEditor::setupUI()
{
    QLineEdit *editName = new QLineEdit();
    editName->setText(m_bandstructure->getName());
    connect(editName, SIGNAL(textEdited(QString)), m_bandstructure, SLOT(setName(QString)));


    comboRef = new SingleSelectTableModelComboBox();
    comboRef->setModel(m_geom_ids);
    comboRef->setModelColumn(0);
    comboRef->setDisplayColumn(1);
    comboRef->setCurrentIndex(m_bandstructure->getGeomID());

    connect(comboRef, SIGNAL(currentTextChanged(QString)), this, SLOT(setGeomID(QString)));


    QLineEdit *editMethod = new UpperCaseLineEdit();
    editMethod->setText(m_bandstructure->method());
    connect(editMethod, SIGNAL(textEdited(QString)), m_bandstructure, SLOT(setMethod(QString)));

    QLineEdit *editBasis = new UpperCaseLineEdit();
    editBasis->setText(m_bandstructure->basis());
    connect(editBasis, SIGNAL(textEdited(QString)), m_bandstructure, SLOT(setBasis(QString)));


    QLabel *klineLabel = new QLabel("KLines Definition");
    QPushButton *klinedetails = new QPushButton("Show");
    connect(klinedetails, SIGNAL(clicked()), this, SLOT(showKlineDetails()));

    kpointsSettingwidget = new KPointsSettingWidget(&m_bandstructure->m_kpoints_setting);
    kpointsSettingwidget->setHide_superfolding_setting(true);
    kpointsSettingwidget->updateUI();


    QLabel *bandlabel = new QLabel("Reference Band Structure");
    QPushButton *banddetails = new QPushButton("Detail...");
    connect(banddetails, SIGNAL(clicked()), this, SLOT(showBandDetails()));


    bsWidget = new BandStructureWidget();
    bsModel = new BandStructureBandsModel(m_bandstructure, this);
    bsWidget->setBandStructureModel(bsModel);
    connect(bsModel, SIGNAL(modelReset()), bsWidget, SLOT(prepareGraph()));



    QPushButton *butEditComments = new QPushButton(tr("Comments..."));
    connect(butEditComments, SIGNAL(clicked()), this, SLOT(editComments()));
    butEditComments->setMaximumWidth(100);
    butEditComments->setAutoDefault(false);

    QPushButton *butEditCitation = new QPushButton(tr("Citation..."));
    connect(butEditCitation, SIGNAL(clicked()), this, SLOT(editCitations()));
    butEditCitation->setMaximumWidth(100);
    butEditCitation->setAutoDefault(false);




    refUUID = new QLineEdit(m_bandstructure->getGeomID());
    refUUID->setReadOnly(true);


    QVBoxLayout *layout = new QVBoxLayout();

    QHBoxLayout *hbox = new QHBoxLayout;
    hbox->addWidget(new QLabel(tr("Name:")));
    hbox->addWidget(editName);
    layout->addLayout(hbox);

    hbox = new QHBoxLayout;
//    hbox->addWidget(new QLabel(tr("Ref. UUID:")));
//    hbox->addWidget(refUUID);

    hbox->addWidget(new QLabel(tr("Ref. Geometry:")));
    hbox->addWidget(comboRef);
    layout->addLayout(hbox);

    hbox = new QHBoxLayout;
    hbox->addWidget(new QLabel(tr("Method:")));
    hbox->addWidget(editMethod);


    hbox->addWidget(new QLabel(tr("Basis:")));
    hbox->addWidget(editBasis);
    layout->addLayout(hbox);

    hbox = new QHBoxLayout;

    hbox->addWidget(klineLabel);
    klinedetails->setMaximumWidth(100);
    hbox->addWidget(klinedetails, Qt::AlignLeft);
//    hbox->addStretch(6);
//    layout->addLayout(hbox);

//    hbox = new QHBoxLayout;
    hbox->addSpacing(50);
    hbox->addWidget(bandlabel);
    banddetails->setMaximumWidth(100);
    hbox->addWidget(banddetails, Qt::AlignLeft);  

    hbox->addStretch(6);
    layout->addLayout(hbox);

    hbox = new QHBoxLayout;
    hbox->addWidget(new QLabel("Reference shifting point:" ));
    QRadioButton *shift_ef = new QRadioButton("Fermi energy");
    hbox->addWidget(shift_ef);
    QRadioButton *shift_custom = new QRadioButton("Custom");
    hbox->addWidget(shift_custom);

    QSpinBox *fermiBandSpinBox = new QSpinBox();
    QSpinBox *fermiPointSpinBox = new QSpinBox();
    hbox->addWidget(new QLabel("BandIndex:"));
    hbox->addWidget(fermiBandSpinBox);
    hbox->addWidget(new QLabel("PointIndex:"));
    hbox->addWidget(fermiPointSpinBox);
    fermiPointSpinBox->setMaximum(2000);
    hbox->addStretch(5);



    layout->addLayout(hbox);


    if (m_bandstructure->getFermiIndex() == QPair<int, int>(-1,-1))
    {
        shift_ef->setChecked(true);
        fermiBandSpinBox->setEnabled(false);
        fermiPointSpinBox->setEnabled(false);
    }
    else
    {
        shift_custom->setChecked(true);
        fermiBandSpinBox->setEnabled(true);
        fermiPointSpinBox->setEnabled(true);
        fermiBandSpinBox->setValue(m_bandstructure->getFermiIndex().first);
        fermiPointSpinBox->setValue(m_bandstructure->getFermiIndex().second);
    }


    connect(shift_custom, &QRadioButton::toggled, [=](bool value) {
        fermiBandSpinBox->setEnabled(value);
        fermiPointSpinBox->setEnabled(value);
        if (value)
        {
            m_bandstructure->setFermiIndex(fermiBandSpinBox->value(),fermiPointSpinBox->value());
        }
        else
        {
            m_bandstructure->setFermiIndex(-1,-1);
        }
        bsWidget->replotGraph();
    });

//    QwtPlot *m_plot = bsWidget->getPreviewWidget();
    QCustomPlot* m_customplot = bsWidget->getPreviewPlot();
    layout->addWidget(m_customplot);

    connect(fermiBandSpinBox, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), [=](int value) {
        m_bandstructure->setFermiIndex(value,fermiPointSpinBox->value());
        bsWidget->replotGraph();

    });

    connect(fermiPointSpinBox, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), [=](int value) {
        m_bandstructure->setFermiIndex(fermiBandSpinBox->value(),value);
        bsWidget->replotGraph();
    });





//    layout->addWidget(box1);
//    layout->addWidget(box2);

    hbox = new QHBoxLayout();
    hbox->addWidget(butEditComments, Qt::AlignLeft);
    hbox->addWidget(butEditCitation, Qt::AlignLeft);
    hbox->addStretch(6);
    layout->addLayout(hbox);

    if (m_isReadOnly)
    {
        comboRef->setEditable(false);
        editMethod->setReadOnly(true);
        editBasis->setReadOnly(true);
        editName->setReadOnly(true);
        bsWidget->setEditable(false);
        kpointsSettingwidget->setEditable(false);
        fermiBandSpinBox->setEnabled(false);
        fermiPointSpinBox->setEnabled(false);
        shift_custom->setEnabled(false);
        shift_ef->setEnabled(false);

    }
    this->setLayout(layout);
}

void BandstructureDataEditor::capitalize()
{
    QObject* obj = sender();
    QLineEdit* edit = qobject_cast<QLineEdit*>(obj);
    if (edit)
    {
        edit->setText(edit->text().toUpper());
    }
}

