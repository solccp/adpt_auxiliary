#ifndef KPOINTSSETTINGWIDGET_H
#define KPOINTSSETTINGWIDGET_H

#include <QWidget>
#include <QComboBox>
#include <QStackedLayout>
#include <QVBoxLayout>

namespace ADPT
{
    class KPointsSetting;
}

class KLinesWidget;
class KPointsPlainWidget;
class KPointsSuperFoldingWidget;

class KLinesModel;
class KPointsPlainModel;
class KPointsSupercellFoldingModel;

class KPointsSettingWidget : public QWidget
{
    Q_OBJECT
public:
    KPointsSettingWidget(ADPT::KPointsSetting* kpoints, QWidget *parent = 0);
    bool hide_klines_setting() const;
    bool hide_superfolding_setting() const;
    void setHide_klines_setting(bool hide_klines_setting);
    void setHide_superfolding_setting(bool hide_superfolding_setting);
signals:

public slots:
    void updateUI();
    void setEditable(bool value);
private slots:
    void comboIndexChanged();
private:
    ADPT::KPointsSetting *m_kpoints_setting;

    KLinesWidget* m_klinesWidget;
    KPointsPlainWidget *m_kpointsPlainWidget;
    KPointsSuperFoldingWidget *m_kpointsSupercellWidget;

    KLinesModel *m_klinesModel;
    KPointsPlainModel *m_kpointsPlainModel;
    KPointsSupercellFoldingModel *m_kpointsSupercellModel;

    QComboBox *m_comboBox;

    bool m_hide_klines_setting = false;
    bool m_hide_superfolding_setting = false;

    QStackedLayout *m_layout;
    QVBoxLayout *m_mainlayout;

    void setupUI();

};

#endif // KPOINTSSETTINGWIDGET_H
