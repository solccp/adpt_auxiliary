#-------------------------------------------------
#
# Project created by QtCreator 2013-09-18T14:43:19
#
#-------------------------------------------------

QT += gui sql printsupport
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
TARGET = DFTBRefModels
TEMPLATE = lib
CONFIG += staticlib c++11

SOURCES += \
    klinesmodel.cpp \
    geometrycoordinationmodel.cpp \
    bandstructuredataeditor.cpp \
    reactionenergydataeditor.cpp \
    molecularfrequencyeditor.cpp \
    clustergeometryeditor.cpp \
    reactioncomponentwidget.cpp \
    reactioncomponentdelegate.cpp \
    reactioncomponentmodel.cpp \
    bandstructureviewer.cpp \
    klineswidget.cpp \
    SingleSelectTableModelComboBox.cpp \
    singleselectlistwidget.cpp \
    crystalgeometryform.cpp \
    UpperCaseLineEdit.cpp \
    atomsymbolcombodelegate.cpp \
    doublenumbercelldelegate.cpp \
    dopwidget.cpp \
    molecularatomizationenergyeditor.cpp \
    NumericalStandardItem.cpp \
    crystalgeometryeditor.cpp \
    qcustomplot.cpp \
    kpointssettingwidget.cpp \
    kpointsplainwidget.cpp \
    kpointssuperfoldingwidget.cpp \
    kpointsplainmodel.cpp \
    kpointssupercellfoldingmodel.cpp \
    BSOptTargetIndexBasedModel.cpp \
    geometrymodel.cpp \
    bandstructurebandsmodel.cpp \
    vectorthreeeditor.cpp

HEADERS += \
    klinesmodel.h \
    geometrycoordinationmodel.h \
    reactionenergydataeditor.h \
    molecularfrequencyeditor.h \
    bandstructuredataeditor.h \
    clustergeometryeditor.h \
    reactioncomponentwidget.h \
    reactioncomponentdelegate.h \
    reactioncomponentmodel.h \
    bandstructureviewer.h \
    klineswidget.h \
    SingleSelectTableModelComboBox.h \
    singleselectlistwidget.h \
    crystalgeometryform.h \
    UpperCaseLineEdit.h \
    atomsymbolcombodelegate.h \
    doublenumbercelldelegate.h \
    dopwidget.h \
    molecularatomizationenergyeditor.h \
    NumericalStandardItem.h \
    crystalgeometryeditor.h \
    qcustomplot.h \
    kpointssettingwidget.h \
    kpointsplainwidget.h \
    kpointssuperfoldingwidget.h \
    kpointsplainmodel.h \
    kpointssupercellfoldingmodel.h \
    BSOptTargetIndexBasedModel.h \
    geometrymodel.h \
    bandstructurebandsmodel.h \
    vectorthreeeditor.h

INCLUDEPATH += $$PWD/../external_libs/include

INCLUDEPATH+=$$PWD/../external_libs/include/openbabel-2.0
LIBS+=-L$$PWD/../external_libs/lib -lopenbabel -linchi -lz

#win32:INCLUDEPATH+= $$PWD/../external_libs/win64/include
#win32:LIBS += -L$$PWD/../external_libs/win64/bin -lopenbabel -linchi

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libDFTBRefDatabase/release/ -lDFTBRefDatabase
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libDFTBRefDatabase/debug/ -lDFTBRefDatabase
else:unix: LIBS += -L$$OUT_PWD/../libDFTBRefDatabase/ -lDFTBRefDatabase

INCLUDEPATH += $$PWD/../libDFTBRefDatabase
DEPENDPATH += $$PWD/../libDFTBRefDatabase

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBRefDatabase/release/libDFTBRefDatabase.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBRefDatabase/debug/libDFTBRefDatabase.a
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libDFTBRefDatabase/libDFTBRefDatabase.a

FORMS += \
    crystalgeometryform.ui

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libadpt_common/release/ -ladpt_common
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libadpt_common/debug/ -ladpt_common
else:unix: LIBS += -L$$OUT_PWD/../libadpt_common/ -ladpt_common

INCLUDEPATH += $$PWD/../libadpt_common
DEPENDPATH += $$PWD/../libadpt_common

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/release/libadpt_common.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/debug/libadpt_common.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/release/adpt_common.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/debug/adpt_common.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/libadpt_common.a
