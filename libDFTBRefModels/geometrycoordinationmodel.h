#ifndef GEOMETRYCOORDINATIONMODEL_H
#define GEOMETRYCOORDINATIONMODEL_H

#include <QAbstractTableModel>
#include <QVariant>
#include <QList>
#include <array>
#include <QTextStream>


class Geometry;
class GeometryCoordinationModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    GeometryCoordinationModel(Geometry* geom, QObject *parent = 0);
    
signals:
    void modelChanged();
    void formulaChanged(QString);
public slots:

    void clear();
    void BeginModify();
    void EndModify();
    void addAtom(const QString& Symbol,double x, double y, double z);

    bool loadXYZGeometry(QTextStream &stream, QString &metadata);
    bool loadGaussianOutputGeometry(QTextStream& stream);
    bool loadORCAOutputGeometry(QTextStream& stream);
    bool loadGenGeometry(QTextStream& stream);
    bool loadHSDGeometry(QTextStream& stream);
    bool loadPOSCAR(QTextStream& stream, QString *name, bool *assumed_elements);


private slots:

private:

    Geometry* m_geom;

    bool beingModified = false;

    // QAbstractItemModel interface
public:
    virtual int rowCount(const QModelIndex &parent) const;
    virtual int columnCount(const QModelIndex &parent) const;
    virtual QVariant data(const QModelIndex &index, int role) const;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const;

    // QAbstractItemModel interface
public:
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role);
    virtual Qt::ItemFlags flags(const QModelIndex &index) const;

    // QAbstractItemModel interface
public:
    virtual bool removeRows(int row, int count, const QModelIndex &parent);
};

#endif // GEOMETRYCOORDINATIONMODEL_H
