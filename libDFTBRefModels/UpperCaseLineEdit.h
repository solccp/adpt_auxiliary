#ifndef UPPERCASELINEEDIT_H
#define UPPERCASELINEEDIT_H

#include <QLineEdit>

class UpperCaseLineEdit : public QLineEdit
{
    Q_OBJECT
public:
    explicit UpperCaseLineEdit(QWidget *parent = 0);

signals:

public slots:


    // QWidget interface
protected:
    void keyPressEvent(QKeyEvent *event);
};

#endif // UPPERCASELINEEDIT_H
