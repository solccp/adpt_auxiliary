#ifndef KLINESWIDGET_H
#define KLINESWIDGET_H

#include <QPushButton>
#include <QWidget>
#include <QTableView>
#include <QGridLayout>
#include <QGroupBox>


class KLinesModel;
class KLinesWidget : public QWidget
{
    Q_OBJECT
public:
    explicit KLinesWidget(QWidget *parent = 0);
    ~KLinesWidget();
signals:
    
public slots:
    KLinesModel* getModel() const;
public:
    void setModel(KLinesModel* model);
    void setEditable(bool enable);
private slots:
    void addRow();
    void removeRow();
    void importVaspInput();
    void importPlainInput();
    void importHSDInput();
private:
    QTableView *m_tableView;
    KLinesModel* m_model;
    QPushButton *butImportVaspFile;
    QPushButton *butImportPlainFile;
    QPushButton *butImportHSDFile;
    QPushButton *butAddRow;
    QPushButton *butRemoveRow;
    QGroupBox *buttonBox;

};

#endif // KLINESWIDGET_H
