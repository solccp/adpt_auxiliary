#include "NumericalStandardItem.h"

void NumericalStandardItem::setData(const QVariant &value, int role)
{
    if (role == Qt::EditRole)
    {
        m_value = value.toDouble();
    }
    else if (role == Qt::UserRole)
    {
        m_value = value.toDouble();
    }
    QStandardItem::setData(value, role);
}

QStandardItem *NumericalStandardItem::clone() const
{
    return new NumericalStandardItem(*this);
}


QVariant NumericalStandardItem::data(int role) const
{
    if (role == Qt::UserRole)
    {
        return QVariant(m_value);
    }
    else
    {
        return QStandardItem::data(role);
    }
}
