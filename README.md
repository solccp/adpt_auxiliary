# adpt_auxiliary #

This package contains the auxiliary tools of the Automatic DFTB Parameterization Toolkit (ADPT).

The license of each of the programs and library are as follows:

* libDFTBRefModels (GPLv2, linking openbabel library)
* DFTBOPTInputCreator (GPLv2, linking libDFTBRefModels)
* DFTBRefDatabaseManager (GPLv2, linking libDFTBRefModels)
* libDFTBRefDatabase (LPGLv3)

The following submodules are distributed with LGPLv3 license.

* libadpt_common
* libDFTBTestSuiteCommon
* libHSDParser
* libDFTBPlusAdaptor
* libSKOptimizerCommon
* libErepfitCommon
* libSKBuilderCommon


## Pre-compiled Windows version ##
The pre-compiled binary can be downloaded in the [Downloads](https://bitbucket.org/solccp/adpt_auxiliary/downloads) page.

## 1. Requirements for compiling the code ##

1. A C++11 compiler
2. [CMake](https://cmake.org/)
3. [Qt](http://www.qt.io/) 5 library (including source code)
4. libmysqlclient (libmysqlclient-dev)
5. OpenGL headers (mesa-common-dev or any other packages providing GL/gl.h)
6. OpenGL libraries (libgl1-mesa-dev or any other packages providing libGL.so)
7. libxml2 and libz libraries



## 2. Obtain the source code ##

```
$ git clone https://bitbucket.org/solccp/adpt_auxiliary.git
```

### 2.1 Obtain submodules source code ###
```
$ cd adpt_auxiliary
$ git submodule init 
$ git submodule update
```

## 3. Compile the external libraries ##
```
$ cd external_libs
$ ./make-external-libs.sh  #build the libvraiant and openbabel library
```

## 4. Compile Qt MySql driver ##
```
$ cd /path/to/the/qt/source/code/root #(adjust accordingly to the install path)
$ cd qtbase/src/plugins/sqldrivers/mysql 
$ /path/to/the/qmake #(adjust accordingly to the install path)
$ make install
```

## 5. Compiling  adpt_auxiliary ##
```
$ mkdir adpt_auxiliary_build        #or whatever you name it
$ cd adpt_auxiliary_build
$ /path/to/the/qmake PREFIX=/opt /path/to/adpt_auxiliary #(change /opt to wherever you like)
$ make
# make install 
```
The programs will be installed into PREFIX/adpt/bin/

## To get the updates of the source code
```
$ git pull
$ git submodule update
```


# Documentation #
[Schema for reference data](http://goo.gl/fma0wK)