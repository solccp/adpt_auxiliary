#ifndef SQLRELATIONALTABLEMODEL_H
#define SQLRELATIONALTABLEMODEL_H

#include <QSqlTableModel>
#include <QSqlRelation>
#include <QSet>
#include <QSqlDatabase>


class QRelatedTableModel;
struct QRelation
{
    public:
        QRelation(): model(0), m_parent(0), m_dictInitialized(false) {}
        void init(QSqlTableModel *parent, const QSqlRelation &relation);

        void populateModel();

        bool isDictionaryInitialized();
        void populateDictionary();
        void clearDictionary();

        void clear();
        bool isValid();

        QSqlRelation rel;
        QRelatedTableModel *model;
        QHash<QString, QVariant> dictionary;//maps keys to display values

    private:
        QSqlTableModel *m_parent;
        bool m_dictInitialized;
};


class SqlRelationalTableModel : public QSqlTableModel
{
    Q_OBJECT
public:
    SqlRelationalTableModel(QSqlDatabase sqldatabase, QObject *parent = 0);
    virtual ~SqlRelationalTableModel(){}
    void setRelation(int column, const QSqlRelation &relation);
    void setLinkRelation(int column, int sourceColumn, const QSqlRelation &relation);
    QAbstractItemModel* relationModel(int index) const;
signals:
public slots:
    void clearDirty();
private:
    mutable QVector<QRelation> relations;
    mutable QVector<QPair<QRelation, int> > linkrelations;
    QSet<int> m_dirtyRows;
    int m_checkableColumnIndex = -1;
    QSet<QString> m_checkedItems;
private slots:
    void somethingChanged(const QModelIndex topLeft, const QModelIndex& bottomRight);
public:
    QVariant data(const QModelIndex &index, int role) const;



    // QAbstractItemModel interface
public:
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const;

    // QAbstractItemModel interface
public:
    Qt::ItemFlags flags(const QModelIndex &index) const;
    int checkableColumnIndex() const;
    void setCheckableColumnIndex(int checkableColumnIndex);

    // QAbstractItemModel interface
public:
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);

    // QAbstractItemModel interface
public:
    virtual int columnCount(const QModelIndex &parent) const;
};



#endif // SQLRELATIONALTABLEMODEL_H
