#ifndef EREPFITINPUTFORCEEQUATIONDIALOG_H
#define EREPFITINPUTFORCEEQUATIONDIALOG_H

#include <QDialog>

QT_BEGIN_NAMESPACE
class QTableView;
QT_END_NAMESPACE

class DFTBRefDatabase;
class SqlRelationalTableModel;
class ErepfitInputForceEquationDialog : public QDialog
{
    Q_OBJECT
public:
    ErepfitInputForceEquationDialog(DFTBRefDatabase *database, QWidget *parent = 0);

signals:

public slots:
private slots:
    void addRow();
    void removeRow();
    void commitBut();
    bool commit();
    void revert();
private:
    QTableView* m_tableView;
    DFTBRefDatabase* m_database;
    SqlRelationalTableModel* m_model;

protected:
    void closeEvent(QCloseEvent *event);

};

#endif // EREPFITINPUTFORCEEQUATIONDIALOG_H
