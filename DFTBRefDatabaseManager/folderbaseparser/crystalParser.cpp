#include "crystalParser.hpp"

#include "geometry.h"
#include "bandstructuredata.h"

#include "dftbrefdatabase.h"
#include "geometrycoordinationmodel.h"

#include "klinesmodel.h"
#include "bandstructuremodel.h"
#include "BSOptTargetIndexBasedModel.h"

#include "tools.h"
#include <QUuid>



bool ParseBandStructure(const QMap<QString, QString>& geomIDs, const QDir& root, const QString& path, BandstructureData* data)
{
    QDir pathDir(path);


    QString refgeom = loadFile(root, pathDir.absoluteFilePath("REFGEOM")).first();

    QUuid uuid(refgeom);
    if (uuid.isNull())
    {
        if (!geomIDs.contains(refgeom))
        {
            qDebug() << "The reference geometry folder " << refgeom << " is not found in GEOMETRY";
            return false;
        }
        data->setGeomID(geomIDs[refgeom]);
    }
    else
    {
        data->setGeomID(uuid.toString());
    }


    QString name = loadFile(root, pathDir.absoluteFilePath("NAME")).first();
    if (name.isEmpty())
    {
        qDebug() << "NAME is not found or it is empty.";
        return false;
    }
    data->setName(name);

    QString method = loadFile(root, pathDir.absoluteFilePath("METHOD")).first();

    if (method.isEmpty())
    {
        qDebug() << "METHOD is not found or it is empty.";
        return false;
    }
    data->setMethod(method.toUpper());

    QString basis = loadFile(root, pathDir.absoluteFilePath("BASIS")).first();
    if (basis.isEmpty())
    {
//        qDebug() << "BASIS file is empty, skipped.";
    }
    else
    {
        data->setBasis(basis.toUpper());
    }

    QString comment = loadFile(root, pathDir.absoluteFilePath("COMMENT")).join("\n");
    if (comment.isEmpty())
    {
//        qDebug() << "COMMENT file is empty, skipped.";
    }
    else
    {
        data->setComment(comment);
    }

    QString citation = loadFile(root, pathDir.absoluteFilePath("CITATION")).join("\n");
    if (citation.isEmpty())
    {
//        qDebug() << "CITATION file is empty, skipped.";
    }
    else
    {
        data->setCitation(citation);
    }


//! TODO fixme
//    KLinesModel* klinesModel = new KLinesModel(data);
//    if (klinesModel->importHSDFile(pathDir.absoluteFilePath("KPOINTS")))
//    {
//        qDebug() << "KLINES loaded";
//    }
//    else
//    {
//        qDebug() << "Failed to load KLINES file";
//    }
//    delete klinesModel;

    BandStructureModel* bsModel = new BandStructureModel(data);
    if (bsModel->importPlainFile(pathDir.absoluteFilePath("BANDS")))
    {
//        qDebug() << "BANDS loaded";
    }
    else
    {
        qDebug() << "Failed to load BANDS file";
        return false;
    }
    delete bsModel;

    BSOptTargetIndexBasedModel* dopModel = new BSOptTargetIndexBasedModel(data);
    if (dopModel->importPlaintextFile(pathDir.absoluteFilePath("FITTING")))
    {
//        qDebug() << "FITTING loaded";
    }
    else
    {
        qDebug() << "Failed to load FITTING file";
        return false;
    }
    delete dopModel;


    QString selected = loadFile(root, pathDir.absoluteFilePath("SELECTED")).join("\n");
    if (selected.isEmpty())
    {

    }
    else
    {
        if (selected.startsWith("1"))
        {
            data->setConfining_fitting_selected(true);
        }
    }


    return true;
}

