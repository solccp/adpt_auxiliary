
#include <QDebug>

#include "molecularParser.h"
#include "molecularfrequencydata.h"
#include "molecularatomizationenergydata.h"
#include "tools.h"
#include <QUuid>

bool ParseMolecularFrequency(const QMap<QString, QString> &geom_name_uuid_molecule, const QDir &root, const QString &path, MolecularFrequencyData *data)
{
    QDir pathDir(path);


    QString refgeom = loadFile(root, pathDir.absoluteFilePath("REFGEOM")).first();
    QUuid uuid(refgeom);

    if (uuid.isNull())
    {
        if (!geom_name_uuid_molecule.contains(refgeom))
        {
            qDebug() << "The reference geometry folder " << refgeom << " is not found in GEOMETRY";
            return false;
        }
        data->setGeom_uuid(geom_name_uuid_molecule[refgeom]);
    }
    else
    {
        data->setGeom_uuid(uuid.toString());
    }

    QString method = loadFile(root, pathDir.absoluteFilePath("METHOD")).first();

    if (method.isEmpty())
    {
        qDebug() << "METHOD is not found or it is empty.";
        return false;
    }
    data->setMethod(method.toUpper());

    QString basis = loadFile(root, pathDir.absoluteFilePath("BASIS")).first();
    if (basis.isEmpty())
    {
//        qDebug() << "BASIS file is empty, skipped.";
    }
    else
    {
        data->setBasis(basis.toUpper());
    }


    QString comment = loadFile(root, pathDir.absoluteFilePath("COMMENT")).join("\n");
    if (comment.isEmpty())
    {
//        qDebug() << "COMMENT file is empty, skipped.";
    }
    else
    {
        data->setComment(comment);
    }

    QString citation = loadFile(root, pathDir.absoluteFilePath("CITATION")).join("\n");
    if (citation.isEmpty())
    {
//        qDebug() << "CITATION file is empty, skipped.";
    }
    else
    {
        data->setCitation(citation);
    }

    QList<double> freqs;
    QList<double> weights;
    QStringList frequency = loadFile(root, pathDir.absoluteFilePath("FREQ"));
    for(int i=0; i<frequency.size();++i)
    {
        bool ok;
        double value;
        if (frequency[i].trimmed().size()==0 || frequency[i].startsWith("#"))
        {
            continue;
        }
        QStringList arr = frequency[i].split(" ", QString::SkipEmptyParts);
        if (arr.size() == 2)
        {
            double weight = arr[1].toDouble(&ok);
            value = arr[0].toDouble(&ok);
            if (!ok)
            {
                qDebug() << "The format of FREQ file is incorrect, stopped.";
                return false;
            }
            freqs.append(value);
            weights.append(weight);
        }
        else
        {
            value = frequency[i].toDouble(&ok);
            if (!ok)
            {
                qDebug() << "The format of FREQ file is incorrect, stopped.";
                return false;
            }
            freqs.append(value);
        }

    }
    data->setFreq(freqs);
    if (weights.size() == freqs.size())
    {
        data->setWeight(weights);
    }
    return true;
}


bool ParseMolecularAtomizationEnergy(const QMap<QString, QString> &geom_name_uuid_molecule, const QDir &root, const QString &path, MolecularAtomizationEnergyData *data)
{
    QDir pathDir(path);


    QString refgeom = loadFile(root, pathDir.absoluteFilePath("REFGEOM")).first();

    QUuid uuid(refgeom);

    if (uuid.isNull())
    {
        if (!geom_name_uuid_molecule.contains(refgeom))
        {
            qDebug() << "The reference geometry folder " << refgeom << " is not found in GEOMETRY";
            return false;
        }
        data->setGeom_uuid(geom_name_uuid_molecule[refgeom]);
    }
    else
    {
        data->setGeom_uuid(uuid.toString());
    }

    QString method = loadFile(root, pathDir.absoluteFilePath("METHOD")).first();

    if (method.isEmpty())
    {
        qDebug() << "METHOD is not found or it is empty.";
        return false;
    }
    data->setMethod(method.toUpper());

    QString basis = loadFile(root, pathDir.absoluteFilePath("BASIS")).first();
    if (basis.isEmpty())
    {
//        qDebug() << "BASIS file is empty, skipped.";
    }
    else
    {
        data->setBasis(basis.toUpper());
    }

    QString comment = loadFile(root, pathDir.absoluteFilePath("COMMENT")).join("\n");
    if (comment.isEmpty())
    {
        qDebug() << "COMMENT file is empty, skipped.";
    }
    else
    {
        data->setComment(comment);
    }

    QString citation = loadFile(root, pathDir.absoluteFilePath("CITATION")).join("\n");
    if (citation.isEmpty())
    {
//        qDebug() << "CITATION file is empty, skipped.";
    }
    else
    {
        data->setCitation(citation);
    }

    QString energy_str = loadFile(root, pathDir.absoluteFilePath("ENERGY")).first();
    bool ok;
    double value;
    value = energy_str.toDouble(&ok);
    if (!ok)
    {
        qDebug() << "The format of ENERGY file is incorrect, stopped.";
        return false;
    }
    data->setEnergy(value);
    return true;
}