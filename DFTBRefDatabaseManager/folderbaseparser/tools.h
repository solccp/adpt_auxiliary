#ifndef TOOLS_H_94cbfa60_a922_4e54_838e_3d3e90104955
#define TOOLS_H_94cbfa60_a922_4e54_838e_3d3e90104955

#include <QStringList>
#include <QTextStream>
#include <QFile>
#include <QDebug>
#include <QDir>


QStringList loadFile(const QDir& root, const QString& filename);

bool openStream(QTextStream& stream, const QDir& root, QFile& file);



#endif // TOOLS_H
