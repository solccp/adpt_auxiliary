#ifndef CRYSTALPARSER_HPP_ab575f30_fc40_4408_8e17_942ee12b8426
#define CRYSTALPARSER_HPP_ab575f30_fc40_4408_8e17_942ee12b8426

#include <QDir>
#include <QString>


class Geometry;
class BandstructureData;

bool ParseBandStructure(const QMap<QString, QString>& geomIDs, const QDir& root, const QString& path, BandstructureData* data);

#endif // CRYSTALPARSER_HPP
