#ifndef MOLECULARPARSER_H_560643b1_139a_4a4c_b24c_e32806fdc8eb
#define MOLECULARPARSER_H_560643b1_139a_4a4c_b24c_e32806fdc8eb

#include <QMap>
#include <QString>
#include <QDir>

class MolecularFrequencyData;
class MolecularAtomizationEnergyData;
class Geometry;
bool ParseMolecularFrequency(const QMap<QString, QString>& geom_name_uuid_molecule, const QDir& root, const QString& path, MolecularFrequencyData* data);
bool ParseMolecularAtomizationEnergy(const QMap<QString, QString>& geom_name_uuid_molecule, const QDir& root, const QString& path, MolecularAtomizationEnergyData* data);



#endif // MOLEULARPARSER_H
