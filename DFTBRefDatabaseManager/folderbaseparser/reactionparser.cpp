#include "reactionparser.h"

#include "reactionenergydata.h"
#include "tools.h"
#include <QUuid>

bool ParseReactionEnergy(const QMap<QString, QString> &geom_name_uuid, const QSet<QString> &geom_uuid, const QDir &root, const QString &path, ReactionEnergyData *data)
{
    QDir pathDir(path);

    QString uuid_str = loadFile(root, pathDir.absoluteFilePath("UUID")).first();
    QUuid uuid(uuid_str);
    if (uuid.isNull())
    {
        qDebug() << "UUID is not found or it is empty.";
        return false;
    }
    data->setUUID(uuid.toString());


    QString name = loadFile(root, pathDir.absoluteFilePath("NAME")).first();
    if (name.isEmpty())
    {
        qDebug() << "NAME is not found or it is empty.";
        return false;
    }
    data->setName(name);


    QString method = loadFile(root, pathDir.absoluteFilePath("METHOD")).first();

    if (method.isEmpty())
    {
        qDebug() << "METHOD is not found or it is empty.";
        return false;
    }
    data->setMethod(method.toUpper());

    QString basis = loadFile(root, pathDir.absoluteFilePath("BASIS")).first();
    if (basis.isEmpty())
    {
//        qDebug() << "BASIS file is empty, skipped.";
    }
    else
    {
        data->setBasis(basis.toUpper());
    }

    QString comment = loadFile(root, pathDir.absoluteFilePath("COMMENT")).join("\n");
    if (comment.isEmpty())
    {
//        qDebug() << "COMMENT file is empty, skipped.";
    }
    else
    {
        data->setComment(comment);
    }

    QString citation = loadFile(root, pathDir.absoluteFilePath("CITATION")).join("\n");
    if (citation.isEmpty())
    {
//        qDebug() << "CITATION file is empty, skipped.";
    }
    else
    {
        data->setCitation(citation);
    }

    QString opt = loadFile(root, pathDir.absoluteFilePath("OPT")).first();
    if (!opt.isEmpty())
    {
        if (opt.toLower() == "false" || opt.toLower() == "no")
        {
            data->setOptimizeComponents(false);
        }
    }


    QString energy_str = loadFile(root, pathDir.absoluteFilePath("ENERGY")).first();
    bool ok;
    double value;
    value = energy_str.toDouble(&ok);
    if (!ok)
    {
        qDebug() << "The format of ENERGY file is incorrect, stopped.";
        return false;
    }
    data->setEnergy(value);


    QStringList reacts = loadFile(root, pathDir.absoluteFilePath("REACTANTS"));
    QStringList prods = loadFile(root, pathDir.absoluteFilePath("PRODUCTS"));


    if (reacts.isEmpty() || prods.isEmpty())
    {
        qDebug() << "REACTANTS or PRODUCTS is not found or it is empty.";
        return false;
    }

    bool allMole = true;

    QList<ReactionItem> reactants;
    for(int i=0; i<reacts.size();++i)
    {
        QString str = reacts.at(i);
        QStringList arr = str.split(QRegExp("\\s+"), QString::SkipEmptyParts);
        double factor;
        if (arr.size() != 2)
        {
            allMole = false;
            break;
        }
        bool ok;
        factor = arr[0].toDouble(&ok);
        if (!ok)
        {
            allMole = false;
            break;
        }
        QUuid uuid(arr[1]);
        if (uuid.isNull())
        {
            if (!geom_name_uuid.contains(arr[1]))
            {
                qDebug() << arr[1] << " is not found!";
                allMole = false;
                break;
            }
            else
            {
                ReactionItem item;
                item.coeff = factor;
                item.uuid = geom_name_uuid[arr[1]];
                reactants.append(item);
            }
        }
        else
        {
            if (geom_uuid.contains(uuid.toString()))
            {
                ReactionItem item;
                item.coeff = factor;
                item.uuid = uuid.toString();
                reactants.append(item);
            }
            else
            {
                qDebug() << uuid.toString() << " is not found!";
                allMole = false;
                break;
            }
        }

    }

    QList<ReactionItem> products;
    for(int i=0; i<prods.size();++i)
    {
        QString str = prods.at(i);
        QStringList arr = str.split(QRegExp("\\s+"), QString::SkipEmptyParts);
        double factor;
        if (arr.size() != 2)
        {
            allMole = false;
            break;
        }
        bool ok;
        factor = arr[0].toDouble(&ok);
        if (!ok)
        {
            allMole = false;
            break;
        }
        QUuid uuid(arr[1]);
        if (uuid.isNull())
        {
            if (!geom_name_uuid.contains(arr[1]))
            {
                qDebug() << arr[1] << " is not found!";
                allMole = false;
                break;
            }
            else
            {
                ReactionItem item;
                item.coeff = factor;
                item.uuid = geom_name_uuid[arr[1]];
                products.append(item);
            }
        }
        else
        {
            if (geom_uuid.contains(uuid.toString()))
            {
                ReactionItem item;
                item.coeff = factor;
                item.uuid = uuid.toString();
                products.append(item);
            }
            else
            {
                qDebug() << uuid.toString() << " is not found!";
                allMole = false;
                break;
            }
        }
    }


    if (allMole)
    {

        data->setReactants(reactants);
        data->setProducts(products);
        return true;
    }
    else
    {
        return false;
    }
}
