#ifndef REACTIONPARSER_H_6d027427_08df_494d_bf2f_6ae5d92d60e6
#define REACTIONPARSER_H_6d027427_08df_494d_bf2f_6ae5d92d60e6

#include <QMap>
#include <QString>
#include <QDir>

class ReactionEnergyData;
class Geometry;

bool ParseReactionEnergy(const QMap<QString, QString>& geom_name_uuid, const QSet<QString>& geom_uuid,
                             const QDir& root, const QString& path, ReactionEnergyData* data);




#endif // REACTIONPARSER_H
