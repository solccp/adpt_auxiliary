#include "tools.h"



QStringList loadFile(const QDir& root, const QString& filename)
{
//    qDebug() << "Reading file " << root.relativeFilePath(filename);
    QStringList res;
    QFile file(filename);
    file.open(QIODevice::ReadOnly);
    QTextStream stream(&file);
    while(!stream.atEnd())
    {
        QString temp = stream.readLine();
        res.append(temp);
    }
    if (res.isEmpty())
    {
        res.append(QString());
    }
    return res;
}

bool openStream(QTextStream &stream, const QDir &root, QFile &file)
{
//    qDebug() << "Reading file " << root.relativeFilePath(file.fileName());

    if (file.open(QIODevice::ReadOnly))
    {
        stream.setDevice(&file);
        return true;
    }
    else
    {
        return false;
    }
}
