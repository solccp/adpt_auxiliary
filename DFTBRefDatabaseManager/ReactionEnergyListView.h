#ifndef REACTIONENERGYLISTVIEW_H
#define REACTIONENERGYLISTVIEW_H


#include "DataListViewBase.h"

class ReactionEnergyData;
class ReactionEnergyListView : public DataListViewBase
{
    Q_OBJECT
public:
    ReactionEnergyListView(DFTBRefDatabase *database, QWidget *parent = 0);

signals:

public slots:

    // DataListViewBase interface
protected:
    void deleteFromDatabase(const QStringList &uuids);
    void addToDatabase();
    void editItemInDatabase(const QString& uuid);
    void duplicateItemInDatabase(const QString& uuid);
    void adjustTableView();
    void initModels();
    int propertyID();
private:
    int showEditor(ReactionEnergyData* data);


};

#endif // REACTIONENERGYLISTVIEW_H
