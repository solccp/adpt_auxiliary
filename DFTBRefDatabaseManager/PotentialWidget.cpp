#include "PotentialWidget.h"
#include "atomicproperties.h"


#include <QComboBox>
#include <QHBoxLayout>
#include <QLabel>


PotentialWidget::PotentialWidget(QWidget *parent) :
    QWidget(parent)
{
    QHBoxLayout *layout = new QHBoxLayout();
    this->setLayout(layout);
    layout->setMargin(0);

    m_comboElem1 = new QComboBox();
    m_comboElem2 = new QComboBox();

    m_comboElem1->setMinimumWidth(20);
    m_comboElem2->setMinimumWidth(20);

    QLabel *label = new QLabel(tr("-"));

    layout->addWidget(m_comboElem1);
    layout->addWidget(label, 0, Qt::AlignCenter);
    layout->addWidget(m_comboElem2);

    QVector<QString> allSymbols = ADPT::AtomicProperties::allSymbols();

    for (int i=0; i<allSymbols.size(); ++i)
    {
        m_sortedElements.append(allSymbols[i]);
        m_comboElem1->addItem(allSymbols[i]);
        m_comboElem2->addItem(allSymbols[i]);
    }
    m_sortedElements.sort();
    connect(m_comboElem1, SIGNAL(currentTextChanged(QString)), this, SLOT(elem1changed(QString)));
}

QString PotentialWidget::getElement1()
{
    return m_comboElem1->currentText();
}

QString PotentialWidget::getElement2()
{
    return m_comboElem2->currentText();
}

void PotentialWidget::setElement1(const QString &elem)
{
    m_comboElem1->setCurrentText(elem);
}

void PotentialWidget::setElement2(const QString &elem)
{
    m_comboElem2->setCurrentText(elem);
}

template <typename T1, typename T2>
inline bool QPairFirstLessThan(const QPair<T1,T2>& lhs, const QPair<T1,T2>& rhs)
{
    return lhs.first < rhs.first;
}

template <typename T1, typename T2>
bool QPairSecondLessThan(const QPair<T1,T2>& lhs, const QPair<T1,T2>& rhs)
{
    return lhs.second < rhs.second;
}




void PotentialWidget::elem1changed(const QString &elem1)
{
    QString oldelem2 = m_comboElem2->currentText();
    int index = m_sortedElements.indexOf(elem1);
    QList<QPair<int, QString>> newList;
    for(int i=index; i<m_sortedElements.size();++i)
    {
        QString elem = m_sortedElements.at(i);
        newList.append(qMakePair(ADPT::AtomicProperties::fromSymbol(elem)->getNumber(), elem));
    }

    qSort(newList.begin(), newList.end(), QPairFirstLessThan<int, QString>);

    m_comboElem2->clear();
    QListIterator<QPair<int, QString>> myit(newList);
    while(myit.hasNext())
    {
        const QPair<int, QString>& pair = myit.next();
        m_comboElem2->addItem(pair.second);
    }
    m_comboElem2->setCurrentText(oldelem2);
}
