#include "SqlRelationDelagate.h"

#include <QDebug>

#include "sqlrelationaltablemodel.h"
#include "singleselectlistwidget.h"

SqlRelationDelagate::SqlRelationDelagate(QObject *parent) :
    QStyledItemDelegate(parent)
{
}


QWidget *SqlRelationDelagate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    const SqlRelationalTableModel *model = qobject_cast<const SqlRelationalTableModel*>(index.model());
    if (!model)
        return nullptr;

    SingleSelectListWidget *w = new SingleSelectListWidget(parent);
    w->setWindowFlags(Qt::Popup);
    w->setModel(model->relationModel(index.column()));
    w->setIndexColumn(0);
    connect(w, SIGNAL(valueSelected(int,QVariant,QString)), w, SLOT(close()));
    w->installEventFilter(const_cast<SqlRelationDelagate *>(this));
    return w;
}

void SqlRelationDelagate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    SingleSelectListWidget* w = qobject_cast<SingleSelectListWidget*>(editor);
    if (w)
    {
        QVariant res = w->currentIndex();
        if (res.isValid())
        {
            model->setData(index, res, Qt::EditRole);
        }
    }
}


void SqlRelationDelagate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    if (!index.isValid())
        return;

    SingleSelectListWidget *w = qobject_cast<SingleSelectListWidget*>(editor);
    if (w)
    {
        w->setCurrentIndex(index.model()->data(index, Qt::EditRole));
    }
}

void SqlRelationDelagate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QRect rect = option.rect;
    QSize sizeHint = editor->sizeHint();

    QWidget* parent = editor->parentWidget();
    if (parent)
    {
        QPoint pos5 = parent->mapToGlobal(rect.topLeft());
        rect.setTopLeft(pos5);
    }
    rect.setWidth(sizeHint.width());
    rect.setHeight(sizeHint.height());
    editor->setGeometry(rect);
}
