#include "ReferenceDataImportor.h"

#include "dftbrefdatabase.h"

#include "geometry.h"
#include <Variant/Variant.h>
#include <Variant/SchemaLoader.h>

#include "geometrymodel.h"
#include "datastructure_variant.h"

#include <QtWidgets>

#include <QProgressDialog>

#include <thread>
#include <chrono>
#include <future>

libvariant::Variant Worker::readSchema(const QString& file)
{
    QFile schema_file(file);
    schema_file.open(QIODevice::ReadOnly);
    QString schema_str = schema_file.readAll();
    libvariant::Variant schema = libvariant::Deserialize(schema_str.toStdString(),libvariant::SERIALIZE_JSON);
    schema_file.close();
    return schema;
}

ReferenceDataImportor::ReferenceDataImportor(DFTBRefDatabase* database, QWidget *parent)
    : QMainWindow(parent), m_database(database)
{
    setupUI();
}

void ReferenceDataImportor::load_reference_file(const QString &filename, bool yaml_input)
{
    QProgressDialog pd(this);
    pd.setMinimumDuration(0);
    pd.setMinimum(0);
    pd.setMaximum(0);
    pd.setAutoClose(false);
    pd.setAutoReset(false);
    pd.setWindowModality(Qt::WindowModal);


    QThread* thread = new QThread;
    Worker *worker = new Worker();
    worker->moveToThread(thread);

    connect(thread, &QThread::started, [&, this]() {
        worker->import_worker(this, filename, yaml_input);
    } );

    connect(worker, SIGNAL(finished()), &pd, SLOT(accept()));
    connect(worker, SIGNAL(canceled()), &pd, SLOT(reject()));
    connect(worker, SIGNAL(failed()), &pd, SLOT(reject()));
//    connect(worker, SIGNAL(progressUpdtae(int)), &pd, SLOT(setValue(int)));
    connect(worker, SIGNAL(textUpdated(QString)), &pd, SLOT(setLabelText(QString)));
//    connect(worker, &Worker::rangeUpdated, [&](int min, int max) {
//        pd.setValue(0);
//        pd.setRange(min, max);
//    } );

    connect(worker, SIGNAL(finished()), worker, SLOT(deleteLater()));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    thread->start();
    pd.exec();
    m_tableview_geometry->sortByColumn(1);


    auto mapping = m_model_geometry->UUID_mapping();
    QMapIterator<QString, QString> it(mapping);
    while(it.hasNext())
    {
        it.next();
        qDebug() << it.key() << it.value();
    }




}

void ReferenceDataImportor::clear()
{
    m_model_geometry->clear();
}

void ReferenceDataImportor::import()
{
//! TODO finish this


    m_database->disableTransaction();

    m_database->database().transaction();
    bool failed = false;
    try
    {
        for(auto const & item : m_model_geometry->m_data)
        {
            if (m_model_geometry->isChecked(item.get()->UUID()))
            {
                bool succ = m_database->addGeometry(item.get());
                if (!succ)
                {
                    m_database->database().rollback();
                    failed = true;
                    break;
                }
            }
        }

        for(auto const & item : m_model_bandstructure->m_data)
        {
            bool succ = m_database->addBandStructure(item.get());
            if (!succ)
            {
                m_database->database().rollback();
                failed = true;
                break;
            }
        }

        for(auto const & item : m_model_atomization_energy->m_data)
        {
            bool succ = m_database->addMolecularAtomizationEnergy(item.get());
            if (!succ)
            {
                m_database->database().rollback();
                failed = true;
                break;
            }
        }


        for(auto const & item : m_model_reaction_energy->m_data)
        {
            bool succ = m_database->addReactionEnergy(item.get());
            if (!succ)
            {
                m_database->database().rollback();
                failed = true;
                break;
            }
        }

        for(auto const & item : m_model_frequency->m_data)
        {
            bool succ = m_database->addMolecularFrequency(item.get());
            if (!succ)
            {
                m_database->database().rollback();
                failed = true;
                break;
            }
        }

        if (!failed)
        {
            m_database->database().commit();
        }

    }
    catch(...)
    {
        m_database->database().rollback();
    }

    m_database->enableTransaction();
    this->close();

}

void ReferenceDataImportor::cancel()
{
    this->clear();
    this->close();
}

void ReferenceDataImportor::loadFile()
{

    QString filter;
    QString filename = QFileDialog::getOpenFileName(
                this, tr("Open data file"), QDir::homePath(),
                tr("JSON datafile (*.json);;YAML datafile (*.yaml)"), &filter );

    if (!filename.isNull() && !filename.isEmpty())
    {
        if (filter == "JSON datafile (*.json)")
        {
            load_reference_file(filename, false);
        }
        else if (filter == "YAML datafile (*.yaml)")
        {
            load_reference_file(filename, true);
        }       
    }
}

void ReferenceDataImportor::setupUI()
{
    QMenu *menu_file = new QMenu("&File");
    menu_file->addAction("Open datafile", this, SLOT(loadFile()));
    this->menuBar()->addMenu(menu_file);

    m_tabWidget = new QTabWidget();

    INIT_MODEL(GeometryModel, "Geometry", geometry)
    INIT_MODEL(BandstructureDataModel, "Bandstructure", bandstructure)
    INIT_MODEL(AtomizationEnergyModel, "Atomization Energy", atomization_energy)
    INIT_MODEL(FrequencyModel, "Frequency", frequency)
    INIT_MODEL(ReactionEnergyModel, "Reaction Energy", reaction_energy)




    auto widget = new QWidget();

    auto layout = new QVBoxLayout();
    layout->setMargin(3);
    layout->addWidget(m_tabWidget);

    auto hbox = new QHBoxLayout();
    auto button_OK = new QPushButton(tr("Import"));
    auto button_cancel = new QPushButton(tr("Cancel"));

    connect(button_OK, SIGNAL(clicked(bool)), this, SLOT(import()));
    connect(button_cancel, SIGNAL(clicked(bool)), this, SLOT(cancel()));

    hbox->addStretch(5);
    hbox->addWidget(button_OK);
    hbox->addWidget(button_cancel);


    layout->addLayout(hbox);


    widget->setLayout(layout);

    this->setCentralWidget(widget);
}



void Worker::import_worker(ReferenceDataImportor *importer, const QString &filename, bool yaml_input)
{
    libvariant::Variant var;


    emit textUpdated("Loading file...");
    if (yaml_input)
    {
        var = libvariant::DeserializeYAMLFile(filename.toStdString().c_str());
    }
    else
    {
        var = libvariant::DeserializeJSONFile(filename.toStdString().c_str());
    }

    if (m_isCancelled)
    {
        emit canceled();
        return ;
    }

//        libvariant::Variant schema_geometry = readSchema(":/schemas/geometry.json");
//        libvariant::Variant schema_bandstructure = readSchema(":/schemas/bandstructure.json");
//        libvariant::Variant schema_atomization_energy = readSchema(":/schemas/atomization_energy.json");
//        libvariant::Variant schema_molecular_frequency = readSchema(":/schemas/molecular_frequency.json");
//        libvariant::Variant schema_reaction_energy = readSchema(":/schemas/reaction_energy.json");


    {
        libvariant::AdvSchemaLoader loader;

        libvariant::Variant schema = readSchema(":/schemas/reference_data.json");
        libvariant::Variant empty;
        loader.AddSchema("geometry.json", empty);
        loader.AddSchema("atomization_energy.json", empty);
        loader.AddSchema("bandstructure.json", empty);
        loader.AddSchema("molecular_frequency.json", empty);
        loader.AddSchema("reaction_energy.json", empty);

        emit textUpdated("Validating file...");
        libvariant::SchemaResult result = libvariant::SchemaValidate(schema, var, &loader);
        if (result.Error())
        {
            auto schema_errors = result.GetSchemaErrors();

            std::stringstream stream;

            for(unsigned i=0; i<schema_errors.size();++i)
            {
                schema_errors[i].PrettyPrintMessage(stream);
            }
            auto validation_errors = result.GetErrors();
            for(unsigned i=0; i<validation_errors.size();++i)
            {
                validation_errors[i].PrettyPrintMessage(stream);
            }
            qDebug() << QString::fromStdString(stream.str());
            emit failed();
            return ;
        }
    }
    {
        libvariant::AdvSchemaLoader loader;
        libvariant::Variant schema = readSchema(":/schemas/geometry.json");
        emit textUpdated("Validating Geometry...");
        libvariant::Variant subnode = var["geometry"];
        int size = subnode.Size();
        emit rangeUpdated(0, size);
        for(int item=0; item<size;++item)
        {
            if (m_isCancelled)
            {
                emit canceled();
                return ;
            }
            emit progressUpdtae(item);
            emit textUpdated(QString("Loading geometry %1 of %2").arg(item+1).arg(size));

            libvariant::SchemaResult result = libvariant::SchemaValidate(schema, subnode[item], &loader);
            if (result.Error())
            {
                auto schema_errors = result.GetSchemaErrors();
                std::stringstream stream;
                for(unsigned i=0; i<schema_errors.size();++i)
                {
                    schema_errors[i].PrettyPrintMessage(stream);
                }
                auto validation_errors = result.GetErrors();
                for(unsigned i=0; i<validation_errors.size();++i)
                {
                    validation_errors[i].PrettyPrintMessage(stream);
                }
                qDebug() << qPrintable(QString::fromStdString(stream.str()));
                emit failed();
                return ;
            }
            emit textUpdated(QString("Loading geometry %1 of %2").arg(item+1).arg(size));
            std::shared_ptr<Geometry> geom = std::make_shared<Geometry>();
            libvariant::VariantConvertor<Geometry>::fromVariant(subnode[item], *geom);
            importer->m_model_geometry->addGeometry(geom);
        }
    }

    auto geom_uuid_mapping = importer->m_model_geometry->UUID_mapping();
    auto geom_uuid_name = importer->m_model_geometry->getMapping(1);
    auto geom_uuid_elements = importer->m_model_geometry->getMapping(9);


    //! Bandstructure
    if (var.Contains("bandstructure"))
    {         
        libvariant::AdvSchemaLoader loader;
        libvariant::Variant schema = readSchema(":/schemas/bandstructure.json");
        emit textUpdated("Validating Bandstructure...");
        libvariant::Variant subnode = var["bandstructure"];
        int size = subnode.Size();
        emit rangeUpdated(0, size);
        for(int item=0; item<size;++item)
        {
            if (m_isCancelled)
            {
                emit canceled();
                return ;
            }
            emit progressUpdtae(item);
            emit textUpdated(QString("Loading bandstructure %1 of %2").arg(item+1).arg(size));

            libvariant::SchemaResult result = libvariant::SchemaValidate(schema, subnode[item], &loader);
            if (result.Error())
            {
                auto schema_errors = result.GetSchemaErrors();
                std::stringstream stream;
                for(unsigned i=0; i<schema_errors.size();++i)
                {
                    schema_errors[i].PrettyPrintMessage(stream);
                }
                auto validation_errors = result.GetErrors();
                for(unsigned i=0; i<validation_errors.size();++i)
                {
                    validation_errors[i].PrettyPrintMessage(stream);
                }
                qDebug() << QString::fromStdString(stream.str());
                emit failed();
                return ;
            }
            emit textUpdated(QString("Loading bandstructure %1 of %2").arg(item+1).arg(size));
            std::shared_ptr<BandstructureData> geom = std::make_shared<BandstructureData>();
            libvariant::VariantConvertor<BandstructureData>::fromVariant(subnode[item], *geom);

            if (geom_uuid_mapping.contains(geom->getGeomID()))
            {
                geom->setGeomID(geom_uuid_mapping[geom->getGeomID()]);
            }
            importer->m_model_bandstructure->addData(geom);
        }

        importer->m_model_bandstructure->setDataMapping(2, geom_uuid_name);
        importer->m_model_bandstructure->setDataMapping(5, geom_uuid_elements);
    }


    //! AtomizationEnergy
    if (var.Contains("atomization_energy"))
    {
        libvariant::AdvSchemaLoader loader;
        libvariant::Variant schema = readSchema(":/schemas/atomization_energy.json");
        emit textUpdated("Validating Atomization Energy...");
        libvariant::Variant subnode = var["atomization_energy"];
        int size = subnode.Size();
        emit rangeUpdated(0, size);
        for(int item=0; item<size;++item)
        {
            if (m_isCancelled)
            {
                emit canceled();
                return ;
            }
            emit progressUpdtae(item);
            emit textUpdated(QString("Loading Atomization Energy %1 of %2").arg(item+1).arg(size));

            libvariant::SchemaResult result = libvariant::SchemaValidate(schema, subnode[item], &loader);
            if (result.Error())
            {
                auto schema_errors = result.GetSchemaErrors();
                std::stringstream stream;
                for(unsigned i=0; i<schema_errors.size();++i)
                {
                    schema_errors[i].PrettyPrintMessage(stream);
                }
                auto validation_errors = result.GetErrors();
                for(unsigned i=0; i<validation_errors.size();++i)
                {
                    validation_errors[i].PrettyPrintMessage(stream);
                }
                qDebug() << QString::fromStdString(stream.str());
                emit failed();
                return ;
            }
            emit textUpdated(QString("Loading Atomization Energy %1 of %2").arg(item+1).arg(size));
            auto geom = std::make_shared<MolecularAtomizationEnergyData>();
            libvariant::VariantConvertor<MolecularAtomizationEnergyData>::fromVariant(subnode[item], *geom);

            auto geom_uuid = geom->geom_uuid();
            if (geom_uuid_mapping.contains(geom_uuid))
            {
                geom->setGeom_uuid(geom_uuid_mapping[geom_uuid]);
            }
            importer->m_model_atomization_energy->addData(geom);
        }

        importer->m_model_atomization_energy->setDataMapping(1, geom_uuid_name);
        importer->m_model_atomization_energy->setDataMapping(6, geom_uuid_elements);
    }



    //! Frequency
    if (var.Contains("molecular_frequency"))
    {
        libvariant::AdvSchemaLoader loader;
        libvariant::Variant schema = readSchema(":/schemas/molecular_frequency.json");
        emit textUpdated("Validating Frequency...");
        libvariant::Variant subnode = var["molecular_frequency"];
        int size = subnode.Size();
        emit rangeUpdated(0, size);
        for(int item=0; item<size;++item)
        {
            if (m_isCancelled)
            {
                emit canceled();
                return ;
            }
            emit progressUpdtae(item);
            emit textUpdated(QString("Loading Frequency %1 of %2").arg(item+1).arg(size));

            libvariant::SchemaResult result = libvariant::SchemaValidate(schema, subnode[item], &loader);
            if (result.Error())
            {
                auto schema_errors = result.GetSchemaErrors();
                std::stringstream stream;
                for(unsigned i=0; i<schema_errors.size();++i)
                {
                    schema_errors[i].PrettyPrintMessage(stream);
                }
                auto validation_errors = result.GetErrors();
                for(unsigned i=0; i<validation_errors.size();++i)
                {
                    validation_errors[i].PrettyPrintMessage(stream);
                }
                qDebug() << QString::fromStdString(stream.str());
                emit failed();
                return ;
            }
            emit textUpdated(QString("Loading Frequency %1 of %2").arg(item+1).arg(size));
            auto geom = std::make_shared<MolecularFrequencyData>();
            libvariant::VariantConvertor<MolecularFrequencyData>::fromVariant(subnode[item], *geom);

            auto geom_uuid = geom->geom_uuid();
            if (geom_uuid_mapping.contains(geom_uuid))
            {
                geom->setGeom_uuid(geom_uuid_mapping[geom_uuid]);
            }
            importer->m_model_frequency->addData(geom);
        }

        importer->m_model_frequency->setDataMapping(1, geom_uuid_name);
        importer->m_model_frequency->setDataMapping(4, geom_uuid_elements);
    }


    //! Reaction energy
    if (var.Contains("reaction_energy"))
    {
        libvariant::AdvSchemaLoader loader;
        libvariant::Variant schema = readSchema(":/schemas/reaction_energy.json");
        emit textUpdated("Validating Reaction Energy...");
        libvariant::Variant subnode = var["reaction_energy"];
        int size = subnode.Size();
        emit rangeUpdated(0, size);
        for(int item=0; item<size;++item)
        {
            if (m_isCancelled)
            {
                emit canceled();
                return ;
            }
            emit progressUpdtae(item);
            emit textUpdated(QString("Loading Reaction Energy %1 of %2").arg(item+1).arg(size));

            libvariant::SchemaResult result = libvariant::SchemaValidate(schema, subnode[item], &loader);
            if (result.Error())
            {
                auto schema_errors = result.GetSchemaErrors();
                std::stringstream stream;
                for(unsigned i=0; i<schema_errors.size();++i)
                {
                    schema_errors[i].PrettyPrintMessage(stream);
                }
                auto validation_errors = result.GetErrors();
                for(unsigned i=0; i<validation_errors.size();++i)
                {
                    validation_errors[i].PrettyPrintMessage(stream);
                }
                qDebug() << QString::fromStdString(stream.str());
                emit failed();
                return ;
            }
            emit textUpdated(QString("Loading Reaction Energy %1 of %2").arg(item+1).arg(size));
            std::shared_ptr<ReactionEnergyData> geom = std::make_shared<ReactionEnergyData>();
            libvariant::VariantConvertor<ReactionEnergyData>::fromVariant(subnode[item], *geom);

            for(auto & item : geom->products())
            {
                auto geom_uuid = item.uuid;
                if (geom_uuid_mapping.contains(geom_uuid))
                {
                    item.uuid = geom_uuid_mapping[geom_uuid];
                }
            }

            for(auto & item : geom->reactants())
            {
                auto geom_uuid = item.uuid;
                if (geom_uuid_mapping.contains(geom_uuid))
                {
                    item.uuid = geom_uuid_mapping[geom_uuid];
                }
            }
            importer->m_model_reaction_energy->addData(geom);
        }
    }


    emit finished();
}


Worker::Worker(QObject *parent) :QObject(parent)
{
    m_isCancelled = false;
}

void Worker::setCancelled()
{
    m_isCancelled = true;
}
