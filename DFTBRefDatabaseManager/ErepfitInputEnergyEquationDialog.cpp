#include "ErepfitInputEnergyEquationDialog.h"

#include <QtWidgets>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QDebug>

#include "SqlRelationDelagate.h"
#include "sqlrelationaltablemodel.h"

#include "singleselectlistwidget.h"

#include "doublenumbercelldelegate.h"
#include "dftbrefdatabase.h"

ErepfitInputEnergyEquationDialog::ErepfitInputEnergyEquationDialog(DFTBRefDatabase *database, QWidget *parent) :
    QDialog(parent), m_database(database)
{
    this->setWindowTitle(tr("Erepfit Energy Equations"));
    m_model = new SqlRelationalTableModel(m_database->database(), this);
    m_model->setTable("erepfit_default_input_energy_equation");
    m_model->setEditStrategy(QSqlTableModel::OnManualSubmit);

    m_model->setRelation(0, QSqlRelation("MolecularGeometry_IDSTR", "geom_uuid", "name"));
    m_model->setRelation(1, QSqlRelation("AE_IDSTR", "ae_uuid", "energy"));

    m_model->select();
    m_model->setHeaderData(0, Qt::Horizontal, tr("Name"));
    m_model->setHeaderData(1, Qt::Horizontal, tr("Energy"));
    m_model->setHeaderData(2, Qt::Horizontal, tr("Weight"));
//    m_model->setHeaderData(3, Qt::Horizontal, tr("Force weight"));

    m_tableView = new QTableView(this);
    m_tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    m_tableView->setSelectionBehavior(QAbstractItemView::SelectRows);

    m_tableView->setModel(m_model);
    m_tableView->setAlternatingRowColors(true);

    m_tableView->setItemDelegateForColumn(0, new SqlRelationDelagate());
    m_tableView->setItemDelegateForColumn(1, new SqlRelationDelagate());

    m_tableView->setItemDelegateForColumn(2, new DoubleNumberCellDelegate());
//    m_tableView->setItemDelegateForColumn(3, new DoubleNumberCellDelegate());

    this->resize(800,400);

    m_tableView->resizeColumnsToContents();

    QVBoxLayout *layout = new QVBoxLayout(this);
    this->setLayout(layout);

    QHBoxLayout *hbox = new QHBoxLayout();
    QPushButton* butAdd = new QPushButton(tr("Add"));
    connect(butAdd, SIGNAL(clicked()), this, SLOT(addRow()));
    hbox->addWidget(butAdd);
    QPushButton* butDel = new QPushButton(tr("Remove"));
    connect(butDel, SIGNAL(clicked()), this, SLOT(removeRow()));
    hbox->addWidget(butDel);

    QPushButton* butCommit = new QPushButton(tr("Commit"));
    connect(butCommit, SIGNAL(clicked()), this, SLOT(commitBut()));
    hbox->addWidget(butCommit);

    QPushButton* butRevert = new QPushButton(tr("Revert"));
    connect(butRevert, SIGNAL(clicked()), this, SLOT(revert()));
    hbox->addWidget(butRevert);
    hbox->addStretch(5);

    layout->addLayout(hbox);
    layout->addWidget(m_tableView);

    QPushButton* butClose = new QPushButton(tr("Close"));
    butClose->setDefault(true);
    connect(butClose, SIGNAL(clicked()), this, SLOT(close()));

    hbox = new QHBoxLayout();

    hbox->addStretch(5);
    hbox->addWidget(butClose);
    layout->addLayout(hbox);

    if (!m_database->canWrite())
    {
        butAdd->setEnabled(false);
        butRevert->setEnabled(false);
        butCommit->setEnabled(false);
        butDel->setEnabled(false);
        m_tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    }

}

void ErepfitInputEnergyEquationDialog::addRow()
{
    int row = m_model->rowCount();
    m_model->insertRow(row);
    m_model->setData(m_model->index(row,2), 1.0);
//    m_model->setData(m_model->index(row,3), 1.0);
}

void ErepfitInputEnergyEquationDialog::removeRow()
{
    QModelIndexList list = m_tableView->selectionModel()->selectedRows();
    if (!list.empty())
    {
        for (int i=list.size()-1; i>=0; --i)
        {
            int row = list.at(i).row();
            m_tableView->model()->removeRows(row,1);
        }
    }
}

void ErepfitInputEnergyEquationDialog::commitBut()
{
    if (m_model->isDirty() && commit())
    {
        QMessageBox qbox2(this);
        qbox2.setText("Data written into database.");
        qbox2.setStandardButtons(QMessageBox::Ok);
        qbox2.setIcon(QMessageBox::Information);
        qbox2.exec();
    }
}

bool ErepfitInputEnergyEquationDialog::commit()
{
    m_model->database().transaction();
    if(m_model->submitAll())
    {
        m_model->database().commit();
    }
    else
    {
        m_model->database().rollback();
        QMessageBox qbox2(this);
        qbox2.setText("Failed to write data into database:" + m_model->lastError().text());
        qbox2.setDetailedText( m_model->query().lastQuery());
        qbox2.setStandardButtons(QMessageBox::Ok);
        qbox2.setIcon(QMessageBox::Critical);
        qbox2.exec();
        return false;
    }
    return true;
}

void ErepfitInputEnergyEquationDialog::revert()
{
    m_model->revertAll();
}

void ErepfitInputEnergyEquationDialog::closeEvent(QCloseEvent *event)
{
    if (m_model->isDirty())
    {
        QMessageBox qbox(this);
        qbox.setText("The changes have not been updated in the database.\n"
                     "Do you want to update them?");
        qbox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        qbox.setIcon(QMessageBox::Question);
        int ret = qbox.exec();
        if (ret == QMessageBox::Yes)
        {
            bool succ = commit();
            if (!succ)
            {
                event->ignore();
                return;
            }
        }
        else
        {
            event->accept();
        }
    }
    event->accept();
}
