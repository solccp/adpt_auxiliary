#include "BandstructureListView.h"

#include "bandstructuredataeditor.h"
#include "bandstructuredata.h"

#include "dftbrefdatabase.h"

#include <QSqlTableModel>

BandstructureListView::BandstructureListView(DFTBRefDatabase* database, QWidget *parent) :
    DataListViewBase(database, parent)
{
    setupUI();
    connect(m_database, SIGNAL(geometryUpdated()), this, SLOT(reload()));
}

void BandstructureListView::deleteFromDatabase(const QStringList &uuids)
{
    m_database->removeBandStructure(uuids);
}

void BandstructureListView::addToDatabase()
{
    QAbstractTableModel* model = m_database->getGeometryTableModel(DFTBRefDatabase::Crystal);

    if (model->rowCount() == 0)
    {
        QMessageBox qbox(this);
        qbox.setText("Crystal geometry needs to be defined first");
        qbox.setStandardButtons(QMessageBox::Ok);
        qbox.setIcon(QMessageBox::Critical);
        qbox.exec();
        return;
    }

    BandstructureData* bsData = new BandstructureData();
    bsData->setGeomID(model->data(model->index(0,0)).toString());


    while(1)
    {
        int ret = showEditor(bsData);
        if (ret)
        {
            QString errormsg;
            if (!bsData->checkData(errormsg))
            {
                QMessageBox qbox(this);
                qbox.setText(errormsg);
                qbox.setStandardButtons(QMessageBox::Ok);
                qbox.setIcon(QMessageBox::Critical);
                qbox.exec();
                continue;
            }

            bsData->resetUUID();
            if (!m_database->addBandStructure(bsData))
            {
                QMessageBox qbox(this);
                qbox.setText( m_database->getLastError());
                qbox.setStandardButtons(QMessageBox::Ok);
                qbox.setIcon(QMessageBox::Critical);
                qbox.exec();
                continue;
            }
            m_model->select();
            break;
        }
        break;
    }

    delete model;
    delete bsData;
}

void BandstructureListView::editItemInDatabase(const QString &uuid)
{
//    const QAbstractItemModel *mymodel = uuid.model();
//    QVariant uuid = mymodel->data(mymodel->index(uuid.row(),0), Qt::EditRole);


    BandstructureData* bsData = new BandstructureData();

    m_database->getBandStructure(uuid, bsData);
    QString geom_uuid= bsData->getGeomID();


    bool foundGeomID = m_database->hasGeometryUUID(geom_uuid, DFTBRefDatabase::Crystal);


    if (!foundGeomID)
    {
        QMessageBox qbox(this);
        qbox.setText("The reference geometry data of this band structure data is missing.");
        qbox.setStandardButtons(QMessageBox::Ok);
        qbox.setIcon(QMessageBox::Critical);
        qbox.exec();
    }

    while(1)
    {
        QString old_UUID = bsData->UUID();
        int ret = showEditor(bsData);
        if (ret)
        {
            QString errormsg;
            if (!bsData->checkData(errormsg))
            {
                QMessageBox qbox(this);
                qbox.setText(errormsg);
                qbox.setStandardButtons(QMessageBox::Ok);
                qbox.setIcon(QMessageBox::Critical);
                qbox.exec();
                continue;
            }

            bsData->resetUUID();
            bool succ = m_database->updateBandStructure(bsData, old_UUID);
            if (succ)
            {
                m_model->select();
            }

            break;
        }
        break;
    }

    delete bsData;
}

void BandstructureListView::duplicateItemInDatabase(const QString &uuid)
{
    BandstructureData* bsData = new BandstructureData();

    m_database->getBandStructure(uuid, bsData);

    bsData->setName(bsData->getName()+"_copy");


    QString geom_uuid= bsData->getGeomID();


    bool foundGeomID = m_database->hasGeometryUUID(geom_uuid, DFTBRefDatabase::Crystal);


    if (!foundGeomID)
    {
        QMessageBox qbox(this);
        qbox.setText("The reference geometry data of this band structure data is missing.");
        qbox.setStandardButtons(QMessageBox::Ok);
        qbox.setIcon(QMessageBox::Critical);
        qbox.exec();
    }

    while(1)
    {
        int ret = showEditor(bsData);
        if (ret)
        {
            QString errormsg;
            if (!bsData->checkData(errormsg))
            {
                QMessageBox qbox(this);
                qbox.setText(errormsg);
                qbox.setStandardButtons(QMessageBox::Ok);
                qbox.setIcon(QMessageBox::Critical);
                qbox.exec();
                continue;
            }

            bsData->resetUUID();
            bool succ = m_database->addBandStructure(bsData);
            if (succ)
            {
                m_model->select();
            }

            break;
        }
        break;
    }

    delete bsData;
}

void BandstructureListView::adjustTableView()
{
    m_tableView->hideColumn(0);
}

void BandstructureListView::initModels()
{
    m_model->setTable("BandstructureListView");
}

int BandstructureListView::propertyID()
{
    return 2;
}

int BandstructureListView::showEditor(BandstructureData *data)
{
    QDialog dia;
    dia.resize(800,600);
    QGridLayout *vb =new QGridLayout();


    QSqlTableModel *model = new QSqlTableModel(this, m_database->database());
    model->setTable("CrystalGeometry_IDSTR");
    model->select();
    BandstructureDataEditor* e = new BandstructureDataEditor(data, model);


    vb->addWidget(e,0,0,1,6);

    QPushButton *butOK = new QPushButton(tr("OK"));
    QPushButton *butCancel = new QPushButton(tr("Cancel"));


    connect(butOK, SIGNAL(clicked()), &dia, SLOT(accept()));
    connect(butCancel, SIGNAL(clicked()), &dia, SLOT(reject()));

    vb->addWidget(butOK, 1,4,1,1);
    vb->addWidget(butCancel, 1,5,1,1);

    dia.setLayout(vb);

    return dia.exec();
}
