#ifndef ELECTRONICDEFAULTCONFININGDEFINITIONDIALOG_H
#define ELECTRONICDEFAULTCONFININGDEFINITIONDIALOG_H

#include <QDialog>

QT_BEGIN_NAMESPACE
class QTableView;
QT_END_NAMESPACE

class DFTBRefDatabase;
class SqlRelationalTableModel;
class ElectronicDefaultConfiningDefinitionDialog : public QDialog
{
    Q_OBJECT
public:
    ElectronicDefaultConfiningDefinitionDialog(DFTBRefDatabase *database, QWidget *parent = 0);

signals:

public slots:
private slots:
    void addNewEntry();
    void removeEntry();
    void commitBut();
    void revert();
    bool commit();
private:
    void setupUI();
    QTableView* m_tableView = nullptr;
    DFTBRefDatabase* m_database = nullptr;
    SqlRelationalTableModel* m_model = nullptr;

    // QWidget interface
protected:
    void closeEvent(QCloseEvent *event);
};

#endif // ELECTRONICDEFAULTCONFININGDEFINITIONDIALOG_H
