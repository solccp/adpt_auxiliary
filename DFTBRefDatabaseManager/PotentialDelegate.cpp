#include "PotentialDelegate.h"
#include "PotentialWidget.h"

#include <QDebug>

PotentialDelegate::PotentialDelegate(QObject *parent) :
    QStyledItemDelegate(parent)
{
}

QWidget *PotentialDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    PotentialWidget *cb = new PotentialWidget(parent);
    return cb;
}

void PotentialDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    QString potential = index.data().toString();
    QStringList arr = potential.split("-", QString::SkipEmptyParts);
    PotentialWidget *cb = qobject_cast<PotentialWidget *>(editor);
    if(arr.size() == 2 && cb)
    {
        cb->setElement1(arr[0]);
        cb->setElement2(arr[1]);
    }
}

void PotentialDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    if(PotentialWidget *cb = qobject_cast<PotentialWidget *>(editor))
    {
        QString elem1 = cb->getElement1();
        QString elem2 = cb->getElement2();

        model->setData(index, QString("%1-%2").arg(elem1).arg(elem2));
    }
}

void PotentialDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    QRect rect = option.rect;
    QSize sizeHint = editor->sizeHint();

    if (rect.width()<sizeHint.width()) rect.setWidth(sizeHint.width());
    if (rect.height()<sizeHint.height()) rect.setHeight(sizeHint.height());

    editor->setGeometry(rect);
}

