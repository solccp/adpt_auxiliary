#ifndef FOLDERBASEIMPORTOR_H
#define FOLDERBASEIMPORTOR_H

#include <QMap>
#include <QSet>
#include <QDir>
#include <QSharedPointer>

class DFTBRefDatabase;
class Geometry;
class BandstructureData;
class MolecularFrequencyData;
class MolecularAtomizationEnergyData;
class ReactionEnergyData;

class FolderbaseObserver
{
public:
    virtual void setNewParseRange(int min, int max) const = 0;
    virtual void newText(const QString& text) const = 0;
    virtual void progressUpdated(int step) const = 0;
    virtual bool wasCanceled() const = 0;
    virtual ~FolderbaseObserver(){}
};



class FolderbaseImporter : public QObject
{
    Q_OBJECT
public:
    FolderbaseImporter(DFTBRefDatabase* database, QObject *parent = 0);
    ~FolderbaseImporter();
    QString path() const;
    void setPath(const QString &path);

    void setObserver(FolderbaseObserver* observer);

    QList<Geometry *> geometryData() const;
    QList<BandstructureData*> bandstructureData() const;
    QList<MolecularFrequencyData*> frequencyData() const;
    QList<MolecularAtomizationEnergyData*> atomizationEnergyData() const;
    QList<ReactionEnergyData*>  reactionEnergyData() const;

signals:
    void messageUpdated(const QString& msg);
public slots:
    void scanGeometry();
    bool scanBandstructure();
    bool scanMolecularFrequency();
    void scanMolecularAtomizationEnergy();
    void scanReactionEnergy();
private:
    bool parseGeometryData(const QDir& root, const QString& path, Geometry* geom);
//    bool parseGeometryLattice(const QDir& root, const QString& filename, Geometry* geom);
private:
    QSharedPointer<FolderbaseObserver> m_observer;
    DFTBRefDatabase *m_database;
    QString m_path;
    QList<Geometry*> m_geometryData;
    QList<BandstructureData*> m_bandstructureData;
    QList<MolecularFrequencyData*> m_frequencyData;
    QList<MolecularAtomizationEnergyData*> m_atomizationEnergyData;
    QList<ReactionEnergyData*> m_reactionEnergyData;
private:
    QMap<QString, QString> m_geom_name_uuid_crystal;
    QMap<QString, QString> m_geom_name_uuid_molecule;
    QSet<QString> m_geom_uuid;

//    QMap<QString, Geometry*> m_uuid_geom;
//    QMap<QString, Geometry*> m_uuid_geom;
//    QMap<QString, Geometry*> m_name_geom;

};

#endif // FOLDERBASEIMPORTEXPORTOR_H
