#ifndef POTENTIALWIDGET_H
#define POTENTIALWIDGET_H

#include <QString>

#include <QWidget>
QT_BEGIN_NAMESPACE
class QComboBox;
QT_END_NAMESPACE


class PotentialWidget : public QWidget
{
    Q_OBJECT
public:
    explicit PotentialWidget(QWidget *parent = 0);

signals:

public slots:
    QString getElement1();
    QString getElement2();
    void setElement1(const QString& elem);
    void setElement2(const QString& elem);
private slots:
    void elem1changed(const QString& elem1);

private:
    QComboBox *m_comboElem1;
    QComboBox *m_comboElem2;
    QStringList m_sortedElements;
};

#endif // POTENTIALWIDGET_H

