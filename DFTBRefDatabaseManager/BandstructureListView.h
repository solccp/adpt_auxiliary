#ifndef BANDSTRUCTURELISTVIEW_H
#define BANDSTRUCTURELISTVIEW_H

#include "DataListViewBase.h"

class BandstructureData;
class BandstructureListView : public DataListViewBase
{
    Q_OBJECT
public:
    BandstructureListView(DFTBRefDatabase* database, QWidget *parent = 0);

signals:

public slots:

    // DataListViewBase interface
protected:
    void deleteFromDatabase(const QStringList &uuids);
    void addToDatabase();
    void editItemInDatabase(const QString& uuid);
    void duplicateItemInDatabase(const QString& uuid);
    void adjustTableView();
    void initModels();
    int propertyID();
private:
    int showEditor(BandstructureData* data);

};

#endif // BANDSTRUCTURELISTVIEW_H
