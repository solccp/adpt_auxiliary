#ifndef REFERENCEDATAIMPORTOR_H
#define REFERENCEDATAIMPORTOR_H

#include <memory>
#include <QList>
#include <QMainWindow>
#include <QTableView>

#include <Variant/Variant.h>
#include <QSortFilterProxyModel>
#include <QThread>
#include <QProgressDialog>

#include <atomic>

class DFTBRefDatabase;

class GeometryModel;
class BandstructureDataModel;
class AtomizationEnergyModel;
class FrequencyModel;
class ReactionEnergyModel;




class ReferenceDataImportor;
class Worker : public QObject
{
    Q_OBJECT
public:
    Worker(QObject *parent = 0);
    void import_worker(ReferenceDataImportor* importer, const QString& filename, bool yaml_input);
public slots:
    void setCancelled();
signals:
    void finished();
    void failed();
    void canceled();
    void rangeUpdated(int, int);
    void progressUpdtae(int);
    void textUpdated(QString);
private:
    std::atomic_bool m_isCancelled;
    libvariant::Variant readSchema(const QString& filename);
};






#define DECLEAR_MODEL(modelclassname, model_suffix) \
    QTableView *m_tableview_##model_suffix; \
    modelclassname* m_model_##model_suffix; \
    QSortFilterProxyModel *m_proxymodel_##model_suffix;

#define INIT_MODEL(modelclassname, modelname, model_suffix) \
    m_model_##model_suffix = new modelclassname(this); \
    m_proxymodel_##model_suffix = new QSortFilterProxyModel(this); \
    m_tableview_##model_suffix = new QTableView(); \
    m_proxymodel_##model_suffix->setSourceModel(m_model_##model_suffix); \
    m_tableview_##model_suffix->setModel(m_proxymodel_##model_suffix); \
    m_tableview_##model_suffix->setSortingEnabled(true); \
    m_tableview_##model_suffix->setColumnWidth(0,30); \
    m_tableview_##model_suffix->setEditTriggers(QAbstractItemView::EditTrigger::DoubleClicked); \
    m_tableview_##model_suffix->setSelectionBehavior(QAbstractItemView::SelectRows); \
    m_tabWidget->addTab(m_tableview_##model_suffix, tr(modelname));





class ReferenceDataImportor : public QMainWindow
{
    Q_OBJECT
    friend class Worker;
public:
    explicit ReferenceDataImportor(DFTBRefDatabase* database, QWidget *parent = 0);
    void clear();
private slots:
    void import();
    void cancel();
    void loadFile();
private:
    void setupUI();
    void load_reference_file(const QString& filename, bool yaml_input);
private:
    DFTBRefDatabase* m_database;
    QTabWidget *m_tabWidget;

    DECLEAR_MODEL(GeometryModel, geometry)
    DECLEAR_MODEL(BandstructureDataModel, bandstructure)
    DECLEAR_MODEL(AtomizationEnergyModel, atomization_energy)
    DECLEAR_MODEL(FrequencyModel, frequency)
    DECLEAR_MODEL(ReactionEnergyModel, reaction_energy)
};

#endif // REFERENCEDATAIMPORTOR_H
