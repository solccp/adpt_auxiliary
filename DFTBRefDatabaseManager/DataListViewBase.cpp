#include "DataListViewBase.h"

#include <QSqlQueryModel>
#include <QSqlTableModel>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlError>

#include "dftbrefdatabase.h"
#include "PredefinedSetItemEditor.h"
#include "ElementSortFilterProxyModel.h"

DataListViewBase::DataListViewBase(DFTBRefDatabase *database, QWidget *parent) : QWidget(parent)
{
    m_database = database;
}

DataListViewBase::~DataListViewBase()
{
    delete m_model;
    m_model = nullptr;
}

QStringList DataListViewBase::selectedItems() const
{
    QStringList uuids;
    QModelIndexList selections = m_tableView->selectionModel()->selectedRows();
    for(int i=0; i< selections.size();++i)
    {
        const QModelIndex& index = selections.at(i);
        uuids.append(m_proxyModel->data(m_proxyModel->index(index.row(), 0)).toString());
    }
    return uuids;
}

void DataListViewBase::reload()
{
    if (m_model)
    {
        m_model->select();
    }
}

void DataListViewBase::checkEditable()
{
    if (!m_database->canWrite())
    {
        butAddEntry->setEnabled(false);
        butRemoveEntry->setEnabled(false);
    }
    else
    {
        butAddEntry->setEnabled(true);
        butRemoveEntry->setEnabled(true);
    }
}

void DataListViewBase::editEntry(const QModelIndex &index)
{
    const QAbstractItemModel *mymodel = index.model();
    QVariant uuid = mymodel->data(mymodel->index(index.row(),0), Qt::EditRole);
    editItemInDatabase(uuid.toString());
}

void DataListViewBase::addEntry()
{
    addToDatabase();
}

void DataListViewBase::removeEntry()
{
    QStringList uuids = selectedItems();
    if (!uuids.empty())
    {
        QMessageBox qbox(this);
        qbox.setText("Delete those records from the database?");
        qbox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        qbox.setIcon(QMessageBox::Question);
        int ret = qbox.exec();
        if (ret == QMessageBox::Yes)
        {
            deleteFromDatabase(uuids);
            m_model->select();
        }
    }
}

void DataListViewBase::selectAllItems()
{
    m_tableView->selectAll();
}

void DataListViewBase::selectNoItems()
{
    m_tableView->clearSelection();
}

void DataListViewBase::BuildSelectionMenu(QMenu *setMenu)
{
    QSqlQueryModel query;
    query.setQuery("select name, set_id, parentSet, id from testsuite_set_id order by id", m_database->database());


    QList<QAction*> topActions;

    QMap<QString, QAction*> actions;
    QMap<QString, QStringList> childIds;

    QList<QAction*> finalActions;

    for(int i=0; i<query.rowCount();++i)
    {
        QAction* menu = new QAction(setMenu);

        menu->setText(query.record(i).value(0).toString());
        connect(menu, SIGNAL(triggered()), this, SLOT(addSelectionToSet()));
        menu->setProperty("SET_ID", query.record(i).value(1));
        QString sid = query.record(i).value(1).toString();
        QString pid = query.record(i).value(2).toString();
        actions.insert(sid,menu);

        if(pid == QUuid().toString())
        {
            topActions.append(menu);
        }
        else
        {
            childIds[pid].append(sid);
        }
    }

    QAction *menuNew = new QAction("New Set...", setMenu);
    connect(menuNew, SIGNAL(triggered()), this, SLOT(addSelectionToSet()));
    menuNew->setProperty("SET_ID", QUuid().toString());

    finalActions.append(menuNew);
    finalActions.append(setMenu->addSeparator());

    QStack<QAction*> uuids;
    for(int i=0; i<topActions.size();++i)
    {
        QAction* menu = topActions[i];
        menu->setProperty("LEVEL", 0);
        uuids.push(menu);
    }
    while(!uuids.isEmpty())
    {
        QAction* menu = uuids.pop();
        int level = menu->property("LEVEL").toInt();
        QString indent = QString().fill('-', level*2);
        menu->setText(indent + menu->text());

        finalActions.append(menu);

        QString menuID = menu->property("SET_ID").toString();
        QStringList children = childIds[menuID];
        for(int i=0; i<children.size();++i)
        {
            QAction *cmenu = actions[children[i]];
            cmenu->setProperty("LEVEL", level+1);
            uuids.push(cmenu);
        }
    }

    setMenu->addActions(finalActions);
}

void DataListViewBase::onTableViewContextMenu(const QPoint &pos)
{
    Q_UNUSED(pos);
    QMenu *menu = new QMenu();
    QVariant v = qVariantFromValue((void *) sender());
    menu->setProperty("OBJ", v);

    QMenu *setMenu = new QMenu("Add selection to ...");
    QMenu *testSetMenu = new QMenu("TestSuite sets", setMenu);

    menu->addMenu(setMenu);

    setMenu->addMenu(testSetMenu);

    extraContextMenu(setMenu);
    BuildSelectionMenu(testSetMenu);


    menu->addSeparator();
    menu->addAction("Duplicate", this, SLOT(duplicateSelected()));
    menu->addSeparator();
    menu->addAction("Select all", this, SLOT(selectAllItems()));
    menu->addAction("Select none", this, SLOT(selectNoItems()));
    menu->exec(QCursor::pos());

    delete menu;
}

void DataListViewBase::addSelectionToSet()
{
    QAction* action = qobject_cast<QAction*>(sender());
    if (action)
    {
        m_database->transaction();
        bool setOK = false;
        QString set_id = action->property("SET_ID").toString();
        if (set_id == QUuid().toString())
        {
            set_id = QUuid::createUuid().toString();
            PredefinedSetItemEditor* editor = new PredefinedSetItemEditor(this);
            int ret = editor->exec();
            if (ret == QDialog::Accepted)
            {
                QSqlQuery query(m_database->database());
                query.prepare("INSERT INTO testsuite_set_id (set_id, parentSet, name, description) "
                              "VALUES (:set_id, :parentSet, :name, :description)");
                query.bindValue(":set_id", set_id);
                query.bindValue(":name", editor->name());
                query.bindValue(":description", editor->desc());
                query.bindValue(":parentSet", QUuid().toString());

                if (query.exec())
                {
                    setOK = true;
                }
            }
            delete editor;
        }
        else
        {
            setOK = true;
        }

        if (!setOK)
        {
            m_database->rollback();
            return;
        }

        QSqlTableModel *model = new QSqlTableModel(this, m_database->database());
        model->setEditStrategy(QSqlTableModel::OnManualSubmit);
        model->setTable("testsuite_default_selection");
        model->select();

        QModelIndexList selections_molecule = m_tableView->selectionModel()->selectedRows();
        for(int i=0; i<selections_molecule.size();++i)
        {
            const QModelIndex& index = m_proxyModel->mapToSource(selections_molecule.at(i));

            QString uuid = m_model->data(m_model->index(index.row(),0)).toString();
            QSqlRecord record(model->record());
            record.setValue("set_id", set_id);
            record.setValue("uuid", uuid);
            record.setValue("property_id", propertyID());
            record.setValue("weight", 1.0);

            record.setValue("name_uuid", uuid);
            record.setValue("elements_uuid", uuid);
            model->insertRecord(-1, record);
        }
        bool succ = model->submitAll();

        if (succ)
        {
            m_database->commit();
        }
        else
        {
            qDebug() << model->lastError().text();
            m_database->rollback();
        }
    }
}

void DataListViewBase::resetModels()
{
    if (m_model)
    {
        m_model->deleteLater();
    }
    m_model = new QSqlTableModel(this, m_database->database());

    m_model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    initModels();

    reload();

    if (m_proxyModel)
    {
        m_proxyModel->deleteLater();
    }
    m_proxyModel = new ElementSortFilterProxyModel(this);
    m_proxyModel->setElementFilterMode(ElementSortFilterProxyModel::Any);

    int elementsIndex = m_model->fieldIndex("elements");
    if (elementsIndex != -1)
    {
        m_proxyModel->setElementFilterKeyColumn(elementsIndex);
    }
    m_proxyModel->setSourceModel(m_model);
    m_tableView->setModel(m_proxyModel);
    setupFilter();

    adjustTableView();
}

void DataListViewBase::duplicateSelected()
{
    QObject* sender_obj = sender();
    QTableView *view = (QTableView*)sender_obj->parent()->property("OBJ").value<void *>();
    if (view)
    {
        auto  list = view->selectionModel()->selectedRows(0);
        QList<QString> uuids;
        for(int i=0; i<list.size(); ++i)
        {
            uuids.append(list.at(i).data().toString());
        }
        if (uuids.size() > 1)
        {

        }
        else
        {
            duplicateItemInDatabase(uuids.first());
        }
    }
//            Geometry* geom = new Geometry();
//            m_database->getGeometry(uuids.first(), geom);
//            geom->resetUUID();
//            geom->setName(geom->getName()+"_copy");
//            while(1)
//            {
//                int ret = showEditor(geom);
//                if (ret)
//                {
//                    if (geom->m_name.isEmpty() || geom->m_method.isEmpty())
//                    {
//                        QMessageBox qbox(this);
//                        qbox.setText("Name and Method cannot be empty.");
//                        qbox.setStandardButtons(QMessageBox::Ok);
//                        qbox.setIcon(QMessageBox::Critical);
//                        qbox.exec();
//                        continue;
//                    }

//                    bool succ = m_database->addGeometry(geom);
//                    if (succ)
//                    {
//                        if (geom->PBC())
//                        {
//                            model_crystal->select();
//                        }
//                        else
//                        {
//                            model_molecule->select();
//                        }
//                    }
//                    else
//                    {
//                        QMessageBox qbox(this);
//                        qbox.setText(m_database->getLastError());
//                        qbox.setStandardButtons(QMessageBox::Ok);
//                        qbox.setIcon(QMessageBox::Critical);
//                        qbox.exec();
//                        continue;
//                    }

//                }
//                break;
//            }
//            delete geom;
//        }
//    }
}

void DataListViewBase::setupFilter()
{
    m_filter_columnIndex->clear();
    for(int i=0; i<m_model->columnCount(); ++i)
    {
        if (m_tableView->isColumnHidden(i))
        {
            continue;
        }
        m_filter_columnIndex->addItem(m_proxyModel->headerData(i, Qt::Horizontal).toString(), i);
    }
    m_filter_content->clear();
}

void DataListViewBase::filterChanged()
{
    QString filterText = m_filter_content->text();
    bool ok;
    int filterColumn = m_filter_columnIndex->currentData().toInt(&ok);
    if (!ok)
    {
        return;
    }
    QString filterColumnString = m_filter_columnIndex->currentText().toLower();
    if (filterColumnString == "elements")
    {
        QStringList inputElements = filterText.split(QRegularExpression("[ ,]"), QString::SkipEmptyParts);
        if (inputElements.isEmpty())
        {
            m_proxyModel->clearElementFilter();
        }
        else
        {
            m_proxyModel->setElementFilter(inputElements);
        }

    }
    else
    {
        m_proxyModel->clearElementFilter();
        m_proxyModel->setFilterKeyColumn(filterColumn);
        m_proxyModel->setFilterWildcard(filterText);
    }
}

void DataListViewBase::adjustTableView()
{
}

void DataListViewBase::setupUI()
{
    QGridLayout *layout = new QGridLayout();
    this->setLayout(layout);

    butAddEntry = new QPushButton(tr("Add item..."));
    layout->addWidget(butAddEntry, 0,0,1,1 );
    connect(butAddEntry, SIGNAL(clicked()), this, SLOT(addEntry()));

    butRemoveEntry = new QPushButton(tr("Delete item..."));
    layout->addWidget(butRemoveEntry, 0,1,1,1 );
    connect(butRemoveEntry, SIGNAL(clicked()), this, SLOT(removeEntry()));

    checkEditable();

    m_tableView = new QTableView(this);
    m_tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_tableView->setSortingEnabled(true);
    m_tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    connect(m_tableView, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(editEntry(QModelIndex)));


    layout->addWidget(m_tableView,2,0,1,6);
    m_tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    m_tableView->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(m_tableView, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(onTableViewContextMenu(QPoint)));

    m_filter_columnIndex = new QComboBox();
    m_filter_columnIndex->setEditable(false);
    connect(m_filter_columnIndex, SIGNAL(currentIndexChanged(int)), this, SLOT(filterChanged()));
    m_filter_content = new QLineEdit();
    connect(m_filter_content, SIGNAL(textChanged(QString)), this, SLOT(filterChanged()));


    resetModels();


    connect(m_database, SIGNAL(databaseConnectionChanged()), this, SLOT(resetModels()));

    QHBoxLayout *hbox = new QHBoxLayout;
    hbox->addWidget(new QLabel(tr("Filter:")));
    hbox->addWidget(m_filter_columnIndex);
    hbox->addWidget(m_filter_content);
    layout->addLayout(hbox, 1,0,1,6);


    setupFilter();

}
