#ifndef YESNOCELLITEMDELEGATE_H
#define YESNOCELLITEMDELEGATE_H

#include <QStyledItemDelegate>

class YesNoCellItemDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    explicit YesNoCellItemDelegate(QObject *parent = 0);
signals:

public slots:
public:
    QString displayText(const QVariant &value, const QLocale &locale) const;
};

#endif // YESNOCELLITEMDELEGATE_H
