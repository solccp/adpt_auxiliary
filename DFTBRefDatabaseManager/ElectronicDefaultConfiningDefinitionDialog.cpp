#include "ElectronicDefaultConfiningDefinitionDialog.h"
#include "dftbrefdatabase.h"

#include <QtWidgets>
#include <QSqlTableModel>

#include "doublenumbercelldelegate.h"
#include "sqlrelationaltablemodel.h"
#include "atomsymbolcombodelegate.h"

#include <QMessageBox>
#include <QSqlError>

#include <QStyledItemDelegate>
#include <QComboBox>

class ConfiningTypeComboDelegate : public QStyledItemDelegate
{
public:
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
    {
        Q_UNUSED(option);
        Q_UNUSED(index);
        QComboBox *cb = new QComboBox(parent);
        cb->addItems(QStringList() << "density" << "orbital");
        cb->setEditable(false);
        return cb;
    }
    void setEditorData(QWidget *editor, const QModelIndex &index) const
    {
        QComboBox *cb = qobject_cast<QComboBox*>(editor);
        if (cb)
        {
            cb->setCurrentText(index.data().toString());
        }
    }
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
    {
        if (!index.isValid())
            return;

        QComboBox *cb = qobject_cast<QComboBox*>(editor);
        if (cb)
        {
            model->setData(index, cb->currentText());
        }
    }
    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
    {
        Q_UNUSED(index);
        QRect rect = option.rect;
        QSize sizeHint = editor->sizeHint();

        if (rect.width()<sizeHint.width()) rect.setWidth(sizeHint.width());
        if (rect.height()<sizeHint.height()) rect.setHeight(sizeHint.height());

        editor->setGeometry(rect);
    }
};




ElectronicDefaultConfiningDefinitionDialog::ElectronicDefaultConfiningDefinitionDialog(DFTBRefDatabase *database, QWidget *parent) :
    QDialog(parent)
{
    m_database = database;
    setupUI();
}

void ElectronicDefaultConfiningDefinitionDialog::addNewEntry()
{
    int row = m_model->rowCount();
    m_model->insertRow(row);
    m_tableView->scrollToBottom();
    m_tableView->selectRow(row);
    m_tableView->setFocus();
}

void ElectronicDefaultConfiningDefinitionDialog::removeEntry()
{
    QModelIndexList list = m_tableView->selectionModel()->selectedRows();
    if (!list.empty())
    {
        for (int i=list.size()-1; i>=0; --i)
        {
            int row = list.at(i).row();
            m_tableView->model()->removeRows(row,1);
        }
    }
}

void ElectronicDefaultConfiningDefinitionDialog::commitBut()
{
    if (m_model->isDirty() && commit())
    {
        QMessageBox qbox2(this);
        qbox2.setText("Data written into database.");
        qbox2.setStandardButtons(QMessageBox::Ok);
        qbox2.setIcon(QMessageBox::Information);
        qbox2.exec();
    }
}

void ElectronicDefaultConfiningDefinitionDialog::revert()
{
     m_model->revertAll();
}

bool ElectronicDefaultConfiningDefinitionDialog::commit()
{
    m_model->database().transaction();
    if(m_model->submitAll())
    {
        m_model->database().commit();
    }
    else
    {
        m_model->database().rollback();
        QMessageBox qbox2(this);
        qbox2.setText("Failed to write data into database:" + m_model->lastError().text());
        qbox2.setStandardButtons(QMessageBox::Ok);
        qbox2.setIcon(QMessageBox::Critical);
        qbox2.exec();
        return false;
    }
    return true;
}

void ElectronicDefaultConfiningDefinitionDialog::setupUI()
{
    m_model = new SqlRelationalTableModel(m_database->database(), this);
    m_model->setTable("electronic_fitting_default_confining");
    m_model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    m_model->select();
    m_tableView = new QTableView(this);
    m_tableView->setSelectionBehavior(QAbstractItemView::SelectRows);

    m_tableView->setModel(m_model);
    m_tableView->setAlternatingRowColors(true);


    m_tableView->setItemDelegateForColumn(0, new AtomSymbolComboDelegate());
    m_tableView->setItemDelegateForColumn(1, new ConfiningTypeComboDelegate());
    m_tableView->setItemDelegateForColumn(2, new DoubleNumberCellDelegate());
    m_tableView->setItemDelegateForColumn(3, new DoubleNumberCellDelegate());
    m_tableView->setItemDelegateForColumn(4, new DoubleNumberCellDelegate());

    this->resize(600,400);
    m_tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
//    m_tableView->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Fixed);
//    m_tableView->setColumnWidth(0,130);

    QVBoxLayout *layout = new QVBoxLayout(this);
    this->setLayout(layout);


    QHBoxLayout *hbox = new QHBoxLayout();
    QPushButton* butAdd = new QPushButton(tr("Add"));
    connect(butAdd, SIGNAL(clicked()), this, SLOT(addNewEntry()));
    hbox->addWidget(butAdd);
    QPushButton* butDel = new QPushButton(tr("Remove"));
    connect(butDel, SIGNAL(clicked()), this, SLOT(removeEntry()));
    hbox->addWidget(butDel);

    QPushButton* butCommit = new QPushButton(tr("Commit"));
    connect(butCommit, SIGNAL(clicked()), this, SLOT(commitBut()));
    hbox->addWidget(butCommit);

    QPushButton* butRevert = new QPushButton(tr("Revert"));
    connect(butRevert, SIGNAL(clicked()), this, SLOT(revert()));
    hbox->addWidget(butRevert);
    hbox->addStretch(5);

    layout->addLayout(hbox);
    layout->addWidget(m_tableView);


    QPushButton* butClose = new QPushButton(tr("Close"));
    butClose->setDefault(true);
    connect(butClose, SIGNAL(clicked()), this, SLOT(close()));


    hbox = new QHBoxLayout();

    hbox->addStretch(5);
    hbox->addWidget(butClose);
    layout->addLayout(hbox);

    if (!m_database->canWrite())
    {
        butAdd->setEnabled(false);
        butRevert->setEnabled(false);
        butCommit->setEnabled(false);
        butDel->setEnabled(false);
        m_tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    }
}

void ElectronicDefaultConfiningDefinitionDialog::closeEvent(QCloseEvent *event)
{
    if (m_model->isDirty())
    {
        QMessageBox qbox(this);
        qbox.setText("The changes have not been updated in the database.\n"
                     "Do you want to update them?");
        qbox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        qbox.setIcon(QMessageBox::Question);
        int ret = qbox.exec();
        if (ret == QMessageBox::Yes)
        {
            bool succ = commit();
            if (!succ)
            {
                event->ignore();
                return;
            }
        }
        else
        {
            event->accept();
        }
    }
}
