#ifndef INTCELLDELEGATE_H
#define INTCELLDELEGATE_H

#include <QStyledItemDelegate>

class IntCellDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    explicit IntCellDelegate(QObject *parent = 0);

public:
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;

    // QStyledItemDelegate interface
protected:
    void initStyleOption(QStyleOptionViewItem *option, const QModelIndex &index) const;

    // QAbstractItemDelegate interface
};

#endif // INTCELLDELEGATE_H
