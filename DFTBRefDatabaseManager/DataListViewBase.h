#ifndef DATALISTVIEWBASE_H
#define DATALISTVIEWBASE_H

#include <QWidget>
#include <QtWidgets>

QT_BEGIN_NAMESPACE
class QSqlTableModel;
QT_END_NAMESPACE

class DFTBRefDatabase;
class ElementSortFilterProxyModel;
class DataListViewBase : public QWidget
{
    Q_OBJECT
public:
    DataListViewBase(DFTBRefDatabase* database, QWidget *parent = 0);
    virtual ~DataListViewBase();

public slots:
    QStringList selectedItems() const;
    void reload();
    void checkEditable();
    void resetModels();
private slots:

    void duplicateSelected();

    void editEntry(const QModelIndex& index);
    void addEntry();
    void removeEntry();

    void selectAllItems();
    void selectNoItems();


    void onTableViewContextMenu(const QPoint& pos);
    void addSelectionToSet();



    void setupFilter();
    void filterChanged();
protected:
    virtual void extraContextMenu(QMenu*) {}
    virtual void deleteFromDatabase(const QStringList& uuids) = 0;
    virtual void addToDatabase() = 0;
    virtual void editItemInDatabase(const QString& uuid) = 0;
    virtual void duplicateItemInDatabase(const QString& uuid) = 0;
    virtual void adjustTableView();
    virtual void initModels() = 0;
    virtual int propertyID() = 0;
protected:
    void setupUI();
    void BuildSelectionMenu(QMenu *setMenu);
protected:
    DFTBRefDatabase *m_database = nullptr;
    QTableView* m_tableView = nullptr;
    QSqlTableModel *m_model = nullptr;

    ElementSortFilterProxyModel *m_proxyModel = nullptr;
    QComboBox* m_filter_columnIndex = nullptr;
    QLineEdit* m_filter_content = nullptr;

    QPushButton* butAddEntry ;
    QPushButton* butRemoveEntry;
};

#endif // DATALISTVIEWBASE_H
