#include "GeometryListView.h"

#include <QStringList>
#include "clustergeometryeditor.h"
#include "crystalgeometryeditor.h"
#include "geometry.h"
#include "folderbaseimportor.h"
#include <QDialog>
#include <QMessageBox>
#include <QHeaderView>
#include <QTableWidget>
#include <QLabel>
#include <QProgressDialog>
#include <QCheckBox>
#include <QComboBox>
#include <QLineEdit>
#include <QMenu>
#include <QSqlRecord>

#include <QSqlTableModel>
#include <QSqlError>
#include <QSqlQuery>
#include <QUuid>
#include <QFileDialog>

#include "dftbrefdatabase.h"
#include "PredefinedSetItemEditor.h"
#include "YesNoCellItemDelegate.h"

#include "ElementSortFilterProxyModel.h"

#include <QStack>

GeometryListView::GeometryListView(DFTBRefDatabase *database, QWidget *parent) :
    QWidget(parent)
{
    m_database = database;
    setupUI();
}

GeometryListView::~GeometryListView()
{
    delete model_molecule;
    delete model_crystal;
}


void GeometryListView::addNewMolecularGeometry()
{
    Geometry* geom = new Geometry();
    while(1)
    {
        int ret = showEditor(geom);
        if (ret)
        {
            if (geom->getName().isEmpty() || geom->getMethod().isEmpty())
            {
                QMessageBox qbox(this);
                qbox.setText("Name and Method cannot be empty.");
                qbox.setStandardButtons(QMessageBox::Ok);
                qbox.setIcon(QMessageBox::Critical);
                qbox.exec();
                continue;
            }
            geom->setUUID(geom->computeUUID());
            bool succ = m_database->addGeometry(geom);
            if (succ)
            {
                if (geom->PBC())
                {
                    model_crystal->select();
                }
                else
                {
                    model_molecule->select();
                }
            }

            break;
        }
        break;
    }
    delete geom;
}
void GeometryListView::addNewCrystalGeometry()
{
    Geometry* geom = new Geometry();
    geom->setPBC(true);
    while(1)
    {
        int ret = showEditor(geom);
        if (ret)
        {
            if (geom->getName().isEmpty() || geom->getMethod().isEmpty())
            {
                QMessageBox qbox(this);
                qbox.setText("Name and Method cannot be empty.");
                qbox.setStandardButtons(QMessageBox::Ok);
                qbox.setIcon(QMessageBox::Critical);
                qbox.exec();
                continue;
            }

            geom->setUUID(geom->computeUUID());
            bool succ = m_database->addGeometry(geom);
            if (succ)
            {
                if (geom->PBC())
                {
                    model_crystal->select();
                }
                else
                {
                    model_molecule->select();
                }
            }

            break;
        }
        break;
    }
    delete geom;
}

void GeometryListView::updateGeometry(Geometry *geom)
{
    if (!m_database->canWrite())
    {
        return;
    }
    while(1)
    {
        QString oldUUID = geom->UUID();
        int ret = showEditor(geom);
        if (ret)
        {
            if (geom->getName().isEmpty() || geom->getMethod().isEmpty())
            {
                QMessageBox qbox(this);
                qbox.setText("Name and Method cannot be empty.");
                qbox.setStandardButtons(QMessageBox::Ok);
                qbox.setIcon(QMessageBox::Critical);
                qbox.exec();
                continue;
            }

            bool succ = m_database->updateGeometry(geom, oldUUID);
            if (succ)
            {
                if (geom->PBC())
                {
                    emit crystalGeometryNameUpdated(geom->UUID());
                    model_crystal->select();
                }
                else
                {
                    emit molecularGeometryNameUpdated(geom->UUID());
                    model_molecule->select();
//                    m_tableMolecule->setModel(model);
                }
            }
            break;
        }
        break;
    }
}

void GeometryListView::deleteGeometry()
{
    QStringList uuids = selectedItems();

    if (!uuids.empty())
    {
        QMessageBox qbox(this);
        qbox.setText("Delete those records in geometry?\n"
                     "Warning: if the geometry data is refered to other properties except reaction energy,\n"
                     "those corresponding property data will also be removed.");
        qbox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        qbox.setIcon(QMessageBox::Question);
        int ret = qbox.exec();
        if (ret == QMessageBox::Yes)
        {          
            if (m_database->removeGeometries(uuids))
            {
                model_molecule->select();
                model_crystal->select();
            }
            else
            {
                if (m_database->lastErrorNumber() == 1451)
                {
                    QMessageBox qbox(this);
                    qbox.setText("Some geometry data are refered in the reaction definitions.\n"
                                 "You must remove those reaction energy data before removing those geometry data.");
                    qbox.setStandardButtons(QMessageBox::Ok);
                    qbox.setIcon(QMessageBox::Critical);
                    qbox.exec();
                }
            }
        }
    }
}

void GeometryListView::duplicateSelected()
{
    QObject* sender_obj = sender();
    QTableView *view = (QTableView*)sender_obj->parent()->property("OBJ").value<void *>();
    if (view)
    {
        auto  list = view->selectionModel()->selectedRows(0);
        QList<QString> uuids;
        for(int i=0; i<list.size(); ++i)
        {
            uuids.append(list.at(i).data().toString());
        }
        if (uuids.size() > 1)
        {

        }
        else
        {
            Geometry* geom = new Geometry();
            m_database->getGeometry(uuids.first(), geom);

            geom->setName(geom->getName()+"_copy");

            while(1)
            {
                int ret = showEditor(geom);
                if (ret)
                {
                    if (geom->getName().isEmpty() || geom->getMethod().isEmpty())
                    {
                        QMessageBox qbox(this);
                        qbox.setText("Name and Method cannot be empty.");
                        qbox.setStandardButtons(QMessageBox::Ok);
                        qbox.setIcon(QMessageBox::Critical);
                        qbox.exec();
                        continue;
                    }

                    geom->setUUID(geom->computeUUID());
                    bool succ = m_database->addGeometry(geom);
                    if (succ)
                    {
                        if (geom->PBC())
                        {
                            model_crystal->select();
                        }
                        else
                        {
                            model_molecule->select();
                        }
                    }
                    else
                    {
                        QMessageBox qbox(this);
                        qbox.setText(m_database->getLastError());
                        qbox.setStandardButtons(QMessageBox::Ok);
                        qbox.setIcon(QMessageBox::Critical);
                        qbox.exec();
                        continue;
                    }

                }
                break;
            }
            delete geom;
        }
    }
}

int GeometryListView::showEditor(Geometry *geom)
{
    QDialog dia;
    dia.resize(800,600);
    QGridLayout *vb =new QGridLayout();

    if (geom->PBC())
    {
        CrystalGeometryEditor* e = new CrystalGeometryEditor(geom);
        vb->addWidget(e,0,0,1,6);
    }
    else
    {
        ClusterGeometryEditor* e = new ClusterGeometryEditor(geom);
        vb->addWidget(e,0,0,1,6);
    }


    QPushButton *butOK = new QPushButton(tr("OK"));
    QPushButton *butCancel = new QPushButton(tr("Cancel"));


    connect(butOK, SIGNAL(clicked()), &dia, SLOT(accept()));
    connect(butCancel, SIGNAL(clicked()), &dia, SLOT(reject()));

    vb->addWidget(butOK, 1,4,1,1);
    vb->addWidget(butCancel, 1,5,1,1);

    dia.setLayout(vb);

    return dia.exec();
}

QStringList GeometryListView::selectedItems()
{
    QStringList uuids;

    if (m_tabWidget->currentWidget() == m_tableMolecule)
    {
        QModelIndexList selections_molecule = m_tableMolecule->selectionModel()->selectedRows();
        for(int i=0; i< selections_molecule.size();++i)
        {
            const QModelIndex& index = selections_molecule.at(i);
            int realindex = m_proxyModelMolecule->mapToSource(index).row();
            uuids.append(model_molecule->data(model_molecule->index(realindex,0)).toString());
        }
    }
    else if (m_tabWidget->currentWidget() == m_tableCrystal)
    {
        QModelIndexList selections_crystal = m_tableCrystal->selectionModel()->selectedRows();
        for(int i=0; i< selections_crystal.size();++i)
        {
            const QModelIndex& index = selections_crystal.at(i);
            int realindex = m_proxyModelCrystal->mapToSource(index).row();
            uuids.append(model_crystal->data(model_crystal->index(realindex,0)).toString());
        }
    }

    return uuids;
}

void GeometryListView::reload()
{
    model_molecule->select();
    model_crystal->select();
}

void GeometryListView::checkEditable()
{
    if (!m_database->canWrite())
    {
        butAddMolecularGeometry->setEnabled(false);
        butDelGeometry->setEnabled(false);
    }
    else
    {
        butAddMolecularGeometry->setEnabled(true);
        butDelGeometry->setEnabled(true);
    }
}

void GeometryListView::editItem(const QModelIndex &index)
{
    if (index.isValid())
    {
        Geometry* geom = new Geometry();

        const QAbstractItemModel *model = index.model();
        QVariant data = model->data(model->index(index.row(),0), Qt::EditRole);

        bool succ = m_database->getGeometry(data.toString(), geom);
        if (succ)
        {
            updateGeometry(geom);
        }
        delete geom;
    }
}

void GeometryListView::setupUI()
{
    m_gridlayout = new QGridLayout(this);

    this->setLayout(m_gridlayout);


    //buttons
    butAddMolecularGeometry = new QPushButton(tr("Add Molecular Geometry"));
    connect(butAddMolecularGeometry, SIGNAL(clicked()), this, SLOT(addNewMolecularGeometry()));
    m_gridlayout->addWidget(butAddMolecularGeometry, 0,0,1,1);
    butAddMolecularGeometry = new QPushButton(tr("Add Crystal Geometry"));
    connect(butAddMolecularGeometry, SIGNAL(clicked()), this, SLOT(addNewCrystalGeometry()));
    m_gridlayout->addWidget(butAddMolecularGeometry, 0,1,1,1);

    butDelGeometry = new QPushButton(tr("Delete Geometry"));
    connect(butDelGeometry, SIGNAL(clicked()), this, SLOT(deleteGeometry()));
    m_gridlayout->addWidget(butDelGeometry, 0,2,1,1);

    m_filter_columnIndex = new QComboBox;
    m_filter_content = new QLineEdit;


    m_gridlayout->addWidget(new QLabel(tr("Filter:")), 0,3,1,1);
    m_gridlayout->addWidget(m_filter_columnIndex, 0,4,1,1);
    m_gridlayout->addWidget(m_filter_content, 0,5,1,1);


    checkEditable();


    m_tabWidget = new QTabWidget(this);
    m_gridlayout->addWidget(m_tabWidget, 1,0,4,6);




//    //cluster geometry

    //TableView
    m_tableMolecule = new QTableView(this);
    m_tableMolecule->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_tableMolecule->setSortingEnabled(true);
    m_tableMolecule->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    m_tableMolecule->setEditTriggers(QAbstractItemView::NoEditTriggers);
    connect(m_tableMolecule, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(editItem(QModelIndex)));




    m_tabWidget->addTab(m_tableMolecule, "Molecule");

    //crystal geometry
    m_tableCrystal = new QTableView(this);
    m_tableCrystal->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_tableCrystal->setSortingEnabled(true);
    m_tableCrystal->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    m_tableCrystal->setEditTriggers(QAbstractItemView::NoEditTriggers);
    connect(m_tableCrystal, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(editItem(QModelIndex)));
    m_tabWidget->addTab(m_tableCrystal, "Crystal");

    m_tableMolecule->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(m_tableMolecule, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(onTableViewContextMenu(QPoint)));
    m_tableCrystal->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(m_tableCrystal, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(onTableViewContextMenu(QPoint)));


    connect(m_database, SIGNAL(databaseConnectionChanged()), this, SLOT(resetModels()));

    resetModels();

    connect(m_tabWidget, SIGNAL(currentChanged(int)), this, SLOT(tabChanged(int)));
    m_tabWidget->setCurrentIndex(0);

    connect(m_filter_content, SIGNAL(textChanged(QString)), this, SLOT(filterChanged()));
    connect(m_filter_columnIndex, SIGNAL(currentIndexChanged(int)), this, SLOT(filterChanged()));

}


void GeometryListView::selectAllItems()
{
    QObject* sender_obj = sender();
    QTableView *view = (QTableView*)sender_obj->parent()->property("OBJ").value<void *>();
    if (view)
    {
        view->selectAll();
    }
}

void GeometryListView::selectNoItems()
{
    QObject* sender_obj = sender();
    QTableView *view = (QTableView*)sender_obj->parent()->property("OBJ").value<void *>();
    if (view)
    {
        view->clearSelection();
    }
}

void GeometryListView::selectItemToggle()
{
    QObject* sender_obj = sender();
    QTableView *view = (QTableView*)sender_obj->parent()->property("OBJ").value<void *>();
    if (view)
    {
        QItemSelection selection = view->selectionModel()->selection();
        QItemSelectionModel *seletionModel = view->selectionModel();
        seletionModel->select(selection, QItemSelectionModel::Toggle);
    }
}

void GeometryListView::addItemsToErepfitForceEquations()
{
    m_database->transaction();

    QSqlTableModel *model = new QSqlTableModel(this, m_database->database());
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    model->setTable("erepfit_default_input_force_equation");
    model->select();


    QModelIndexList selections_molecule = m_tableMolecule->selectionModel()->selectedRows();

    bool hasNonStationaryPoint = false;

    for(int i=0; i<selections_molecule.size();++i)
    {
        const QModelIndex& index = m_proxyModelMolecule->mapToSource(selections_molecule.at(i));

        QSqlRecord record(model->record());

        int index_stat = model_molecule->fieldIndex("Stationary");
        if (model_molecule->data(model_molecule->index(index.row(), index_stat)).toInt() == 0)
        {
            hasNonStationaryPoint = true;
            continue;
        }


        QString uuid = model_molecule->data(model_molecule->index(index.row(),0)).toString();
        record.setValue("uuid", uuid);
        record.setValue("weight", 1.0);


        model->insertRecord(-1, record);
    }
    bool succ = model->submitAll();

    if (succ)
    {
        m_database->commit();

        if (hasNonStationaryPoint)
        {
            QMessageBox qbox(this);
            qbox.setText("Some selected entries were non-stationary data, which cannot be added to the Erepfit Force Equations. Skipped.");
            qbox.setStandardButtons(QMessageBox::Ok);
            qbox.setIcon(QMessageBox::Information);
            qbox.exec();
        }

    }
    else
    {
        qDebug() << model->lastError().text();
        m_database->rollback();
    }

}

void GeometryListView::BuildSelectionMenu(QMenu *setMenu)
{
    QSqlQueryModel query;
    query.setQuery("select name, set_id, parentSet from testsuite_set_id", m_database->database());


    QList<QAction*> topActions;

    QMap<QString, QAction*> actions;
    QMap<QString, QStringList> childIds;

    QList<QAction*> finalActions;

    for(int i=0; i<query.rowCount();++i)
    {
        QAction* menu = new QAction(setMenu);

        menu->setText(query.record(i).value(0).toString());
        connect(menu, SIGNAL(triggered()), this, SLOT(addToDefaultFittingSet()));
        menu->setProperty("SET_ID", query.record(i).value(1));
        QString sid = query.record(i).value(1).toString();
        QString pid = query.record(i).value(2).toString();
        actions.insert(sid,menu);

        if(pid == QUuid().toString())
        {
            topActions.append(menu);
        }
        else
        {
            childIds[pid].append(sid);
        }
    }

    QAction *menuNew = new QAction("New Set...", setMenu);
    connect(menuNew, SIGNAL(triggered()), this, SLOT(addToDefaultFittingSet()));
    menuNew->setProperty("SET_ID", QUuid().toString());

    finalActions.append(menuNew);
    finalActions.append(setMenu->addSeparator());

    QStack<QAction*> uuids;
    for(int i=0; i<topActions.size();++i)
    {
        QAction* menu = topActions[i];
        menu->setProperty("LEVEL", 0);
        uuids.push(menu);
    }
    while(!uuids.isEmpty())
    {
        QAction* menu = uuids.pop();
        int level = menu->property("LEVEL").toInt();
        QString indent = QString().fill(' ', level*4);
        menu->setText(indent + menu->text());

        finalActions.append(menu);

        QString menuID = menu->property("SET_ID").toString();
        QStringList children = childIds[menuID];
        for(int i=0; i<children.size();++i)
        {
            QAction *cmenu = actions[children[i]];
            cmenu->setProperty("LEVEL", level+1);
            uuids.push(cmenu);
        }
    }

    setMenu->addActions(finalActions);
}

void GeometryListView::onTableViewContextMenu(const QPoint &pos)
{
    QObject* thesender = sender();

    Q_UNUSED(pos);
    QMenu *menu = new QMenu();
    QVariant v = qVariantFromValue((void *) sender());
    menu->setProperty("OBJ", v);

    if (thesender == m_tableMolecule)
    {

        QMenu *setMenu = new QMenu("Add selection to ...", menu);
        menu->addMenu(setMenu);
        setMenu->addAction("Erepfit Force Equations", this, SLOT(addItemsToErepfitForceEquations()));
        QMenu *testsetMenu = new QMenu("TestSuite sets", setMenu);
        setMenu->addMenu(testsetMenu);
        BuildSelectionMenu(testsetMenu);
    }

    menu->addSeparator();
    menu->addAction("Duplicate", this, SLOT(duplicateSelected()));
    menu->addSeparator();
    menu->addAction("Export", this, SLOT(exportSelected()));

    menu->addSeparator();
    menu->addAction("Select all", this, SLOT(selectAllItems()));
    menu->addAction("Select none", this, SLOT(selectNoItems()));
    menu->exec(QCursor::pos());

    delete menu;
}

void GeometryListView::addToDefaultFittingSet()
{
    QAction* action = qobject_cast<QAction*>(sender());
    if (action)
    {

        m_database->transaction();
        bool setOK = false;
        QString set_id = action->property("SET_ID").toString();
        if (set_id == QUuid().toString())
        {
            set_id = QUuid::createUuid().toString();
            PredefinedSetItemEditor* editor = new PredefinedSetItemEditor(this);
            int ret = editor->exec();
            if (ret == QDialog::Accepted)
            {
                QSqlQuery query(m_database->database());
                query.prepare("INSERT INTO testsuite_set_id (set_id, parentSet, name, description) "
                              "VALUES (:set_id, :parentSet, :name, :description)");
                query.bindValue(":set_id", set_id);
                query.bindValue(":name", editor->name());
                query.bindValue(":description", editor->desc());
                query.bindValue(":parentSet", QUuid().toString());
                if (query.exec())
                {
                    setOK = true;
                }
            }
            delete editor;
        }
        else
        {
            setOK = true;
        }

        if (!setOK)
        {
            m_database->rollback();
            return;
        }

        QSqlTableModel *model = new QSqlTableModel(this, m_database->database());
        model->setEditStrategy(QSqlTableModel::OnManualSubmit);
        model->setTable("testsuite_default_selection");
        model->select();


        QModelIndexList selections_molecule = m_tableMolecule->selectionModel()->selectedRows();

        bool hasNonStationaryPoint = false;

        for(int i=0; i<selections_molecule.size();++i)
        {
            const QModelIndex& index = m_proxyModelMolecule->mapToSource(selections_molecule.at(i));

            QSqlRecord record(model->record());

            int index_elements = model_molecule->fieldIndex("elements");

            int index_stat = model_molecule->fieldIndex("Stationary");
            if (model_molecule->data(model_molecule->index(index.row(), index_stat)).toInt() == 0)
            {
                hasNonStationaryPoint = true;
                continue;
            }


            record.setValue("elements", model_molecule->data(model_molecule->index(index.row(), index_elements)).toString());

            QString uuid = model_molecule->data(model_molecule->index(index.row(),0)).toString();
            record.setValue("set_id", set_id);
            record.setValue("uuid", uuid);

            record.setValue("property_id", 1);
            record.setValue("weight", 1.0);

            record.setValue("name_uuid", uuid);
            record.setValue("elements_uuid", uuid);

            model->insertRecord(-1, record);
        }
        bool succ = model->submitAll();

        if (succ)
        {
            m_database->commit();

            if (hasNonStationaryPoint)
            {
                QMessageBox qbox(this);
                qbox.setText("Some selected entries were non-stationary data, which cannot be added to testsuite set. Skipped.");
                qbox.setStandardButtons(QMessageBox::Ok);
                qbox.setIcon(QMessageBox::Information);
                qbox.exec();
            }

        }
        else
        {
            qDebug() << model->lastError().text();
            m_database->rollback();
        }


    }

}

void GeometryListView::exportSelected()
{
    QObject* sender_obj = sender();
    QTableView *view = (QTableView*)sender_obj->parent()->property("OBJ").value<void *>();
    if (view)
    {
        auto  list = view->selectionModel()->selectedRows(0);
        QList<QString> uuids;
        for(int i=0; i<list.size(); ++i)
        {
            uuids.append(list.at(i).data().toString());
        }
        if (uuids.size() > 1)
        {

        }
        else
        {
            QString selectedFilter;
            QString filename = QFileDialog::getSaveFileName(this, "Choose the export filename", QDir::homePath(), "GenFormat (*.gen);;XYZ Format (*.xyz);;HSD Format (*.hsd)", &selectedFilter);
            if (filename.isEmpty())
            {
                return;
            }
            QString format;
            if (selectedFilter == "GenFormat (*.gen)")
            {
                format = "gen";
            }
            else if (selectedFilter == "XYZ Format (*.xyz)")
            {
                format = "xyz";
            }
            else if (selectedFilter == "HSD Format (*.hsd)")
            {
                format = "hsd";
            }

            Geometry geom;
            m_database->getGeometry(uuids.first(), &geom);
            if (QFileInfo(filename).suffix() != format)
            {
                filename += "." + format;
            }

            QFile fileout(filename);
            if (fileout.open(QIODevice::WriteOnly | QIODevice::Text))
            {
                QTextStream stream(&fileout);
                if (geom.writeOut(stream, format))
                {
                    QMessageBox qbox(this);
                    qbox.setText(filename + " is wriiten.");
                    qbox.setStandardButtons(QMessageBox::Ok);
                    qbox.setIcon(QMessageBox::Information);
                    qbox.exec();
                }
            }
        }
    }
}

void GeometryListView::setupFilter()
{
    m_filter_columnIndex->clear();
    for(int i=0; i<m_model->columnCount(); ++i)
    {
        if (m_tableView->isColumnHidden(i))
        {
            continue;
        }
        m_filter_columnIndex->addItem(m_proxyModel->headerData(i, Qt::Horizontal).toString(), i);
    }
    m_filter_content->clear();
}

void GeometryListView::filterChanged()
{
    QString filterText = m_filter_content->text();
    bool ok;
    int filterColumn = m_filter_columnIndex->currentData().toInt(&ok);
    if (!ok)
    {
        return;
    }
    QString filterColumnString = m_filter_columnIndex->currentText().toLower();
    if (filterColumnString == "elements")
    {
        QStringList inputElements = filterText.split(QRegularExpression("[ ,]"), QString::SkipEmptyParts);
        if (inputElements.isEmpty())
        {
            m_proxyModel->clearElementFilter();
        }
        else
        {
            m_proxyModel->setElementFilter(inputElements);
        }

    }
    else
    {
        m_proxyModel->clearElementFilter();
        m_proxyModel->setFilterKeyColumn(filterColumn);
        m_proxyModel->setFilterFixedString(filterText);
    }
}

void GeometryListView::tabChanged(int index)
{
    if (index==0)
    {
        m_model = model_molecule;
        m_proxyModel = m_proxyModelMolecule;
        m_tableView = m_tableMolecule;
    }
    else
    {
        m_model = model_crystal;
        m_proxyModel = m_proxyModelCrystal;
        m_tableView = m_tableCrystal;
    }
    setupFilter();
}

void GeometryListView::resetModels()
{
    if (model_crystal)
    {
//        model_crystal->deleteLater();
        delete model_crystal;
    }

    QSqlDatabase db = m_database->database();

    model_crystal = new QSqlTableModel(this, db);
    model_crystal->setEditStrategy(QSqlTableModel::OnManualSubmit);
    model_crystal->setTable("CrystalGeometryListView");

    if (model_molecule)
    {
        delete model_molecule;
//        model_molecule->deleteLater();
    }
    model_molecule = new QSqlTableModel(this, db);
    model_molecule->setEditStrategy(QSqlTableModel::OnManualSubmit);
    model_molecule->setTable("MoleculeGeometryListView");

    reload();


    m_proxyModelCrystal = new ElementSortFilterProxyModel;
    m_proxyModelMolecule = new ElementSortFilterProxyModel;
    m_proxyModelCrystal->setElementFilterMode(ElementSortFilterProxyModel::Any);
    m_proxyModelMolecule->setElementFilterMode(ElementSortFilterProxyModel::Any);

    int elementsIndexCrystal = model_crystal->fieldIndex("elements");
    if (elementsIndexCrystal != -1)
    {
        m_proxyModelCrystal->setElementFilterKeyColumn(elementsIndexCrystal);
    }
    m_proxyModelCrystal->setSourceModel(model_crystal);

    int elementsIndexMolecule = model_molecule->fieldIndex("elements");
    if (elementsIndexMolecule != -1)
    {
        m_proxyModelMolecule->setElementFilterKeyColumn(elementsIndexMolecule);
    }
    m_proxyModelMolecule->setSourceModel(model_molecule);



    m_tableCrystal->setModel(m_proxyModelCrystal);
    m_tableMolecule->setModel(m_proxyModelMolecule);

//    m_tableCrystal->setModel(model_crystal);
//    m_tableMolecule->setModel(model_molecule);

    m_tableMolecule->hideColumn(0);
    m_tableCrystal->hideColumn(0);
    m_tableMolecule->setItemDelegateForColumn(5,new YesNoCellItemDelegate(this));

    tabChanged(0);
    setupFilter();
}
