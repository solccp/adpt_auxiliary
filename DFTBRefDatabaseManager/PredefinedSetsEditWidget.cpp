#include "PredefinedSetsEditWidget.h"
#include <QHBoxLayout>
#include <QTreeWidget>
#include <QSqlQueryModel>
#include <QSqlRecord>
#include <QTableView>
#include <QTreeWidgetItemIterator>
#include <QSqlQuery>
#include <QMessageBox>
#include <QCloseEvent>
#include <QDebug>
#include "PredefinedSetItemEditor.h"
#include <QSqlTableModel>
#include <QSortFilterProxyModel>
#include <QSqlError>
#include <QMenu>

#include "ReorderableTreeWidget.h"
#include "sqlrelationaltablemodel.h"
#include "doublenumbercelldelegate.h"

#include <QVBoxLayout>
#include <QHBoxLayout>


#include <QUuid>


#include <QStyledItemDelegate>
#include <QApplication>
#include <QHeaderView>
#include <QFileDialog>
#include <fstream>

#include "testsuiteinputfilegenerator.h"
//#include "DFTBTestSuite/input_yaml.h"

class ItemDelegate : public QStyledItemDelegate
{
public:
    ItemDelegate(QObject *parent = 0)
        : QStyledItemDelegate(parent)
    {
    }

    void paint ( QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const
    {
        QStyleOptionViewItemV4 viewItemOption(option);

//        if (index.column() == 0)
        {
            const int textMargin = QApplication::style()->pixelMetric(QStyle::PM_FocusFrameHMargin) + 1;
            QRect newRect = QStyle::alignedRect(option.direction, Qt::AlignCenter,
                                                QSize(option.decorationSize.width() + 5,option.decorationSize.height()),
                                                QRect(option.rect.x() + textMargin, option.rect.y(),
                                                      option.rect.width() - (2 * textMargin), option.rect.height()));
            viewItemOption.rect = newRect;
        }
        QStyledItemDelegate::paint(painter, viewItemOption, index);
    }

    virtual bool editorEvent(QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option,
                             const QModelIndex &index)
    {
        Q_ASSERT(event);
        Q_ASSERT(model);

        // make sure that the item is checkable
        Qt::ItemFlags flags = model->flags(index);
        if (!(flags & Qt::ItemIsUserCheckable) || !(flags & Qt::ItemIsEnabled))
            return false;
        // make sure that we have a check state
        QVariant value = index.data(Qt::CheckStateRole);
        if (!value.isValid())
            return false;
        // make sure that we have the right event type
        if (event->type() == QEvent::MouseButtonRelease) {
            const int textMargin = QApplication::style()->pixelMetric(QStyle::PM_FocusFrameHMargin) + 1;
            QRect checkRect = QStyle::alignedRect(option.direction, Qt::AlignCenter,
                                                  option.decorationSize,
                                                  QRect(option.rect.x() + (2 * textMargin), option.rect.y(),
                                                        option.rect.width() - (2 * textMargin),
                                                        option.rect.height()));
            if (!checkRect.contains(static_cast<QMouseEvent*>(event)->pos()))
                return false;
        } else if (event->type() == QEvent::KeyPress) {
            if (static_cast<QKeyEvent*>(event)->key() != Qt::Key_Space&& static_cast<QKeyEvent*>(event)->key() != Qt::Key_Select)
                return false;
        } else {
            return false;
        }
        Qt::CheckState state = (static_cast<Qt::CheckState>(value.toInt()) == Qt::Checked
                                ? Qt::Unchecked : Qt::Checked);
        return model->setData(index, state, Qt::CheckStateRole);
    }
};





PredefinedSetsEditWidget::PredefinedSetsEditWidget(DFTBRefDatabase *database, bool editable, QWidget *parent) :
    QDialog(parent), m_database(database)
{
    Q_UNUSED(editable);
    m_splitter = new QSplitter(this);
    m_treeView = new ReorderableTreeWidget();
    m_tableView = new QTableView();
    m_splitter->addWidget(m_treeView);
    m_splitter->addWidget(m_tableView);




    QHBoxLayout *layout = new QHBoxLayout();
    this->setLayout(layout);
    layout->addWidget(m_splitter);

    m_splitter->setStretchFactor(0,1);
    m_splitter->setStretchFactor(1,3);

    m_model_tree = new QSqlTableModel(this, m_database->database());
    m_model_tree->setTable("testsuite_set_id");
    m_model_tree->setEditStrategy(QSqlTableModel::OnManualSubmit);
    m_model_tree->select();


    if (!m_database->canWrite())
    {
        setIsEditable(false);
        m_treeView->setReorderable(false);
    }

    loadItems();
    connect(m_treeView, SIGNAL(itemClicked(QTreeWidgetItem*,int)), this, SLOT(itemClicked(QTreeWidgetItem*,int)));
    connect(m_treeView, SIGNAL(itemReordered(QTreeWidgetItem*,QTreeWidgetItem*)), this, SLOT(itemReordered(QTreeWidgetItem*,QTreeWidgetItem*)));
    connect(m_treeView, SIGNAL(itemDoubleClicked(QTreeWidgetItem*,int)), this, SLOT(itemDoubleClicked(QTreeWidgetItem*,int)));
    connect(m_treeView, SIGNAL(itemSelectionChanged()), this, SLOT(itemSelectionChanged()));

    resize(800, 600);
    m_treeView->setExpandsOnDoubleClick(false);

    SqlRelationalTableModel *sourceModel = new SqlRelationalTableModel(m_database->database(), this);

    sourceModel->setRelation(2, QSqlRelation("properties_info", "id", "property_name"));
    sourceModel->setRelation(4, QSqlRelation("All_UUID_View", "UUID", "name"));
    sourceModel->setRelation(5, QSqlRelation("All_UUID_View", "UUID", "elements"));
    sourceModel->setLinkRelation(6, 1, QSqlRelation("All_UUID_View", "UUID", "Method"));
    sourceModel->setLinkRelation(7, 1, QSqlRelation("All_UUID_View", "UUID", "Basis"));

    sourceModel->setTable("testsuite_default_selection");
    sourceModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
    sourceModel->select();

    m_model_table = new QSortFilterProxyModel(this);
    m_model_table->setSourceModel(sourceModel);
    m_model_table->setFilterKeyColumn(0);


    int p_typeIndex = sourceModel->fieldIndex("property_id");
    int nameIndex = sourceModel->fieldIndex("name_uuid");
    int elementsIndex = sourceModel->fieldIndex("elements_uuid");
    int weightIndex = sourceModel->fieldIndex("weight");

    m_model_table->setHeaderData(p_typeIndex, Qt::Horizontal, "Property Type");
    m_model_table->setHeaderData(nameIndex, Qt::Horizontal, "Name");
    m_model_table->setHeaderData(elementsIndex, Qt::Horizontal, "Elements");
    m_model_table->setHeaderData(weightIndex, Qt::Horizontal, "Weight");

    m_model_table->setHeaderData(6, Qt::Horizontal, "Method");
    m_model_table->setHeaderData(7, Qt::Horizontal, "Basis");


    m_tableView->setModel(m_model_table);
    m_tableView->hideColumn(0);

    m_tableView->setItemDelegateForColumn(weightIndex, new DoubleNumberCellDelegate(this));

    m_tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    m_tableView->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(m_tableView, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(tableViewContextMenu(QPoint)));

    m_treeView->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(m_treeView, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(treeViewContextMenu(QPoint)));


    m_tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_tableView->resizeColumnsToContents();
    m_tableView->setSortingEnabled(true);

    connect(m_tableView, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(tableCellDoubleClicked(QModelIndex)));



    m_tableView->hideColumn(1);
//    m_tableView->setItemDelegateForColumn(1, new ItemDelegate() );
//    sourceModel->setCheckableColumnIndex(1);
//    m_tableView->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Fixed);
//    m_tableView->setColumnWidth(1, 50);
//    m_treeView->setReorderable(false);
//    setIsEditable(false);





}

QSet<QString> PredefinedSetsEditWidget::selectedSets() const
{
    QSet<QString> selectedSets;

    QTreeWidgetItemIterator it(m_treeView);
    while (*it)
    {
        QTreeWidgetItem *item = (*it);
        if (item->checkState(0) == Qt::Checked)
        {
            selectedSets.insert(item->data(0, Qt::UserRole).toString());
        }
        ++it;
    }
    return selectedSets;
}

void PredefinedSetsEditWidget::itemClicked(QTreeWidgetItem *item, int column)
{
    Q_UNUSED(column);
    if (!m_isEditable)
    {
        return;
    }

    checkChildState(item);
    checkParentState(item);

    QSet<QString> selectedSets_new = selectedSets();

    if (m_selectedSets != selectedSets_new)
    {
        m_selectedSets = selectedSets_new;

        updateTreeModel();
        emit selectionChanged(m_selectedSets);
    }
}

void PredefinedSetsEditWidget::itemReordered(QTreeWidgetItem *moveditem, QTreeWidgetItem *old_parent)
{
    QTreeWidgetItem* new_parent = moveditem->parent();
    QString new_parent_id = QUuid().toString();
    QString old_parent_id = new_parent_id;

    if (new_parent)
    {
        new_parent_id = new_parent->data(0, Qt::UserRole).toString();
    }

    if (old_parent)
    {
        old_parent_id = old_parent->data(0, Qt::UserRole).toString();
    }

    QVariant moveditem_id = moveditem->data(0, Qt::UserRole);

    if (old_parent_id == new_parent_id)
        return;

    QSortFilterProxyModel *model = new QSortFilterProxyModel();
    model->setSourceModel(m_model_tree);

    model->setFilterKeyColumn(0);
    model->setFilterFixedString(moveditem_id.toString());

    if (model->rowCount() == 1)
    {
        model->setData(model->index(0,1), new_parent_id);
    }
    delete model;
}

void PredefinedSetsEditWidget::itemDoubleClicked(QTreeWidgetItem *item, int column)
{
    if (!isEditable())
    {
        return;
    }
    PredefinedSetItemEditor *editor = new PredefinedSetItemEditor(this);
    editor->setName(item->text(0));
    editor->setDesc(item->toolTip(0));
    int ret = editor->exec();
    if (ret == QDialog::Accepted)
    {
        QString set_id = item->data(0, Qt::UserRole).toString();

        QSortFilterProxyModel *model = new QSortFilterProxyModel();
        model->setSourceModel(m_model_tree);

        model->setFilterKeyColumn(0);
        model->setFilterFixedString(set_id);

        if (model->rowCount() == 1)
        {
            if (item->text(0) != editor->name())
            {
                if (model->setData(model->index(0,2), editor->name()))
                {
                    item->setText(0, editor->name());
                }
            }
            if (item->toolTip(0) != editor->desc())
            {
                if (model->setData(model->index(0,3), editor->desc()))
                {
                    item->setToolTip(0, editor->desc());
                }
            }

        }
        delete model;
    }
}



void PredefinedSetsEditWidget::itemSelectionChanged()
{
    QTreeWidgetItem *item = m_treeView->currentItem();
    if (item)
    {
        QList<QString> all_setids = allSetIds(item);
        QStringList filter;
        for(int i=0; i<all_setids.size();++i)
        {
            filter.append(all_setids[i].replace("{", "\\{").replace("}", "\\}"));
        }
//        qDebug() << filter.join("|");
        m_model_table->setFilterRegExp(filter.join("|"));
        m_model_table->setFilterKeyColumn(0);
    }
}

void PredefinedSetsEditWidget::treeViewContextMenu(const QPoint &pos)
{
    if (!m_isEditable)
    {
        return ;
    }

    QMenu *menu = new QMenu();

    QTreeWidgetItem *item = m_treeView->itemAt(pos);

    QList<QAction*> actions;
    if (item)
    {
        QAction *action = new QAction("Add New Subset...", menu);
        connect(action, SIGNAL(triggered()), this, SLOT(addSubSet()));
        action->setProperty("SET_ID", item->data(0, Qt::UserRole));
        actions.append(action);

        action = new QAction("Remove Selected Set", menu);
        connect(action, SIGNAL(triggered()), this, SLOT(removeSet()));
        action->setProperty("SET_ID", item->data(0, Qt::UserRole));
        actions.append(action);


        action = new QAction("Export", menu);
        connect(action, SIGNAL(triggered()), this, SLOT(exportSet()));
        action->setProperty("SET_ID", item->data(0, Qt::UserRole));
        actions.append(action);

    }
    else
    {
        QAction *action = new QAction("Add New Set...", menu);
        connect(action, SIGNAL(triggered()), this, SLOT(addSubSet()));
        action->setProperty("SET_ID", QUuid().toString());
        actions.append(action);
    }

    menu->addActions(actions);

    menu->exec(QCursor::pos());

    delete menu;
}

void PredefinedSetsEditWidget::tableViewContextMenu(const QPoint &pos)
{
    Q_UNUSED(pos);
    if (!isEditable())
        return;

    QMenu *menu = new QMenu();
    menu->addAction("Remove Selected", this, SLOT(tableViewRemoveSelected()));
    menu->exec(QCursor::pos());
    delete menu;
}

void PredefinedSetsEditWidget::tableCellDoubleClicked(const QModelIndex &index)
{
    if (m_model_table->headerData(index.column(), Qt::Horizontal).toString().toLower() == "weight")
    {
        m_tableView->edit(index);
    }
}

void PredefinedSetsEditWidget::tableViewRemoveSelected()
{
    QModelIndexList list = m_tableView->selectionModel()->selectedRows();
    if (!list.empty())
    {
        for (int i=list.size()-1; i>=0; --i)
        {
            int row = list.at(i).row();
            m_tableView->model()->removeRows(row,1);
        }
    }
}

void PredefinedSetsEditWidget::addSubSet()
{
    QAction* action = qobject_cast<QAction*>(sender());
    if (action)
    {
        QString parent_set_id = QUuid().toString();
        if (action->property("SET_ID").isValid())
        {
            parent_set_id = action->property("SET_ID").toString();
        }


        PredefinedSetItemEditor *editor = new PredefinedSetItemEditor(this);
        int ret = editor->exec();
        if (ret == QDialog::Accepted)
        {
            QSqlRecord record(m_model_tree->record());
            record.setValue("set_id", QUuid::createUuid().toString());
            record.setValue("parentSet", parent_set_id);
            record.setValue("name", editor->name());
            record.setValue("description", editor->desc());
            record.setValue("selected", 0);
            record.setGenerated(0, true);
            record.setGenerated(1, true);
            record.setGenerated(2, true);
            record.setGenerated(3, true);
            if (m_model_tree->insertRecord(-1, record))
            {
                loadItems();
            }
        }
        delete editor;
    }
}

void PredefinedSetsEditWidget::removeSet()
{
    QAction* action = qobject_cast<QAction*>(sender());
    if (action)
    {
        QString set_id = QUuid().toString();
        if (action->property("SET_ID").isValid())
        {
            set_id = action->property("SET_ID").toString();
        }

        QSortFilterProxyModel *proxyModel = new QSortFilterProxyModel(this);
        proxyModel->setSourceModel(m_model_tree);

        proxyModel->setFilterKeyColumn(0);
        proxyModel->setFilterFixedString(set_id);

        if (proxyModel->rowCount() == 1)
        {
            QModelIndex index = proxyModel->mapToSource(proxyModel->index(0,0));
            if (index.isValid())
            {
                if (m_model_tree->removeRow(index.row()))
                {
                    removeTableItems(set_id);
                    loadItems();
                }
            }
        }

        delete proxyModel;
    }
}

void PredefinedSetsEditWidget::exportSet()
{
    QAction* action = qobject_cast<QAction*>(sender());
    if (action)
    {
        QString set_id = QUuid().toString();
        if (action->property("SET_ID").isValid())
        {
            set_id = action->property("SET_ID").toString();
        }

        TestSuiteInputFileGenerator *generator = new TestSuiteInputFileGenerator(m_database, this);

        QSqlTableModel realtableModel(this, m_database->database());

        realtableModel.setTable("testsuite_default_selection");
        realtableModel.select();

        QSortFilterProxyModel tableModel;
        tableModel.setSourceModel(&realtableModel);

        QTreeWidgetItem *item = m_treeView->currentItem();
        if (!item)
        {
            return;
        }
        QList<QString> all_setids = allSetIds(item);
        QStringList filter;
        for(int i=0; i<all_setids.size();++i)
        {
            filter.append(all_setids[i].replace("{", "\\{").replace("}", "\\}"));
        }
        tableModel.setFilterKeyColumn(0);
        tableModel.setFilterRegExp(filter.join("|"));



        int prop_id_index = realtableModel.fieldIndex("property_id");
        int uuid_index = realtableModel.fieldIndex("uuid");
        int weight_index = realtableModel.fieldIndex("weight");
        for(int i=0; i<tableModel.rowCount();++i)
        {
            int pid = tableModel.data(tableModel.index(i, prop_id_index)).toInt();
            QString uuid = tableModel.data(tableModel.index(i, uuid_index)).toString();
            double weight = tableModel.data(tableModel.index(i, weight_index)).toDouble();

            switch(pid)
            {
            case 1:
                generator->addGeometryTarget(uuid,weight);
                break;
            case 2:
                generator->addBandStructureTarget(uuid,weight);
                break;
            case 3:
                generator->addFrequencyTarget(uuid,weight);
                break;
            case 4:
                generator->addAtomizationEnergyTarget(uuid,weight);
                break;
            case 5:
                generator->addReactionEnergyTarget(uuid,weight);
                break;
            }
        }

        DFTBTestSuite::ReferenceData ref_data = generator->exportRefData();
        DFTBTestSuite::InputData input_data = generator->exportInputData();


         QString ref_file = QFileDialog::getSaveFileName(this, tr("Export TestSuite Reference file"),
                                                        QDir::homePath(), "YAML file (*.yaml)");


//         if (!ref_file.isEmpty())
//         {
//             std::ofstream final_reference(ref_file.toStdString());
//             YAML::Node node;

//            node = ref_data;
//            final_reference << node << std::endl;

//            QMessageBox box(this);
//            box.setText("TestSuite reference file has been saved to " + ref_file);
//            box.setIcon(QMessageBox::Information);
//            box.setStandardButtons(QMessageBox::Yes );
//            box.exec();
//         }

//         QString input_file = QFileDialog::getSaveFileName(this, tr("Export TestSuite Input"),
//                                                         QDir::homePath(), "YAML file (*.yaml)");

//         if (!input_file.isEmpty())
//         {
//             std::ofstream final_input(input_file.toStdString());
//             YAML::Node node;
//             node = input_data;
//             final_input << node << std::endl;

//             QMessageBox box(this);
//             box.setText("TestSuite input file has been saved to " + input_file);
//             box.setIcon(QMessageBox::Information);
//             box.setStandardButtons(QMessageBox::Yes);
//             box.exec();
//         }
         delete generator;

    }





}

void PredefinedSetsEditWidget::removeTableItems(const QString &set_id)
{
    QSortFilterProxyModel *proxyModel = new QSortFilterProxyModel(this);
    proxyModel->setSourceModel(m_model_table);

    proxyModel->setFilterKeyColumn(0);
    proxyModel->setFilterFixedString(set_id);

    for (int i=0; i< proxyModel->rowCount(); ++i)
    {
        QModelIndex index = proxyModel->mapToSource(proxyModel->index(i,0));
        if (index.isValid())
        {
            if (m_model_table->removeRow(index.row()))
            {
            }
        }
    }

    delete proxyModel;
}
bool PredefinedSetsEditWidget::isEditable() const
{
    return m_isEditable;
}

void PredefinedSetsEditWidget::setIsEditable(bool isEditable)
{
    m_isEditable = isEditable;
}


bool PredefinedSetsEditWidget::isTreeDirty()
{
    return m_model_tree->isDirty();
}

bool PredefinedSetsEditWidget::isTableDirty()
{
    QSqlTableModel *sqlModel = qobject_cast<QSqlTableModel*>(m_model_table->sourceModel());
    if (sqlModel)
    {
        return sqlModel->isDirty();
    }
    return false;
}

void PredefinedSetsEditWidget::checkParentState(QTreeWidgetItem *item)
{
    QTreeWidgetItem *parentItem = item->parent();
    if (parentItem)
    {
        int numChildChecked = 0;

        for(int i=0; i<parentItem->childCount();++i)
        {
            if (parentItem->child(i)->checkState(0) == Qt::Checked )
            {
                ++numChildChecked;
            }
        }

        if (numChildChecked == parentItem->childCount())
        {
            parentItem->setCheckState(0, Qt::Checked);
        }
        else if (numChildChecked == 0)
        {
            parentItem->setCheckState(0, Qt::Unchecked);
        }
        else
        {
            parentItem->setCheckState(0, Qt::PartiallyChecked);
        }
        checkParentState(parentItem);
    }
}

void PredefinedSetsEditWidget::checkChildState(QTreeWidgetItem *item)
{
    if (item->checkState(0) == Qt::PartiallyChecked)
        return ;

    for(int i=0; i<item->childCount();++i)
    {
        item->child(i)->setCheckState(0, item->checkState(0));
        checkChildState(item->child(i));
    }
}

QList<QString> PredefinedSetsEditWidget::allSetIds(QTreeWidgetItem *item)
{
    if (item)
    {
        QList<QString> res;
        res.append(item->data(0, Qt::UserRole).toString());
        for(int i=0; i<item->childCount();++i)
        {
            res.append(allSetIds(item->child(i)));
        }
        return res;
    }
    return QList<QString>();
}

void PredefinedSetsEditWidget::loadItems()
{
    m_selectedSets.clear();
    m_treeView->clear();

    m_treeView->setColumnCount(1);
    m_treeView->setHeaderHidden(true);


    QMap<QString, QTreeWidgetItem *> items;
    QMap<QString, QString> parentIDs;

    for (int i = 0; i < m_model_tree->rowCount(); ++i)
    {
        // if marked deleted, skip
        if (m_model_tree->headerData(i,Qt::Vertical).toString() == "!")
        {
            continue;
        };

        QString set_id = m_model_tree->data(m_model_tree->index(i,0)).toString();
        QString parentSet = m_model_tree->data(m_model_tree->index(i,1)).toString();
        QString name = m_model_tree->data(m_model_tree->index(i,2)).toString();
        QString description = m_model_tree->data(m_model_tree->index(i,3)).toString();
        bool selected = m_model_tree->data(m_model_tree->index(i,4)).toBool();

        QTreeWidgetItem *item = new QTreeWidgetItem();
        parentIDs.insert(set_id, parentSet);

        if (selected)
        {
            item->setCheckState(0, Qt::Checked);
            m_selectedSets.insert(set_id);
        }
        else
        {
            item->setCheckState(0, Qt::Unchecked);
        }

        item->setText(0,name);
        item->setToolTip(0,description);
        item->setData(0, Qt::UserRole, set_id);

        item->setData(0, Qt::UserRole+1, i);

        item->setFlags(item->flags() | Qt::ItemIsDropEnabled | Qt::ItemIsDragEnabled);


        if (!m_isEditable)
        {
            item->setFlags(item->flags() ^ Qt::ItemIsUserCheckable);
        }

        items.insert(set_id, item);

    }

    QList<QTreeWidgetItem*> topItems;
    QMapIterator<QString, QTreeWidgetItem*> it(items);
    while(it.hasNext())
    {
        it.next();
        QString uuid = it.key();
        QString parentID = parentIDs[uuid];
        QTreeWidgetItem* item = it.value();
        if (parentID != QUuid().toString() && items.contains(parentID))
        {
            QTreeWidgetItem* parentItem = items[parentID];
            parentItem->addChild(item);
        }
        else
        {
            topItems.append(item);
        }
    }



    m_treeView->insertTopLevelItems(0, topItems);

//    m_treeView->expandAll();
    m_treeView->resizeColumnToContents(0);
}

//void PredefinedSetsEditWidget::loadItems()
//{
//    m_selectedSets.clear();
//    m_treeView->clear();

//    m_treeView->setColumnCount(1);
//    m_treeView->setHeaderHidden(true);


//    QSortFilterProxyModel *proxyModel = new QSortFilterProxyModel(this);
//    proxyModel->setSourceModel(m_model_tree);

//    proxyModel->setFilterKeyColumn(1);
//    proxyModel->setFilterFixedString(QUuid().toString());


//    QMap<QString, QTreeWidgetItem *> topItems;

//    for (int i = 0; i < proxyModel->rowCount(); ++i)
//    {
//        if (proxyModel->headerData(i,Qt::Vertical).toString() == "!")
//        {
//            continue;
//        };

//        QString set_id = proxyModel->data(proxyModel->index(i,0)).toString();

//        QString name = proxyModel->data(proxyModel->index(i,2)).toString();
//        QString description = proxyModel->data(proxyModel->index(i,3)).toString();
//        bool selected = proxyModel->data(proxyModel->index(i,4)).toBool();

//        QTreeWidgetItem *item = new QTreeWidgetItem();
//        if (selected)
//        {
//            item->setCheckState(0, Qt::Checked);
//            m_selectedSets.insert(set_id);
//        }
//        else
//        {
//            item->setCheckState(0, Qt::Unchecked);
//        }

//        item->setText(0,name);
//        item->setToolTip(0,description);
//        item->setData(0, Qt::UserRole, set_id);

//        int sourceRow = proxyModel->mapToSource(proxyModel->index(i, 0)).row();
//        item->setData(0, Qt::UserRole+1, sourceRow);

//        item->setFlags(item->flags() | Qt::ItemIsDropEnabled | Qt::ItemIsDragEnabled);


//        if (!m_isEditable)
//        {
//            item->setFlags(item->flags() ^ Qt::ItemIsUserCheckable);
//        }

//        topItems.insert(set_id, item);

//    }
//    QString nullUUID = QUuid().toString();
//    QString null_UUID_nobrace = nullUUID.mid(1,nullUUID.size()-2);
//    proxyModel->setFilterRegExp(QString("(?!\\{%1\\})").arg(null_UUID_nobrace));

//    for (int i = 0; i < proxyModel->rowCount(); ++i)
//    {
//        QString set_id = proxyModel->data(proxyModel->index(i,0)).toString();
//        QString parent_set_id = proxyModel->data(proxyModel->index(i,1)).toString();

//        if (!topItems.contains(parent_set_id))
//        {
//            continue;
//        }

//        QTreeWidgetItem *parent_item = topItems.value(parent_set_id);

//        QString name = proxyModel->data(proxyModel->index(i,2)).toString();
//        QString description = proxyModel->data(proxyModel->index(i,3)).toString();
//        bool selected = proxyModel->data(proxyModel->index(i,4)).toBool();

//        QTreeWidgetItem *item = new QTreeWidgetItem();
//        if (selected)
//        {
//            m_selectedSets.insert(set_id);
//            item->setCheckState(0, Qt::Checked);
//        }
//        else
//        {
//            item->setCheckState(0, Qt::Unchecked);
//        }

//        item->setText(0,name);
//        item->setToolTip(0,description);
//        item->setData(0, Qt::UserRole, set_id);

//        int sourceRow = proxyModel->mapToSource(proxyModel->index(i, 0)).row();
//        item->setData(0, Qt::UserRole+1, sourceRow);

//        item->setFlags(item->flags() | Qt::ItemIsDropEnabled | Qt::ItemIsDragEnabled);

//        if (!m_isEditable)
//        {
//            item->setFlags(item->flags() ^ Qt::ItemIsUserCheckable);
//        }
//        parent_item->addChild(item);

//    }


//    m_treeView->insertTopLevelItems(0, topItems.values());

//    m_treeView->expandAll();
//    m_treeView->resizeColumnToContents(0);
//}

void PredefinedSetsEditWidget::updateTreeModel()
{
    QTreeWidgetItemIterator it(m_treeView);
    int selectedIndex = m_model_tree->fieldIndex("selected");
    while (*it)
    {
        QString set_id = (*it)->data(0, Qt::UserRole).toString();
        int row = (*it)->data(0, Qt::UserRole+1).toInt();


        if (m_selectedSets.contains(set_id))
        {
            m_model_tree->setData(m_model_tree->index(row,selectedIndex), 1);
        }
        else
        {
            m_model_tree->setData(m_model_tree->index(row,selectedIndex), 0);
        }
        ++it;
    }
}


void PredefinedSetsEditWidget::closeEvent(QCloseEvent *event)
{
    if (isTableDirty() || isTreeDirty())
    {
        QMessageBox qbox;
        qbox.setText("The records have been modified.\nUpdate database for the modification?");
        qbox.setIcon(QMessageBox::Information);
        qbox.setStandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
        int ret = qbox.exec();

        if (ret == QMessageBox::No)
        {
            event->accept();
            return;
        }
        else if ( ret == QMessageBox::Cancel )
        {
            event->ignore();
            return;
        }
    }


    if (isTableDirty())
    {
        QSqlTableModel *sqlModel = qobject_cast<QSqlTableModel*>(m_model_table->sourceModel());
        if (sqlModel)
        {
            sqlModel->database().transaction();
            if(sqlModel->submitAll())
            {
                sqlModel->database().commit();
            }
            else
            {
                sqlModel->database().rollback();
                QMessageBox qbox2(this);
                qbox2.setText("Failed to write data into database:" + sqlModel->lastError().text());
                qbox2.setStandardButtons(QMessageBox::Ok);
                qbox2.setIcon(QMessageBox::Critical);
                qbox2.exec();
                event->ignore();
                return;
            }
        }
        event->accept();
    }
    if (isTreeDirty())
    {
        m_model_tree->database().transaction();
        if(m_model_tree->submitAll())
        {
            m_model_tree->database().commit();
        }
        else
        {
            m_model_tree->database().rollback();
            QMessageBox qbox2(this);
            qbox2.setText("Failed to write data into database:" + m_model_tree->lastError().text());
            qbox2.setStandardButtons(QMessageBox::Ok);
            qbox2.setIcon(QMessageBox::Critical);
            qbox2.exec();
            event->ignore();
            return;
        }
        event->accept();
    }
    event->accept();
}
