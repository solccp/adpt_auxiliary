#ifndef POTENTIALDELEGATE_H
#define POTENTIALDELEGATE_H

#include <QStyledItemDelegate>
class PotentialDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    explicit PotentialDelegate(QObject *parent = 0);

signals:

public slots:
public:
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;
private:


};

#endif // POTENTIALDELEGATE_H
