#ifndef EREPFITINPUTENERGYEQUATIONDIALOG_H
#define EREPFITINPUTENERGYEQUATIONDIALOG_H

#include <QDialog>

QT_BEGIN_NAMESPACE
class QTableView;
QT_END_NAMESPACE

class DFTBRefDatabase;
class SqlRelationalTableModel;
class ErepfitInputEnergyEquationDialog : public QDialog
{
    Q_OBJECT
public:
    ErepfitInputEnergyEquationDialog(DFTBRefDatabase *database, QWidget *parent = 0);

signals:

public slots:
private slots:
    void addRow();
    void removeRow();
    void commitBut();
    bool commit();
    void revert();
private:
    QTableView* m_tableView;
    DFTBRefDatabase* m_database;
    SqlRelationalTableModel* m_model;

protected:
    void closeEvent(QCloseEvent *event);

};

#endif // EREPFITINPUTENERGYEQUATIONDIALOG_H
