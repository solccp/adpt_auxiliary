#ifndef EREPFITPOTENTIALDEFINITIONWIDGET_H
#define EREPFITPOTENTIALDEFINITIONWIDGET_H

#include <QDialog>


QT_BEGIN_NAMESPACE
class QTableView;
class QSqlTableModel;
QT_END_NAMESPACE

class DFTBRefDatabase;

class ErepfitPotentialDefinitionWidget : public QDialog
{
    Q_OBJECT
public:
    ErepfitPotentialDefinitionWidget(DFTBRefDatabase* database, QWidget *parent = 0);

signals:

public slots:
private slots:
    void addNewPotential();
    void removePotential();
    void commitBut();
    bool commit();
    void revert();
private:
    QTableView* m_tableView;
    DFTBRefDatabase* m_database;
    QSqlTableModel* m_model;

    // QWidget interface
protected:
    void closeEvent(QCloseEvent *event);
};

#endif // EREPFITPOTENTIALDEFINITIONWIDGET_H
