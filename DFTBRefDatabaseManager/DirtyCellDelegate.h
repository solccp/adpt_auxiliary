#ifndef DIRTYCELLDELEGATE_H
#define DIRTYCELLDELEGATE_H

#include <QStyledItemDelegate>

class DirtyCellDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    explicit DirtyCellDelegate(QObject *parent = 0);

signals:

public slots:

    // QStyledItemDelegate interface
protected:
    void initStyleOption(QStyleOptionViewItem *option, const QModelIndex &index) const;
};

#endif // DIRTYCELLDELEGATE_H
