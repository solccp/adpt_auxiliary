#ifndef SQLRELATIONDELAGATE_H
#define SQLRELATIONDELAGATE_H

#include <QStyledItemDelegate>

class SqlRelationDelagate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    SqlRelationDelagate(QObject *parent = 0);

signals:

public slots:
private slots:
//    void itemSelected(int row, )    ;
private:
public:
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;

    // QAbstractItemDelegate interface
public:
    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const;
    virtual void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;
};

#endif // SQLRELATIONDELAGATE_H
