#ifndef EREPFITREACTIONDIALOG_H
#define EREPFITREACTIONDIALOG_H

#include <QDialog>


QT_BEGIN_NAMESPACE
class QTableView;
QT_END_NAMESPACE

class DFTBRefDatabase;
class SqlRelationalTableModel;
class ErepfitReactionDialog : public QDialog
{
    Q_OBJECT
public:
    explicit ErepfitReactionDialog(DFTBRefDatabase *database, QWidget *parent = 0);

signals:

public slots:
private slots:
    void addRow();
    void removeRow();
    void commitBut();
    bool commit();
    void revert();

private:
    DFTBRefDatabase* m_database;
    SqlRelationalTableModel* m_model;
    QTableView* m_tableView;


    // QWidget interface
protected:
    void closeEvent(QCloseEvent *);
};

#endif // EREPFITREACTIONDIALOG_H
