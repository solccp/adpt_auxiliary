#ifndef CONFININGDEFAULTFITTINGSETDIALOG_H
#define CONFININGDEFAULTFITTINGSETDIALOG_H

#include <QDialog>

QT_BEGIN_NAMESPACE
class QTableView;
class QSortFilterProxyModel;
QT_END_NAMESPACE

class DFTBRefDatabase;
class CheckableSqlRelationalTableModel;

class ConfiningDefaultFittingSetDialog : public QDialog
{
    Q_OBJECT
public:
    ConfiningDefaultFittingSetDialog(DFTBRefDatabase *database, QWidget *parent = 0);

signals:

public slots:

private slots:
    void commitBut();
    void revert();
    bool commit();
    void element1Changed(const QString& elem);
    void element2Changed(const QString& elem);
    void updateModelFilter();
    void cellClicked(const QModelIndex& index);
private:
    QTableView* m_tableView = nullptr;
    DFTBRefDatabase* m_database = nullptr;
    CheckableSqlRelationalTableModel* m_model = nullptr;
    QSortFilterProxyModel* m_proxyModel = nullptr;
    void setupUI();

    QString m_elem1 = "H";
    QString m_elem2 = "H";
    int m_filter_column = -1;


    // QWidget interface
protected:
    void closeEvent(QCloseEvent *event);

};

#endif // CONFININGDEFAULTFITTINGSETDIALOG_H
