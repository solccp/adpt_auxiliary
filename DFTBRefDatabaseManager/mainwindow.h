#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>


QT_BEGIN_NAMESPACE
class QTabWidget;
QT_END_NAMESPACE


class GeometryListView;
class DFTBRefDatabase;
class BandstructureListView;
class MolecularFrequencyListView;
class MolecularAtomizationEnergyListView;
class ReactionEnergyListView;
class DefaultValueSettingsWidget;

class DataListViewBase;
class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();  
private slots:
    bool init();
    void databaseConnectionChanged();

private slots:
    void importFolderBaseData();
    void clearDatabase();
    void changeDatabase();

    void exportTestSuiteInput();
    void reload_all();
    void about();
    void aboutQt();
    void exportDB();
    void importor();

//    void migrate();

private:
    void setupUI();
    void updateWindowTitle();


private:
    QTabWidget *m_category;
    GeometryListView* m_geomView;
    DFTBRefDatabase* m_database = nullptr;
    BandstructureListView* m_bandstructureView;
    MolecularFrequencyListView* m_molecularFrequencyView;
    DataListViewBase* m_molecularAtomizationEnergyView;
    ReactionEnergyListView *m_reactionEnergyView;
    DefaultValueSettingsWidget *m_defaultValueSettings;
};

#endif // MAINWINDOW_H
