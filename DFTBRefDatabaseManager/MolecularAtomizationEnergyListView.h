#ifndef MOLECULARATOMIZATIONENERGYLISTVIEW_H
#define MOLECULARATOMIZATIONENERGYLISTVIEW_H

#include "DataListViewBase.h"

class MolecularAtomizationEnergyData;
class MolecularAtomizationEnergyListView : public DataListViewBase
{
    Q_OBJECT
public:
    MolecularAtomizationEnergyListView(DFTBRefDatabase *database, QWidget *parent = 0);

signals:

public slots:
private slots:
    void addToErepfitEnergyEquations();

    // DataListViewBase interface
protected:
    void deleteFromDatabase(const QStringList &uuids);
    void addToDatabase();
    void editItemInDatabase(const QString& uuid);
    void duplicateItemInDatabase(const QString& uuid);
    void adjustTableView();
    void initModels();
    int propertyID();
private:
    int showEditor(MolecularAtomizationEnergyData* data);

    // DataListViewBase interface
protected:
    void extraContextMenu(QMenu *parent);
};

#endif // MOLECULARATOMIZATIONENERGYLISTVIEW_H
