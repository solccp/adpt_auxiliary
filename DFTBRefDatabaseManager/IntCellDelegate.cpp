#include "IntCellDelegate.h"

#include <QLineEdit>
#include <QIntValidator>

IntCellDelegate::IntCellDelegate(QObject *parent) :QStyledItemDelegate(parent)
{
}


QWidget *IntCellDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(option)
    if (!index.isValid())
        return 0;
    QLineEdit* editor = new QLineEdit(parent);
    editor->setValidator(new QIntValidator());
    return editor;
}



void IntCellDelegate::initStyleOption(QStyleOptionViewItem *option, const QModelIndex &index) const
{
    QStyledItemDelegate::initStyleOption(option, index);
    option->displayAlignment |= (Qt::AlignRight | Qt::AlignVCenter) ;
}
