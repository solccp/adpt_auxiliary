#include "CheckableSqlRelationalTableModel.h"

CheckableSqlRelationalTableModel::CheckableSqlRelationalTableModel(QSqlDatabase sqldatabase, QObject *parent) :
    SqlRelationalTableModel(sqldatabase, parent)
{
}

Qt::ItemFlags CheckableSqlRelationalTableModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags result = SqlRelationalTableModel::flags(index);
    if (m_checkableColumn != -1 && index.column() == m_checkableColumn)
    {
        result |= Qt::ItemIsUserCheckable;
    }
    return result;


}
int CheckableSqlRelationalTableModel::checkableColumn() const
{
    return m_checkableColumn;
}

void CheckableSqlRelationalTableModel::setCheckableColumn(int checkableColumn)
{
    m_checkableColumn = checkableColumn;
}

QVariant CheckableSqlRelationalTableModel::data(const QModelIndex &index, int role) const
{
    if (m_checkableColumn != -1 && index.column() == m_checkableColumn)
    {
        if (role == Qt::CheckStateRole)
        {
            QVariant value = SqlRelationalTableModel::data(index, Qt::EditRole);
            if (value.toBool())
            {
                return QVariant(Qt::Checked);
            }
            else
            {
                return QVariant(Qt::Unchecked);
            }
        }
        else if (role == Qt::DisplayRole)
        {
            return QVariant();
        }
    }
    return SqlRelationalTableModel::data(index, role);
}

