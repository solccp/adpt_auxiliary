#include "ReorderableTreeWidget.h"
#include <QDragMoveEvent>
#include <QDebug>
#include <QMimeData>

ReorderableTreeWidget::ReorderableTreeWidget(QWidget *parent) :
    QTreeWidget(parent)
{
    setSelectionMode(QAbstractItemView::SingleSelection);
    setDragDropMode(QAbstractItemView::InternalMove);
    setReorderable(true);
}

void ReorderableTreeWidget::setReorderable(bool value)
{
    setAcceptDrops(value);
    setDragEnabled(value);
    setDropIndicatorShown(value);
}

void ReorderableTreeWidget::dropEvent(QDropEvent *event)
{
    QTreeWidget::dropEvent(event);

    if (old_item)
    {
        if (old_item_parent != old_item->parent())
        {
            emit itemReordered(old_item, old_item_parent);
        }
    }
    old_item = nullptr;
    old_item_parent = nullptr;
}

void ReorderableTreeWidget::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        old_item = itemAt(event->pos());
        if (old_item)
        {
            if (old_item->parent())
            {
                old_item_parent = old_item->parent();
            }
        }
    }
    QTreeWidget::mousePressEvent(event);
}

