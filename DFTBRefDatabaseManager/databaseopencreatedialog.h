#ifndef DATABASEOPENCREATEDIALOG_H
#define DATABASEOPENCREATEDIALOG_H

#include <QDialog>

class DataBaseOpenCreateDialog : public QDialog
{
    Q_OBJECT

public:

    enum ActionMode
    {
        Unspecified,
        Open,
        Create,
        Connect
    };

    explicit DataBaseOpenCreateDialog(QWidget *parent = 0);


    ~DataBaseOpenCreateDialog();

    QString getPath() const;
    ActionMode getActionMode() const;


private:
    void setupUI();
signals:
    void databaseOpened(QString filename);
    void databaseCreated(QString filename);

private slots:
    void on_butCreateDB_clicked();
    void on_butOpenDB_clicked();
    void on_butNetDB_clicked();
    void setActionMode(const ActionMode &value);
private:
    ActionMode actionMode = ActionMode::Unspecified;
    QString m_path;
};

#endif // DATABASEOPENCREATEDIALOG_H
