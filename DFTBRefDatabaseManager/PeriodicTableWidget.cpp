#include "PeriodicTableWidget.h"

#include "dftbrefdatabase.h"
#include <QGridLayout>
#include <QSet>
#include <QStringList>
#include <QStyle>
#include <QCheckBox>

#include <QDebug>

PeriodicTableWidget::PeriodicTableWidget(QWidget *parent) :
    QWidget(parent)
{  

    layout_grid = new QGridLayout();
    this->setLayout(layout_grid);


    elem_H  = new QPushButton(tr("H" ));
    elem_He = new QPushButton(tr("He"));

    elem_Li = new QPushButton(tr("Li"));
    elem_Be = new QPushButton(tr("Be"));
    elem_B  = new QPushButton(tr("B" ));
    elem_C  = new QPushButton(tr("C" ));
    elem_N  = new QPushButton(tr("N" ));
    elem_O  = new QPushButton(tr("O" ));
    elem_F  = new QPushButton(tr("F" ));
    elem_Ne = new QPushButton(tr("Ne"));

    elem_Na = new QPushButton(tr("Na"));
    elem_Mg = new QPushButton(tr("Mg"));
    elem_Al = new QPushButton(tr("Al"));
    elem_Si = new QPushButton(tr("Si"));
    elem_P  = new QPushButton(tr("P" ));
    elem_S  = new QPushButton(tr("S" ));
    elem_Cl = new QPushButton(tr("Cl"));
    elem_Ar = new QPushButton(tr("Ar"));

    elem_K  = new QPushButton(tr("K" ));
    elem_Ca = new QPushButton(tr("Ca"));
    elem_Sc = new QPushButton(tr("Sc"));
    elem_Ti = new QPushButton(tr("Ti"));
    elem_V  = new QPushButton(tr("V" ));
    elem_Cr = new QPushButton(tr("Cr"));
    elem_Mn = new QPushButton(tr("Mn"));
    elem_Fe = new QPushButton(tr("Fe"));
    elem_Co = new QPushButton(tr("Co"));
    elem_Ni = new QPushButton(tr("Ni"));
    elem_Cu = new QPushButton(tr("Cu"));
    elem_Zn = new QPushButton(tr("Zn"));
    elem_Ga = new QPushButton(tr("Ga"));
    elem_Ge = new QPushButton(tr("Ge"));
    elem_As = new QPushButton(tr("As"));
    elem_Se = new QPushButton(tr("Se"));
    elem_Br = new QPushButton(tr("Br"));
    elem_Kr = new QPushButton(tr("Kr"));

    elem_Rb = new QPushButton(tr("Rb"));
    elem_Sr = new QPushButton(tr("Sr"));
    elem_Y  = new QPushButton(tr("Y" ));
    elem_Zr = new QPushButton(tr("Zr"));
    elem_Nb = new QPushButton(tr("Nb"));
    elem_Mo = new QPushButton(tr("Mo"));
    elem_Tc = new QPushButton(tr("Tc"));
    elem_Ru = new QPushButton(tr("Ru"));
    elem_Rh = new QPushButton(tr("Rh"));
    elem_Pd = new QPushButton(tr("Pd"));
    elem_Ag = new QPushButton(tr("Ag"));
    elem_Cd = new QPushButton(tr("Cd"));
    elem_In = new QPushButton(tr("In"));
    elem_Sn = new QPushButton(tr("Sn"));
    elem_Sb = new QPushButton(tr("Sb"));
    elem_Te = new QPushButton(tr("Te"));
    elem_I  = new QPushButton(tr("I" ));
    elem_Xe = new QPushButton(tr("Xe"));


    elem_Cs = new QPushButton(tr("Cs"));
    elem_Ba = new QPushButton(tr("Ba"));
    elem_Hf = new QPushButton(tr("Hf"));
    elem_Ta = new QPushButton(tr("Ta"));
    elem_W  = new QPushButton(tr("W" ));
    elem_Re = new QPushButton(tr("Re"));
    elem_Os = new QPushButton(tr("Os"));
    elem_Ir = new QPushButton(tr("Ir"));
    elem_Pt = new QPushButton(tr("Pt"));
    elem_Au = new QPushButton(tr("Au"));
    elem_Hg = new QPushButton(tr("Hg"));
    elem_Tl = new QPushButton(tr("Tl"));
    elem_Pb = new QPushButton(tr("Pb"));
    elem_Bi = new QPushButton(tr("Bi"));
    elem_Po = new QPushButton(tr("Po"));
    elem_At = new QPushButton(tr("At"));
    elem_Rn = new QPushButton(tr("Rn"));

    elem_Fr = new QPushButton(tr("Fr"));
    elem_Ra = new QPushButton(tr("Ra"));

    elem_La = new QPushButton(tr("La"));
    elem_Ce = new QPushButton(tr("Ce"));
    elem_Pr = new QPushButton(tr("Pr"));
    elem_Nd = new QPushButton(tr("Nd"));
    elem_Pm = new QPushButton(tr("Pm"));
    elem_Sm = new QPushButton(tr("Sm"));
    elem_Eu = new QPushButton(tr("Eu"));
    elem_Gd = new QPushButton(tr("Gd"));
    elem_Tb = new QPushButton(tr("Tb"));
    elem_Dy = new QPushButton(tr("Dy"));
    elem_Ho = new QPushButton(tr("Ho"));
    elem_Er = new QPushButton(tr("Er"));
    elem_Tm = new QPushButton(tr("Tm"));
    elem_Yb = new QPushButton(tr("Yb"));
    elem_Lu = new QPushButton(tr("Lu"));

    elem_Ac = new QPushButton(tr("Ac"));
    elem_Th = new QPushButton(tr("Th"));
    elem_Pa = new QPushButton(tr("Pa"));
    elem_U  = new QPushButton(tr("U" ));
    elem_Np = new QPushButton(tr("Np"));
    elem_Pu = new QPushButton(tr("Pu"));
    elem_Am = new QPushButton(tr("Am"));
    elem_Cm = new QPushButton(tr("Cm"));
    elem_Bk = new QPushButton(tr("Bk"));
    elem_Cf = new QPushButton(tr("Cf"));
    elem_Es = new QPushButton(tr("Es"));
    elem_Fm = new QPushButton(tr("Fm"));
    elem_Md = new QPushButton(tr("Md"));
    elem_No = new QPushButton(tr("No"));
    elem_Lr = new QPushButton(tr("Lr"));

    elems.clear();


    elems.insert("H" ,elem_H);
    elems.insert("He",elem_He);
                     
    elems.insert("Li",elem_Li);
    elems.insert("Be",elem_Be);
    elems.insert("B" ,elem_B);
    elems.insert("C" ,elem_C);
    elems.insert("N" ,elem_N);
    elems.insert("O" ,elem_O);
    elems.insert("F" ,elem_F);
    elems.insert("Ne",elem_Ne);
                     
    elems.insert("Na",elem_Na);
    elems.insert("Mg",elem_Mg);
    elems.insert("Al",elem_Al);
    elems.insert("Si",elem_Si);
    elems.insert("P" ,elem_P);
    elems.insert("S" ,elem_S);
    elems.insert("Cl",elem_Cl);
    elems.insert("Ar",elem_Ar);
                     
    elems.insert("K" ,elem_K);
    elems.insert("Ca",elem_Ca);
    elems.insert("Sc",elem_Sc);
    elems.insert("Ti",elem_Ti);
    elems.insert("V" ,elem_V);
    elems.insert("Cr",elem_Cr);
    elems.insert("Mn",elem_Mn);
    elems.insert("Fe",elem_Fe);
    elems.insert("Co",elem_Co);
    elems.insert("Ni",elem_Ni);
    elems.insert("Cu",elem_Cu);
    elems.insert("Zn",elem_Zn);
    elems.insert("Ga",elem_Ga);
    elems.insert("Ge",elem_Ge);
    elems.insert("As",elem_As);
    elems.insert("Se",elem_Se);
    elems.insert("Br",elem_Br);
    elems.insert("Kr",elem_Kr);
                      
    elems.insert("Rb",elem_Rb);
    elems.insert("Sr",elem_Sr);
    elems.insert("Y" ,elem_Y);
    elems.insert("Zr",elem_Zr);
    elems.insert("Nb",elem_Nb);
    elems.insert("Mo",elem_Mo);
    elems.insert("Tc",elem_Tc);
    elems.insert("Ru",elem_Ru);
    elems.insert("Rh",elem_Rh);
    elems.insert("Pd",elem_Pd);
    elems.insert("Ag",elem_Ag);
    elems.insert("Cd",elem_Cd);
    elems.insert("In",elem_In);
    elems.insert("Sn",elem_Sn);
    elems.insert("Sb",elem_Sb);
    elems.insert("Te",elem_Te);
    elems.insert("I" ,elem_I);
    elems.insert("Xe",elem_Xe);
                      
                      
    elems.insert("Cs",elem_Cs);
    elems.insert("Ba",elem_Ba);
    elems.insert("Hf",elem_Hf);
    elems.insert("Ta",elem_Ta);
    elems.insert("W" ,elem_W);
    elems.insert("Re",elem_Re);
    elems.insert("Os",elem_Os);
    elems.insert("Ir",elem_Ir);
    elems.insert("Pt",elem_Pt);
    elems.insert("Au",elem_Au);
    elems.insert("Hg",elem_Hg);
    elems.insert("Tl",elem_Tl);
    elems.insert("Pb",elem_Pb);
    elems.insert("Bi",elem_Bi);
    elems.insert("Po",elem_Po);
    elems.insert("At",elem_At);
    elems.insert("Rn",elem_Rn);
                      
    elems.insert("Fr",elem_Fr);
    elems.insert("Ra",elem_Ra);
                      
    elems.insert("La",elem_La);
    elems.insert("Ce",elem_Ce);
    elems.insert("Pr",elem_Pr);
    elems.insert("Nd",elem_Nd);
    elems.insert("Pm",elem_Pm);
    elems.insert("Sm",elem_Sm);
    elems.insert("Eu",elem_Eu);
    elems.insert("Gd",elem_Gd);
    elems.insert("Tb",elem_Tb);
    elems.insert("Dy",elem_Dy);
    elems.insert("Ho",elem_Ho);
    elems.insert("Er",elem_Er);
    elems.insert("Tm",elem_Tm);
    elems.insert("Yb",elem_Yb);
    elems.insert("Lu",elem_Lu);
                      
    elems.insert("Ac",elem_Ac);
    elems.insert("Th",elem_Th);
    elems.insert("Pa",elem_Pa);
    elems.insert("U" ,elem_U );
    elems.insert("Np",elem_Np);
    elems.insert("Pu",elem_Pu);
    elems.insert("Am",elem_Am);
    elems.insert("Cm",elem_Cm);
    elems.insert("Bk",elem_Bk);
    elems.insert("Cf",elem_Cf);
    elems.insert("Es",elem_Es);
    elems.insert("Fm",elem_Fm);
    elems.insert("Md",elem_Md);
    elems.insert("No",elem_No);
    elems.insert("Lr",elem_Lr);

    QHashIterator<QString, QPushButton*> it(elems);

    while(it.hasNext())
    {
        it.next();
        QPushButton* but = it.value();
        but->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Expanding );
        but->setMaximumWidth(100);
        but->setMinimumWidth(30);
        but->setMaximumHeight(100);
        but->setMinimumHeight(30);
        but->setAutoDefault(false);
        but->setFocusPolicy(Qt::NoFocus);
        connect(but, SIGNAL(clicked()), this, SLOT(buttonClicked()) );
    }

    layout_grid->addWidget(elem_H,0,0,1,1);
    layout_grid->addWidget(elem_He,0,17,1,1);

    layout_grid->addWidget(elem_Li,1,0,1,1);
    layout_grid->addWidget(elem_Be,1,1,1,1);
    layout_grid->addWidget(elem_B,1,12,1,1);
    layout_grid->addWidget(elem_C,1,13,1,1);
    layout_grid->addWidget(elem_N,1,14,1,1);
    layout_grid->addWidget(elem_O,1,15,1,1);
    layout_grid->addWidget(elem_F,1,16,1,1);
    layout_grid->addWidget(elem_Ne,1,17,1,1);


    layout_grid->addWidget(elem_Na,2,0,1,1);
    layout_grid->addWidget(elem_Mg,2,1,1,1);
    layout_grid->addWidget(elem_Al,2,12,1,1);
    layout_grid->addWidget(elem_Si,2,13,1,1);
    layout_grid->addWidget(elem_P ,2,14,1,1);
    layout_grid->addWidget(elem_S ,2,15,1,1);
    layout_grid->addWidget(elem_Cl,2,16,1,1);
    layout_grid->addWidget(elem_Ar,2,17,1,1);


    layout_grid->addWidget(elem_K,3,0,1,1);
    layout_grid->addWidget(elem_Ca,3,1,1,1);
    layout_grid->addWidget(elem_Sc,3,2,1,1);
    layout_grid->addWidget(elem_Ti,3,3,1,1);
    layout_grid->addWidget(elem_V ,3,4,1,1);
    layout_grid->addWidget(elem_Cr,3,5,1,1);
    layout_grid->addWidget(elem_Mn,3,6,1,1);
    layout_grid->addWidget(elem_Fe,3,7,1,1);
    layout_grid->addWidget(elem_Co,3,8,1,1);
    layout_grid->addWidget(elem_Ni,3,9,1,1);
    layout_grid->addWidget(elem_Cu,3,10,1,1);
    layout_grid->addWidget(elem_Zn,3,11,1,1);
    layout_grid->addWidget(elem_Ga,3,12,1,1);
    layout_grid->addWidget(elem_Ge,3,13,1,1);
    layout_grid->addWidget(elem_As,3,14,1,1);
    layout_grid->addWidget(elem_Se,3,15,1,1);
    layout_grid->addWidget(elem_Br,3,16,1,1);
    layout_grid->addWidget(elem_Kr,3,17,1,1);

    layout_grid->addWidget(elem_Rb,4,0,1,1);
    layout_grid->addWidget(elem_Sr,4,1,1,1);
    layout_grid->addWidget(elem_Y ,4,2,1,1);
    layout_grid->addWidget(elem_Zr,4,3,1,1);
    layout_grid->addWidget(elem_Nb,4,4,1,1);
    layout_grid->addWidget(elem_Mo,4,5,1,1);
    layout_grid->addWidget(elem_Tc,4,6,1,1);
    layout_grid->addWidget(elem_Ru,4,7,1,1);
    layout_grid->addWidget(elem_Rh,4,8,1,1);
    layout_grid->addWidget(elem_Pd,4,9,1,1);
    layout_grid->addWidget(elem_Ag,4,10,1,1);
    layout_grid->addWidget(elem_Cd,4,11,1,1);
    layout_grid->addWidget(elem_In,4,12,1,1);
    layout_grid->addWidget(elem_Sn,4,13,1,1);
    layout_grid->addWidget(elem_Sb,4,14,1,1);
    layout_grid->addWidget(elem_Te,4,15,1,1);
    layout_grid->addWidget(elem_I ,4,16,1,1);
    layout_grid->addWidget(elem_Xe,4,17,1,1);

//
    layout_grid->addWidget(elem_Cs,5,0,1,1);
    layout_grid->addWidget(elem_Ba,5,1,1,1);
    layout_grid->addWidget(elem_Hf,5,3,1,1);
    layout_grid->addWidget(elem_Ta,5,4,1,1);
    layout_grid->addWidget(elem_W ,5,5,1,1);
    layout_grid->addWidget(elem_Re,5,6,1,1);
    layout_grid->addWidget(elem_Os,5,7,1,1);
    layout_grid->addWidget(elem_Ir,5,8,1,1);
    layout_grid->addWidget(elem_Pt,5,9,1,1);
    layout_grid->addWidget(elem_Au,5,10,1,1);
    layout_grid->addWidget(elem_Hg,5,11,1,1);
    layout_grid->addWidget(elem_Tl,5,12,1,1);
    layout_grid->addWidget(elem_Pb,5,13,1,1);
    layout_grid->addWidget(elem_Bi,5,14,1,1);
    layout_grid->addWidget(elem_Po,5,15,1,1);
    layout_grid->addWidget(elem_At,5,16,1,1);
    layout_grid->addWidget(elem_Rn,5,17,1,1);

    layout_grid->addWidget(elem_At,6,0,1,1);
    layout_grid->addWidget(elem_Rn,6,1,1,1);

    layout_grid->addWidget(elem_La,7,2,1,1);
    layout_grid->addWidget(elem_Ce,7,3,1,1);
    layout_grid->addWidget(elem_Pr,7,4,1,1);
    layout_grid->addWidget(elem_Nd,7,5,1,1);
    layout_grid->addWidget(elem_Pm,7,6,1,1);
    layout_grid->addWidget(elem_Sm,7,7,1,1);
    layout_grid->addWidget(elem_Eu,7,8,1,1);
    layout_grid->addWidget(elem_Gd,7,9,1,1);
    layout_grid->addWidget(elem_Tb,7,10,1,1);
    layout_grid->addWidget(elem_Dy,7,11,1,1);
    layout_grid->addWidget(elem_Ho,7,12,1,1);
    layout_grid->addWidget(elem_Er,7,13,1,1);
    layout_grid->addWidget(elem_Tm,7,14,1,1);
    layout_grid->addWidget(elem_Yb,7,15,1,1);
    layout_grid->addWidget(elem_Lu,7,16,1,1);

    layout_grid->addWidget(elem_Ac,8,2,1,1);
    layout_grid->addWidget(elem_Th,8,3,1,1);
    layout_grid->addWidget(elem_Pa,8,4,1,1);
    layout_grid->addWidget(elem_U ,8,5,1,1);
    layout_grid->addWidget(elem_Np,8,6,1,1);
    layout_grid->addWidget(elem_Pu,8,7,1,1);
    layout_grid->addWidget(elem_Am,8,8,1,1);
    layout_grid->addWidget(elem_Cm,8,9,1,1);
    layout_grid->addWidget(elem_Bk,8,10,1,1);
    layout_grid->addWidget(elem_Cf,8,11,1,1);
    layout_grid->addWidget(elem_Es,8,12,1,1);
    layout_grid->addWidget(elem_Fm,8,13,1,1);
    layout_grid->addWidget(elem_Md,8,14,1,1);
    layout_grid->addWidget(elem_No,8,15,1,1);
    layout_grid->addWidget(elem_Lr,8,16,1,1);
}

PeriodicTableWidget::~PeriodicTableWidget()
{
}

void PeriodicTableWidget::setAvailableElements(const QStringList &ava_elems)
{   
    QHashIterator<QString, QPushButton*> it(elems);
    while(it.hasNext())
    {
        it.next();
        QPushButton* but = it.value();
        QString elem = it.key();
        if (ava_elems.contains(elem))
        {
            but->setEnabled(true);
        }
        else
        {
            but->setEnabled(false);
        }
    }
}

void PeriodicTableWidget::setMode(PeriodicTableWidget::Mode mode)
{
    m_mode = mode;
    adjustMode();
}

PeriodicTableWidget::Mode PeriodicTableWidget::getMode()
{
    return m_mode;
}

void PeriodicTableWidget::buttonClicked()
{
    QPushButton *but = qobject_cast<QPushButton*>(sender());
    if (but)
    {
        emit elementButtonClicked(but);
    }
}

void PeriodicTableWidget::adjustMode()
{
    QHashIterator<QString, QPushButton*> it(elems);

    while(it.hasNext())
    {
        it.next();
        QPushButton* but = it.value();
        if (m_mode == Mode::SingleClickMode)
        {
            but->setCheckable(false);
            but->setStyleSheet("");
        }
        else
        {
            but->setCheckable(true);
            but->setStyleSheet(QString("QPushButton:checked{color: white; background-color: green; border: none;} QPushButton:pressed {background-color: green;}"));
        }
    }
}

QStringList PeriodicTableWidget::getSelectedElements() const
{
    QStringList res;
    QHashIterator<QString, QPushButton*> it(elems);
    while(it.hasNext())
    {
        it.next();
        QPushButton* but = it.value();
        QString elem = it.key();
        if (but->isChecked())
        {
            res.append(elem);
        }
    }
    res.sort();
    return res;
}

