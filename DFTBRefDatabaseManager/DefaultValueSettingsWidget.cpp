#include "DefaultValueSettingsWidget.h"

#include "ErepfitPotentialDefinitionWidget.h"
#include "ErepfitInputEnergyEquationDialog.h"
#include "ErepfitInputForceEquationDialog.h"
#include "ElectronicDefaultConfiningDefinitionDialog.h"
#include "ConfiningDefaultFittingSetDialog.h"
#include "ErepfitDefAdditionalEqDialog.h"
#include "PredefinedSetsEditWidget.h"
#include "ErepfitReactionDialog.h"

#include <QtWidgets>
#include <QGroupBox>

DefaultValueSettingsWidget::DefaultValueSettingsWidget(DFTBRefDatabase *database, QWidget *parent) :
    QWidget(parent), m_database(database)
{
    setupUI();
}

void DefaultValueSettingsWidget::erepfit_default_potential_def()
{
    ErepfitPotentialDefinitionWidget *w = new ErepfitPotentialDefinitionWidget(m_database, this);
    w->exec();
    delete w;
}

void DefaultValueSettingsWidget::erepfit_default_input_energy_equation()
{
    ErepfitInputEnergyEquationDialog *w = new ErepfitInputEnergyEquationDialog(m_database, this);
    w->exec();
    delete w;
}
void DefaultValueSettingsWidget::erepfit_default_input_force_equation()
{
    ErepfitInputForceEquationDialog *w = new ErepfitInputForceEquationDialog(m_database, this);
    w->exec();
    delete w;
}

void DefaultValueSettingsWidget::erepfit_default_input_reaction()
{
    ErepfitReactionDialog *w = new ErepfitReactionDialog(m_database, this);
    w->exec();
    delete w;
}

void DefaultValueSettingsWidget::erepfit_default_add_eq()
{
    ErepfitDefAdditionalEqDialog *w = new ErepfitDefAdditionalEqDialog(m_database, this);
    w->exec();
    delete w;
}

void DefaultValueSettingsWidget::confining_default_def()
{
    ElectronicDefaultConfiningDefinitionDialog *w = new ElectronicDefaultConfiningDefinitionDialog(m_database, this);
    w->exec();
    delete w;
}

void DefaultValueSettingsWidget::confining_default_fitting_def()
{
    ConfiningDefaultFittingSetDialog *w = new ConfiningDefaultFittingSetDialog(m_database, this);
    w->exec();
    delete w;
}

void DefaultValueSettingsWidget::predefined_sets_editor()
{
    PredefinedSetsEditWidget *w = new PredefinedSetsEditWidget(m_database, this);
    w->exec();
    delete w;
}

void DefaultValueSettingsWidget::setupUI()
{
    QVBoxLayout *box = new QVBoxLayout();
    this->setLayout(box);

    //Erepfit
    {
        QGroupBox *gbox = new QGroupBox();
        QVBoxLayout *vbox = new QVBoxLayout();
        gbox->setLayout(vbox);
        gbox->setTitle("Erepfit defaults");
        {
            QHBoxLayout *hbox = new QHBoxLayout();
            hbox->addWidget(new QLabel(tr("Repulsive potential definitions:")));
            QPushButton * butErepfitRepulsiveDefinition = new QPushButton(tr("Details..."));
            connect(butErepfitRepulsiveDefinition, SIGNAL(clicked()), this, SLOT(erepfit_default_potential_def()));
            hbox->addWidget(butErepfitRepulsiveDefinition);
            hbox->addStretch(5);
            vbox->addLayout(hbox);
        }

        {
            QHBoxLayout *hbox = new QHBoxLayout();
            hbox->addWidget(new QLabel(tr("Energy equations:")));
            QPushButton * butErepfitInputBase = new QPushButton(tr("Details..."));
            connect(butErepfitInputBase, SIGNAL(clicked()), this, SLOT(erepfit_default_input_energy_equation()));
            hbox->addWidget(butErepfitInputBase);
            hbox->addStretch(5);
            vbox->addLayout(hbox);
        }
        {
            QHBoxLayout *hbox = new QHBoxLayout();
            hbox->addWidget(new QLabel(tr("Force equations:")));
            QPushButton * butErepfitInputBase = new QPushButton(tr("Details..."));
            connect(butErepfitInputBase, SIGNAL(clicked()), this, SLOT(erepfit_default_input_force_equation()));
            hbox->addWidget(butErepfitInputBase);
            hbox->addStretch(5);
            vbox->addLayout(hbox);
        }

        {
            QHBoxLayout *hbox = new QHBoxLayout();
            hbox->addWidget(new QLabel(tr("Reaction equations:")));
            QPushButton * butErepfitInputBase = new QPushButton(tr("Details..."));
            connect(butErepfitInputBase, SIGNAL(clicked()), this, SLOT(erepfit_default_input_reaction()));
            hbox->addWidget(butErepfitInputBase);
            hbox->addStretch(5);
            vbox->addLayout(hbox);
        }

        {
            QHBoxLayout *hbox = new QHBoxLayout();
            hbox->addWidget(new QLabel(tr("Additional equations:")));
            QPushButton * butErepfitInputBase = new QPushButton(tr("Details..."));
            connect(butErepfitInputBase, SIGNAL(clicked()), this, SLOT(erepfit_default_add_eq()));
            hbox->addWidget(butErepfitInputBase);
            hbox->addStretch(5);
            vbox->addLayout(hbox);
        }
        box->addWidget(gbox);
    }

    //Electronic
    {
        QGroupBox *gbox = new QGroupBox();
        QVBoxLayout *vbox = new QVBoxLayout();
        gbox->setLayout(vbox);
        gbox->setTitle("Electronic");
        {
            QHBoxLayout *hbox = new QHBoxLayout();
            hbox->addWidget(new QLabel(tr("Confining potential definitions:")));
            QPushButton * button = new QPushButton(tr("Details..."));
            connect(button, SIGNAL(clicked()), this, SLOT(confining_default_def()));
            hbox->addWidget(button);
            hbox->addStretch(5);
            vbox->addLayout(hbox);
        }
        {
            QHBoxLayout *hbox = new QHBoxLayout();
            hbox->addWidget(new QLabel(tr("Confining potential fitting set:")));
            QPushButton * button = new QPushButton(tr("Details..."));
            connect(button, SIGNAL(clicked()), this, SLOT(confining_default_fitting_def()));
            hbox->addWidget(button);
            hbox->addStretch(5);
            vbox->addLayout(hbox);
        }

        box->addWidget(gbox);
    }

    //Sets
    {
        QGroupBox *gbox = new QGroupBox();
        QVBoxLayout *vbox = new QVBoxLayout();
        gbox->setLayout(vbox);
        gbox->setTitle("Predefined Sets");
        {
            QHBoxLayout *hbox = new QHBoxLayout();
            hbox->addWidget(new QLabel(tr("Manage sets:")));
            QPushButton * button = new QPushButton(tr("Details..."));
            connect(button, SIGNAL(clicked()), this, SLOT(predefined_sets_editor()));
            hbox->addWidget(button);
            hbox->addStretch(5);
            vbox->addLayout(hbox);
        }
        box->addWidget(gbox);
    }



    box->addStretch(5);

}
