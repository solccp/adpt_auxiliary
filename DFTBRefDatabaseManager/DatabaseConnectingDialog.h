#ifndef DATABASECONNECTINGDIALOG_H
#define DATABASECONNECTINGDIALOG_H

#include <QDialog>
#include <QComboBox>
#include <QLineEdit>
#include <QPushButton>

class DatabaseConnectingDialog : public QDialog
{
    Q_OBJECT
public:
    explicit DatabaseConnectingDialog(QWidget *parent = 0);




    struct Login
    {
        QString profile;
        QString server_addr;
        QString database_name;
        QString account;
        QString password;
        bool operator==(const Login& rhs)
        {
            return (profile==rhs.profile && server_addr==rhs.server_addr
                    && database_name==rhs.database_name && account==rhs.account
                    && password==rhs.password);
        }
     };
signals:

public slots:
    QString server_addr() const;
    QString database_name() const;
    QString account() const;
    QString password() const;
    QString profile() const;
private slots:
    void comboIndexChanged(int index);
    void butOkClicked();
    void butCleanHistroryClicked();

    void set_server_addr(const QString& value);
    void set_database_name(const QString& value);
    void set_account(const QString& value);
    void set_password(const QString& value);
    void set_profile(const QString& value);

private:
    Login current;
    QComboBox* m_combo_profile;
    QLineEdit* m_edit_server_addr;
    QLineEdit* m_edit_database_name;
    QLineEdit* m_edit_account;
    QLineEdit* m_edit_password;
    QPushButton* butOk;
    QPushButton* butCancel;
    QPushButton* butCleanHistrory;
    QList<Login> logins;

    void saveSettings();
    void loadSettings();
    void setupUI();
};

#endif // DATABASECONNECTINGDIALOG_H
