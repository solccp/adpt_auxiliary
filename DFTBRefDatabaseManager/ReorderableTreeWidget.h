#ifndef REORDERABLETREEWIDGET_H
#define REORDERABLETREEWIDGET_H

#include <QTreeWidget>

class ReorderableTreeWidget : public QTreeWidget
{
    Q_OBJECT
public:
    explicit ReorderableTreeWidget(QWidget *parent = 0);
signals:
    void itemReordered(QTreeWidgetItem* moved_item, QTreeWidgetItem* old_parent);
public slots:
    void setReorderable(bool value);
protected:
    void dropEvent(QDropEvent *event);
    void mousePressEvent(QMouseEvent *event);
private:
    QTreeWidgetItem* old_item = nullptr;
    QTreeWidgetItem* old_item_parent = nullptr;
};

#endif // REORDERABLETREEWIDGET_H
