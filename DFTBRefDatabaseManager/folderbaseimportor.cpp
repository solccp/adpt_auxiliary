#include "folderbaseimportor.h"

#include "folderbaseparser/tools.h"
#include "folderbaseparser/crystalParser.hpp"
#include "folderbaseparser/molecularParser.h"
#include "folderbaseparser/reactionparser.h"
#include "geometry.h"
#include "geometrycoordinationmodel.h"
#include "bandstructuredata.h"
#include "molecularfrequencydata.h"
#include "molecularatomizationenergydata.h"
#include "reactionenergydata.h"

#include "dftbrefdatabase.h"

#include <QDir>
#include <QStringList>
#include <QMutex>
#include <QUuid>

FolderbaseImporter::FolderbaseImporter(DFTBRefDatabase *database, QObject *parent) : QObject(parent)
{
    m_database = database;
}

FolderbaseImporter::~FolderbaseImporter()
{
    for(int i = 0; i< m_geometryData.size(); ++i)
    {
        delete m_geometryData[i];
    }
    m_geometryData.clear();
}
QString FolderbaseImporter::path() const
{
    return m_path;
}

void FolderbaseImporter::setPath(const QString &path)
{
    m_path = path;
}

void FolderbaseImporter::setObserver(FolderbaseObserver *observer)
{
    m_observer.reset(observer);
}

bool FolderbaseImporter::parseGeometryData(const QDir& root, const QString& path, Geometry* geom)
{
    QDir pathDir(path);

    QString uuid_str = loadFile(root, pathDir.absoluteFilePath("UUID")).first();
    QUuid uuid(uuid_str);
    if (uuid.isNull())
    {
        emit messageUpdated("UUID is not found or it is empty or it's not a valid one.");
        return false;
    }
    geom->setUUID(uuid.toString());


    QString name = loadFile(root, pathDir.absoluteFilePath("NAME")).first();
    if (name.isEmpty())
    {
        emit messageUpdated("NAME is not found or it is empty.");
        return false;
    }
    geom->setName(name);

    QString type = loadFile(root, pathDir.absoluteFilePath("TYPE")).first();
    if (type.isEmpty() || (type.toLower() != "crystal" && type.toLower() != "cluster" && type.toLower() != "molecule"))
    {
        emit messageUpdated("TYPE is not found or it does not contain cluster(molecule) or crystal");
        return false;
    }
    if (type.toLower() == "crystal")
    {
        geom->setPBC(true);
    }
    else
    {
        geom->setPBC(false);
    }

    QString method = loadFile(root, pathDir.absoluteFilePath("METHOD")).first();

    if (method.isEmpty())
    {
        qDebug() << "METHOD is not found or it is empty.";
        return false;
    }
    geom->setMethod(method.toUpper());

    QString basis = loadFile(root, pathDir.absoluteFilePath("BASIS")).first();
    if (basis.isEmpty())
    {
//        qDebug() << "BASIS file is empty, skipped.";
    }
    else
    {
        geom->setBasis(basis.toUpper());
    }


    QString isstationary = loadFile(root, pathDir.absoluteFilePath("STATIONARY")).first();
    if (isstationary.isEmpty())
    {
        emit messageUpdated("STATIONARY is not found or it is empty.");
        return false;
    }
    geom->setIsStationary(isstationary == "1" ? true : false);


    if (!geom->PBC())
    {

        QString charge_str = loadFile(root, pathDir.absoluteFilePath("CHARGE")).first();
        bool ok;
        int charge = charge_str.toInt(&ok);
        geom->setCharge(0);
        if (ok)
        {
            geom->setCharge(charge);
        }
        else
        {
//            qDebug() << "CHARGE file is empty or non-integer, assumed 0.";
        }
    }

    QString comment = loadFile(root, pathDir.absoluteFilePath("COMMENT")).join("\n");
    if (comment.isEmpty())
    {
//        qDebug() << "COMMENT file is empty, skipped.";
    }
    else
    {
        geom->setComment(comment);
    }

    QString citation = loadFile(root, pathDir.absoluteFilePath("CITATION")).join("\n");
    if (citation.isEmpty())
    {
//        qDebug() << "CITATION file is empty, skipped.";
    }
    else
    {
        geom->setCitation(citation);
    }

    GeometryCoordinationModel *model = new GeometryCoordinationModel(geom);
    QTextStream stream;
    QString dummy;
    QFile file(pathDir.absoluteFilePath("GEOM"));
    if (openStream(stream, root, file))
    {
        if (geom->PBC())
        {
            qDebug() << "Loading GEOM for crystal from HSD format...";
            if (model->loadHSDGeometry(stream))
            {
                qDebug() << "GEOM loaded.";
            }
            else
            {
                stream.seek(0);
                qDebug() << "Loading GEOM for crystal from POSCAR format...";
                if (model->loadPOSCAR(stream, dummy))
                {
                    qDebug() << "GEOM loaded.";
                }
                else
                {
                    stream.seek(0);
                    qDebug() << "Loading GEOM for crystal from gen format...";
                    if (model->loadGenGeometry(stream))
                    {
                        qDebug() << "GEOM loaded.";
                    }
                    else
                    {
                        qDebug() << "GEOM parse error";
                        return false;
                    }
                }
            }
        }
        else
        {
            qDebug() << "Loading GEOM for molecule from XYZ format...";
            if (model->loadXYZGeometry(stream))
            {
                qDebug() << "GEOM loaded.";
            }
            else
            {
                stream.seek(0);
                qDebug() << "Loading GEOM for molecule from XYZ format...";
                if (model->loadGenGeometry(stream))
                {
                    qDebug() << "GEOM loaded.";
                }
                else
                {
                    qDebug() << "GEOM parse error";
                    return false;
                }
            }
        }
    }

    if (geom->PBC())
    {
        QString list = loadFile(root, pathDir.absoluteFilePath("SAMPLING")).first();
        if (list.isEmpty())
        {
            qDebug() << "SAMPLING parse error";
            return false;
        }
        QStringList arr = list.split(" ", QString::SkipEmptyParts);
        if (arr.size() == 6)
        {
            iVector3 kpoints = {{32,32,32}};
            dVector3 kpoint_shifts = {{0.5, 0.5, 0.5}};
            for(int i=0; i<3; ++i)
            {
                kpoints[i] = arr[i].toInt();
            }
            for(int i=0; i<3; ++i)
            {
                kpoint_shifts[i] = arr[i+3].toDouble();
            }

            geom->setKpoints(kpoints);
            geom->setKpoint_shifts(kpoint_shifts);
        }
    }

//    if (geom->PBC())
//    {

//        if (parseGeometryLattice(root, pathDir.absoluteFilePath("LATTICE"), geom))
//        {
////            qDebug() << "LATTICE loaded";
//        }
//        else
//        {
//            qDebug() << "LATTICE parse error";
//            return false;
//        }
//    }


    return true;
}

//bool FolderbaseImporter::parseGeometryLattice(const QDir& root, const QString& filename, Geometry* geom)
//{
//    QStringList lines = loadFile(root, filename);
//    GenericVector<3, double> latticeParameter;
//    QGenericMatrix<3, 3, qint32> latticeMask;
//    QGenericMatrix<3, 3, double> latticeVector;
//    QStringList LP_array;
//    LP_array.clear();
//    LP_array = lines.at(0).split(" ", QString::SkipEmptyParts);


//    int maxMask = 0;
//    for(int i=0; i<3; ++i)
//    {
//        QStringList array = lines.at(1+i).split(" ",QString::SkipEmptyParts);
//        if (array.size() <3)
//        {
//            qDebug() << "Parse Error: Line " << i+1 ;
//            return false;
//        }
//        bool ok;
//        for(int j=0; j<3; ++j)
//        {
//            int value = array.at(j).toInt(&ok);
//            if (!ok)
//            {
//                qDebug() << "Parse Error: Element # " << j+1 << " of the line " << i+1 ;
//                return false;
//            }
//            if (value > maxMask)
//            {
//                maxMask = value;
//            }
//            latticeMask(i,j) = value;
//        }
//    }

//    for(int i=0; i<maxMask;++i)
//    {
//        bool ok;
//        double value = LP_array.at(i).toDouble(&ok);
//        if (!ok)
//        {
//            qDebug() << "Parse Error: Element " << i+1 << " of the lattice parameter";
//            return false;
//        }
//        latticeParameter(i) = value;
//    }


//    for(int i=0; i<3; ++i)
//    {
//        QStringList array = lines.at(4+i).split(" ",QString::SkipEmptyParts);
//        if (array.size() <3)
//        {
//            qDebug() << "Parse Error: Line " << i+1 ;
//            return false;
//        }
//        bool ok;
//        for(int j=0; j<3; ++j)
//        {
//            double value = array.at(j).toDouble(&ok);
//            if (!ok)
//            {
//                qDebug() << "Parse Error: Element # " << j+1 << " of the line " << i+1 ;
//                return false;
//            }
//            latticeVector(i,j) = value;
//        }
//    }
///// Todo: fixit
////    geom->setLatticeParameter(latticeParameter);
////    geom->setLatticeMask(latticeMask);
////    geom->setLatticeVector(latticeVector);
//    return false;
//}


QList<Geometry *> FolderbaseImporter::geometryData() const
{
    return m_geometryData;
}

QList<BandstructureData *> FolderbaseImporter::bandstructureData() const
{
    return m_bandstructureData;
}

QList<MolecularFrequencyData *> FolderbaseImporter::frequencyData() const
{
    return m_frequencyData;
}

QList<MolecularAtomizationEnergyData *> FolderbaseImporter::atomizationEnergyData() const
{
    return m_atomizationEnergyData;
}

QList<ReactionEnergyData *> FolderbaseImporter::reactionEnergyData() const
{
    return m_reactionEnergyData;
}

void FolderbaseImporter::scanGeometry()
{
    QDir root = QDir(m_path);
    if (!root.exists())
    {
        emit messageUpdated(root.absolutePath() + " does not exist, stopped...");
        return;
    }

    emit messageUpdated("Scaning reference data folder..." + root.absolutePath()) ;

    m_geom_name_uuid_crystal.clear();
    m_geom_name_uuid_molecule.clear();
    m_geom_uuid.clear();

    QList<QString> db_uuids = m_database->getGeometryUUID();
    m_geom_uuid = db_uuids.toSet();

    emit messageUpdated("Searching for subfolder named GEOMETRY...");

    QFileInfo geomInfo = root.absoluteFilePath("GEOMETRY");
    if (!geomInfo.isDir())
    {
        emit messageUpdated("GEOMETRY folder is not found.");
    }
    else
    {
//        emit messageUpdated("GEOMETRY folder is found.");
        QDir geomDir = QDir(geomInfo.absoluteFilePath());
        QFileInfoList infolist = geomDir.entryInfoList(QDir::Dirs|QDir::Readable|QDir::NoDotAndDotDot);

        int size = infolist.size();
        if (!m_observer.isNull())
        {
            m_observer->setNewParseRange(0, size);
            m_observer->newText("Parsing Geometry...");
        }
        for(int i=0; i<size; ++i)
        {
            if (!m_observer.isNull())
            {
                if (m_observer->wasCanceled())
                {
                    break;
                }
                m_observer->progressUpdated(i);
            }


            QString folderName = geomDir.relativeFilePath(infolist.at(i).absoluteFilePath());
            emit messageUpdated("Loading Geometry in folder " + geomDir.relativeFilePath(infolist.at(i).absoluteFilePath()) + "...");
            qDebug() << "Loading Geometry in folder " << geomDir.relativeFilePath(infolist.at(i).absoluteFilePath()) << "...";
            Geometry* geom = new Geometry(this);

            if (parseGeometryData(QDir(infolist.at(i).absoluteFilePath()),infolist.at(i).absoluteFilePath(), geom))
            {
//                emit messageUpdated("Geometry loaded successfully");

                if (m_geom_uuid.contains(geom->UUID()))
                {
                    continue;
                }

                m_geometryData.append(geom);

                if (geom->PBC())
                {
                    m_geom_name_uuid_crystal.insert(folderName, geom->UUID());
                    m_geom_uuid.insert(geom->UUID());
                }
                else
                {
                    m_geom_name_uuid_molecule.insert(folderName, geom->UUID());
                    m_geom_uuid.insert(geom->UUID());
                }
            }
            else
            {
                emit messageUpdated("Geometry loading failed, skipped.");
            }
        }
    }

}

bool FolderbaseImporter::scanBandstructure()
{
    QDir root = QDir(m_path);
    qDebug() << "Searching for subfolder named BANDSTRUCTURE...";

    QFileInfo geomInfo = root.absoluteFilePath("BANDSTRUCTURE");
    if (!geomInfo.isDir())
    {
        qDebug() << "BANDSTRUCTURE folder is not found.";
        return false;
    }
    else
    {
//        qDebug() << "BANDSTRUCTURE folder is found.";
        QDir geomDir = QDir(geomInfo.absoluteFilePath());
        QFileInfoList infolist = geomDir.entryInfoList(QDir::Dirs|QDir::Readable|QDir::NoDotAndDotDot);


        if (!m_observer.isNull())
        {
            m_observer->setNewParseRange(0, infolist.size());
            m_observer->newText("Parsing Bandstructure...");
        }

        m_bandstructureData.clear();

        for(int i=0; i<infolist.size();++i)
        {
            if (!m_observer.isNull())
            {
                if (m_observer->wasCanceled())
                {
                    return false;
                }
                m_observer->progressUpdated(i);
            }

            qDebug() << "Loading BandStructure in folder " << geomDir.relativeFilePath(infolist.at(i).absoluteFilePath()) << "...";
            BandstructureData* data = new BandstructureData(this);
            if (ParseBandStructure(m_geom_name_uuid_crystal, QDir(infolist.at(i).absoluteFilePath()),infolist.at(i).absoluteFilePath(), data))
            {
//                qDebug() << "BandStructure loaded successfully";
                m_bandstructureData.append(data);
            }
            else
            {
                qDebug() << "BandStructure loading failed, skipped.";
            }
        }
    }
    return true;
}



bool FolderbaseImporter::scanMolecularFrequency()
{
    QDir root = QDir(m_path);
    const QString keyword = "FREQ";
    const QString des = "Frequency";
    qDebug() << "Searching for subfolder named" << keyword << "...";

    QFileInfo geomInfo = root.absoluteFilePath(keyword);
    if (!geomInfo.isDir())
    {
        qDebug() << keyword << "folder is not found.";
        return false;
    }
    else
    {
//        qDebug() << keyword << "folder is found.";
        QDir geomDir = QDir(geomInfo.absoluteFilePath());
        QFileInfoList infolist = geomDir.entryInfoList(QDir::Dirs|QDir::Readable|QDir::NoDotAndDotDot);

        if (!m_observer.isNull())
        {
            m_observer->setNewParseRange(0, infolist.size());
            m_observer->newText("Parsing MolecularFrequency...");
        }
        m_frequencyData.clear();
        for(int i=0; i<infolist.size();++i)
        {
            if (!m_observer.isNull())
            {
                if (m_observer->wasCanceled())
                {
                    return false;
                }
                m_observer->progressUpdated(i);
            }
            qDebug() << "Loading" << des << "in folder " << geomDir.relativeFilePath(infolist.at(i).absoluteFilePath()) << "...";
            MolecularFrequencyData* data = new MolecularFrequencyData();
            if (ParseMolecularFrequency(m_geom_name_uuid_molecule, QDir(infolist.at(i).absoluteFilePath()),infolist.at(i).absoluteFilePath(), data))
            {
//                qDebug() << des << "loaded successfully";
                m_frequencyData.append(data);
            }
            else
            {
                qDebug() << des << "loading failed, skipped.";
            }
        }
    }
    return true;
}

void FolderbaseImporter::scanMolecularAtomizationEnergy()
{
    QDir root = QDir(m_path);
    const QString keyword = "AE";
    const QString des = "AtomizationEnergy";
    qDebug() << "Searching for subfolder named" << keyword << "...";

    QFileInfo geomInfo = root.absoluteFilePath(keyword);
    if (!geomInfo.isDir())
    {
        qDebug() << keyword << "folder is not found.";
    }
    else
    {
        qDebug() << keyword << "folder is found.";
        QDir geomDir = QDir(geomInfo.absoluteFilePath());
        QFileInfoList infolist = geomDir.entryInfoList(QDir::Dirs|QDir::Readable|QDir::NoDotAndDotDot);

        if (!m_observer.isNull())
        {
            m_observer->setNewParseRange(0, infolist.size());
            m_observer->newText("Parsing MolecularAtomizationEnergy...");
        }
        m_atomizationEnergyData.clear();
        for(int i=0; i<infolist.size();++i)
        {
            if (!m_observer.isNull())
            {
                if (m_observer->wasCanceled())
                {
                    break;
                }
                m_observer->progressUpdated(i);
            }
            qDebug() << "Loading" << des << "in folder " << geomDir.relativeFilePath(infolist.at(i).absoluteFilePath()) << "...";
            MolecularAtomizationEnergyData* data = new MolecularAtomizationEnergyData();
            if (ParseMolecularAtomizationEnergy(m_geom_name_uuid_molecule, QDir(infolist.at(i).absoluteFilePath()),infolist.at(i).absoluteFilePath(), data))
            {
                m_atomizationEnergyData.append(data);
            }
            else
            {
                qDebug() << des << "loading failed, skipped.";
            }
        }
    }
}

void FolderbaseImporter::scanReactionEnergy()
{
    QDir root = QDir(m_path);
    const QString keyword = "REACTION";
    const QString des = "ReactionEnergy";
    qDebug() << "Searching for subfolder named" << keyword << "...";

    QFileInfo geomInfo = root.absoluteFilePath(keyword);
    if (!geomInfo.isDir())
    {
        qDebug() << keyword << "folder is not found.";
    }
    else
    {
        qDebug() << keyword << "folder is found.";
        QDir geomDir = QDir(geomInfo.absoluteFilePath());
        QFileInfoList infolist = geomDir.entryInfoList(QDir::Dirs|QDir::Readable|QDir::NoDotAndDotDot);

        if (!m_observer.isNull())
        {
            m_observer->setNewParseRange(0, infolist.size());
            m_observer->newText("Parsing ReactionEnergy...");
        }
        QMap<QString, QString> m_geom_name_uuid = m_geom_name_uuid_crystal;
        m_geom_name_uuid.unite(m_geom_name_uuid_molecule);
        for(int i=0; i<infolist.size();++i)
        {
            if (!m_observer.isNull())
            {
                if (m_observer->wasCanceled())
                {
                    break;
                }
                m_observer->progressUpdated(i);
            }
            qDebug() << "Loading" << des << "in folder " << geomDir.relativeFilePath(infolist.at(i).absoluteFilePath()) << "...";
            ReactionEnergyData* data = new ReactionEnergyData();
            if (ParseReactionEnergy(m_geom_name_uuid, m_geom_uuid, QDir(infolist.at(i).absoluteFilePath()),infolist.at(i).absoluteFilePath(), data))
            {
//                qDebug() << des << "loaded successfully";
                m_reactionEnergyData.append(data);
            }
            else
            {
                qDebug() << des << "loading failed, skipped.";
            }
        }
    }
}

