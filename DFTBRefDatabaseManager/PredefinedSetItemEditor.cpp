#include "PredefinedSetItemEditor.h"

#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QTextEdit>
#include <QDialogButtonBox>

PredefinedSetItemEditor::PredefinedSetItemEditor(QWidget *parent) :
    QDialog(parent)
{
    QGridLayout *layout = new QGridLayout;
    this->setLayout(layout);
    layout->addWidget(new QLabel(tr("Name:")),0,0,1,1);
    m_editName = new QLineEdit();
    layout->addWidget(m_editName, 0, 1, 1, 3);

    layout->addWidget(new QLabel(tr("Description:")), 1,0,1,1);
    m_editDesc = new QTextEdit();
    layout->addWidget(m_editDesc, 1, 1, 4, 3);

    QDialogButtonBox *but = new QDialogButtonBox(this);
    but->setStandardButtons(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);

    layout->addWidget(but, 5, 0, 1,4);

    connect(but, SIGNAL(accepted()), this, SLOT(varify()));
    connect(but, SIGNAL(rejected()), this, SLOT(close()));

    resize(600,400);

}

void PredefinedSetItemEditor::setName(const QString &name)
{
    m_editName->setText(name);
}

QString PredefinedSetItemEditor::name()
{
    return m_editName->text();
}

void PredefinedSetItemEditor::setDesc(const QString &Desc)
{
    m_editDesc->setText(Desc);
}

QString PredefinedSetItemEditor::desc()
{
    return m_editDesc->toPlainText();
}

bool PredefinedSetItemEditor::varify()
{
    if (m_editName->text().trimmed().isEmpty())
    {
        return false;
    }
    accept();
    return true;
}
