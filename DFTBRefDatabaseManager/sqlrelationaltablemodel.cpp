#include "sqlrelationaltablemodel.h"

#include <QSqlRecord>
#include <QSqlDriver>
#include <QSqlField>
#include <QSize>
#include <QDebug>

class QRelatedTableModel;



class QRelatedTableModel : public QSqlTableModel
{
public:
    QRelatedTableModel(QRelation *rel, QObject *parent = 0, QSqlDatabase db = QSqlDatabase());
    bool select();
private:
    bool firstSelect;
    QRelation *relation;
};
/*
    A QRelation must be initialized before it is considered valid.
    Note: population of the model and dictionary are kept separate
          from initialization, and are populated on an as needed basis.
*/
void QRelation::init(QSqlTableModel *parent, const QSqlRelation &relation)
{
    Q_ASSERT(parent != NULL);
    m_parent = parent;
    rel = relation;
}

void QRelation::populateModel()
{
    if (!isValid())
        return;
    Q_ASSERT(m_parent != NULL);

    if (!model) {
        model = new QRelatedTableModel(this, m_parent, m_parent->database());
        model->setTable(rel.tableName());
        model->select();
    }
}

bool QRelation::isDictionaryInitialized()
{
    return m_dictInitialized;
}

void QRelation::populateDictionary()
{
    if (!isValid())
        return;

    if (model ==  NULL)
        populateModel();

    QSqlRecord record;
    QString indexColumn;
    QString displayColumn;
    for (int i=0; i < model->rowCount(); ++i) {
        record = model->record(i);

        indexColumn = rel.indexColumn();
        if (m_parent->database().driver()->isIdentifierEscaped(indexColumn, QSqlDriver::FieldName))
            indexColumn = m_parent->database().driver()->stripDelimiters(indexColumn, QSqlDriver::FieldName);

        displayColumn = rel.displayColumn();
        if (m_parent->database().driver()->isIdentifierEscaped(displayColumn, QSqlDriver::FieldName))
            displayColumn = m_parent->database().driver()->stripDelimiters(displayColumn, QSqlDriver::FieldName);

        dictionary[record.field(indexColumn).value().toString()] =
            record.field(displayColumn).value();
    }
    m_dictInitialized = true;
}

void QRelation::clearDictionary()
{
    dictionary.clear();
    m_dictInitialized = false;
}

void QRelation::clear()
{
    delete model;
    model = 0;
    clearDictionary();
}

bool QRelation::isValid()
{
    return (rel.isValid() && m_parent != NULL);
}



QRelatedTableModel::QRelatedTableModel(QRelation *rel, QObject *parent, QSqlDatabase db) :
    QSqlTableModel(parent, db), firstSelect(true), relation(rel)
{
}

bool QRelatedTableModel::select()
{
    if (firstSelect)
    {
        firstSelect = false;
        return QSqlTableModel::select();
    }
    relation->clearDictionary();
    bool res = QSqlTableModel::select();
    if (res)
    {
        relation->populateDictionary();
    }
    return res;
}


SqlRelationalTableModel::SqlRelationalTableModel(QSqlDatabase sqldatabase, QObject *parent) :
    QSqlTableModel(parent, sqldatabase)
{
    connect(this, SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(somethingChanged(QModelIndex,QModelIndex)));
}

void SqlRelationalTableModel::setRelation(int column, const QSqlRelation &relation)
{
    if (column < 0)
        return;
    if (relations.size() <= column)
        relations.resize(column + 1);
    relations[column].init(this, relation);

}

void SqlRelationalTableModel::setLinkRelation(int column, int sourceColumn, const QSqlRelation &relation)
{
    if (column < 0)
        return;
    if (linkrelations.size() <= column)
        linkrelations.resize(column + 1);
    linkrelations[column].first.init(this, relation);
    linkrelations[column].second = sourceColumn;
}

QAbstractItemModel *SqlRelationalTableModel::relationModel(int index) const
{
    if (index>= 0 && index < relations.count() && relations.value(index).isValid())
    {
        QRelation &relation = relations[index];
        relation.populateModel();
        return relation.model;
    }
    return nullptr;
}

void SqlRelationalTableModel::clearDirty()
{
    m_dirtyRows.clear();
}
int SqlRelationalTableModel::checkableColumnIndex() const
{
    return m_checkableColumnIndex;
}

void SqlRelationalTableModel::setCheckableColumnIndex(int checkableColumnIndex)
{
    m_checkableColumnIndex = checkableColumnIndex;
}


void SqlRelationalTableModel::somethingChanged(const QModelIndex topLeft, const QModelIndex &bottomRight)
{
    for(int iRow = topLeft.row(); iRow <= bottomRight.row();++iRow)
    {
        int iCol = topLeft.column();
        QModelIndex l_index = this->index(iRow, iCol);
        if (this->isDirty(l_index))
        {
            m_dirtyRows.insert(iRow);
        }
        else
        {
            m_dirtyRows.remove(iRow);
        }
    }
    emit headerDataChanged(Qt::Vertical, topLeft.row(), bottomRight.row());
}


QVariant SqlRelationalTableModel::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole && index.column() >= 0 && index.column() < linkrelations.count() &&
            linkrelations.value(index.column()).first.isValid())
    {
        QRelation &relation = linkrelations[index.column()].first;
        if (!relation.isDictionaryInitialized())
        {
            relation.populateDictionary();
        }

        QModelIndex newIndex = this->index(index.row(),linkrelations[index.column()].second) ;
        QVariant v = QSqlTableModel::data(newIndex, role);

        if (v.isValid())
        {
           return relation.dictionary[v.toString()];
        }
    }
    else if (role == Qt::DisplayRole && index.column() >= 0 && index.column() < relations.count() &&
            relations.value(index.column()).isValid())
    {
        QRelation &relation = relations[index.column()];
        if (!relation.isDictionaryInitialized())
        {
            relation.populateDictionary();
        }

        QVariant v = QSqlTableModel::data(index, role);

        if (v.isValid())
        {
           return relation.dictionary[v.toString()];
        }
    }
    else if (role == Qt::CheckStateRole && index.column() == m_checkableColumnIndex)
    {
        QString key = data(index, Qt::EditRole).toString();
        if (m_checkedItems.contains(key))
        {
             return Qt::Checked;
        }
        else
        {
            return Qt::Unchecked;
        }
    }
    return QSqlTableModel::data(index, role);
}


QVariant SqlRelationalTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Vertical && role == Qt::DisplayRole)
    {
        if (m_dirtyRows.contains(section))
        {
            return "*";
        }
    }
    else if (orientation == Qt::Horizontal && section == m_checkableColumnIndex)
    {
        if (role == Qt::DisplayRole)
        {
            return "Select";
        }
    }

    return QSqlTableModel::headerData(section, orientation, role);
}


Qt::ItemFlags SqlRelationalTableModel::flags(const QModelIndex &index) const
{
    if (index.column() >= 0 && index.column() < linkrelations.count() &&
            linkrelations.value(index.column()).first.isValid())
    {
        Qt::ItemFlags flags = QSqlTableModel::flags(this->index(index.row(), linkrelations.value(index.column()).second));
        return flags;
    }

    Qt::ItemFlags flags = QSqlTableModel::flags(index);
    if (index.column() == m_checkableColumnIndex)
    {
        flags |= Qt::ItemIsUserCheckable | Qt::ItemIsEditable | Qt::ItemIsEnabled;
    }
    return flags;
}


bool SqlRelationalTableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (role == Qt::CheckStateRole && index.column() == m_checkableColumnIndex)
    {
        QString key = data(index, Qt::EditRole).toString();
        if (value == Qt::Checked)
        {
            m_checkedItems.insert(key);
        }
        else
        {
            m_checkedItems.remove(key);
        }
        return true;
    }
    return QSqlTableModel::setData(index, value, role);
}


int SqlRelationalTableModel::columnCount(const QModelIndex &parent) const
{
    return qMax(linkrelations.size(), QSqlTableModel::columnCount(parent));
}
