#include "ConfiningDefaultFittingSetDialog.h"

#include "atomicproperties.h"

#include <QMessageBox>
#include <QtWidgets>
#include "sqlrelationaltablemodel.h"
#include <QSqlError>

#include <QSortFilterProxyModel>
#include "CheckableSqlRelationalTableModel.h"
#include "dftbrefdatabase.h"

ConfiningDefaultFittingSetDialog::ConfiningDefaultFittingSetDialog(DFTBRefDatabase *database, QWidget *parent) :
    QDialog(parent)
{
    m_database = database;
    setupUI();
}

void ConfiningDefaultFittingSetDialog::revert()
{
    m_model->revertAll();
}

bool ConfiningDefaultFittingSetDialog::commit()
{
    m_model->database().transaction();
    if(m_model->submitAll())
    {
        m_model->database().commit();
    }
    else
    {
        m_model->database().rollback();
        QMessageBox qbox2(this);
        qbox2.setText("Failed to write data into database:" + m_model->lastError().text());
        qbox2.setStandardButtons(QMessageBox::Ok);
        qbox2.setIcon(QMessageBox::Critical);
        qbox2.exec();
        return false;
    }
    return true;
}

void ConfiningDefaultFittingSetDialog::element1Changed(const QString &elem)
{
    m_elem1 = elem;
    updateModelFilter();
}

void ConfiningDefaultFittingSetDialog::element2Changed(const QString &elem)
{
    m_elem2 = elem;
    updateModelFilter();
}

void ConfiningDefaultFittingSetDialog::updateModelFilter()
{
    if (!m_proxyModel)
        return;

    QString regex;
    if (m_elem1 == m_elem2 )
    {
       regex = QString("^\\[%1\\]$").arg(m_elem1);
    }
    else
    {
        regex = QString("(.*\\[%1\\].*\\[%2\\].*)|(.*\\[%2\\].*\\[%1\\].*)").arg(m_elem1).arg(m_elem2);
    }
    m_proxyModel->setFilterRegExp(QRegExp(regex));
    m_proxyModel->setFilterKeyColumn(m_filter_column);
    m_tableView->resizeColumnsToContents();
    m_tableView->setColumnWidth(1,5);
    m_tableView->sortByColumn(2, Qt::AscendingOrder);

}

void ConfiningDefaultFittingSetDialog::cellClicked(const QModelIndex &index)
{
    if (!index.isValid())
    {
        return;
    }
    if (index.column() != 1)
    {
        return ;
    }


    QVariant ala = m_proxyModel->data(index, Qt::EditRole);
    if (ala.toBool())
    {
        m_proxyModel->setData(index, 0, Qt::EditRole);
    }
    else
    {
        m_proxyModel->setData(index, 1, Qt::EditRole);
    }
}



void ConfiningDefaultFittingSetDialog::setupUI()
{
    m_tableView = new QTableView(this);
    m_tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_tableView->setAlternatingRowColors(true);


    this->resize(800,600);


    QVBoxLayout *layout = new QVBoxLayout(this);
    this->setLayout(layout);


    QHBoxLayout *hbox = new QHBoxLayout();

    QPushButton* butCommit = new QPushButton(tr("Commit"));
    connect(butCommit, SIGNAL(clicked()), this, SLOT(commitBut()));
    hbox->addWidget(butCommit);

    QPushButton* butRevert = new QPushButton(tr("Revert"));
    connect(butRevert, SIGNAL(clicked()), this, SLOT(revert()));
    hbox->addWidget(butRevert);
    hbox->addStretch(5);

    layout->addLayout(hbox);

    hbox = new QHBoxLayout();
    hbox->addWidget(new QLabel(tr("Potential:")));
    QComboBox *elem1 = new QComboBox();
    QComboBox *elem2 = new QComboBox();
    elem1->setEditable(false);
    elem2->setEditable(false);
    auto allsymbol = ADPT::AtomicProperties::allSymbols().toList();
    elem1->addItems(allsymbol);
    elem2->addItems(allsymbol);

    elem1->setCurrentText(m_elem1);
    elem2->setCurrentText(m_elem2);

    connect(elem1, SIGNAL(currentIndexChanged(QString)), this, SLOT(element1Changed(QString)));
    connect(elem2, SIGNAL(currentIndexChanged(QString)), this, SLOT(element2Changed(QString)));


    m_tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);


    hbox->addWidget(elem1);
    hbox->addWidget(elem2);
    hbox->addStretch(5);

    layout->addLayout(hbox);



    layout->addWidget(m_tableView);


    QPushButton* butClose = new QPushButton(tr("Close"));
    butClose->setDefault(true);
    connect(butClose, SIGNAL(clicked()), this, SLOT(close()));


    hbox = new QHBoxLayout();

    hbox->addStretch(5);
    hbox->addWidget(butClose);
    layout->addLayout(hbox);





    m_model = new CheckableSqlRelationalTableModel(m_database->database(), this);
    m_model->setTable("ElectronicFittingDefaultSelection");
    m_model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    m_model->select();

    m_model->setCheckableColumn(1);
    m_model->setHeaderData(1, Qt::Horizontal, "");

    m_proxyModel = new QSortFilterProxyModel(this);
    m_proxyModel->setSourceModel(m_model);



    m_filter_column = m_model->fieldIndex("elements");
    if (m_filter_column == -1)
    {
        m_filter_column = m_model->fieldIndex("Elements");
    }


    m_tableView->setModel(m_proxyModel);

    m_tableView->hideColumn(m_filter_column);
    m_tableView->hideColumn(0);

    if (m_tableView->horizontalHeader()->count() >1)
    {
        m_tableView->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Fixed);
    }
    m_tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

    updateModelFilter();


    if (!m_database->canWrite())
    {
        butRevert->setEnabled(false);
        butCommit->setEnabled(false);
        m_tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    }
    else
    {
        connect(m_tableView, SIGNAL(clicked(QModelIndex)), this, SLOT(cellClicked(QModelIndex)));
    }
}

void ConfiningDefaultFittingSetDialog::closeEvent(QCloseEvent *event)
{
    if (m_model->isDirty())
    {
        QMessageBox qbox(this);
        qbox.setText("The changes have not been updated in the database.\n"
                     "Do you want to update them?");
        qbox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        qbox.setIcon(QMessageBox::Question);
        int ret = qbox.exec();
        if (ret == QMessageBox::Yes)
        {
            bool succ = commit();
            if (!succ)
            {
                event->ignore();
                return;
            }
        }
        else
        {
            event->accept();
        }
    }
}

void ConfiningDefaultFittingSetDialog::commitBut()
{
    if (m_model->isDirty() && commit())
    {
        QMessageBox qbox2(this);
        qbox2.setText("Data written into database.");
        qbox2.setStandardButtons(QMessageBox::Ok);
        qbox2.setIcon(QMessageBox::Information);
        qbox2.exec();
        m_model->clearDirty();
    }
}
