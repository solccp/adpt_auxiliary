#ifndef DEFAULTVALUESETTINGSWIDGET_H
#define DEFAULTVALUESETTINGSWIDGET_H

#include <QWidget>

class DFTBRefDatabase;
class DefaultValueSettingsWidget : public QWidget
{
    Q_OBJECT
public:
    DefaultValueSettingsWidget(DFTBRefDatabase *database, QWidget *parent = 0);

signals:

public slots:
private slots:
    void erepfit_default_potential_def();
    void erepfit_default_input_energy_equation();
    void erepfit_default_input_force_equation();
    void erepfit_default_input_reaction();
    void erepfit_default_add_eq();
    void confining_default_def();
    void confining_default_fitting_def();
    void predefined_sets_editor();

private:
    void setupUI();
    DFTBRefDatabase* m_database;
};

#endif // DEFAULTVALUESETTINGSWIDGET_H
