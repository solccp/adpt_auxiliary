#include "databaseopencreatedialog.h"

#include <QFileDialog>
#include <QMessageBox>
#include <QDebug>
#include <QtWidgets>
#include <QVBoxLayout>

DataBaseOpenCreateDialog::DataBaseOpenCreateDialog(QWidget *parent) :
    QDialog(parent)
{
    setupUI();
}

DataBaseOpenCreateDialog::~DataBaseOpenCreateDialog()
{

}

void DataBaseOpenCreateDialog::on_butCreateDB_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Create new database"),
                                QDir::homePath(),
                                tr("DFTB DB (*.dftbdb)"), 0, QFileDialog::DontConfirmOverwrite);

    if (fileName.isEmpty())
        return;
    if (!fileName.toLower().endsWith(".dftbdb"))
    {
        fileName.append(".dftbdb");
    }
    QFileInfo fileinfo(fileName);
    if (fileinfo.exists())
    {
        QMessageBox qbox(this);
        qbox.setText(tr("The database is already existing.\nDo you want to open it?"));
        qbox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
        qbox.setIcon(QMessageBox::Question);
        int ret = qbox.exec();
        if (ret == QMessageBox::Cancel)
        {
            return ;
        }
        actionMode = ActionMode::Open;
        emit databaseOpened(fileName);
    }
    else
    {
        actionMode = ActionMode::Create;
        emit databaseCreated(fileName);
    }
    m_path = fileName;
    accept();
}

void DataBaseOpenCreateDialog::on_butOpenDB_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open existing database"),
                                QDir::homePath(),
                                tr("DFTB DB (*.dftbdb)"));
    QFileInfo info(fileName);
    if (info.exists())
    {
        m_path = fileName;
        actionMode = ActionMode::Open;
        emit databaseOpened(fileName);
        accept();
    }
}

void DataBaseOpenCreateDialog::setupUI()
{
    QVBoxLayout *layout = new QVBoxLayout(this);
    QPushButton* butOpenDB = new QPushButton(tr("Open existing database..."));
    QPushButton* butCreateDB = new QPushButton(tr("Create new database..."));
    QPushButton* butConnectDB = new QPushButton(tr("Connect to online database..."));

    butOpenDB->setMinimumHeight(100);
    butCreateDB->setMinimumHeight(100);
    butConnectDB->setMinimumHeight(100);

    butOpenDB->setMinimumWidth(300);
    butCreateDB->setMinimumWidth(300);
    butConnectDB->setMinimumWidth(300);

    connect(butOpenDB, SIGNAL(clicked()), this, SLOT(on_butOpenDB_clicked()));
    connect(butCreateDB, SIGNAL(clicked()), this, SLOT(on_butCreateDB_clicked()));
    connect(butConnectDB, SIGNAL(clicked()), this, SLOT(on_butNetDB_clicked()));

    layout->addWidget(butOpenDB);
    layout->addWidget(butCreateDB);
    layout->addWidget(butConnectDB);
}

QString DataBaseOpenCreateDialog::getPath() const
{
    return m_path;
}

void DataBaseOpenCreateDialog::on_butNetDB_clicked()
{
    actionMode = ActionMode::Connect;
    accept();
}
DataBaseOpenCreateDialog::ActionMode DataBaseOpenCreateDialog::getActionMode() const
{
    return actionMode;
}

void DataBaseOpenCreateDialog::setActionMode(const ActionMode &value)
{
    actionMode = value;
}

