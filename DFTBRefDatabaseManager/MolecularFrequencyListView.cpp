#include "MolecularFrequencyListView.h"
#include "molecularfrequencyeditor.h"

#include "molecularfrequencydata.h"
#include "dftbrefdatabase.h"

#include <QAbstractTableModel>
#include <QSqlTableModel>

MolecularFrequencyListView::MolecularFrequencyListView(DFTBRefDatabase *database, QWidget *parent) :
    DataListViewBase(database, parent)
{
    setupUI();
}

void MolecularFrequencyListView::deleteFromDatabase(const QStringList &uuids)
{
    m_database->removeMolecularFrequency(uuids);
}

void MolecularFrequencyListView::addToDatabase()
{
    QAbstractTableModel* model = m_database->getGeometryTableModel(DFTBRefDatabase::Molecule);
    if (model->rowCount() == 0)
    {
        QMessageBox qbox(this);
        qbox.setText("Molecular geometry needs to be defined first");
        qbox.setStandardButtons(QMessageBox::Ok);
        qbox.setIcon(QMessageBox::Critical);
        qbox.exec();
        return;
    }

    MolecularFrequencyData* data = new MolecularFrequencyData();
    data->setGeom_uuid(model->data(model->index(0,0)).toString());


    while(1)
    {
        int ret = showEditor(data);
        if (ret)
        {
            QString errormsg;
            if (!data->checkData(errormsg))
            {
                QMessageBox qbox(this);
                qbox.setText(errormsg);
                qbox.setStandardButtons(QMessageBox::Ok);
                qbox.setIcon(QMessageBox::Critical);
                qbox.exec();
                continue;
            }

            if (!m_database->addMolecularFrequency(data))
            {
                QMessageBox qbox(this);
                qbox.setText( m_database->getLastError());
                qbox.setStandardButtons(QMessageBox::Ok);
                qbox.setIcon(QMessageBox::Critical);
                qbox.exec();
                continue;
            }
            m_model->select();

            break;
        }
        break;
    }

    delete model;
    delete data;
}

void MolecularFrequencyListView::editItemInDatabase(const QString &uuid)
{
//    const QAbstractItemModel *mymodel = uuid.model();
//    QVariant uuid = mymodel->data(mymodel->index(uuid.row(),0), Qt::EditRole);


    MolecularFrequencyData* data = new MolecularFrequencyData();
    m_database->getMolecularFrequency(uuid, data);
    QString geom_uuid= data->geom_uuid();


    bool foundGeomID = m_database->hasGeometryUUID(geom_uuid, DFTBRefDatabase::Molecule);

    if (!foundGeomID)
    {
        QMessageBox qbox(this);
        qbox.setText("The reference geometry data of this frequency data is missing.");
        qbox.setStandardButtons(QMessageBox::Ok);
        qbox.setIcon(QMessageBox::Critical);
        qbox.exec();
    }

    while(1)
    {
        int ret = showEditor(data);
        if (ret)
        {
            QString errormsg;
            if (!data->checkData(errormsg))
            {
                QMessageBox qbox(this);
                qbox.setText(errormsg);
                qbox.setStandardButtons(QMessageBox::Ok);
                qbox.setIcon(QMessageBox::Critical);
                qbox.exec();
                continue;
            }

            bool succ = m_database->updateMolecularFrequency(data);
            if (succ)
            {
                m_model->select();
            }

            break;
        }
        break;
    }

    delete data;
}

void MolecularFrequencyListView::duplicateItemInDatabase(const QString &uuid)
{
    MolecularFrequencyData* data = new MolecularFrequencyData();
    m_database->getMolecularFrequency(uuid, data);
    data->resetUUID();
    QString geom_uuid= data->geom_uuid();


    bool foundGeomID = m_database->hasGeometryUUID(geom_uuid, DFTBRefDatabase::Molecule);

    if (!foundGeomID)
    {
        QMessageBox qbox(this);
        qbox.setText("The reference geometry data of this frequency data is missing.");
        qbox.setStandardButtons(QMessageBox::Ok);
        qbox.setIcon(QMessageBox::Critical);
        qbox.exec();
    }

    while(1)
    {
        int ret = showEditor(data);
        if (ret)
        {
            QString errormsg;
            if (!data->checkData(errormsg))
            {
                QMessageBox qbox(this);
                qbox.setText(errormsg);
                qbox.setStandardButtons(QMessageBox::Ok);
                qbox.setIcon(QMessageBox::Critical);
                qbox.exec();
                continue;
            }

            bool succ = m_database->addMolecularFrequency(data);
            if (succ)
            {
                m_model->select();
            }

            break;
        }
        break;
    }

    delete data;
}

void MolecularFrequencyListView::adjustTableView()
{
    m_tableView->hideColumn(0);
}

void MolecularFrequencyListView::initModels()
{
    m_model->setTable("MolecularFrequencyListView");
}

int MolecularFrequencyListView::propertyID()
{
    return 3;
}

int MolecularFrequencyListView::showEditor(MolecularFrequencyData *data)
{
    QDialog dia;
    dia.resize(800,600);
    QGridLayout *vb =new QGridLayout();

    QSqlTableModel *model = new QSqlTableModel(this, m_database->database());
    model->setTable("MolecularGeometry_IDSTR");
    model->select();
    MolecularFrequencyDataEditor* e = new MolecularFrequencyDataEditor(data, model);
    vb->addWidget(e,0,0,1,6);

    QPushButton *butOK = new QPushButton(tr("OK"));
    QPushButton *butCancel = new QPushButton(tr("Cancel"));


    connect(butOK, SIGNAL(clicked()), &dia, SLOT(accept()));
    connect(butCancel, SIGNAL(clicked()), &dia, SLOT(reject()));

    vb->addWidget(butOK, 1,4,1,1);
    vb->addWidget(butCancel, 1,5,1,1);

    dia.setLayout(vb);

    return dia.exec();
}
