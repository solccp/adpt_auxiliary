#ifndef GEOMETRYLISTVIEW_H
#define GEOMETRYLISTVIEW_H

#include <QWidget>

#include <QGridLayout>
#include <QPushButton>
#include <QSet>
#include <QTableWidget>

QT_BEGIN_NAMESPACE
class QTableView;
class QTableWidget;
class QSqlTableModel;
class QCheckBox;
class QComboBox;
QT_END_NAMESPACE

class Geometry;
class DFTBRefDatabase;
class ElementSortFilterProxyModel;

class GeometryListView : public QWidget
{
    Q_OBJECT
public:
    GeometryListView(DFTBRefDatabase* database, QWidget *parent = 0);
    virtual ~GeometryListView();
signals:
    void molecularGeometryNameUpdated(const QString& uuid);
    void crystalGeometryNameUpdated(const QString& uuid);
public slots:
    QStringList selectedItems();
    void reload();
    void checkEditable();
    void resetModels();
private slots:
    void addNewCrystalGeometry();
    void addNewMolecularGeometry();

    void updateGeometry(Geometry* geom);
    void deleteGeometry();
    void duplicateSelected();


    int showEditor(Geometry* geom);

    void editItem(const QModelIndex& index);

    void selectAllItems();
    void selectNoItems();
    void selectItemToggle();
    void addItemsToErepfitForceEquations();

    void onTableViewContextMenu(const QPoint& pos);
    void addToDefaultFittingSet();
    void exportSelected();

    void setupFilter();
    void filterChanged();
    void tabChanged(int index);

private:
    void setupUI();
    QTableView* m_tableMolecule;
    QTableView* m_tableCrystal;

    QGridLayout *m_gridlayout;
    QPushButton *butAddMolecularGeometry;
    QPushButton *butAddCrystalGeometry;

    QPushButton* butDelGeometry;
    void BuildSelectionMenu(QMenu *setMenu);

    QComboBox *m_filter_columnIndex;
    QLineEdit *m_filter_content;

private:
    DFTBRefDatabase* m_database;
    QSqlTableModel * model_molecule = nullptr;
    QSqlTableModel * model_crystal = nullptr;
    QSqlTableModel * m_model = nullptr;
    QTableView* m_tableView = nullptr;

    QTabWidget* m_tabWidget = nullptr;

    ElementSortFilterProxyModel *m_proxyModelMolecule = nullptr;
    ElementSortFilterProxyModel *m_proxyModelCrystal = nullptr;
    ElementSortFilterProxyModel *m_proxyModel = nullptr;
};

#endif // GEOMETRYLISTVIEW_H
