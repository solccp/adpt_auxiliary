#ifndef MOLECULARFREQUENCYLISTVIEW_H
#define MOLECULARFREQUENCYLISTVIEW_H

#include "DataListViewBase.h"

class MolecularFrequencyData;
class MolecularFrequencyListView : public DataListViewBase
{
    Q_OBJECT
public:
    MolecularFrequencyListView(DFTBRefDatabase *database, QWidget *parent = 0);

signals:

public slots:  

    // DataListViewBase interface
protected:
    void deleteFromDatabase(const QStringList &uuids);
    void addToDatabase();
    void editItemInDatabase(const QString& uuid);
    void duplicateItemInDatabase(const QString& uuid);
    void adjustTableView();
    void initModels();
    int propertyID();
private:
    int showEditor(MolecularFrequencyData* data);

};

#endif // MOLECULARFREQUENCYLISTVIEW_H
