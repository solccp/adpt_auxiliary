#include "mainwindow.h"
#include "databaseopencreatedialog.h"

#include <QApplication>
#include <QMessageBox>
#include <QDesktopWidget>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QCoreApplication::setOrganizationName("qcl.ac.nctu.edu.tw");
    QCoreApplication::setOrganizationDomain("qcl.ac.nctu.edu.tw");
    QCoreApplication::setApplicationName("DFTBOPT");

    MainWindow w;
    w.resize(1024,600);

    QDesktopWidget *d = QApplication::desktop();
    int ws = d->width();   // returns screen width
    int h = d->height();  // returns screen height
    int mw = w.size().width();
    int mh = w.size().height();
    int cw = (ws/2) - (mw/2);
    int ch = (h/2) - (mh/2);
    w.move(cw,ch);
    w.show();

    return a.exec();
}
