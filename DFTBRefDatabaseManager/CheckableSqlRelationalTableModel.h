#ifndef CHECKABLESQLRELATIONALTABLEMODEL_H
#define CHECKABLESQLRELATIONALTABLEMODEL_H

#include "sqlrelationaltablemodel.h"

#include <QSqlDatabase>

class CheckableSqlRelationalTableModel : public SqlRelationalTableModel
{
    Q_OBJECT
public:
    CheckableSqlRelationalTableModel(QSqlDatabase sqldatabase ,QObject *parent = 0);
    virtual ~CheckableSqlRelationalTableModel(){}
signals:

public slots:


    // QAbstractItemModel interface
public:
    Qt::ItemFlags flags(const QModelIndex &index) const;


    int checkableColumn() const;
    void setCheckableColumn(int checkableColumn);

private:
    int m_checkableColumn = -1;

    // QAbstractItemModel interface
public:
    QVariant data(const QModelIndex &index, int role) const;
};

#endif // CHECKABLESQLRELATIONALTABLEMODEL_H
