#include "MolecularAtomizationEnergyListView.h"

#include "molecularatomizationenergyeditor.h"
#include "molecularatomizationenergydata.h"
#include "dftbrefdatabase.h"
#include "doublenumbercelldelegate.h"
#include <QAbstractTableModel>
#include <QSqlTableModel>
#include <QSqlRecord>
#include <QSqlError>
#include "ElementSortFilterProxyModel.h"

MolecularAtomizationEnergyListView::MolecularAtomizationEnergyListView(DFTBRefDatabase *database, QWidget *parent) :
    DataListViewBase(database, parent)
{
    setupUI();
}

void MolecularAtomizationEnergyListView::addToErepfitEnergyEquations()
{
    m_database->transaction();


    int geom_uuid_index = m_model->fieldIndex("geom_uuid");
    if (geom_uuid_index == -1)
    {
        return;
    }


    QSqlTableModel *model = new QSqlTableModel(this, m_database->database());
    model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    model->setTable("erepfit_default_input_energy_equation");
    model->select();



    QModelIndexList selections_molecule = m_tableView->selectionModel()->selectedRows();

    for(int i=0; i<selections_molecule.size();++i)
    {
        const QModelIndex& index = m_proxyModel->mapToSource(selections_molecule.at(i));


        QSqlRecord record(model->record());


        QString uuid = m_model->data(m_model->index(index.row(),0)).toString();
        QString geom_uuid = m_model->data(m_model->index(index.row(),geom_uuid_index)).toString();

        record.setValue("uuid", uuid);
        record.setValue("geom_uuid", geom_uuid);
        record.setValue("weight", 1.0);


        model->insertRecord(-1, record);
    }
    bool succ = model->submitAll();

    if (succ)
    {
        m_database->commit();
    }
    else
    {
        qDebug() << model->lastError().text();
        m_database->rollback();
    }
}

void MolecularAtomizationEnergyListView::deleteFromDatabase(const QStringList &uuids)
{
    m_database->removeMolecularAtomizationEnergy(uuids);
}

void MolecularAtomizationEnergyListView::addToDatabase()
{
    QAbstractTableModel* model = m_database->getGeometryTableModel(DFTBRefDatabase::Molecule);
    if (model->rowCount() == 0)
    {
        QMessageBox qbox(this);
        qbox.setText("Molecular geometry needs to be defined first");
        qbox.setStandardButtons(QMessageBox::Ok);
        qbox.setIcon(QMessageBox::Critical);
        qbox.exec();
        return;
    }

    MolecularAtomizationEnergyData* data = new MolecularAtomizationEnergyData();
    data->setGeom_uuid(model->data(model->index(0,0)).toString());


    while(1)
    {
        int ret = showEditor(data);
        if (ret)
        {
            QString errormsg;
            data->resetUUID();
            if (!m_database->addMolecularAtomizationEnergy(data))
            {
                QMessageBox qbox(this);
                qbox.setText( m_database->getLastError());
                qbox.setStandardButtons(QMessageBox::Ok);
                qbox.setIcon(QMessageBox::Critical);
                qbox.exec();
                continue;
            }
            m_model->select();

            break;
        }
        break;
    }

    delete model;
    delete data;
}

void MolecularAtomizationEnergyListView::editItemInDatabase(const QString &uuid)
{
//    const QAbstractItemModel *mymodel = uuid.model();
//    QVariant uuid = mymodel->data(mymodel->index(uuid.row(),0), Qt::EditRole);


    MolecularAtomizationEnergyData* data = new MolecularAtomizationEnergyData();
    m_database->getMolecularAtomizationEnergy(uuid, data);
    QString geom_uuid= data->geom_uuid();



    bool foundGeomID = m_database->hasGeometryUUID(geom_uuid, DFTBRefDatabase::Molecule);


    if (!foundGeomID)
    {
        QMessageBox qbox(this);
        qbox.setText("The reference geometry data of this frequency data is missing.");
        qbox.setStandardButtons(QMessageBox::Ok);
        qbox.setIcon(QMessageBox::Critical);
        qbox.exec();
    }

    while(1)
    {
        int ret = showEditor(data);
        if (ret)
        {
            bool succ = m_database->updateMolecularAtomizationEnergy(data);
            if (succ)
            {
                m_model->select();
            }

            break;
        }
        break;
    }

    delete data;
}

void MolecularAtomizationEnergyListView::duplicateItemInDatabase(const QString &uuid)
{
    MolecularAtomizationEnergyData* data = new MolecularAtomizationEnergyData();
    m_database->getMolecularAtomizationEnergy(uuid, data);

    QString geom_uuid= data->geom_uuid();



    bool foundGeomID = m_database->hasGeometryUUID(geom_uuid, DFTBRefDatabase::Molecule);
    if (!foundGeomID)
    {
        QMessageBox qbox(this);
        qbox.setText("The reference geometry data of this frequency data is missing.");
        qbox.setStandardButtons(QMessageBox::Ok);
        qbox.setIcon(QMessageBox::Critical);
        qbox.exec();
    }

    while(1)
    {
        int ret = showEditor(data);
        if (ret)
        {
            data->resetUUID();
            bool succ = m_database->addMolecularAtomizationEnergy(data);
            if (succ)
            {
                m_model->select();
            }

            break;
        }
        break;
    }

    delete data;
}

void MolecularAtomizationEnergyListView::adjustTableView()
{
    m_tableView->hideColumn(0);
    m_tableView->setItemDelegateForColumn(4, new DoubleNumberCellDelegate());
    int geom_uuid_index = m_model->fieldIndex("geom_uuid");
    if (geom_uuid_index != -1)
    {
        m_tableView->hideColumn(geom_uuid_index);
    }
}

void MolecularAtomizationEnergyListView::initModels()
{
    m_model->setTable("MolecularAtomizationEnergyListView");
}

int MolecularAtomizationEnergyListView::propertyID()
{
    return 4;
}

int MolecularAtomizationEnergyListView::showEditor(MolecularAtomizationEnergyData *data)
{
    QDialog dia;
    dia.resize(600,200);
    QGridLayout *vb =new QGridLayout();

    QSqlTableModel *model = new QSqlTableModel(this, m_database->database());
    model->setTable("MolecularGeometry_IDSTR");
    model->select();
    MolecularAtomizationEnergyEditor* e = new MolecularAtomizationEnergyEditor(data, model);
    vb->addWidget(e,0,0,1,6);

    QPushButton *butOK = new QPushButton(tr("OK"));
    QPushButton *butCancel = new QPushButton(tr("Cancel"));


    connect(butOK, SIGNAL(clicked()), &dia, SLOT(accept()));
    connect(butCancel, SIGNAL(clicked()), &dia, SLOT(reject()));

    vb->addWidget(butOK, 1,4,1,1);
    vb->addWidget(butCancel, 1,5,1,1);

    dia.setLayout(vb);

    return dia.exec();
}




void MolecularAtomizationEnergyListView::extraContextMenu(QMenu *parent)
{
    parent->addAction(tr("Erepfit Energy Equations"), this, SLOT(addToErepfitEnergyEquations()));
    return;

}
