#include "ReactionEnergyListView.h"

#include "reactionenergydataeditor.h"
#include "reactionenergydata.h"

#include "dftbrefdatabase.h"
#include "doublenumbercelldelegate.h"

#include <QSqlTableModel>

#include "YesNoCellItemDelegate.h"

ReactionEnergyListView::ReactionEnergyListView(DFTBRefDatabase *database, QWidget *parent) :
    DataListViewBase(database, parent)
{
    setupUI();
}

void ReactionEnergyListView::deleteFromDatabase(const QStringList &uuids)
{
    m_database->removeReactionEnergy(uuids);
}

void ReactionEnergyListView::addToDatabase()
{
//    QAbstractTableModel* model = m_database->getGeometryTableModel(DFTBRefDatabase::Molecule);
//    if (model->rowCount() == 0)
//    {
//        QMessageBox qbox(this);
//        qbox.setText("Molecular geometry needs to be defined first");
//        qbox.setStandardButtons(QMessageBox::Ok);
//        qbox.setIcon(QMessageBox::Critical);
//        qbox.exec();
//        return;
//    }

    ReactionEnergyData* data = new ReactionEnergyData();

    while(1)
    {
        int ret = showEditor(data);
        if (ret)
        {
            QString errormsg;
            if (!data->checkData(errormsg))
            {
                QMessageBox qbox(this);
                qbox.setText(errormsg);
                qbox.setStandardButtons(QMessageBox::Ok);
                qbox.setIcon(QMessageBox::Critical);
                qbox.exec();
                continue;
            }

            data->resetUUID();
            if (!m_database->addReactionEnergy(data))
            {
                QMessageBox qbox(this);
                qbox.setText( m_database->getLastError());
                qbox.setStandardButtons(QMessageBox::Ok);
                qbox.setIcon(QMessageBox::Critical);
                qbox.exec();
                continue;
            }
            m_model->select();
            break;
        }
        break;
    }

//    delete model;
    delete data;
}

void ReactionEnergyListView::editItemInDatabase(const QString& uuid)
{
//    const QAbstractItemModel *mymodel = index.model();
//    QVariant uuid = mymodel->data(mymodel->index(index.row(),0), Qt::EditRole);


    ReactionEnergyData* data = new ReactionEnergyData();
    m_database->getReactionEnergy(uuid, data);



    while(1)
    {
        int ret = showEditor(data);
        if (ret)
        {
            QString errormsg;
            if (!data->checkData(errormsg))
            {
                QMessageBox qbox(this);
                qbox.setText(errormsg);
                qbox.setStandardButtons(QMessageBox::Ok);
                qbox.setIcon(QMessageBox::Critical);
                qbox.exec();
                continue;
            }
            if (m_database->updateReactionEnergy(data))
            {
                m_model->select();
            }


            break;
        }
        break;
    }

    delete data;
}

void ReactionEnergyListView::duplicateItemInDatabase(const QString &uuid)
{
    ReactionEnergyData* data = new ReactionEnergyData();
    m_database->getReactionEnergy(uuid, data);

    data->setName(data->name()+"_copy");




    while(1)
    {
        int ret = showEditor(data);
        if (ret)
        {
            QString errormsg;
            if (!data->checkData(errormsg))
            {
                QMessageBox qbox(this);
                qbox.setText(errormsg);
                qbox.setStandardButtons(QMessageBox::Ok);
                qbox.setIcon(QMessageBox::Critical);
                qbox.exec();
                continue;
            }

            data->resetUUID();
            if (m_database->addReactionEnergy(data))
            {
                m_model->select();
            }


            break;
        }
        break;
    }

    delete data;
}

void ReactionEnergyListView::adjustTableView()
{
    m_tableView->hideColumn(0);
    m_tableView->setItemDelegateForColumn(4, new DoubleNumberCellDelegate(this));
    m_tableView->setItemDelegateForColumn(6,new YesNoCellItemDelegate(this));
}

void ReactionEnergyListView::initModels()
{
    m_model->setTable("ReactionEnergyListView");
}

int ReactionEnergyListView::propertyID()
{
    return 5;
}

int ReactionEnergyListView::showEditor(ReactionEnergyData *data)
{
    QDialog dia;
    dia.resize(800,600);
    QGridLayout *vb =new QGridLayout();

    ReactionEnergyDataEditor* e = new ReactionEnergyDataEditor(data, m_database);
    vb->addWidget(e,0,0,1,6);

    QPushButton *butOK = new QPushButton(tr("OK"));
    QPushButton *butCancel = new QPushButton(tr("Cancel"));


    connect(butOK, SIGNAL(clicked()), &dia, SLOT(accept()));
    connect(butCancel, SIGNAL(clicked()), &dia, SLOT(reject()));

    vb->addWidget(butOK, 1,4,1,1);
    vb->addWidget(butCancel, 1,5,1,1);

    dia.setLayout(vb);

    return dia.exec();
}
