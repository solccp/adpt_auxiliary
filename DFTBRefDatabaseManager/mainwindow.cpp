#include "mainwindow.h"

#include "VERSION_INFO.h"

#include "clustergeometryeditor.h"
#include "crystalgeometryform.h"

#include "GeometryListView.h"
#include "databaseopencreatedialog.h"
#include "dftbrefdatabase.h"

#include "BandstructureListView.h"
#include "MolecularFrequencyListView.h"
#include "MolecularAtomizationEnergyListView.h"
#include "ReactionEnergyListView.h"


#include "folderbaseimportor.h"
#include "geometry.h"
#include "bandstructuredata.h"
#include "molecularfrequencydata.h"
#include "molecularatomizationenergydata.h"
#include "reactionenergydata.h"

#include "testsuiteinputfilegenerator.h"

#include <QAbstractTableModel>
#include <QTableWidget>
#include <QMessageBox>
#include <QMenuBar>
#include <QFileDialog>
#include <QProgressDialog>
#include <QSet>
#include <QEvent>

#include <QThread>

#include <fstream>

#include "PeriodicTableWidget.h"
#include "ErepfitPotentialDefinitionWidget.h"
#include "DefaultValueSettingsWidget.h"

#include "singleselectlistwidget.h"
#include <QSqlTableModel>

#include <QScrollArea>

#include "DatabaseConnectingDialog.h"

#include <QtCore/QTimer>

#include <QSqlTableModel>

#include <Variant/Variant.h>
#include <variantqt.h>
//#include <dbexporter.h>

#include "datastructure_variant.h"

#include "ReferenceDataImportor.h"



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    m_database =  new DFTBRefDatabase(this);
    setupUI();
    m_category->setEnabled(false);
    QTimer::singleShot(0, this, SLOT(init()));
    connect(m_database, SIGNAL(databaseConnectionChanged()), this, SLOT(databaseConnectionChanged()));
}

MainWindow::~MainWindow()
{
    delete m_geomView;
    m_geomView = nullptr;


    delete m_bandstructureView;
    m_bandstructureView = nullptr;

    delete m_molecularFrequencyView;
    m_molecularFrequencyView = nullptr;
    delete m_molecularAtomizationEnergyView;
    m_molecularAtomizationEnergyView = nullptr;

    delete m_reactionEnergyView;
    m_reactionEnergyView = nullptr;

    delete m_defaultValueSettings;
    m_defaultValueSettings = nullptr;
}

bool MainWindow::init()
{
    bool exit = false;
    int errorCode = 0;
    while(1)
    {
        DatabaseConnectingDialog dia(this);
        dia.setWindowModality(Qt::WindowModal);
        int ret = dia.exec();
        if (ret != QDialog::Accepted)
        {
            exit = true;
            break;
        }

        errorCode = m_database->loadOnlineDatabase(dia.server_addr(), dia.account(), dia.password(), dia.database_name());
        if (errorCode != DFTBRefDatabase::NoError)
        {
            QMessageBox qbox(this);
            qbox.setText("Error: " + m_database->getLastError());
            qbox.setStandardButtons(QMessageBox::Yes);
            qbox.setIcon(QMessageBox::Critical);
            qbox.exec();
            m_database->clearError();
            continue;
        }
        else
        {
            break;
        }
    }
    if (exit)
    {
        return false;
    }
    return true;
}

void MainWindow::databaseConnectionChanged()
{
    updateWindowTitle();
    if (m_database->isOpen())
    {
        m_category->setEnabled(true);
    }
    else
    {
        m_category->setEnabled(false);
    }
    this->m_geomView->checkEditable();
    this->m_bandstructureView->checkEditable();
    this->m_molecularFrequencyView->checkEditable();
    this->m_molecularAtomizationEnergyView->checkEditable();
    this->m_reactionEnergyView->checkEditable();

    this->m_geomView->resetModels();
    this->m_bandstructureView->resetModels();
    this->m_molecularFrequencyView->resetModels();
    this->m_molecularAtomizationEnergyView->resetModels();
    this->m_reactionEnergyView->resetModels();
}

class PDObserver : public FolderbaseObserver
{
    // FolderbaseObserver interface
public:
    explicit PDObserver(QProgressDialog* pd) : m_pd(pd) {}

    void setNewParseRange(int min, int max) const
    {
        m_pd->setRange(min, max);
    }
    void newText(const QString &text) const
    {
        m_pd->setLabelText(text);
    }
    void progressUpdated(int step) const
    {
        m_pd->setValue(step);
    }
    bool wasCanceled() const
    {
        return m_pd->wasCanceled();
    }
private:
     QProgressDialog* m_pd;
};

void MainWindow::importFolderBaseData()
{

//    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
//                                                    QDir::homePath(),
//                                                    QFileDialog::ShowDirsOnly
//                                                    | QFileDialog::DontResolveSymlinks);

//    if (dir.isEmpty())
//        return;

////    m_geomView->importFolderbaseData(dir);
//    FolderbaseImporter *importor = new FolderbaseImporter(m_database);
//    importor->setPath(dir);

//    QProgressDialog pd(this);
//    pd.setMinimumDuration(0);
//    pd.setWindowModality(Qt::WindowModal);
//    pd.setCancelButtonText("Stop");

//    PDObserver *pdov = new PDObserver(&pd);

//    importor->setObserver(pdov);
//    importor->scanGeometry();
//    pd.hide();

//    QAbstractTableModel *model =  m_database->getGeometryTableModel();
//    QMap<QString, QString> gid;

//    m_database->database().transaction();
//    bool AllGood = true;
//    QSet<QString> exist_geomID;

//    for(int i=0; i< model->rowCount(); ++i)
//    {
//        QString str;
//        if (model->data(model->index(i,4)).toInt() == 1)
//        {
//            str = QString("%1_%2_%3_%4").arg("crystal")
//                    .arg(model->data(model->index(i,1)).toString())
//                    .arg(model->data(model->index(i,5)).toString())
//                    .arg(model->data(model->index(i,6)).toString());
//        }
//        else if (model->data(model->index(i,4)).toInt() == 0)
//        {
//            bool isstat = model->data(model->index(i,9)).toBool();
//            str = QString("%1_%2_%3_%4_%5_%6_%7").arg("molecule")
//                    .arg(model->data(model->index(i,7)).toString())
//                    .arg(model->data(model->index(i,2)).toString())
//                    .arg(model->data(model->index(i,3)).toString())
//                    .arg(model->data(model->index(i,5)).toString())
//                    .arg(model->data(model->index(i,6)).toString())
//                    .arg(isstat);
//        }
//        if (!str.isEmpty())
//        {
//            str = str.replace(QRegExp("[^a-zA-z0-9_]"), "").toLower();
//            gid.insert(str, model->data(model->index(i,1)).toString());
//        }
//        exist_geomID.insert(model->data(model->index(i,0)).toString());
//    }

//    QList<Geometry*> scanned_geom = importor->geometryData();
//    QList<Geometry*> unrepeated_geom;
//    QList<Geometry*> repeated_geom;
//    QSet<QString> final_geomID;



//    QStringList repeated_names_molecule;
//    QStringList repeated_names_crystal;

//    for(int i=0; i<scanned_geom.size(); ++i)
//    {
//        if (scanned_geom[i]->PBC())
//        {
//            QString str = QString("%1_%2_%3_%4").arg("crystal")
//                    .arg(scanned_geom[i]->getName())
//                    .arg(scanned_geom[i]->getMethod())
//                    .arg(scanned_geom[i]->getBasis());
//            str = str.replace(QRegExp("[^a-zA-z0-9_]"), "").toLower();
//            if (gid.contains(str))
//            {
//                repeated_names_crystal.append(gid[str]+"(" + str + ")");
//                repeated_geom.append(scanned_geom[i]);
//            }
//            else
//            {
//                unrepeated_geom.append(scanned_geom[i]);
//            }
//        }
//        else
//        {
//            QString str = QString("%1_%2_%3_%4_%5_%6").arg("molecule")
//                    .arg(scanned_geom[i]->getCharge())
//                    .arg(scanned_geom[i]->getFormula())
//                    .arg(scanned_geom[i]->getMethod())
//                    .arg(scanned_geom[i]->getBasis())
//                    .arg(scanned_geom[i]->isStationary());
//            str = str.replace(QRegExp("[^a-zA-z0-9_]"), "").toLower();
//            if (gid.contains(str))
//            {
//                repeated_geom.append(scanned_geom[i]);
//                repeated_names_molecule.append(gid[str]+"(" + str + ")");
//            }
//            else
//            {
//                unrepeated_geom.append(scanned_geom[i]);
//            }
//        }
//    }

//    bool failed = false;
//    bool skipping = false;
//    if (!repeated_names_molecule.isEmpty() || !repeated_names_crystal.isEmpty())
//    {

//        QMessageBox qbox(this);
//        qbox.setText("Warning: There may exist some duplicated records in the database!\n"
//                     "Noted: If Ignore is chosen, all data which refer to those duplicated records will be ignored as well.");
//        qbox.setInformativeText("For more information, see detailed text.");
//        QStringList detailedtext;
//        if (!repeated_names_crystal.isEmpty())
//        {
//            detailedtext.append("Crystal:");
//            for(int i=0; i<repeated_names_crystal.size();++i)
//            {
//                detailedtext.append("  " + repeated_names_crystal[i]);
//            }
//        }
//        if (!repeated_names_molecule.isEmpty())
//        {
//            detailedtext.append("Molecule:");
//            for(int i=0; i<repeated_names_molecule.size();++i)
//            {
//                detailedtext.append("  " + repeated_names_molecule[i]);
//            }
//        }


//        qbox.setDetailedText(detailedtext.join("\n"));
//        qbox.setStandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::Ignore);
//        qbox.setButtonText(QMessageBox::Yes, "Continue");
//        qbox.setButtonText(QMessageBox::Ignore, "Skip duplicated");
//        qbox.setButtonText(QMessageBox::No, "Stop");


//        qbox.setDefaultButton(QMessageBox::No);
//        qbox.setIcon(QMessageBox::Warning);

//        pd.reset();


//        int ret = qbox.exec();
//        if (ret == QMessageBox::No)
//        {
//            failed = true;
//            AllGood = false;
//        }
//        else if ( ret == QMessageBox::Ignore )
//        {
//            skipping = true;
//        }
//    }
//    if (!failed)
//    {
//        pd.reset();
//        pd.setLabelText("Importing geometry data...");

//        m_database->disbleSignal();

//        if (skipping)
//        {
//            pd.setRange(0, unrepeated_geom.size());
//            for(int i=0; i<unrepeated_geom.size(); ++i)
//            {
//                pd.setValue(i);
//                if (pd.wasCanceled())
//                {
//                    failed = true;
//                    AllGood = false;
//                    break;
//                }
//                bool succ = m_database->addGeometry(unrepeated_geom[i]);
//                if (!succ)
//                {
//                    failed = true;
//                    AllGood = false;
//                    break;
//                }
//                final_geomID.insert(unrepeated_geom[i]->UUID());
//            }
//        }
//        else
//        {
//            pd.setRange(0, scanned_geom.size());
//            for(int i=0; i<scanned_geom.size(); ++i)
//            {
//                pd.setValue(i);
//                if (pd.wasCanceled())
//                {
//                    failed = true;
//                    AllGood = false;
//                    break;
//                }
//                bool succ = m_database->addGeometry(scanned_geom[i]);
//                if (!succ)
//                {
//                    failed = true;
//                    AllGood = false;
//                    break;
//                }
//                final_geomID.insert(scanned_geom[i]->UUID());
//            }
//        }

//        m_database->enableSignal();
//    }

//    if (!failed)
//    {
//        if (importor->scanBandstructure())
//        {
//            QList<BandstructureData*> scanned_data = importor->bandstructureData();
//            pd.reset();
//            pd.setRange(0, scanned_data.size());
//            pd.setLabelText("Importing bandstructure data...");

//            m_database->disbleSignal();
//            for(int i=0; i<scanned_data.size(); ++i)
//            {
//                pd.setValue(i);
//                if (pd.wasCanceled())
//                {
//                    failed = true;
//                    AllGood = false;
//                    break;
//                }
//                if (final_geomID.contains(scanned_data[i]->getGeomID()) || exist_geomID.contains(scanned_data[i]->getGeomID()))
//                {
//                    bool succ = m_database->addBandStructure(scanned_data[i]);
//                    if (!succ)
//                    {
//                        failed = true;
//                        AllGood = false;
//                        break;
//                    }
//                }
//            }
//            m_database->enableSignal();
//        }
//    }

//    if (!failed)
//    {
//        if (importor->scanMolecularFrequency())
//        {
//            QList<MolecularFrequencyData*> scanned_data = importor->frequencyData();
//            pd.reset();
//            pd.setRange(0, scanned_data.size());
//            pd.setLabelText("Importing frequency data...");

//            m_database->disbleSignal();
//            for(int i=0; i<scanned_data.size(); ++i)
//            {
//                pd.setValue(i);
//                if (pd.wasCanceled())
//                {
//                    failed = true;
//                    AllGood = false;
//                    break;
//                }
//                if (final_geomID.contains(scanned_data[i]->geom_uuid()) || exist_geomID.contains(scanned_data[i]->geom_uuid()))
//                {
//                    bool succ = m_database->addMolecularFrequency(scanned_data[i]);
//                    if (!succ)
//                    {
//                        failed = true;
//                        AllGood = false;
//                        break;
//                    }
//                }
//            }
//            m_database->enableSignal();
//        }
//    }

//    if (!failed)
//    {
//        importor->scanMolecularAtomizationEnergy();
//        QList<MolecularAtomizationEnergyData*> scanned_data = importor->atomizationEnergyData();
//        pd.reset();
//        pd.setRange(0, scanned_data.size());
//        pd.setLabelText("Importing atomization energy data...");

//        m_database->disbleSignal();
//        for(int i=0; i<scanned_data.size(); ++i)
//        {
//            pd.setValue(i);
//            if (pd.wasCanceled())
//            {
//                failed = true;
//                AllGood = false;
//                break;
//            }
//            if (final_geomID.contains(scanned_data[i]->geom_uuid()) || exist_geomID.contains(scanned_data[i]->geom_uuid()))
//            {
//                bool succ = m_database->addMolecularAtomizationEnergy(scanned_data[i]);
//                if (!succ)
//                {
//                    failed = true;
//                    AllGood = false;
//                    break;
//                }
//            }
//        }
//        m_database->enableSignal();
//    }

//    if (!failed)
//    {
//        importor->scanReactionEnergy();
//        QList<ReactionEnergyData*> scanned_data = importor->reactionEnergyData();
//        pd.reset();
//        pd.setRange(0, scanned_data.size());
//        pd.setLabelText("Importing reaction energy data...");

//        m_database->disbleSignal();
//        for(int i=0; i<scanned_data.size(); ++i)
//        {
//            pd.setValue(i);
//            if (pd.wasCanceled())
//            {
//                failed = true;
//                AllGood = false;
//                break;
//            }

//            bool containAllMole = true;
//            QList<ReactionItem> reacts = scanned_data[i]->reactants();
//            for(int ireact = 0; ireact< reacts.size(); ++ireact  )
//            {
//                if (!final_geomID.contains(reacts.at(ireact).uuid) && !exist_geomID.contains(reacts.at(ireact).uuid))
//                {
//                    containAllMole = false;
//                    break;
//                }
//            }
//            QList<ReactionItem> products = scanned_data[i]->products();
//            for(int ireact = 0; ireact< products.size(); ++ireact  )
//            {
//                if (!final_geomID.contains(products.at(ireact).uuid) && !exist_geomID.contains(products.at(ireact).uuid))
//                {
//                    containAllMole = false;
//                    break;
//                }
//            }
//            if (containAllMole)
//            {
//                bool succ = m_database->addReactionEnergy(scanned_data[i]);
//                if (!succ)
//                {
//                    failed = true;
//                    AllGood = false;
//                    break;
//                }
//            }
//        }
//        m_database->enableSignal();
//    }

//    if (AllGood)
//    {
//        qDebug() << "No Errors. Writting to database...";
//        m_database->database().commit();
//        reload_all();
//    }
//    else
//    {

//        qDebug() << "There's errors. Nothing writting to databse...";
//        m_database->database().rollback();
//    }


//    delete importor;


}

void MainWindow::clearDatabase()
{
    QMessageBox qbox(this);
    qbox.setText("Warning: Are you sure you want to delete everyting from database file?");
    qbox.setStandardButtons(QMessageBox::Yes | QMessageBox::No );
    qbox.setDefaultButton(QMessageBox::No);
    qbox.setIcon(QMessageBox::Warning);
    int ret = qbox.exec();
    if (ret == QMessageBox::Yes)
    {
        m_database->clear();
    }

}

void MainWindow::changeDatabase()
{
    DatabaseConnectingDialog dia;
    int ret = dia.exec();
    if (ret != QDialog::Accepted)
    {
        return;
    }

    m_database->closeDatabase();
    int errorCode = m_database->loadOnlineDatabase(dia.server_addr(), dia.account(), dia.password(), dia.database_name());
    if (errorCode != DFTBRefDatabase::NoError)
    {
        QMessageBox qbox(this);
        qbox.setText("Error: " + m_database->getLastError());
        qbox.setStandardButtons(QMessageBox::Yes);
        qbox.setIcon(QMessageBox::Critical);
        qbox.exec();
        m_database->clearError();
    }
}

void MainWindow::exportTestSuiteInput()
{

   TestSuiteInputFileGenerator *generator = new TestSuiteInputFileGenerator(m_database, this);

   QStringList items = m_geomView->selectedItems();
   for(int i=0; i<items.size();++i)
   {
       generator->addGeometryTarget(items.at(i));
   }

   items = m_molecularFrequencyView->selectedItems();
   for(int i=0; i<items.size();++i)
   {
       generator->addFrequencyTarget(items.at(i));
   }

   items = m_bandstructureView->selectedItems();
   for(int i=0; i<items.size();++i)
   {
       generator->addBandStructureTarget(items.at(i));
   }

   items = m_molecularAtomizationEnergyView->selectedItems();
   for(int i=0; i<items.size();++i)
   {
       generator->addAtomizationEnergyTarget(items.at(i));
   }

   items = m_reactionEnergyView->selectedItems();
   for(int i=0; i<items.size();++i)
   {
       generator->addReactionEnergyTarget(items.at(i));
   }

   DFTBTestSuite::ReferenceData ref_data = generator->exportRefData();
   DFTBTestSuite::InputData input_data = generator->exportInputData();


    QString ref_file = QFileDialog::getSaveFileName(this, tr("Export TestSuite Reference file"),
                                                   QDir::homePath(), "YAML file (*.yaml)");


//    if (!ref_file.isEmpty())
//    {
//        std::ofstream final_reference(ref_file.toStdString());
//        YAML::Node node;

//       node = ref_data;
//       final_reference << node << std::endl;

//       QMessageBox box(this);
//       box.setText("TestSuite reference file has been saved to " + ref_file);
//       box.setIcon(QMessageBox::Information);
//       box.setStandardButtons(QMessageBox::Yes );
//       box.exec();
//    }

//    QString input_file = QFileDialog::getSaveFileName(this, tr("Export TestSuite Input"),
//                                                    QDir::homePath(), "YAML file (*.yaml)");

//    if (!input_file.isEmpty())
//    {
//        std::ofstream final_input(input_file.toStdString());
//        YAML::Node node;
//        node = input_data;
//        final_input << node << std::endl;

//        QMessageBox box(this);
//        box.setText("TestSuite input file has been saved to " + input_file);
//        box.setIcon(QMessageBox::Information);
//        box.setStandardButtons(QMessageBox::Yes);
//        box.exec();
//    }
    delete generator;
}

void MainWindow::reload_all()
{
    m_geomView->reload();
    m_bandstructureView->reload();
    m_molecularFrequencyView->reload();
    m_molecularAtomizationEnergyView->reload();
    m_reactionEnergyView->reload();
}

void MainWindow::about()
{
    QMessageBox msgBox;
    QString version("Version: %1.%2.%3 <br>Website:<a href='http://qcl.ac.nctu.edu.tw/dftboptbundle'>http://qcl.ac.nctu.edu.tw/dftboptbundle</a><br>Email: sol.chou@gmail.com<br>Powered by Chien-Pin Chou");
    msgBox.setTextFormat(Qt::RichText);
    msgBox.setText(version.arg(VERSION_MAJOR).arg(VERSION_MINOR).arg(VERSION_REVISION));
    msgBox.setWindowTitle("DFTBRefDatabaseManager");
    msgBox.exec();

}

void MainWindow::aboutQt()
{
    QMessageBox::aboutQt(this,tr("DFTBRefDatabaseManager"));
}

void MainWindow::exportDB()
{

    const int NITEM = 10;
    {       
        QString filename = QFileDialog::getSaveFileName();

        if (!filename.isEmpty())
        {
            bool yaml = true;
            if (!filename.endsWith(".yaml"))
            {
                yaml = false;
            }


            libvariant::Variant var;


            QSet<QString> geom_uuids;


            {
                libvariant::Variant data_node(libvariant::Variant::ListType);
                qDebug() << "export bandstructure";
                QSqlTableModel *model = new QSqlTableModel(this, m_database->database());
                model->setTable("BandstructureListView");
                model->select();


                for(int i=0; i<std::min(model->rowCount(), NITEM);++i)
                {
                    QString uuid = model->data(model->index(i,0)).toString();
                    qDebug() << uuid  << i << model->rowCount();
                    BandstructureData data;
                    m_database->getBandStructure(uuid, &data);
                    geom_uuids.insert(data.getGeomID());
                    libvariant::Variant var_data = libvariant::VariantConvertor<BandstructureData>::toVariant(data);
                    data_node.Append(var_data);
                }
                delete model;

                var["bandstructure"] = data_node;
            }


            {
                libvariant::Variant data_node(libvariant::Variant::ListType);
                qDebug() << "export atomization energy";
                QSqlTableModel *model = new QSqlTableModel(this, m_database->database());
                model->setTable("MolecularAtomizationEnergyListView");
                model->select();


                for(int i=0; i<std::min(model->rowCount(), NITEM);++i)
                {
                    QString uuid = model->data(model->index(i,0)).toString();
                    qDebug() << uuid  << i << model->rowCount();
                    MolecularAtomizationEnergyData data;
                    m_database->getMolecularAtomizationEnergy(uuid, &data);
                    geom_uuids.insert(data.geom_uuid());
                    libvariant::Variant var_data = libvariant::VariantConvertor<MolecularAtomizationEnergyData>::toVariant(data);
                    data_node.Append(var_data);
                }
                delete model;

                var["atomization_energy"] = data_node;
            }

            {
                libvariant::Variant data_node(libvariant::Variant::ListType);
                qDebug() << "export MolecularFrequency";
                QSqlTableModel *model = new QSqlTableModel(this, m_database->database());
                model->setTable("MolecularFrequencyListView");
                model->select();


                for(int i=0; i<std::min(model->rowCount(), NITEM);++i)
                {
                    QString uuid = model->data(model->index(i,0)).toString();
                    qDebug() << uuid  << i << model->rowCount();
                    MolecularFrequencyData data;
                    m_database->getMolecularFrequency(uuid, &data);
                    geom_uuids.insert(data.geom_uuid());
                    libvariant::Variant var_data = libvariant::VariantConvertor<MolecularFrequencyData>::toVariant(data);
                    data_node.Append(var_data);
                }
                delete model;

                var["molecular_frequency"] = data_node;
            }

            {
                libvariant::Variant data_node(libvariant::Variant::ListType);
                qDebug() << "export reaction energy";
                QSqlTableModel *model = new QSqlTableModel(this, m_database->database());
                model->setTable("ReactionEnergyListView");
                model->select();


                for(int i=0; i<std::min(model->rowCount(), NITEM);++i)
                {
                    QString uuid = model->data(model->index(i,0)).toString();
                    qDebug() << uuid  << i << model->rowCount();
                    ReactionEnergyData data;
                    m_database->getReactionEnergy(uuid, &data);
                    for(auto const & item : data.products())
                    {
                        geom_uuids.insert(item.uuid);
                    }
                    for(auto const & item : data.reactants())
                    {
                        geom_uuids.insert(item.uuid);
                    }

                    libvariant::Variant var_data = libvariant::VariantConvertor<ReactionEnergyData>::toVariant(data);
                    data_node.Append(var_data);
                }
                delete model;

                var["reaction_energy"] = data_node;
            }



            {
                libvariant::Variant data_node(libvariant::Variant::ListType);
                qDebug() << "export geometry";
                auto geometrys = m_database->getGeometryListModel(DFTBRefDatabase::Both);
                for(int i=0; i<std::min(geometrys->rowCount(), NITEM);++i)
                {
                    QString uuid = geometrys->data(geometrys->index(i,0)).toString();
                    geom_uuids.insert(uuid);
                    qDebug() << uuid  << i << geometrys->rowCount();
                }

                for(auto const & item : geom_uuids)
                {
                    Geometry data;
                    m_database->getGeometry(item, &data);
                    libvariant::Variant var_geom = libvariant::VariantConvertor<Geometry>::toVariant(data);
                    data_node.Append(var_geom);
                }

                var["geometry"] = data_node;
            }

            if (yaml)
            {
                libvariant::SerializeYAML(filename.toStdString(), var);
            }
            else
            {
                libvariant::SerializeJSON(filename.toStdString(), var, true);
            }
        }
    }
}

void MainWindow::importor()
{
    ReferenceDataImportor *importer = new ReferenceDataImportor(m_database, this);
    importer->resize(800,600);
    importer->setWindowModality(Qt::WindowModal);
    importer->show();
    this->reload_all();
}

//void MainWindow::migrate()
//{
//    {
//        qDebug() << "migrate crystal geometry";
//        auto geometrys = m_database->getGeometryListModel(DFTBRefDatabase::Crystal);
//        for(int i=0; i<geometrys->rowCount();++i)
//        {
//            QString uuid = geometrys->data(geometrys->index(i,0)).toString();
//            qDebug() << uuid  << i << geometrys->rowCount();
//            Geometry data;
//            m_database->getGeometry(uuid, &data);

//            data.m_kpoints_setting.m_type = KPointsSetting::Type::SupercellFolding;
//            for(auto i: {0,1,2})
//            {
//                data.m_kpoints_setting.m_kpoints_supercell.num_of_cells[i][i] = data.kpoints()[i];
//            }
//            data.m_kpoints_setting.m_kpoints_supercell.shifts = data.kpoint_shifts();

//            m_database->updateGeometry(&data);
//        }
//    }

//    {
//        qDebug() << "migrate bandstructure";
//        QSqlTableModel *model = new QSqlTableModel(this, m_database->database());
//        model->setTable("BandstructureListView");
//        model->select();

//        for(int i=0; i<model->rowCount();++i)
//        {
//            QString uuid = model->data(model->index(i,0)).toString();
//            qDebug() << uuid  << i << model->rowCount();
//            BandstructureData data;
//            m_database->getBandStructure(uuid, &data);

//            data.m_kpoints_setting.m_type = KPointsSetting::Type::KLines;
//            for(auto const & line : data.klines())
//            {
//                KLinesEntry entry;
//                entry.num_of_points = line[0].toInt();
//                entry.kpoint[0] = line[1].toDouble();
//                entry.kpoint[1] = line[2].toDouble();
//                entry.kpoint[2] = line[3].toDouble();
//                data.m_kpoints_setting.m_kpoints_klines.append(entry);
//            }

//            m_database->updateBandStructure(&data);
//        }
//        delete model;
//    }
//}

void MainWindow::setupUI()
{
    m_category = new QTabWidget(this);

    m_geomView = new GeometryListView(m_database, this);
    m_bandstructureView = new BandstructureListView(m_database, this);
    m_molecularFrequencyView = new MolecularFrequencyListView(m_database, this);
    m_molecularAtomizationEnergyView = new MolecularAtomizationEnergyListView(m_database, this);
    m_reactionEnergyView = new ReactionEnergyListView(m_database, this);
    m_defaultValueSettings = new DefaultValueSettingsWidget(m_database, this);

    connect(m_geomView, SIGNAL(molecularGeometryNameUpdated(QString)), m_molecularFrequencyView, SLOT(reload()));
    connect(m_geomView, SIGNAL(molecularGeometryNameUpdated(QString)), m_molecularAtomizationEnergyView, SLOT(reload()));
    connect(m_geomView, SIGNAL(crystalGeometryNameUpdated(QString)), m_bandstructureView, SLOT(reload()));

    connect(m_database, SIGNAL(geometryUpdated()), m_molecularFrequencyView, SLOT(reload()));
    connect(m_database, SIGNAL(geometryUpdated()), m_molecularAtomizationEnergyView, SLOT(reload()));

    m_category->addTab(m_geomView, tr("Geometry"));
    m_category->addTab(m_bandstructureView, tr("BandStructures"));
    m_category->addTab(m_molecularFrequencyView, tr("Molecular Frequency"));
    m_category->addTab(m_molecularAtomizationEnergyView, tr("Molecular Atomization Energy"));
    m_category->addTab(m_reactionEnergyView, tr("Reaction Energy"));


//    QScrollArea *scrollArea = new QScrollArea;
//    scrollArea->setWidget(m_defaultValueSettings);


//    m_category->addTab(scrollArea, tr("Default Values"));

    this->setCentralWidget(m_category);

    QMenu *menu_file = new QMenu("&File");
//    menu_file->addAction("Import folderbase...", this, SLOT(importFolderBaseData()));

//    menu_file->addSeparator();
    menu_file->addAction("Exit", this, SLOT(close()));
    this->menuBar()->addMenu(menu_file);

    QMenu *menu_database = new QMenu("&Database");
    menu_database->addAction("Change database connection...", this, SLOT(changeDatabase()));
    menu_database->addAction("Reload", this, SLOT(databaseConnectionChanged()));
    this->menuBar()->addMenu(menu_database);

    QMenu *menu_test = new QMenu("&Test");
    menu_test->addAction("Import...", this, SLOT(importor()));
    menu_test->addAction("Export", this, SLOT(exportDB()));
//    menu_test->addAction("Migrate", this, SLOT(migrate()));
    this->menuBar()->addMenu(menu_test);

    QMenu *menu_help = new QMenu("&Help");
    menu_help->addAction("About Qt", this, SLOT(aboutQt()));
    menu_help->addAction("About", this, SLOT(about()));
    this->menuBar()->addMenu(menu_help);

}

void MainWindow::updateWindowTitle()
{
    QString connectionName = QString("(DB: %1, %2@%3)").arg(m_database->database().databaseName())
            .arg(m_database->database().userName())
            .arg(m_database->database().hostName());
    QString dbReadWriteState;
    if (!m_database->canWrite())
    {
        dbReadWriteState = "[ReadOnly]";
    }
    setWindowTitle("DFTBRefDatabaseManager" +
                   connectionName + dbReadWriteState);

}
