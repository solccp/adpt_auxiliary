#ifndef PREDEFINEDSETITEMEDITOR_H
#define PREDEFINEDSETITEMEDITOR_H

#include <QTreeWidgetItem>

#include <QDialog>

QT_BEGIN_NAMESPACE
class QLineEdit;
class QTextEdit;
QT_END_NAMESPACE

class PredefinedSetItemEditor : public QDialog
{
    Q_OBJECT
public:
    explicit PredefinedSetItemEditor(QWidget *parent = 0);

signals:

public slots:
    void setName(const QString& name);
    QString name();
    void setDesc(const QString& Desc);
    QString desc();
private slots:
    bool varify();

private:

    QLineEdit* m_editName;
    QTextEdit* m_editDesc;

};

#endif // PREDEFINEDSETITEMEDITOR_H
