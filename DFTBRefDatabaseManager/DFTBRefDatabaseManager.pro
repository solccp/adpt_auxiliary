#-------------------------------------------------
#
# Project created by QtCreator 2013-07-27T13:58:48
#
#-------------------------------------------------

DEFINES += BUGLESS_ZIP BUGLESS_TAR

QT       += core gui sql printsupport
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

TARGET = DFTBRefDatabaseManager
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
#    klineswidget.cpp \
#    bandstructureviewer.cpp \
#    atomsymbolcombodelegate.cpp \
#    crystalgeometryform.cpp \
#    clustergeometryeditor.cpp \
    utils.cpp \
#    bandstructuredataeditor.cpp \
    databaseopencreatedialog.cpp \
#    molecularfrequencyeditor.cpp \
#    dopwidget.cpp \
#    molecularatomizationenergyeditor.cpp \
#    reactioncomponentmodel.cpp \
#    reactioncomponentdelegate.cpp \
#    reactioncomponentwidget.cpp \
#    reactionenergydataeditor.cpp \
#    doublenumbercelldelegate.cpp \
    PeriodicTableWidget.cpp \
    ErepfitPotentialDefinitionWidget.cpp \
    PotentialDelegate.cpp \
    PotentialWidget.cpp \
    DirtyCellDelegate.cpp \
    IntCellDelegate.cpp \
    DefaultValueSettingsWidget.cpp \
    SqlRelationDelagate.cpp \
    sqlrelationaltablemodel.cpp \
#    singleselectlistwidget.cpp \
    MolecularFrequencyListView.cpp \
    GeometryListView.cpp \
    ElectronicDefaultConfiningDefinitionDialog.cpp \
    ConfiningDefaultFittingSetDialog.cpp \
    CheckableSqlRelationalTableModel.cpp \
    ErepfitDefAdditionalEqDialog.cpp \
    PredefinedSetsEditWidget.cpp \
    ReorderableTreeWidget.cpp \
    PredefinedSetItemEditor.cpp \
#    SingleSelectTableModelComboBox.cpp \
    DatabaseConnectingDialog.cpp \
    SimpleCrypt.cpp \
#    UpperCaseLineEdit.cpp \
    ElementSortFilterProxyModel.cpp \
    DataListViewBase.cpp \
    MolecularAtomizationEnergyListView.cpp \
    ReactionEnergyListView.cpp \
    BandstructureListView.cpp \
    YesNoCellItemDelegate.cpp \
    ErepfitReactionDialog.cpp \
    ErepfitInputEnergyEquationDialog.cpp \
    ErepfitInputForceEquationDialog.cpp \
    ReferenceDataImportor.cpp

HEADERS  += mainwindow.h \
    utils.h \
    databaseopencreatedialog.h \
    PeriodicTableWidget.h \
    ErepfitPotentialDefinitionWidget.h \
    PotentialDelegate.h \
    PotentialWidget.h \
    DirtyCellDelegate.h \
    IntCellDelegate.h \
    DefaultValueSettingsWidget.h \
    SqlRelationDelagate.h \
    sqlrelationaltablemodel.h \
    MolecularFrequencyListView.h \
    GeometryListView.h \
    ElectronicDefaultConfiningDefinitionDialog.h \
    ConfiningDefaultFittingSetDialog.h \
    CheckableSqlRelationalTableModel.h \
    ErepfitDefAdditionalEqDialog.h \
    PredefinedSetsEditWidget.h \
    ReorderableTreeWidget.h \
    PredefinedSetItemEditor.h \
    DatabaseConnectingDialog.h \
    SimpleCrypt.h \
    ElementSortFilterProxyModel.h \
    DataListViewBase.h \
    MolecularAtomizationEnergyListView.h \
    ReactionEnergyListView.h \
    BandstructureListView.h \
    VERSION_INFO.h \
    YesNoCellItemDelegate.h \
    ErepfitReactionDialog.h \
    ErepfitInputEnergyEquationDialog.h \
    ErepfitInputForceEquationDialog.h \
    ReferenceDataImportor.h

FORMS    += \
    klinesdialog.ui \

include(../settings.pri)
unix {
    target.path = $$PREFIX/adpt/bin
    INSTALLS += target
}

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libDFTBRefModels/release -lDFTBRefModels
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libDFTBRefModels/debug -lDFTBRefModels
else:unix: LIBS += -L$$OUT_PWD/../libDFTBRefModels/ -lDFTBRefModels


INCLUDEPATH += $$PWD/../libDFTBRefModels
DEPENDPATH += $$PWD/../libDFTBRefModels

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBRefModels/release/libDFTBRefModels.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBRefModels/debug/libDFTBRefModels.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBRefModels/release/DFTBRefModels.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBRefModels/debug/libDFTBRefModels.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libDFTBRefModels/libDFTBRefModels.a




win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libDFTBRefDatabase/release -lDFTBRefDatabase
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libDFTBRefDatabase/debug -lDFTBRefDatabase
else:unix: LIBS += -L$$OUT_PWD/../libDFTBRefDatabase/ -lDFTBRefDatabase

INCLUDEPATH += $$PWD/../libDFTBRefDatabase
DEPENDPATH += $$PWD/../libDFTBRefDatabase

OPENBABEL_ROOT = $$PWD/../external_libs/
INCLUDEPATH+= $$OPENBABEL_ROOT$$/include/openbabel-2.0
unix:LIBS+= -L$$OPENBABEL_ROOT$$/lib -lopenbabel -linchi -lz
win32: LIBS += -L$$PWD/../external_libs/win32/bin -lopenbabel -linchi

#win32: LIBS += -L$$PWD/../external_libs/win64/lib -lxml2 -lz -liconv

#INCLUDEPATH += $$PWD/../external_libs/yaml-cpp/include/ $$PWD/../external_libs/boost_for_yaml-cpp/

#unix:LIBS += $$PWD/../external_libs/lib/libyaml-cpp.a
#win32: LIBS += $$PWD/../external_libs/win64/lib/libyaml-cpp.a

#INCLUDEPATH += $$PWD/../libDFTBTestSuite
#DEPENDPATH += $$PWD/../libDFTBTestSuite



RESOURCES += \
    data.qrc

#unix:!macx: LIBS += -L$$OUT_PWD/../external_libs/archive/lib/ -larchive

#INCLUDEPATH += $$PWD/../external_libs/archive/Archive
#DEPENDPATH += $$PWD/../external_libs/archive/Archive

#INCLUDEPATH += $$PWD/../external_libs/archive/include
#DEPENDPATH += $$PWD/../external_libs/archive/include

INCLUDEPATH += $$PWD/../external_libs/include

#unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../external_libs/archive/lib/libarchive.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libDFTBOPTInputAdaptor/release -lDFTBOPTInputAdaptor
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libDFTBOPTInputAdaptor/debug -lDFTBOPTInputAdaptor
else:unix:!macx: LIBS += -L$$OUT_PWD/../libDFTBOPTInputAdaptor/ -lDFTBOPTInputAdaptor

INCLUDEPATH += $$PWD/../libDFTBOPTInputAdaptor
DEPENDPATH += $$PWD/../libDFTBOPTInputAdaptor

unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../libDFTBOPTInputAdaptor/libDFTBOPTInputAdaptor.a
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../libDFTBRefDatabase/libDFTBRefDatabase.a
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../libDFTBRefModels/libDFTBRefModels.a

unix:LIBS += $$PWD/../external_libs/lib/libVariant.a -lxml2 $$PWD/../external_libs/lib/libyaml.a 
win32:LIBS += $$PWD/../external_libs/win32/lib/libVariant.a $$PWD/../external_libs/win32/lib/libyaml.a -lregex


win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libDFTBTestSuiteCommon/release/ -lDFTBTestSuiteCommon
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libDFTBTestSuiteCommon/debug/ -lDFTBTestSuiteCommon
else:unix: LIBS += -L$$OUT_PWD/../libDFTBTestSuiteCommon/ -lDFTBTestSuiteCommon

INCLUDEPATH += $$PWD/../libDFTBTestSuiteCommon
DEPENDPATH += $$PWD/../libDFTBTestSuiteCommon

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuiteCommon/release/libDFTBTestSuiteCommon.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuiteCommon/debug/libDFTBTestSuiteCommon.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuiteCommon/release/DFTBTestSuiteCommon.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuiteCommon/debug/DFTBTestSuiteCommon.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuiteCommon/libDFTBTestSuiteCommon.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libadpt_common/release/ -ladpt_common
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libadpt_common/debug/ -ladpt_common
else:unix: LIBS += -L$$OUT_PWD/../libadpt_common/ -ladpt_common

INCLUDEPATH += $$PWD/../libadpt_common
DEPENDPATH += $$PWD/../libadpt_common

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/release/libadpt_common.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/debug/libadpt_common.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/release/adpt_common.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/debug/adpt_common.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/libadpt_common.a
