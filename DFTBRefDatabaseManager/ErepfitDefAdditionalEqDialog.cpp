#include "ErepfitDefAdditionalEqDialog.h"

#include "PotentialDelegate.h"
#include "doublenumbercelldelegate.h"
#include "IntCellDelegate.h"

#include <QTableView>
#include <QSqlTableModel>
#include <QVBoxLayout>
#include <QHeaderView>
#include <QMessageBox>
#include <QPushButton>
#include <QSqlRecord>
#include <QSqlError>
#include <QCloseEvent>

#include "dftbrefdatabase.h"

ErepfitDefAdditionalEqDialog::ErepfitDefAdditionalEqDialog(DFTBRefDatabase *database, QWidget *parent) :
    QDialog(parent), m_database(database)
{
    m_model = new QSqlTableModel(this, m_database->database());
    m_model->setTable("erepfit_default_input_addition_eq");
    m_model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    m_model->select();
    m_tableView = new QTableView(this);
    m_tableView->setSelectionBehavior(QAbstractItemView::SelectRows);

    m_tableView->setModel(m_model);
    m_tableView->setAlternatingRowColors(true);


    m_tableView->setItemDelegateForColumn(0, new PotentialDelegate());
    m_tableView->setItemDelegateForColumn(1, new DoubleNumberCellDelegate());
    m_tableView->setItemDelegateForColumn(2, new IntCellDelegate());
    m_tableView->setItemDelegateForColumn(3, new DoubleNumberCellDelegate());
    m_tableView->setItemDelegateForColumn(4, new DoubleNumberCellDelegate());
    m_tableView->setItemDelegateForColumn(5, new DoubleNumberCellDelegate());

    this->resize(600,400);
    m_tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    m_tableView->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Fixed);
    m_tableView->setColumnWidth(0,130);

    QVBoxLayout *layout = new QVBoxLayout(this);
    this->setLayout(layout);


    QHBoxLayout *hbox = new QHBoxLayout();
    QPushButton* butAdd = new QPushButton(tr("Add"));
    connect(butAdd, SIGNAL(clicked()), this, SLOT(addNewPotential()));
    hbox->addWidget(butAdd);
    QPushButton* butDel = new QPushButton(tr("Remove"));
    connect(butDel, SIGNAL(clicked()), this, SLOT(removePotential()));
    hbox->addWidget(butDel);

    QPushButton* butCommit = new QPushButton(tr("Commit"));
    connect(butCommit, SIGNAL(clicked()), this, SLOT(commitBut()));
    hbox->addWidget(butCommit);

    QPushButton* butRevert = new QPushButton(tr("Revert"));
    connect(butRevert, SIGNAL(clicked()), this, SLOT(revert()));
    hbox->addWidget(butRevert);
    hbox->addStretch(5);

    layout->addLayout(hbox);
    layout->addWidget(m_tableView);


    QPushButton* butClose = new QPushButton(tr("Close"));
    butClose->setDefault(true);
    connect(butClose, SIGNAL(clicked()), this, SLOT(close()));


    hbox = new QHBoxLayout();

    hbox->addStretch(5);
    hbox->addWidget(butClose);
    layout->addLayout(hbox);

    if (!m_database->canWrite())
    {
        butAdd->setEnabled(false);
        butRevert->setEnabled(false);
        butCommit->setEnabled(false);
        butDel->setEnabled(false);
        m_tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    }

}

void ErepfitDefAdditionalEqDialog::addNewPotential()
{
    int row = m_model->rowCount();
    m_model->insertRow(row);
    m_tableView->edit(m_model->index(row,0));
}

void ErepfitDefAdditionalEqDialog::removePotential()
{
    QModelIndexList list = m_tableView->selectionModel()->selectedRows();
    if (!list.empty())
    {
        for (int i=list.size()-1; i>=0; --i)
        {
            int row = list.at(i).row();
            m_tableView->model()->removeRows(row,1);
        }
    }
}

void ErepfitDefAdditionalEqDialog::commitBut()
{
    if (m_model->isDirty() && commit())
    {
        QMessageBox qbox2(this);
        qbox2.setText("Data written into database.");
        qbox2.setStandardButtons(QMessageBox::Ok);
        qbox2.setIcon(QMessageBox::Information);
        qbox2.exec();
    }
}

bool ErepfitDefAdditionalEqDialog::commit()
{
    m_model->database().transaction();
    if(m_model->submitAll())
    {
        m_model->database().commit();
    }
    else
    {
        m_model->database().rollback();
        QMessageBox qbox2(this);
        qbox2.setText("Failed to write data into database:" + m_model->lastError().text());
        qbox2.setStandardButtons(QMessageBox::Ok);
        qbox2.setIcon(QMessageBox::Critical);
        qbox2.exec();
        return false;
    }
    return true;
}

void ErepfitDefAdditionalEqDialog::revert()
{
    m_model->revertAll();
}


void ErepfitDefAdditionalEqDialog::closeEvent(QCloseEvent *event)
{
    if (m_model->isDirty())
    {
        QMessageBox qbox(this);
        qbox.setText("The changes have not been updated in the database.\n"
                     "Do you want to update them?");
        qbox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        qbox.setIcon(QMessageBox::Question);
        int ret = qbox.exec();
        if (ret == QMessageBox::Yes)
        {
            bool succ = commit();
            if (!succ)
            {
                event->ignore();
                return;
            }
        }
        else
        {
            event->accept();
        }
    }
    event->accept();
}
