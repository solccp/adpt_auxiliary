#ifndef PREDEFINEDSETSEDITWIDGET_H
#define PREDEFINEDSETSEDITWIDGET_H

#include <QDialog>
#include <QWidget>
#include <QTreeWidget>
#include <QSplitter>
#include <QTableView>
#include <QHash>
#include "dftbrefdatabase.h"


QT_BEGIN_NAMESPACE
class QSortFilterProxyModel;
class QSqlTableModel;
QT_END_NAMESPACE

class ReorderableTreeWidget;
class PredefinedSetsEditWidget : public QDialog
{
    Q_OBJECT
public:
    PredefinedSetsEditWidget(DFTBRefDatabase* database, bool editable = true, QWidget *parent = 0);

    bool isEditable() const;
    void setIsEditable(bool isEditable);

signals:
    void selectionChanged(const QSet<QString>& selectedIds);
public slots:
    QSet<QString> selectedSets() const;

private slots:
    void itemClicked(QTreeWidgetItem* item, int column);
    void itemReordered(QTreeWidgetItem* moveditem, QTreeWidgetItem* old_parent);
    void itemDoubleClicked(QTreeWidgetItem* item, int column);
    void itemSelectionChanged();

    void treeViewContextMenu(const QPoint& pos);
    void tableViewContextMenu(const QPoint& pos);

    void tableCellDoubleClicked(const QModelIndex& index);
    void tableViewRemoveSelected();

    void addSubSet();
    void removeSet();
    void exportSet();

    void removeTableItems(const QString& set_id);

private:
    bool m_isEditable = true;
    bool isTreeDirty();
    bool isTableDirty();
    void checkParentState(QTreeWidgetItem* item);
    void checkChildState(QTreeWidgetItem* item);

    QList<QString> allSetIds(QTreeWidgetItem* item);

    ReorderableTreeWidget* m_treeView;
    QTableView* m_tableView;
    QSortFilterProxyModel* m_model_table;

    QSqlTableModel* m_model_tree;


    QSplitter *m_splitter;
    void loadItems();

    DFTBRefDatabase* m_database;
    QSet<QString> m_selectedSets;

    void updateTreeModel();

    // QWidget interface
protected:
    void closeEvent(QCloseEvent *event);
};

#endif // PREDEFINEDSETSEDITWIDGET_H
