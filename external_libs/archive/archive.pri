
# include this pri file in your project file to 
# add the bugless::Archive headers and libraries to 
# your project

# dont forget to set ARCHIVEHOME to the directory containing this file
ARCHIVEHOME = $$IN_PWD

ARCHIVEHOME = $$PWD
isEmpty( ARCHIVEHOME ) { 
    warning( "Couldn't locate archive dir: has the environment ARCHIVEHOME been set?" )
}

exists ( $$ARCHIVEHOME/ ) {

HEADERS += $${ARCHIVEHOME}/include/Archive.h \
           $${ARCHIVEHOME}/include/ArchiveIterator.h 
           
INCLUDEPATH += $${ARCHIVEHOME}/include


macx {
    QMAKE_LFLAGS += -noprebind
}

!win32 {
    # ensure that Archive is linked in before zLib
    LIBS += -L$${ARCHIVEHOME}/lib -larchive -lz
}
win32 {
    LIBS += -L$${ARCHIVEHOME}/lib -larchive
}

}
