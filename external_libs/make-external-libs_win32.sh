#!/bin/bash

#TOOLCHAIN=i686-w64-mingw32.static
TOOLCHAIN=x86_64-w64-mingw32.static

mkdir build_yaml -p
cd build_yaml
../yaml-0.1.5/configure --host=$TOOLCHAIN --prefix=`pwd`/../win32  --enable-static --disable-shared 
make && make install
cd ..
rm -rf build_yaml

mkdir build_libvariant -p
cd build_libvariant
$TOOLCHAIN-cmake -DCMAKE_INSTALL_PREFIX=../win32 -DEXAMPLES_AND_TESTS=OFF -DLIBYAML_LIBRARIES=../win32/lib/libyaml.a -DLIBYAML_INCLUDE_DIR=../win32/include ../libvariant/
make && make install
cd ..
rm -rf build_libvariant

mkdir build_ob -p
cd build_ob
$TOOLCHAIN-cmake -DCMAKE_INSTALL_PREFIX=../win32 -DEIGEN3_INCLUDE_DIR=../Eigen ../openbabel-2.3.2/
make && make install
cd ..
rm -rf build_ob

