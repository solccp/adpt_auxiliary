#!/bin/bash

mkdir -p build_yaml
cd build_yaml
../yaml-0.1.5/configure --prefix=`pwd`/../
make && make install
cd ..
rm -rf build_yaml

mkdir build_libvariant -p
cd build_libvariant
cmake -DLIBVARIANT_BUILD_DEBUG=ON -DCMAKE_INSTALL_PREFIX=../ ../libvariant/ -DLIBYAML_LIBRARIES=../lib/libyaml.a -DLIBYAML_INCLUDE_DIR=../include
make && make test && make install
cd ..
rm -rf build_libvariant


mkdir build_ob -p
cd build_ob
cmake -DCMAKE_INSTALL_PREFIX=../ -DEIGEN3_INCLUDE_DIR=../Eigen ../openbabel-2.3.2/
make && make install
cd ..
rm -rf build_ob


