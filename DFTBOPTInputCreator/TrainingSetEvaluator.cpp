#include "TrainingSetEvaluator.h"
#include "DFTBTestSuite/dftbtester.h"

#include <QTextStream>
#include <QByteArray>
#include <QDebug>

TrainingSetEvaluator::TrainingSetEvaluator(DFTBTestSuite::ReferenceData *refData, DFTBTestSuite::InputData *inputData, QObject *parent)
    :QThread(parent)
{
    m_refData = refData;
    m_inputData = inputData;

}

void TrainingSetEvaluator::stop()
{
    m_tester->stop();
}

void TrainingSetEvaluator::progressChangedSlot(const QString &msg)
{
    emit progressChanged(msg);
}

void TrainingSetEvaluator::progressChangedSlot(int percentage)
{
    emit progressChanged(percentage);
}

const DFTBTestSuite::Statistics * TrainingSetEvaluator::getStatistics() const
{
    return m_tester->getStatistics();
}

const DFTBTestSuite::EvaluatedData* TrainingSetEvaluator::getEvaluatedData() const
{
    return m_tester->getEvalutedData();
}

void TrainingSetEvaluator::run()
{
    if (m_tester)
    {
        delete m_tester;
    }
    m_tester = new DFTBTestSuite::DFTBTester(*m_inputData, m_refData, this);
    m_tester->setShowErrorUnitResult(false);
    m_tester->setAutoRemove(false);
    connect(m_tester, SIGNAL(progressUpdate(QString)), this, SLOT(progressChangedSlot(QString)));

    emit progressChanged("Evaluating...");
    emit progressChanged(30);
    QByteArray res;
    QTextStream out_stream(&res, QIODevice::WriteOnly | QIODevice::Text);
    bool succ = m_tester->evaluate(out_stream, out_stream);

    if (succ)
    {
        emit resultReady(QString(res));
        emit progressChanged("Finished");
        emit progressChanged(100);
    }
    else
    {
        emit resultReady("Cancelled.");
        emit progressChanged("Failed");
        emit progressChanged(100);
    }

}
