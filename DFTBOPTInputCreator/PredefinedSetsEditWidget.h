#ifndef PREDEFINEDSETSEDITWIDGET_H
#define PREDEFINEDSETSEDITWIDGET_H

//#include <QDialog>
#include <QWidget>
#include <QTreeWidget>
#include <QSplitter>
#include <QTableView>
#include <QHash>
#include "dftbrefdatabase.h"
#include "ElementSortFilterProxyModel.h"


QT_BEGIN_NAMESPACE
class QSqlTableModel;
class QTreeWidget;
QT_END_NAMESPACE

class SqlRelationalTableModel;
typedef QTreeWidget ReorderableTreeWidget;
//class ReorderableTreeWidget;
class PredefinedSetsEditWidget : public QWidget
{
    Q_OBJECT
public:
    PredefinedSetsEditWidget(DFTBRefDatabase* database, bool editable = true, QWidget *parent = 0);

    bool isEditable() const;
    void setIsEditable(bool isEditable);

    void setTableItemCheckState();


    QHash<QPair<int, QString>, double> getTestSuiteSet();


signals:
    void selectionChanged(const QSet<QString>& selectedIds);
public slots:
    QSet<QString> selectedSets() const;
    void setElementFilter(const QStringList& elems);
    void loaditems();
private slots:
    void itemClicked(QTreeWidgetItem* item, int column);
    void itemReordered(QTreeWidgetItem* moveditem, QTreeWidgetItem* old_parent);
    void itemDoubleClicked(QTreeWidgetItem* item, int column);
    void itemSelectionChanged();

    void treeViewContextMenu(const QPoint& pos);
    void tableViewContextMenu(const QPoint& pos);

    void tableViewRemoveSelected();
    void tableCellDoubleClicked(const QModelIndex& index);

private:
    bool m_isEditable = true;
    bool isTreeDirty();
    bool isTableDirty();
    void checkParentState(QTreeWidgetItem* item);
    void checkChildState(QTreeWidgetItem* item);

    QList<QString> allSetIds(QTreeWidgetItem* item);

    ReorderableTreeWidget* m_treeView;
    QTableView* m_tableView;
    ElementSortFilterProxyModel* m_model_table = nullptr;
    QSqlTableModel* m_model_tree = nullptr;
    SqlRelationalTableModel *sourceModel = nullptr;


    QSplitter *m_splitter;
    void loadTreeItems();
    DFTBRefDatabase* m_database;
    QSet<QString> m_selectedSets;
};

#endif // PREDEFINEDSETSEDITWIDGET_H
