#include "TrainingSetPage.h"
#include "dftbrefdatabase.h"
#include "ElementFilteredTableModel.h"

#include "geometry.h"
#include "clustergeometryeditor.h"
#include "bandstructuredata.h"
#include "bandstructuredataeditor.h"
#include "molecularfrequencydata.h"
#include "molecularfrequencyeditor.h"
#include "reactionenergydata.h"
#include "reactionenergydataeditor.h"


#include "PropertyTableView.h"
#include <QVBoxLayout>
#include <QHeaderView>
#include <QtWidgets>
#include <QSqlTableModel>

#include "Profile.h"
#include "YesNoCellItemDelegate.h"

#include <QDebug>

TrainingSetPage::TrainingSetPage(DFTBRefDatabase *database, QWidget *parent) :
    QWizardPage(parent), m_database(database)
{
    setTitle("Training Set");
    setSubTitle("Select interested properties into the training set.");
    m_tabWidget = new QTabWidget();

    m_tableGeometry = new PropertyTableView(m_database);
    m_tabWidget->addTab(m_tableGeometry, "Geometry");
    connect(m_tableGeometry, SIGNAL(itemDblClicked(QString)), this, SLOT(showGeometryDetail(QString)));

    m_tableBandstructure = new PropertyTableView(m_database);
    m_tabWidget->addTab(m_tableBandstructure, "Bandstructure");
    connect(m_tableBandstructure, SIGNAL(itemDblClicked(QString)), this, SLOT(showBandstructureDetail(QString)));

    m_tableFrequency = new PropertyTableView(m_database);
    m_tabWidget->addTab(m_tableFrequency, "Frequency");
    connect(m_tableFrequency, SIGNAL(itemDblClicked(QString)), this, SLOT(showFrequencyDetail(QString)));

    m_tableAtomizationEnergy = new PropertyTableView(m_database);
    m_tabWidget->addTab(m_tableAtomizationEnergy, "AtomizationEnergy");

    m_tableReactionEnergy = new PropertyTableView(m_database);
    m_tabWidget->addTab(m_tableReactionEnergy, "ReactionEnergy");
    connect(m_tableReactionEnergy, SIGNAL(itemDblClicked(QString)), this, SLOT(showReactionEnergyDetail(QString)));

    QVBoxLayout *layout =  new QVBoxLayout(this);
    layout->setMargin(0);

    menubar =  new QMenuBar();
    menu_Adv = menubar->addMenu("&Advanced");

    action_erepfit = menu_Adv->addAction("Erepfit equations...");

    connect(action_erepfit, &QAction::triggered, [this](){this->emit showGeneratingSetPage();});

    action_save_select = menu_Adv->addAction("Save selection...");
    connect(action_save_select, &QAction::triggered, [this](){this->emit saveSelection();});
    action_load_select = menu_Adv->addAction("Load selection...");
    connect(action_load_select, &QAction::triggered, [this](){this->emit loadSelection();});

    layout->addWidget(menubar);
    layout->addWidget(m_tabWidget);
    this->setLayout(layout);


    m_tableGeometry->setTableName("MoleculeGeometryListView");
    m_tableGeometry->setCheckableColumnIndex(0);

    m_tableBandstructure->setTableName("BandstructureListView");
    m_tableBandstructure->setCheckableColumnIndex(0);

    m_tableFrequency->setTableName("MolecularFrequencyListView");
    m_tableFrequency->setCheckableColumnIndex(0);

    m_tableAtomizationEnergy->setTableName("EnergyEquationList");
    m_tableAtomizationEnergy->setCheckableColumnIndex(0);

    m_tableReactionEnergy->setTableName("ReactionEnergyListView");
    m_tableReactionEnergy->setCheckableColumnIndex(0);
    m_tableReactionEnergy->setItemDelegateForColumn(6, new YesNoCellItemDelegate());



    connect(m_tableGeometry, SIGNAL(itemSelectionChanged(bool,QString,double)),
            this, SLOT(propertySelectionChanged(bool,QString,double)));
    connect(m_tableBandstructure, SIGNAL(itemSelectionChanged(bool,QString,double)),
            this, SLOT(propertySelectionChanged(bool,QString,double)));
    connect(m_tableFrequency, SIGNAL(itemSelectionChanged(bool,QString,double)),
            this, SLOT(propertySelectionChanged(bool,QString,double)));

    connect(m_tableAtomizationEnergy, SIGNAL(itemSelectionChanged(bool,QString,double)),
            this, SLOT(propertySelectionChanged(bool,QString,double)));
    connect(m_tableReactionEnergy, SIGNAL(itemSelectionChanged(bool,QString,double)),
            this, SLOT(propertySelectionChanged(bool,QString,double)));

}

TrainingSetPage::~TrainingSetPage()
{
}
QList<QPair<QString, double> > TrainingSetPage::geometryTargets() const
{
    QList<QPair<QString, double> > res;
    QHashIterator<QString, double> it(m_tableGeometry->model()->checkedItems());
    while(it.hasNext())
    {
        it.next();
        res.append(qMakePair(it.key(), it.value()));
    }
    return res;
}

QList<QPair<QString, double> > TrainingSetPage::bandstructureTargets() const
{
    QList<QPair<QString, double> > res;
    QHashIterator<QString, double> it(m_tableBandstructure->model()->checkedItems());
    while(it.hasNext())
    {
        it.next();
        res.append(qMakePair(it.key(), it.value()));
    }
    return res;
}


QList<QPair<QString, double> > TrainingSetPage::frequencyTargets() const
{
    QList<QPair<QString, double> > res;
    QHashIterator<QString, double> it(m_tableFrequency->model()->checkedItems());
    while(it.hasNext())
    {
        it.next();
        res.append(qMakePair(it.key(), it.value()));
    }
    return res;
}


QList<QPair<QString, double> > TrainingSetPage::atomizationEnergyTargets() const
{
    QList<QPair<QString, double> > res;
    QHashIterator<QString, double> it(m_tableAtomizationEnergy->model()->checkedItems());
    while(it.hasNext())
    {
        it.next();
        res.append(qMakePair(it.key(), it.value()));
    }
    return res;
}


QList<QPair<QString, double> > TrainingSetPage::reactionEnergyTargets() const
{
    QList<QPair<QString, double> > res;
    QHashIterator<QString, double> it(m_tableReactionEnergy->model()->checkedItems());
    while(it.hasNext())
    {
        it.next();
        res.append(qMakePair(it.key(), it.value()));
    }
    return res;
}

void TrainingSetPage::setSelectedElements(const QStringList &elements)
{
    m_elements = elements;
    //Geometry
    m_tableGeometry->setElements(m_elements);
    m_tableBandstructure->setElements(m_elements);
    m_tableFrequency->setElements(m_elements);
    m_tableAtomizationEnergy->setElements(m_elements);
    m_tableReactionEnergy->setElements(m_elements);

}

QStringList TrainingSetPage::selectedElements() const
{
    return m_elements;
}

void TrainingSetPage::reload()
{
    qDebug() << "TrainingSetPage::reload";

//    m_tableGeometry->hideColumn(5);
    m_tableGeometry->hideColumn(6);
    m_tableGeometry->reload();


    m_tableBandstructure->reload();
    m_tableFrequency->reload();

    m_tableAtomizationEnergy->reload();


//    m_tableReactionEnergy->hideColumn(6);
    m_tableReactionEnergy->reload();

    qDebug() << "TrainingSetPage::loaded";

}

void TrainingSetPage::postload()
{
    qDebug() << "TrainingSetPage::postload";
    m_tableGeometry->postload();
    m_tableBandstructure->postload();
    m_tableFrequency->postload();
    m_tableAtomizationEnergy->postload();
    m_tableReactionEnergy->postload();
    qDebug() << "TrainingSetPage::postload finished";
}

void TrainingSetPage::updateSelection(const QList<PropertySelection *> &properties)
{
    for(int i=0; i<properties.size();++i)
    {
        if (properties[i]->propertyName() == "geometry")
        {
            auto it = properties[i]->getIterator();
            while(it.hasNext())
            {
                it.next();
                this->m_tableGeometry->updateItem(true, it.key(), it.value());
            }
        }
        else if (properties[i]->propertyName() == "bandstructure")
        {
            auto it = properties[i]->getIterator();
            while(it.hasNext())
            {
                it.next();
                this->m_tableBandstructure->updateItem(true, it.key(), it.value());
            }
        }
        else if (properties[i]->propertyName() == "frequency")
        {
            auto it = properties[i]->getIterator();
            while(it.hasNext())
            {
                it.next();
                this->m_tableFrequency->updateItem(true, it.key(), it.value());
            }
        }
        else if (properties[i]->propertyName() == "atomizationenergy")
        {
            auto it = properties[i]->getIterator();
            while(it.hasNext())
            {
                it.next();
                this->m_tableAtomizationEnergy->updateItem(true, it.key(), it.value());
            }
        }
        else if (properties[i]->propertyName() == "reactionenergy")
        {
            auto it = properties[i]->getIterator();
            while(it.hasNext())
            {
                it.next();
                this->m_tableReactionEnergy->updateItem(true, it.key(), it.value());
            }
        }
    }

}

void TrainingSetPage::clear()
{
    m_tableAtomizationEnergy->selectNoItems();
    m_tableFrequency->selectNoItems();
    m_tableBandstructure->selectNoItems();
    m_tableGeometry->selectNoItems();
    m_tableReactionEnergy->selectNoItems();
}

void TrainingSetPage::showGeometryDetail(const QString &uuid)
{
    Geometry geom;
    if (!m_database->getGeometry(uuid, &geom))
        return;

    QDialog dia;
    dia.resize(800,600);
    QGridLayout *vb =new QGridLayout();

    bool ReadOnly = true;

    ClusterGeometryEditor* e = new ClusterGeometryEditor(&geom, ReadOnly);


    vb->addWidget(e,0,0,1,6);

    QPushButton *butOK = new QPushButton(tr("OK"));

    connect(butOK, SIGNAL(clicked()), &dia, SLOT(accept()));


    if (ReadOnly)
    {
        vb->addWidget(butOK, 1,5,1,1);
    }
    else
    {
        QPushButton *butCancel = new QPushButton(tr("Cancel"));
        connect(butCancel, SIGNAL(clicked()), &dia, SLOT(reject()));
        vb->addWidget(butOK, 1,4,1,1);
        vb->addWidget(butCancel, 1,5,1,1);
    }

    dia.setLayout(vb);

    dia.exec();
}

void TrainingSetPage::showBandstructureDetail(const QString &uuid)
{
    BandstructureData mydata;
    if (!m_database->getBandStructure(uuid, &mydata))
        return;

    QDialog dia;
    dia.resize(800,600);
    QGridLayout *vb =new QGridLayout();


    QSqlTableModel *model = new QSqlTableModel(this, m_database->database());
    model->setTable("CrystalGeometry_IDSTR");
    model->select();

    bool ReadOnly = true;

    BandstructureDataEditor* e = new BandstructureDataEditor(&mydata, model, ReadOnly);


    vb->addWidget(e,0,0,1,6);

    QPushButton *butOK = new QPushButton(tr("OK"));
    connect(butOK, SIGNAL(clicked()), &dia, SLOT(accept()));

    if (ReadOnly)
    {
        vb->addWidget(butOK, 1,5,1,1);
    }
    else
    {
        QPushButton *butCancel = new QPushButton(tr("Cancel"));
        connect(butCancel, SIGNAL(clicked()), &dia, SLOT(reject()));
        vb->addWidget(butOK, 1,4,1,1);
        vb->addWidget(butCancel, 1,5,1,1);
    }

    dia.setLayout(vb);

    dia.exec();
}

void TrainingSetPage::showFrequencyDetail(const QString &uuid)
{
    MolecularFrequencyData data;

    m_database->getMolecularFrequency(uuid, &data);

    QDialog dia;
    dia.resize(800,600);
    QGridLayout *vb =new QGridLayout();

    QSqlTableModel *model = new QSqlTableModel(this, m_database->database());
    model->setTable("MolecularGeometry_IDSTR");
    model->select();

    bool ReadOnly = true;

    MolecularFrequencyDataEditor* e = new MolecularFrequencyDataEditor(&data, model, ReadOnly);
    vb->addWidget(e,0,0,1,6);

    QPushButton *butOK = new QPushButton(tr("OK"));
    connect(butOK, SIGNAL(clicked()), &dia, SLOT(accept()));

    if (!ReadOnly)
    {
        QPushButton *butCancel = new QPushButton(tr("Cancel"));
        connect(butCancel, SIGNAL(clicked()), &dia, SLOT(reject()));
        vb->addWidget(butOK, 1,4,1,1);
        vb->addWidget(butCancel, 1,5,1,1);
    }
    else
    {
        vb->addWidget(butOK, 1,5,1,1);
    }

    dia.setLayout(vb);

    dia.exec();
}


void TrainingSetPage::showReactionEnergyDetail(const QString &uuid)
{
    ReactionEnergyData data;

    m_database->getReactionEnergy(uuid, &data);

    QDialog dia;
    dia.resize(800,600);
    QGridLayout *vb =new QGridLayout();


    bool ReadOnly = true;

    ReactionEnergyDataEditor* e = new ReactionEnergyDataEditor(&data, m_database, ReadOnly);
    vb->addWidget(e,0,0,1,6);

    QPushButton *butOK = new QPushButton(tr("OK"));

    connect(butOK, SIGNAL(clicked()), &dia, SLOT(accept()));

    if (!ReadOnly)
    {

        QPushButton *butCancel = new QPushButton(tr("Cancel"));
        connect(butCancel, SIGNAL(clicked()), &dia, SLOT(reject()));

        vb->addWidget(butOK, 1,4,1,1);
        vb->addWidget(butCancel, 1,5,1,1);
    }
    else
    {
        vb->addWidget(butOK, 1,5,1,1);
    }
    dia.setLayout(vb);

    dia.exec();
}

void TrainingSetPage::propertySelectionChanged(bool selected, const QString &uuid, double weight)
{
    PropertyTableView* table = qobject_cast<PropertyTableView*>(sender());
    if (table == this->m_tableGeometry)
    {
        emit selectionChanged(Property_Geometry, selected, uuid, weight);
        emit selectionChanged("Geometry", selected, uuid, weight);
    }
    else if (table == this->m_tableBandstructure)
    {
        emit selectionChanged(Property_Bandstructure, selected, uuid, weight);
        emit selectionChanged("Bandstructure", selected, uuid, weight);
    }
    else if (table == this->m_tableFrequency)
    {
        emit selectionChanged(Property_Frequency, selected, uuid, weight);
        emit selectionChanged("Frequency", selected, uuid, weight);
    }
    else if (table == this->m_tableAtomizationEnergy)
    {
        emit selectionChanged(Property_AtomizationEnergy, selected, uuid, weight);
        emit selectionChanged("AtomizationEnergy", selected, uuid, weight);
    }
    else if (table == this->m_tableReactionEnergy)
    {
        emit selectionChanged(Property_ReactionEnergy, selected, uuid, weight);
        emit selectionChanged("ReactionEnergy", selected, uuid, weight);
    }
}


void TrainingSetPage::initializePage()
{

}



bool TrainingSetPage::validatePage()
{
    int totalTargets = 0;
    totalTargets += m_tableGeometry->model()->checkedItems().size();
    totalTargets += m_tableBandstructure->model()->checkedItems().size();
    totalTargets += m_tableFrequency->model()->checkedItems().size();
    totalTargets += m_tableAtomizationEnergy->model()->checkedItems().size();
    totalTargets += m_tableReactionEnergy->model()->checkedItems().size();
    return (totalTargets > 0);
}
