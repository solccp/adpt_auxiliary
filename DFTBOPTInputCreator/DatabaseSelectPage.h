#ifndef DATABASESELECTPAGE_H
#define DATABASESELECTPAGE_H

#include <QWizardPage>
#include <QtWidgets>
#include "DatabaseConnectingDialog.h"

class DFTBRefDatabase;
class DatabaseSelectPage : public QWizardPage
{
    Q_OBJECT
public:
    DatabaseSelectPage(DFTBRefDatabase* database, QWidget *parent = 0);

signals:

public slots:
private slots:
    void configDatabase();
private:
    QRadioButton *openRadioButton;
    QRadioButton *connectRadioButton;
    QPushButton* butConnectDb;
    DFTBRefDatabase* m_database;
    DatabaseConnectingDialog dia;
    QLabel *labelProfileName;

    // QWizardPage interface
public:
    bool validatePage();


    // QWizardPage interface
public:
    void initializePage();
};

#endif // DATABASESELECTPAGE_H
