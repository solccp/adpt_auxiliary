#-------------------------------------------------
#
# Project created by QtCreator 2014-01-13T14:35:33
#
#-------------------------------------------------

DEFINES += BUGLESS_ZIP BUGLESS_TAR

QT       += core gui sql printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

TARGET = DFTBOPTInputCreator
TEMPLATE = app

include(../settings.pri)
unix {
    target.path = $$PREFIX/adpt/bin
    INSTALLS += target
}


SOURCES += main.cpp\
    databaseopencreatedialog.cpp \
    HubburdParameter.cpp \
    ElementSelectPage.cpp \
    DatabaseSelectPage.cpp \
    SelectionListWidget.cpp \
    FinalPage.cpp \
    ElectronicFittingSetPage.cpp \
    InputCreatorWizard.cpp \
#    ImplementationKitDialog.cpp \
#    ImplementationTestingThread.cpp \
    TestSuiteTestSetPage.cpp \
    PredefinedSetsEditWidget.cpp \
    sqlrelationaltablemodel.cpp \
    ElementSortFilterProxyModel.cpp \
    DatabaseConnectingDialog.cpp \
    SimpleCrypt.cpp \
    MiscellaneousOptionPage.cpp \
    GeneratingSetPage.cpp \
    ElementFilteredTableModel.cpp \
    TrainingSetPage.cpp \
#    TrainingSetEvaluationPage.cpp \
    PropertyTableView.cpp \
#    TrainingSetEvaluator.cpp \
    PrepareInputThread.cpp \
    FetchingThread.cpp \
#    EvaluatedResultWindow.cpp \
    ErepfitAdditionalEquationWidget.cpp \
    AdvancedOptions.cpp \
    RepulsivePotentialDefinitionWidget.cpp \
    AtomicParamatersWidget.cpp \
    Profile.cpp \
    YesNoCellItemDelegate.cpp \
    Exportor.cpp

HEADERS  +=  databaseopencreatedialog.h \
    Singleton.h \
    HubburdParameter.h \
    ElementSelectPage.h \
    DatabaseSelectPage.h \
    SelectionListWidget.h \
    FinalPage.h \
    ElectronicFittingSetPage.h \
    InputCreatorWizard.h \
#    ImplementationKitDialog.h \
#    ImplementationTestingThread.h \
    TestSuiteTestSetPage.h \
    PredefinedSetsEditWidget.h \
    sqlrelationaltablemodel.h \
    ElementSortFilterProxyModel.h \
    DatabaseConnectingDialog.h \
    SimpleCrypt.h \
    MiscellaneousOptionPage.h \
    GeneratingSetPage.h \
    ElementFilteredTableModel.h \
    TrainingSetPage.h \
#    TrainingSetEvaluationPage.h \
    PropertyTableView.h \
#    TrainingSetEvaluator.h \
    PrepareInputThread.h \
    FetchingThread.h \
#    EvaluatedResultWindow.h \
    ErepfitAdditionalEquationWidget.h \
    AdvancedOptions.h \
    RepulsivePotentialDefinitionWidget.h \
    AtomicParamatersWidget.h \
    Profile.h \
    YesNoCellItemDelegate.h \
    Exportor.h

FORMS    += \
    databaseopencreatedialog.ui


RESOURCES += \
    ../defaultValues/onecent_input.qrc \
    ../defaultValues/atomic_info.qrc \
    ../defaultValues/dftbplus_input.qrc \
    default.qrc

INCLUDEPATH += $$PWD/../libDFTBRefDatabase
DEPENDPATH += $$PWD/../libDFTBRefDatabase
INCLUDEPATH += $$PWD/../libDFTBOPTInputAdaptor
DEPENDPATH += $$PWD/../libDFTBOPTInputAdaptor


win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libDFTBRefModels/release -lDFTBRefModels
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libDFTBRefModels/debug -lDFTBRefModels
else:unix: LIBS += -L$$OUT_PWD/../libDFTBRefModels/ -lDFTBRefModels

INCLUDEPATH += $$PWD/../libDFTBRefModels
DEPENDPATH += $$PWD/../libDFTBRefModels


win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBRefModels/release/libDFTBRefModels.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBRefModels/debug/libDFTBRefModels.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBRefModels/release/DFTBRefModels.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBRefModels/debug/libDFTBRefModels.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libDFTBRefModels/libDFTBRefModels.a

unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../libDFTBRefModels/libDFTBRefModels.a

unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../libDFTBRefDatabase/libDFTBRefDatabase.a
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../libDFTBOPTInputAdaptor/libDFTBOPTInputAdaptor.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libDFTBRefDatabase/release -lDFTBRefDatabase
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libDFTBRefDatabase/debug -lDFTBRefDatabase
else:unix:!macx: LIBS += -L$$OUT_PWD/../libDFTBRefDatabase/ -lDFTBRefDatabase

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libDFTBOPTInputAdaptor/release -lDFTBOPTInputAdaptor
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libDFTBOPTInputAdaptor/debug -lDFTBOPTInputAdaptor
else:unix:!macx: LIBS += -L$$OUT_PWD/../libDFTBOPTInputAdaptor/ -lDFTBOPTInputAdaptor


#external_libs
OPENBABEL_ROOT = $$PWD/../external_libs/
INCLUDEPATH+= $$OPENBABEL_ROOT$$/include/openbabel-2.0
unix: LIBS+= -L$$OPENBABEL_ROOT$$/lib -lopenbabel -linchi -lz
win32: LIBS += -L$$PWD/../external_libs/win32/bin -lopenbabel -linchi

#win32: LIBS += -L$$PWD/../external_libs/win64/bin -lopenbabel -linchi
#win32: LIBS += -L$$PWD/../external_libs/win64/lib -lxml2 -lz -liconv

#win32: LIBS += $$PWD/../external_libs/win64/lib/libmysql.a
#win32: LIBS += -L $$PWD/../external_libs/win64/lib/

INCLUDEPATH += $$PWD/../external_libs/include

#Archive library
LIBS += -L$$OUT_PWD/../external_libs/archive/lib/ -larchive -lz
unix:!macx: LIBS += -L$$OUT_PWD/../external_libs/archive/lib/ -larchive  -lz
INCLUDEPATH += $$PWD/../external_libs/archive/Archive
DEPENDPATH += $$PWD/../external_libs/archive/Archive
INCLUDEPATH += $$PWD/../external_libs/archive/include
DEPENDPATH += $$PWD/../external_libs/archive/include
unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../external_libs/archive/lib/libarchive.a

unix: LIBS += $$PWD/../external_libs/lib/libVariant.a -lxml2 $$PWD/../external_libs/lib/libyaml.a 
win32: LIBS += $$PWD/../external_libs/win32/lib/libVariant.a $$PWD/../external_libs/win32/lib/libyaml.a -lregex


win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libDFTBTestSuiteCommon/release/ -lDFTBTestSuiteCommon
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libDFTBTestSuiteCommon/debug/ -lDFTBTestSuiteCommon
else:unix: LIBS += -L$$OUT_PWD/../libDFTBTestSuiteCommon/ -lDFTBTestSuiteCommon

INCLUDEPATH += $$PWD/../libDFTBTestSuiteCommon
DEPENDPATH += $$PWD/../libDFTBTestSuiteCommon

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuiteCommon/release/libDFTBTestSuiteCommon.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuiteCommon/debug/libDFTBTestSuiteCommon.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuiteCommon/release/DFTBTestSuiteCommon.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuiteCommon/debug/DFTBTestSuiteCommon.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuiteCommon/libDFTBTestSuiteCommon.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libDFTBPlusAdaptor/release/ -lDFTBPlusAdaptor
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libDFTBPlusAdaptor/debug/ -lDFTBPlusAdaptor
else:unix: LIBS += -L$$OUT_PWD/../libDFTBPlusAdaptor/ -lDFTBPlusAdaptor

INCLUDEPATH += $$PWD/../libDFTBPlusAdaptor
DEPENDPATH += $$PWD/../libDFTBPlusAdaptor

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBPlusAdaptor/release/libDFTBPlusAdaptor.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBPlusAdaptor/debug/libDFTBPlusAdaptor.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBPlusAdaptor/release/DFTBPlusAdaptor.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBPlusAdaptor/debug/DFTBPlusAdaptor.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libDFTBPlusAdaptor/libDFTBPlusAdaptor.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libadpt_common/release/ -ladpt_common
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libadpt_common/debug/ -ladpt_common
else:unix: LIBS += -L$$OUT_PWD/../libadpt_common/ -ladpt_common

INCLUDEPATH += $$PWD/../libadpt_common
DEPENDPATH += $$PWD/../libadpt_common

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/release/libadpt_common.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/debug/libadpt_common.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/release/adpt_common.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/debug/adpt_common.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/libadpt_common.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libSKBuilderCommon/release/ -lSKBuilderCommon
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libSKBuilderCommon/debug/ -lSKBuilderCommon
else:unix: LIBS += -L$$OUT_PWD/../libSKBuilderCommon/ -lSKBuilderCommon

INCLUDEPATH += $$PWD/../libSKBuilderCommon
DEPENDPATH += $$PWD/../libSKBuilderCommon

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libSKBuilderCommon/release/libSKBuilderCommon.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libSKBuilderCommon/debug/libSKBuilderCommon.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libSKBuilderCommon/release/SKBuilderCommon.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libSKBuilderCommon/debug/SKBuilderCommon.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libSKBuilderCommon/libSKBuilderCommon.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libErepfitCommon/release/ -lErepfitCommon
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libErepfitCommon/debug/ -lErepfitCommon
else:unix: LIBS += -L$$OUT_PWD/../libErepfitCommon/ -lErepfitCommon

INCLUDEPATH += $$PWD/../libErepfitCommon
DEPENDPATH += $$PWD/../libErepfitCommon

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libErepfitCommon/release/libErepfitCommon.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libErepfitCommon/debug/libErepfitCommon.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libErepfitCommon/release/ErepfitCommon.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libErepfitCommon/debug/ErepfitCommon.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libErepfitCommon/libErepfitCommon.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libSKOptimizerCommon/release/ -lSKOptimizerCommon
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libSKOptimizerCommon/debug/ -lSKOptimizerCommon
else:unix: LIBS += -L$$OUT_PWD/../libSKOptimizerCommon/ -lSKOptimizerCommon

INCLUDEPATH += $$PWD/../libSKOptimizerCommon
DEPENDPATH += $$PWD/../libSKOptimizerCommon

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libSKOptimizerCommon/release/libSKOptimizerCommon.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libSKOptimizerCommon/debug/libSKOptimizerCommon.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libSKOptimizerCommon/release/SKOptimizerCommon.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libSKOptimizerCommon/debug/SKOptimizerCommon.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libSKOptimizerCommon/libSKOptimizerCommon.a
