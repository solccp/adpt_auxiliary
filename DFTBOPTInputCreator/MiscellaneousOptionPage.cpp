#include "MiscellaneousOptionPage.h"

#include <QtWidgets>

MiscellaneousOptionPage::MiscellaneousOptionPage(QWidget *parent) :
    QWizardPage(parent)
{
    setTitle(tr("Miscellaneous Options"));

    QVBoxLayout *layout = new QVBoxLayout;

    setLayout(layout);


    {
        auto gbox = new QGroupBox(this);

        layout->addWidget(gbox);

        gbox->setTitle("DFTB Formalism");

        m_DFTB = new QRadioButton(tr("DFTB"));
        m_DFTB->setProperty("ID", 1);

        m_SCCDFTB = new QRadioButton(tr("SCC-DFTB"));
        m_SCCDFTB->setProperty("ID", 2);

        m_DFTB3 = new QRadioButton(tr("DFTB3"));
        m_DFTB3->setProperty("ID", 3);
        auto hbox = new QHBoxLayout();

        OrbitalSolvedSCC = new QCheckBox(tr("Orbital-Resolved SCC-DFTB"));

        hbox->addSpacing(30);
        hbox->addWidget(OrbitalSolvedSCC);

        connect(m_SCCDFTB, SIGNAL(toggled(bool)), OrbitalSolvedSCC, SLOT(setEnabled(bool)));
        connect(m_SCCDFTB, SIGNAL(clicked()), this, SLOT(dftbfomulismchanged()));
        connect(m_DFTB, SIGNAL(clicked()), this, SLOT(dftbfomulismchanged()));
        connect(m_DFTB3, SIGNAL(clicked()), this, SLOT(dftbfomulismchanged()));

        connect(OrbitalSolvedSCC, SIGNAL(clicked(bool)), this, SLOT(setOrbitalSolvedSCC(bool)));

        m_SCCDFTB->setChecked(true);

        auto vbox = new QVBoxLayout();
        vbox->addWidget(m_DFTB);
        vbox->addWidget(m_SCCDFTB);
        vbox->addLayout(hbox);
        vbox->addWidget(m_DFTB3);
        gbox->setLayout(vbox);
    }


    {
        auto gbox = new QGroupBox(this);

        layout->addWidget(gbox);

        gbox->setTitle("SKBuilder Options");


        toolchain = new QComboBox();
        toolchain->setEditable(false);
        toolchain->addItems(QStringList() << "NCTU" << "BCCMS");

        auto vbox = new QVBoxLayout();
        auto hbox = new QHBoxLayout();
        hbox->addWidget(new QLabel(tr("Toolchain:")));
        hbox->addWidget(toolchain);
        hbox->addStretch(4);
        vbox->addLayout(hbox);
        gbox->setLayout(vbox);
    }


    {
        auto gbox = new QGroupBox(this);

        layout->addWidget(gbox);

        gbox->setTitle("Erepfit Options");

        auto checkFitAllAtomEnergy = new QCheckBox(tr("Fit All Atom Energy"));


        connect(checkFitAllAtomEnergy, SIGNAL(clicked(bool)), this, SLOT(setFitAllAtomEnergy(bool)));

        auto vbox = new QVBoxLayout();
        vbox->addWidget(checkFitAllAtomEnergy);
        gbox->setLayout(vbox);
    }


    {
        auto gbox = new QGroupBox(this);

        layout->addWidget(gbox);

        gbox->setTitle("Dispersion Options");

        auto check = new QCheckBox(tr("Enable dispersion"));

        connect(check, &QCheckBox::toggled, [=](bool value){
            m_dispersion = value;
        });


        auto vbox = new QVBoxLayout();
        vbox->addWidget(check);
        gbox->setLayout(vbox);
    }




}

int MiscellaneousOptionPage::getDFTBFormulism() const
{
    return m_DFTBFormulism;
}

bool MiscellaneousOptionPage::isUseDispersion() const
{
    return m_dispersion;
}

bool MiscellaneousOptionPage::useOrbitalResolvedSCC() const
{
    return m_OrbitalSolvedSCC;
}

bool MiscellaneousOptionPage::fitAllAtomEnergy() const
{
    return m_fitAllAtomEnergy;
}

void MiscellaneousOptionPage::setOrbitalSolvedSCC(bool on)
{
    m_OrbitalSolvedSCC = on;
}

void MiscellaneousOptionPage::setFitAllAtomEnergy(bool on)
{
    m_fitAllAtomEnergy = on;
}

void MiscellaneousOptionPage::dftbfomulismchanged()
{
    QRadioButton *button = dynamic_cast<QRadioButton*>(sender());
    if (button)
    {
        int id = button->property("ID").toInt();
        m_DFTBFormulism = id;
    }

}
