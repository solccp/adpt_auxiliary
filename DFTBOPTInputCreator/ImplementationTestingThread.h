#ifndef IMPLEMENTATIONTESTINGTHREAD_H
#define IMPLEMENTATIONTESTINGTHREAD_H

#include <QObject>
#include <QProgressDialog>

class ImplementationKitDialog;
class ImplementationTestingThread : public QObject
{
    Q_OBJECT
public:
    ImplementationTestingThread(ImplementationKitDialog* dia, QObject *parent = 0);

    QString errorMsg() const;


signals:
    void verified();
    void error();
    void progress(int);
    void progressText(const QString& text);
public slots:
    void verify();
private:
    void setErrorMsg(const QString &errorMsg);
    ImplementationKitDialog *m_dia;
    QString m_errorMsg;
};

#endif // IMPLEMENTATIONTESTINGTHREAD_H
