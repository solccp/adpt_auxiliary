#include "databaseopencreatedialog.h"
#include "ui_databaseopencreatedialog.h"

#include <QFileDialog>
#include <QMessageBox>
#include <QDebug>

DataBaseOpenCreateDialog::DataBaseOpenCreateDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DataBaseOpenCreateDialog)
{
    ui->setupUi(this);
}

DataBaseOpenCreateDialog::~DataBaseOpenCreateDialog()
{
    delete ui;
}

void DataBaseOpenCreateDialog::on_butOpenDB_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open existing database"),
                                QDir::homePath(),
                                tr("DFTB DB (*.dftbdb)"));
    QFileInfo info(fileName);
    if (info.exists())
    {
        path = fileName;
        emit databaseOpened(fileName);
        accept();
    }
}
QString DataBaseOpenCreateDialog::getPath() const
{
    return path;
}


