#include "DatabaseConnectingDialog.h"

#include <QLineEdit>
#include <QComboBox>
#include <QSettings>
#include <QGridLayout>
#include <QLabel>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QDebug>
#include <QSqlError>

#include "SimpleCrypt.h"

DatabaseConnectingDialog::DatabaseConnectingDialog(QWidget *parent) :
    QDialog(parent)
{
    setupUI();
}
QString DatabaseConnectingDialog::server_addr() const
{
    return current.server_addr;
}
QString DatabaseConnectingDialog::database_name() const
{
    return current.database_name;
}
QString DatabaseConnectingDialog::account() const
{
    return current.account;
}
QString DatabaseConnectingDialog::password() const
{
    return current.password;
}
QString DatabaseConnectingDialog::profile() const
{
    return current.profile;
}
void DatabaseConnectingDialog::comboIndexChanged(int index)
{
    if (index<0)
        return;
    const Login& login = logins.at(index);
    this->m_edit_server_addr->setText(login.server_addr);
    this->m_edit_database_name->setText(login.database_name);
    this->m_edit_account->setText(login.account);
    this->m_edit_password->setText(login.password);

    set_profile(m_combo_profile->currentText());
    set_account(m_edit_account->text());
    set_database_name(m_edit_database_name->text());
    set_password(m_edit_password->text());
    set_server_addr(m_edit_server_addr->text());
}

void DatabaseConnectingDialog::butOkClicked()
{
    if (m_combo_profile->currentText().isEmpty())
    {
        QMessageBox vbox;
        vbox.setStandardButtons(QMessageBox::Ok);
        vbox.setIcon(QMessageBox::Critical);
        vbox.setText("Profile name cannot be empty");
        vbox.exec();
        return ;
    }

    bool opened = false;
    {
        QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL", "test_conn");

        set_profile(m_combo_profile->currentText());
        set_account(m_edit_account->text());
        set_database_name(m_edit_database_name->text());
        set_password(m_edit_password->text());
        set_server_addr(m_edit_server_addr->text());

        db.setDatabaseName(current.database_name);
        db.setUserName(current.account);
        db.setHostName(current.server_addr);
        db.setPassword(current.password);

        opened = db.open();

        db.close();
    }

    QSqlDatabase::removeDatabase("test_conn");
    if (!opened)
    {
        QMessageBox vbox;
        vbox.setStandardButtons(QMessageBox::Ok);
        vbox.setIcon(QMessageBox::Critical);
        vbox.setText("Database connection failed");
        vbox.exec();

        return;
    }

    saveSettings();
    accept();
}

void DatabaseConnectingDialog::butCleanHistroryClicked()
{
    m_combo_profile->clear();
    m_edit_account->clear();
    m_edit_database_name->clear();
    m_edit_password->clear();
    m_edit_server_addr->clear();

    logins.clear();
    set_profile("");
    set_account("");
    set_database_name("");
    set_password("");
    set_server_addr("");

    QSettings settings;
    settings.remove("database_server");

    m_edit_server_addr->setFocus();

}

void DatabaseConnectingDialog::set_server_addr(const QString &value)
{
    current.server_addr = value;
}

void DatabaseConnectingDialog::set_database_name(const QString &value)
{
    current.database_name = value;
}

void DatabaseConnectingDialog::set_account(const QString &value)
{
    current.account = value;
}

void DatabaseConnectingDialog::set_password(const QString &value)
{
    current.password = value;
}

void DatabaseConnectingDialog::set_profile(const QString &value)
{
    current.profile = value;
}

void DatabaseConnectingDialog::saveSettings()
{
    SimpleCrypt crypto(Q_UINT64_C(0x0c2ad6e7acb9f023));
    bool newEntry = true;

    int last_entry = -1;

    for(int i=0; i<logins.size();++i)
    {
        if (current == logins.at(i))
        {
            newEntry = false;
            break;
        }
        else if ( current.profile == logins.at(i).profile )
        {
            last_entry = i;
            newEntry = false;
            logins[i] = current;
            break;
        }

    }
    if (newEntry)
    {
        last_entry = logins.size();
        logins.append(current);
    }

    QSettings settings;
    settings.beginGroup("database_server");
    int size = logins.size();
    settings.beginWriteArray("servers", size);
    for (int i = 0; i < size; ++i)
    {
         settings.setArrayIndex(i);
         const Login& login = logins.at(i);

         settings.setValue("server_name", login.profile);
         settings.setValue("server_addr", login.server_addr);
         settings.setValue("password", crypto.encryptToString(login.password));
         settings.setValue("account", login.account);
         settings.setValue("database_name", login.database_name);
     }
    settings.endArray();
    settings.setValue("last_connected", last_entry);
    settings.endGroup();
}

void DatabaseConnectingDialog::loadSettings()
{
    SimpleCrypt crypto(Q_UINT64_C(0x0c2ad6e7acb9f023));
    QSettings settings;
    settings.beginGroup("database_server");
    int size = settings.beginReadArray("servers");
    for (int i = 0; i < size; ++i) {
         settings.setArrayIndex(i);
         Login login;
         login.profile = settings.value("server_name").toString();
         login.server_addr = settings.value("server_addr").toString();
         login.password = crypto.decryptToString(settings.value("password").toString());
         login.account = settings.value("account").toString();
         login.database_name = settings.value("database_name").toString();
         logins.append(login);
     }
    settings.endArray();

    int last_index = settings.value("last_connected").toInt();

    settings.endGroup();

    m_combo_profile->clear();
    for(int i=0; i<logins.size();++i)
    {
        const Login& login = logins.at(i);
        m_combo_profile->addItem(login.profile);
    }
    if (last_index == -1 || last_index >= logins.size())
    {
        last_index = 0;
    }
    m_combo_profile->setCurrentIndex(last_index);
}

void DatabaseConnectingDialog::setupUI()
{
    QGridLayout* grid = new QGridLayout();
    this->setLayout(grid);

    m_combo_profile = new QComboBox();
    m_combo_profile->setEditable(true);
    m_edit_server_addr = new QLineEdit;
    m_edit_database_name = new QLineEdit;
    m_edit_account = new QLineEdit;
    m_edit_password = new QLineEdit;
    m_edit_password->setEchoMode(QLineEdit::Password);

    grid->addWidget(new QLabel(tr("Profile:")),        0,0,1,1,Qt::AlignRight);
    grid->addWidget(new QLabel(tr("Server address:")), 1,0,1,1,Qt::AlignRight);
    grid->addWidget(new QLabel(tr("Databasename:")),   2,0,1,1,Qt::AlignRight);
    grid->addWidget(new QLabel(tr("Account:")),        3,0,1,1,Qt::AlignRight);
    grid->addWidget(new QLabel(tr("Password:")),       4,0,1,1,Qt::AlignRight);

    grid->addWidget(m_combo_profile,      0,1,1,3);
    grid->addWidget(m_edit_server_addr,   1,1,1,3);
    grid->addWidget(m_edit_database_name, 2,1,1,3);
    grid->addWidget(m_edit_account,       3,1,1,3);
    grid->addWidget(m_edit_password,      4,1,1,3);

    connect(m_combo_profile, SIGNAL(currentIndexChanged(int)), this, SLOT(comboIndexChanged(int)));

    butCancel = new QPushButton(tr("Cancel"));
    butOk = new QPushButton(tr("OK"));
    butCleanHistrory = new QPushButton(tr("Clean history"));
    butOk->setDefault(true);

    grid->addWidget(butCleanHistrory, 5, 0, 1, 1);
    grid->addWidget(butCancel, 5, 1, 1, 1);
    grid->addWidget(butOk, 5, 2, 1, 1);

    connect(butCancel, SIGNAL(clicked()), this, SLOT(reject()));
    connect(butCleanHistrory, SIGNAL(clicked()), this, SLOT(butCleanHistroryClicked()));
    connect(butOk, SIGNAL(clicked()), this, SLOT(butOkClicked()));

    loadSettings();
}
