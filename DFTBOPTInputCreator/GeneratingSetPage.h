#ifndef GENERATINGSETPAGE_H
#define GENERATINGSETPAGE_H

#include <QWidget>
#include <QTabWidget>
#include <QTableView>
#include <QStringList>
#include <QVBoxLayout>

#include "ErepfitAdditionalEquationWidget.h"

class DFTBRefDatabase;
class ElementFilteredTableModel;
class PropertyTableView;
class ErepfitParameter;
class ErepfitSetPage : public QWidget
{
    Q_OBJECT
public:
    enum PROPERTY_TYPE
    {
        Property_Geometry = 0,
        Property_Bandstructure = 1,
        Property_Frequency = 2,
        Property_AtomizationEnergy = 3,
        Property_ReactionEnergy = 4
    };
    ErepfitSetPage(ErepfitParameter *profile, DFTBRefDatabase *database, QWidget *parent = 0);
    virtual ~ErepfitSetPage();

    QList<QPair<QString, double> > forceEquations() const;
    QList<QPair<QString, double> > energyEquations() const;
    QList<QPair<QString, double> > reactionEnergyEquations() const;
    QList<AdditionalEquationSelection> additionalEquations() const;
    void setSelectedElements(const QStringList& elements);
    QStringList selectedElements() const;

    void setSelectedEnergyEquations(const QStringList& uuids);
    void setSelectedForceEquations(const QStringList& uuids);
    void setSelectedReactionEnergyEquations(const QStringList& uuids);

signals:
    void selectionChanged(const QString& propertyName, bool selected, const QString& uuid, double weight);
public slots:
    void reload();
    void postload();
    void updateSelection(int property_type, bool selected, const QString& uuid, double weight);
    void updateSelection(const QList<PropertySelection *> &properties);
    void clear();
private slots:
    void propertySelectionChanged(bool selected, const QString &uuid, double weight);
//    void showGeometryDetail(const QString& uuid);
//    void showBandstructureDetail(const QString& uuid);
//    void showFrequencyDetail(const QString& uuid);
//    void showAtomizationEnergyDetail(const QString& uuid);
//    void showReactionEnergyDetail(const QString& uuid);
private:
    DFTBRefDatabase *m_database = nullptr;
    QTabWidget *m_tabWidget = nullptr;
    PropertyTableView *m_tableForce = nullptr;
    PropertyTableView *m_tableEnergy = nullptr;
    PropertyTableView *m_tableReactionEnergy = nullptr;


    ErepfitParameter* m_profile;

    QStringList m_elements;
    ErepfitAdditionalEquationWidget* m_tabAdditionalEquations = nullptr;

    QVBoxLayout *layout = nullptr;
public:
    bool validatePage();
};

#endif // GENERATINGSETPAGE_H
