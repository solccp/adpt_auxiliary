#ifndef REPULSIVEPOTENTIALDEFINITIONWIDGET_H
#define REPULSIVEPOTENTIALDEFINITIONWIDGET_H

#include <QWidget>
#include <QMap>
class RepulsivePotentialDefinition
{
public:
    bool optimize = true;
    double start;
    double cutoff;
    int numKnots = 2;
    QString externalFile;
};

class RepulsivePotentialDefinitionWidget : public QWidget
{
    Q_OBJECT
public:
    explicit RepulsivePotentialDefinitionWidget(QWidget *parent = 0);
    void setElements(const QStringList& elements);
signals:

public slots:
private:
    QStringList m_elements;
    QList<RepulsivePotentialDefinition*> m_potentialPointers;
    QMap<QPair<QString, QString>, RepulsivePotentialDefinition*> m_potentials;

};

#endif // REPULSIVEPOTENTIALDEFINITIONWIDGET_H
