#include "PropertyTableView.h"
#include "ElementFilteredTableModel.h"

#include <QTableView>
#include <QHeaderView>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QComboBox>
#include <QLineEdit>
#include <QUuid>
#include <QMenu>

#include "doublenumbercelldelegate.h"

#include <QDebug>

PropertyTableView::PropertyTableView(DFTBRefDatabase *database, QWidget *parent) :
    QWidget(parent), m_database(database)
{
    QVBoxLayout *vbox = new QVBoxLayout();
    vbox->setMargin(2);

    QHBoxLayout *hbox = new QHBoxLayout;
    hbox->setMargin(2);
    hbox->addWidget(new QLabel(tr("Search:")));

    m_filterType = new QComboBox();
    m_filterType->setEditable(false);

    m_filterText = new QLineEdit();

    connect(m_filterText, SIGNAL(textChanged(QString)), this, SLOT(filterTextChanged(QString)));
    connect(m_filterType, SIGNAL(currentIndexChanged(int)), this, SLOT(filterTypeChanged(int)));

    hbox->addWidget(m_filterType);
    hbox->addWidget(m_filterText);

    m_model = new ElementFilteredTableModel(m_database, this);
    connect(m_model, SIGNAL(selectionChanged(bool,QString,double)), this, SLOT(modelSelectionChanged(bool,QString,double)));


    m_view = new QTableView();
    m_view->setSelectionBehavior(QAbstractItemView::SelectRows);
    connect(m_view, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(tableViewItemDoubleClicked(QModelIndex)));

    vbox->addLayout(hbox);
    vbox->addWidget(m_view);

    this->setLayout(vbox);

    m_view->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(m_view, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(customContextMenuRequested(QPoint)));

}

PropertyTableView::~PropertyTableView()
{

}

const ElementFilteredTableModel *PropertyTableView::model() const
{
    return m_model;
}

void PropertyTableView::setItemDelegateForColumn(int column, QAbstractItemDelegate *delegate)
{
    m_view->setItemDelegateForColumn(column, delegate);
}

void PropertyTableView::reload()
{
    m_model->select();
}

void PropertyTableView::postload()
{
    m_view->setModel(m_model);
    m_filterType->clear();
    m_view->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

    if (m_model->checkableColumnIndex() > -1)
    {
        m_view->horizontalHeader()->setSectionResizeMode(m_model->checkableColumnIndex(), QHeaderView::Fixed);
        m_view->setColumnWidth(m_model->checkableColumnIndex(),30);
    }

    m_view->setItemDelegateForColumn(m_model->weightColumnIndex(), new DoubleNumberCellDelegate());

    m_view->setSortingEnabled(true);
    bool sortTabSet = false;

    QSetIterator<int> it(m_hiddenColumns);
    while(it.hasNext())
    {
        m_view->hideColumn(it.next());
    }

    m_filterText->clear();

    for(int i=0; i<m_view->horizontalHeader()->count(); ++i)
    {
        if (!m_view->horizontalHeader()->isSectionHidden(i) && i != m_model->checkableColumnIndex()
                && i != m_model->weightColumnIndex())
        {
            if (!sortTabSet)
            {
                m_view->horizontalHeader()->setSortIndicator(i, Qt::AscendingOrder);
                sortTabSet = true;
            }

            if (i > m_model->weightColumnIndex())
            {
                m_filterType->addItem(m_view->model()->headerData(i, Qt::Horizontal, Qt::DisplayRole).toString(), i-1);
            }
            else
            {
                m_filterType->addItem(m_view->model()->headerData(i, Qt::Horizontal, Qt::DisplayRole).toString(), i);
            }
        }
    }
}

void PropertyTableView::hideColumn(int column)
{
    m_hiddenColumns.insert(column);

}

void PropertyTableView::unHideColumn(int column)
{
    if(column > -1)
    {
        m_hiddenColumns.remove(column);
    }
    else
    {
        m_hiddenColumns.clear();
    }
}

void PropertyTableView::setTableName(const QString &name)
{
    m_model->setTableName(name);
}

void PropertyTableView::setElements(const QStringList &elements)
{
    m_model->setSelectedElements(elements);
}

void PropertyTableView::setCheckableColumnIndex(int column)
{
    m_model->setCheckableColumnIndex(column);
}

void PropertyTableView::checkItem(const QString &uuid)
{
    m_model->checkItem(uuid);
}

void PropertyTableView::updateItem(bool selected, const QString &uuid, double weight)
{
    m_model->updateItem(selected, uuid, weight);
}

void PropertyTableView::filterTextChanged(const QString &text)
{
    m_model->setFilterText(text);
    m_view->updateGeometry();
}

void PropertyTableView::filterTypeChanged(int column)
{
    int filterType = m_filterType->itemData(column, Qt::UserRole).toInt();
    m_model->setFilterColumnIndex(filterType);
    m_filterText->clear();
}

void PropertyTableView::tableViewItemDoubleClicked(const QModelIndex &index)
{
    if (index.column() == m_model->checkableColumnIndex()+1)
    {
        m_view->setCurrentIndex(index);
        m_view->edit(index);
        return;
    }
    QString str = m_model->getItemUUID(index);
    QUuid uuid(str);
    if (uuid.toString() != QUuid().toString())
    {
        emit itemDblClicked(uuid.toString());
    }
}

void PropertyTableView::customContextMenuRequested(const QPoint &pos)
{
    Q_UNUSED(pos);
    QMenu *menu = new QMenu();
    menu->addAction("Check selected", this, SLOT(selectItems()));
    menu->addSeparator();
    menu->addAction("Check all", this, SLOT(selectAllItems()));
    menu->addAction("Check none", this, SLOT(selectNoItems()));
    menu->exec(QCursor::pos());

    delete menu;
}

void PropertyTableView::selectAllItems()
{
    int index = m_model->checkableColumnIndex();
    int rowCount = m_model->rowCount(QModelIndex());
    for (int i=0; i<rowCount ;++i)
    {
        m_model->setData(m_model->index(i,index), Qt::Checked, Qt::CheckStateRole);
    }
}

void PropertyTableView::selectItems()
{
    QModelIndexList list = m_view->selectionModel()->selectedRows();
    int index = m_model->checkableColumnIndex();
    for(int i=0; i<list.size(); ++i)
    {
        int row = list[i].row(); //m_model->sourceIndex(list[i].row());
        auto checked = m_model->data(m_model->index(row, index), Qt::CheckStateRole).toInt();

        m_model->setData(m_model->index(row,index), Qt::Checked-checked, Qt::CheckStateRole );
    }
}

void PropertyTableView::selectNoItems()
{
    for (int i=0; i<m_model->rowCount(QModelIndex());++i)
    {
        m_model->setData(m_model->index(i,m_model->checkableColumnIndex()), Qt::Unchecked, Qt::CheckStateRole);
    }
}

void PropertyTableView::modelSelectionChanged(bool selected, const QString &uuid, double weight)
{
    emit itemSelectionChanged(selected, uuid, weight);
}
