#ifndef FETCHINGTHREAD_H
#define FETCHINGTHREAD_H

#include <QThread>

class InputCreatorWizard;
class FetchingThread : public QThread
{
    Q_OBJECT
public:
    explicit FetchingThread(InputCreatorWizard* wizard, QObject* parent = 0);
    void setPageId(int id);
    QString message() const;
    void setMessage(const QString &message);

private:
    InputCreatorWizard* m_wizard;
    int m_id;
    QString m_message;

    // QThread interface
protected:
    void run();
};

#endif // FETCHINGTHREAD_H
