#include "EvaluatedResultWindow.h"

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QtWidgets>
#include <QSizePolicy>

#include "DFTBTestSuite/ReferenceDataType/ReferenceData.h"
#include "DFTBTestSuite/EvaluatedData.h"
#include "DFTBTestSuite/statisticanalyser.h"
#include "DFTBTestSuite/util.h"

EvaluatedResultWindow::EvaluatedResultWindow(QWidget *parent) :
    QWidget(parent)
{
    m_layout = new QVBoxLayout;
    this->setLayout(m_layout);  

    m_layout->addStretch(5);
}

void EvaluatedResultWindow::removePushed()
{
    QPushButton* but = qobject_cast<QPushButton*>(sender());
    if (but)
    {
        QString setname = but->property("setname").toString();
        if (m_items.contains(setname))
        {
            delete m_items[setname];
            m_items.remove(setname);
        }
    }
}

void EvaluatedResultWindow::showDetails()
{
    QPushButton* but = qobject_cast<QPushButton*>(sender());
    if (but)
    {
        QString setname = but->property("setname").toString();
        if (m_items.contains(setname))
        {
            int id = QFontDatabase::addApplicationFont(":/UbuntuMono.ttf");
            QStringList families = QFontDatabase::applicationFontFamilies(id);
            QFont font = QFont(families.first());
            font.setStyleHint (QFont::TypeWriter);
            QPlainTextEdit* tb = new QPlainTextEdit;
            tb->setReadOnly(true);
            tb->setContextMenuPolicy(Qt::DefaultContextMenu);
            tb->setFont(font);
            tb->setPlainText(m_items[setname]->property("detail").toString());
            tb->resize(800,600);
            tb->show();
        }
    }

}

void EvaluatedResultWindow::showBSGraph()
{
    QPushButton* but = qobject_cast<QPushButton*>(sender());
    if (but)
    {
        QString setname = but->property("setname").toString();
        QString ID = but->property("ID").toString();
        auto pair = qMakePair(setname, ID);
        if (m_bsGraphs.contains(pair))
        {
//            QwtPlot* plot = m_bsGraphs[pair];
//            plot->resize(600,400);
//            plot->show();
        }
    }
}


void EvaluatedResultWindow::addResult(const QString& setname,
                                      const QString& fulldetail,
                                      const DFTBTestSuite::ReferenceData *ref_data,
                                      const DFTBTestSuite::EvaluatedData *calc_data,
                                      const DFTBTestSuite::Statistics *statistics)
{

    if (m_items.contains(setname))
    {
        return ;
    }


    QFrame *widget = new QFrame;


    widget->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
    widget->setProperty("detail", fulldetail);

    QHBoxLayout *layout = new QHBoxLayout;
    QVBoxLayout *vbox = new QVBoxLayout;

    QPushButton* butDetail = new QPushButton("Details");
    butDetail->setProperty("setname",setname);
    connect(butDetail, SIGNAL(clicked()), this, SLOT(showDetails()));
    vbox->addWidget(new QLabel(setname),0,Qt::AlignHCenter );
    vbox->addWidget(butDetail);
    layout->addLayout(vbox);

    QVBoxLayout *types_layout = new QVBoxLayout;
    QMap<QString, QHBoxLayout*> map_type_layout;
    QList<QString> types = statistics->getTypes();

    for(int i=0; i<types.size();++i)
    {
        QString type = types[i];
        QString unit = statistics->getUnit(type);

        if (type.startsWith("Bond") || type.startsWith("Torsion"))
        {
            type = "Geometry";
            unit = "Ang., Deg., Deg.";
        }
        QHBoxLayout *type_layout = new QHBoxLayout;

        if (type.startsWith("Frequenc"))
        {
            unit = "wavenumbers(cm^-1)";
        }
        if (!map_type_layout.contains(type))
        {

            QLabel *label = new QLabel(type + "\n" + "(" + unit + ")");
            label->setMinimumWidth(150);
            label->setMaximumWidth(150);
            label->setAlignment(Qt::AlignCenter);
            type_layout->addWidget(label,0, Qt::AlignLeft);
            map_type_layout[type] = type_layout;
            types_layout->addLayout(type_layout);
        }
    }

    layout->addLayout(types_layout);
    widget->setLayout(layout);
    widget->setFrameStyle(QFrame::Box);
    widget->setFrameShadow(QFrame::Plain);
    m_layout->insertWidget(0,widget);
    m_items[setname] = widget;


    if (map_type_layout.contains("Geometry"))
    {
        QHBoxLayout *type_layout = map_type_layout["Geometry"];
        QVBoxLayout *rows_layout = new QVBoxLayout();
        rows_layout->setSpacing(0);
        type_layout->addLayout(rows_layout);

        QList<QString> moles = statistics->getError_UUIDs("BondLength");

        int width = 60;

        QHBoxLayout *header = new QHBoxLayout;
        header->addStretch(5);
        QLabel *label = new QLabel(tr("M.A.D."));
        label->setAlignment(Qt::AlignCenter);
        label->setMinimumWidth(width*3);
        label->setMaximumWidth(width*3);
        header->addWidget(label,0,Qt::AlignRight);
        QFrame *frame = new QFrame();
        frame->setFrameStyle(QFrame::VLine | QFrame::Plain);
        header->addWidget(frame);
        label = new QLabel(tr("Max. D."));
        label->setAlignment(Qt::AlignCenter);
        label->setMinimumWidth(width*3+15);
        label->setMaximumWidth(width*3+15);
        header->addWidget(label,0,Qt::AlignRight);
        rows_layout->addLayout(header);

        header = new QHBoxLayout;
        header->addStretch(5);
        label = new QLabel(tr("Length"));
        label->setAlignment(Qt::AlignRight);
        label->setMinimumWidth(width);
        label->setMaximumWidth(width);
        header->addWidget(label,0,Qt::AlignRight);
        label = new QLabel(tr("Angle"));
        label->setAlignment(Qt::AlignRight);
        label->setMinimumWidth(width);
        label->setMaximumWidth(width);
        header->addWidget(label,0,Qt::AlignRight);
        label = new QLabel(tr("Torsion"));
        label->setAlignment(Qt::AlignRight);
        label->setMinimumWidth(width+15);
        label->setMaximumWidth(width+15);
        header->addWidget(label,0,Qt::AlignRight);
        frame = new QFrame();
        frame->setFrameStyle(QFrame::VLine | QFrame::Plain);
        header->addWidget(frame);
        label = new QLabel(tr("Length"));
        label->setAlignment(Qt::AlignRight);
        label->setMinimumWidth(width);
        label->setMaximumWidth(width);
        header->addWidget(label,0,Qt::AlignRight);
        label = new QLabel(tr("Angle"));
        label->setAlignment(Qt::AlignRight);
        label->setMinimumWidth(width);
        label->setMaximumWidth(width);
        header->addWidget(label,0,Qt::AlignRight);
        label = new QLabel(tr("Torsion"));
        label->setAlignment(Qt::AlignRight);
        label->setMinimumWidth(width+15);
        label->setMaximumWidth(width+15);
        header->addWidget(label,0,Qt::AlignRight);
        rows_layout->addLayout(header);
        frame = new QFrame();
        frame->setFrameStyle(QFrame::HLine | QFrame::Plain);
        rows_layout->addWidget(frame);

        {
            QHBoxLayout *box = new QHBoxLayout;
            QLabel* lab_total = new QLabel("Total");

            box->addWidget(lab_total, Qt::AlignLeft);

            QHBoxLayout *box2 = new QHBoxLayout;
            try
            {
                double mue = statistics->getError_MUE("BondLength");
                QString value;
                value.sprintf("%+8.3lf", mue);
                QLabel *l_label = new QLabel(value);
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box2->addWidget(l_label,0,Qt::AlignRight);
            }
            catch(DFTBTestSuite::Statistics::WrongArgumentException& e)
            {
                QLabel *l_label = new QLabel(tr("N/A"));
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box2->addWidget(l_label,0,Qt::AlignRight);
            }

            try
            {
                double mue = statistics->getError_MUE("BondAngle");
                QString value;
                value.sprintf("%+8.3lf", mue);
                QLabel *l_label = new QLabel(value);
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box2->addWidget(l_label,0,Qt::AlignRight);
            }
            catch(DFTBTestSuite::Statistics::WrongArgumentException& e)
            {
                QLabel *l_label = new QLabel(tr("N/A"));
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box2->addWidget(l_label,0,Qt::AlignRight);
            }

            try
            {
                double mue = statistics->getError_MUE("TorsionAngle");
                QString value;
                value.sprintf("%+8.3lf", mue);
                QLabel *l_label = new QLabel(value);
                l_label->setMinimumWidth(width+15);
                l_label->setMaximumWidth(width+15);
                l_label->setAlignment(Qt::AlignRight);
                box2->addWidget(l_label,0,Qt::AlignRight);
            }
            catch(DFTBTestSuite::Statistics::WrongArgumentException& e)
            {
                QLabel *l_label = new QLabel(tr("N/A"));
                l_label->setMinimumWidth(width+15);
                l_label->setMaximumWidth(width+15);
                l_label->setAlignment(Qt::AlignRight);
                box2->addWidget(l_label,0,Qt::AlignRight);
            }
            box->addLayout(box2);
            QFrame *frame = new QFrame();
            frame->setFrameStyle(QFrame::VLine | QFrame::Plain);
            box->addWidget(frame);

            box2 = new QHBoxLayout;
            try
            {
                QPair<double, double> mue = statistics->getError_MAX("BondLength");
                QString value;
                value.sprintf("%+8.3lf", mue.first);
                QLabel *l_label = new QLabel(value);
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box2->addWidget(l_label,0,Qt::AlignRight);
            }
            catch(DFTBTestSuite::Statistics::WrongArgumentException& e)
            {
                QLabel *l_label = new QLabel(tr("N/A"));
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box2->addWidget(l_label,0,Qt::AlignRight);
            }

            try
            {
                QPair<double, double> mue = statistics->getError_MAX("BondAngle");
                QString value;
                value.sprintf("%+8.3lf", mue.first);
                QLabel *l_label = new QLabel(value);
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box2->addWidget(l_label,0,Qt::AlignRight);
            }
            catch(DFTBTestSuite::Statistics::WrongArgumentException& e)
            {
                QLabel *l_label = new QLabel(tr("N/A"));
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box2->addWidget(l_label,0,Qt::AlignRight);
            }

            try
            {
                QPair<double, double> mue = statistics->getError_MAX("TorsionAngle");
                QString value;
                value.sprintf("%+8.3lf", mue.first);
                QLabel *l_label = new QLabel(value);
                l_label->setMinimumWidth(width+15);
                l_label->setMaximumWidth(width+15);
                l_label->setAlignment(Qt::AlignRight);
                box2->addWidget(l_label,0,Qt::AlignRight);
            }
            catch(DFTBTestSuite::Statistics::WrongArgumentException& e)
            {
                QLabel *l_label = new QLabel(tr("N/A"));
                l_label->setMinimumWidth(width+15);
                l_label->setMaximumWidth(width+15);
                l_label->setAlignment(Qt::AlignRight);
                box2->addWidget(l_label,0,Qt::AlignRight);
            }
            box->addLayout(box2);
            rows_layout->addLayout(box);
        }

        for(int i=0; i<moles.size();++i)
        {
            QHBoxLayout *box = new QHBoxLayout;
            box->addWidget(new QLabel(statistics->getError_Name(moles[i])), Qt::AlignLeft);

            QHBoxLayout *box2 = new QHBoxLayout;
            try
            {
                double mue = statistics->getError_MUE("BondLength", moles[i]);
                QString value;
                value.sprintf("%+8.3lf", mue);
                QLabel *l_label = new QLabel(value);
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box2->addWidget(l_label,0,Qt::AlignRight);
            }
            catch(DFTBTestSuite::Statistics::WrongArgumentException& e)
            {
                QLabel *l_label = new QLabel(tr("N/A"));
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box2->addWidget(l_label,0,Qt::AlignRight);
            }

            try
            {
                double mue = statistics->getError_MUE("BondAngle", moles[i]);
                QString value;
                value.sprintf("%+8.3lf", mue);
                QLabel *l_label = new QLabel(value);
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box2->addWidget(l_label,0,Qt::AlignRight);
            }
            catch(DFTBTestSuite::Statistics::WrongArgumentException& e)
            {
                QLabel *l_label = new QLabel(tr("N/A"));
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box2->addWidget(l_label,0,Qt::AlignRight);
            }

            try
            {
                double mue = statistics->getError_MUE("TorsionAngle", moles[i]);
                QString value;
                value.sprintf("%+8.3lf", mue);
                QLabel *l_label = new QLabel(value);
                l_label->setMinimumWidth(width+15);
                l_label->setMaximumWidth(width+15);
                l_label->setAlignment(Qt::AlignRight);
                box2->addWidget(l_label,0,Qt::AlignRight);
            }
            catch(DFTBTestSuite::Statistics::WrongArgumentException& e)
            {
                QLabel *l_label = new QLabel(tr("N/A"));
                l_label->setMinimumWidth(width+15);
                l_label->setMaximumWidth(width+15);
                l_label->setAlignment(Qt::AlignRight);
                box2->addWidget(l_label,0,Qt::AlignRight);
            }
            box->addLayout(box2);
            QFrame *frame = new QFrame();
            frame->setFrameStyle(QFrame::VLine | QFrame::Plain);
            box->addWidget(frame);

            box2 = new QHBoxLayout;
            try
            {
                double mue = statistics->getError_MAX("BondLength", moles[i]);
                QString value;
                value.sprintf("%+8.3lf", mue);
                QLabel *l_label = new QLabel(value);
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box2->addWidget(l_label,0,Qt::AlignRight);
            }
            catch(DFTBTestSuite::Statistics::WrongArgumentException& e)
            {
                QLabel *l_label = new QLabel(tr("N/A"));
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box2->addWidget(l_label,0,Qt::AlignRight);
            }

            try
            {
                double mue = statistics->getError_MAX("BondAngle", moles[i]);
                QString value;
                value.sprintf("%+8.3lf", mue);
                QLabel *l_label = new QLabel(value);
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box2->addWidget(l_label,0,Qt::AlignRight);
            }
            catch(DFTBTestSuite::Statistics::WrongArgumentException& e)
            {
                QLabel *l_label = new QLabel(tr("N/A"));
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box2->addWidget(l_label,0,Qt::AlignRight);
            }

            try
            {
                double mue = statistics->getError_MAX("TorsionAngle", moles[i]);
                QString value;
                value.sprintf("%+8.3lf", mue);
                QLabel *l_label = new QLabel(value);
                l_label->setMinimumWidth(width+15);
                l_label->setMaximumWidth(width+15);
                l_label->setAlignment(Qt::AlignRight);
                box2->addWidget(l_label,0,Qt::AlignRight);
            }
            catch(DFTBTestSuite::Statistics::WrongArgumentException& e)
            {
                QLabel *l_label = new QLabel(tr("N/A"));
                l_label->setMinimumWidth(width+15);
                l_label->setMaximumWidth(width+15);
                l_label->setAlignment(Qt::AlignRight);
                box2->addWidget(l_label,0,Qt::AlignRight);
            }
            box->addLayout(box2);
            rows_layout->addLayout(box);
        }
    }

    if (map_type_layout.contains("Frequencies"))
    {
        QHBoxLayout *type_layout = map_type_layout["Frequencies"];
        QVBoxLayout *rows_layout = new QVBoxLayout();
        rows_layout->setSpacing(0);
        type_layout->addLayout(rows_layout);

        QList<QString> moles = statistics->getError_UUIDs("Frequencies");

        int width = 75;
        QHBoxLayout *header = new QHBoxLayout;
        header->addStretch(5);
        QLabel *label = new QLabel(tr("M.A.D."));
        label->setAlignment(Qt::AlignRight);
        label->setMinimumWidth(width);
        label->setMaximumWidth(width);
        header->addWidget(label,0,Qt::AlignRight);
        label = new QLabel(tr("Max. D."));
        label->setAlignment(Qt::AlignRight);
        label->setMinimumWidth(width);
        label->setMaximumWidth(width);
        header->addWidget(label,0,Qt::AlignRight);
        rows_layout->addLayout(header);

        {
            QHBoxLayout *box = new QHBoxLayout;
            box->addWidget(new QLabel("Total"), Qt::AlignLeft);

            try
            {
                double mue = statistics->getError_MUE("Frequencies");
                QString value;
                value.sprintf("%+10.3lf", mue);
                QLabel *l_label = new QLabel(value);
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box->addWidget(l_label,0,Qt::AlignRight);
            }
            catch(DFTBTestSuite::Statistics::WrongArgumentException& e)
            {
                QLabel *l_label = new QLabel(tr("N/A"));
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box->addWidget(l_label,0,Qt::AlignRight);
            }

            try
            {
                QPair<double, double> mue = statistics->getError_MAX("Frequencies");
                QString value;
                value.sprintf("%+10.3lf", mue.first);
                QLabel *l_label = new QLabel(value);
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box->addWidget(l_label,0,Qt::AlignRight);
            }
            catch(DFTBTestSuite::Statistics::WrongArgumentException& e)
            {
                QLabel *l_label = new QLabel(tr("N/A"));
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box->addWidget(l_label,0,Qt::AlignRight);
            }

            rows_layout->addLayout(box);
        }


        for(int i=0; i<moles.size();++i)
        {
            QHBoxLayout *box = new QHBoxLayout;
            box->addWidget(new QLabel(statistics->getError_Name(moles[i])), Qt::AlignLeft);

            try
            {
                double mue = statistics->getError_MUE("Frequencies", moles[i]);
                QString value;
                value.sprintf("%+10.3lf", mue);
                QLabel *l_label = new QLabel(value);
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box->addWidget(l_label,0,Qt::AlignRight);
            }
            catch(DFTBTestSuite::Statistics::WrongArgumentException& e)
            {
                QLabel *l_label = new QLabel(tr("N/A"));
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box->addWidget(l_label,0,Qt::AlignRight);
            }

            try
            {
                double mue = statistics->getError_MAX("Frequencies", moles[i]);
                QString value;
                value.sprintf("%+10.3lf", mue);
                QLabel *l_label = new QLabel(value);
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box->addWidget(l_label,0,Qt::AlignRight);
            }
            catch(DFTBTestSuite::Statistics::WrongArgumentException& e)
            {
                QLabel *l_label = new QLabel(tr("N/A"));
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box->addWidget(l_label,0,Qt::AlignRight);
            }

            rows_layout->addLayout(box);
        }
    }

    if (map_type_layout.contains("AtomizationEnergy"))
    {
        QHBoxLayout *type_layout = map_type_layout["AtomizationEnergy"];
        QVBoxLayout *rows_layout = new QVBoxLayout();
        type_layout->addLayout(rows_layout);
        QList<QString> moles = statistics->getError_UUIDs("AtomizationEnergy");

        int width = 55;
        QHBoxLayout *header = new QHBoxLayout;
        header->addStretch(5);
        QLabel *label = new QLabel(tr("M.A.D."));
        label->setAlignment(Qt::AlignRight);
        label->setMinimumWidth(width);
        label->setMaximumWidth(width);
        header->addWidget(label,0,Qt::AlignRight);
        label = new QLabel(tr("Max. D."));
        label->setAlignment(Qt::AlignRight);
        label->setMinimumWidth(width);
        label->setMaximumWidth(width);
        header->addWidget(label,0,Qt::AlignRight);
        rows_layout->addLayout(header);

        {
            QHBoxLayout *box = new QHBoxLayout;
            box->addWidget(new QLabel("Total"), Qt::AlignLeft);

            try
            {
                double mue = statistics->getError_MUE("AtomizationEnergy");
                QString value;
                value.sprintf("%+8.3lf", mue);
                QLabel *l_label = new QLabel(value);
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box->addWidget(l_label,0,Qt::AlignRight);
            }
            catch(DFTBTestSuite::Statistics::WrongArgumentException& e)
            {
                QLabel *l_label = new QLabel(tr("N/A"));
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box->addWidget(l_label,0,Qt::AlignRight);
            }

            try
            {
                QPair<double, double> mue = statistics->getError_MAX("AtomizationEnergy");
                QString value;
                value.sprintf("%+8.3lf", mue.first);
                QLabel *l_label = new QLabel(value);
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box->addWidget(l_label,0,Qt::AlignRight);
            }
            catch(DFTBTestSuite::Statistics::WrongArgumentException& e)
            {
                QLabel *l_label = new QLabel(tr("N/A"));
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box->addWidget(l_label,0,Qt::AlignRight);
            }

            rows_layout->addLayout(box);
        }


        for(int i=0; i<moles.size();++i)
        {
            QHBoxLayout *box = new QHBoxLayout;
            box->addWidget(new QLabel(statistics->getError_Name(moles[i])), Qt::AlignLeft);

            try
            {
                double mue = statistics->getError_MUE("AtomizationEnergy", moles[i]);
                QString value;
                value.sprintf("%+8.3lf", mue);
                QLabel *l_label = new QLabel(value);
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box->addWidget(l_label,0,Qt::AlignRight);
            }
            catch(DFTBTestSuite::Statistics::WrongArgumentException& e)
            {
                QLabel *l_label = new QLabel(tr("N/A"));
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box->addWidget(l_label,0,Qt::AlignRight);
            }

            try
            {
                double mue = statistics->getError_MAX("AtomizationEnergy", moles[i]);
                QString value;
                value.sprintf("%+8.3lf", mue);
                QLabel *l_label = new QLabel(value);
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box->addWidget(l_label,0,Qt::AlignRight);
            }
            catch(DFTBTestSuite::Statistics::WrongArgumentException& e)
            {
                QLabel *l_label = new QLabel(tr("N/A"));
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box->addWidget(l_label,0,Qt::AlignRight);
            }

            rows_layout->addLayout(box);
        }
    }

    if (map_type_layout.contains("BandStructure"))
    {
        QHBoxLayout *type_layout = map_type_layout["BandStructure"];
        QVBoxLayout *rows_layout = new QVBoxLayout();
//        QScrollArea *scrollArea = new QScrollArea;
//        scrollArea->setWidgetResizable(true);
//        QWidget* ala = new QWidget;
//        ala->setLayout(rows_layout);
//        scrollArea->setWidget(ala);
//        type_layout->addWidget(scrollArea);

        type_layout->addLayout(rows_layout);

        QList<QString> moles = statistics->getError_UUIDs("BandStructure");

        int width = 55;
        QHBoxLayout *header = new QHBoxLayout;
        header->addStretch(5);
        QLabel *label = new QLabel(tr("M.A.D."));
        label->setAlignment(Qt::AlignRight);
        label->setMinimumWidth(width);
        label->setMaximumWidth(width);
        header->addWidget(label,0,Qt::AlignRight);
        label = new QLabel(tr("Max. D."));
        label->setAlignment(Qt::AlignRight);
        label->setMinimumWidth(width);
        label->setMaximumWidth(width);
        header->addWidget(label,0,Qt::AlignRight);
        rows_layout->addLayout(header);

        {
            QHBoxLayout *box = new QHBoxLayout;
            QLabel *nameLabel = new QLabel("Total");
            box->addWidget(nameLabel, Qt::AlignLeft);

            try
            {
                double mue = statistics->getError_MUE("BandStructure");
                QString value;
                value.sprintf("%+8.3lf", mue);
                QLabel *l_label = new QLabel(value);
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box->addWidget(l_label,0,Qt::AlignRight);
            }
            catch(DFTBTestSuite::Statistics::WrongArgumentException& e)
            {
                QLabel *l_label = new QLabel(tr("N/A"));
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box->addWidget(l_label,0,Qt::AlignRight);
            }

            try
            {
                QPair<double, double> mue = statistics->getError_MAX("BandStructure");
                QString value;
                value.sprintf("%+8.3lf", mue.first);
                QLabel *l_label = new QLabel(value);
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box->addWidget(l_label,0,Qt::AlignRight);
            }
            catch(DFTBTestSuite::Statistics::WrongArgumentException& e)
            {
                QLabel *l_label = new QLabel(tr("N/A"));
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box->addWidget(l_label,0,Qt::AlignRight);
            }

            rows_layout->addLayout(box);
        }


        for(int i=0; i<moles.size();++i)
        {
            QHBoxLayout *box = new QHBoxLayout;
            QLabel *nameLabel = new QLabel(statistics->getError_Name(moles[i]));
            QPushButton *namebutton = new QPushButton("Details...");
            namebutton->setProperty("setname", setname);
            namebutton->setProperty("ID", moles[i]);
            namebutton->setMaximumWidth(80);
            box->addWidget(nameLabel, Qt::AlignLeft);
            box->addWidget(namebutton, Qt::AlignLeft);
            box->addStretch(5);
            connect(namebutton, SIGNAL(clicked()), this, SLOT(showBSGraph()));

            try
            {
                double mue = statistics->getError_MUE("BandStructure", moles[i]);
                QString value;
                value.sprintf("%+8.3lf", mue);
                QLabel *l_label = new QLabel(value);
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box->addWidget(l_label,0,Qt::AlignRight);
            }
            catch(DFTBTestSuite::Statistics::WrongArgumentException& e)
            {
                QLabel *l_label = new QLabel(tr("N/A"));
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box->addWidget(l_label,0,Qt::AlignRight);
            }

            try
            {
                double mue = statistics->getError_MAX("BandStructure", moles[i]);
                QString value;
                value.sprintf("%+8.3lf", mue);
                QLabel *l_label = new QLabel(value);
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box->addWidget(l_label,0,Qt::AlignRight);
            }
            catch(DFTBTestSuite::Statistics::WrongArgumentException& e)
            {
                QLabel *l_label = new QLabel(tr("N/A"));
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box->addWidget(l_label,0,Qt::AlignRight);
            }

            rows_layout->addLayout(box);
        }
        prepareBSGraphData(setname, ref_data, calc_data);
    }

    if (map_type_layout.contains("ReactionEnergy"))
    {
        QHBoxLayout *type_layout = map_type_layout["ReactionEnergy"];
        QVBoxLayout *rows_layout = new QVBoxLayout();
//        QScrollArea *scrollArea = new QScrollArea;
//        scrollArea->setWidgetResizable(true);
//        QWidget* ala = new QWidget;
//        ala->setLayout(rows_layout);
//        scrollArea->setWidget(ala);
//        type_layout->addWidget(scrollArea);

        type_layout->addLayout(rows_layout);

        QList<QString> moles = statistics->getError_UUIDs("ReactionEnergy");

        int width = 55;
        QHBoxLayout *header = new QHBoxLayout;
        header->addStretch(5);
        QLabel *label = new QLabel(tr("M.A.D."));
        label->setAlignment(Qt::AlignRight);
        label->setMinimumWidth(width);
        label->setMaximumWidth(width);
        header->addWidget(label,0,Qt::AlignRight);
        label = new QLabel(tr("Max. D."));
        label->setAlignment(Qt::AlignRight);
        label->setMinimumWidth(width);
        label->setMaximumWidth(width);
        header->addWidget(label,0,Qt::AlignRight);
        rows_layout->addLayout(header);

        {
            QHBoxLayout *box = new QHBoxLayout;
            box->addWidget(new QLabel("Total"), Qt::AlignLeft);

            try
            {
                double mue = statistics->getError_MUE("ReactionEnergy");
                QString value;
                value.sprintf("%+8.3lf", mue);
                QLabel *l_label = new QLabel(value);
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box->addWidget(l_label,0,Qt::AlignRight);
            }
            catch(DFTBTestSuite::Statistics::WrongArgumentException& e)
            {
                QLabel *l_label = new QLabel(tr("N/A"));
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box->addWidget(l_label,0,Qt::AlignRight);
            }

            try
            {
                QPair<double, double> mue = statistics->getError_MAX("ReactionEnergy");
                QString value;
                value.sprintf("%+8.3lf", mue.first);
                QLabel *l_label = new QLabel(value);
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box->addWidget(l_label,0,Qt::AlignRight);
            }
            catch(DFTBTestSuite::Statistics::WrongArgumentException& e)
            {
                QLabel *l_label = new QLabel(tr("N/A"));
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box->addWidget(l_label,0,Qt::AlignRight);
            }

            rows_layout->addLayout(box);
        }
        for(int i=0; i<moles.size();++i)
        {
            QHBoxLayout *box = new QHBoxLayout;
            box->addWidget(new QLabel(statistics->getError_Name(moles[i])), Qt::AlignLeft);

            try
            {
                double mue = statistics->getError_MUE("ReactionEnergy", moles[i]);
                QString value;
                value.sprintf("%+8.3lf", mue);
                QLabel *l_label = new QLabel(value);
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box->addWidget(l_label,0,Qt::AlignRight);
            }
            catch(DFTBTestSuite::Statistics::WrongArgumentException& e)
            {
                QLabel *l_label = new QLabel(tr("N/A"));
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box->addWidget(l_label,0,Qt::AlignRight);
            }

            try
            {
                double mue = statistics->getError_MAX("ReactionEnergy", moles[i]);
                QString value;
                value.sprintf("%+8.3lf", mue);
                QLabel *l_label = new QLabel(value);
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box->addWidget(l_label,0,Qt::AlignRight);
            }
            catch(DFTBTestSuite::Statistics::WrongArgumentException& e)
            {
                QLabel *l_label = new QLabel(tr("N/A"));
                l_label->setMinimumWidth(width);
                l_label->setMaximumWidth(width);
                l_label->setAlignment(Qt::AlignRight);
                box->addWidget(l_label,0,Qt::AlignRight);
            }

            rows_layout->addLayout(box);
        }
    }



    QPushButton *butRemove = new QPushButton;
    butRemove->setProperty("setname", setname);
    butRemove->setText("X");
    butRemove->setMinimumSize(30,30);
    butRemove->setMaximumSize(30,30);
    layout->addWidget(butRemove,0,Qt::AlignCenter);
    connect(butRemove, SIGNAL(clicked()), this, SLOT(removePushed()));

}

void EvaluatedResultWindow::reset()
{
    QMapIterator<QString, QWidget*> it(m_items);
    while(it.hasNext())
    {
        it.next();
        delete it.value();
    }
    m_items.clear();


    QMapIterator<QPair<QString, QString>, QwtPlot*> it2(m_bsGraphs);
    while(it2.hasNext())
    {
        it2.next();
        delete it2.value();
    }
    m_bsGraphs.clear();
}

void EvaluatedResultWindow::prepareBSGraphData(const QString& setname, const DFTBTestSuite::ReferenceData *ref_data, const DFTBTestSuite::EvaluatedData *calc_data)
{
//    m_bsGraphs.clear();

//    QMapIterator<QString, DFTBTestSuite::BandStructureData> it(calc_data->bandstructureDataMapping);
//    while(it.hasNext())
//    {
//        it.next();
//        QString uuid = it.key();
//        const DFTBTestSuite::BandStructureData& calc_bs = it.value();

//        int ref_index = indexUUID(ref_data->crystalData.bandstructureData, uuid);
//        if (ref_index < 0)
//        {
//            continue;
//        }

//        const DFTBTestSuite::BandStructureData& ref_bs = ref_data->crystalData.bandstructureData[ref_index];


//        QwtPlot *m_plot = new QwtPlot();
//        m_plot->setTitle(QString("%1: %2").arg(setname).arg(ref_bs.name));

//        m_plot->setWindowModality(Qt::WindowModal);
//        m_plot->setCanvasBackground( Qt::white );
//        m_plot->setAxisScale( QwtPlot::yLeft, -15, 25 );

//        m_plot->setAxisTitle(QwtPlot::xBottom, "k-vector");
//        m_plot->setAxisTitle(QwtPlot::yLeft, "Energy[eV]");

//        double calc_shift = 0.0;
//        {
//            auto fermiIndex = calc_bs.fermiIndex;
//            if (fermiIndex.first > -1 && fermiIndex.first<calc_bs.bands.size() &&
//                    fermiIndex.second > -1 && fermiIndex.second <calc_bs.bands[fermiIndex.first].size() )
//            {
//                calc_shift = calc_bs.bands[fermiIndex.first][fermiIndex.second];
//            }
//        }
//        double ref_shift = 0.0;
//        {
//            auto fermiIndex = ref_bs.fermiIndex;
//            if (fermiIndex.first > -1 && fermiIndex.first<ref_bs.bands.size() &&
//                    fermiIndex.second > -1 && fermiIndex.second <ref_bs.bands[fermiIndex.first].size() )
//            {
//                ref_shift = ref_bs.bands[fermiIndex.first][fermiIndex.second];
//            }
//        }


//        {
//            int npoints = calc_bs.bands.first().size();
//            int nbands  = calc_bs.bands.size();
////            QwtLegendLabel *legendtext = new QwtLegendLabel(legend);
////            legendtext->setText(setname);


//            for (int i=0; i<nbands; ++i)
//            {
//                QwtPlotCurve *curve = new QwtPlotCurve("DFTB");
//                if (i!=0)
//                {
//                    curve->setItemAttribute( QwtPlotItem::Legend, false );
//                }

//                curve->setPen( Qt::red, 2 ),
//                curve->setRenderHint( QwtPlotItem::RenderAntialiased, true );

//                QPolygonF points;
//                for(int j=0; j<npoints; ++j)
//                {
//                    points << QPointF(j, calc_bs.bands[i][j]-calc_shift );
//                }

//                curve->setSamples( points );
//                curve->attach( m_plot );
//            }
//        }
//        {
//            int npoints = ref_bs.bands.first().size();
//            int nbands  = std::min(ref_bs.bands.size(), calc_bs.bands.size()+3);
//            for (int i=0; i<nbands; ++i)
//            {
//                QwtPlotCurve *curve = new QwtPlotCurve("Reference");
//                if (i!=0)
//                {
//                    curve->setItemAttribute( QwtPlotItem::Legend, false );
//                }

//                curve->setPen( Qt::black, 2 ),
//                curve->setRenderHint( QwtPlotItem::RenderAntialiased, true );

//                QPolygonF points;
//                for(int j=0; j<npoints; ++j)
//                {
//                    points << QPointF(j, ref_bs.bands[i][j]-ref_shift );
//                }

//                curve->setSamples( points );
//                curve->attach( m_plot );
//            }
//        }


//        QwtPlotLegendItem* legendItem = new QwtPlotLegendItem;
//        legendItem->setRenderHint(QwtPlotItem::RenderAntialiased);
//        legendItem->setMaxColumns(1);
//        QColor color( Qt::white );
//        legendItem->setTextPen( color );
//        legendItem->setBorderPen( color );
//        QColor c( Qt::gray );
//        c.setAlpha( 200 );
//        legendItem->setBackgroundBrush( c );

//        legendItem->attach(m_plot);
//        legendItem->setAlignment(Qt::AlignRight | Qt::AlignTop);
//        legendItem->setBackgroundMode(QwtPlotLegendItem::LegendBackground);
//        m_bsGraphs.insert(qMakePair(setname, uuid), m_plot);
//    }

}
