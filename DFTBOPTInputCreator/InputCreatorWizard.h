#ifndef INPUTCREATORWIZARD_H
#define INPUTCREATORWIZARD_H

#include <QWizard>
#include "dftbrefdatabase.h"
#include <QFuture>
#include "Profile.h"





class DatabaseSelectPage;
class ElementSelectPage;
class TrainingSetPage;
//class TrainingSetEvaluationPage;
class ErepfitSetPage;

class ElectronicFittingSetPage;
class FinalPage;
class TestSuiteTestSetPage;
class ErepfitEquationSelectionPage;
class MiscellaneousOptionPage;

class InputCreatorWizard : public QWizard
{
    friend class Exportor;
    Q_OBJECT
public:
    explicit InputCreatorWizard(QWidget *parent = 0);
    ~InputCreatorWizard();
    enum
    {
        Page_SelectDatabase,
        Page_SelectElement,
        Page_TrainingSet,
//        Page_TrainingSetEvaluation,
        Page_MiscellaneousOptionPage,
        Page_Final
    };
signals:

public slots:
    void changePage(int id);
private slots:
    void currentIdChanged(int id);
    void showGeneratingSet();
    void FinalSave(const QString& filename);
    // QWidget interface
    void customButtonClicked(int button);
    void trainingSetSelectionChanged(const QString& propertyName, bool selected, const QString& uuid, double weight);
    void erepfitSelectionChanged(const QString& propertyName, bool selected, const QString& uuid, double weight);
    void saveSelection();
    void loadSelection();
public:
    QSize sizeHint() const;
private:
    DFTBRefDatabase* m_database;
    ElementSelectPage* m_pageElementSelectPage;
    DatabaseSelectPage* m_pageSelectDatabase;
    TrainingSetPage *m_pageTrainingSet;
//    TrainingSetEvaluationPage *m_pageTrainingSetEvaluation;
    ErepfitSetPage* m_pageErepfitSet;
    MiscellaneousOptionPage* m_pageMiscellaneousOption;
    FinalPage* m_pageFinal;
    DFTBOPTProfile m_profile;
private:


protected:
    void initializePage(int id);
};

#endif // INPUTCREATORWIZARD_H
