#ifndef TRAININGSETPAGE_H
#define TRAININGSETPAGE_H

#include <QMenuBar>
#include <QWizardPage>
#include <QTabWidget>
#include <QTableView>
#include <QStringList>

class DFTBRefDatabase;
class ElementFilteredTableModel;
class PropertyTableView;
class PropertySelection;
class TrainingSetPage : public QWizardPage
{
    Q_OBJECT
public:

    enum PROPERTY_TYPE
    {
        Property_Geometry = 0,
        Property_Bandstructure = 1,
        Property_Frequency = 2,
        Property_AtomizationEnergy = 3,
        Property_ReactionEnergy = 4
    };

    TrainingSetPage(DFTBRefDatabase *database, QWidget *parent = 0);
    virtual ~TrainingSetPage();

    QList<QPair<QString, double> > geometryTargets() const;
    QList<QPair<QString, double> > bandstructureTargets() const;
    QList<QPair<QString, double> > frequencyTargets() const;
    QList<QPair<QString, double> > atomizationEnergyTargets() const;
    QList<QPair<QString, double> > reactionEnergyTargets() const;
    void setSelectedElements(const QStringList& elements);
    QStringList selectedElements() const;
signals:
    void selectionChanged(int type, bool selected, const QString& uuid, double weight);
    void selectionChanged(const QString& propertyName, bool selected, const QString& uuid, double weight);
    void showGeneratingSetPage() ;
    void saveSelection();
    void loadSelection();
public slots:
    void reload();
    void postload();
    void updateSelection(const QList<PropertySelection*>& properties);
    void clear();
private slots:
    void showGeometryDetail(const QString& uuid);
    void showBandstructureDetail(const QString& uuid);
    void showFrequencyDetail(const QString& uuid);
    void showReactionEnergyDetail(const QString& uuid);
    void propertySelectionChanged(bool selected, const QString& uuid, double weight);
private:
    DFTBRefDatabase *m_database = nullptr;
    QTabWidget *m_tabWidget = nullptr;
    PropertyTableView *m_tableGeometry = nullptr;
    PropertyTableView *m_tableBandstructure = nullptr;
    PropertyTableView *m_tableFrequency = nullptr;
    PropertyTableView *m_tableAtomizationEnergy = nullptr;
    PropertyTableView *m_tableReactionEnergy = nullptr;

    QStringList m_elements;

    QMenuBar* menubar = nullptr;
    QMenu* menu_Adv = nullptr;
    QAction* action_erepfit = nullptr;
    QAction* action_load_select = nullptr;
    QAction* action_save_select = nullptr;



    // QWizardPage interface
public:
    void initializePage();

    // QWizardPage interface
public:
    bool validatePage();
};

#endif // TRAININGSETPAGE_H
