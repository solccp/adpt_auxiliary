#include "DatabaseSelectPage.h"

#include "dftbrefdatabase.h"


DatabaseSelectPage::DatabaseSelectPage(DFTBRefDatabase *database, QWidget *parent) :
    QWizardPage(parent)
{
//    setCommitPage(true);
    m_database = database;
    setTitle("Data Source");
    setSubTitle("Setup data source for reference data.");


    openRadioButton =  new QRadioButton(tr("&Open local databse file"));
    openRadioButton->setEnabled(false);

    QHBoxLayout *box = new QHBoxLayout;
    connectRadioButton =  new QRadioButton(tr("&Connect to database server"));
    labelProfileName = new QLabel();
    butConnectDb = new QPushButton(tr("Configure..."));

    connect(butConnectDb, SIGNAL(clicked()), this, SLOT(configDatabase()));

    QLabel* tip = new QLabel(tr("Currently, this program supports MySQL or MariaDB 5.5 as the data source.<br>"
                                "For more information, please check<br>"
                                "<p>MySQL: <a href=http://www.mysql.com>http://www.mysql.com/</a>"
                                "<p>MariaDB: <a href=http://mariadb.org>http://mariadb.org/</a>"
                                "<p>If the database has not been initialized, please follow <a href=tba>here</a>"));
    tip->setTextFormat(Qt::RichText);

    box->addWidget(connectRadioButton);
    box->addSpacing(50);
    box->addWidget(labelProfileName);
    box->addSpacing(50);
    box->addWidget(butConnectDb);

    box->addStretch(5);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(tip);
    layout->addSpacing(50);
//    layout->addWidget(openRadioButton);
    layout->addLayout(box);
    connectRadioButton->setChecked(true);
    this->setLayout(layout);

}

void DatabaseSelectPage::configDatabase()
{
    dia.exec();
    labelProfileName->setText(dia.profile());
}


bool DatabaseSelectPage::validatePage()
{
    m_database->closeDatabase();
    m_database->loadOnlineDatabase(dia.server_addr(), dia.account(), dia.password(), dia.database_name());
    if (!m_database->isOpen())
    {
        QMessageBox vbox;
        vbox.setStandardButtons(QMessageBox::Ok);
        vbox.setIcon(QMessageBox::Critical);
        vbox.setText("Database connection failed\nPlease configure the connection first");
        vbox.exec();
        return false;
    }
    return true;
}


void DatabaseSelectPage::initializePage()
{
    labelProfileName->setText(dia.profile());
}
