#ifndef SELECTIONLISTWIDGET_H
#define SELECTIONLISTWIDGET_H

#include <QTableWidget>
#include <QStringList>
#include <QSet>
QT_BEGIN_NAMESPACE
class QAbstractItemModel;
QT_END_NAMESPACE

class DFTBRefDatabase;
class SelectionListWidget : public QWidget
{
    Q_OBJECT
public:
    SelectionListWidget(QAbstractItemModel *totalModel, const QString& elem1, const QString& elem2,
                        DFTBRefDatabase* database,
                        QWidget *parent = 0);

    QSet<QString> selectedUUIDs() const;
    ~SelectionListWidget();

signals:
    void numOfSelectionChanged(const QString& elem1, const QString& elem2, int num);
    void itemSelected(const QString& elem1, const QString& elem2, const QString& uuid);
    void itemUnslected(const QString& elem1, const QString& elem2, const QString& uuid);
public slots:
    void reset();
    void load_data();
    void selectItem(const QString& uuid);
    void unselectItem(const QString& uuid);
private slots:
    void checkStateChanged(QTableWidgetItem *item);

private:
    bool m_loaded = false;
    void setupUI();
    QSet<QString> m_selectedUUIDs;
    QHash<QString, int> m_uuidIndex;
    QAbstractItemModel *m_totalModel;
private:
    DFTBRefDatabase* m_database;
    QString m_elem1;
    QString m_elem2;

    QTableWidget *m_listWidget;

    // QWidget interface
public:
    QSize sizeHint() const;
};

#endif // SELECTIONLISTWIDGET_H
