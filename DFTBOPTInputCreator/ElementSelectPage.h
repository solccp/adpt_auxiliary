#ifndef ELEMENTSELECTDIALOG_H
#define ELEMENTSELECTDIALOG_H

#include <QWizardPage>
#include <QPushButton>
#include <QSet>
#include <QGridLayout>

class ElementSelectPage : public QWizardPage
{
    Q_OBJECT

public:
    ElementSelectPage(QWidget *parent = 0);
    ~ElementSelectPage();

public slots:
//    void setAvailableElements(const QStringList& ava_elems);

private:
    QGridLayout* layout_grid;
    QPushButton* elem_H;
    QPushButton* elem_He;

    QPushButton* elem_Li;
    QPushButton* elem_Be;
    QPushButton* elem_B;
    QPushButton* elem_C;
    QPushButton* elem_N;
    QPushButton* elem_O;
    QPushButton* elem_F;
    QPushButton* elem_Ne;

    QPushButton* elem_Na;
    QPushButton* elem_Mg;
    QPushButton* elem_Al;
    QPushButton* elem_Si;
    QPushButton* elem_P;
    QPushButton* elem_S;
    QPushButton* elem_Cl;
    QPushButton* elem_Ar;

    QPushButton* elem_K;
    QPushButton* elem_Ca;
    QPushButton* elem_Sc;
    QPushButton* elem_Ti;
    QPushButton* elem_V;
    QPushButton* elem_Cr;
    QPushButton* elem_Mn;
    QPushButton* elem_Fe;
    QPushButton* elem_Co;
    QPushButton* elem_Ni;
    QPushButton* elem_Cu;
    QPushButton* elem_Zn;
    QPushButton* elem_Ga;
    QPushButton* elem_Ge;
    QPushButton* elem_As;
    QPushButton* elem_Se;
    QPushButton* elem_Br;
    QPushButton* elem_Kr;


    QPushButton* elem_Rb;
    QPushButton* elem_Sr;
    QPushButton* elem_Y;
    QPushButton* elem_Zr;
    QPushButton* elem_Nb;
    QPushButton* elem_Mo;
    QPushButton* elem_Tc;
    QPushButton* elem_Ru;
    QPushButton* elem_Rh;
    QPushButton* elem_Pd;
    QPushButton* elem_Ag;
    QPushButton* elem_Cd;
    QPushButton* elem_In;
    QPushButton* elem_Sn;
    QPushButton* elem_Sb;
    QPushButton* elem_Te;
    QPushButton* elem_I;
    QPushButton* elem_Xe;


    QPushButton* elem_Cs;
    QPushButton* elem_Ba;
    QPushButton* elem_Hf;
    QPushButton* elem_Ta;
    QPushButton* elem_W;
    QPushButton* elem_Re;
    QPushButton* elem_Os;
    QPushButton* elem_Ir;
    QPushButton* elem_Pt;
    QPushButton* elem_Au;
    QPushButton* elem_Hg;
    QPushButton* elem_Tl;
    QPushButton* elem_Pb;
    QPushButton* elem_Bi;
    QPushButton* elem_Po;
    QPushButton* elem_At;
    QPushButton* elem_Rn;

    QPushButton* elem_Fr;
    QPushButton* elem_Ra;

    QPushButton* elem_La;
    QPushButton* elem_Ce;
    QPushButton* elem_Pr;
    QPushButton* elem_Nd;
    QPushButton* elem_Pm;
    QPushButton* elem_Sm;
    QPushButton* elem_Eu;
    QPushButton* elem_Gd;
    QPushButton* elem_Tb;
    QPushButton* elem_Dy;
    QPushButton* elem_Ho;
    QPushButton* elem_Er;
    QPushButton* elem_Tm;
    QPushButton* elem_Yb;
    QPushButton* elem_Lu;

    QPushButton* elem_Ac;
    QPushButton* elem_Th;
    QPushButton* elem_Pa;
    QPushButton* elem_U ;
    QPushButton* elem_Np;
    QPushButton* elem_Pu;
    QPushButton* elem_Am;
    QPushButton* elem_Cm;
    QPushButton* elem_Bk;
    QPushButton* elem_Cf;
    QPushButton* elem_Es;
    QPushButton* elem_Fm;
    QPushButton* elem_Md;
    QPushButton* elem_No;
    QPushButton* elem_Lr;

    QSet<QPushButton*> elems;

    // QWizardPage interface
public:
    void initializePage();
    QStringList getSelectedElements() const;
    void setSelectedElements(const QStringList& elements);
private:

    // QWizardPage interface
public:
    bool validatePage();

    // QWizardPage interface
public:
    int nextId() const;
};

#endif // ELEMENTSELECTDIALOG_H
