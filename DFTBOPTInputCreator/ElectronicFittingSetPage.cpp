#include "ElectronicFittingSetPage.h"
#include "SelectionListWidget.h"
#include "dftbrefdatabase.h"

#include <QDebug>
#include <QPushButton>
#include <QVBoxLayout>
#include <QSqlTableModel>

ElectronicFittingSetPage::ElectronicFittingSetPage(DFTBRefDatabase* database, QWidget *parent) :
    QWizardPage(parent), m_database(database)
{
    m_table = new QTableWidget(this);
    QVBoxLayout *layout = new QVBoxLayout();
    layout->addWidget(m_table);
    this->setLayout(layout);

    this->setTitle("Electronic part fitting set settings");
    this->setSubTitle("Select bandstructures for fitting electronic parameters");

}

void ElectronicFittingSetPage::show_details()
{
    int row = sender()->property("ROW").toInt();
    int column = sender()->property("COLUMN").toInt();
    SelectionListWidget *list = m_listwidgets.value(qMakePair(row, column));

//    list->resize(400,400);
    list->setWindowFlags(Qt::Popup);
    QPushButton *but  = qobject_cast<QPushButton*>(sender());

    QPoint pos = mapToGlobal(but->pos());
    pos.ry() += but->height()+30;
    pos.rx() += 30;
    list->move(pos);
    list->show();
}

void ElectronicFittingSetPage::itemSelectionNumberChanged(const QString &elem1, const QString &elem2, int num)
{
//    qDebug() << "itemSelectionNumberChanged" << elem1 << elem2 << num;
    int iElem1 = SelectedElements.indexOf(elem1);
    int iElem2 = SelectedElements.indexOf(elem2);
    QPushButton *but  = qobject_cast<QPushButton*>(m_table->cellWidget(iElem1, iElem2));
    if (but)
    {
        if (num > 0)
        {
            but->setStyleSheet("background-color:green; color: white; QPushButton {text-align: center;}");
            but->setProperty("IS_OK", true);
            if (m_listwidgets.value(qMakePair(iElem1,iElem1))->selectedUUIDs().isEmpty())
            {
                QPushButton *but2  = qobject_cast<QPushButton*>(m_table->cellWidget(iElem1, iElem1));
                but2->setStyleSheet("background-color:GoldenRod; color: white");
                but2->setProperty("IS_OK", true);
            }
            if (m_listwidgets.value(qMakePair(iElem2,iElem2))->selectedUUIDs().isEmpty())
            {
                QPushButton *but2  = qobject_cast<QPushButton*>(m_table->cellWidget(iElem2, iElem2));
                but2->setStyleSheet("background-color:GoldenRod; color: white");
                but2->setProperty("IS_OK", true);
            }
        }
        else
        {
            if (elem1 == elem2)
            {

                bool allZero = true;
                for(int i=0; i<SelectedElements.size();++i)
                {
                    if (i == iElem1)
                    {
                        continue;
                    }
                    auto pair  = qMakePair(iElem1,i);
                    if (i < iElem1)
                    {
                        qSwap(pair.first, pair.second);
                    }
                    if (!m_listwidgets.value(pair)->selectedUUIDs().isEmpty())
                    {
                        allZero = false;
                        break;
                    }
                }

                if (allZero)
                {
                    but->setStyleSheet("background-color:red; color: white");
                    but->setProperty("IS_OK", false);
                }
                else
                {
                    but->setStyleSheet("background-color:GoldenRod; color: white");
                    but->setProperty("IS_OK", true);
                }

            }
            else
            {
                but->setStyleSheet("background-color:GoldenRod; color: white");

                if (m_listwidgets.value(qMakePair(iElem1,iElem1))->selectedUUIDs().isEmpty())
                {
                    QPushButton *but2  = qobject_cast<QPushButton*>(m_table->cellWidget(iElem1, iElem1));
                    but2->setStyleSheet("background-color:red; color: white");
                    but2->setProperty("IS_OK", false);
                }
                if (m_listwidgets.value(qMakePair(iElem2,iElem2))->selectedUUIDs().isEmpty())
                {
                    QPushButton *but2  = qobject_cast<QPushButton*>(m_table->cellWidget(iElem2, iElem2));
                    but2->setStyleSheet("background-color:red; color: white");
                    but2->setProperty("IS_OK", false);
                }

            }
        }
        QString text = QString("%1-%2\n(%3)").arg(elem1).arg(elem2).arg(num);
        but->setText(text);
    }
}

void ElectronicFittingSetPage::itemSelected(const QString &elem1, const QString &elem2, const QString &uuid)
{
    int iElem1 = SelectedElements.indexOf(elem1);
    int iElem2 = SelectedElements.indexOf(elem2);
    int nElems = SelectedElements.size();
    for(int i=0; i<nElems; ++i)
    {
        for(int j=i; j<nElems; ++j)
        {
            if (i==iElem1 && j==iElem2)
            {
                continue;
            }
            SelectionListWidget *list = this->m_listwidgets.value(qMakePair(i, j));
            list->selectItem(uuid);
        }
    }
}

void ElectronicFittingSetPage::itemUnselected(const QString &elem1, const QString &elem2, const QString &uuid)
{
    int iElem1 = SelectedElements.indexOf(elem1);
    int iElem2 = SelectedElements.indexOf(elem2);
    int nElems = SelectedElements.size();
    for(int i=0; i<nElems; ++i)
    {
        for(int j=i; j<nElems; ++j)
        {
            if (i==iElem1 && j==iElem2)
            {
                continue;
            }
            SelectionListWidget *list = this->m_listwidgets.value(qMakePair(i, j));
            list->unselectItem(uuid);
        }
    }
}

QStringList ElectronicFittingSetPage::getSelectedElements() const
{
    return SelectedElements;
}

void ElectronicFittingSetPage::setSelectedElements(const QStringList &value)
{
    if (SelectedElements == value)
        return;

    SelectedElements = value;
    reset();
    load();
}

void ElectronicFittingSetPage::reset()
{
    m_hasLoaded = false;
}

void ElectronicFittingSetPage::load()
{
    if (m_hasLoaded)
        return;
    m_table->clear();

    QMapIterator<QPair<int, int>, SelectionListWidget*> it(m_listwidgets);
    while(it.hasNext())
    {
        it.next();
        SelectionListWidget *list = it.value();
        list->setParent(0);
        delete list;
    }
    m_listwidgets.clear();

    m_table->setEditTriggers(QTableView::NoEditTriggers);
    int nElems = SelectedElements.size();
    m_table->setRowCount(nElems);
    m_table->setColumnCount(nElems);
    m_table->setHorizontalHeaderLabels(SelectedElements);
    m_table->setVerticalHeaderLabels(SelectedElements);



    QStringList allAvailableElements = m_database->getAvailableElements();

    QStringList noElements;
    QStringListIterator it_avelem(allAvailableElements);
    while(it.hasNext())
    {
        QString elem = it_avelem.next();
        if (SelectedElements.contains(elem))
        {
            continue;
        }
        noElements.append(elem);
    }
    QSqlTableModel *totalModel = new QSqlTableModel(this, m_database->database());
    totalModel->setTable("ElectronicFittingDefaultSelection");
    QStringListIterator elem_in_noElements(noElements);
    QStringList sql_elements;
    while(elem_in_noElements.hasNext())
    {
        QString elem = elem_in_noElements.next();
        sql_elements.append( "elements not like '%[" + elem + "]%'");
    }
    QString sql_full = sql_elements.join(" AND ");

    totalModel->setFilter(sql_full);
    totalModel->setSort(2, Qt::AscendingOrder);
    totalModel->select();



    for(int i=0; i<nElems; ++i)
    {
        for(int j=i; j<nElems; ++j)
        {
            QString elem1 = SelectedElements.at(i);
            QString elem2 = SelectedElements.at(j);
            QString text = QString("%1-%2").arg(elem1).arg(elem2);
            QPushButton *but = new QPushButton(text);
            but->setMinimumHeight(60);
            but->setMinimumWidth(60);

            but->setProperty("ROW", i);
            but->setProperty("COLUMN", j);

            SelectionListWidget *list = new SelectionListWidget(totalModel, elem1, elem2, m_database, this);

            connect(list, SIGNAL(numOfSelectionChanged(QString,QString,int)), this, SLOT(itemSelectionNumberChanged(QString,QString,int)));
            connect(list, SIGNAL(itemSelected(QString,QString,QString)), this, SLOT(itemSelected(QString,QString,QString)));
            connect(list, SIGNAL(itemUnslected(QString,QString,QString)), this, SLOT(itemUnselected(QString,QString,QString)));


            m_listwidgets.insert(qMakePair(i,j),list);
            connect(but, SIGNAL(clicked()), this, SLOT(show_details()));
            m_table->setCellWidget(i,j,but);
        }
    }

    for(int i=0; i<nElems; ++i)
    {
        for(int j=i; j<nElems; ++j)
        {
            m_listwidgets.value(qMakePair(i,j))->load_data();
        }
    }

    m_table->resizeColumnsToContents();
    m_table->resizeRowsToContents();
    m_hasLoaded = true;
}
QSet<QString> ElectronicFittingSetPage::fittingSetSelection() const
{
    return m_fittingSetSelection;
}


void ElectronicFittingSetPage::initializePage()
{

}


bool ElectronicFittingSetPage::validatePage()
{
    //Don't force to select bandstructure fitting
    m_fittingSetSelection.clear();
    bool valid = true;
    int nElems = SelectedElements.size();
    for(int i=0; i<nElems; ++i)
    {
        for(int j=i; j<nElems; ++j)
        {
            SelectionListWidget *list = this->m_listwidgets.value(qMakePair(i, j));
            m_fittingSetSelection.unite(list->selectedUUIDs());

//            if (i == j)
//            {
//                QPushButton *but  = qobject_cast<QPushButton*>(m_table->cellWidget(i, j));
//                if (but->property("IS_OK").toBool() == false)
//                {
//                    valid = false;
//                }
//            }

        }
    }
    return valid;
}
