#ifndef EXPORTOR_H
#define EXPORTOR_H

#include <QObject>
#include <QSet>
#include <QString>
#include <QMap>
#include <QVariant>
namespace bugless
{
    class Archive;
}
namespace DFTBPSO
{
    namespace Input
    {
        class SKOPTInput;
    }
}

namespace DFTBTestSuite
{
    class InputData;
}

namespace SKBuilder
{
    namespace Input
    {
        class SKBuilderInput;
    }
}

class BaseEquationSelection
{
public:
    QString UUID;
    double force_weight = 0.0;
    bool hasEnergy = false;
    bool hasForce = false;
    double energy = 0.0;
    double energy_weight = 0.0;
};


class InputCreatorWizard;
class Exportor : public QObject
{
    Q_OBJECT
public:
    Exportor(const QString& filename, InputCreatorWizard* wizard, QObject *parent = 0);

    enum MsgType
    {
        MSG_INFO = 0,
        MSG_SPECIAL_INFO = 1,
        MSG_WARNING = 2,
        MSG_ERROR = -1
    };

signals:
    void Message(int msgType, const QString& msg);
    void finished();
public slots:
    void save();
private:
    QString m_filename;
    QSet<QString> xyzfilenames;
    InputCreatorWizard* m_wizard;
    void fetchErepfitAdditionEquation(const QStringList& repulsive_pairs, DFTBPSO::Input::SKOPTInput& dftb_pso_input);
    void fetchErepfitRepulsiveDefinition(const QStringList& repulsive_pairs, DFTBPSO::Input::SKOPTInput& dftb_pso_input);
    void buildDefaultConfiningInput(DFTBPSO::Input::SKOPTInput &dftb_pso_input, const QStringList &selectedElements, const QMap<QString, QVariant> &orbs_map);
    void buildDefaultAtomicInfoInSKBuilderInput(const QMap<QString, QVariant>& orbs_map,
                                                const QMap<QString, QVariant>& occ_map,
                                                const QStringList& selectedElements,
                                                const QMap<QString, QVariant>& hubb_map,
                                                const QMap<QString, QVariant>& oe_map,
                                                SKBuilder::Input::SKBuilderInput &input);
    void fetchDefaultOnecentInput(const QStringList& selectedElements, bugless::Archive& zipfile);
    void createDefaultDFTBPlusInput(const QStringList &selectedElements, DFTBPSO::Input::SKOPTInput &dftb_pso_input, DFTBTestSuite::InputData &input_data, bugless::Archive &zipfile);
    void fetchErepfitBaseEquation(DFTBPSO::Input::SKOPTInput &dftb_pso_input);
    void fetchErepfitReactionEquation(DFTBPSO::Input::SKOPTInput &dftb_pso_input);
    QMap<QString, QVariant> loadDefaultHubbard();
    QMap<QString, QVariant> loadDefaultHubbardDerivs();
    QMap<QString, QVariant> loadDefaultOrbitalEnergy();
    QMap<QString, QVariant> loadDefaultOrbitalOccupation();
    QMap<QString, QVariant> loadDefaultOrbitalDefinition();
    QString getGoodFilename(const QString &oriFilename) const;
};

#endif // EXPORTOR_H
