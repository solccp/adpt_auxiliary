#include "PredefinedSetsEditWidget.h"
#include <QHBoxLayout>
#include <QTreeWidget>
#include <QSqlQueryModel>
#include <QSqlRecord>
#include <QTableView>
#include <QTreeWidgetItemIterator>
#include <QSqlQuery>
#include <QMessageBox>
#include <QCloseEvent>
#include <QDebug>

#include <QSqlTableModel>
#include <QSortFilterProxyModel>
#include <QSqlError>
#include <QMenu>

#include "sqlrelationaltablemodel.h"

#include <QVBoxLayout>
#include <QHBoxLayout>


#include <QUuid>


#include <QResizeEvent>

#include <QStyledItemDelegate>
#include <QApplication>
#include <QHeaderView>

class ItemDelegate : public QStyledItemDelegate
{
public:
    ItemDelegate(QObject *parent = 0)
        : QStyledItemDelegate(parent)
    {
    }

    void paint ( QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const
    {
        QStyleOptionViewItemV4 viewItemOption(option);

//        if (index.column() == 0)
        {
            const int textMargin = QApplication::style()->pixelMetric(QStyle::PM_FocusFrameHMargin) + 1;
            QRect newRect = QStyle::alignedRect(option.direction, Qt::AlignCenter,
                                                QSize(option.decorationSize.width() + 5,option.decorationSize.height()),
                                                QRect(option.rect.x() + textMargin, option.rect.y(),
                                                      option.rect.width() - (2 * textMargin), option.rect.height()));
            viewItemOption.rect = newRect;
        }
        QStyledItemDelegate::paint(painter, viewItemOption, index);
    }

    virtual bool editorEvent(QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option,
                             const QModelIndex &index)
    {
        Q_ASSERT(event);
        Q_ASSERT(model);

        // make sure that the item is checkable
        Qt::ItemFlags flags = model->flags(index);
        if (!(flags & Qt::ItemIsUserCheckable) || !(flags & Qt::ItemIsEnabled))
            return false;
        // make sure that we have a check state
        QVariant value = index.data(Qt::CheckStateRole);
        if (!value.isValid())
            return false;
        // make sure that we have the right event type
        if (event->type() == QEvent::MouseButtonRelease) {
            const int textMargin = QApplication::style()->pixelMetric(QStyle::PM_FocusFrameHMargin) + 1;
            QRect checkRect = QStyle::alignedRect(option.direction, Qt::AlignCenter,
                                                  option.decorationSize,
                                                  QRect(option.rect.x() + (2 * textMargin), option.rect.y(),
                                                        option.rect.width() - (2 * textMargin),
                                                        option.rect.height()));
            if (!checkRect.contains(static_cast<QMouseEvent*>(event)->pos()))
                return false;
        } else if (event->type() == QEvent::KeyPress) {
            if (static_cast<QKeyEvent*>(event)->key() != Qt::Key_Space&& static_cast<QKeyEvent*>(event)->key() != Qt::Key_Select)
                return false;
        } else {
            return false;
        }
        Qt::CheckState state = (static_cast<Qt::CheckState>(value.toInt()) == Qt::Checked
                                ? Qt::Unchecked : Qt::Checked);
        return model->setData(index, state, Qt::CheckStateRole);
    }
};





PredefinedSetsEditWidget::PredefinedSetsEditWidget(DFTBRefDatabase *database, bool editable, QWidget *parent) :
    QWidget(parent), m_database(database)
{

    m_treeView = new ReorderableTreeWidget();
    m_tableView = new QTableView();
    m_tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);


    QHBoxLayout *layout = new QHBoxLayout();
    layout->addWidget(m_treeView, 1);
    layout->addWidget(m_tableView,2);
    this->setLayout(layout);

    connect(m_treeView, SIGNAL(itemClicked(QTreeWidgetItem*,int)), this, SLOT(itemClicked(QTreeWidgetItem*,int)));
    connect(m_treeView, SIGNAL(itemDoubleClicked(QTreeWidgetItem*,int)), this, SLOT(itemDoubleClicked(QTreeWidgetItem*,int)));
    connect(m_treeView, SIGNAL(itemSelectionChanged()), this, SLOT(itemSelectionChanged()));

    m_treeView->setExpandsOnDoubleClick(false);



    m_tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    m_tableView->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(m_tableView, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(tableViewContextMenu(QPoint)));

    m_treeView->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(m_treeView, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(treeViewContextMenu(QPoint)));


    m_tableView->setSelectionBehavior(QAbstractItemView::SelectRows);

    m_tableView->setSortingEnabled(true);

    connect(m_tableView, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(tableCellDoubleClicked(QModelIndex)));
}

void PredefinedSetsEditWidget::tableCellDoubleClicked(const QModelIndex &index)
{
    if (m_model_table->headerData(index.column(), Qt::Horizontal).toString().toLower() == "weight")
    {
        m_tableView->edit(index);
    }
}

QSet<QString> PredefinedSetsEditWidget::selectedSets() const
{
    QSet<QString> selectedSets;

    QTreeWidgetItemIterator it(m_treeView);
    while (*it)
    {
        QTreeWidgetItem *item = (*it);
        if (item->childCount() == 0 && item->checkState(0) == Qt::Checked)
        {
            selectedSets.insert(item->data(0, Qt::UserRole).toString());
        }
        ++it;
    }
    return selectedSets;
}

void PredefinedSetsEditWidget::setElementFilter(const QStringList &elems)
{
    int hcount = m_model_table->columnCount();
    for(int i=0; i<hcount; ++i)
    {
        if (m_model_table->headerData(i,Qt::Horizontal, Qt::DisplayRole).toString() == "elements")
        {
            m_model_table->setElementFilterKeyColumn(i);
            break;
        }
    }
    m_model_table->setElementFilter(elems);
}

void PredefinedSetsEditWidget::setTableItemCheckState()
{
    for(int i=0; i<sourceModel->rowCount();++i)
    {
        QModelIndex index = sourceModel->index(i,0);
        QModelIndex checkindex = sourceModel->index(i,sourceModel->checkableColumnIndex());

        if ( m_selectedSets.contains(sourceModel->data(index,Qt::EditRole).toString()))
        {
            sourceModel->setData(checkindex, Qt::Checked, Qt::CheckStateRole);
        }
        else
        {
            sourceModel->setData(checkindex, Qt::Unchecked, Qt::CheckStateRole);
        }
    }
}

QHash<QPair<int, QString>, double > PredefinedSetsEditWidget::getTestSuiteSet()
{
    QHash<QPair<int, QString>, double > res;
    m_model_table->setFilterFixedString("");

    int weightIndex = sourceModel->fieldIndex("weight");
    for(int i=0; i<m_model_table->rowCount(); ++i)
    {
        QModelIndex checkIndex = m_model_table->index(i,sourceModel->checkableColumnIndex());
        QModelIndex TypeIndex = m_model_table->index(i,2);
        if (m_model_table->data(checkIndex,Qt::CheckStateRole) == Qt::Checked)
        {
            int typeID = m_model_table->data(TypeIndex,Qt::EditRole).toInt();
            QString uuid = m_model_table->data(checkIndex, Qt::EditRole).toString();
            double weight = m_model_table->data(m_model_table->index(i, weightIndex)).toDouble();
            res.insert(qMakePair(typeID,uuid), weight);
        }
    }
    return res;
}

void PredefinedSetsEditWidget::itemClicked(QTreeWidgetItem *item, int column)
{
    Q_UNUSED(column);
    checkChildState(item);
    checkParentState(item);

    QSet<QString> selectedSets_new = selectedSets();

    if (m_selectedSets != selectedSets_new)
    {
        m_selectedSets = selectedSets_new;

        setTableItemCheckState();
        emit selectionChanged(m_selectedSets);
    }




}

void PredefinedSetsEditWidget::itemReordered(QTreeWidgetItem *moveditem, QTreeWidgetItem *old_parent)
{
    QTreeWidgetItem* new_parent = moveditem->parent();

    QString new_parent_id = QUuid().toString();
    QString old_parent_id = new_parent_id;

    if (new_parent)
    {
        new_parent_id = new_parent->data(0, Qt::UserRole).toString();
    }

    if (old_parent)
    {
        old_parent_id = old_parent->data(0, Qt::UserRole).toString();
    }

    QVariant moveditem_id = moveditem->data(0, Qt::UserRole);

    if (old_parent_id == new_parent_id)
        return;

    QSortFilterProxyModel *model = new QSortFilterProxyModel();
    model->setSourceModel(m_model_tree);

    model->setFilterKeyColumn(0);
    model->setFilterFixedString(moveditem_id.toString());

    if (model->rowCount() == 1)
    {
        model->setData(model->index(0,1), new_parent_id);
    }
    delete model;
}

void PredefinedSetsEditWidget::itemDoubleClicked(QTreeWidgetItem *item, int column)
{
    if (!isEditable())
    {
        return;
    }
}



void PredefinedSetsEditWidget::itemSelectionChanged()
{
    QTreeWidgetItem *item = m_treeView->currentItem();
    if (item)
    {
        QList<QString> all_setids = allSetIds(item);
        QStringList filter;
        for(int i=0; i<all_setids.size();++i)
        {
            filter.append(all_setids[i].replace("{", "\\{").replace("}", "\\}"));
        }
        m_model_table->setFilterRegExp(filter.join("|"));
        m_model_table->setFilterKeyColumn(0);
        m_tableView->resizeColumnsToContents();
        m_tableView->setColumnWidth(1, 50);
    }
}

void PredefinedSetsEditWidget::treeViewContextMenu(const QPoint &pos)
{
    if (!m_isEditable)
    {
        return ;
    }

    QMenu *menu = new QMenu();

    QTreeWidgetItem *item = m_treeView->itemAt(pos);

    QList<QAction*> actions;
    if (item)
    {
        QAction *action = new QAction("Add New Subset...", menu);
        connect(action, SIGNAL(triggered()), this, SLOT(addSubSet()));
        action->setProperty("SET_ID", item->data(0, Qt::UserRole));
        actions.append(action);

        action = new QAction("Remove Selected Set", menu);
        connect(action, SIGNAL(triggered()), this, SLOT(removeSet()));
        action->setProperty("SET_ID", item->data(0, Qt::UserRole));
        actions.append(action);
    }
    else
    {
        QAction *action = new QAction("Add New Set...", menu);
        connect(action, SIGNAL(triggered()), this, SLOT(addSubSet()));
        action->setProperty("SET_ID", QUuid().toString());
        actions.append(action);
    }

    menu->addActions(actions);

    menu->exec(QCursor::pos());

    delete menu;
}

void PredefinedSetsEditWidget::tableViewContextMenu(const QPoint &pos)
{
    Q_UNUSED(pos);
    if (!isEditable())
        return;

    QMenu *menu = new QMenu();
    menu->addAction("Remove Selected", this, SLOT(tableViewRemoveSelected()));
    menu->exec(QCursor::pos());
    delete menu;
}

void PredefinedSetsEditWidget::tableViewRemoveSelected()
{
    QModelIndexList list = m_tableView->selectionModel()->selectedRows();
    if (!list.empty())
    {
        for (int i=list.size()-1; i>=0; --i)
        {
            int row = list.at(i).row();
            m_tableView->model()->removeRows(row,1);
        }
    }
}



bool PredefinedSetsEditWidget::isEditable() const
{
    return false;
}

void PredefinedSetsEditWidget::setIsEditable(bool isEditable)
{
    m_isEditable = isEditable;
}


bool PredefinedSetsEditWidget::isTreeDirty()
{
    return false;
}

bool PredefinedSetsEditWidget::isTableDirty()
{
    return false;
}

void PredefinedSetsEditWidget::checkParentState(QTreeWidgetItem *item)
{
    QTreeWidgetItem *parentItem = item->parent();
    if (parentItem)
    {
        int numChildChecked = 0;

        for(int i=0; i<parentItem->childCount();++i)
        {
            if (parentItem->child(i)->checkState(0) == Qt::Checked )
            {
                ++numChildChecked;
            }
        }

        if (numChildChecked == parentItem->childCount())
        {
            parentItem->setCheckState(0, Qt::Checked);
        }
        else if (numChildChecked == 0)
        {
            parentItem->setCheckState(0, Qt::Unchecked);
        }
        else
        {
            parentItem->setCheckState(0, Qt::PartiallyChecked);
        }
        checkParentState(parentItem);
    }
}

void PredefinedSetsEditWidget::checkChildState(QTreeWidgetItem *item)
{
    if (item->checkState(0) == Qt::PartiallyChecked)
        return ;

    for(int i=0; i<item->childCount();++i)
    {
        item->child(i)->setCheckState(0, item->checkState(0));
        checkChildState(item->child(i));
    }
}

QList<QString> PredefinedSetsEditWidget::allSetIds(QTreeWidgetItem *item)
{
    if (item)
    {
        QList<QString> res;
        res.append(item->data(0, Qt::UserRole).toString());
        for(int i=0; i<item->childCount();++i)
        {
            res.append(allSetIds(item->child(i)));
        }
        return res;
    }
    return QList<QString>();
}

void PredefinedSetsEditWidget::loadTreeItems()
{
    m_selectedSets.clear();
    m_treeView->clear();

    m_treeView->setColumnCount(1);
    m_treeView->setHeaderHidden(true);


    QMap<QString, QTreeWidgetItem *> items;
    QMap<QString, QString> parentIDs;

    for (int i = 0; i < m_model_tree->rowCount(); ++i)
    {
        // if marked deleted, skip
        if (m_model_tree->headerData(i,Qt::Vertical).toString() == "!")
        {
            continue;
        };

        QString set_id = m_model_tree->data(m_model_tree->index(i,0)).toString();
        QString parentSet = m_model_tree->data(m_model_tree->index(i,1)).toString();
        QString name = m_model_tree->data(m_model_tree->index(i,2)).toString();
        QString description = m_model_tree->data(m_model_tree->index(i,3)).toString();
        bool selected = m_model_tree->data(m_model_tree->index(i,4)).toBool();

        QTreeWidgetItem *item = new QTreeWidgetItem();
        parentIDs.insert(set_id, parentSet);

        if (selected)
        {
            item->setCheckState(0, Qt::Checked);
            m_selectedSets.insert(set_id);
        }
        else
        {
            item->setCheckState(0, Qt::Unchecked);
        }

        item->setText(0,name);
        item->setToolTip(0,description);
        item->setData(0, Qt::UserRole, set_id);

        item->setData(0, Qt::UserRole+1, i);

        item->setFlags(item->flags() | Qt::ItemIsDropEnabled | Qt::ItemIsDragEnabled);


        if (!m_isEditable)
        {
            item->setFlags(item->flags() ^ Qt::ItemIsUserCheckable);
        }

        items.insert(set_id, item);

    }

    QList<QTreeWidgetItem*> topItems;
    {
        QMapIterator<QString, QTreeWidgetItem*> it(items);
        while(it.hasNext())
        {
            it.next();
            QString uuid = it.key();
            QString parentID = parentIDs[uuid];
            QTreeWidgetItem* item = it.value();
            if (parentID != QUuid().toString())
            {

                QTreeWidgetItem* parentItem = items[parentID];
                parentItem->addChild(item);
            }
            else
            {
                topItems.append(item);
            }
        }
    }


    m_treeView->insertTopLevelItems(0, topItems);

    m_treeView->expandAll();
    m_treeView->resizeColumnToContents(0);

    QTreeWidgetItemIterator it(m_treeView, QTreeWidgetItemIterator::NoChildren);
    while (*it)
    {
        if ((*it)->checkState(0) == Qt::Checked )
        {
            m_selectedSets.insert((*it)->data(0, Qt::UserRole).toString());
        }
        ++it;
    }

}

void PredefinedSetsEditWidget::loaditems()
{
    if (m_model_tree)
    {
        delete m_model_tree;
        m_model_tree = nullptr;
    }
    m_model_tree = new QSqlTableModel(this, m_database->database());
    m_model_tree->setTable("testsuite_set_id");
    m_model_tree->setEditStrategy(QSqlTableModel::OnManualSubmit);
    m_model_tree->select();


    loadTreeItems();

    if (sourceModel)
    {
        delete sourceModel;
        sourceModel = nullptr;
    }
    sourceModel = new SqlRelationalTableModel(this, m_database->database());

    sourceModel->setRelation(2, QSqlRelation("properties_info", "id", "property_name"));


//    sourceModel->setTable("testsuite_default_selection");
    sourceModel->setTable("TestSuiteDefaultSelectionView");
    sourceModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
    sourceModel->select();
    sourceModel->setCheckableColumnIndex(1);

    if (m_model_table)
    {
        delete m_model_table;
        m_model_table = nullptr;
    }
    m_model_table = new ElementSortFilterProxyModel(this);
    m_model_table->setSourceModel(sourceModel);
    m_model_table->setFilterKeyColumn(0);
    m_model_table->setElementFilterMode(ElementSortFilterProxyModel::Subset);


    int p_typeIndex = sourceModel->fieldIndex("property_id");
    int nameIndex = sourceModel->fieldIndex("name");
    int weightIndex = sourceModel->fieldIndex("weight");

    m_model_table->setHeaderData(p_typeIndex, Qt::Horizontal, "Property Type");
    m_model_table->setHeaderData(nameIndex, Qt::Horizontal, "Name");
    m_model_table->setHeaderData(weightIndex, Qt::Horizontal, "Weight");

    setTableItemCheckState();

    m_tableView->setModel(m_model_table);


    m_tableView->hideColumn(0);
//    m_tableView->hideColumn(1);

    m_tableView->setItemDelegateForColumn(1, new ItemDelegate() );
    m_tableView->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Fixed);
    m_tableView->resizeColumnsToContents();
    m_tableView->setColumnWidth(1, 50);


}

