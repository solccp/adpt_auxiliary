#include "SelectionListWidget.h"

#include "dftbrefdatabase.h"

#include <QStringBuilder>
#include <QDebug>
#include <QVBoxLayout>
#include <QPushButton>
#include <QHeaderView>

#include <QSortFilterProxyModel>

SelectionListWidget::SelectionListWidget(QAbstractItemModel* totalmodel, const QString &elem1, const QString &elem2, DFTBRefDatabase *database, QWidget *parent) :
    QWidget(parent)
{
    m_totalModel = totalmodel;
    m_elem1 = elem1;
    m_elem2 = elem2;
    m_database = database;
    setupUI();
}

void SelectionListWidget::reset()
{
    m_loaded = false;
    load_data();
}

void SelectionListWidget::checkStateChanged(QTableWidgetItem *item)
{

    QString UUID = item->data(99).toString();

    int size_before = m_selectedUUIDs.size();

    if(item->checkState() == Qt::Checked)
    {
        m_selectedUUIDs.insert(UUID);
        emit itemSelected(m_elem1, m_elem2, UUID);
    }
    else
    {
        m_selectedUUIDs.remove(UUID);
        emit itemUnslected(m_elem1, m_elem2, UUID);
    }
    int size_after = m_selectedUUIDs.size();

    if (size_before != size_after)
    {
        emit numOfSelectionChanged(m_elem1, m_elem2, size_after);
    }
}

void SelectionListWidget::load_data()
{
    if (m_loaded)
        return ;

//    QStringList noElements;
//    QStringListIterator it(m_availableElements);
//    while(it.hasNext())
//    {
//        QString elem = it.next();
//        if (m_selectedElements.contains(elem))
//        {
//            continue;
//        }
//        noElements.append(elem);
//    }
//    QString sql_full = "select BS.UUID, GEOM.elements, CONCAT(BS.name, '(', CONCAT_WS('/',BS.method, BS.basis), ')') as name, case when exists (select 1 from `electronic_fitting_default_selection` as EFD where EFD.bandstructure_id = BS.UUID )  then 1 else 0 End as existed from `geometry` as GEOM, `bandstructure` as BS  where ( ";

//    QString sql_pair = "GEOM.elements like '[" + m_elem1 + "]'";
//    if (m_elem1 != m_elem2)
//    {
//        sql_pair = "GEOM.elements like '%["
//            + m_elem1 + "]%' AND GEOM.elements like '%[" + m_elem2 + "]%'";
//    }

//    sql_full += sql_pair;
//    sql_full += " AND BS.geom_id = GEOM.UUID AND BS.UUID IN (SELECT a.UUID FROM `bandstructure` as a, `geometry` as b where (a.geom_id = b.UUID AND ( ";

//    QStringListIterator elem_in_noElements(noElements);
//    QStringList sql_elements;
//    while(elem_in_noElements.hasNext())
//    {
//        QString elem = elem_in_noElements.next();
//        sql_elements.append( "b.elements not like '%[" + elem + "]%'");
//    }
//    sql_full += sql_elements.join(" AND ");
//    sql_full += " )))) ORDER BY name;";

//    qDebug() << sql_full;

//    QAbstractTableModel *tableModel = m_database->getTableModelFromSQL(sql_full);
    QSortFilterProxyModel *tableModel = new QSortFilterProxyModel(this);
    tableModel->setSourceModel(m_totalModel);
    tableModel->setFilterKeyColumn(m_totalModel->columnCount()-1);


    QString regex;
    if (m_elem1 == m_elem2 )
    {
       regex = QString("^\\[%1\\]$").arg(m_elem1);
    }
    else
    {
        regex = QString("(.*\\[%1\\].*\\[%2\\].*)|(.*\\[%2\\].*\\[%1\\].*)").arg(m_elem1).arg(m_elem2);
    }

    tableModel->setFilterRegExp(regex);


    QStringList headers;
    headers.append("");
    for(int i=2; i<tableModel->columnCount()-1; ++i)
    {
        headers.append(tableModel->headerData(i,Qt::Horizontal).toString());
    }

    m_listWidget->clear();

    m_listWidget->setColumnCount(headers.size());
    m_listWidget->setHorizontalHeaderLabels(headers);
    m_listWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
    m_listWidget->setRowCount(tableModel->rowCount());

    for(int i=0; i<tableModel->rowCount();++i)
    {
        QString uuid = tableModel->data(tableModel->index(i,0)).toString();

        QTableWidgetItem *item = new QTableWidgetItem();

        item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
        if (tableModel->data(tableModel->index(i,1)).toInt() == 1)
        {
            item->setCheckState(Qt::Checked);
            m_selectedUUIDs.insert(uuid);
        }
        else
        {
            item->setCheckState(Qt::Unchecked);
        }
        item->setData(99,uuid);
        m_uuidIndex.insert(uuid, i);




        m_listWidget->setItem(i, 0, item);


        for(int j=2; j<tableModel->columnCount()-1; ++j)
        {
            item = new QTableWidgetItem();
            item->setText(tableModel->data(tableModel->index(i,j)).toString());
            m_listWidget->setItem(i,j-1, item);
        }

    }



    m_listWidget->resizeColumnsToContents();


    m_listWidget->verticalHeader()->hide();




    connect(m_listWidget, SIGNAL(itemChanged(QTableWidgetItem*)), this, SLOT(checkStateChanged(QTableWidgetItem*)));

    emit numOfSelectionChanged(m_elem1, m_elem2, m_selectedUUIDs.size());

    m_loaded = true;
}

void SelectionListWidget::selectItem(const QString &uuid)
{
    if (m_uuidIndex.contains(uuid))
    {
        int rowIndex = m_uuidIndex[uuid];
        QTableWidgetItem* item = m_listWidget->item(rowIndex, 0);
        if (item)
        {
            if (item->checkState() == Qt::Unchecked)
            {
                item->setCheckState(Qt::Checked);
            }
        }
    }
}

void SelectionListWidget::unselectItem(const QString &uuid)
{
    if (m_uuidIndex.contains(uuid))
    {
        int rowIndex = m_uuidIndex[uuid];
        QTableWidgetItem* item = m_listWidget->item(rowIndex,0);
        if (item)
        {
            if (item->checkState() == Qt::Checked)
            {
                item->setCheckState(Qt::Unchecked);
            }
        }
    }
}

void SelectionListWidget::setupUI()
{
    m_listWidget = new QTableWidget();
    m_listWidget->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
    QVBoxLayout *layout = new QVBoxLayout();
    layout->addWidget(m_listWidget);
    layout->setMargin(0);
    this->setLayout(layout);
    QPushButton* but = new QPushButton(tr("Close"));

    layout->addWidget(but,0,Qt::AlignRight);
    connect(but, SIGNAL(clicked()), this, SLOT(close()));
}
QSet<QString> SelectionListWidget::selectedUUIDs() const
{
    return m_selectedUUIDs;
}

SelectionListWidget::~SelectionListWidget()
{
//    qDebug() << "dtor" << "SelectionListWidget";
}



QSize SelectionListWidget::sizeHint() const
{
    int width = this->m_listWidget->sizeHint().width();
    QSize size;
    size.setHeight(300);
    size.setWidth(width+20);
    return size;
}
