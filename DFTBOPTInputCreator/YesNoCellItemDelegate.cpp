#include "YesNoCellItemDelegate.h"

#include <QComboBox>

YesNoCellItemDelegate::YesNoCellItemDelegate(QObject *parent) :
    QStyledItemDelegate(parent)
{
}

QString YesNoCellItemDelegate::displayText(const QVariant &value, const QLocale &locale) const
{
    bool ok;
    int val = value.toInt(&ok);
    if (!ok)
    {
        return "No";
    }
    if (val == 1)
        return "Yes";
    else
        return "No";
}
