#ifndef IMPLEMENTATIONKITDIALOG_H
#define IMPLEMENTATIONKITDIALOG_H

#include <QDialog>

namespace SKBuilder
{
class ImplementationFactory;
}

QT_BEGIN_NAMESPACE
class QLineEdit;
class QComboBox;
QT_END_NAMESPACE

class ImplementationKitDialog : public QDialog
{
    friend class ImplementationTestingThread;
    Q_OBJECT
public:
    explicit ImplementationKitDialog(QWidget *parent = 0);

signals:

public slots:

private slots:
    void browserExecutableFile();
    void testAll();
    void removeAll();
    void detect();
private:
    void setupUI();
    QString which(const QString& exec);
private:
    SKBuilder::ImplementationFactory* m_oc_tc_factory;
    QLineEdit *m_editOnecent;
    QLineEdit *m_editTwocent;
    QLineEdit *m_editDFTB;
    QComboBox *m_comboElecType;
    QComboBox *m_comboDFTBType;
};

#endif // IMPLEMENTATIONKITDIALOG_H
