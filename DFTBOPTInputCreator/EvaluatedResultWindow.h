#ifndef EVALUATEDRESULTWINDOW_H
#define EVALUATEDRESULTWINDOW_H

#include <QWidget>
#include <QVBoxLayout>
#include <QMap>
#include <QString>
#include <QScrollArea>

namespace DFTBTestSuite
{
    class ReferenceData;
    class EvaluatedData;
    class Statistics;

}

class QwtPlot;

class EvaluatedResultWindow : public QWidget
{
    Q_OBJECT
public:
    explicit EvaluatedResultWindow(QWidget *parent = 0);

signals:

public slots:
private slots:
    void removePushed();
    void showDetails();
    void showBSGraph();
public:
    void addResult(const QString& setname, const QString &fulldetail,
                   const DFTBTestSuite::ReferenceData *ref_data,
                   const DFTBTestSuite::EvaluatedData* calc_data,
                   const DFTBTestSuite::Statistics* statistics);
    QVBoxLayout *m_layout;

    QMap<QString, QWidget*> m_items;
    QMap<QPair<QString, QString>, QwtPlot*> m_bsGraphs;
    void reset();
private:
    void prepareBSGraphData(const QString& setname, const DFTBTestSuite::ReferenceData *ref_data, const DFTBTestSuite::EvaluatedData* calc_data);
};

#endif // EVALUATEDRESULTWINDOW_H
