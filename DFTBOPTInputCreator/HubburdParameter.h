#ifndef HUBBURDPARAMETER_H
#define HUBBURDPARAMETER_H

#include <QString>
#include <QMap>
#include "Singleton.h"

class HubburdParameter
{
    friend class Singleton<HubburdParameter>;
public:
    QMap<QString, double> getHubbardParametersForElement(const QString& element) const;
    bool contains(const QString& element) const;
    double getHubbardParameter(const QString& element, const QString& orbital) const;

private:
    HubburdParameter();
    bool load_data();
    QMap<QString, QMap<QString, double>> m_data;
};

#endif // HUBBURDPARAMETER_H
