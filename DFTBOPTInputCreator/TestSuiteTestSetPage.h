#ifndef TESTSUITETESTSETPAGE_H
#define TESTSUITETESTSETPAGE_H

#include <QWizardPage>

class DFTBRefDatabase;
class PredefinedSetsEditWidget;
class TestSuiteTestSetPage : public QWizardPage
{
    Q_OBJECT
public:
    explicit TestSuiteTestSetPage(DFTBRefDatabase* database, QWidget *parent = 0);

    enum PropertyType
    {
        Geometry = 1,
        Bandstructure = 2,
        Frequency = 3,
        AtomizationEnergy = 4,
        ReactionEnergy = 5
    };

signals:

public slots:
    void setElements(const QStringList& elems);
    QHash<QPair<int, QString>, double> getTestSuiteSet();
private:
    DFTBRefDatabase* m_database;
    PredefinedSetsEditWidget* m_editor;

    // QWizardPage interface
public:
    void initializePage();

};

#endif // TESTSUITETESTSETPAGE_H
