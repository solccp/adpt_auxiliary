#include "FetchingThread.h"

#include "InputCreatorWizard.h"


FetchingThread::FetchingThread(InputCreatorWizard *wizard, QObject *parent) :
    QThread(parent), m_wizard(wizard)
{}

void FetchingThread::setPageId(int id)
{
    m_id = id;
}

QString FetchingThread::message() const
{
    return m_message;
}

void FetchingThread::setMessage(const QString &message)
{
    m_message = message;
}

void FetchingThread::run()
{
    m_wizard->changePage(m_id);
}
