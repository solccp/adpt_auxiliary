#include <QApplication>
#include <QStringList>
#include "dftbrefdatabase.h"

#include <InputCreatorWizard.h>
#include <QSettings>
#include <QMessageBox>

//#include "ImplementationKitDialog.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QCoreApplication::setOrganizationName("qcl.ac.nctu.edu.tw");
    QCoreApplication::setOrganizationDomain("qcl.ac.nctu.edu.tw");
    QCoreApplication::setApplicationName("DFTBOPT");

//#ifdef Q_OS_UNIX

//    QSettings settings;
//    while (!settings.contains("program_path/oc_path") ||
//        !settings.contains("program_path/tc_path") ||
//        !settings.contains("program_path/dftb_path") )
//    {
//        ImplementationKitDialog dia;
//        bool succ = dia.exec();
//        if (!succ)
//        {
//            QMessageBox vbox;
//            vbox.setText("If the programs not set, later you must configure them in your input files,\n"
//                         "the default names are onecent, twocent and dftb+.");
//            vbox.setIcon(QMessageBox::Warning);
//            vbox.setStandardButtons(QMessageBox::Ok);
//            int ret = vbox.exec();
//            if (ret == QMessageBox::Ok )
//            {
//                break;
//            }
//            else
//            {
//                continue;
//            }
//        }
//    }
//#endif

    InputCreatorWizard wizard;
    wizard.resize(1024,768);
    wizard.show();

    return a.exec();
}
