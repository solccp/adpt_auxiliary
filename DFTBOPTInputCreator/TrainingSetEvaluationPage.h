#ifndef TRAININGSETEVALUATIONPAGE_H
#define TRAININGSETEVALUATIONPAGE_H

#include <QWizardPage>
#include <QComboBox>
#include <QHash>
#include <QSet>
#include <QTextEdit>
#include <QScrollArea>


namespace DFTBTestSuite {
    class ReferenceData;
    class InputData;
}
class TrainingSetPage;
class TestSuiteInputFileGenerator;
class DFTBRefDatabase;
class EvaluatedResultWindow;
class TrainingSetEvaluationPage : public QWizardPage
{
    Q_OBJECT
public:
    TrainingSetEvaluationPage(DFTBRefDatabase *database, QWidget *parent = 0);

signals:

public slots:
private slots:
    void calculate();
    void showReadMe();
    void setEvaluateResult(const QString& result);
    void showEvaluateResult();
    void prepareInputs();

    void resultRemoveClicked(const QUrl& link);
    // QWizardPage interface
public:
    void initializePage();
    void checkNeededPairs(TrainingSetPage *trainingSetPage);
private:
    void scanSKFiles();
    void scanAvailablePairs(const QString& name, const QString& path);
    void showEvaluatedResults();


    QComboBox *m_skSetSelector = nullptr;

    DFTBRefDatabase *m_database;
    QPushButton* m_sksetReadme;
    QPushButton* m_evaluateTargets;

    TestSuiteInputFileGenerator *m_generator = nullptr;
    QHash<QString, QSet<QString> > m_available_pairs;
    QSet<QPair<QString, QString> > m_neededSKPairs;


    QHash<QString, QString> m_sksetPath;

    QString m_evaluateResult;

    void appendLog(const QString& msg, QColor color = Qt::black);

    EvaluatedResultWindow* m_evaluatedWindow = nullptr;
    QScrollArea *m_scroll = nullptr;


    // QWizardPage interface
public:
    void cleanupPage();
};

#endif // TRAININGSETEVALUATIONPAGE_H
