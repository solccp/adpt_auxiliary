#include "TrainingSetEvaluationPage.h"

#include "DFTBTestSuite/ReferenceDataType/ReferenceData.h"
#include "DFTBTestSuite/InputFile.h"

#include "TrainingSetEvaluator.h"

#include "EvaluatedResultWindow.h"


#include <QtWidgets>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include "DFTBTestSuite/ReferenceDataType/ReferenceData.h"
#include "testsuiteinputfilegenerator.h"
#include "TrainingSetPage.h"

#include <QCoreApplication>
#include <QDir>
#include <QUrl>
#include <QPlainTextEdit>

#include <QTemporaryDir>
#include <QFontDatabase>

#include <sstream>

#include <QThread>
#include <PrepareInputThread.h>
#include <QJsonDocument>
#include <QJsonObject>


#include <QDebug>

TrainingSetEvaluationPage::TrainingSetEvaluationPage(DFTBRefDatabase *database, QWidget *parent) :
    QWizardPage(parent), m_database(database)
{
    QVBoxLayout *vbox = new QVBoxLayout();
    QHBoxLayout *hbox = new QHBoxLayout();

    this->setSubTitle("The performance of existing sets can be evaluated here. (Optional)");


    m_evaluateTargets = new QPushButton("Evaluate");

    m_sksetReadme = new QPushButton("Citation...");
    connect(m_sksetReadme, SIGNAL(clicked()), this, SLOT(showReadMe()));


//    m_evaluateTargets->setEnabled(false);

    hbox->addWidget(new QLabel(tr("SK set:")));

    m_skSetSelector = new QComboBox;
    m_skSetSelector->setEditable(false);

    hbox->addWidget(m_skSetSelector);

    hbox->addWidget(m_sksetReadme);
    hbox->addWidget(m_evaluateTargets);


    vbox->addLayout(hbox);

    this->setTitle("Evaluating Existing SK Parameter Performance");

//    vbox->addWidget(m_logWindow);

    this->setLayout(vbox);

    connect(m_evaluateTargets, SIGNAL(clicked()), this, SLOT(calculate()));

    m_scroll = new QScrollArea;
    m_evaluatedWindow = new EvaluatedResultWindow();
    m_scroll->setWidget(m_evaluatedWindow);
    m_scroll->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    m_scroll->setWidgetResizable(true);
    vbox->addWidget(m_scroll);



}


void TrainingSetEvaluationPage::initializePage()
{
    qDebug() << "init";
    scanSKFiles();
    if (m_generator)
    {
        delete m_generator;
        m_generator = nullptr;
    }
    m_generator = new TestSuiteInputFileGenerator(m_database);
    m_evaluatedWindow->reset();
//    m_logWindow->clear();
}

void TrainingSetEvaluationPage::scanSKFiles()
{
    QString skfiles_path = QDir(QCoreApplication::applicationDirPath()).absoluteFilePath("skfiles");
    QDir skfiles_dir(skfiles_path);

    if (skfiles_dir.exists() && skfiles_dir.isReadable())
    {
        m_available_pairs.clear();
        m_sksetPath.clear();
        m_skSetSelector->clear();
        QFileInfoList skdirs = skfiles_dir.entryInfoList(QDir::AllDirs | QDir::NoDotAndDotDot);
        for(int i=0; i<skdirs.size(); ++i)
        {
            QString setName = skdirs[i].completeBaseName();
            m_skSetSelector->addItem(setName);
            m_sksetPath[setName] = skdirs[i].absoluteFilePath();
            scanAvailablePairs(setName, skdirs[i].absoluteFilePath());
        }

    }

}

void TrainingSetEvaluationPage::scanAvailablePairs(const QString &name, const QString &path)
{
    QDir skdir(path);
    QFileInfoList list = skdir.entryInfoList(QStringList() << "*-*.skf", QDir::Files | QDir::Readable);
    QSet<QString> pairs;

    for(int i=0; i<list.size(); ++i)
    {
        pairs.insert(list[i].baseName());
    }
    m_available_pairs[name] = pairs;
}

void copyPath(const QString& src, const QString& dst)
{
    QDir dir(src);
    if (! dir.exists())
        return;

    foreach (QString d, dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot)) {
        QString dst_path = dst + QDir::separator() + d;
        dir.mkpath(dst_path);
        copyPath(src+ QDir::separator() + d, dst_path);
    }

    foreach (QString f, dir.entryList(QDir::Files)) {
        QFile newfile(dst + QDir::separator() + f);
        if (newfile.exists())
        {
            newfile.remove();
        }

        QFile::copy(src + QDir::separator() + f, dst + QDir::separator() + f);
    }
}

void TrainingSetEvaluationPage::calculate()
{

    QProgressDialog pd;
    pd.setMinimum(0);
    pd.setMaximum(0);
    pd.setModal(true);


    QString skset = m_skSetSelector->currentText();
    QSetIterator<QPair<QString, QString> > it(m_neededSKPairs);

    bool setOK = true;
    while(it.hasNext())
    {
        auto& value = it.next();
        QString pair = QString("%1-%2").arg(value.first).arg(value.second);
        if (!m_available_pairs[skset].contains(pair))
        {
            setOK = false;
        }
    }

    if (!setOK)
    {
        return;
    }

    //calculate
    pd.setMinimumDuration(0);
    pd.show();
    pd.setLabelText(QString("Preparing input files..."));


    //write the testsuite files
    DFTBTestSuite::ReferenceData *ref_data = new DFTBTestSuite::ReferenceData;
    DFTBTestSuite::InputData *input_data = new DFTBTestSuite::InputData;

    pd.setCancelButtonText(QString());

    PrepareInputThread *mythread = new PrepareInputThread(m_generator, ref_data, input_data);
    connect(mythread, SIGNAL(finished()), &pd, SLOT(close()));
    connect(mythread, SIGNAL(finished()), mythread, SLOT(deleteLater()));
    mythread->start();
    pd.exec();

    input_data->evaluation.SKInfo.prefix = m_sksetPath[skset] + "/";
    input_data->evaluation.SKInfo.suffix = ".skf";
    input_data->evaluation.SKInfo.type2Names = true;
    input_data->evaluation.SKInfo.separator = "-";
    input_data->evaluation.SKInfo.isValid = true;

    input_data->control.CopyIntermediateFiles = false;
    input_data->evaluation.mole_default_dftbinput.template_energy = "";
    input_data->evaluation.mole_default_dftbinput.template_frequency = "";
    input_data->evaluation.mole_default_dftbinput.template_opt = "";

    input_data->evaluation.crystal_default_dftbinput.template_energy = "";

    if (QFileInfo(QDir(m_sksetPath[skset]).absoluteFilePath("mole_energy.hsd")).exists())
    {
        input_data->evaluation.mole_default_dftbinput.template_energy = QDir(m_sksetPath[skset]).absoluteFilePath("mole_energy.hsd");
    }
    if (QFileInfo(QDir(m_sksetPath[skset]).absoluteFilePath("crystal_energy.hsd")).exists())
    {
        input_data->evaluation.crystal_default_dftbinput.template_energy = QDir(m_sksetPath[skset]).absoluteFilePath("crystal_energy.hsd");
    }

    if (QFileInfo(QDir(m_sksetPath[skset]).absoluteFilePath("SKFileInfo.json")).exists())
    {
        QFile loadFile(QDir(m_sksetPath[skset]).absoluteFilePath("SKFileInfo.json"));

        if (!loadFile.open(QIODevice::ReadOnly)) {
            qWarning("Couldn't open save file.");
        }
        else
        {
            QByteArray saveData = loadFile.readAll();

            QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));
            QJsonObject rootObj = loadDoc.object();
            QVariantMap map = rootObj.toVariantMap();
            if (map.contains("SKFiles"))
            {
                input_data->evaluation.SKInfo.skfiles.clear();
                input_data->evaluation.SKInfo.type2Names = false;

                QVariantMap skfiles = map["SKFiles"].toMap();
                QMapIterator<QString, QVariant> it(skfiles);
                while(it.hasNext())
                {
                    it.next();
                    DFTBTestSuite::SKFileEntry entry;
                    auto name = it.key();
                    auto pair = name.split("-");
                    entry.name.first = pair[0];
                    entry.name.second = pair[1];
                    auto list = it.value().toStringList();
                    for(int i=0; i<list.size(); ++i)
                    {
                        entry.filepaths.append(m_sksetPath[skset] + "/" + list[i]);
                    }
                    input_data->evaluation.SKInfo.skfiles.append(entry);
                }
            }
            if (map.contains("MaxAngularMomentum"))
            {
                QVariantMap momentum = map["MaxAngularMomentum"].toMap();
                QMapIterator<QString, QVariant> it(momentum);

                while(it.hasNext())
                {
                    it.next();
                    auto list = it.value().toStringList();
                    input_data->evaluation.SKInfo.selectedShells.insert(it.key(), list);
                }
            }
        }
    }


    QTemporaryDir tempDir;
    tempDir.setAutoRemove(false);
    input_data->control.ScratchDir = tempDir.path();




    TrainingSetEvaluator *workerThread = new TrainingSetEvaluator(ref_data, input_data, this);
    connect(workerThread, SIGNAL(progressChanged(QString)), &pd, SLOT(setLabelText(QString)));
//    connect(workerThread, SIGNAL(progressChanged(int)), &pd, SLOT(setValue(int)));
    connect(workerThread, SIGNAL(resultReady(QString)), this, SLOT(setEvaluateResult(QString)));
//    connect(workerThread, SIGNAL(finished()), workerThread, SLOT(deleteLater()));
    connect(&pd, SIGNAL(canceled()), workerThread, SLOT(stop()));
    workerThread->start();

    connect(workerThread, SIGNAL(resultReady(QString)), &pd, SLOT(accept()));
    pd.setCancelButtonText("Cancel");
    int ret = pd.exec();
    if (ret)
    {
        m_evaluatedWindow->addResult(skset, m_evaluateResult, ref_data, workerThread->getEvaluatedData(), workerThread->getStatistics());
//        m_evaluatedWindow->show();
    }
    workerThread->deleteLater();
}

void TrainingSetEvaluationPage::showReadMe()
{
    QString readmepath = QDir(m_sksetPath[m_skSetSelector->currentText()]).absoluteFilePath("README");
    QPlainTextEdit* tb = new QPlainTextEdit;
    QFile readmefile(readmepath);
    if (readmefile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        tb->setPlainText(QString(readmefile.readAll()));
        QDialog dia;
        dia.setModal(true);
        dia.resize(800,600);
        QVBoxLayout *vbox=new QVBoxLayout;
        vbox->addWidget(tb);
        dia.setLayout(vbox);
        dia.exec();
        delete tb;
    }

}

void TrainingSetEvaluationPage::setEvaluateResult(const QString &result)
{
    m_evaluateResult = result;
}

void TrainingSetEvaluationPage::showEvaluateResult()
{
    int id = QFontDatabase::addApplicationFont(":/UbuntuMono.ttf");
    QStringList families = QFontDatabase::applicationFontFamilies(id);
    QFont font = QFont(families.first());
    font.setStyleHint (QFont::TypeWriter);
//    font.setPointSize (10);
//    font.setFixedPitch (true);

    QPlainTextEdit* tb = new QPlainTextEdit;
    tb->setFont(font);
    tb->setPlainText(m_evaluateResult);
    QDialog dia;
    dia.setModal(true);
    dia.resize(800,600);
    QVBoxLayout *vbox=new QVBoxLayout;
    vbox->addWidget(tb);
    dia.setLayout(vbox);
    dia.exec();
    delete tb;
}

void TrainingSetEvaluationPage::prepareInputs()
{

}

void TrainingSetEvaluationPage::resultRemoveClicked(const QUrl &link)
{
    qDebug() << link;
}

void TrainingSetEvaluationPage::checkNeededPairs(TrainingSetPage *trainingSetPage)
{


    QList<QPair<QString, double> > geometryTargets = trainingSetPage->geometryTargets();
    for(int i=0; i<geometryTargets.size();++i)
    {
        m_generator->addGeometryTarget(geometryTargets[i].first, geometryTargets[i].second);
    }

    QList<QPair<QString, double> > bandstructureTargets = trainingSetPage->bandstructureTargets();
    for(int i=0; i<bandstructureTargets.size();++i)
    {
        m_generator->addBandStructureTarget(bandstructureTargets[i].first, bandstructureTargets[i].second);
    }

    QList<QPair<QString, double> > frequencyTargets = trainingSetPage->frequencyTargets();
    for(int i=0; i<frequencyTargets.size();++i)
    {
        m_generator->addFrequencyTarget(frequencyTargets[i].first, frequencyTargets[i].second);
    }

    QList<QPair<QString, double> > atomizationEnergyTargets = trainingSetPage->atomizationEnergyTargets();
    for(int i=0; i<atomizationEnergyTargets.size();++i)
    {
        m_generator->addAtomizationEnergyTarget(atomizationEnergyTargets[i].first, atomizationEnergyTargets[i].second);
    }

    QList<QPair<QString, double> > reactionEnergyTargets = trainingSetPage->reactionEnergyTargets();
    for(int i=0; i<reactionEnergyTargets.size();++i)
    {
        m_generator->addReactionEnergyTarget(reactionEnergyTargets[i].first, reactionEnergyTargets[i].second);
    }

    m_neededSKPairs = m_generator->neededSKPairs();

    for(int i=m_skSetSelector->count()-1; i>=0; --i)
    {
        QString setname = m_skSetSelector->itemText(i);
        QSetIterator<QPair<QString, QString> > it(m_neededSKPairs);
        while(it.hasNext())
        {
            auto pair = it.next();
            QString pairstr = QString("%1-%2").arg(pair.first).arg(pair.second);
            if (!m_available_pairs[setname].contains(pairstr))
            {
                m_skSetSelector->removeItem(i);
                break;
            }
        }
    }
    m_skSetSelector->setCurrentIndex(0);


}



void TrainingSetEvaluationPage::cleanupPage()
{
    qDebug() << "clean up";
    m_evaluatedWindow->reset();
}
