#ifndef ELEMENTFILTEREDTABLEMODEL_H
#define ELEMENTFILTEREDTABLEMODEL_H

#include <QAbstractTableModel>
#include <QSqlQueryModel>
#include <QStringList>
#include <QHash>
#include <QSet>

#include <QSortFilterProxyModel>

class DFTBRefDatabase;

class ElementFilteredTableModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    ElementFilteredTableModel(DFTBRefDatabase *database, QObject *parent = 0);
    virtual ~ElementFilteredTableModel();
    QStringList selectedElements() const;
    void setSelectedElements(const QStringList &selectedElements);
    void select();
    int sourceIndex(int row);
signals:
    void selectionChanged(bool selected, const QString& uuid, double weight);
public slots:
    void setFilterText(const QString& filter);
    void setFilterColumnIndex(int column);
    void checkItem(const QString& uuid);
    void updateItem(bool selected, const QString& uuid, double weight);
private:
    DFTBRefDatabase *m_database = nullptr;
    QStringList m_selectedElements;
    QString m_tableName;
    QSortFilterProxyModel *m_model = nullptr;
    QSqlQueryModel *m_sqlModel = nullptr;

    int m_checkableColumnIndex = -1;
    int m_weightColumnIndex = m_checkableColumnIndex + 1;

    QHash<QString, double> m_weights;
    QSet<QString> m_checkedItems;
    QString buildSQLStatement();

    // QAbstractItemModel interface
public:
    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index_, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    bool setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role);
//    bool insertColumns(int column, int count, const QModelIndex &parent);
//    bool removeColumns(int column, int count, const QModelIndex &parent);
    void fetchMore(const QModelIndex &parent);
    bool canFetchMore(const QModelIndex &parent) const;
    QString tableName() const;
    void setTableName(const QString &tableName);
    int checkableColumnIndex() const;
    int weightColumnIndex() const;


    void setCheckableColumnIndex(int checkableColumnIndex);
    QString getItemUUID(const QModelIndex& index) const;
    // QAbstractItemModel interface
public:
    Qt::ItemFlags flags(const QModelIndex &index) const;

    // QAbstractItemModel interface
public:
    bool setData(const QModelIndex &index, const QVariant &value, int role);
    QHash<QString, double> checkedItems() const;

    // QAbstractItemModel interface
public:
    void sort(int column, Qt::SortOrder order);
};

#endif // ELEMENTFILTEREDTABLEMODEL_H
