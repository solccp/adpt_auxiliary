#include "ImplementationTestingThread.h"
#include "ImplementationKitDialog.h"

#include "skbuilder/implementationfactory.h"
#include "skbuilder/sktoolkitbase.h"

#include <QtWidgets>

ImplementationTestingThread::ImplementationTestingThread(ImplementationKitDialog* dia, QObject *parent) :
    QObject(parent), m_dia(dia)
{
}

void ImplementationTestingThread::verify()
{
    bool dummy = true;
    bool all_pass = true;
    QString reason;
    while(dummy)
    {
        dummy = false;


        emit progressText("Testing one-center program");
        emit progress(33);

        m_dia->m_oc_tc_factory->setToolchainName(m_dia->m_comboElecType->currentData().toString());
        std::shared_ptr<SKBuilder::SKToolKit> sktoolkit = m_dia->m_oc_tc_factory->getSKToolKit();

        all_pass = sktoolkit->setOneConterProgram(m_dia->m_editOnecent->text());
        if (!all_pass)
        {
            reason = QString("One-center program %1 is not a proper implementaion for electronic type %2.")
                    .arg(m_dia->m_editOnecent->text()).arg(m_dia->m_comboElecType->currentText());
            all_pass = false;
            break;
        }


        emit progressText("Testing two-center program");
        emit progress(66);

        all_pass = sktoolkit->setTwoConterProgram(m_dia->m_editTwocent->text());
        if (!all_pass)
        {
            reason = QString("Two-center program %1 is not a proper implementaion for electronic type %2.")
                    .arg(m_dia->m_editTwocent->text()).arg(m_dia->m_comboElecType->currentText());
            all_pass = false;
            break;
        }
    }

    if (!all_pass)
    {
        setErrorMsg(reason);
        emit error();
        return ;
    }
    emit progress(100);
    emit verified();
}
QString ImplementationTestingThread::errorMsg() const
{
    return m_errorMsg;
}

void ImplementationTestingThread::setErrorMsg(const QString &errorMsg)
{
    m_errorMsg = errorMsg;
}

