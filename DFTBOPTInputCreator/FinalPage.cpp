#include "FinalPage.h"
#include <QtWidgets>
#include <QFileInfo>
#include <QDir>

FinalPage::FinalPage(QWidget *parent) :
    QWizardPage(parent)
{
    this->setTitle("Ready for new parameterization!");
    this->setSubTitle("The output archive file will be generated.\nPlease unzip it before running the optimization.");
    setupUI();

}

void FinalPage::appendLog(QString msg, QColor color)
{
    m_logWindow->append("<font color=" + color.name() + ">" + msg + "</font>");
}

void FinalPage::appendLog(int type, QString msg)
{
    QColor color(Qt::black);
    if (type == Exportor::MsgType::MSG_INFO)
    {
        color = Qt::darkGreen;
    }
    else if (type == Exportor::MsgType::MSG_ERROR)
    {
        color = Qt::darkRed;
    }
    else if (type == Exportor::MsgType::MSG_SPECIAL_INFO)
    {
        color = Qt::red;
    }
    else if (type == Exportor::MsgType::MSG_WARNING)
    {
        color = Qt::red;
    }
    m_logWindow->append("<font color=" + color.name() + ">" + msg + "</font>");
}

void FinalPage::SelectOutputPath()
{
    QString filename = QFileDialog::getSaveFileName(this, "Select output archive file path", QDir::homePath(), "ZIP file(*.zip)");

    if (filename.isEmpty())
        return ;

    if (!filename.endsWith(".zip", Qt::CaseInsensitive))
    {
        filename += ".zip";
    }

    m_outputpathEdit->setText(filename);

    QFileInfo fileinfo(m_outputpathEdit->text());

    QString dirpath = fileinfo.absoluteDir().absolutePath();
    QFileInfo dirinfo(dirpath);


    if (!dirinfo.isWritable())
    {
        QMessageBox qbox;
        qbox.setIcon(QMessageBox::Critical);
        qbox.setText(fileinfo.absoluteFilePath() + " cannot be wriiten.");
        qbox.setStandardButtons(QMessageBox::Ok);
        qbox.exec();
        m_outputpathEdit->clear();
        return;
    }

    m_logWindow->clear();
    m_outputpath = fileinfo.absoluteFilePath();
    emit SaveInputFilesTo(m_outputpath);
}

void FinalPage::setupUI()
{
    QVBoxLayout *layout = new QVBoxLayout();
    this->setLayout(layout);

    layout->addWidget(new QLabel(tr("File path:")));

    QPushButton* butSaveFile = new QPushButton(tr("Browse..."));
    connect(butSaveFile, SIGNAL(clicked()), this, SLOT(SelectOutputPath()));
    m_outputpathEdit = new QLineEdit();
    m_outputpathEdit->setReadOnly(true);


    QHBoxLayout *hlayout = new QHBoxLayout();
    hlayout->addWidget(m_outputpathEdit);
    hlayout->addSpacing(10);
    hlayout->addWidget(butSaveFile);
    layout->addLayout(hlayout);



    layout->addSpacing(15);
    layout->addWidget(new QLabel(tr("Log:")));
    m_logWindow = new QTextEdit();
    m_logWindow->setReadOnly(true);
    m_logWindow->setAcceptRichText(true);

    layout->addWidget(m_logWindow);
}


bool FinalPage::validatePage()
{
    return true;
}


bool FinalPage::isComplete() const
{
    return true;
}


void FinalPage::initializePage()
{

}
