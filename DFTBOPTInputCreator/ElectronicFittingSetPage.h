#ifndef ElectronicFittingSetPage_H
#define ElectronicFittingSetPage_H

#include <QWizardPage>
#include <QTableWidget>
#include <QListWidget>

class DFTBRefDatabase;
class SelectionListWidget;
class ElectronicFittingSetPage : public QWizardPage
{
    Q_OBJECT
public:
    explicit ElectronicFittingSetPage(DFTBRefDatabase* database, QWidget *parent = 0);

signals:

private slots:
    void show_details();
    void itemSelectionNumberChanged(const QString& elem1, const QString& elem2, int num);
    void itemSelected(const QString& elem1, const QString& elem2, const QString& uuid);
    void itemUnselected(const QString& elem1, const QString& elem2, const QString& uuid);
public slots:
    QStringList getSelectedElements() const;
    void setSelectedElements(const QStringList &value);
    void reset();
private:

    void load();
    QTableWidget* m_table;
    bool m_hasLoaded = false;

    DFTBRefDatabase* m_database;
    QStringList SelectedElements;

    QMap<QPair<int, int>, SelectionListWidget*> m_listwidgets;

    QSet<QString> m_fittingSetSelection;

    // QWizardPage interface
public:
    void initializePage();


    // QWizardPage interface
public:
    bool validatePage();
    QSet<QString> fittingSetSelection() const;
};

#endif // DETAILSPAGE_H
