#ifndef MISCELLANEOUSOPTIONPAGE_H
#define MISCELLANEOUSOPTIONPAGE_H

#include <QWizardPage>
#include <QtWidgets>

class MiscellaneousOptionPage : public QWizardPage
{
    friend class Exportor;
    Q_OBJECT
public:
    explicit MiscellaneousOptionPage(QWidget *parent = 0);
    enum DFTBFormulism
    {
        DFTB=1,
        SCCDFTB=2,
        DFTB3=3
    };

    int getDFTBFormulism() const;
    bool isUseDispersion() const;
    bool useOrbitalResolvedSCC() const ;
    bool fitAllAtomEnergy() const;
signals:

public slots:
    void setOrbitalSolvedSCC(bool on);
    void setFitAllAtomEnergy(bool on);
private slots:
    void dftbfomulismchanged();
private:
    QRadioButton *m_DFTB = nullptr;
    QRadioButton *m_SCCDFTB = nullptr;
    QRadioButton *m_DFTB3 = nullptr;
    QComboBox *toolchain = nullptr;
    QCheckBox *OrbitalSolvedSCC = nullptr;


    int m_DFTBFormulism = SCCDFTB;
    bool m_OrbitalSolvedSCC = false;
    bool m_fitAllAtomEnergy = false;
    bool m_dispersion = false;

};

#endif // MISCELLANEOUSOPTIONPAGE_H


