#include "GeneratingSetPage.h"
#include "dftbrefdatabase.h"
#include "ElementFilteredTableModel.h"

#include "geometry.h"
#include "clustergeometryeditor.h"
#include "bandstructuredata.h"
#include "bandstructuredataeditor.h"
#include "molecularfrequencydata.h"
#include "molecularfrequencyeditor.h"
#include "reactionenergydata.h"
#include "reactionenergydataeditor.h"

#include "ErepfitAdditionalEquationWidget.h"


#include "PropertyTableView.h"
#include <QVBoxLayout>
#include <QHeaderView>
#include <QtWidgets>
#include <QSqlTableModel>

#include <QDebug>

ErepfitSetPage::ErepfitSetPage(ErepfitParameter *profile, DFTBRefDatabase *database, QWidget *parent) :
    QWidget(parent), m_database(database)
{
//    setTitle("Generating Set");
//    setSubTitle("Select interested properties into the generating set.");

    layout =  new QVBoxLayout(this);
    m_tabWidget = new QTabWidget();

    m_profile = profile;

    m_tableEnergy = new PropertyTableView(m_database);
    m_tabWidget->addTab(m_tableEnergy, "Energy Equations");
//    connect(m_tableEnergy, SIGNAL(itemDblClicked(QString)), this, SLOT(showAtomizationEnergyDetail(QString)));

    m_tableForce = new PropertyTableView(m_database);
    m_tabWidget->addTab(m_tableForce, "Force Equations");
//    connect(m_tableForce, SIGNAL(itemDblClicked(QString)), this, SLOT(showGeometryDetail(QString)));

    m_tabAdditionalEquations = new ErepfitAdditionalEquationWidget(m_profile, m_database);
//    m_tabWidget->addTab(m_tabAdditionalEquations, "Additional Equations");


    m_tableReactionEnergy = new PropertyTableView(m_database);
    m_tabWidget->addTab(m_tableReactionEnergy, "ReactionEnergy Equations");
//    connect(m_tableReactionEnergy, SIGNAL(itemDblClicked(QString)), this, SLOT(showReactionEnergyDetail(QString)));


    layout->setMargin(0);
    layout->addWidget(m_tabWidget);
    this->setLayout(layout);

    m_tableEnergy->setTableName("EnergyEquationList");
    m_tableEnergy->setCheckableColumnIndex(0);

    m_tableForce->setTableName("MoleculeGeometryListView");
    m_tableForce->setCheckableColumnIndex(0);

    m_tableReactionEnergy->setTableName("ReactionEnergyListView");
    m_tableReactionEnergy->setCheckableColumnIndex(0);


    connect(m_tableEnergy, SIGNAL(itemSelectionChanged(bool,QString,double)),
            this, SLOT(propertySelectionChanged(bool,QString,double)));
    connect(m_tableForce, SIGNAL(itemSelectionChanged(bool,QString,double)),
            this, SLOT(propertySelectionChanged(bool,QString,double)));
    connect(m_tableReactionEnergy, SIGNAL(itemSelectionChanged(bool,QString,double)),
            this, SLOT(propertySelectionChanged(bool,QString,double)));


}


void ErepfitSetPage::propertySelectionChanged(bool selected, const QString &uuid, double weight)
{
    PropertyTableView* table = qobject_cast<PropertyTableView*>(sender());
    if (table == this->m_tableEnergy)
    {
        emit selectionChanged("Energy", selected, uuid, weight);
    }
    else if (table == this->m_tableForce)
    {
        emit selectionChanged("Force", selected, uuid, weight);
    }
    else if (table == this->m_tableReactionEnergy)
    {
        emit selectionChanged("ReactionEnergy", selected, uuid, weight);
    }
}

ErepfitSetPage::~ErepfitSetPage()
{


}
QList<QPair<QString, double> > ErepfitSetPage::forceEquations() const
{
    QList<QPair<QString, double> > res;
    QHashIterator<QString, double> it(m_tableForce->model()->checkedItems());
    while(it.hasNext())
    {
        it.next();
        res.append(qMakePair(it.key(), it.value()));
    }
    return res;
}

QList<QPair<QString, double> > ErepfitSetPage::energyEquations() const
{
    QList<QPair<QString, double> > res;
    QHashIterator<QString, double> it(m_tableEnergy->model()->checkedItems());
    while(it.hasNext())
    {
        it.next();
        res.append(qMakePair(it.key(), it.value()));
    }
    return res;
}


QList<QPair<QString, double> > ErepfitSetPage::reactionEnergyEquations() const
{
    QList<QPair<QString, double> > res;
    QHashIterator<QString, double> it(m_tableReactionEnergy->model()->checkedItems());
    while(it.hasNext())
    {
        it.next();
        res.append(qMakePair(it.key(), it.value()));
    }
    return res;
}

QList<AdditionalEquationSelection> ErepfitSetPage::additionalEquations() const
{
    return m_tabAdditionalEquations->getEquations();
}

void ErepfitSetPage::setSelectedElements(const QStringList &elements)
{
    m_elements = elements;
    //Geometry
    m_tableForce->setElements(m_elements);
    m_tableEnergy->setElements(m_elements);
    m_tableReactionEnergy->setElements(m_elements);

    m_tabAdditionalEquations->setElements(m_elements);
}

QStringList ErepfitSetPage::selectedElements() const
{
    return m_elements;
}

void ErepfitSetPage::setSelectedEnergyEquations(const QStringList &uuids)
{
    for(int i=0; i<uuids.size(); ++i)
    {
        m_tableEnergy->checkItem(uuids[i]);
    }
}

void ErepfitSetPage::setSelectedForceEquations(const QStringList &uuids)
{
    for(int i=0; i<uuids.size(); ++i)
    {
        m_tableForce->checkItem(uuids[i]);
    }
}

void ErepfitSetPage::setSelectedReactionEnergyEquations(const QStringList &uuids)
{
    for(int i=0; i<uuids.size(); ++i)
    {
        m_tableReactionEnergy->checkItem(uuids[i]);
    }
}

void ErepfitSetPage::reload()
{
    qDebug() << "reload";

    m_tableForce->hideColumn(5);
    m_tableForce->hideColumn(8);
    m_tableForce->reload();

    m_tableEnergy->reload();

    m_tableReactionEnergy->hideColumn(6);
    m_tableReactionEnergy->reload();



    qDebug() << "loaded";

}

void ErepfitSetPage::postload()
{
    qDebug() << "postload";
    m_tableForce->postload();
    m_tableEnergy->postload();
    m_tableReactionEnergy->postload();

}

void ErepfitSetPage::updateSelection(int property_type, bool selected, const QString &uuid, double weight)
{
    if (property_type == Property_Geometry)
    {
        m_tableForce->updateItem(selected, uuid, weight);
    }
    else if (property_type == Property_AtomizationEnergy)
    {
        m_tableEnergy->updateItem(selected, uuid, weight);
    }
    else if (property_type == Property_ReactionEnergy)
    {
        m_tableReactionEnergy->updateItem(selected, uuid, weight);
    }
}
void ErepfitSetPage::updateSelection(const QList<PropertySelection *> &properties)
{
    for(int i=0; i<properties.size();++i)
    {
        if (properties[i]->propertyName() == "force")
        {
            auto it = properties[i]->getIterator();
            while(it.hasNext())
            {
                it.next();
                this->m_tableForce->updateItem(true, it.key(), it.value());
            }
        }
        else if (properties[i]->propertyName() == "energy")
        {
            auto it = properties[i]->getIterator();
            while(it.hasNext())
            {
                it.next();
                this->m_tableEnergy->updateItem(true, it.key(), it.value());
            }
        }
        else if (properties[i]->propertyName() == "reactionenergy")
        {
            auto it = properties[i]->getIterator();
            while(it.hasNext())
            {
                it.next();
                this->m_tableReactionEnergy->updateItem(true, it.key(), it.value());
            }
        }
    }
}

void ErepfitSetPage::clear()
{
    m_tableEnergy->selectNoItems();
    m_tableForce->selectNoItems();
    m_tableReactionEnergy->selectNoItems();
}



//void GeneratingSetPage::showGeometryDetail(const QString &uuid)
//{
//    Geometry geom;
//    if (!m_database->getGeometry(uuid, &geom))
//        return;

//    QDialog dia;
//    dia.resize(800,600);
//    QGridLayout *vb =new QGridLayout();

//    bool ReadOnly = true;

//    ClusterGeometryEditor* e = new ClusterGeometryEditor(&geom, ReadOnly);


//    vb->addWidget(e,0,0,1,6);

//    QPushButton *butOK = new QPushButton(tr("OK"));

//    connect(butOK, SIGNAL(clicked()), &dia, SLOT(accept()));


//    if (ReadOnly)
//    {
//        vb->addWidget(butOK, 1,5,1,1);
//    }
//    else
//    {
//        QPushButton *butCancel = new QPushButton(tr("Cancel"));
//        connect(butCancel, SIGNAL(clicked()), &dia, SLOT(reject()));
//        vb->addWidget(butOK, 1,4,1,1);
//        vb->addWidget(butCancel, 1,5,1,1);
//    }

//    dia.setLayout(vb);

//    dia.exec();
//}

//void GeneratingSetPage::showBandstructureDetail(const QString &uuid)
//{
//    BandstructureData mydata;
//    if (!m_database->getBandStructure(uuid, &mydata))
//        return;

//    QDialog dia;
//    dia.resize(800,600);
//    QGridLayout *vb =new QGridLayout();


//    QSqlTableModel *model = new QSqlTableModel(this, m_database->database());
//    model->setTable("CrystalGeometry_IDSTR");
//    model->select();

//    bool ReadOnly = true;

//    BandstructureDataEditor* e = new BandstructureDataEditor(&mydata, model, ReadOnly);


//    vb->addWidget(e,0,0,1,6);

//    QPushButton *butOK = new QPushButton(tr("OK"));
//    connect(butOK, SIGNAL(clicked()), &dia, SLOT(accept()));

//    if (ReadOnly)
//    {
//        vb->addWidget(butOK, 1,5,1,1);
//    }
//    else
//    {
//        QPushButton *butCancel = new QPushButton(tr("Cancel"));
//        connect(butCancel, SIGNAL(clicked()), &dia, SLOT(reject()));
//        vb->addWidget(butOK, 1,4,1,1);
//        vb->addWidget(butCancel, 1,5,1,1);
//    }

//    dia.setLayout(vb);

//    dia.exec();
//}

//void GeneratingSetPage::showFrequencyDetail(const QString &uuid)
//{
//    MolecularFrequencyData data;

//    m_database->getMolecularFrequency(uuid, &data);

//    QDialog dia;
//    dia.resize(800,600);
//    QGridLayout *vb =new QGridLayout();

//    QSqlTableModel *model = new QSqlTableModel(this, m_database->database());
//    model->setTable("MolecularGeometry_IDSTR");
//    model->select();

//    bool ReadOnly = true;

//    MolecularFrequencyDataEditor* e = new MolecularFrequencyDataEditor(&data, model, ReadOnly);
//    vb->addWidget(e,0,0,1,6);

//    QPushButton *butOK = new QPushButton(tr("OK"));
//    connect(butOK, SIGNAL(clicked()), &dia, SLOT(accept()));

//    if (!ReadOnly)
//    {
//        QPushButton *butCancel = new QPushButton(tr("Cancel"));
//        connect(butCancel, SIGNAL(clicked()), &dia, SLOT(reject()));
//        vb->addWidget(butOK, 1,4,1,1);
//        vb->addWidget(butCancel, 1,5,1,1);
//    }
//    else
//    {
//        vb->addWidget(butOK, 1,5,1,1);
//    }

//    dia.setLayout(vb);

//    dia.exec();
//}

//void GeneratingSetPage::showAtomizationEnergyDetail(const QString &uuid)
//{

//}

//void GeneratingSetPage::showReactionEnergyDetail(const QString &uuid)
//{
//    ReactionEnergyData data;

//    m_database->getReactionEnergy(uuid, &data);

//    QDialog dia;
//    dia.resize(800,600);
//    QGridLayout *vb =new QGridLayout();


//    bool ReadOnly = true;

//    ReactionEnergyDataEditor* e = new ReactionEnergyDataEditor(&data, m_database, ReadOnly);
//    vb->addWidget(e,0,0,1,6);

//    QPushButton *butOK = new QPushButton(tr("OK"));

//    connect(butOK, SIGNAL(clicked()), &dia, SLOT(accept()));

//    if (!ReadOnly)
//    {

//        QPushButton *butCancel = new QPushButton(tr("Cancel"));
//        connect(butCancel, SIGNAL(clicked()), &dia, SLOT(reject()));

//        vb->addWidget(butOK, 1,4,1,1);
//        vb->addWidget(butCancel, 1,5,1,1);
//    }
//    else
//    {
//        vb->addWidget(butOK, 1,5,1,1);
//    }
//    dia.setLayout(vb);

//    dia.exec();
//}


bool ErepfitSetPage::validatePage()
{
    int totalTargets = 0;
    totalTargets += m_tableForce->model()->checkedItems().size();
    totalTargets += m_tableEnergy->model()->checkedItems().size();
    totalTargets += m_tableReactionEnergy->model()->checkedItems().size();
    return (totalTargets > 0);
}
