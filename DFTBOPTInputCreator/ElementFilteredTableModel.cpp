#include "ElementFilteredTableModel.h"
#include "atomicproperties.h"
#include <QDebug>

#include "dftbrefdatabase.h"

ElementFilteredTableModel::ElementFilteredTableModel(DFTBRefDatabase *database, QObject *parent) :
    QAbstractTableModel(parent), m_database(database)
{
    m_sqlModel = new QSqlQueryModel(this);
    m_model = new QSortFilterProxyModel(this);
}

ElementFilteredTableModel::~ElementFilteredTableModel()
{

}
QStringList ElementFilteredTableModel::selectedElements() const
{
    return m_selectedElements;
}

void ElementFilteredTableModel::setSelectedElements(const QStringList &selectedElements)
{
    m_selectedElements = selectedElements;
}

void ElementFilteredTableModel::select()
{
    m_checkedItems.clear();
    m_weights.clear();

    m_sqlModel->clear();
    m_model->clear();
    m_model->setFilterCaseSensitivity(Qt::CaseInsensitive);


    m_model->setSortRole(Qt::EditRole);
    m_model->setSortCaseSensitivity(Qt::CaseInsensitive);
    m_model->setSourceModel(m_sqlModel);
    QString sql = buildSQLStatement();
    m_sqlModel->setQuery(sql, m_database->database());


}

int ElementFilteredTableModel::sourceIndex(int row)
{
    return m_model->mapToSource(m_model->index(row,0)).row();
}

void ElementFilteredTableModel::setFilterText(const QString &filter)
{
    m_model->setFilterWildcard(filter);
    m_model->invalidate();
    emit layoutChanged();
}

void ElementFilteredTableModel::setFilterColumnIndex(int column)
{
    m_model->setFilterKeyColumn(column);
    m_model->invalidate();
    emit layoutChanged();
}

void ElementFilteredTableModel::checkItem(const QString &uuid)
{
    for(int i=0; i<m_model->rowCount();++i)
    {
        QModelIndex index = m_model->index(i,m_checkableColumnIndex);
        if (m_model->data(index, Qt::EditRole).toString() == uuid)
        {
            m_model->setData(index, Qt::Checked, Qt::CheckStateRole);
            m_checkedItems.insert(uuid);
            break;
        }
    }   
}


void ElementFilteredTableModel::updateItem(bool selected, const QString &uuid, double weight)
{
    if (!selected)
    {
        if (!m_checkedItems.contains(uuid))
        {
            return;
        }
    }
    for(int i=0; i<m_model->rowCount();++i)
    {
        QModelIndex index = m_model->index(i,m_checkableColumnIndex);
        if (m_model->data(index, Qt::EditRole).toString() == uuid)
        {
            m_weights[uuid] = weight;
            if (selected)
            {
                m_model->setData(index, Qt::Checked, Qt::CheckStateRole);
                m_checkedItems.insert(uuid);
            }
            else
            {
                m_model->setData(index, Qt::Unchecked, Qt::CheckStateRole);
                m_checkedItems.remove(uuid);
            }

            break;
        }
    }
    emit layoutChanged();
}
QHash<QString, double> ElementFilteredTableModel::checkedItems() const
{
    QHash<QString, double> res;
    QSetIterator<QString> it(m_checkedItems);
    while(it.hasNext())
    {
        QString key = it.next();
        if (m_weights.contains(key))
        {
            res.insert(key, m_weights[key]);
        }
        else
        {
            res.insert(key, 1.0);
        }
    }
    return res;
}

int ElementFilteredTableModel::checkableColumnIndex() const
{
    return m_checkableColumnIndex;
}

int ElementFilteredTableModel::weightColumnIndex() const
{
    return m_weightColumnIndex;
}

void ElementFilteredTableModel::setCheckableColumnIndex(int checkableColumnIndex)
{
    m_checkableColumnIndex = checkableColumnIndex;
    m_weightColumnIndex = m_checkableColumnIndex + 1;
}

QString ElementFilteredTableModel::getItemUUID(const QModelIndex &index) const
{
    if (index.model() == this)
    {
        return m_model->data(m_model->index(index.row(), 0)).toString();
    }
    return QString();
}

QString ElementFilteredTableModel::tableName() const
{
    return m_tableName;
}

void ElementFilteredTableModel::setTableName(const QString &tableName)
{
    m_tableName = tableName;
}


QString ElementFilteredTableModel::buildSQLStatement()
{
    QStringList noElements;

    QVectorIterator<QString> it(ADPT::AtomicProperties::allSymbols());
    while(it.hasNext())
    {
        QString elem = it.next();
        if (m_selectedElements.contains(elem))
        {
            continue;
        }
        noElements.append(elem);
    }
    QString sql_full = QString("select * from %1").arg(m_tableName);

    QStringListIterator elem_in_noElements(noElements);
    QStringList sql_elements;
    while(elem_in_noElements.hasNext())
    {
        QString elem = elem_in_noElements.next();
        sql_elements.append( " elements not like '%[" + elem + "]%'");
    }
    sql_full += " where " + sql_elements.join(" AND ");
//    qDebug() << sql_full;

    return sql_full;
}



int ElementFilteredTableModel::rowCount(const QModelIndex &parent) const
{
    if (m_model)
    {
        return m_model->rowCount(parent);
    }
    return 0;
}

int ElementFilteredTableModel::columnCount(const QModelIndex &parent) const
{
    if (m_model)
    {
        return m_model->columnCount(parent)+1;
    }
    return 0;
}

QVariant ElementFilteredTableModel::data(const QModelIndex &index_, int role) const
{
//    qDebug() << index.model();
    if (m_model)
    {
        if (role == Qt::CheckStateRole && index_.column() == m_checkableColumnIndex)
        {
            QString key = data(index_, Qt::EditRole).toString();
            if (m_checkedItems.contains(key))
            {
                 return Qt::Checked;
            }
            else
            {
                return Qt::Unchecked;
            }
        }
        else if (role == Qt::DisplayRole && index_.column() == m_checkableColumnIndex)
        {
            return QVariant();
        }
        else if (index_.column() == m_weightColumnIndex)
        {
            if (role == Qt::DisplayRole || role == Qt::EditRole)
            {

                QString key = data(index(index_.row(), m_checkableColumnIndex), Qt::EditRole).toString();

                if (m_weights.contains(key))
                {
                    return m_weights[key];
                }
                else
                {
                    return 1.0;
                }
            }
        }
        else
        {
            if (index_.column() > m_weightColumnIndex)
            {
                return m_model->data(m_model->index(index_.row(), index_.column()-1), role);
            }
            else
            {
                return m_model->data(m_model->index(index_.row(), index_.column()), role);
            }
        }
    }
    return QVariant();
}

QVariant ElementFilteredTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (m_model)
    {
        if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        {
            if (section == m_checkableColumnIndex)
            {
                return "";
            }
            if (section == m_weightColumnIndex)
            {
                return "Weight";
            }
            if (section > m_weightColumnIndex)
                return m_model->headerData(section-1, orientation, role);
            else
                return m_model->headerData(section, orientation, role);
        }
        return m_model->headerData(section, orientation, role);
    }
    return QAbstractTableModel::headerData(section, orientation, role);
}

bool ElementFilteredTableModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role)
{
    if (m_model)
    {
        if (section < m_weightColumnIndex)
        {
            return m_model->setHeaderData(section, orientation, value, role);
        }
        else if (section > m_weightColumnIndex)
        {
            return m_model->setHeaderData(section-1, orientation, value, role);
        }
    }
    return QAbstractTableModel::setHeaderData(section, orientation, value, role);
}

//bool ElementFilteredTableModel::insertColumns(int column, int count, const QModelIndex &parent)
//{
//    if (m_model)
//    {
//        return m_model->insertColumns(column, count, parent);
//    }
//    return QAbstractTableModel::insertColumns(column, count, parent);
//}

//bool ElementFilteredTableModel::removeColumns(int column, int count, const QModelIndex &parent)
//{
//    if (m_model)
//    {
//        return m_model->removeColumns(column, count, parent);
//    }
//    return QAbstractTableModel::removeColumns(column, count, parent);
//}

void ElementFilteredTableModel::fetchMore(const QModelIndex &parent)
{
    if (m_model)
    {
        m_model->fetchMore(parent);
    }
}

bool ElementFilteredTableModel::canFetchMore(const QModelIndex &parent) const
{
    if (m_model)
    {
        return m_model->canFetchMore(parent);
    }
    return QAbstractTableModel::canFetchMore(parent);
}


Qt::ItemFlags ElementFilteredTableModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags flags = QAbstractTableModel::flags(index);
    if (index.column() == m_checkableColumnIndex)
    {
        flags |= Qt::ItemIsUserCheckable | Qt::ItemIsEditable | Qt::ItemIsEnabled;
    }
    if (index.column() == m_weightColumnIndex)
    {
        flags |= Qt::ItemIsEditable | Qt::ItemIsEnabled;
    }
    return flags;
}


bool ElementFilteredTableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (role == Qt::CheckStateRole && index.column() == m_checkableColumnIndex)
    {
        QString key = data(index, Qt::EditRole).toString();
        if (value == Qt::Checked)
        {
            if (m_weights.contains(key))
            {
                m_checkedItems.insert(key);
            }
            else
            {
                m_checkedItems.insert(key);
            }
        }
        else
        {
            m_checkedItems.remove(key);
        }

        if (m_weights.contains(key))
            emit selectionChanged((value == Qt::Checked), key, m_weights[key]);
        else
            emit selectionChanged((value == Qt::Checked), key, 1.0);

//        emit dataChanged(index, index, QVector<int>() << Qt::CheckStateRole);
        return true;
    }

    if (index.column() == m_weightColumnIndex)
    {
        if (role == Qt::EditRole)
        {
            QString key = data(this->index(index.row(), m_checkableColumnIndex), Qt::EditRole).toString();
            m_weights[key] = value.toDouble();            
            if (m_checkedItems.contains(key))
                emit selectionChanged(true, key, m_weights[key]);
            return true;
        }
        return false;
    }
    else if (index.column() < m_weightColumnIndex)
    {
        return QAbstractTableModel::setData(QAbstractTableModel::index(index.row(), index.column(), index.parent()), value, role);
    }
    else
    {
        return QAbstractTableModel::setData(QAbstractTableModel::index(index.row(), index.column()-1, index.parent()), value, role);
    }
}


void ElementFilteredTableModel::sort(int column, Qt::SortOrder order)
{
    if (m_model)
    {
        this->m_model->sort(column, order);
        emit layoutChanged();
    }
}

