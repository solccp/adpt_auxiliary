#include "ErepfitAdditionalEquationWidget.h"

#include "dftbrefdatabase.h"
#include <QTableWidget>
#include <QSqlTableModel>
#include <QtWidgets>



ErepfitAdditionalEquationWidget::ErepfitAdditionalEquationWidget(ErepfitParameter* profile, DFTBRefDatabase *database, QWidget *parent) :
    QWidget(parent)
{
    m_profile = profile;
    m_database = database;
    QVBoxLayout *layout = new QVBoxLayout;
    QHBoxLayout *hbox = new QHBoxLayout;
    this->setLayout(layout);
    m_table = new QTableWidget;
    m_table->setSelectionBehavior(QAbstractItemView::SelectRows);
    layout->addLayout(hbox);
    layout->addWidget(m_table);
    QPushButton* butAdd = new QPushButton("Add");
    QPushButton* butDelete = new QPushButton("Delete");
    connect(butAdd, SIGNAL(clicked()), this, SLOT(addEquation()));
    connect(butDelete, SIGNAL(clicked()), this, SLOT(deleteEquation()));
    hbox->addWidget(butAdd);
    hbox->addWidget(butDelete);
    hbox->addStretch(5);
}

QList<AdditionalEquationSelection> ErepfitAdditionalEquationWidget::getEquations()
{
    QList<AdditionalEquationSelection> res;
    for(int i=0; i<m_table->rowCount();++i)
    {
        QCheckBox *cb = qobject_cast<QCheckBox*>(m_table->cellWidget(i,0));
        cb = m_checkboxes[i];
        if (cb && cb->isChecked())
        {
            AdditionalEquationSelection item;
            item.potential = m_table->item(i,1)->text();
            item.derivative = m_table->item(i,2)->text().toInt();
            item.distance = m_table->item(i,3)->text().toDouble();
            QWidget* widget = m_table->cellWidget(i,4);
            item.isFix = (widget->property("OptFix").toInt() == 1);
            item.min = (widget->property("OptMinValue").toDouble());
            item.max = (widget->property("OptMaxValue").toDouble());
            item.value = (widget->property("FixValue").toDouble());
            res.append(item);
        }
    }
    return res;
}

void ErepfitAdditionalEquationWidget::addRow(bool selected, double max, int deriv, QString pot, double value, double min, double dist)
{
    int index = m_table->rowCount();
    m_table->setRowCount(m_table->rowCount()+1);

    {
        QWidget *pWidget = new QWidget();
        QCheckBox *pCheckBox = new QCheckBox();
        QHBoxLayout *pLayout = new QHBoxLayout(pWidget);
        pLayout->addWidget(pCheckBox);
        pLayout->setAlignment(Qt::AlignCenter);
        pLayout->setContentsMargins(0,0,0,0);
        pWidget->setLayout(pLayout);
        m_table->setCellWidget(index,0, pWidget);
        this->m_checkboxes.append(pCheckBox);
        if (selected)
        {
            pCheckBox->setCheckState(Qt::Checked);
        }
        else
        {
            pCheckBox->setCheckState(Qt::Unchecked);
        }
    }

    {
        QTableWidgetItem *item_pot = new QTableWidgetItem(pot);
        m_table->setItem(index,1,item_pot);
    }

    {
        QTableWidgetItem *item_derivative = new QTableWidgetItem();
        item_derivative->setData(Qt::DisplayRole, deriv);
        m_table->setItem(index,2,item_derivative);
    }

    {
        QTableWidgetItem *item = new QTableWidgetItem();
        item->setData(Qt::DisplayRole, dist);
        m_table->setItem(index,3,item);
    }

    m_table->setCellWidget(index,4, getOptFixValueWidget(value, min, max));



}

void ErepfitAdditionalEquationWidget::reload()
{
    m_table->clearContents();
    QSqlTableModel *m_sqlmodel = new QSqlTableModel(this, m_database->database());
    m_sqlmodel->setTable("erepfit_default_input_addition_eq");
    m_sqlmodel->select();

    m_table->setColumnCount(5);
    m_checkboxes.clear();

    QStringList headers;
    headers << "Use" << "Potential" << "Derivative" << "Distance" << "Optimize/Fixed";

    m_table->setHorizontalHeaderLabels(headers);

    int index = 0;
    for(int i=0; i<m_sqlmodel->rowCount();++i)
    {

        QString pot = m_sqlmodel->data(m_sqlmodel->index(i,0)).toString();
        QStringList pot_array = pot.split("-");
        if (!m_elements.contains(pot_array[0]) || !m_elements.contains(pot_array[1]))
        {
            continue;
        }

        int deriv = m_sqlmodel->data(m_sqlmodel->index(i,2)).toInt();
        double dist = m_sqlmodel->data(m_sqlmodel->index(i,1)).toDouble();
        double value = m_sqlmodel->data(m_sqlmodel->index(i,3)).toDouble();
        double min = m_sqlmodel->data(m_sqlmodel->index(i,4)).toDouble();
        double max = m_sqlmodel->data(m_sqlmodel->index(i,5)).toDouble();

        addRow(true, max, deriv, pot, value, min, dist);

        ++index;
    }
    loadProfile();

    m_table->resizeColumnsToContents();
    m_table->resizeRowsToContents();
}

void ErepfitAdditionalEquationWidget::loadProfile()
{
    int rows = m_table->rowCount();
    for(int i=0; i<rows; ++i)
    {
        QCheckBox *cb = qobject_cast<QCheckBox*>(m_table->cellWidget(i,0));
        if (cb && cb->isChecked())
        {
            cb->setChecked(false);
        }
    }
    for(int i=0; i<m_profile->m_additionalEquations.size();++i)
    {
        const AdditionalEquationSelection& sele = m_profile->m_additionalEquations[i];
        addRow(true, sele.max, sele.derivative, sele.potential, sele.value, sele.min, sele.distance);
    }
}

void ErepfitAdditionalEquationWidget::setElements(const QStringList &elements)
{
    m_elements = elements;
    reload();
}

void ErepfitAdditionalEquationWidget::addEquation()
{
    addRow(false, 0.0, 0, "", 0.0, 0.0, 0.0);
}

void ErepfitAdditionalEquationWidget::deleteEquation()
{
    QModelIndexList list = m_table->selectionModel()->selectedRows();
    for(int i=list.size()-1; i>=0; --i)
    {
        m_table->removeRow(list[i].row());
        m_checkboxes.removeAt(i);
    }
}

QWidget *ErepfitAdditionalEquationWidget::getOptFixValueWidget(double value, double min, double max)
{
    QWidget* widget = new QWidget;
    QHBoxLayout *hbox = new QHBoxLayout();
    widget->setLayout(hbox);
    hbox->setContentsMargins(0,0,0,0);
    QComboBox *combo = new QComboBox;
    combo->addItem("Optimize");
    combo->addItem("Fixed");
    hbox->addWidget(combo);
    QStackedLayout *stack = new QStackedLayout;
    stack->setContentsMargins(0,0,0,0);
    QWidget *rangeWidget = new QWidget;
    {
        QHBoxLayout *hbox = new QHBoxLayout();
        rangeWidget->setLayout(hbox);
        hbox->setContentsMargins(0,0,0,0);
        QLineEdit *editMin = new QLineEdit;
        editMin->setText(QString::number(min));
        editMin->setValidator(new QDoubleValidator);
        QLineEdit *editMax = new QLineEdit;
        editMax->setText(QString::number(max));
        editMax->setValidator(new QDoubleValidator);
        hbox->addWidget(new QLabel(tr("Min:")));
        hbox->addWidget(editMin);
        hbox->addWidget(new QLabel(tr("Max:")));
        hbox->addWidget(editMax);

        widget->setProperty("OptMinValue", min);
        widget->setProperty("OptMaxValue", max);

        connect(editMin, &QLineEdit::textChanged, [=](const QString& value2){
            widget->setProperty("OptMinValue", value2);
        });
        connect(editMax, &QLineEdit::textChanged, [=](const QString& value2){
            widget->setProperty("OptMaxValue", value2);
        });

    }


    QWidget *fixedWidget = new QWidget();
    {
        QHBoxLayout *hbox = new QHBoxLayout();
        fixedWidget->setLayout(hbox);
        hbox->setContentsMargins(0,0,0,0);
        QLineEdit *editFixed = new QLineEdit;
        hbox->addWidget(new QLabel(tr("Value:")));
        hbox->addWidget(editFixed);
        editFixed->setValidator(new QDoubleValidator);
        editFixed->setText(QString::number(value));
        widget->setProperty("FixValue", value);
        connect(editFixed, &QLineEdit::textChanged, [=](const QString& value2){
            widget->setProperty("FixValue", value2);
        });
    }

    hbox->addLayout(stack);

    widget->setProperty("OptFix", 0);

    stack->addWidget(rangeWidget);
    stack->addWidget(fixedWidget);
    connect(combo, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), [=](int index){
        stack->setCurrentIndex(index);
        widget->setProperty("OptFix", index);
    });
    return widget;
}
