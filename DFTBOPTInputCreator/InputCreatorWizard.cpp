#include "InputCreatorWizard.h"

#include "ElementSelectPage.h"
#include "DatabaseSelectPage.h"
#include "TrainingSetPage.h"
//#include "TrainingSetEvaluationPage.h"
#include "GeneratingSetPage.h"

#include "ElectronicFittingSetPage.h"
#include "MiscellaneousOptionPage.h"
#include "FinalPage.h"



#include "TestSuiteTestSetPage.h"

#include <QStringBuilder>
#include <sstream>
#include <QByteArray>
#include <QtConcurrent/QtConcurrent>
#include <QFuture>
#include <QSqlTableModel>
#include "ElementSortFilterProxyModel.h"

//#include "../pso_dftb_parameter/input.h"
//#include "PSODFTBSKOptimizerInputEditor.h"


#include "ImplementationKitDialog.h"

#include <QThread>

#include "Exportor.h"
#define MT



InputCreatorWizard::InputCreatorWizard(QWidget *parent) :
    QWizard(parent)
{
    setOption(IndependentPages, false);
//    setOption(QWizard::HaveCustomButton1, true);
//    setButtonText(QWizard::CustomButton1, tr("Settings..."));
//    setButtonText(QWizard::CustomButton2, tr("Save profile..."));
//    setButtonText(QWizard::CustomButton3, tr("Load profile..."));

    QList<QWizard::WizardButton> layout;
//    layout << QWizard::CustomButton1 << QWizard::CustomButton2 << QWizard::CustomButton3
    layout << QWizard::Stretch
           << QWizard::BackButton    << QWizard::NextButton
           << QWizard::FinishButton  << QWizard::CancelButton;
    setButtonLayout(layout);

//    connect(this, SIGNAL(customButtonClicked(int)), this, SLOT(customButtonClicked(int)));

    m_database = new DFTBRefDatabase(this);


    m_pageElementSelectPage = new ElementSelectPage(this);
    m_pageSelectDatabase = new DatabaseSelectPage(m_database, this);
    m_pageTrainingSet = new TrainingSetPage(m_database, this);
//    m_pageTrainingSetEvaluation = new TrainingSetEvaluationPage(m_database, this);
    m_pageErepfitSet = new ErepfitSetPage(m_profile.getErepfitSelection(), m_database);

    connect(m_pageTrainingSet, SIGNAL(selectionChanged(int,bool,QString,double)),
            m_pageErepfitSet, SLOT(updateSelection(int,bool,QString,double)));

    connect(m_pageTrainingSet, SIGNAL(selectionChanged(QString,bool,QString,double)),
            this, SLOT(trainingSetSelectionChanged(QString,bool,QString,double)));

    connect(m_pageErepfitSet, SIGNAL(selectionChanged(QString,bool,QString,double)),
            this, SLOT(erepfitSelectionChanged(QString,bool,QString,double)));


    connect(m_pageTrainingSet, &TrainingSetPage::showGeneratingSetPage, [this](){this->showGeneratingSet();});
    connect(m_pageTrainingSet, &TrainingSetPage::saveSelection, [this](){this->saveSelection();});
    connect(m_pageTrainingSet, &TrainingSetPage::loadSelection, [this](){this->loadSelection();});


//    m_pageElectronicFittingSet = new ElectronicFittingSetPage(m_database, this);
//    m_pageTestSuiteTestSet = new TestSuiteTestSetPage(m_database, this);
//    m_pageRepulsiveFittingSet = new ErepfitEquationSelectionPage(m_database, this);
    m_pageMiscellaneousOption = new MiscellaneousOptionPage(this);

    m_pageFinal = new FinalPage(this);


    setPage(Page_SelectElement, m_pageElementSelectPage);
    setPage(Page_SelectDatabase, m_pageSelectDatabase);
    setPage(Page_TrainingSet, m_pageTrainingSet);
//    setPage(Page_TrainingSetEvaluation, m_pageTrainingSetEvaluation);


//    setPage(Page_ElectronicFittingSet, m_pageElectronicFittingSet);
//    setPage(Page_RepulsiveFittingSet, m_pageRepulsiveFittingSet);
//    setPage(Page_TestSuiteTestSet, m_pageTestSuiteTestSet);
    setPage(Page_MiscellaneousOptionPage, m_pageMiscellaneousOption);
    setPage(Page_Final, m_pageFinal);



    connect(this, SIGNAL(currentIdChanged(int)), this, SLOT(currentIdChanged(int)));
    connect(m_pageFinal, SIGNAL(SaveInputFilesTo(QString)), this, SLOT(FinalSave(QString)));
}

InputCreatorWizard::~InputCreatorWizard()
{
    delete m_pageErepfitSet;
}

void InputCreatorWizard::changePage(int id)
{
    qDebug() << "InputCreatorWizard::changePage " << id;
}

void InputCreatorWizard::currentIdChanged(int id)
{
    qDebug() << "InputCreatorWizard::currentIdChanged " << id;
    QStringList selectedElements = m_pageElementSelectPage->getSelectedElements();

    if (id == Page_TrainingSet)
    {
        bool mustReload = false;
        if (selectedElements != m_pageTrainingSet->selectedElements())
        {
            m_pageTrainingSet->setSelectedElements(selectedElements);
            m_pageErepfitSet->setSelectedElements(selectedElements);
            mustReload = true;
            m_profile.setElements(selectedElements);
        }
        if (mustReload)
        {
#ifdef MT
            QProgressDialog pd;
            pd.setMinimum(0);
            pd.setMaximum(0);
            pd.setMinimumDuration(0);
            pd.setLabelText("Fetching data...");
            pd.setModal(true);
            pd.setCancelButton(0);                       

            QFutureWatcher<void> watcher;
            {
                QFuture<void> future = QtConcurrent::run(m_pageTrainingSet,&TrainingSetPage::reload);
                watcher.setFuture(future);
                connect(&watcher, SIGNAL(finished()), &pd, SLOT(accept()));
            }
            pd.exec();
            {
                QFuture<void> future = QtConcurrent::run(m_pageErepfitSet,&ErepfitSetPage::reload);
                watcher.setFuture(future);
                connect(&watcher, SIGNAL(finished()), &pd, SLOT(accept()));
            }
            pd.exec();



#else
            m_pageTrainingSet->reload();
            m_pageErepfitSet->reload();
#endif
        }

        m_pageTrainingSet->postload();
    }
//    else if (id == Page_TrainingSetEvaluation)
//    {
//        QProgressDialog pd;
//        pd.setMinimum(0);
//        pd.setMaximum(0);
//        pd.setMinimumDuration(0);
//        pd.setLabelText("Fetching data...");
//        pd.setModal(true);
//        pd.setCancelButton(0);
//        QFuture<void> future = QtConcurrent::run(m_pageTrainingSetEvaluation,&TrainingSetEvaluationPage::checkNeededPairs, m_pageTrainingSet);
//        QFutureWatcher<void> watcher;
//        watcher.setFuture(future);
//        connect(&watcher, SIGNAL(finished()), &pd, SLOT(accept()));
//        pd.exec();

//    }


}

void InputCreatorWizard::showGeneratingSet()
{
    QDialog dia;
    dia.resize(800,600);
    dia.setModal(true);
    dia.setWindowTitle("Generating set settings");
    QVBoxLayout * layout =new QVBoxLayout;
    dia.setLayout(layout);
    layout->addWidget(m_pageErepfitSet);
    m_pageErepfitSet->postload();
    dia.exec();
    m_profile.getErepfitSelection()->m_additionalEquations = m_pageErepfitSet->additionalEquations();
    m_pageErepfitSet->setParent(0);
}

void InputCreatorWizard::FinalSave(const QString &filename)
{
    QProgressDialog pd;
    pd.setMinimum(0);
    pd.setMaximum(0);
    pd.setMinimumDuration(0);
    pd.setLabelText("Creating input files...");
    pd.setModal(true);
    pd.setCancelButton(0);

    QThread* thread = new QThread;
    Exportor* exportor = new Exportor(filename, this);
    exportor->moveToThread(thread);

    connect(exportor, SIGNAL(Message(int,QString)), m_pageFinal, SLOT(appendLog(int,QString)));
    connect(thread, SIGNAL(started()), exportor, SLOT(save()));

    connect(exportor, SIGNAL(finished()), &pd, SLOT(accept()));
    connect(exportor, SIGNAL(finished()), thread, SLOT(quit()));
    connect(exportor, SIGNAL(finished()), exportor, SLOT(deleteLater()));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    thread->start();
    pd.exec();
}






void InputCreatorWizard::customButtonClicked(int button)
{
    if (button == CustomButton1)
    {
//        ImplementationKitDialog dia;
//        dia.exec();
    }
    else if (button == CustomButton2)
    {
        QString filename = QFileDialog::getSaveFileName(this, tr(""), tr(""), tr("*.skp"));
        if (!filename.isEmpty())
        {
            if (!filename.endsWith(".skp"))
            {
                filename.append(".skp");
            }
            QFile file(filename);
            file.open(QIODevice::WriteOnly);
            QDataStream out(&file);
            out.setVersion(QDataStream::Qt_5_0);
            m_profile.saveProfile(out);
            file.close();
        }
    }
    else if (button == CustomButton3)
    {
        QString filename = QFileDialog::getOpenFileName(this, tr(""), tr(""), tr("*.skp"));
        if (!filename.isEmpty())
        {
//            if (!filename.endsWith(".skp"))
//            {
//                filename.append(".skp");
//            }
            QFile file(filename);
            file.open(QIODevice::ReadOnly);
            QDataStream out(&file);
            out.setVersion(QDataStream::Qt_5_0);
            bool succ = m_profile.loadProfile(out);
            file.close();
            if (succ)
            {
                m_pageElementSelectPage->setSelectedElements(m_profile.elements());
                changePage(Page_TrainingSet);
                m_pageTrainingSet->updateSelection(m_profile.getTrainingSetSelection()->getSelection());
            }
        }
    }
}

void InputCreatorWizard::trainingSetSelectionChanged(const QString &propertyName, bool selected, const QString &uuid, double weight)
{
    PropertySelection *property = m_profile.getTrainingSetSelection()->getSelection(propertyName);
    if (selected)
    {
        property->insert(uuid, weight);
    }
    else
    {
        property->remove(uuid);
    }
}

void InputCreatorWizard::erepfitSelectionChanged(const QString &propertyName, bool selected, const QString &uuid, double weight)
{
    PropertySelection *property = m_profile.getErepfitSelection()->getSelection(propertyName);
    if (selected)
    {
        property->insert(uuid, weight);
    }
    else
    {
        property->remove(uuid);
    }
}

void InputCreatorWizard::saveSelection()
{
    QString filter;
    QString filename = QFileDialog::getSaveFileName(
           this,
           tr("Save Selection"),
           QDir::homePath(),
           tr("ADPT selection file (*.adpt);;"), &filter);
    if (!filename.isEmpty())
    {
        QFile file(filename);
        if( !file.open( QIODevice::WriteOnly | QIODevice::Text ) )
        {
            qFatal( ("Could not open the file:" + filename).toLatin1() );
            return;
        }
        QDataStream stream(&file);
        m_profile.saveProfile(stream);
        file.close();

    }

}

void InputCreatorWizard::loadSelection()
{
    QString filter;
    QString filename = QFileDialog::getOpenFileName(
           this,
           tr("Load Selection"),
           QDir::homePath(),
           tr("ADPT selection file (*.adpt);;"), &filter );
    if (!filename.isNull())
    {
        QFile file(filename);
        if( !file.open( QIODevice::ReadOnly | QIODevice::Text ) )
        {
            qFatal( ("Could not open the file:" + filename).toLatin1() );
            return;
        }
        QDataStream stream(&file);
        bool succ = false;

        DFTBOPTProfile profile;
        succ = profile.loadProfile(stream);
        file.close();
        if (!succ)
        {
            QMessageBox qbox(this);
//            qbox.setWindowTitle(tr("DFTBRefDatabaseManager"));
            qbox.setText("Parsing " + filter + " failed. Cancelled.");
            qbox.setStandardButtons(QMessageBox::Ok);
            qbox.setIcon(QMessageBox::Critical);
            qbox.exec();
            return;
        }

        //make ui updates
        m_pageTrainingSet->clear();
        m_pageErepfitSet->clear();
        m_pageTrainingSet->updateSelection(profile.getTrainingSetSelection()->getSelection());
        m_pageErepfitSet->updateSelection(profile.getErepfitSelection()->getSelection());
        m_pageTrainingSet->update();
        m_pageErepfitSet->update();
    }
}

QSize InputCreatorWizard::sizeHint() const
{
    return QSize(800,600);
}

void InputCreatorWizard::initializePage(int id)
{
    QWizard::initializePage(id);
    qDebug() << "InputCreatorWizard::initializePage " << id;
}
