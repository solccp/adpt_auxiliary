#ifndef ADVANCEDOPTIONS_H
#define ADVANCEDOPTIONS_H

#include <QWizardPage>
#include <QtWidgets>

class RepulsivePotentialDefinitionWidget;
class AtomicParamatersWidget;
class AdvancedOptions : public QWizardPage
{
    Q_OBJECT
public:
    explicit AdvancedOptions(QWidget *parent = 0);

signals:

public slots:
private:
    QTabWidget* m_tabs;
    RepulsivePotentialDefinitionWidget* m_repulsivePotential;
    AtomicParamatersWidget* m_atomicParameters;
};

#endif // ADVANCEDOPTIONS_H
