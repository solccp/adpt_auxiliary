#ifndef EREPFITADDITIONALEQUATIONWIDGET_H
#define EREPFITADDITIONALEQUATIONWIDGET_H

#include <QWidget>
#include <QVBoxLayout>
#include <QTableWidget>
#include "Profile.h"

#include <QCheckBox>

class DFTBRefDatabase;
class ErepfitAdditionalEquationWidget : public QWidget
{
    Q_OBJECT
public:
    ErepfitAdditionalEquationWidget(ErepfitParameter* profile, DFTBRefDatabase* database, QWidget *parent = 0);
    QList<AdditionalEquationSelection> getEquations();

    void addRow(bool select, double max, int deriv, QString pot, double value, double min, double dist);
signals:

public slots:
    void reload();
    void setElements(const QStringList& elements);
public slots:
    void addEquation();
    void deleteEquation();
private slots:
    void loadProfile();
private:
    DFTBRefDatabase* m_database;
    QTableWidget* m_table;
    QStringList m_elements;
    ErepfitParameter* m_profile;

    QList<QCheckBox*> m_checkboxes;

    QWidget* getOptFixValueWidget(double value, double min, double max);
};

#endif // EREPFITADDITIONALEQUATIONWIDGET_H
