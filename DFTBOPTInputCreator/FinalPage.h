#ifndef FINALPAGE_H
#define FINALPAGE_H

#include <QWizardPage>
#include <QtWidgets>

#include "Exportor.h"

class FinalPage : public QWizardPage
{
    Q_OBJECT
public:
    explicit FinalPage(QWidget *parent = 0);

signals:
    void SaveInputFilesTo(const QString& filepath);
public slots:
    void appendLog(QString msg, QColor color = Qt::black);
    void appendLog(int, QString msg);

private slots:
    void SelectOutputPath();
private:
    void setupUI();
    QLineEdit *m_outputpathEdit;
    QString m_outputpath;
    QTextEdit *m_logWindow;


    // QWizardPage interface
public:
    bool validatePage();

    // QWizardPage interface
public:
    bool isComplete() const;

    // QWizardPage interface
public:
    void initializePage();
};

#endif // FINALPAGE_H
