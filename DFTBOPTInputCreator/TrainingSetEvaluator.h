#ifndef TRAININGSETEVALUATOR_H
#define TRAININGSETEVALUATOR_H

#include <QThread>

namespace DFTBTestSuite
{
    class ReferenceData;
    class InputData;
    class DFTBTester;
    class Statistics;
    class EvaluatedData;
}

class TrainingSetEvaluator : public QThread
{
    Q_OBJECT
public:
    TrainingSetEvaluator(DFTBTestSuite::ReferenceData* refData, DFTBTestSuite::InputData* inputData,  QObject *parent = 0);

signals:
    void progressChanged(const QString& msg);
    void progressChanged(int percentage);
    void resultReady(const QString& results);
public slots:
    void stop();
private slots:
    void progressChangedSlot(const QString& msg);
    void progressChangedSlot(int percentage);

public:
    const DFTBTestSuite::Statistics *getStatistics() const;
    const DFTBTestSuite::EvaluatedData *getEvaluatedData() const;

    // QThread interface

protected:
    void run();
private:
    DFTBTestSuite::DFTBTester *m_tester = nullptr;
    DFTBTestSuite::ReferenceData* m_refData;
    DFTBTestSuite::InputData* m_inputData;
};

#endif // TRAININGSETEVALUATOR_H
