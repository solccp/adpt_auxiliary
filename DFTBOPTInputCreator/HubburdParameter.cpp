#include "HubburdParameter.h"

#include <QJsonDocument>
#include <QJsonArray>
#include <QVariant>
#include <QFile>
#include <QException>

class ElementNotFoundException : public QException
{
public:
    void raise() const { throw *this; }
    ElementNotFoundException *clone() const { return new ElementNotFoundException(*this); }
};

class OrbitalNotFoundException : public QException
{
public:
    void raise() const { throw *this; }
    OrbitalNotFoundException *clone() const { return new OrbitalNotFoundException(*this); }
};

QMap<QString, double> HubburdParameter::getHubbardParametersForElement(const QString &element) const
{
    if (m_data.contains(element))
    {
        return m_data.value(element);
    }
    throw ElementNotFoundException();
}

bool HubburdParameter::contains(const QString &element) const
{
    return m_data.contains(element);
}

double HubburdParameter::getHubbardParameter(const QString &element, const QString &orbital) const
{
    if (m_data.contains(element))
    {
        if (m_data.value(element).contains(orbital))
        {
            return m_data.value(element).value(orbital);
        }
        else
        {
            throw OrbitalNotFoundException();
        }
    }
    throw ElementNotFoundException();
}

HubburdParameter::HubburdParameter()
{
    load_data();
}

bool HubburdParameter::load_data()
{
    QJsonParseError error;
    QFile file(":/onecent/general/hubbard.json");
    if (!file.open(QIODevice::ReadOnly))
    {
        return false;
    }

    QByteArray array = file.readAll();
    QJsonDocument json_doc = QJsonDocument::fromJson(array, &error);

    if (error.error != QJsonParseError::NoError)
    {
        return false;
    }

    QVariant ala = json_doc.toVariant();
    QMapIterator<QString, QVariant> it(ala.toMap());
    while(it.hasNext())
    {
        it.next();
        QString elem = it.key();
        QMap<QString, double> submap;
        QMapIterator<QString, QVariant> it2(it.value().toMap());
        while(it2.hasNext())
        {
            it2.next();
            QString orb = it2.key();
            double hubbard = it2.value().toDouble();
            submap.insert(orb, hubbard);
        }
        m_data.insert(elem, submap);
    }

    return true;
}
