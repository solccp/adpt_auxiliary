#include "ImplementationKitDialog.h"

#include <QGridLayout>
#include <QPushButton>
#include <QLineEdit>
#include <QLabel>



#include <QComboBox>
#include <QGroupBox>

#include <QVBoxLayout>
#include <QDialogButtonBox>

#include <QSettings>
#include <QFileDialog>
#include <QDir>
#include <QProcessEnvironment>

#include <QProgressDialog>

#include <QMessageBox>

#include <QTimer>
#include <QThread>

#include "ImplementationTestingThread.h"
#include "skbuilder/implementationfactory.h"

#include "Singleton.h"

#include <QDebug>

ImplementationKitDialog::ImplementationKitDialog(QWidget *parent) :
    QDialog(parent)
{
    m_oc_tc_factory = &Singleton<SKBuilder::ImplementationFactory>::Instance();
    setupUI();
    this->resize(800,400);
}

void ImplementationKitDialog::browserExecutableFile()
{
    QPushButton* but = qobject_cast<QPushButton*>(sender());
    if (but)
    {
        QString filename = QFileDialog::getOpenFileName(this, tr("Choose the executable file"), QDir::homePath(),
                                                        tr("*.*"),0, QFileDialog::ReadOnly);
        if (but->property("TARGET").toString() == "OC")
        {
            m_editOnecent->setText(filename);
        }
        else if (but->property("TARGET").toString() == "TC")
        {
            m_editTwocent->setText(filename);
        }
        else if (but->property("TARGET").toString() == "DFTB")
        {
            m_editDFTB->setText(filename);
        }
    }
}

void ImplementationKitDialog::testAll()
{

    if (m_editOnecent->text().isEmpty() ||
        m_editTwocent->text().isEmpty() ||
        m_editDFTB->text().isEmpty())
    {
        QMessageBox qbox;
        qbox.setStandardButtons(QMessageBox::Ok);
        qbox.setIcon(QMessageBox::Critical);
        qbox.setText("All pathes must be filled.");
        qbox.exec();
        return;
    }



    QProgressDialog pd;
    pd.setWindowModality(Qt::WindowModal);
    pd.setAutoClose(false);
    pd.setAutoReset(false);
    pd.setRange(0,100);

    QThread *t = new QThread();
    ImplementationTestingThread *worker = new ImplementationTestingThread(this);
    connect(worker, SIGNAL(verified()), &pd, SLOT(accept()));
    connect(worker, SIGNAL(error()), &pd, SLOT(cancel()));

    connect(t, SIGNAL(started()), worker, SLOT(verify()));
    connect(t, SIGNAL(finished()), t, SLOT(deleteLater()));

    connect(worker, SIGNAL(progressText(QString)), &pd, SLOT(setLabelText(QString)));
    connect(worker, SIGNAL(progress(int)), &pd, SLOT(setValue(int)));

    worker->moveToThread(t);
    t->start();

    int ret = pd.exec();

    if (!ret)
    {
        QString reason = worker->errorMsg();
        if (reason.isEmpty() && pd.wasCanceled())
        {
            disconnect(worker,0,0,0);
            reason = "Cancelled.";
        }

        QMessageBox qbox;
        qbox.setStandardButtons(QMessageBox::Ok);
        qbox.setIcon(QMessageBox::Critical);
        qbox.setText(reason);
        qbox.exec();
        return;
    }


    QSettings settings;
    settings.setValue("program_path/elec_type", m_comboElecType->currentData().toString());
    settings.setValue("program_path/dftb_type", m_comboDFTBType->currentData().toString());
    settings.setValue("program_path/oc_path", m_editOnecent->text());
    settings.setValue("program_path/tc_path", m_editTwocent->text());
    settings.setValue("program_path/dftb_path", m_editDFTB->text());

    QMessageBox qbox;
    qbox.setStandardButtons(QMessageBox::Ok);
    qbox.setIcon(QMessageBox::Information);
    qbox.setText("All programs are verified to work!");
    qbox.exec();

    accept();
}

void ImplementationKitDialog::removeAll()
{
//    QSettings settings;
//    settings.remove("program_path");
    reject();
}

void ImplementationKitDialog::detect()
{
    QString temp_oc = which("onecent");
    QString temp_tc = which("twocent");
    QString temp_dftb = which("dftb+");
    if (!temp_oc.isEmpty())
    {
        m_editOnecent->setText(temp_oc);
    }
    if (!temp_tc.isEmpty())
    {
        m_editTwocent->setText(temp_tc);
    }
    if (!temp_dftb.isEmpty())
    {
        m_editDFTB->setText(temp_dftb);
    }
}

void ImplementationKitDialog::setupUI()
{
    QSettings settings;
    QVBoxLayout *total_layout = new QVBoxLayout();

    QLabel *intro = new QLabel();
    intro->setText("This dialog is shown because the pathes of executable programs which needed for the parameterization have not been set and verified.\n"
                   "Please browse the correct files in order to perform the paramterization.");
    intro->setWordWrap(true);
    total_layout->addWidget(intro);


    QGridLayout* layout = new QGridLayout();


    m_comboElecType = new QComboBox();
    m_comboElecType->addItem("TWN", "twn");
    m_comboElecType->setEditable(false);
    if (settings.contains("program_path/elec_type"))
    {
        int index = m_comboElecType->findData(settings.value("program_path/dftb_type"));
        if (index > -1)
        {
            m_comboElecType->setCurrentIndex(index);
        }
    }


    layout->addWidget(new QLabel(tr("Implementation type:")),0,0,1,1,Qt::AlignRight);
    layout->addWidget(m_comboElecType, 0,1,1,1);

    layout->addWidget(new QLabel(tr("One-center path:")),1,0,1,1,Qt::AlignRight);
    m_editOnecent = new QLineEdit();
    if (settings.contains("program_path/oc_path"))
    {
        m_editOnecent->setText(settings.value("program_path/oc_path").toString());
    }
    layout->addWidget(m_editOnecent,1,1,1,6);


    layout->addWidget(new QLabel(tr("Two-center path:")),2,0,1,1,Qt::AlignRight);
    m_editTwocent = new QLineEdit();
    if (settings.contains("program_path/tc_path"))
    {
        m_editTwocent->setText(settings.value("program_path/tc_path").toString());
    }
    layout->addWidget(m_editTwocent,2,1,1,6);

    QPushButton* button = new QPushButton(tr("Browse..."));
    button->setProperty("TARGET", "OC");
    connect(button, SIGNAL(clicked()), this, SLOT(browserExecutableFile()));
    layout->addWidget(button,1,7,1,1);

    button = new QPushButton(tr("Browse..."));
    button->setProperty("TARGET", "TC");
    connect(button, SIGNAL(clicked()), this, SLOT(browserExecutableFile()));
    layout->addWidget(button,2,7,1,1);

    QGroupBox *box = new QGroupBox(tr("Electronic programs"));
    box->setLayout(layout);

    box->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);

    total_layout->addWidget(box);


    layout = new QGridLayout();


    m_comboDFTBType = new QComboBox();
    m_comboDFTBType->addItem("DFTB+", "dftb+");
    m_comboDFTBType->setEditable(false);
    if (settings.contains("program_path/dftb_type"))
    {
        int index = m_comboDFTBType->findData(settings.value("program_path/dftb_type"));
        if (index > -1)
        {
            m_comboDFTBType->setCurrentIndex(index);
        }
    }

    layout->addWidget(new QLabel(tr("Implementation type:")),0,0,1,1,Qt::AlignRight);
    layout->addWidget(m_comboDFTBType, 0,1,1,1);

    layout->addWidget(new QLabel(tr("DFTB path:")),1,0,1,1,Qt::AlignRight);

    m_editDFTB = new QLineEdit();
    if (settings.contains("program_path/dftb_path"))
    {
        m_editDFTB->setText(settings.value("program_path/dftb_path").toString());
    }
    layout->addWidget(m_editDFTB,1,1,1,6);
    button = new QPushButton(tr("Browse..."));
    button->setProperty("TARGET", "DFTB");
    connect(button, SIGNAL(clicked()), this, SLOT(browserExecutableFile()));
    layout->addWidget(button,1,7,1,1);

    box = new QGroupBox(tr("Evaluation programs"));
    box->setLayout(layout);

    total_layout->addWidget(box);




    QHBoxLayout *hbox = new QHBoxLayout();

    QPushButton* butOK = new QPushButton(tr("OK"));
    QPushButton* butDetect = new QPushButton(tr("Detect..."));
    QPushButton* butCancel = new QPushButton(tr("Cancel"));
//    butOK->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
//    butCancel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

    connect(butOK, SIGNAL(clicked()), this, SLOT(testAll()), Qt::QueuedConnection);
    connect(butCancel, SIGNAL(clicked()), this, SLOT(removeAll()));

    connect(butDetect, SIGNAL(clicked()), this, SLOT(detect()));

    hbox->addWidget(butDetect);
    hbox->addStretch(2);
    hbox->addWidget(butCancel);
    hbox->addWidget(butOK);

    total_layout->addStretch(10);


//    hbox->addWidget(buttonBox);

    total_layout->addLayout(hbox);

    this->setLayout(total_layout);
}

QString ImplementationKitDialog::which(const QString &exec)
{
    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    QStringList pathes = env.value("PATH").split(":") ;

    for(int i=0; i<pathes.size();++i)
    {
        QDir path = QDir(pathes[i]);
        QFileInfo info = QFileInfo(path.absoluteFilePath(exec));
        if (info.exists() && info.isExecutable())
        {
            return info.absoluteFilePath();
        }
    }

    return "";
}
