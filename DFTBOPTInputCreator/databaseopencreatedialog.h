#ifndef DATABASEOPENCREATEDIALOG_H
#define DATABASEOPENCREATEDIALOG_H

#include <QDialog>

namespace Ui {
class DataBaseOpenCreateDialog;
}

class DataBaseOpenCreateDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DataBaseOpenCreateDialog(QWidget *parent = 0);
    ~DataBaseOpenCreateDialog();

    QString getPath() const;

private:
    Ui::DataBaseOpenCreateDialog *ui;


signals:
    void databaseOpened(QString filename);

private slots:
    void on_butOpenDB_clicked();

private:
    QString path;

};

#endif // DATABASEOPENCREATEDIALOG_H
