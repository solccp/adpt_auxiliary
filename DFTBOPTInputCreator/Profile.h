#ifndef PROFILE_H
#define PROFILE_H

#include <QString>
#include <QList>
#include <QHash>
#include <QPair>
#include <QDataStream>
#include <QStringList>

class PropertySelection
{
    friend class TrainingSetParameter;
    friend class ErepfitParameter;
protected:
//    PropertySelection(){}
    explicit PropertySelection(const QString& property_name) :m_property_name(property_name) {}
public:
    virtual ~PropertySelection(){}
    QString propertyName();
    int size() const ;

    double& operator[](const QString& uuid);
    double operator[](const QString& uuid) const;

    void remove(const QString& uuid);
    void insert(const QString& uuid, double weight);
    void clear();

    QHashIterator<QString, double> getIterator() const;
    QList<QPair<QString, double>> getSelectionList() const;

protected:
    QHash<QString, double > m_selection;
    QString m_property_name;

    void save(QDataStream& out) const;
    void load(QDataStream& in);
};

class OrbitalConfiningPotential
{

};

class DensityConfiningPotential
{

};

class DFTBOption
{
public:
    bool m_orbitalResolvedSCC = false;
    int dftbOrder = 3;

};


class TrainingSetParameter
{
public:
    virtual void clear();
    virtual ~TrainingSetParameter();
    QList<PropertySelection*> m_selection;
    const QList<PropertySelection *> getSelection() const;
    QList<PropertySelection *> getSelection() ;
    PropertySelection* getSelection(const QString& propertyName);
    virtual void save(QDataStream& out) const;
    virtual void load(QDataStream& in);
};

class AdditionalEquationSelection
{
public:
    QString potential;
    qint32 derivative;
    double distance;
    bool isFix;
    double value;
    double min;
    double max;
    void save(QDataStream& out) const;
    void load(QDataStream& in);
};

class ErepfitParameter : public TrainingSetParameter
{
public:
    virtual ~ErepfitParameter(){}
    virtual void save(QDataStream& out) const;
    virtual void load(QDataStream& in);
    QList<AdditionalEquationSelection> m_additionalEquations;
};



class DFTBOPTProfile
{
public:

public:
    TrainingSetParameter* getTrainingSetSelection();
    ErepfitParameter* getErepfitSelection();
    ~DFTBOPTProfile();
    bool saveProfile(QDataStream& out) const;
    bool loadProfile(QDataStream& in);
    void clear();
private:
    QStringList m_elements;

    TrainingSetParameter m_trainingSetParameter;
    ErepfitParameter m_erepfitParameter;


public:
    QStringList elements() const;
    void setElements(const QStringList &elements);
};
QDataStream &operator<<(QDataStream &out, const DFTBOPTProfile &profile);
QDataStream &operator>>(QDataStream &in, DFTBOPTProfile &profile);





#endif // PROFILE_H
