#include "PrepareInputThread.h"

#include "DFTBTestSuite/ReferenceDataType/ReferenceData.h"
#include "DFTBTestSuite/InputFile.h"

#include "testsuiteinputfilegenerator.h"

#include <QDebug>

PrepareInputThread::PrepareInputThread(TestSuiteInputFileGenerator *generator, DFTBTestSuite::ReferenceData* _ref_data, DFTBTestSuite::InputData* _input_data, QObject *parent) :
    QThread(parent)
{
    ref_data = _ref_data;
    input_data = _input_data;
    m_generator = generator;
}


void PrepareInputThread::run()
{
    ///fixme
    ///this may not working, maybe json interface?
    *ref_data = m_generator->exportRefData();
    *input_data = m_generator->exportInputData();
}
