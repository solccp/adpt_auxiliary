#ifndef PROPERTYTABLEVIEW_H
#define PROPERTYTABLEVIEW_H

#include <QWidget>
#include <QString>
#include <QSet>

QT_BEGIN_NAMESPACE
class QTableView;
class QComboBox;
class QLineEdit;
class QAbstractItemDelegate;
QT_END_NAMESPACE


class DFTBRefDatabase;
class ElementFilteredTableModel;

class PropertyTableView : public QWidget
{
    Q_OBJECT
public:
    PropertyTableView(DFTBRefDatabase *database,QWidget *parent = 0);
    virtual ~PropertyTableView();
    const ElementFilteredTableModel* model() const;
    void setItemDelegateForColumn(int column, QAbstractItemDelegate * delegate);
signals:
    void itemDblClicked(const QString& UUID);
    void itemSelectionChanged(bool selected, const QString& uuid, double weight);
public slots:
    void reload();
    void postload();

    void hideColumn(int column);
    void unHideColumn(int column = -1);

    void setTableName(const QString &name);
    void setElements(const QStringList& elements);
    void setCheckableColumnIndex(int column);
    void checkItem(const QString& uuid);
    void updateItem(bool selected, const QString& uuid, double weight);

    void selectNoItems();
private slots:
    void filterTextChanged(const QString& text);
    void filterTypeChanged(int column);
    void tableViewItemDoubleClicked(const QModelIndex& index);
    void customContextMenuRequested(const QPoint &pos);
    void selectAllItems();
    void selectItems();

    void modelSelectionChanged(bool selected, const QString& uuid, double weight);
private:
    DFTBRefDatabase *m_database;
    QTableView* m_view;
    ElementFilteredTableModel *m_model;
    QComboBox *m_filterType;
    QLineEdit *m_filterText;
    QSet<int> m_hiddenColumns;
};

#endif // PROPERTYTABLEVIEW_H
