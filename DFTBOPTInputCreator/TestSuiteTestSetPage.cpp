#include "TestSuiteTestSetPage.h"

#include "PredefinedSetsEditWidget.h"
#include <QVBoxLayout>
#include <QDebug>
#include <QResizeEvent>

TestSuiteTestSetPage::TestSuiteTestSetPage(DFTBRefDatabase *database, QWidget *parent) :
    QWizardPage(parent), m_database(database)
{
    QVBoxLayout *layout = new QVBoxLayout();

    setSubTitle("Setup default fitting set for PSO");

    m_editor = new PredefinedSetsEditWidget(m_database, false, this);

    layout->addWidget(m_editor);

    this->setLayout(layout);



}

void TestSuiteTestSetPage::setElements(const QStringList &elems)
{
    m_editor->setElementFilter(elems);
}

QHash<QPair<int, QString>, double > TestSuiteTestSetPage::getTestSuiteSet()
{
    return m_editor->getTestSuiteSet();
}


void TestSuiteTestSetPage::initializePage()
{
    m_editor->loaditems();
}
