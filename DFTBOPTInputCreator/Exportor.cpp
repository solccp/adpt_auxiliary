#include "Exportor.h"
#include "testsuiteinputfilegenerator.h"
#include "Archive.h"
#include "InputCreatorWizard.h"

//pages
#include "ElementSelectPage.h"
#include "DatabaseSelectPage.h"
#include "TrainingSetPage.h"
#include "TrainingSetEvaluationPage.h"
#include "GeneratingSetPage.h"

#include "ElectronicFittingSetPage.h"
#include "MiscellaneousOptionPage.h"
#include "FinalPage.h"
//end pages


//pso
#include <adpt/input.h>
#include <adpt/input_variant.h>
//end pso

#include <QSqlTableModel>

#include "atomicproperties.h"

#include "skbuilder/utils.h"
#include "skbuilder/input_nctu.h"
#include "skbuilder/input_bccms.h"

//DATATYPE
#include "molecularatomizationenergydata.h"
#include "geometry.h"
#include "reactionenergydata.h"

//DFTB+
#include "DFTBPlusInput.h"
#include "HSDNodeSerializer.h"
//end DFTB+

#include <Variant/Variant.h>
#include "DFTBTestSuite/input_variant.h"
#include "skbuilder/input_variant.h"




Exportor::Exportor(const QString& filename, InputCreatorWizard* wizard, QObject *parent) :
    QObject(parent)
{
    m_wizard = wizard;
    m_filename = filename;
}

void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    Q_UNUSED(type);
    Q_UNUSED(context);
    Q_UNUSED(msg);
}


QStringList buildSQLRepulsivePairs(const QStringList& RepulsivePairs)
{
    QStringList str_pairs;
    int nElems = RepulsivePairs.size();
    for(int i=0; i<nElems; ++i)
    {
        str_pairs.append(QString("'%1'").arg(RepulsivePairs[i]));
    }
    return str_pairs;
}
QStringList buildRepulsivePairs(const QStringList& selectedElements)
{
    QStringList str_pairs;
    int nElems = selectedElements.size();
    for(int i=0; i<nElems; ++i)
    {
        for(int j=i; j<nElems; ++j)
        {
            str_pairs.append(QString("%1-%2").arg(selectedElements.at(i))
                         .arg(selectedElements.at(j)));
        }
    }
    return str_pairs;
}


void Exportor::save()
{
    xyzfilenames.clear();

    TestSuiteInputFileGenerator *generator = new TestSuiteInputFileGenerator(m_wizard->m_database);

    QList<QPair<QString, double> > electronic_fittings = m_wizard->m_profile.getTrainingSetSelection()->getSelection("bandstructure")->getSelectionList();

    QStringList selectedElements = m_wizard->m_pageElementSelectPage->getSelectedElements();

    for(int i=0; i<electronic_fittings.size();++i)
    {
        generator->addBandStructureTarget(electronic_fittings.at(i).first, electronic_fittings.at(i).second);
    }

    bool elec_only = true;



    QList<QPair<QString, double> > geometryTargets = m_wizard->m_profile.getTrainingSetSelection()->getSelection("geometry")->getSelectionList();
    for(int i=0; i<geometryTargets.size();++i)
    {
        generator->addGeometryTarget(geometryTargets[i].first, geometryTargets[i].second);
        elec_only = false;
    }

    QList<QPair<QString, double> > frequencyTargets = m_wizard->m_profile.getTrainingSetSelection()->getSelection("frequency")->getSelectionList();
    for(int i=0; i<frequencyTargets.size();++i)
    {
        generator->addFrequencyTarget(frequencyTargets[i].first, frequencyTargets[i].second);
        elec_only = false;
    }

    QList<QPair<QString, double> > atomizationEnergyTargets = m_wizard->m_profile.getTrainingSetSelection()->getSelection("atomizationenergy")->getSelectionList();
    for(int i=0; i<atomizationEnergyTargets.size();++i)
    {
        generator->addAtomizationEnergyTarget(atomizationEnergyTargets[i].first, atomizationEnergyTargets[i].second);
        elec_only = false;
    }

    QList<QPair<QString, double> > reactionEnergyTargets = m_wizard->m_profile.getTrainingSetSelection()->getSelection("reactionenergy")->getSelectionList();
    for(int i=0; i<reactionEnergyTargets.size();++i)
    {
        generator->addReactionEnergyTarget(reactionEnergyTargets[i].first, reactionEnergyTargets[i].second);
        elec_only = false;
    }


    //create the final output zip
    qInstallMessageHandler(myMessageOutput);
    bugless::Archive zipfile(m_filename);
    zipfile.setType(bugless::Archive::ZIP);
    zipfile.open();
    zipfile.removeAll();

    //write the testsuite files
    DFTBTestSuite::ReferenceData ref_data = generator->exportRefData();

    DFTBTestSuite::InputData input_data = generator->exportInputData();




    DFTBPSO::Input::SKOPTInput dftb_pso_input;




    //Onecent Input
//    fetchDefaultOnecentInput(selectedElements, zipfile);
    //DFTBPlus Input



    QList<QString> all_pairs;
    QStringList repulsive_pairs = buildRepulsivePairs(selectedElements);
    QStringList sql_repulsive_pairs = buildSQLRepulsivePairs(repulsive_pairs);


    for(int i=0; i<selectedElements.size();++i)
    {
        for(int j=0; j<selectedElements.size();++j)
        {
            all_pairs.append(QString("%1-%2").arg(selectedElements.at(i)).arg(selectedElements.at(j)));
        }
    }

    SKBuilder::Input::SKBuilderInput skBuilderInput;

    auto toolchain_name = this->m_wizard->m_pageMiscellaneousOption->toolchain->currentText().toLower();
    if (toolchain_name == "bccms")
    {
        skBuilderInput.toolchain_info.name = "bccms";
        auto onecent_default = std::make_shared<SKBuilder::Input::OneCenterParameter_BCCMS>();
        skBuilderInput.toolchain_info.onecent_info.default_parameter = onecent_default;
        auto twocent_default = std::make_shared<SKBuilder::Input::TwoCenterParameter_BCCMS>();
        twocent_default->ngrid1 = 300;
        twocent_default->ngrid2 = 100;
        twocent_default->interval = 0.1;
        skBuilderInput.toolchain_info.twocent_info.default_parameter = twocent_default;
        skBuilderInput.toolchain_info.twocent_info.path = "sktwocnt";
        skBuilderInput.toolchain_info.onecent_info.path = "slateratom";
    }
    else
    {
        skBuilderInput.toolchain_info.name = "nctu";
        auto onecent_default = std::make_shared<SKBuilder::Input::OneCenterParameter_NCTU>();
        skBuilderInput.toolchain_info.onecent_info.default_parameter = onecent_default;
        auto twocent_default = std::make_shared<SKBuilder::Input::TwoCenterParameter_NCTU>();
        twocent_default->ngrid1 = 100;
        twocent_default->ngrid2 = 100;
        twocent_default->interval = 0.1;
        skBuilderInput.toolchain_info.twocent_info.default_parameter = twocent_default;
        skBuilderInput.toolchain_info.twocent_info.path = "twocent"; //settings.value("program_path/oc_path").toString();
        skBuilderInput.toolchain_info.onecent_info.path = "onecent"; //settings.value("program_path/tc_path").toString();
    }


    skBuilderInput.desired_pairs = all_pairs;



    QMap<QString, QVariant> hubb_map = loadDefaultHubbard();
    QMap<QString, QVariant> oe_map = loadDefaultOrbitalEnergy();
    QMap<QString, QVariant> orbs_map = loadDefaultOrbitalDefinition();
    QMap<QString, QVariant> occ_map = loadDefaultOrbitalOccupation();
    buildDefaultAtomicInfoInSKBuilderInput(orbs_map, occ_map, selectedElements, hubb_map, oe_map, skBuilderInput);

    QSettings settings;

    dftb_pso_input.pso.numOfIterations = 50;
    dftb_pso_input.pso.numOfParticles = 40;

    //Erepfit input

    if (!elec_only)
    {
        dftb_pso_input.psoVector.repulsivePart = std::make_shared<DFTBPSO::Input::RepulsivePart>();
        dftb_pso_input.psoVector.repulsivePart->erepfitPSOVector = std::make_shared<DFTBPSO::Input::ErepfitPSOVector>();

        //Base Equation
        fetchErepfitBaseEquation(dftb_pso_input);
        //Reaction Equation
        fetchErepfitReactionEquation(dftb_pso_input);
        //Addition Equations
        fetchErepfitAdditionEquation(sql_repulsive_pairs, dftb_pso_input);
        //Repulsive Potention Definition
        fetchErepfitRepulsiveDefinition(repulsive_pairs, dftb_pso_input);

        dftb_pso_input.psoVector.repulsivePart->erepfitPSOVector->
                erepfit2_input_data.options.toolchain.name = "dftb+";

        dftb_pso_input.psoVector.repulsivePart->erepfitPSOVector->
                erepfit2_input_data.options.toolchain.path = "dftb+";

        dftb_pso_input.psoVector.repulsivePart->erepfitPSOVector->erepfit2_input_data.options.FitAllAtomEnergy = m_wizard->m_pageMiscellaneousOption->fitAllAtomEnergy();

    }

    dftb_pso_input.psoVector.electronicPart = std::make_shared<DFTBPSO::Input::ElectronicPart>();


    buildDefaultConfiningInput(dftb_pso_input, selectedElements, orbs_map);
    createDefaultDFTBPlusInput(selectedElements, dftb_pso_input, input_data, zipfile);



    //output
    {
        libvariant::Variant ref = libvariant::VariantConvertor<SKBuilder::Input::SKBuilderInput>::toVariant(skBuilderInput);

        {
            std::stringbuf final_reference_str;
            libvariant::SerializeJSON(&final_reference_str, ref, true);
            QByteArray array;
            array.append(QString::fromStdString(final_reference_str.str()));
            QIODevice* dev = zipfile.device( QString("skbuilder_input.json") );
            if (dev->open(QIODevice::WriteOnly))
            {
               emit Message(MsgType::MSG_INFO, QString("Info: The skbuilder input file is added: skbuilder_input.json\n"));
               dev->write(array);
            }
            dev->close();
        }
        {
            std::stringbuf final_reference_str;
            libvariant::SerializeYAML(&final_reference_str, ref);
            QByteArray array;
            array.append(QString::fromStdString(final_reference_str.str()));
            QIODevice* dev = zipfile.device( QString("skbuilder_input.yaml") );
            if (dev->open(QIODevice::WriteOnly))
            {
               emit Message(MsgType::MSG_INFO, QString("Info: The skbuilder input file is added: skbuilder_input.yaml\n"));
               dev->write(array);
            }
            dev->close();
        }
    }

    {
        libvariant::Variant ref = libvariant::VariantConvertor<DFTBTestSuite::ReferenceData>::toVariant(ref_data);

        {
            std::stringbuf final_reference_str;
            libvariant::SerializeJSON(&final_reference_str, ref, true);
            QByteArray array;
            array.append(QString::fromStdString(final_reference_str.str()));
            QIODevice* dev = zipfile.device( QString("tester_reference.json") );
            if (dev->open(QIODevice::WriteOnly))
            {
               emit Message(MsgType::MSG_INFO, QString("Info: The reference file of TestSuite is added: tester_reference.json\n"));
               dev->write(array);
            }
            dev->close();

            dftb_pso_input.tester.reference_file = "tester_reference.json";
        }
        {
            std::stringbuf final_reference_str;
            libvariant::SerializeYAML(&final_reference_str, ref);
            QByteArray array;
            array.append(QString::fromStdString(final_reference_str.str()));
            QIODevice* dev = zipfile.device( QString("tester_reference.yaml") );
            if (dev->open(QIODevice::WriteOnly))
            {
               emit Message(MsgType::MSG_INFO, QString("Info: The reference file of TestSuite is added: tester_reference.json\n"));
               dev->write(array);
            }
            dev->close();
            dftb_pso_input.tester.reference_file = "tester_reference.yaml";

        }

    }

    {
        libvariant::Variant input = libvariant::VariantConvertor<DFTBTestSuite::InputData>::toVariant(input_data);

        {
            std::stringbuf final_input_str;
            libvariant::SerializeJSON(&final_input_str, input, true);

            QByteArray array;
            array.append(QString::fromStdString(final_input_str.str()));

            QIODevice* dev = zipfile.device( QString("tester_input.json") );
            if (dev->open(QIODevice::WriteOnly))
            {
               emit Message(MsgType::MSG_INFO, QString("Info: The input file of TestSuite is added: tester_input.json\n"));
               dev->write(array);
            }
            dev->close();
            dftb_pso_input.tester.input_file = "tester_input.json";
        }

        {
            std::stringbuf final_input_str;
            libvariant::SerializeYAML(&final_input_str, input);

            QByteArray array;
            array.append(QString::fromStdString(final_input_str.str()));

            QIODevice* dev = zipfile.device( QString("tester_input.yaml") );
            if (dev->open(QIODevice::WriteOnly))
            {
               emit Message(MsgType::MSG_INFO, QString("Info: The input file of TestSuite is added: tester_input.yaml\n"));
               dev->write(array);
            }
            dev->close();
            dftb_pso_input.tester.input_file = "tester_input.yaml";
        }
    }

    {

        if (dftb_pso_input.psoVector.repulsivePart)
        {
            libvariant::Variant erepfit_input = libvariant::VariantConvertor<Erepfit2::Input::Erepfit2_InputData>::toVariant(dftb_pso_input.psoVector.repulsivePart->erepfitPSOVector->erepfit2_input_data);
            {
                std::stringbuf final_input_str;
                libvariant::SerializeJSON(&final_input_str, erepfit_input, true);

                QByteArray array;
                array.append(QString::fromStdString(final_input_str.str()));

                QIODevice* dev = zipfile.device( QString("erepfit_input.json") );
                if (dev->open(QIODevice::WriteOnly))
                {
                   emit Message(MsgType::MSG_INFO, QString("Info: The input file of Erepfit is added: erepfit_input.json\n"));
                   dev->write(array);
                }
                dev->close();
                dftb_pso_input.psoVector.repulsivePart->erepfitPSOVector->erepfit2_input_file = "erepfit_input.json";

            }
            {
                std::stringbuf final_input_str;
                libvariant::SerializeYAML(&final_input_str, erepfit_input);

                QByteArray array;
                array.append(QString::fromStdString(final_input_str.str()));

                QIODevice* dev = zipfile.device( QString("erepfit_input.yaml") );
                if (dev->open(QIODevice::WriteOnly))
                {
                   emit Message(MsgType::MSG_INFO, QString("Info: The input file of Erepfit is added: erepfit_input.yaml\n"));
                   dev->write(array);
                }
                dev->close();
                dftb_pso_input.psoVector.repulsivePart->erepfitPSOVector->erepfit2_input_file = "erepfit_input.yaml";
            }
        }
    }

    {


        libvariant::Variant input = libvariant::VariantConvertor<DFTBPSO::Input::SKOPTInput>::toVariant(dftb_pso_input);
        {
            input["skbuilder_input_file"] = "skbuilder_input.json";
            input["tester"]["input_file"] = "tester_input.json";
            input["tester"]["reference_file"] = "tester_reference.json";

            input["pso_vector"]["repulsive_part"]["erepfit_psovector"]["erepfit_input_file"] = "erepfit_input.json";
            input.ErasePath("pso_vector/repulsive_part/erepfit_psovector/erepfit_input_data", true);


            std::stringbuf final_input_str;
            libvariant::SerializeJSON(&final_input_str, input, true);

            QByteArray array;
            array.append(QString::fromStdString(final_input_str.str()));

            QIODevice* dev = zipfile.device( QString("input.json") );
            if (dev->open(QIODevice::WriteOnly))
            {
               emit Message(MsgType::MSG_INFO, QString("Info: The input file is added: input.json\n"));
               dev->write(array);
            }
            dev->close();
        }

        {
            input["skbuilder_input_file"] = "skbuilder_input.yaml";
            input["tester"]["input_file"] = "tester_input.yaml";
            input["tester"]["reference_file"] = "tester_reference.yaml";

            input["pso_vector"]["repulsive_part"]["erepfit_psovector"]["erepfit_input_file"] = "erepfit_input.yaml";
            input.ErasePath("pso_vector/repulsive_part/erepfit_psovector/erepfit_input_data", true);

            std::stringbuf final_input_str;
            libvariant::SerializeYAML(&final_input_str, input);

            QByteArray array;
            array.append(QString::fromStdString(final_input_str.str()));

            QIODevice* dev = zipfile.device( QString("input.yaml") );
            if (dev->open(QIODevice::WriteOnly))
            {
               emit Message(MsgType::MSG_INFO, QString("Info: The input file is added: input.yaml\n"));
               dev->write(array);
            }
            dev->close();
        }
    }



    //close
    zipfile.close();

    emit Message(MsgType::MSG_SPECIAL_INFO, QString("All done!!\n"));

    qInstallMessageHandler(0);
    delete generator;

    emit finished();
}

void Exportor::fetchErepfitAdditionEquation(const QStringList &repulsive_pairs, DFTBPSO::Input::SKOPTInput &dftb_pso_input)
{
//    auto list = m_wizard->m_pageErepfitSet->additionalEquations();
//    for(int i=0; i<list.size();++i)
//    {
//        QStringList elems = list[i].potential.split("-");
//        if (!list[i].isFix)
//        {
//            DFTBPSO_Input::ErepfitAdditionalEquation add_eq;
//            add_eq.potentialName = qMakePair(elems.at(0), elems.at(1));
//            add_eq.distance = list[i].distance;
//            add_eq.derivative = list[i].derivative;
//            add_eq.range = qMakePair(list[i].min, list[i].max);
//            dftb_pso_input.psoVector.repulsivePart.erepfitPSOVector.additionalEquations.append(add_eq);
//        }
//        else
//        {
//            Erepfit2Input::AdditionalEquation add_eq;
//            add_eq.derivative = list[i].derivative;
//            add_eq.distance = list[i].distance;
//            add_eq.value = list[i].value;
//            add_eq.weight = 1.0;
//            dftb_pso_input.psoVector.repulsivePart.erepfitPSOVector.erepfit2_input.equations.additionalEquations.insert(list[i].potential, add_eq);
//        }

//    }
}

void Exportor::fetchErepfitRepulsiveDefinition(const QStringList &repulsive_pairs, DFTBPSO::Input::SKOPTInput &dftb_pso_input)
{
    QSqlTableModel *model = new QSqlTableModel(this, m_wizard->m_database->database());
    QString sql_filter = QString("potential IN (%1)").arg(buildSQLRepulsivePairs(repulsive_pairs).join(","));

    model->setTable("erepfit_defualt_potenial_def");
    model->setFilter(sql_filter);
    model->select();

    auto temp_pairs = repulsive_pairs.toSet();
    for(int i=0; i<model->rowCount();++i)
    {
        QString elems_str = model->data(model->index(i,0)).toString();
        QStringList elems = elems_str.split("-");
        int knots = model->data(model->index(i,1)).toInt();
        double range_start = QString::number(model->data(model->index(i,2)).toDouble(),'f',2).toDouble();
        double range_end = QString::number(model->data(model->index(i,3)).toDouble(),'f',2).toDouble();

        DFTBPSO::Input::RepulsivePotentials potential;
        potential.m_PotentialKnots = knots;
        potential.m_PotentialRanges = qMakePair(range_start, range_end);
        potential.m_aggressive_initialization = false;

        QString potential_name = QString("%1-%2").arg(elems.at(0)).arg(elems.at(1));

        dftb_pso_input.psoVector.repulsivePart->erepfitPSOVector->repulsivePotentials.insert(potential_name, potential);

        temp_pairs.remove(elems_str);
    }

    if (!temp_pairs.isEmpty())
    {
        QSetIterator<QString> it(temp_pairs);
        while(it.hasNext())
        {
            QString potential_name = it.next();
            QStringList pot = potential_name.split("-");
            const QPair<QString, QString> & l_pair{pot[0], pot[1]};

            emit Message(MsgType::MSG_WARNING, QString("Warning: The default repulsive definition for pair %1-%2 is missing.\n")
                                   .arg(l_pair.first).arg(l_pair.second));
            int knots = 3;
            double cr1 = ADPT::AtomicProperties::fromSymbol(l_pair.first)->getCovalentRadius();
            double cr2 = ADPT::AtomicProperties::fromSymbol(l_pair.second)->getCovalentRadius();
            double range_start = QString::number(0.7*(cr1+cr2)/0.529177, 'f',2).toDouble();
            double range_end = QString::number(1.5*(cr1+cr2)/0.529177,'f',2).toDouble();

            DFTBPSO::Input::RepulsivePotentials potential;
            potential.m_PotentialKnots = knots;
            potential.m_PotentialRanges = qMakePair(range_start, range_end);
            potential.m_aggressive_initialization = false;



            dftb_pso_input.psoVector.repulsivePart->erepfitPSOVector->repulsivePotentials.insert(potential_name, potential);

        }
    }

    delete model;
}

void Exportor::buildDefaultConfiningInput(DFTBPSO::Input::SKOPTInput &dftb_pso_input,
                                          const QStringList &selectedElements,
                                          const QMap<QString, QVariant> &orbs_map)
{
    int nElems = selectedElements.size();
    for(int i=0; i<nElems;++i)
    {
        DFTBPSO::Input::AtomicDefinition atomic_def;
        atomic_def.element = selectedElements.at(i);

        QListIterator<QVariant> it_shells(orbs_map.value(atomic_def.element).toList());
        while(it_shells.hasNext())
        {
            DFTBPSO::Input::ConfiningAction conf_action_orb;
            DFTBPSO::Input::ConfiningAction conf_action_density;

            {
                DFTBPSO::Input::Confining conf;
                QListIterator<QVariant> it_orbs(it_shells.next().toList());
                while(it_orbs.hasNext())
                {
                    QString orb = it_orbs.next().toString();
                    QRegularExpression reg("\\d+(\\w+)");
                    QRegularExpressionMatch match = reg.match(orb);
                    if (match.hasMatch())
                    {
                        QString type = match.captured(1);
                        conf.orbitalTypes.append(type);
                    }
                }

                auto toolchain = this->m_wizard->m_pageMiscellaneousOption->toolchain->currentText().toLower();
                if (toolchain == "bccms")
                {
                    auto ranges = std::make_shared<DFTBPSO::Input::ConfiningParamaterRange_BCCMS>();

                    ranges->n = qMakePair(1.8,6.0);
                    ranges->r = qMakePair(2.0,10);
                    conf.ranges = std::static_pointer_cast<DFTBPSO::Input::ConfiningParamaterRange>(ranges);
                }
                else
                {
                    auto ranges = std::make_shared<DFTBPSO::Input::ConfiningParamaterRange_NCTU>();

                    ranges->w = qMakePair(0.2,7);
                    ranges->a = qMakePair(3,8);
                    ranges->r = qMakePair(2.0,10);

                    conf.ranges = std::static_pointer_cast<DFTBPSO::Input::ConfiningParamaterRange>(ranges);
                }

                conf_action_orb.take = conf.orbitalTypes;
                conf_action_orb.confinings.append(conf);
            }

            {
                conf_action_density.confinings = conf_action_orb.confinings;
                auto ranges = std::make_shared<DFTBPSO::Input::ConfiningParamaterRange_NCTU>();
                conf_action_density.take = (QStringList() << "potential" << "density");
            }
            atomic_def.confining_actions.append(conf_action_orb);
            atomic_def.confining_actions.append(conf_action_density);
        }

        dftb_pso_input.psoVector.electronicPart->atomicDefinitions.append(atomic_def);
    }
}

void Exportor::buildDefaultAtomicInfoInSKBuilderInput(const QMap<QString, QVariant> &orbs_map,
                                                      const QMap<QString, QVariant> &occ_map,
                                                      const QStringList &selectedElements,
                                                      const QMap<QString, QVariant> &hubb_map,
                                                      const QMap<QString, QVariant> &oe_map,
                                                      SKBuilder::Input::SKBuilderInput& input)
{
    for(int i=0; i<selectedElements.size();++i)
    {
        QString elem = selectedElements.at(i);
        SKBuilder::Input::AtomicInfo atomic_info;

        {
            QMapIterator<QString, QVariant> it(hubb_map.value(elem).toMap());
            while(it.hasNext())
            {
                it.next();
                atomic_info.hubbard.insert(it.key(), it.value().toDouble());
            }
        }

        {
            QMapIterator<QString, QVariant> it(oe_map.value(elem).toMap());
            while(it.hasNext())
            {
                it.next();
                atomic_info.orbitalEnergy.insert(it.key(), it.value().toDouble());
            }
        }
        {
            QMapIterator<QString, QVariant> it(occ_map.value(elem).toMap());
            while(it.hasNext())
            {
                it.next();
                atomic_info.occupation.insert(it.key(), it.value().toDouble());
            }
        }

        {
            QListIterator<QVariant> it_shells(orbs_map.value(elem).toList());
            while(it_shells.hasNext())
            {
                QStringList l_orbs;
                QListIterator<QVariant> it_orbs(it_shells.next().toList());
                while(it_orbs.hasNext())
                {
                    QString orb = it_orbs.next().toString();
                    l_orbs.append(orb);
                }
                atomic_info.orbitals.append(l_orbs);
            }
        }
        input.atomicinfo.insert(elem, atomic_info);
    }
}

void Exportor::fetchDefaultOnecentInput(const QStringList &selectedElements, bugless::Archive &zipfile)
{
    for(int i=0; i<selectedElements.size();++i)
    {
        QFile inputFile(QString(":/onecent/twn/input/%1").arg(selectedElements.at(i)));
        if (inputFile.open(QIODevice::ReadOnly))
        {
            QIODevice* dev = zipfile.device( QString("oc_inputs/%1.input").arg(selectedElements.at(i)) );
            if (dev->open(QIODevice::WriteOnly))
            {
                emit Message(MsgType::MSG_INFO, QString("Info: The default onecent input for element %1 is added.\n").arg(selectedElements.at(i)));
                dev->write(inputFile.readAll());
            }
            dev->close();
        }
        else
        {
            emit Message(MsgType::MSG_WARNING, QString("Warning: The default onecent input for element %1 is not found.\n"
                                   "Please add it into oc_inputs/%1.input manually\n").arg(selectedElements.at(i)));
        }

    }
}

void Exportor::createDefaultDFTBPlusInput(const QStringList &selectedElements,
                                          DFTBPSO::Input::SKOPTInput &dftb_pso_input,
                                          DFTBTestSuite::InputData &input_data, bugless::Archive &zipfile)
{
    for(int i=0; i<selectedElements.size();++i)
    {
        QFile inputFile(QString("://dftbplus/input/atomic/%1.hsd").arg(selectedElements.at(i)));
        if (inputFile.open(QIODevice::ReadOnly))
        {
            QString filename = QString("dftbinp/atom_%1.hsd").arg(selectedElements.at(i));
            QIODevice* dev = zipfile.device( filename );
            if (dev->open(QIODevice::WriteOnly))
            {
                emit Message(MsgType::MSG_INFO, QString("Info: The default DFTB+ input for element %1 is added.\n")
                                       .arg(selectedElements.at(i)));
                dev->write(inputFile.readAll());
            }
            dev->close();
            // TODO

            if (dftb_pso_input.psoVector.repulsivePart && dftb_pso_input.psoVector.repulsivePart->erepfitPSOVector)
            {
                dftb_pso_input.psoVector.repulsivePart->erepfitPSOVector->erepfit2_input_data.atomicEnergy.atomic_dftbinputs.
                    insert(selectedElements.at(i), filename);
            }

            input_data.evaluation.evaluationMolecule.atomicEnergy.atomic_dftbinputs.
                    insert(selectedElements.at(i), filename);
        }
        else
        {
            emit Message(MsgType::MSG_WARNING, QString("Warning: The default DFTB+ input for element %1 is not found.\n"
                                   "Please add it into dftbinp/atom_%1.hsd manually\n").arg(selectedElements.at(i)));
//            if (dftb_pso_input.psoVector.repulsivePart && dftb_pso_input.psoVector.repulsivePart->erepfitPSOVector)
//            {
//                dftb_pso_input.psoVector.repulsivePart->erepfitPSOVector->erepfit2_input.options.FitAllAtomEnergy = true;
//            }
        }
    }

    //DFTB+ input for molecule


    QStringList catagory;
    catagory << "molecule" << "crystal";
    QStringList drivers;
    drivers << "opt" << "freq";

    for(int i=0; i<catagory.size();++i)
    {
        DFTBPlus::DFTBPlusInput dftbplus_input;
        dftbplus_input.ParserOptions.IgnoreUnprocessedNodes = true;

        if (m_wizard->m_pageMiscellaneousOption->getDFTBFormulism() == MiscellaneousOptionPage::SCCDFTB)
        {
            dftbplus_input.Hamiltonian.SCC = true;
            if (m_wizard->m_pageMiscellaneousOption->useOrbitalResolvedSCC())
            {
                dftbplus_input.Hamiltonian.OrbitalResolvedSCC = true;
            }
        }
        else if (m_wizard->m_pageMiscellaneousOption-> getDFTBFormulism() == MiscellaneousOptionPage::DFTB3)
        {
            dftbplus_input.Hamiltonian.SCC = true;
            dftbplus_input.Hamiltonian.ThirdOrderFull = true;
            dftbplus_input.Hamiltonian.DampXH = true;
            dftbplus_input.Hamiltonian.DampXHExponent = 4.0;

            QMap<QString, QVariant> hubb_map = loadDefaultHubbardDerivs();

            for(int j= 0; j<selectedElements.size();++j)
            {
                QString elem = selectedElements.at(j);
                if (hubb_map.contains(elem))
                {
                    QList<QString> orbitalList = hubb_map.value(elem).toMap().keys();
                    QString orbital = orbitalList.first();
                    int l = -1;
                    for(int k=0; k<orbitalList.size(); ++k)
                    {
                        auto pair = getOrbitalQuantumNumbers(orbitalList[k]);
                        if (pair.second > l)
                        {
                            orbital = orbitalList[k];
                            l = pair.second;
                        }
                    }

                    dftbplus_input.Hamiltonian.HubbardDerivs.insert(selectedElements.at(j), hubb_map.value(elem).toMap().value(orbital));
                }
            }
        }

        if (m_wizard->m_pageMiscellaneousOption->isUseDispersion())
        {
            dftbplus_input.Hamiltonian.Dispersion = DFTBPlus::DispersionNode(DFTBPlus::LennardJonesNode());
        }

        if (catagory[i] == "crystal")
        {

        }


        QString outputPath = QString("dftbinp/%1_energy.hsd").arg(catagory.at(i));
        QIODevice* dev = zipfile.device(outputPath);
        QByteArray data_;
        QTextStream os(&data_);
        DFTBPlus::HSDNodeSerializer serializer(os);
        dftbplus_input.writeOut(&serializer);
        if (dev->open(QIODevice::WriteOnly))
        {
            emit Message(MsgType::MSG_INFO, QString("Info: The default DFTB+ input for %1 energy is added.\n").arg(catagory.at(i)));
            dev->write(data_);
        }
        dev->close();
        if(catagory.at(i) == "crystal" )
        {
            input_data.evaluation.crystal_default_dftbinput.template_energy = outputPath;
        }
        else
        {
            input_data.evaluation.mole_default_dftbinput.template_energy = outputPath;
        }



        for(int j=0; j<drivers.size();++j)
        {
            QString rcPath = QString("://dftbplus/input/%1/%2.hsd").arg(catagory.at(i)).arg(drivers.at(j));
            QString outputPath = QString("dftbinp/%1_%2.hsd").arg(catagory.at(i)).arg(drivers.at(j));
            QFile inputFile(rcPath);
            if (inputFile.open(QIODevice::ReadOnly))
            {
                QIODevice* dev = zipfile.device(outputPath);
                if (dev->open(QIODevice::WriteOnly))
                {
                    emit Message(MsgType::MSG_INFO, QString("Info: The default DFTB+ input for %1 %2 is added.\n").arg(catagory.at(i)).arg(drivers.at(j)));
                    dev->write(inputFile.readAll());
                }
                dev->close();
            }
        }
    }
}
void GeometryToSystem(const Geometry& geom, Erepfit2::Input::System& system)
{
    auto atoms = geom.coordinates();
    for(int i=0; i<atoms.size();++i)
    {
        Erepfit2::Input::Coordinate coord;
        coord.symbol = atoms[i].symbol;
        coord.coord[0] = atoms[i].coord[0];
        coord.coord[1] = atoms[i].coord[1];
        coord.coord[2] = atoms[i].coord[2];
        system.geometry.coordinates.append(coord);
    }
    system.uuid = geom.UUID();
    system.geometry.isPBC = geom.PBC();
    if (system.geometry.isPBC)
    {
        system.geometry.kpoints = geom.kpointsSettings();
        system.geometry.lattice_vectors = geom.lattice_vectors();
        system.geometry.scaling_factor = geom.scaling_factor();
        system.geometry.fractional_coordinates = geom.relativeCoordinates();
        system.geometry.unpaired_electrons = geom.getUPE();
        if (system.geometry.unpaired_electrons > 0.001)
        {
            system.geometry.spin_polarization = true;
        }
        if (system.geometry.spin_polarization)
            system.template_input =  "dftbinp/crystal_energy_spin.hsd";
        else
            system.template_input =  "dftbinp/crystal_energy.hsd";
    }
    else
    {
        system.geometry.charge = geom.getCharge();
        system.geometry.spin = geom.getSpin();
        if (system.geometry.spin > 1)
        {
            system.geometry.spin_polarization = true;
        }

        system.template_input = "dftbinp/molecule_energy.hsd";
        if (system.geometry.getNumAtoms() == 1)
        {
            if (system.geometry.spin > 1)
            {
                system.template_input = "dftbinp/atom_energy_spin.hsd";
            }
            else
            {
                system.template_input = "dftbinp/atom_energy.hsd";
            }
        }
    }
}

void Exportor::fetchErepfitBaseEquation(DFTBPSO::Input::SKOPTInput& dftb_pso_input)
{
    QList<QPair<QString, double> > energy_selection = m_wizard->m_pageErepfitSet->energyEquations();
    QList<QPair<QString, double>> force_selection = m_wizard->m_pageErepfitSet->forceEquations();


    QMap<QString, int> system_uuid_index_map;
    for(int i=0; i<dftb_pso_input.psoVector.repulsivePart->erepfitPSOVector->erepfit2_input_data.systems.size();++i)
    {
        system_uuid_index_map.insert(dftb_pso_input.psoVector.repulsivePart->erepfitPSOVector->erepfit2_input_data.systems[i].uuid.toString(), i);
    }

    for(int i=0; i<force_selection.size();++i)
    {
        QString uuid = force_selection[i].first;


        Geometry geom;
        if (!system_uuid_index_map.contains(uuid))
        {

            m_wizard->m_database->getGeometry(uuid, &geom);
            Erepfit2::Input::System system;
            QString pFileName = QString("%1").arg(geom.getName());
            system.name = pFileName;
            GeometryToSystem(geom, system);
            system_uuid_index_map.insert(uuid, dftb_pso_input.psoVector.repulsivePart->erepfitPSOVector->erepfit2_input_data.systems.size());
            dftb_pso_input.psoVector.repulsivePart->erepfitPSOVector->erepfit2_input_data.systems.append(system);
        }

        Erepfit2::Input::ForceEquation eq;
        eq.uuid = uuid;
        eq.weight = force_selection[i].second;
        eq.name = dftb_pso_input.psoVector.repulsivePart->erepfitPSOVector->erepfit2_input_data.systems[system_uuid_index_map[uuid]].name;
        eq.has_reference_force = false;

        if (geom.force().size() == geom.coordinates().size() && geom.isStationary() == false)
        {
            eq.has_reference_force = true;
            eq.reference_force = geom.force();
        }


        dftb_pso_input.psoVector.repulsivePart->erepfitPSOVector->erepfit2_input_data.equations.forceEquations.append(eq);

    }


    for(int i=0; i<energy_selection.size();++i)
    {
        QString uuid = energy_selection[i].first;

        MolecularAtomizationEnergyData energy_data;
        m_wizard->m_database->getMolecularAtomizationEnergy(uuid, &energy_data);

        auto geom_uuid = energy_data.geom_uuid();


        if (!system_uuid_index_map.contains(geom_uuid))
        {
            Geometry geom;
            m_wizard->m_database->getGeometry(geom_uuid, &geom);
            Erepfit2::Input::System system;
            QString pFileName = QString("%1").arg(geom.getName());
            system.name = pFileName;
            GeometryToSystem(geom, system);
            system_uuid_index_map.insert(geom_uuid, dftb_pso_input.psoVector.repulsivePart->erepfitPSOVector->erepfit2_input_data.systems.size());
            dftb_pso_input.psoVector.repulsivePart->erepfitPSOVector->erepfit2_input_data.systems.append(system);
        }



        Erepfit2::Input::EnergyEquation eq;
        eq.uuid = geom_uuid;
        eq.energy = energy_data.energy();
        eq.weight = energy_selection[i].second;
        eq.name = dftb_pso_input.psoVector.repulsivePart->erepfitPSOVector->erepfit2_input_data.systems[system_uuid_index_map[geom_uuid]].name;
        eq.unit = energy_data.unit();
        dftb_pso_input.psoVector.repulsivePart->erepfitPSOVector->erepfit2_input_data.equations.energyEquations.append(eq);

    }
}



void Exportor::fetchErepfitReactionEquation(DFTBPSO::Input::SKOPTInput &dftb_pso_input)
{
    QList<QPair<QString, double> > selection = m_wizard->m_pageErepfitSet->reactionEnergyEquations();

    for(int i=0; i<selection.size(); ++i)
    {
        QString uuid = selection[i].first;
        double weight = selection[i].second;
        Erepfit2::Input::ReactionEquation reaction_eq;
        ReactionEnergyData reaction_data;
        m_wizard->m_database->getReactionEnergy(uuid, &reaction_data);

        reaction_eq.name = reaction_data.name();
        reaction_eq.energy = reaction_data.energy();
        reaction_eq.unit = reaction_data.unit().trimmed();
        reaction_eq.weight = weight;
        reaction_eq.uuid = reaction_data.UUID();


        for(int iItem = 0; iItem < reaction_data.reactants().size();++iItem)
        {
            Erepfit2::Input::ReactionItem item;
            Geometry geom;
            auto pair = reaction_data.reactants().at(iItem);
            m_wizard->m_database->getGeometry(pair.uuid, &geom);

            QString pFileName = QString("%1").arg(geom.getName());

            bool existSystem = false;
            for(int i=0; i<dftb_pso_input.psoVector.repulsivePart->erepfitPSOVector->erepfit2_input_data.systems.size();++i)
            {
                if (dftb_pso_input.psoVector.repulsivePart->erepfitPSOVector->erepfit2_input_data.systems[i].name == pFileName)
                {
                    existSystem = true;
                }
            }
            if (!existSystem)
            {
                Erepfit2::Input::System system;
                system.name = pFileName;
                GeometryToSystem(geom, system);
                dftb_pso_input.psoVector.repulsivePart->erepfitPSOVector->erepfit2_input_data.systems.append(system);
            }

            item.coefficient = pair.coeff;
            item.name = pFileName;
            item.uuid = geom.UUID();
            reaction_eq.reactants.append(item);
        }

        for(int iItem = 0; iItem < reaction_data.products().size();++iItem)
        {
            Erepfit2::Input::ReactionItem item;
            Geometry geom;
            auto pair = reaction_data.products().at(iItem);
            m_wizard->m_database->getGeometry(pair.uuid, &geom);

            QString pFileName = QString("%1").arg(geom.getName());

            bool existSystem = false;
            for(int i=0; i<dftb_pso_input.psoVector.repulsivePart->erepfitPSOVector->erepfit2_input_data.systems.size();++i)
            {
                if (dftb_pso_input.psoVector.repulsivePart->erepfitPSOVector->erepfit2_input_data.systems[i].name == pFileName)
                {
                    existSystem = true;
                }
            }
            if (!existSystem)
            {
                Erepfit2::Input::System system;
                system.name = pFileName;
                GeometryToSystem(geom, system);
                dftb_pso_input.psoVector.repulsivePart->erepfitPSOVector->erepfit2_input_data.systems.append(system);
            }

            item.coefficient = pair.coeff;
            item.name = pFileName;
            item.uuid = geom.UUID();

            reaction_eq.products.append(item);
        }

        dftb_pso_input.psoVector.repulsivePart->erepfitPSOVector->erepfit2_input_data.equations.reactionEquations.append(reaction_eq);
    }
}

QMap<QString, QVariant> Exportor::loadDefaultHubbard()
{
    QMap<QString, QVariant> hubb_map;
    {
        QFile file(":/onecent/general/hubbard.json");
        file.open(QIODevice::ReadOnly | QIODevice::Text);
        QJsonDocument json_doc = QJsonDocument::fromJson(file.readAll());
        QVariant ala = json_doc.toVariant();
        hubb_map = ala.toMap();
        file.close();
    }

    return hubb_map;
}

QMap<QString, QVariant> Exportor::loadDefaultHubbardDerivs()
{
    QMap<QString, QVariant> hubb_map;
    {
        QFile file(":/onecent/general/hubbdriv.json");
        file.open(QIODevice::ReadOnly | QIODevice::Text);
        QJsonDocument json_doc = QJsonDocument::fromJson(file.readAll());
        QVariant ala = json_doc.toVariant();
        hubb_map = ala.toMap();
        file.close();
    }

    return hubb_map;
}

QMap<QString, QVariant> Exportor::loadDefaultOrbitalEnergy()
{
    QMap<QString, QVariant> oe_map;
    {
        QFile file(":/onecent/general/oe.json");
        file.open(QIODevice::ReadOnly);
        QJsonDocument json_doc = QJsonDocument::fromJson(file.readAll());
        file.close();
        QVariant ala = json_doc.toVariant();
        oe_map = ala.toMap();
    }

    return oe_map;
}

QMap<QString, QVariant> Exportor::loadDefaultOrbitalOccupation()
{
    QMap<QString, QVariant> occ_map;
    {
        QFile file(":/onecent/general/occ.json");
        file.open(QIODevice::ReadOnly);
        QJsonDocument json_doc = QJsonDocument::fromJson(file.readAll());
        file.close();
        QVariant ala = json_doc.toVariant();
        occ_map = ala.toMap();
    }

    return occ_map;
}

QMap<QString, QVariant> Exportor::loadDefaultOrbitalDefinition()
{
    QMap<QString, QVariant> orbs_map;
    {
        QFile file(":/onecent/general/orbs.json");
        file.open(QIODevice::ReadOnly | QIODevice::Text);
        QJsonDocument json_doc = QJsonDocument::fromJson(file.readAll());
        file.close();
        QVariant ala = json_doc.toVariant();
        orbs_map = ala.toMap();
    }
    return orbs_map;
}

QString Exportor::getGoodFilename(const QString &oriFilename) const
{
    QString newFileName = oriFilename;
    newFileName.replace(QRegExp("[^a-zA-Z0-9_+\\-]"), "");
    return newFileName;
}
