#ifndef ELEMENTSORTFILTERPROXYMODEL_H
#define ELEMENTSORTFILTERPROXYMODEL_H

#include <QSortFilterProxyModel>
#include <QSet>
#include <QString>

class ElementSortFilterProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    explicit ElementSortFilterProxyModel(QObject *parent = 0);
    int elementFilterKeyColumn() const;
    void setElementFilterKeyColumn(int elementFilterKeyColumn);

    enum ElementFilterMode
    {
        Exact = 0,
        Any = 1,
        Subset = 2
    };

    ElementFilterMode elementFilterMode() const;
    void setElementFilterMode(const ElementFilterMode &elementFilterMode);

signals:
public slots:
    void setElementFilter(const QStringList& elems);
    void clearElementFilter();
private:
    void addElement(const QString& elem);
    void removeElement(const QString& elem);
    int m_elementFilterKeyColumn = -1;
    QSet<QString> m_acceptElements;
    ElementFilterMode m_elementFilterMode = Exact;


    // QSortFilterProxyModel interface
protected:
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;
};

#endif // ELEMENTSORTFILTERPROXYMODEL_H
