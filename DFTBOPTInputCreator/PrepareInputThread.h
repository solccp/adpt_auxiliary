#ifndef PREPAREINPUTTHREAD_H
#define PREPAREINPUTTHREAD_H

#include <QThread>

namespace DFTBTestSuite
{
    class ReferenceData;
    class InputData;
}
class TestSuiteInputFileGenerator;
class PrepareInputThread : public QThread
{
    Q_OBJECT
public:
    PrepareInputThread(TestSuiteInputFileGenerator *generator, DFTBTestSuite::ReferenceData* _ref_data, DFTBTestSuite::InputData* _input_data, QObject *parent = 0);

signals:

public slots:

private:
    DFTBTestSuite::ReferenceData *ref_data;
    DFTBTestSuite::InputData *input_data;
    TestSuiteInputFileGenerator *m_generator;

    // QThread interface
protected:
    void run();
};

#endif // PREPAREINPUTTHREAD_H
