#include "ElementSortFilterProxyModel.h"
#include <atomicproperties.h>
#include <QSet>

ElementSortFilterProxyModel::ElementSortFilterProxyModel(QObject *parent) :
    QSortFilterProxyModel(parent)
{
}

void ElementSortFilterProxyModel::addElement(const QString &elem)
{
    const ADPT::AtomicProperties* elem_real = ADPT::AtomicProperties::fromSymbol(elem);
    if (elem_real != &ADPT::AtomicProperties::ERROR_ATOM)
    {
        m_acceptElements.insert(QString("[%1]").arg(elem_real->getSymbol()));
    }
}

void ElementSortFilterProxyModel::removeElement(const QString &elem)
{
    if (m_acceptElements.remove(QString("[%1]").arg(elem)))
        invalidateFilter();
}
ElementSortFilterProxyModel::ElementFilterMode ElementSortFilterProxyModel::elementFilterMode() const
{
    return m_elementFilterMode;
}

void ElementSortFilterProxyModel::setElementFilterMode(const ElementFilterMode &elementFilterMode)
{
    m_elementFilterMode = elementFilterMode;
}

int ElementSortFilterProxyModel::elementFilterKeyColumn() const
{
    return m_elementFilterKeyColumn;
}

void ElementSortFilterProxyModel::setElementFilterKeyColumn(int elementFilterKeyColumn)
{
    m_elementFilterKeyColumn = elementFilterKeyColumn;
    invalidateFilter();
}

void ElementSortFilterProxyModel::setElementFilter(const QStringList &elems)
{
    m_acceptElements.clear();
    for(int i=0; i<elems.size();++i)
    {
        addElement(elems[i]);
    }
    invalidateFilter();
}

void ElementSortFilterProxyModel::clearElementFilter()
{
    m_acceptElements.clear();
    invalidateFilter();
}



bool ElementSortFilterProxyModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    bool succ = QSortFilterProxyModel::filterAcceptsRow(source_row, source_parent);
    if (!succ)
    {
        return false;
    }
    if (m_elementFilterKeyColumn == -1)
    {
        return succ;
    }
    if (m_acceptElements.isEmpty())
    {
        return succ;
    }
    int column = m_elementFilterKeyColumn;
    QModelIndex index = sourceModel()->index(source_row, column, source_parent);
    if (!index.isValid())
    {
        return succ;
    }
    QString elem_str = sourceModel()->data(index).toString();
    QStringList elems = elem_str.split(",", QString::SkipEmptyParts);

    QSet<QString> targetElements = elems.toSet();

    if (m_elementFilterMode == Exact)
    {
        if (targetElements==m_acceptElements)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else if (m_elementFilterMode == Any)
    {
        if (!(m_acceptElements-targetElements).isEmpty())
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    else
    {
        if ((targetElements - m_acceptElements).isEmpty())
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    return true;
}
