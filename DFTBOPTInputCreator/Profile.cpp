#include "Profile.h"
#include <QDebug>
#include <QStringList>

QDataStream &operator<<(QDataStream &out, const DFTBOPTProfile &profile)
{
    profile.saveProfile(out);
    return out;
}


QDataStream &operator>>(QDataStream &in, DFTBOPTProfile &profile)
{
    profile.loadProfile(in);
    return in;
}


TrainingSetParameter* DFTBOPTProfile::getTrainingSetSelection()
{
    return &this->m_trainingSetParameter;
}

ErepfitParameter *DFTBOPTProfile::getErepfitSelection()
{
    return &this->m_erepfitParameter;
}



DFTBOPTProfile::~DFTBOPTProfile()
{
    clear();
}


QString PropertySelection::propertyName()
{
    return m_property_name;
}

double &PropertySelection::operator[](const QString &uuid)
{
    return m_selection[uuid];
}

double PropertySelection::operator[](const QString &uuid) const
{
    return m_selection[uuid];
}

void PropertySelection::remove(const QString &uuid)
{
    m_selection.remove(uuid);
}

void PropertySelection::insert(const QString &uuid, double weight)
{
    m_selection.insert(uuid, weight);
}

void PropertySelection::clear()
{
    m_selection.clear();
}

QHashIterator<QString, double> PropertySelection::getIterator() const
{
    return QHashIterator<QString, double>(m_selection);
}

QList<QPair<QString, double >> PropertySelection::getSelectionList() const
{
    QList<QPair<QString, double>> list;
    QHashIterator<QString, double> it(m_selection);
    while(it.hasNext())
    {
        it.next();
        list.append(qMakePair(it.key(), it.value()));
    }
    return list;
}

void PropertySelection::save(QDataStream &out) const
{
    out << m_property_name;
    out << (qint32) m_selection.size();
    QHashIterator<QString, double> it(m_selection);
    while(it.hasNext())
    {
        it.next();
        out << it.key() << it.value();
    }
}

void PropertySelection::load(QDataStream &in)
{
    qint32 selection_size;
    in >> selection_size;
    for(int j=0; j< selection_size; ++j)
    {
        QString selection_id;
        double selection_weight;
        in >> selection_id >> selection_weight;
        m_selection.insert(selection_id, selection_weight);
    }
}

bool DFTBOPTProfile::saveProfile(QDataStream &out) const
{
    out << m_elements;
    m_trainingSetParameter.save(out);
    m_erepfitParameter.save(out);
    return true;
}

bool DFTBOPTProfile::loadProfile(QDataStream &in)
{
    try
    {
        this->clear();
        in >> m_elements;
        m_trainingSetParameter.load(in);
        m_erepfitParameter.load(in);
        return true;
    }
    catch(...)
    {
        return false;
    }
}

void DFTBOPTProfile::clear()
{
    m_trainingSetParameter.clear();
    m_erepfitParameter.clear();
}
QStringList DFTBOPTProfile::elements() const
{
    return m_elements;
}

void DFTBOPTProfile::setElements(const QStringList &elements)
{
    m_elements = elements;
}



void TrainingSetParameter::clear()
{
    for(int i=0; i<m_selection.size(); ++i)
    {
       delete m_selection[i];
    }
    m_selection.clear();
}

TrainingSetParameter::~TrainingSetParameter()
{
    for(int i=0; i<m_selection.size(); ++i)
    {
       delete m_selection[i];
    }
    m_selection.clear();
}

const QList<PropertySelection *> TrainingSetParameter::getSelection() const
{
    return m_selection;
}

QList<PropertySelection *> TrainingSetParameter::getSelection()
{
    return m_selection;
}

PropertySelection *TrainingSetParameter::getSelection(const QString &propertyName)
{
    for(int i=0; i<m_selection.size(); ++i)
    {
        if (m_selection[i]->propertyName() == propertyName.toLower())
        {
            return m_selection[i];
        }
    }
    PropertySelection *property = new PropertySelection(propertyName.toLower());
    m_selection.append(property);
    return property;
}

void TrainingSetParameter::save(QDataStream &out) const
{
    out << (qint32) m_selection.size();
    for(int i=0; i<m_selection.size();++i)
    {
        m_selection[i]->save(out);
    }
}

void TrainingSetParameter::load(QDataStream &in)
{
    qint32 property_size;
    in >> property_size;
    for(int i=0; i<property_size;++i)
    {
        QString name;
        in >> name;
        PropertySelection* property = new PropertySelection(name);
        property->load(in);
        m_selection.append(property);
    }
}


void ErepfitParameter::save(QDataStream &out) const
{
    TrainingSetParameter::save(out);
    out << (qint32) m_additionalEquations.size();
    for(int i=0; i<m_additionalEquations.size();++i)
    {
        m_additionalEquations[i].save(out);
    }
}

void ErepfitParameter::load(QDataStream &in)
{
    TrainingSetParameter::load(in);
    qint32 size;
    in >> size;
    for(int i=0; i<size;++i)
    {
        AdditionalEquationSelection add;
        add.load(in);
        m_additionalEquations.append(add);
    }
}


void AdditionalEquationSelection::save(QDataStream &out) const
{
    out << potential << derivative << distance << isFix;
    if (isFix)
    {
        out << value;
    }
    else
    {
        out << min << max;
    }
}

void AdditionalEquationSelection::load(QDataStream &in)
{
    in >> potential >> derivative >> distance >> isFix;
    if (isFix)
    {
        in >> value;
    }
    else
    {
        in >> min >> max;
    }
}
