#ifndef TESTSUITEINPUTFILEGENERATOR_H
#define TESTSUITEINPUTFILEGENERATOR_H

#include <QObject>
#include <QSet>
#include <QMap>
#include "DFTBTestSuite/ReferenceDataType/ReferenceData.h"
#include "DFTBTestSuite/InputFile.h"


class DFTBRefDatabase;
class TestSuiteInputFileGenerator : public QObject
{
    Q_OBJECT
public:
    TestSuiteInputFileGenerator(DFTBRefDatabase* database, QObject *parent = 0);

signals:

public slots:
//    void reset();
    void addGeometryTarget(const QString& uuid, double weight = 1.0);
    void addFrequencyTarget(const QString& uuid, double weight = 1.0);
    void addAtomizationEnergyTarget(const QString& uuid, double weight = 1.0);
    void addBandStructureTarget(const QString& uuid, double weight = 1.0);
    void addReactionEnergyTarget(const QString& uuid, double weight = 1.0);
    DFTBTestSuite::ReferenceData exportRefData();
    DFTBTestSuite::InputData exportInputData();
public:
    bool addGeometryData(const QString &uuid, bool *isPBC);
    QSet<QPair<QString, QString> > neededSKPairs() const;

private:
    DFTBRefDatabase* m_database;
private:
    //Reference Data
    QMap<QString, DFTBTestSuite::MolecularData> m_uuid_molecularData;
    QMap<QString, DFTBTestSuite::ReferenceReactionData> m_uuid_reactionData;
    QMap<QString, DFTBTestSuite::CrystalGeometryData> m_uuid_crystalGeometryData;
    QMap<QString, DFTBTestSuite::BandStructureData> m_uuid_bandstructureData;

private:
    //Evaluation
    QSet<QString> m_eval_crystalStructure;
    QMap<QString, DFTBTestSuite::MoleculeTarget::EvaluationLevel > m_eval_molecule;
    QSet<QString> m_eval_bandStructure;
    QMap<QString, bool> m_eval_reaction;
private:
    //Test Input
    //Molecule
    QMap<QString, double> m_test_geometry;
    QMap<QString, double> m_test_frequency;
    QMap<QString, double> m_test_atomizationEnergy;
    //Crystal
    QMap<QString, double> m_test_bandStructure;
    //Reaction
    QMap<QString, double> m_test_reaction;

    QMap<QString, QString> m_uuid_name;
};

#endif // TESTSUITEINPUTFILEGENERATOR_H
