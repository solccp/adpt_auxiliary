#include "testsuiteinputfilegenerator.h"

#include "geometry.h"
#include "molecularfrequencydata.h"
#include "molecularatomizationenergydata.h"
#include "bandstructuredata.h"
#include "reactionenergydata.h"

#include "dftbrefdatabase.h"

TestSuiteInputFileGenerator::TestSuiteInputFileGenerator(DFTBRefDatabase *database, QObject *parent) :
    QObject(parent)
{
    m_database = database;
}

bool TestSuiteInputFileGenerator::addGeometryData(const QString &uuid, bool *isPBC = nullptr)
{
    Geometry* data = new Geometry();
    m_database->getGeometry(uuid, data);
    if (!data->PBC())
    {
        if (!m_uuid_molecularData.contains(uuid))
        {

            DFTBTestSuite::MolecularData mole_data;

            mole_data.name = data->getName();
            mole_data.uuid = data->UUID();
            mole_data.name = data->getName();
            mole_data.charge = data->getCharge();
            mole_data.spin = data->getSpin();

            DFTBTestSuite::GeometryData geom_data;
            geom_data.basisSet = data->getBasis();
            geom_data.method = data->getMethod();

            const QList<Coordinate> &atoms = data->coordinates();
            for(int i=0; i<atoms.size();++i)
            {
                geom_data.geometry.addAtom(atoms.at(i).symbol,
                                           atoms.at(i).coord[0],
                                           atoms.at(i).coord[1],
                                           atoms.at(i).coord[2]
                                           );
            }
            geom_data.optimized = data->isStationary();
            mole_data.geometryData = geom_data;
            m_uuid_molecularData.insert(uuid, mole_data);
            m_uuid_name.insert(uuid, mole_data.name);
        }
        delete data;
        if (isPBC)
        {
            *isPBC = false;
        }
        return true;
    }
    else
    {
        if (!m_uuid_crystalGeometryData.contains(uuid))
        {

            DFTBTestSuite::CrystalGeometryData geom_data;
            geom_data.name = data->getName();
            geom_data.basis = data->getBasis();
            geom_data.method = data->getMethod();
            geom_data.uuid = data->UUID();

            if (data->relativeCoordinates())
                geom_data.fractional_coordinates = true;
            else
                geom_data.fractional_coordinates = false;

            geom_data.scaling_factor = data->scaling_factor();
            geom_data.lattice_vectors = data->lattice_vectors();

            const QList<Coordinate> &atoms = data->coordinates();
            for(int i=0; i<atoms.size();++i)
            {
                geom_data.coordinates.append(DFTBTestSuite::AtomCoordinate(atoms.at(i).symbol,
                                                                           atoms.at(i).coord[0],
                                                                           atoms.at(i).coord[1],
                                                                           atoms.at(i).coord[2])
                                             );
            }


//            for(auto i: {0,1,2})
//            {
//                geom_data.kpoints[i] = data->kpointsSettings().m_kpoints_supercell.num_of_cells[i][i];
//            }
//            geom_data.kpoint_shifts = data->kpointsSettings().m_kpoints_supercell.shifts;

            geom_data.kpoints = data->kpointsSettings();

            m_uuid_crystalGeometryData.insert(uuid, geom_data);
            m_uuid_name.insert(uuid, geom_data.name);

        }
        delete data;
        if (isPBC)
        {
            *isPBC = true;
        }
        return true;
    }
    return false;
}
QSet<QPair<QString, QString> > TestSuiteInputFileGenerator::neededSKPairs() const
{
    QSet<QPair<QString, QString> > m_neededSKPairs;

    {
        QMapIterator<QString, DFTBTestSuite::MolecularData> it(m_uuid_molecularData);
        while(it.hasNext())
        {
            it.next();
            const DFTBTestSuite::MolecularData& data = it.value();
            const QList<QString>& elements = data.geometryData.geometry.getElements();
            for(int i=0; i<elements.size(); ++i)
            {
                for(int j=0; j<=i; ++j)
                {
                    QPair<QString, QString> pair(elements[i], elements[j]);
                    m_neededSKPairs.insert(pair);
                }
            }
        }
    }
    {
        QMapIterator<QString, DFTBTestSuite::CrystalGeometryData> it(m_uuid_crystalGeometryData);
        while(it.hasNext())
        {
            it.next();
            const DFTBTestSuite::CrystalGeometryData& data = it.value();
            const QList<QString>& elements = data.getElements();
            for(int i=0; i<elements.size(); ++i)
            {
                for(int j=0; j<=i; ++j)
                {
                    QPair<QString, QString> pair(elements[i], elements[j]);
                    m_neededSKPairs.insert(pair);
                }
            }
        }
    }




    return m_neededSKPairs;
}

void TestSuiteInputFileGenerator::addGeometryTarget(const QString &uuid, double weight)
{
    bool PBC = false;
    if(addGeometryData(uuid, &PBC))
    {
        if (PBC)
        {
            if (!m_eval_crystalStructure.contains(uuid))
            {
                m_eval_crystalStructure.insert(uuid);
            }
        }
        else
        {
            if (!m_eval_molecule.contains(uuid) || m_eval_molecule.value(uuid) < DFTBTestSuite::MoleculeTarget::Geometry)
            {
                m_eval_molecule.insert(uuid, DFTBTestSuite::MoleculeTarget::Geometry);
            }
            m_test_geometry.insert(uuid, weight);
        }
    }
}

void TestSuiteInputFileGenerator::addFrequencyTarget(const QString &uuid, double weight)
{
    MolecularFrequencyData* data = new MolecularFrequencyData();
    m_database->getMolecularFrequency(uuid, data);
    if (!m_uuid_molecularData.contains(data->geom_uuid()))
    {
        if (!addGeometryData(data->geom_uuid()))
        {
            return ;
        }
    }
    m_eval_molecule.insert(data->geom_uuid(), DFTBTestSuite::MoleculeTarget::Frequency);

    DFTBTestSuite::MolecularData& mole_data = m_uuid_molecularData[data->geom_uuid()];
    if (!mole_data.frequencyData.isValid)
    {
        DFTBTestSuite::FrequencyData freq_data;
        freq_data.basisSet = data->basis();
        freq_data.method = data->method();
        freq_data.frequencies = data->freq();
        freq_data.isValid = true;
        freq_data.ZPE = CalcZPE(freq_data.frequencies);
        freq_data.weights = data->weight();
        mole_data.frequencyData = freq_data;

    }
    m_test_frequency.insert(data->geom_uuid(), weight);
}

void TestSuiteInputFileGenerator::addAtomizationEnergyTarget(const QString &uuid, double weight)
{
    MolecularAtomizationEnergyData* data = new MolecularAtomizationEnergyData();
    m_database->getMolecularAtomizationEnergy(uuid, data);
    if (!m_uuid_molecularData.contains(data->geom_uuid()))
    {
        if (!addGeometryData(data->geom_uuid()))
        {
            return ;
        }
    }
    if (!m_eval_molecule.contains(data->geom_uuid()))
    {
        m_eval_molecule.insert(data->geom_uuid(), DFTBTestSuite::MoleculeTarget::Energy);
    }

    DFTBTestSuite::MolecularData& mole_data = m_uuid_molecularData[data->geom_uuid()];
    if (!mole_data.atomizationEnergyData.isValid)
    {
        DFTBTestSuite::AtomizationEnergyData ae_data;
        ae_data.basisSet = data->basis();
        ae_data.method = data->method();
        ae_data.energy = data->energy();
        ae_data.energy_unit = data->unit();
        ae_data.isValid = true;


        mole_data.atomizationEnergyData = ae_data;
    }

    m_test_atomizationEnergy.insert(data->geom_uuid(), weight);
}

void TestSuiteInputFileGenerator::addBandStructureTarget(const QString &uuid, double weight)
{


    BandstructureData* data = new BandstructureData();
    m_database->getBandStructure(uuid, data);

    if (!m_uuid_crystalGeometryData.contains(data->getGeomID()))
    {
        if (!addGeometryData(data->getGeomID()))
        {
            return ;
        }
    }

    DFTBTestSuite::BandStructureData bs_data;
    bs_data.basis = data->basis();
    bs_data.geom_uuid = data->getGeomID();
    bs_data.method = data->method();
    bs_data.name = data->getName();
    bs_data.uuid = data->UUID();
    bs_data.name = data->getName();
    bs_data.fermiIndex = data->getFermiIndex();


    bs_data.kpoints = data->getKpoints_setting();

    for(int i=0; i<data->default_optimization().size();++i)
    {
        DFTBTestSuite::BandStructureTestEntry entry;
        if (data->default_optimization().at(i).index_based)
        {
            entry.type = "index_based";
            entry.bandIndex_start = data->default_optimization().at(i).band_startindex;
            entry.bandIndex_end = data->default_optimization().at(i).band_endindex;
            entry.pointIndex_start = data->default_optimization().at(i).point_startindex;
            entry.pointIndex_end = data->default_optimization().at(i).point_endindex;
        }
        else
        {
            entry.type = "energy_based";
            entry.lower_energy = data->default_optimization().at(i).energy_lowerbound;
            entry.upper_energy = data->default_optimization().at(i).energy_upperbound;
        }
        entry.weight = data->default_optimization().at(i).weight;
        bs_data.defaultTestRegion.append(entry);
    }

    const QVector<QVector<double> > l_bands = data->bands();
    for(int i=0; i<l_bands.size();++i)
    {
        bs_data.bands.append(l_bands.at(i));
    }



    m_uuid_name.insert(uuid, bs_data.name);
    m_uuid_bandstructureData.insert(uuid, bs_data);
    m_eval_bandStructure.insert(uuid);
    m_test_bandStructure.insert(uuid, weight);
}

void TestSuiteInputFileGenerator::addReactionEnergyTarget(const QString &uuid, double weight)
{
    ReactionEnergyData* data = new ReactionEnergyData();
    m_database->getReactionEnergy(uuid, data);

    bool GOptComp = data->getOptimizeComponents();

    for(int i=0; i<data->reactants().size(); ++i)
    {
        QString geom_uuid = data->reactants().at(i).uuid;

        Geometry *geom_data = new Geometry();
        m_database->getGeometry(geom_uuid, geom_data);
        if (geom_data->PBC())
        {
            if (!m_uuid_crystalGeometryData.contains(geom_uuid))
            {
                if (!addGeometryData(geom_uuid))
                {
                    return ;
                }
            }
            m_eval_crystalStructure.insert(geom_uuid);
        }
        else
        {
            if (!m_uuid_molecularData.contains(geom_uuid))
            {
                if (!addGeometryData(geom_uuid))
                {
                    return ;
                }
            }
            if (GOptComp)
            {
                if (!m_eval_molecule.contains(geom_uuid) || m_eval_molecule.value(geom_uuid) < DFTBTestSuite::MoleculeTarget::Geometry)
                {
                    m_eval_molecule.insert(geom_uuid, DFTBTestSuite::MoleculeTarget::Geometry);
                }
            }
            else
            {
                if (!m_eval_molecule.contains(geom_uuid) || m_eval_molecule.value(geom_uuid) < DFTBTestSuite::MoleculeTarget::Energy)
                {
                    m_eval_molecule.insert(geom_uuid, DFTBTestSuite::MoleculeTarget::Energy);
                }
            }
        }
        delete geom_data;
    }
    for(int i=0; i<data->products().size(); ++i)
    {
        QString geom_uuid = data->products().at(i).uuid;
        Geometry *geom_data = new Geometry();
        m_database->getGeometry(geom_uuid, geom_data);

        if (geom_data->PBC())
        {
            if (!m_uuid_crystalGeometryData.contains(geom_uuid))
            {
                if (!addGeometryData(geom_uuid))
                {
                    return ;
                }
            }
            m_eval_crystalStructure.insert(geom_uuid);
        }
        else
        {
            if (!m_uuid_molecularData.contains(geom_uuid))
            {
                if (!addGeometryData(geom_uuid))
                {
                    return ;
                }
            }
            if (GOptComp)
            {
                if (!m_eval_molecule.contains(geom_uuid) || m_eval_molecule.value(geom_uuid) < DFTBTestSuite::MoleculeTarget::Geometry)
                {
                    m_eval_molecule.insert(geom_uuid, DFTBTestSuite::MoleculeTarget::Geometry);
                }
            }
            else
            {
                if (!m_eval_molecule.contains(geom_uuid) || m_eval_molecule.value(geom_uuid) < DFTBTestSuite::MoleculeTarget::Energy)
                {
                    m_eval_molecule.insert(geom_uuid, DFTBTestSuite::MoleculeTarget::Energy);
                }
            }
        }
        delete geom_data;
    }

    DFTBTestSuite::ReferenceReactionData reaction_data;
    reaction_data.basis = data->basis();
    reaction_data.energy = data->energy();
    reaction_data.energy_unit = data->unit();
    reaction_data.uuid = data->UUID();
    reaction_data.name = data->name();
    reaction_data.method = data->method();
    for(int i=0; i<data->reactants().size();++i)
    {
        reaction_data.reactants.append(DFTBTestSuite::ReactionItem(data->reactants().at(i).coeff,
                                                                   data->reactants().at(i).uuid));
    }
    for(int i=0; i<data->products().size();++i)
    {
        reaction_data.products.append(DFTBTestSuite::ReactionItem(data->products().at(i).coeff,
                                                                  data->products().at(i).uuid));
    }

    m_uuid_reactionData.insert(uuid, reaction_data);
    m_uuid_name.insert(uuid, data->name());
    m_eval_reaction.insert(uuid, GOptComp);
    m_test_reaction.insert(uuid, weight);
}

struct QPairSecondComparer
{
    template<typename T1, typename T2>
    bool operator()(const QPair<T1,T2> & a, const QPair<T1,T2> & b) const
    {
        return a.second < b.second;
    }
};

template<typename T>
QList<QPair<QString, QString> > sortMap(const QMap<QString, T>& map)
{
    QList<QPair<QString, QString> > pairs;

    auto uuids = map.keys();
    auto strs = map.values();
    for(int i=0; i<map.size();++i)
    {
        pairs.append(qMakePair(uuids[i], strs[i].name));
    }

    qSort(pairs.begin(), pairs.end(), QPairSecondComparer());
    return pairs;
}

QList<QPair<QString, QString> > sortMap(const QList<QString>& uuids, const QMap<QString, QString>& namemap)
{
    QList<QPair<QString, QString> > pairs;


    for(int i=0; i<uuids.size();++i)
    {
        pairs.append(qMakePair(uuids[i], namemap[uuids[i]]));
    }

    qSort(pairs.begin(), pairs.end(), QPairSecondComparer());
    return pairs;
}



DFTBTestSuite::ReferenceData TestSuiteInputFileGenerator::exportRefData()
{
    DFTBTestSuite::ReferenceData ref_data;

    //Molecule
    {
        {
            QList<QPair<QString, QString> > sortedUUID = sortMap(m_uuid_molecularData);
            QListIterator<QPair<QString, QString>> it(sortedUUID);
            while(it.hasNext())
            {
                auto pair = it.next();
                QString uuid = pair.first;
                const DFTBTestSuite::MolecularData& data = m_uuid_molecularData[uuid];
//                const QList<QString>& elements = data.geometryData.geometry.getElements();
//                for(int i=0; i<elements.size(); ++i)
//                {
//                    for(int j=0; j<=i; ++j)
//                    {
//                        QPair<QString, QString> pair(elements[i], elements[j]);
//                    }
//                }

                ref_data.molecularDataSet.append(data);
            }
        }
    }

    //Reaction
    {
        QList<QPair<QString, QString> > sortedUUID = sortMap(m_uuid_reactionData);
        QListIterator<QPair<QString, QString>> it(sortedUUID);
        while(it.hasNext())
        {
            auto pair = it.next();
            QString uuid = pair.first;
            const DFTBTestSuite::ReferenceReactionData& data = m_uuid_reactionData[uuid];
            ref_data.reactionDataSet.append(data);
        }
    }


    //Crystal Geometry
    {
        QList<QPair<QString, QString> > sortedUUID = sortMap(m_uuid_crystalGeometryData);
        QListIterator<QPair<QString, QString>> it(sortedUUID);
        while(it.hasNext())
        {
            auto pair = it.next();
            QString uuid = pair.first;
            const DFTBTestSuite::CrystalGeometryData& data = m_uuid_crystalGeometryData[uuid];
            ref_data.crystalData.geometryData.append(data);
        }
    }

    //Bandstructure
    {
        QList<QPair<QString, QString> > sortedUUID = sortMap(m_uuid_bandstructureData);
        QListIterator<QPair<QString, QString>> it(sortedUUID);
        while(it.hasNext())
        {
            auto pair = it.next();
            QString uuid = pair.first;
            const DFTBTestSuite::BandStructureData& data = m_uuid_bandstructureData[uuid];
            ref_data.crystalData.bandstructureData.append(data);
        }
    }

    return ref_data;
}

DFTBTestSuite::InputData TestSuiteInputFileGenerator::exportInputData()
{
    DFTBTestSuite::InputData input;


    {
        QList<QPair<QString, QString> > sortedUUID = sortMap(m_eval_molecule.keys(), m_uuid_name);
        QListIterator<QPair<QString, QString>> it(sortedUUID);
        while(it.hasNext())
        {
            auto pair = it.next();
            QString uuid = pair.first;
            const DFTBTestSuite::MoleculeTarget::EvaluationLevel& level = m_eval_molecule[uuid];
            DFTBTestSuite::MoleculeTarget target;
            target.evalutationLevel = level;
            target.uuid = uuid;
            target.name = m_uuid_name[uuid];
            input.evaluation.evaluationMolecule.moleculeTargets.append(target);
        }
    }
    {

        QList<QPair<QString, QString> > sortedUUID = sortMap(m_eval_crystalStructure.toList(), m_uuid_name);
        QListIterator<QPair<QString, QString>> it(sortedUUID);
        while(it.hasNext())
        {
            auto pair = it.next();
            QString uuid = pair.first;
            DFTBTestSuite::CrystalStructureTarget target;
            target.uuid = uuid;
            target.name = m_uuid_name[uuid];
            target.kpoints = this->m_uuid_crystalGeometryData[uuid].kpoints;
//            target.kpointShifts = this->m_uuid_crystalGeometryData[uuid].kpoint_shifts;
            input.evaluation.evaluationCrystal.crystalstructureTargets.append(target);
        }
    }
    {

        QList<QPair<QString, QString> > sortedUUID = sortMap(m_eval_reaction.keys(), m_uuid_name);
        QListIterator<QPair<QString, QString>> it(sortedUUID);
        while(it.hasNext())
        {
            auto pair = it.next();
            QString uuid = pair.first;
            DFTBTestSuite::ReactionTarget target;
            target.uuid = uuid;
            target.name = m_uuid_name[uuid];
            target.GeomOptComponents = m_eval_reaction[uuid];
            input.evaluation.evaluationReaction.reactionTargets.append(target);

        }
    }
    {

        QList<QPair<QString, QString> > sortedUUID = sortMap(m_eval_bandStructure.toList(), m_uuid_name);
        QListIterator<QPair<QString, QString>> it(sortedUUID);
        while(it.hasNext())
        {
            auto pair = it.next();
            QString uuid = pair.first;
            DFTBTestSuite::BandStructureTarget target;
            target.uuid = uuid;
            target.name = m_uuid_name[uuid];
//            target.kpoints = m_uuid_bandstructureData[uuid].kpoints;
//            target.kpointShifts = m_uuid_bandstructureData[uuid].kpointShifts;
            input.evaluation.evaluationCrystal.bandstructureTargets.append(target);

        }
    }

    //Testing section

    QMap<QString, DFTBTestSuite::TestMolecule> testTargets;

    {       

        QList<QPair<QString, QString> > sortedUUID = sortMap(m_test_atomizationEnergy.keys(), m_uuid_name);

        QListIterator<QPair<QString, QString>> it(sortedUUID);
        while(it.hasNext())
        {
            auto pair = it.next();
            QString uuid = pair.first;

            DFTBTestSuite::TestMolecule test_target;
            test_target.uuid = uuid;
            test_target.name = m_uuid_name[uuid];

            if (testTargets.contains(uuid))
            {
                test_target = testTargets[uuid];
            }
            DFTBTestSuite::PropertyTester_Input tester = DFTBTestSuite::PropertyTester_Input("atomization_energy"); //DFTBTestSuite::TestMolecule::AtomizationEnergyTester;
            tester.weight = m_test_atomizationEnergy[uuid];
            test_target.testers.append(tester);          
            testTargets.insert(uuid, test_target);
        }
    }
    {
        QList<QPair<QString, QString> > sortedUUID = sortMap(m_test_geometry.keys(), m_uuid_name);

        QListIterator<QPair<QString, QString>> it(sortedUUID);
        while(it.hasNext())
        {
            auto pair = it.next();
            QString uuid = pair.first;
            DFTBTestSuite::TestMolecule test_target;
            test_target.uuid = uuid;
            test_target.name = m_uuid_name[uuid];

            if (testTargets.contains(uuid))
            {
                test_target = testTargets[uuid];
            }

            DFTBTestSuite::PropertyTester_Input tester = DFTBTestSuite::PropertyTester_Input("molecular_geometry");
            tester.weight = m_test_geometry[uuid];
            test_target.testers.append(tester);
            testTargets.insert(uuid, test_target);
        }
    }
    {

        QList<QPair<QString, QString> > sortedUUID = sortMap(m_test_frequency.keys(), m_uuid_name);

        QListIterator<QPair<QString, QString>> it(sortedUUID);
        while(it.hasNext())
        {
            auto pair = it.next();
            QString uuid = pair.first;
            DFTBTestSuite::TestMolecule test_target;
            test_target.uuid = uuid;
            test_target.name = m_uuid_name[uuid];

            if (testTargets.contains(uuid))
            {
                test_target = testTargets[uuid];
            }

            DFTBTestSuite::PropertyTester_Input tester = DFTBTestSuite::PropertyTester_Input("frequency");
            tester.weight = m_test_frequency[uuid];
            test_target.testers.append(tester);
            testTargets.insert(uuid, test_target);
        }
    }

    {
        QList<QPair<QString, QString> > sortedUUID = sortMap(m_test_reaction.keys(), m_uuid_name);

        QListIterator<QPair<QString, QString>> it(sortedUUID);
        while(it.hasNext())
        {
            auto pair = it.next();
            QString uuid = pair.first;


            DFTBTestSuite::TestReaction test_target;
            test_target.weight = m_test_reaction[uuid];
            test_target.uuid = uuid;
            test_target.name = m_uuid_name[uuid];
            input.testingOption.testReactions.append(test_target);
        }
    }

    {

        QList<QPair<QString, QString> > sortedUUID = sortMap(m_test_bandStructure.keys(), m_uuid_name);

        QListIterator<QPair<QString, QString>> it(sortedUUID);
        while(it.hasNext())
        {
            auto pair = it.next();
            QString uuid = pair.first;

            DFTBTestSuite::TestBandStructure test_target;
            test_target.weight = m_test_bandStructure[uuid];
            test_target.testRegion = m_uuid_bandstructureData[uuid].defaultTestRegion;
            test_target.uuid = uuid;
            test_target.name = m_uuid_name[uuid];
            input.testingOption.testBandStructure.append(test_target);
        }
    }

    {
        QList<QPair<QString, QString> > sortedUUID = sortMap(testTargets);
        QListIterator<QPair<QString, QString>> it(sortedUUID);
        while(it.hasNext())
        {
            auto pair = it.next();
            QString uuid = pair.first;
            const DFTBTestSuite::TestMolecule& target = testTargets[uuid];
            input.testingOption.testMolecules.append(target);

        }
    }



    input.control.ScratchDir = QDir::tempPath();

    if (!m_eval_bandStructure.isEmpty())
    {
        input.evaluation.crystal_default_dftbinput.template_energy = "dftbinp/crystal_energy.hsd";
    }
    if (!m_eval_molecule.isEmpty())
    {
        input.evaluation.mole_default_dftbinput.template_energy = "dftbinp/molecule_energy.hsd";
        input.evaluation.mole_default_dftbinput.template_opt = "dftbinp/molecule_opt.hsd";
        input.evaluation.mole_default_dftbinput.template_frequency = "dftbinp/molecule_freq.hsd";
    }

    input.evaluation.SKInfo = std::make_shared<ADPT::SKFileInfo>();
    input.evaluation.SKInfo->type2Names = true;
    input.evaluation.SKInfo->prefix = "skfiles/";

    return input;

}
