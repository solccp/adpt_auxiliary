#-------------------------------------------------
#
# Project created by QtCreator 2014-03-03T12:59:49
#
#-------------------------------------------------

QT += sql
TARGET = DFTBOPTInputAdaptor
TEMPLATE = lib
CONFIG += staticlib c++11

SOURCES += \
    testsuiteinputfilegenerator.cpp

HEADERS += \
    testsuiteinputfilegenerator.h


INCLUDEPATH += $$PWD/../external_libs/include/

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libDFTBTestSuiteCommon/release/ -lDFTBTestSuiteCommon
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libDFTBTestSuiteCommon/debug/ -lDFTBTestSuiteCommon
else:unix: LIBS += -L$$OUT_PWD/../libDFTBTestSuiteCommon/ -lDFTBTestSuiteCommon

INCLUDEPATH += $$PWD/../libDFTBTestSuiteCommon
DEPENDPATH += $$PWD/../libDFTBTestSuiteCommon

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuiteCommon/release/libDFTBTestSuiteCommon.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuiteCommon/debug/libDFTBTestSuiteCommon.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuiteCommon/release/DFTBTestSuiteCommon.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuiteCommon/debug/DFTBTestSuiteCommon.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libDFTBTestSuiteCommon/libDFTBTestSuiteCommon.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libDFTBRefDatabase/release/ -lDFTBRefDatabase
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libDFTBRefDatabase/debug/ -lDFTBRefDatabase
else:unix: LIBS += -L$$OUT_PWD/../libDFTBRefDatabase/ -lDFTBRefDatabase

INCLUDEPATH += $$PWD/../libDFTBRefDatabase
DEPENDPATH += $$PWD/../libDFTBRefDatabase

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBRefDatabase/release/libDFTBRefDatabase.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBRefDatabase/debug/libDFTBRefDatabase.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBRefDatabase/release/DFTBRefDatabase.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libDFTBRefDatabase/debug/DFTBRefDatabase.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libDFTBRefDatabase/libDFTBRefDatabase.a

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../libadpt_common/release/ -ladpt_common
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../libadpt_common/debug/ -ladpt_common
else:unix: LIBS += -L$$OUT_PWD/../libadpt_common/ -ladpt_common

INCLUDEPATH += $$PWD/../libadpt_common
DEPENDPATH += $$PWD/../libadpt_common

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/release/libadpt_common.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/debug/libadpt_common.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/release/adpt_common.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/debug/adpt_common.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../libadpt_common/libadpt_common.a
