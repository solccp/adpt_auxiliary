#!/usr/bin/env python3

import json
import sys
import uuid

from pathlib import Path
import pymatgen as mg
from pyparsing import *
import numpy as np
import pyparsing as pp
from json import encoder
encoder.FLOAT_REPR = lambda o: format(o, '.6f')

def progress(count, total, suffix=''):
    bar_len = 60
    filled_len = int(round(bar_len * count / float(total)))

    percents = round(100.0 * count / float(total), 1)
    bar = '=' * filled_len + '-' * (bar_len - filled_len)

    sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', suffix))
    sys.stdout.flush()  

def read_single_line(filename, ignoreError = True):
    try:
        with open (filename, "r") as f:
            return f.readline().strip()
    except:
        if (ignoreError):
            return ''
        else:
            raise
def read_lines(filename, ignoreError = True):
    try:
        with open (filename, "r") as f:
            return f.readlines()
    except:
        if (ignoreError):
            return []
        else:
            raise

def convertInt(origString, loc, tokens):
    return int(tokens[0])

def convertFloat(origString, loc, tokens):
    return float(tokens[0])

def parseKLinesFile(filename):

    point = Literal('.')
    e = CaselessLiteral('E')
    plusorminus = Literal('+') | Literal('-')
    number = Word(nums)
    integer = Combine( Optional(plusorminus) + number )
    floatnumber = Combine( integer +  Optional( point + Optional(number) ) + Optional( e + integer ))

    noLineBreakWhite = White(' \t')

    xyz = Suppress(OneOrMore(White(' \t'))) + floatnumber.setParseAction(convertFloat) + Suppress(OneOrMore(White(' \t'))) + floatnumber.setParseAction(convertFloat) + Suppress(OneOrMore(White(' \t'))) + floatnumber.setParseAction(convertFloat)

    klines = Optional(Suppress(CaselessLiteral('KPointsAndWeights') + OneOrMore(noLineBreakWhite) + Literal('=') + OneOrMore(noLineBreakWhite) + CaselessLiteral('klines') + OneOrMore(noLineBreakWhite) + Literal('{'))) + OneOrMore(Suppress(ZeroOrMore(White())) + Group(integer.setParseAction(convertInt) + xyz ))

    res = klines.parseFile(filename)
    return res.asList()

def parseBandsFile(filename):
    tbands = []
    ncolumns = 0
    with open(filename, 'r') as f:
        for line in f:
            arr = list(map(float, line.split()[1:]))
            if (ncolumns == 0):
                ncolumns = len(arr)
            elif (ncolumns > 0 and len(arr) != ncolumns):
                raise ValueError("Number of columns are not consistent in the BANDS file")
            tbands.append(arr)
    bands = list(map(list, zip(*tbands)))
    return bands

def withModifier(s, loc, tokens):
    temp = tokens[0]
    res = [temp['key'], temp['value']]
    tokens[0] = pp.ParseResults(res)
    if ('modifier' in temp):
        tokens.append(pp.ParseResults([temp['key']+'Modifier', temp['modifier']]))
    return tokens


def convertNumbers(s,l,toks):
    n = toks[0]
    try:
        return int(n)
    except ValueError as ve:
        return float(n)

def parseGeomFile(filename):
    TRUE = pp.CaselessKeyword("yes").setParseAction( pp.replaceWith(True) )
    FALSE = pp.CaselessKeyword("no").setParseAction( pp.replaceWith(False) )

    hsdString = pp.Word(pp.alphas) | pp.dblQuotedString.setParseAction(pp.removeQuotes)

    hsdNumber = pp.Combine( pp.Optional('-') + ( '0' | pp.Word('123456789', pp.nums) ) +
                        pp.Optional( '.' + pp.Word(pp.nums) ) +
                        pp.Optional( pp.Word('eE',exact=1) + pp.Word(pp.nums+'+-',pp.nums) ) )

    hsdPropertyList = pp.Forward()
    hsdMethod = pp.Forward()
    hsdValue = pp.Forward()
    hsdIdentifier = pp.Word( pp.srange("[a-zA-Z_]"), pp.srange("[a-zA-Z0-9_]") )

    hsdModifier = pp.Group(pp.Suppress('[') + pp.delimitedList(pp.Word(pp.srange("[a-zA-Z_^-/]"))) + pp.Suppress(']'))

    hsdElements = pp.OneOrMore( hsdValue )
    hsdArray = pp.Group(pp.Suppress('{') + pp.Optional(hsdElements) + pp.Suppress('}') )
    hsdValue << ( TRUE | FALSE | hsdString | hsdNumber | pp.Group(hsdMethod) |  pp.Group(hsdPropertyList) | hsdArray )

    memberDef = pp.Group( hsdIdentifier.setResultsName('key') + pp.Optional(hsdModifier.setResultsName('modifier')) + pp.Suppress('=') + hsdValue.setResultsName('value') ).setParseAction(withModifier)
    hsdMembers = pp.ZeroOrMore( memberDef )
    hsdPropertyList << pp.Dict( pp.Suppress('{') + pp.Optional(hsdMembers) + pp.Suppress('}') )
    hsdMethod << pp.Dict( hsdIdentifier + pp.Suppress('{') + pp.Optional(hsdMembers) + pp.Suppress('}') )

    hsdObject = pp.Dict( pp.OneOrMore(memberDef) )


    hsdNumber.setParseAction( convertNumbers )
    res = hsdObject.parseFile(filename, parseAll=True)
    if (not 'Geometry' in res):
        raise ValueError("Not a valid DFTB+ GEOM file")

    obj = {}
    geom = res.Geometry
    if ('Periodic' in geom and geom.Periodic):
        obj['pbc'] = True
        fractional = False
        if ('TypesAndCoordinatesModifier' in geom and geom.TypesAndCoordinatesModifier[0].lower() == 'relative'):
            fractional = True 
        obj['fractional'] = fractional
        if ('LatticeVectorsModifier' in geom and geom.LatticeVectorsModifier[0].lower() != 'angstrom'):
            raise ValueError("modifier unit convertion is not implement yet")

        obj['lattice_vectors'] = np.reshape(geom.LatticeVectors.asList(), (3,3)).tolist() 
    else:
        obj['pbc'] = False
    
    elements = geom.TypeNames.asList()
    coords = []
    
    ori_coords = np.reshape(geom.TypesAndCoordinates.asList(), (len(elements), 4)).tolist()
    for line in ori_coords:
        sym = elements[int(line[0])-1]
        coord = [sym]
        coord += line[1:]
        coords.append(coord)

    obj['coordinates'] = coords
    obj['scaling_factor'] = 1.0
    
    return obj

def parsePOSCARFile(filename):
    obj = {}
    poscar_str = read_lines(filename, False)
    scaling = float(poscar_str[1])
    obj['scaling_factor'] = scaling
    direct = False
    for lineno in [6, 7, 8]:
        if (lineno > len(poscar_str)-1):
            break
        if (poscar_str[lineno].lower().startswith("d")):
            direct = True
            break
    obj['fractional'] = direct

    poscar = mg.Structure.from_str("".join(poscar_str), fmt='poscar')
    obj['lattice_vectors'] = list(map(list,poscar.lattice_vectors()/scaling))
    coords = []
    for site in poscar.sites:
        tmp = [str(site.specie)]
        for x in site.coords:
            tmp.append(x)
        coords.append(tmp)

    obj['coordinates'] = coords
    obj['pbc'] = True
    return obj

def parseXYZFile(filename):
    obj = {}
    geom_str = read_lines(filename, False)
    nat = int(geom_str[0])
    title = geom_str[1]
    coords = []
    for i in range(nat):
        line = geom_str[2+i]
        arr = line.split()
        tmp = [str(mg.Element(arr[0]))]
        tmp += list(map(float, arr[1:4]))
        coords.append(tmp)

    obj['coordinates'] = coords
    obj['pbc'] = False

    return obj




curPath = Path('.')
geom_uuid_path_map = {}
geom_path_uuid_map = {}
referred_geom_uuid = set()

print ("Checking if GEOMETRY exists...", end= '')
if ((curPath / "GEOMETRY").exists()):
    print ("yes")
    print ("Scanning GEOMETRY UUID...")
    geomPath = curPath / "GEOMETRY"
    entries = [x for x in geomPath.iterdir() if x.is_dir()]
    for i in range(len(entries)):
        progress(i+1, len(entries))
        temp = read_single_line(str(entries[i]/'UUID'))
        try:
            m_uuid = uuid.UUID(temp)
        except:
            print ("Wrong UUID: {} in folder {}".format(temp, entries[i]))
            sys.exit(1)
        geom_uuid_path_map[m_uuid] = entries[i]
        geom_path_uuid_map[entries[i].name] = m_uuid
    print ("")
else:
    print ("no")
    sys.exit(1)

object_root = {}

if ((curPath / "REACTION").exists()):
    print('Scanning REACTION...')
    reactionPath = curPath / "REACTION"
    entries = [x for x in reactionPath.iterdir() if x.is_dir()]
    reactions = []
    for i in range(len(entries)):
        progress(i+1, len(entries))
        temp = read_single_line(str(entries[i]/'UUID'))
        m_uuid = uuid.uuid4()
        try:
            m_uuid = uuid.UUID(temp)
        except:
            pass
        
        obj = {}
        obj["uuid"] = str(m_uuid)

        reactants = []
        reactants_str = read_lines(str(entries[i]/'REACTANTS'), False)
        for line in reactants_str:
            arr = line.split()
            coeff = float(arr[0])
            geom_uuid = None
            try:
                geom_uuid = uuid.UUID(arr[1])
            except:
                raise
            if (not geom_uuid in geom_uuid_path_map):
                print ("The reactant item {} of reaction {} is not defined in GEOMETRY".format(geom_uuid, str(entries[i])))
                sys.exit(2)
            referred_geom_uuid.add(geom_uuid)
            reactant_item = {}
            reactant_item['coeff'] = coeff
            reactant_item['uuid'] = str(geom_uuid)
            reactants.append(reactant_item)

        products = []
        products_str = read_lines(str(entries[i]/'PRODUCTS'), False)
        for line in products_str:
            arr = line.split()
            coeff = float(arr[0])
            geom_uuid = None
            try:
                geom_uuid = uuid.UUID(arr[1])
            except:
                raise
            if (not geom_uuid in geom_uuid_path_map):
                print ("The product item {} of reaction {} is not defined in GEOMETRY".format(geom_uuid, str(entries[i])))
                sys.exit(2)
            referred_geom_uuid.add(geom_uuid)
            product_item = {}
            product_item['coeff'] = coeff
            product_item['uuid'] = str(geom_uuid)
            products.append(product_item)

        obj['basis_set'] = read_single_line(str(entries[i]/'BASIS'))
        obj['method'] = read_single_line(str(entries[i]/'METHOD'), False)
        obj["comment"] = "".join(read_lines(str(entries[i]/'COMMENT')))
        obj["citation"] = "".join(read_lines(str(entries[i]/'CITATION')))
        obj["energy"] = float(read_single_line(str(entries[i]/'ENERGY'), False))
        obj["unit"] = "kcal/mol"
        obj["name"] = read_single_line(str(entries[i]/'NAME'), False)
        obj['reactants'] = reactants
        obj['products'] = products

        opt_str = read_single_line(str(entries[i]/'OPT'))
        opt = False
        if (opt_str.lower() == 'yes' or opt_str.lower() == 'true'):
            opt = True
        obj['optimize_components'] = opt

        reactions.append(obj)
    object_root['reaction_energy'] = reactions
    print ("")


if ((curPath / "BANDSTRUCTURE").exists()):
    print ("Scanning BANDSTRUCTURE...")
    bsPath = curPath / "BANDSTRUCTURE"

    entries = [x for x in bsPath.iterdir() if x.is_dir()]
    bandstrs = []
    for i in range(len(entries)):
        progress(i+1, len(entries))
        temp = read_single_line(str(entries[i]/'UUID'))
        m_uuid = uuid.uuid4()
        try:
            m_uuid = uuid.UUID(temp)
        except:
            pass
        
        obj = {}
        obj["uuid"] = str(m_uuid)
        obj['basis_set'] = read_single_line(str(entries[i]/'BASIS'))
        obj['method'] = read_single_line(str(entries[i]/'METHOD'), False)
        obj["comment"] = "".join(read_lines(str(entries[i]/'COMMENT')))
        obj["citation"] = "".join(read_lines(str(entries[i]/'CITATION')))
        obj["name"] = read_single_line(str(entries[i]/'NAME'), False)

        
        temp = read_single_line(str(entries[i]/'REFGEOM'))
        geom_uuid = None
        if (temp in geom_path_uuid_map):
            geom_uuid = geom_path_uuid_map[temp]
        else:
            try:
                geom_uuid = uuid.UUID(temp)
            except:
                raise
            if (not geom_uuid in geom_uuid_path_map):
                print ("The REFGEOM({}) of {} is not defined in GEOMETRY".format(geom_uuid, str(entries[i])))
                sys.exit(1)
        referred_geom_uuid.add(geom_uuid)
        obj['geom_uuid'] = str(geom_uuid)

        klines = parseKLinesFile(str(entries[i]/'KLINES'))
        obj['kpoints'] = {}
        obj['kpoints']["klines"] = klines

        bands = parseBandsFile(str(entries[i]/'BANDS'))
        obj['bands'] = bands

#        targets = []
#        target_part = parseTargetsFile(str(entries[i]/'TARGET'))
#        if (len(target_part) > 0):
#            targets.append(target_part)
#        target_part = parseFittingFile(str(entries[i]/'FITTING'))
#        if (len(target_part) > 0):
#            targets.append(target_part)

#        obj['default_optimization_target'] = targets

        bandstrs.append(obj)
    object_root['bandstructure'] = bandstrs
    print ("")


#really load the geometry
print ("Scanning GEOMETRY...")
geomPath = curPath / "GEOMETRY"
geometry = []
referred_geom_uuid_list = list(referred_geom_uuid)
for i in range(len(referred_geom_uuid_list)):
    progress(i+1, len(referred_geom_uuid_list))
    uuid = referred_geom_uuid_list[i]
    entry_path = geom_uuid_path_map[uuid]

    obj = {}
    obj['uuid'] = str(uuid)
    obj['basis_set'] = read_single_line(str(entry_path/'BASIS'))
    obj['method'] = read_single_line(str(entry_path/'METHOD'), False)
    obj["comment"] = "".join(read_lines(str(entry_path/'COMMENT')))
    obj["citation"] = "".join(read_lines(str(entry_path/'CITATION')))
    obj["name"] = read_single_line(str(entry_path/'NAME'), False)
    
    
    #try parse as POSCAR
    GeomIsOK = True
    try:
        geom_obj = parsePOSCARFile(str(entry_path/'GEOM'))
        obj.update(geom_obj)
        supercell = []
        sampling_str = " ".join(read_lines(str(entry_path/'SAMPLING'), False))
        arr = sampling_str.split()
        if (len(arr) != 6):
            raise ValueError("Wrong format for SAMPLING file.")
            

        for i in range(3):
            linearr = []
            for j in range(3):
                if (j==i):
                    linearr.append(int(arr[i]))
                else:
                    linearr.append(0)
            supercell.append(linearr)
        supercell.append(list(map(float,[arr[3],arr[4],arr[5]])))

        obj['kpoints'] = {}
        obj['kpoints']["supercell_folding"] = supercell
    except:
        GeomIsOK = False

    if (GeomIsOK == False):
        GeomIsOK = True
        #try XYZ
        try:
            geom_obj = parseXYZFile(str(entry_path/'GEOM'))
            obj.update(geom_obj)

            spin = 1
            try:
                spin = int(read_single_line(str(entry_path/'SPIN'), False))
                obj['spin'] = spin
            except:
                pass

            if (spin < 1):
                raise ValueError("SPIN should >= 1, but read {}".format(spin))

            try:
                charge = int(read_single_line(str(entry_path/'CHARGE'), False))
                obj['charge'] = charge
            except:
                pass

            stat_point = read_single_line(str(entry_path/'STATIONARY'), False).strip()
            if (stat_point == '0' or stat_point.lower() == 'no' or stat_point.lower() == 'false'):
                obj["stationary_point"] = False
            elif (stat_point == '1' or stat_point.lower() == 'yes' or stat_point.lower() == 'true'):
                obj["stationary_point"] = True
            else:
                raise ValueError("STATIONARY should contain 0/1 or false/true or no/yes")
        except:
            GeomIsOK = False

    if (GeomIsOK == False):
        GeomIsOK = True
        try:
            geom_obj = parseGeomFile(str(entry_path/'GEOM'))
            obj.update(geom_obj)
            if (obj['pbc']):
                supercell = []
                sampling_str = " ".join(read_lines(str(entry_path/'SAMPLING'), False))
                arr = sampling_str.split()
                if (len(arr) != 6):
                    raise ValueError("Wrong format for SAMPLING file.")
                    

                for i in range(3):
                    linearr = []
                    for j in range(3):
                        if (j==i):
                            linearr.append(int(arr[i]))
                        else:
                            linearr.append(0)
                    supercell.append(linearr)
                supercell.append(list(map(float,[arr[3],arr[4],arr[5]])))

                obj['kpoints'] = {}
                obj['kpoints']["supercell_folding"] = supercell
            else:
                spin = 1
                try:
                    spin = int(read_single_line(str(entry_path/'SPIN'), False))
                    obj['spin'] = spin
                except:
                    pass

                if (spin < 1):
                    raise ValueError("SPIN should >= 1, but read {}".format(spin))

                try:
                    charge = int(read_single_line(str(entry_path/'CHARGE'), False))
                    obj['charge'] = charge
                except:
                    pass

                stat_point = read_single_line(str(entry_path/'STATIONARY'), False).strip()
                if (stat_point == '0' or stat_point.lower() == 'no' or stat_point.lower() == 'false'):
                    obj["stationary_point"] = False
                elif (stat_point == '1' or stat_point.lower() == 'yes' or stat_point.lower() == 'true'):
                    obj["stationary_point"] = True
                else:
                    raise ValueError("STATIONARY should contain 0/1 or false/true or no/yes")
        except:
            raise
            GeomIsOK = False

    if (GeomIsOK == False):
        raise ValueError("GEOM is not valid")

    geometry.append(obj)

print ("")
object_root['geometry'] = geometry

print ("JSON file written as folderbase.json")
with open("folderbase.json", "w") as f:
    print (json.dumps(object_root, sort_keys=True, indent=4, separators=(',', ': ')), file=f)


